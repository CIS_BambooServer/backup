package appium.tgo;

import com.tg.octopus.base.TGOWrapper;
import com.tg.octopus.libs.testng.listener.TestListener;
import static com.tg.octopus.base.TGOWrapper.Action.*;
import static com.tg.octopus.base.TGOWrapper.LOGGER;
import com.tg.octopus.wrapper.ActionWrapper;

import com.jayway.jsonpath.JsonPath;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.SessionNotCreatedException;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.Augmenter;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.remote.UnreachableBrowserException;

import org.testng.Assert;
import org.testng.ITestContext;
import org.testng.ITestResult;
import org.testng.Reporter;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;
import java.net.HttpURLConnection;
import java.net.URL;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import org.json.JSONArray;
import org.json.JSONObject;
import helper.WebTestUtility;
import java.sql.*;
import com.testautomationguru.utility.PDFUtil;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.multipdf.Splitter;

import java.util.*;
import org.openqa.selenium.*;
import java.io.*;
import java.net.*;

import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;


@Listeners(TestListener.class)
public class Test_Joe extends TGOWrapper {



    static {
       needsresetoncebeforetest ="false";
       needsresetbeforeeverymethod ="true";
       captureverystepsnap ="true";
       appendlastsnap ="false";
       ishybridapp ="false";

    }


    @Test
    public void logintogias() {

        // No need to declare / print method name here, handled by TestNg listener
        // declare("logintogias");

        CALL.$("LoginToGIAS","TGTYPESCREENREG");

        TYPE.$("ELE_LOGIN_USERNAMETEXTFIELD","//input[@type='text'][@name='outerForm:username']","mcgrath","TRUE","TGTYPESCREENREG");

        TYPE.$("ELE_LOGIN_PASSWORDTEXTFIELD","//input[@type='password'][@name='outerForm:password']","123456a","TRUE","TGTYPESCREENREG");

        TAP.$("ELE_LOGIN_LOGINBUTTON","//input[@type='submit'][@name='outerForm:loginButton']","TGTYPESCREENREG");


    }


    @Test
    public void addclient() {

        // No need to declare / print method name here, handled by TestNg listener
        // declare("addclient");

        CALL.$("LoginToGIAS","TGTYPESCREENREG");

        TYPE.$("ELE_LOGIN_USERNAMETEXTFIELD","//input[@type='text'][@name='outerForm:username']","mcgrath","TRUE","TGTYPESCREENREG");

        TYPE.$("ELE_LOGIN_PASSWORDTEXTFIELD","//input[@type='password'][@name='outerForm:password']","123456a","TRUE","TGTYPESCREENREG");

        TAP.$("ELE_LOGIN_LOGINBUTTON","//input[@type='submit'][@name='outerForm:loginButton']","TGTYPESCREENREG");

        CALL.$("AddAClient","TGTYPESCREENREG");

        HOVER.$("ELE_ADDCLIENT_NAVCLIENT","//td[text()='Names/Addresses']	","TGTYPESCREENREG");

        TAP.$("ELE_ADDCLIENT_NAMESADDRESSNAV","//td[text()='Names/Addresses']	","TGTYPESCREENREG");

        TAP.$("ELE_ADDCLIENT_ADDBUTTON","//*[@id=\"outerForm:gnAddImage\"]","TGTYPESCREENREG");

        TYPE.$("ELE_ADDCLIENT_FIRSTNAME","//*[@id=\"outerForm:nameFirst\"]","Tony","TRUE","TGTYPESCREENREG");

        TYPE.$("ELE_ADDCLIENT_LASTNAME","//*[@id=\"outerForm:nameLast\"]","Stark","TRUE","TGTYPESCREENREG");

        SELECT.$("ELE_ADDCLIENT_BIRTHSTATECOUNTRY","//*[@id=\"outerForm:birthCountryAndState2\"]","New York US","TGTYPESCREENREG");

        TYPE.$("ELE_ADDCLIENT_DATEOFBIRTH","//*[@id=\"outerForm:dateOfBirth2_input\"]","05/29/1970","TRUE","TGTYPESCREENREG");

        SELECT.$("ELE_ADDCLIENT_GENDER","//*[@id=\"outerForm:sexCode2\"]","Male","TGTYPESCREENREG");

        SELECT.$("ELE_ADDCLIENT_MARITALSTATUS","//*[@id=\"outerForm:maritalStatus2\"]","Single","TGTYPESCREENREG");

        SELECT.$("ELE_ADDCLIENT_IDTYPE","//*[@id=\"outerForm:taxIdenUsag2\"]","Social Security Number","TGTYPESCREENREG");

        TYPE.$("ELE_ADDCLIENT_IDNUMBER","//*[@id=\"outerForm:identificationNumber2\"]","123 45 6712","TRUE","TGTYPESCREENREG");

        TAP.$("ELE_ADDCLIENT_FIRSTSUBMITBUTTON","//*[@id=\"outerForm:Submit\"]","TGTYPESCREENREG");

        TYPE.$("ELE_ADDCLIENT_ADDRESSLINE1","//*[@id=\"outerForm:addressLineOne\"]","123 2nd Ave","TRUE","TGTYPESCREENREG");

        TYPE.$("ELE_ADDCLIENT_CITY","//*[@id=\"outerForm:addressCity\"]","Gramercy","TRUE","TGTYPESCREENREG");

        SELECT.$("ELE_ADDCLIENT_STATEANDCOUNTRY","//*[@id=\"outerForm:countryAndStateCode\"]","New York US","TGTYPESCREENREG");

        TYPE.$("ELE_ADDCLIENT_ZIPCODE","//*[@id=\"outerForm:zipCode\"]","10003","TRUE","TGTYPESCREENREG");

        TAP.$("ELE_ADDCLIENT_FINALSUBMITBUTTON","//*[@id=\"outerForm:Submit\"]","TGTYPESCREENREG");


    }


    @Test
    public void beneficiarychanges() {

        // No need to declare / print method name here, handled by TestNg listener
        // declare("beneficiarychanges");

        CALL.$("LoginToGIAS","TGTYPESCREENREG");

        TYPE.$("ELE_LOGIN_USERNAMETEXTFIELD","//input[@type='text'][@name='outerForm:username']","mcgrath","TRUE","TGTYPESCREENREG");

        TYPE.$("ELE_LOGIN_PASSWORDTEXTFIELD","//input[@type='password'][@name='outerForm:password']","123456a","TRUE","TGTYPESCREENREG");

        TAP.$("ELE_LOGIN_LOGINBUTTON","//input[@type='submit'][@name='outerForm:loginButton']","TGTYPESCREENREG");

        CALL.$("BeneficiaryChanges","TGTYPESCREENREG");

        HOVER.$("ELE_BC_CLIENTNAV","//td[text()='Client']","TGTYPESCREENREG");

        TAP.$("ELE_BC_POLICYRELATIONSHIPNAV","//td[text()='Policy Relationships']","TGTYPESCREENREG");

        TYPE.$("ELE_BC_POLICYNUMBER","//input[@type='text'][@name='outerForm:policyNumber']","BRAUTO9012","TRUE","TGTYPESCREENREG");

        TAP.$("ELE_BC_CONTINUEBUTTON","//input[@type='submit'][@name='outerForm:Continue']","TGTYPESCREENREG");

        TAP.$("ELE_BC_BCLINK","//span[text()='Beneficiary (Contract)']","TGTYPESCREENREG");

        TAP.$("ELE_BC_UPDATEBUTTON","//input[@type='submit'][@name='outerForm:Update']","TGTYPESCREENREG");

        TAP.$("ELE_BC_SELECTCLIENT","//span[text()='Select Client']","TGTYPESCREENREG");

        TAP.$("ELE_BC_ADDBUTTON","//input[@type='submit'][@name='outerForm:Add']","TGTYPESCREENREG");

        TYPE.$("ELE_BC_FIRSTNAME","//input[@type='text'][@name='outerForm:nameFirst']","John","TRUE","TGTYPESCREENREG");

        TYPE.$("ELE_BC_LASTNAME","//input[@type='text'][@name='outerForm:nameLast']","Whick","TRUE","TGTYPESCREENREG");

        SELECT.$("ELE_BC_BIRTHSTATETCOUNTRY","//select[@name='outerForm:birthCountryAndState2']","Florida US","TGTYPESCREENREG");

        TYPE.$("ELE_BC_DATEOFBIRTH","//input[@type='text'][@name='outerForm:dateOfBirth2']","01/11/1976","TRUE","TGTYPESCREENREG");

        SELECT.$("ELE_BC_GENDER","//select[@name='outerForm:sexCode2']","Male","TGTYPESCREENREG");

        SELECT.$("ELE_BC_IDTYPE","//select[@name='outerForm:taxIdenUsag2']","Social Security Number","TGTYPESCREENREG");

        TYPE.$("ELE_BC_IDNUMBER","//input[@type='text'][@name='outerForm:identificationNumber2']","111 11 3322","TRUE","TGTYPESCREENREG");

        TAP.$("ELE_BC_SUBMITBUTTON","//input[@type='submit'][@name='outerForm:Submit']","TGTYPESCREENREG");

        TYPE.$("ELE_BC_ADDRESSLINE1","//input[@type='text'][@name='outerForm:addressLineOne']","123 Anywhere St","TRUE","TGTYPESCREENREG");

        TYPE.$("ELE_BC_CITY","//input[@type='text'][@name='outerForm:addressCity']","Tampa","TRUE","TGTYPESCREENREG");

        SELECT.$("ELE_BC_STATEANDCOUNTRY","//select[@name='outerForm:countryAndStateCode']","Florida US","TGTYPESCREENREG");

        TYPE.$("ELE_BC_ZIPCODE","//input[@type='text'][@name='outerForm:zipCode']","33601","TRUE","TGTYPESCREENREG");

        TAP.$("ELE_BC_SUBMITBUTTON2","//input[@type='submit'][@name='outerForm:Submit']","TGTYPESCREENREG");

        TAP.$("ELE_BC_CANCELBUTTON","//input[@type='submit'][@name='outerForm:Cancel']","TGTYPESCREENREG");

        SELECT.$("ELE_BC_RELATIONSHIPDROPDOWN","//select[@name='outerForm:grid:0:relationship']","Beneficiary (Contract)","TGTYPESCREENREG");

        TAP.$("ELE_BC_SUBMITBUTTON3","//input[@type='submit'][@name='outerForm:Submit']","TGTYPESCREENREG");

        TAP.$("ELE_BC_SUBMITBUTTON4","//input[@type='submit'][@name='outerForm:Submit']","TGTYPESCREENREG");

        TAP.$("ELE_BC_CANCELBUTTON2","//input[@type='submit'][@name='outerForm:Cancel']","TGTYPESCREENREG");


    }


    @Test
    public void addagent() {

        // No need to declare / print method name here, handled by TestNg listener
        // declare("addagent");

        CALL.$("LoginToGIAS","TGTYPESCREENREG");

        TYPE.$("ELE_LOGIN_USERNAMETEXTFIELD","//input[@type='text'][@name='outerForm:username']","mcgrath","TRUE","TGTYPESCREENREG");

        TYPE.$("ELE_LOGIN_PASSWORDTEXTFIELD","//input[@type='password'][@name='outerForm:password']","123456a","TRUE","TGTYPESCREENREG");

        TAP.$("ELE_LOGIN_LOGINBUTTON","//input[@type='submit'][@name='outerForm:loginButton']","TGTYPESCREENREG");

        CALL.$("AddAgent","TGTYPESCREENREG");

        HOVER.$("ELE_CREATEAGENT_AGENCYNAV","//td[text()='Agency']	","TGTYPESCREENREG");

        TAP.$("ELE_CREATEAGENT_AGENTNAV","//td[text()='Agents']	","TGTYPESCREENREG");

        TAP.$("ELE_CREATEAGENT_ADDBUTTON","//img[@id='outerForm:gnAddImage1']","TGTYPESCREENREG");

        TAP.$("ELE_CREATEAGENT_SELECTCLIENTTAB","//span[text()='Select Client']	","TGTYPESCREENREG");

        TYPE.$("ELE_CREATEAGENT_CLIENTNAMESEARCH","//input[@type='text'][@name='outerForm:grid:individualName']	","Sparrow Jack","TRUE","TGTYPESCREENREG");

        TAP.$("ELE_CREATEAGENT_FINDPOSITIONTOBUTTON","//img[@id='outerForm:gnFindImage']	","TGTYPESCREENREG");

        SELECT.$("ELE_CREATEAGENT_NAMESELECTIONDROPDOWN","//select[@name='outerForm:grid:0:relationship']	","Agent","TGTYPESCREENREG");

        TAP.$("ELE_CREATEAGENT_SUBMITBUTTON1","//input[@type='submit'][@name='outerForm:Submit']	","TGTYPESCREENREG");

        TYPE.$("ELE_CREATEAGENT_AGENTNUMBER","//input[@type='text'][@name='outerForm:agentNumber']	","1121","TRUE","TGTYPESCREENREG");

        SELECT.$("ELE_CREATEAGENT_AGENTSTATUSDROPDOWN","//select[@name='outerForm:agentStatus']","Active","TGTYPESCREENREG");

        TYPE.$("ELE_CREATEAGENT_DATEHIRED","//input[@type='text'][@name='outerForm:hireDate']	","11/05/2018","TRUE","TGTYPESCREENREG");

        SELECT.$("ELE_CREATEAGENT_DOMICILESTATECOUNTRYDROPDOWN","//select[@name='outerForm:countryAndStateCode']","New York US","TGTYPESCREENREG");

        SELECT.$("ELE_CREATEAGENT_DISTRUBTIONCHANNELDROPDOWN","//select[@name='outerForm:distributionChannel']","test","TGTYPESCREENREG");

        SELECT.$("ELE_CREATEAGENT_AGENTRANKDROPDOWN","//select[@name='outerForm:agencyRankCode']","Writing Agent","TGTYPESCREENREG");

        TAP.$("ELE_CREATEAGENT_SUBMITBUTTON2","//input[@type='submit'][@name='outerForm:Submit']	","TGTYPESCREENREG");


    }


    @Test
    public void createdistrubtionchannel() {

        // No need to declare / print method name here, handled by TestNg listener
        // declare("createdistrubtionchannel");

        CALL.$("LoginToGIAS","TGTYPESCREENREG");

        TYPE.$("ELE_LOGIN_USERNAMETEXTFIELD","//input[@type='text'][@name='outerForm:username']","mcgrath","TRUE","TGTYPESCREENREG");

        TYPE.$("ELE_LOGIN_PASSWORDTEXTFIELD","//input[@type='password'][@name='outerForm:password']","123456a","TRUE","TGTYPESCREENREG");

        TAP.$("ELE_LOGIN_LOGINBUTTON","//input[@type='submit'][@name='outerForm:loginButton']","TGTYPESCREENREG");

        CALL.$("CreateDistrubtionChannel","TGTYPESCREENREG");

        HOVER.$("ELE_CREATEDISTRUBTIONCHANNEL_AGENCYNAV","//td[text()='Agency']","TGTYPESCREENREG");

        TAP.$("ELE_CREATEDISTRUBTIONCHANNEL_DISTCHANNELNAV","//td[text()='Distribution Channel']","TGTYPESCREENREG");

        TAP.$("ELE_CREATEDISTRUBTIONCHANNEL_ADDBUTTON","//input[@type='submit'][@name='outerForm:Add']","TGTYPESCREENREG");

        TYPE.$("ELE_CREATEDISTRUBTIONCHANNEL_DISTRCHANNELNAME","//input[@type='text'][@name='outerForm:distributionChanName']","DCTest01","TRUE","TGTYPESCREENREG");

        TYPE.$("ELE_CREATEDISTRUBTIONCHANNEL_DISTCHANNELDESCRIPTION","//input[@type='text'][@name='outerForm:channelDescription']","DCTest01 Automation Test","TRUE","TGTYPESCREENREG");

        TYPE.$("ELE_CREATEDISTRUBTIONCHANNEL_CODE","//input[@type='text'][@name='outerForm:duf_dist_code']","DCT01","TRUE","TGTYPESCREENREG");

        TAP.$("ELE_CREATEDISTRUBTIONCHANNEL_SUBMITBUTTON","//input[@type='submit'][@name='outerForm:Submit']","TGTYPESCREENREG");


    }


    @Test
    public void addevent() {

        // No need to declare / print method name here, handled by TestNg listener
        // declare("addevent");

        CALL.$("LoginToGIAS","TGTYPESCREENREG");

        TYPE.$("ELE_LOGIN_USERNAMETEXTFIELD","//input[@type='text'][@name='outerForm:username']","mcgrath","TRUE","TGTYPESCREENREG");

        TYPE.$("ELE_LOGIN_PASSWORDTEXTFIELD","//input[@type='password'][@name='outerForm:password']","123456a","TRUE","TGTYPESCREENREG");

        TAP.$("ELE_LOGIN_LOGINBUTTON","//input[@type='submit'][@name='outerForm:loginButton']","TGTYPESCREENREG");

        CALL.$("AddEvent","TGTYPESCREENREG");

        HOVER.$("ELE_ADDEVENT_COMPANYPROPERTIESNAV","//td[text()='Company Properties']","TGTYPESCREENREG");

        HOVER.$("ELE_ADDEVENT_NONTRADITIONALNAV","//td[text()='Nontraditional']","TGTYPESCREENREG");

        TAP.$("ELE_ADDEVENT_EVENTDESCRIPTION","//td[text()='Event Descriptions']","TGTYPESCREENREG");

        TAP.$("ELE_ADDEVENT_ADDBUTTON","//img[@id='outerForm:gnAddImage']","TGTYPESCREENREG");

        TYPE.$("ELE_ADDEVENT_TYPE","//input[@type='text'][@name='outerForm:eventType']","001","TRUE","TGTYPESCREENREG");

        TYPE.$("ELE_ADDEVENT_EVENTDESCRIPTION","//td[text()='Event Descriptions']","Test 001","TRUE","TGTYPESCREENREG");

        TAP.$("ELE_ADDEVENT_CREATECONFIRMBUTTON","//input[@type='radio'][@name='outerForm:writeConfrimationRcd']","TGTYPESCREENREG");

        TAP.$("ELE_ADDEVENT_SUBMITBUTTON","//input[@type='submit'][@name='outerForm:Submit']","TGTYPESCREENREG");


    }


    @Test
    public void benefitstatusschange() {

        // No need to declare / print method name here, handled by TestNg listener
        // declare("benefitstatusschange");

        CALL.$("LoginToGIAS","TGTYPESCREENREG");

        TYPE.$("ELE_LOGIN_USERNAMETEXTFIELD","//input[@type='text'][@name='outerForm:username']","mcgrath","TRUE","TGTYPESCREENREG");

        TYPE.$("ELE_LOGIN_PASSWORDTEXTFIELD","//input[@type='password'][@name='outerForm:password']","123456a","TRUE","TGTYPESCREENREG");

        TAP.$("ELE_LOGIN_LOGINBUTTON","//input[@type='submit'][@name='outerForm:loginButton']","TGTYPESCREENREG");

        CALL.$("BenefitStatusChange","TGTYPESCREENREG");

        HOVER.$("ELE_BENEFITSTATUSCHANGE_POLICYOWNERSERVICENAV","//td[text()='Policyowner Services']","TGTYPESCREENREG");

        HOVER.$("ELE_BENEFITSTATUSCHANGE_BENEFITMAITENCENAV","//td[text()='Benefit Maintenance']","TGTYPESCREENREG");

        TAP.$("ELE_BENEFITSTATUSCHANGE_BENEFITSTATUSCHANGENAV","//td[text()='Benefit Status Change']","TGTYPESCREENREG");

        TYPE.$("ELE_BENEFITSTATUSCHANGE_POLICYNUMBER","//input[@type='text'][@name='outerForm:policyNumber']","BRAUTO9015","TRUE","TGTYPESCREENREG");

        TAP.$("ELE_BENEFITSTATUSCHANGE_CONTINUEBUTTON","//input[@type='submit'][@name='outerForm:Continue']","TGTYPESCREENREG");

        TAP.$("ELE_BENEFITSTATUSCHANGE_BASELINK","//span[text()='Base']","TGTYPESCREENREG");

        SELECT.$("ELE_BENEFITSTATUSCHANGE_NEWBENEFIT","//select[@name='outerForm:newBenefitStatus']","Cancelled","TGTYPESCREENREG");

        TYPE.$("ELE_BENEFITSTATUSCHANGE_TERMINATIONDATE","//input[@type='text'][@name='outerForm:terminationDate']","12/12/2019","TRUE","TGTYPESCREENREG");

        TAP.$("ELE_BENEFITSTATUSCHANGE_SUBMITBUTTON","//input[@type='submit'][@name='outerForm:Submit']","TGTYPESCREENREG");


    }


    @Test
    public void transactionhistorylookup() {

        // No need to declare / print method name here, handled by TestNg listener
        // declare("transactionhistorylookup");

        CALL.$("LoginToGIAS","TGTYPESCREENREG");

        TYPE.$("ELE_LOGIN_USERNAMETEXTFIELD","//input[@type='text'][@name='outerForm:username']","mcgrath","TRUE","TGTYPESCREENREG");

        TYPE.$("ELE_LOGIN_PASSWORDTEXTFIELD","//input[@type='password'][@name='outerForm:password']","123456a","TRUE","TGTYPESCREENREG");

        TAP.$("ELE_LOGIN_LOGINBUTTON","//input[@type='submit'][@name='outerForm:loginButton']","TGTYPESCREENREG");

        CALL.$("TransactionHistoryLookUp","TGTYPESCREENREG");

        HOVER.$("ELE_TRANSACTIONHISTORY_INQUIRYNAV","//td[text()='Inquiry']	","TGTYPESCREENREG");

        TAP.$("ELE_TRANSACTOINHISTORY_QUICKINQUIRYNAV","//td[text()='Quick Inquiry']","TGTYPESCREENREG");

        TYPE.$("ELE_TRANSACTIONHISTORY_POLICYNUMBERTXTBOX","//input[@type='text'][@name='outerForm:policyNumber']","BRAUTO9012","TRUE","TGTYPESCREENREG");

        TAP.$("ELE_TRANSACTIONHISTORY_CONTINUEBUTTON","//input[@type='submit'][@name='outerForm:Continue']","TGTYPESCREENREG");

        TAP.$("ELE_TRANSACTIONHISTORY_POLICYINQUIRYLINK","//span[text()='Policy Inquiry']","TGTYPESCREENREG");

        TAP.$("ELE_TRANSACTIONHISTORY_TRANSACTIONHISTROYLINK","//span[text()='Transaction History']","TGTYPESCREENREG");

        TAP.$("ELE_TRANSACTIONHISTORY_TRANSACTIONDATESELECTION","//span[text()='03/19/2028']","TGTYPESCREENREG");


    }


    @Test
    public void freelookadd() {

        // No need to declare / print method name here, handled by TestNg listener
        // declare("freelookadd");

        CALL.$("LoginToGIAS","TGTYPESCREENREG");

        TYPE.$("ELE_LOGIN_USERNAMETEXTFIELD","//input[@type='text'][@name='outerForm:username']","mcgrath","TRUE","TGTYPESCREENREG");

        TYPE.$("ELE_LOGIN_PASSWORDTEXTFIELD","//input[@type='password'][@name='outerForm:password']","123456a","TRUE","TGTYPESCREENREG");

        TAP.$("ELE_LOGIN_LOGINBUTTON","//input[@type='submit'][@name='outerForm:loginButton']","TGTYPESCREENREG");

        CALL.$("FreeLookAdd","TGTYPESCREENREG");

        HOVER.$("ELE_FREELOOKADD_POLICYMAITENCENAV","//td[text()='Policyowner Services']","TGTYPESCREENREG");

        HOVER.$("ELE_FREELOOKADD_NONTRADITIONALNAV","//td[text()='Nontraditional']","TGTYPESCREENREG");

        TAP.$("ELE_FREELOOKADD_FREELOOKPROVISIONSCHEDULE","//td[text()='Free Look Provision Schedules']","TGTYPESCREENREG");

        TAP.$("ELE_FREELOOKADD_ADDBUTTON","//img[@id='outerForm:gnAddImage']","TGTYPESCREENREG");

        TYPE.$("ELE_FREELOOKADD_TABLEFIELD","//input[@type='text'][@name='outerForm:freeLookProvSchTbl']","BRAT1","TRUE","TGTYPESCREENREG");

        SELECT.$("ELE_FREELOOKADD_STATECOUNTRY","//select[@name='outerForm:countryAndStateCode']","New York US","TGTYPESCREENREG");

        TYPE.$("ELE_FREELOOKADD_ENDINGAGE","//input[@type='text'][@name='outerForm:endingAge']","80","TRUE","TGTYPESCREENREG");

        TYPE.$("ELE_FREELOOKADD_NUMBEROFDAYS","//input[@type='text'][@name='outerForm:freeLookNumberOfDays']","364","TRUE","TGTYPESCREENREG");

        SELECT.$("ELE_FREELOOKADD_INITALALLOCATIONRULE","//select[@name='outerForm:initialAllocRule']","Allocate Premium Immediately","TGTYPESCREENREG");

        TAP.$("ELE_FREELOOKADD_SUBMITBUTTON","//input[@type='submit'][@name='outerForm:Submit']","TGTYPESCREENREG");


    }


    @Test
    public void changenonforfeiture() {

        // No need to declare / print method name here, handled by TestNg listener
        // declare("changenonforfeiture");

        CALL.$("LoginToGIAS","TGTYPESCREENREG");

        TYPE.$("ELE_LOGIN_USERNAMETEXTFIELD","//input[@type='text'][@name='outerForm:username']","mcgrath","TRUE","TGTYPESCREENREG");

        TYPE.$("ELE_LOGIN_PASSWORDTEXTFIELD","//input[@type='password'][@name='outerForm:password']","123456a","TRUE","TGTYPESCREENREG");

        TAP.$("ELE_LOGIN_LOGINBUTTON","//input[@type='submit'][@name='outerForm:loginButton']","TGTYPESCREENREG");

        CALL.$("ChangeNonForfeiture","TGTYPESCREENREG");

        HOVER.$("ELE_NONFORFITURE_BILLINGNAV","//td[text()='Billing']","TGTYPESCREENREG");

        TAP.$("ELE_NONFORFEITURE_POLICYCONTRACTSNAV","//td[text()='Policy Contracts']","TGTYPESCREENREG");

        TYPE.$("ELE_NONFORFEITURE_POLICYNUM","//input[@type='text'][@name='outerForm:policyNumber']","BRAUTO9015","TRUE","TGTYPESCREENREG");

        TYPE.$("ELE_NONFORFEITURE_APPLYDATE","//input[@type='text'][@name='outerForm:promptDate']","4/30/2019","TRUE","TGTYPESCREENREG");

        TAP.$("ELE_NONFORFEITURE_CONTINUEBUTTON","/input[@type='submit'][@name='outerForm:Continue']","TGTYPESCREENREG");

        SELECT.$("ELE_NONFORFEITURE_SELECTNFO","/input[@type='submit'][@name='outerForm:Continue']","Extended Term","TGTYPESCREENREG");

        TAP.$("ELE_NONFORFEITURE_SUBMITBUTTON","//input[@type='submit'][@name='outerForm:Submit']","TGTYPESCREENREG");


    }


    @Test
    public void clientaddresschange() {

        // No need to declare / print method name here, handled by TestNg listener
        // declare("clientaddresschange");

        CALL.$("LoginToGIAS","TGTYPESCREENREG");

        TYPE.$("ELE_LOGIN_USERNAMETEXTFIELD","//input[@type='text'][@name='outerForm:username']","mcgrath","TRUE","TGTYPESCREENREG");

        TYPE.$("ELE_LOGIN_PASSWORDTEXTFIELD","//input[@type='password'][@name='outerForm:password']","123456a","TRUE","TGTYPESCREENREG");

        TAP.$("ELE_LOGIN_LOGINBUTTON","//input[@type='submit'][@name='outerForm:loginButton']","TGTYPESCREENREG");

        CALL.$("ClientAddressChange","TGTYPESCREENREG");

        HOVER.$("ELE_CLIENTADDRESSCHANGE_CLIENTNAV","//td[text()='Client']","TGTYPESCREENREG");

        TAP.$("ELE_CLIENTADDRESSCHANGE_NAMESADDRESSESNAV","//td[text()='Names/Addresses']","TGTYPESCREENREG");

        TYPE.$("ELE_CLIENTADDRESSCHANGE_NAMETXTBOX","//input[@type='text'][@name='outerForm:grid:individualName']","Sparrow JJ","TRUE","TGTYPESCREENREG");

        TAP.$("ELE_CLIENTADDRESSCHANGE_FINDPOSITIONTO","//img[@id='outerForm:gnFindImage']","TGTYPESCREENREG");

        TAP.$("ELE_CLIENTADDRESSCHANGE_NAMESELECTION","//span[text()='Sparrow, JJ']	","TGTYPESCREENREG");

        TAP.$("ELE_CLIENTADDRESSCHANGE_ADDRESSINFORMATION","//span[text()='Address Information']","TGTYPESCREENREG");

        TAP.$("ELE_CLIENTADDRESSCHANGE_CLIENTADDRESSLINK","//span[text()='123 1st AVe, New York, NY New Jersey US 07719']","TGTYPESCREENREG");

        TAP.$("ELE_CLIENTADDRESSCHANGE_UPDATEBUTTON","//input[@type='submit'][@name='outerForm:Update']","TGTYPESCREENREG");

        TYPE.$("ELE_CLIENTADDRESSCHANGE_ADDRESSLINE1","//input[@type='text'][@name='outerForm:addressLineOne']","321 West End","TRUE","TGTYPESCREENREG");

        TYPE.$("ELE_CLLIENTADDRESSCHANGE_CITY","//input[@type='text'][@name='outerForm:addressCity']","Longbranch","TRUE","TGTYPESCREENREG");

        TAP.$("ELE_CLIENTADDRESSCHANGE_SUBMITBUTTON","//input[@type='submit'][@name='outerForm:Submit']","TGTYPESCREENREG");


    }


    @Test
    public void inputdataaddresschange() {

        // No need to declare / print method name here, handled by TestNg listener
        // declare("inputdataaddresschange");

        CALL.$("LoginToGIAS","TGTYPESCREENREG");

        TYPE.$("ELE_LOGIN_USERNAMETEXTFIELD","//input[@type='text'][@name='outerForm:username']","mcgrath","TRUE","TGTYPESCREENREG");

        TYPE.$("ELE_LOGIN_PASSWORDTEXTFIELD","//input[@type='password'][@name='outerForm:password']","123456a","TRUE","TGTYPESCREENREG");

        TAP.$("ELE_LOGIN_LOGINBUTTON","//input[@type='submit'][@name='outerForm:loginButton']","TGTYPESCREENREG");

		int var_Count;
                 LOGGER.info("Executed Step = VAR,Integer,var_Count,TGTYPESCREENREG");
		var_Count = 0;
                 LOGGER.info("Executed Step = STORE,var_Count,0,TGTYPESCREENREG");
        LOGGER.info("Executed Step = REPEAT IF");
    	while (check(var_Count,"<",1,"TGTYPESCREENREG")) {
          LOGGER.info("Executed Step = WHILE,var_Count,<,1,TGTYPESCREENREG");
        CALL.$("AddressChangeInputData","TGTYPESCREENREG");

		com.jayway.jsonpath.ReadContext var_CSVData = getJsonData("C:/GIASAutomation/3.6 BaseRegression/InputData/36Reg_AddressChanges.csv");
                 LOGGER.info("Executed Step = VAR,Json,var_CSVData,C:/GIASAutomation/3.6 BaseRegression/InputData/36Reg_AddressChanges.csv,TGTYPESCREENREG");
		String var_ClientLastNameFirstName;
                 LOGGER.info("Executed Step = VAR,String,var_ClientLastNameFirstName,TGTYPESCREENREG");
		var_ClientLastNameFirstName = getJsonData(var_CSVData , "$.records["+var_Count+"].ClientLastNameFirstName");
                 LOGGER.info("Executed Step = STORE,var_ClientLastNameFirstName,var_CSVData,$.records[{var_Count}].ClientLastNameFirstName,TGTYPESCREENREG");
		String var_NewStreetName;
                 LOGGER.info("Executed Step = VAR,String,var_NewStreetName,TGTYPESCREENREG");
		var_NewStreetName = getJsonData(var_CSVData , "$.records["+var_Count+"].NewStreetName");
                 LOGGER.info("Executed Step = STORE,var_NewStreetName,var_CSVData,$.records[{var_Count}].NewStreetName,TGTYPESCREENREG");
		String var_NewCity;
                 LOGGER.info("Executed Step = VAR,String,var_NewCity,TGTYPESCREENREG");
		var_NewCity = getJsonData(var_CSVData , "$.records["+var_Count+"].NewCity");
                 LOGGER.info("Executed Step = STORE,var_NewCity,var_CSVData,$.records[{var_Count}].NewCity,TGTYPESCREENREG");
		String var_NewStateandCountry;
                 LOGGER.info("Executed Step = VAR,String,var_NewStateandCountry,TGTYPESCREENREG");
		var_NewStateandCountry = getJsonData(var_CSVData , "$.records["+var_Count+"].NewStateandCountry");
                 LOGGER.info("Executed Step = STORE,var_NewStateandCountry,var_CSVData,$.records[{var_Count}].NewStateandCountry,TGTYPESCREENREG");
		String var_NewZip;
                 LOGGER.info("Executed Step = VAR,String,var_NewZip,TGTYPESCREENREG");
		var_NewZip = getJsonData(var_CSVData , "$.records["+var_Count+"].NewZip");
                 LOGGER.info("Executed Step = STORE,var_NewZip,var_CSVData,$.records[{var_Count}].NewZip,TGTYPESCREENREG");
        HOVER.$("ELE_CLIENTADDRESSCHANGE_CLIENTNAV","//td[text()='Client']","TGTYPESCREENREG");

        TAP.$("ELE_CLIENTADDRESSCHANGE_NAMESADDRESSESNAV","//td[text()='Names/Addresses']","TGTYPESCREENREG");

        TYPE.$("ELE_CLIENTADDRESSCHANGE_NAMETXTBOX","//input[@type='text'][@name='outerForm:grid:individualName']",var_ClientLastNameFirstName,"FALSE","TGTYPESCREENREG");

        TAP.$("ELE_CLIENTADDRESSCHANGE_FINDPOSITIONTO","//img[@id='outerForm:gnFindImage']","TGTYPESCREENREG");

        TAP.$("ELE_CLIENTADDRESSCHANGE_NAMESELECTION","//span[text()='Sparrow, JJ']	","TGTYPESCREENREG");

        TAP.$("ELE_CLIENTADDRESSCHANGE_ADDRESSINFORMATION","//span[text()='Address Information']","TGTYPESCREENREG");

        TAP.$("ELE_CLIENTADDRESSCHANGE_CLIENTADDRESSLINK","//span[text()='123 1st AVe, New York, NY New Jersey US 07719']","TGTYPESCREENREG");

        TAP.$("ELE_CLIENTADDRESSCHANGE_UPDATEBUTTON","//input[@type='submit'][@name='outerForm:Update']","TGTYPESCREENREG");

        TYPE.$("ELE_CLIENTADDRESSCHANGE_ADDRESSLINE1","//input[@type='text'][@name='outerForm:addressLineOne']",var_NewStreetName,"FALSE","TGTYPESCREENREG");

        TYPE.$("ELE_CLLIENTADDRESSCHANGE_CITY","//input[@type='text'][@name='outerForm:addressCity']",var_NewCity,"FALSE","TGTYPESCREENREG");

        TYPE.$("ELE_CLIENTADDRESSCHANGE_STATEANDCOUNTRY","//select[@name='outerForm:countryAndStateCode']",var_NewStateandCountry,"FALSE","TGTYPESCREENREG");

        TYPE.$("ELE_CLIENTADDRESSCHANGE_ZIPCODE","//input[@type='text'][@name='outerForm:zipCode']",var_NewZip,"FALSE","TGTYPESCREENREG");

        TAP.$("ELE_CLIENTADDRESSCHANGE_SUBMITBUTTON","//input[@type='submit'][@name='outerForm:Submit']","TGTYPESCREENREG");

		var_Count = var_Count + ( 1 );
                 LOGGER.info("Executed Step = INCREASEVAR,var_Count,1,TGTYPESCREENREG");
    	}
          LOGGER.info("Executed Step = WEND,TGTYPESCREENREG");
    	resetAttemps();

    }


}
