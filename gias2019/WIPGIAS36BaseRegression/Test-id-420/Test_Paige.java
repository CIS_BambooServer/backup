package appium.tgo;

import com.tg.octopus.base.TGOWrapper;
import com.tg.octopus.libs.testng.listener.TestListener;
import static com.tg.octopus.base.TGOWrapper.Action.*;
import static com.tg.octopus.base.TGOWrapper.LOGGER;
import com.tg.octopus.wrapper.ActionWrapper;

import com.jayway.jsonpath.JsonPath;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.SessionNotCreatedException;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.Augmenter;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.remote.UnreachableBrowserException;

import org.testng.Assert;
import org.testng.ITestContext;
import org.testng.ITestResult;
import org.testng.Reporter;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;
import java.net.HttpURLConnection;
import java.net.URL;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import org.json.JSONArray;
import org.json.JSONObject;
import helper.WebTestUtility;
import java.sql.*;
import com.testautomationguru.utility.PDFUtil;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.multipdf.Splitter;

import java.util.*;
import org.openqa.selenium.*;
import java.io.*;
import java.net.*;

import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;


@Listeners(TestListener.class)
public class Test_Paige extends TGOWrapper {



    static {
       needsresetoncebeforetest ="false";
       needsresetbeforeeverymethod ="true";
       captureverystepsnap ="true";
       appendlastsnap ="false";
       ishybridapp ="false";

    }


    @Test
    public void t01changenfo() {

        // No need to declare / print method name here, handled by TestNg listener
        // declare("t01changenfo");

        CALL.$("loginGIAS","TGTYPESCREENREG");

        TYPE.$("ELE_LOGIN_USERNAME","//input[@type='text'][@name='outerForm:username']","Eagleton","TRUE","TGTYPESCREENREG");

        TYPE.$("ELE_LOGIN_PASSWORD","//input[@type='password'][@name='outerForm:password']","123456a","TRUE","TGTYPESCREENREG");

        TAP.$("ELE_LOGIN_LOGINBUTTON","//input[@type='submit'][@name='outerForm:loginButton']","TGTYPESCREENREG");

        CALL.$("ChangeNFO","TGTYPESCREENREG");

        HOVER.$("ELE_NONFORFEITURE_BILLINGNAV","//td[text()='Billing']","TGTYPESCREENREG");

        TAP.$("ELE_NONFORFEITURE_POLICYCONTRACTSNAV","//td[text()='Policy Contracts']","TGTYPESCREENREG");

        TYPE.$("ELE_NONFORFEITURE_POLICYNUM","//input[@type='text'][@name='outerForm:policyNumber']","BRAUTO9021","TRUE","TGTYPESCREENREG");

        TYPE.$("ELE_NONFORFEITURE_APPLYDATE","//input[@type='text'][@name='outerForm:promptDate']","04/25/2019","TRUE","TGTYPESCREENREG");

        TAP.$("ELE_NONFORFEITURE_CONTINUEBUTTON","//input[@type='submit'][@name='outerForm:Continue']","TGTYPESCREENREG");

        SELECT.$("ELE_NONFORFEITURE_SELECTNFO","//select[@name='outerForm:nonForfeitureOption']","Extended Term","TGTYPESCREENREG");

        TAP.$("ELE_NONFORFEITURE_SUBMITBUTTON","//input[@type='submit'][@name='outerForm:Submit']","TGTYPESCREENREG");


    }


    @Test
    public void t02createclient() {

        // No need to declare / print method name here, handled by TestNg listener
        // declare("t02createclient");

        CALL.$("loginGIAS","TGTYPESCREENREG");

        TYPE.$("ELE_LOGIN_USERNAME","//input[@type='text'][@name='outerForm:username']","Eagleton","TRUE","TGTYPESCREENREG");

        TYPE.$("ELE_LOGIN_PASSWORD","//input[@type='password'][@name='outerForm:password']","123456a","TRUE","TGTYPESCREENREG");

        TAP.$("ELE_LOGIN_LOGINBUTTON","//input[@type='submit'][@name='outerForm:loginButton']","TGTYPESCREENREG");

        CALL.$("CreateClient","TGTYPESCREENREG");

        HOVER.$("ELE_CREATECLIENT_NAVCLIENT","//td[text()='Client']","TGTYPESCREENREG");

        TAP.$("ELE_CREATECLIENT_NAMESADDRESSESNAV","//td[text()='Names/Addresses']	","TGTYPESCREENREG");

        TAP.$("ELE_CREATECLIENT_ADDBUTTON","//input[@type='submit'][@name='outerForm:Add']","TGTYPESCREENREG");

        TYPE.$("ELE_CREATECLIENT_FIRSTNAME","//input[@type='text'][@name='outerForm:nameFirst']","Sam","TRUE","TGTYPESCREENREG");

        TYPE.$("ELE_CREATECLIENT_LASTNAME","//input[@type='text'][@name='outerForm:nameLast']","Smith","TRUE","TGTYPESCREENREG");

        SELECT.$("ELE_CREATECLIENT_BIRTHSTATECOUNTRY","//select[@name='outerForm:birthCountryAndState2']","New York US","TGTYPESCREENREG");

        TYPE.$("ELE_CREATECLIENT_BIRTHDATE","//input[@type='text'][@name='outerForm:dateOfBirth2']","08/14/1986","TRUE","TGTYPESCREENREG");

        SELECT.$("ELE_CREATECLIENT_GENDER","//select[@name='outerForm:sexCode2']","Female","TGTYPESCREENREG");

        SELECT.$("ELE_CREATECLIENT_MARITALSTATUS","//select[@name='outerForm:maritalStatus2']","Single","TGTYPESCREENREG");

        SELECT.$("ELE_CREATECLIENT_IDTYPE","//select[@name='outerForm:taxIdenUsag2']","Social Security Number","TGTYPESCREENREG");

        TYPE.$("ELE_CREATECLIENT_IDNUMBER","//input[@type='text'][@name='outerForm:identificationNumber2']","205 29 3910","TRUE","TGTYPESCREENREG");

        TAP.$("ELE_CREATECLIENT_FIRSTSUBMITBUTTON","//input[@type='submit'][@name='outerForm:Submit']","TGTYPESCREENREG");

        TYPE.$("ELE_CREATECLIENT_ADDRESSLINE","//input[@type='text'][@name='outerForm:addressLineOne']","30 Oak Tree Circle","TRUE","TGTYPESCREENREG");

        TYPE.$("ELE_CREATECLIENT_ADDRESSCITY","//input[@type='text'][@name='outerForm:addressCity']","Queensbury","TRUE","TGTYPESCREENREG");

        SELECT.$("ELE_CREATECLIENT_ADDRESSSTATECITY","//select[@name='outerForm:countryAndStateCode']","New York US","TGTYPESCREENREG");

        TYPE.$("ELE_CREATECLIENT_ADDRESSZIPCODE","//input[@type='text'][@name='outerForm:zipCode']","12804","TRUE","TGTYPESCREENREG");

        TAP.$("ELE_CREATECLIENT_SUBMITBUTTON","//input[@type='submit'][@name='outerForm:Submit']","TGTYPESCREENREG");


    }


    @Test
    public void t03createdistch() {

        // No need to declare / print method name here, handled by TestNg listener
        // declare("t03createdistch");

        CALL.$("loginGIAS","TGTYPESCREENREG");

        TYPE.$("ELE_LOGIN_USERNAME","//input[@type='text'][@name='outerForm:username']","Eagleton","TRUE","TGTYPESCREENREG");

        TYPE.$("ELE_LOGIN_PASSWORD","//input[@type='password'][@name='outerForm:password']","123456a","TRUE","TGTYPESCREENREG");

        TAP.$("ELE_LOGIN_LOGINBUTTON","//input[@type='submit'][@name='outerForm:loginButton']","TGTYPESCREENREG");

        CALL.$("CreateDistChannel","TGTYPESCREENREG");

        HOVER.$("ELE_CREATEDISTCHANNEL_AGENCYNAV","//td[text()='Agency']","TGTYPESCREENREG");

        TAP.$("ELE_CREATEDISTCHANNEL_DISTCHANNELNAV","//td[text()='Distribution Channel']","TGTYPESCREENREG");

        TAP.$("ELE_CREATEDISTCHANNEL_ADDBUTTON","//input[@type='submit'][@name='outerForm:Add']","TGTYPESCREENREG");

        TYPE.$("ELE_CREATEDISTCHANNEL_DISTCHNAME","//input[@type='text'][@name='outerForm:distributionChanName']","DistChannelTest01","TRUE","TGTYPESCREENREG");

        TYPE.$("ELE_CREATEDISTCHANNEL_DESCRIPTION","//input[@type='text'][@name='outerForm:channelDescription']","DistChDescTest01","TRUE","TGTYPESCREENREG");

        TYPE.$("ELE_CREATEDISTCHANNEL_DISTCHCODE","//input[@type='text'][@name='outerForm:duf_dist_code']","DistChTst01","TRUE","TGTYPESCREENREG");

        TAP.$("ELE_CREATEDISTCHANNEL_SUBMITBUTTON","//input[@type='submit'][@name='outerForm:Submit']","TGTYPESCREENREG");


    }


    @Test
    public void t04benefitpayment() {

        // No need to declare / print method name here, handled by TestNg listener
        // declare("t04benefitpayment");

        CALL.$("loginGIAS","TGTYPESCREENREG");

        TYPE.$("ELE_LOGIN_USERNAME","//input[@type='text'][@name='outerForm:username']","Eagleton","TRUE","TGTYPESCREENREG");

        TYPE.$("ELE_LOGIN_PASSWORD","//input[@type='password'][@name='outerForm:password']","123456a","TRUE","TGTYPESCREENREG");

        TAP.$("ELE_LOGIN_LOGINBUTTON","//input[@type='submit'][@name='outerForm:loginButton']","TGTYPESCREENREG");

        CALL.$("BenefitPayment","TGTYPESCREENREG");

        HOVER.$("ELE_BENEFITPAYMENT_PAYMENTNAV","//td[text()='Payments']","TGTYPESCREENREG");

        TAP.$("ELE_BENEFITPAYMENT_BENEFITNAV","//td[text()='Benefit']","TGTYPESCREENREG");

        TYPE.$("ELE_BENEFITPAYMENT_POLICYNUM","//input[@type='text'][@name='outerForm:policyNumber']","04022019T","TRUE","TGTYPESCREENREG");

        TAP.$("ELE_BENEFITPAYMENT_CONTINUEBUTTON","//input[@type='submit'][@name='outerForm:Continue']","TGTYPESCREENREG");

        TAP.$("ELE_BENEFITPAYMENT_BASEBUTTON","//span[text()='Base']","TGTYPESCREENREG");

        TYPE.$("ELE_BENEFITPAYMENT_ADDAMT","//input[@type='text'][@name='outerForm:amountToApply']","50","TRUE","TGTYPESCREENREG");

        TAP.$("ELE_BENEFITPAYMENT_SUBMITBUTTON","//input[@type='submit'][@name='outerForm:Submit']","TGTYPESCREENREG");


    }


    @Test
    public void t05createpolicy() {

        // No need to declare / print method name here, handled by TestNg listener
        // declare("t05createpolicy");

        CALL.$("loginGIAS","TGTYPESCREENREG");

        TYPE.$("ELE_LOGIN_USERNAME","//input[@type='text'][@name='outerForm:username']","Eagleton","TRUE","TGTYPESCREENREG");

        TYPE.$("ELE_LOGIN_PASSWORD","//input[@type='password'][@name='outerForm:password']","123456a","TRUE","TGTYPESCREENREG");

        TAP.$("ELE_LOGIN_LOGINBUTTON","//input[@type='submit'][@name='outerForm:loginButton']","TGTYPESCREENREG");

        CALL.$("CreatePolicy","TGTYPESCREENREG");

        HOVER.$("ELE_CREATEPOLICY_NEWBIZNAV","//td[text()='New Business']","TGTYPESCREENREG");

        TAP.$("ELE_CREATEPOLICY_APPENTRYNAV","//td[text()='Application Entry/Update']","TGTYPESCREENREG");

        TAP.$("ELE_CREATEPOLICY_ADDBUTTON","//input[@type='submit'][@name='outerForm:Add']	","TGTYPESCREENREG");

        TAP.$("ELE_CREATEPOLICY_SELECTCLIENTBUTTON","//span[text()='Select Client']","TGTYPESCREENREG");

        TYPE.$("ELE_CREATEPOLICY_SEARCHCLIENT","//input[@type='text'][@name='outerForm:grid:individualName']","Smith","TRUE","TGTYPESCREENREG");

        TAP.$("ELE_CREATEPOLICY_FINDPOSITIONBUTTON","//img[@id='outerForm:gnFindImage']","TGTYPESCREENREG");

        SELECT.$("ELE_CREATEPOLICY_SELECTRELATIONSHIPDROPDOWN","//select[@name='outerForm:grid:2:relationship']","Owner 1","TGTYPESCREENREG");

        TAP.$("ELE_CREATEPOLICY_SUBMITBUTTONFOROWNER","//input[@type='submit'][@name='outerForm:Submit']","TGTYPESCREENREG");

        TYPE.$("ELE_CREATEPOLICY_POLICYNUMBER","//input[@type='text'][@name='outerForm:policyNumber']","TEST801","TRUE","TGTYPESCREENREG");

        TYPE.$("ELE_CREATEPOLICY_CASHWITHAPP","//input[@type='text'][@name='outerForm:cashWithApplication']","500","000","TRUE","TGTYPESCREENREG");

        TYPE.$("ELE_CREATEPOLICY_AGENTNUMBER","//input[@type='text'][@name='outerForm:grid:0:agentNumberOut2']","12000","TRUE","TGTYPESCREENREG");

        TAP.$("ELE_CREATEPOLICY_SUBMITBUTTON","//input[@type='submit'][@name='outerForm:Submit']","TGTYPESCREENREG");


    }


    @Test
    public void t06addevent() {

        // No need to declare / print method name here, handled by TestNg listener
        // declare("t06addevent");

        CALL.$("loginGIAS","TGTYPESCREENREG");

        TYPE.$("ELE_LOGIN_USERNAME","//input[@type='text'][@name='outerForm:username']","Eagleton","TRUE","TGTYPESCREENREG");

        TYPE.$("ELE_LOGIN_PASSWORD","//input[@type='password'][@name='outerForm:password']","123456a","TRUE","TGTYPESCREENREG");

        TAP.$("ELE_LOGIN_LOGINBUTTON","//input[@type='submit'][@name='outerForm:loginButton']","TGTYPESCREENREG");

        CALL.$("AddEvent","TGTYPESCREENREG");

        HOVER.$("ELE_ADDEVENT_COMPANYPROPERTIESNAV","//td[text()='Company Properties']","TGTYPESCREENREG");

        HOVER.$("ELE_ADDEVENT_NONTRADITIONALNAV","//td[text()='Nontraditional']","TGTYPESCREENREG");

        TAP.$("ELE_ADDEVENT_EVENTDESCNAV","//td[text()='Event Descriptions']","TGTYPESCREENREG");

        TAP.$("ELE_ADDEVENT_ADDBUTTON","//input[@type='submit'][@name='outerForm:Add']","TGTYPESCREENREG");

        TYPE.$("ELE_ADDEVENT_TYPE","//input[@type='text'][@name='outerForm:eventType']","T01","TRUE","TGTYPESCREENREG");

        TYPE.$("ELE_ADDEVENT_DESCRIPTION","//input[@type='text'][@name='outerForm:eventDescription']","Test","TRUE","TGTYPESCREENREG");

        TAP.$("ELE_ADDEVENT_CONFIRMATION","//input[@type='radio'][@name='outerForm:writeConfrimationRcd']","TGTYPESCREENREG");

        TAP.$("ELE_ADDEVENT_SUBMITBUTTON","//input[@type='submit'][@name='outerForm:Submit']","TGTYPESCREENREG");


    }


    @Test
    public void t07addcommissioncontract() {

        // No need to declare / print method name here, handled by TestNg listener
        // declare("t07addcommissioncontract");

        CALL.$("loginGIAS","TGTYPESCREENREG");

        TYPE.$("ELE_LOGIN_USERNAME","//input[@type='text'][@name='outerForm:username']","Eagleton","TRUE","TGTYPESCREENREG");

        TYPE.$("ELE_LOGIN_PASSWORD","//input[@type='password'][@name='outerForm:password']","123456a","TRUE","TGTYPESCREENREG");

        TAP.$("ELE_LOGIN_LOGINBUTTON","//input[@type='submit'][@name='outerForm:loginButton']","TGTYPESCREENREG");

        CALL.$("AddCommissionContract","TGTYPESCREENREG");

        HOVER.$("ELE_ADDCOMMISSIONCONTRACT_AGENCYNAV","//td[text()='Agency']","TGTYPESCREENREG");

        TAP.$("ELE_ADDCOMMISSIONCONTRACT_COMCONNAV","//td[text()='Commission Contracts']","TGTYPESCREENREG");

        TAP.$("ELE_ADDCOMMISSIONCONTRACT_ADDBUTTON","//input[@type='submit'][@name='outerForm:Add']","TGTYPESCREENREG");

        TYPE.$("ELE_ADDCOMMISSIONCONTRACT_CONTRACTNUM","//input[@type='text'][@name='outerForm:commissionContNumb']","800","TRUE","TGTYPESCREENREG");

        TYPE.$("ELE_ADDCOMMISSIONCONTRACT_DESCRIPTION","//input[@type='text'][@name='outerForm:longDescription']","Test Contract","TRUE","TGTYPESCREENREG");

        SELECT.$("ELE_ADDCOMMISSIONCONTRACT_DISTCHDROPDOWN","//select[@name='outerForm:distributionChannel']","DistChDescTest01","TGTYPESCREENREG");

        TYPE.$("ELE_ADDCOMMISSIONCONTRACT_EFFECTIVEDATE","//input[@type='text'][@name='outerForm:effectiveDate']","04/26/2019","TRUE","TGTYPESCREENREG");

        TAP.$("ELE_ADDCOMMISSIONCONTRACT_SUBMITBUTTON","//input[@type='submit'][@name='outerForm:Submit']","TGTYPESCREENREG");


    }


    @Test
    public void t08addbenefit() {

        // No need to declare / print method name here, handled by TestNg listener
        // declare("t08addbenefit");

        CALL.$("loginGIAS","TGTYPESCREENREG");

        TYPE.$("ELE_LOGIN_USERNAME","//input[@type='text'][@name='outerForm:username']","Eagleton","TRUE","TGTYPESCREENREG");

        TYPE.$("ELE_LOGIN_PASSWORD","//input[@type='password'][@name='outerForm:password']","123456a","TRUE","TGTYPESCREENREG");

        TAP.$("ELE_LOGIN_LOGINBUTTON","//input[@type='submit'][@name='outerForm:loginButton']","TGTYPESCREENREG");

        CALL.$("AddBenefit","TGTYPESCREENREG");

        HOVER.$("ELE_ADDBENEFIT_POLICYOWNERSERVNAV","//td[text()='Policyowner Services']","TGTYPESCREENREG");

        HOVER.$("ELE_ADDBENEFIT_BENEFITMAINTENANCENAV","//td[text()='Benefit Maintenance']","TGTYPESCREENREG");

        TAP.$("ELE_ADDBENEFIT_ADDBENEFITNAV","//td[text()='Add Benefit']","TGTYPESCREENREG");

        TYPE.$("ELE_ADDBENEFIT_ENTERPOLICYNUM","//input[@type='text'][@name='outerForm:policyNumber']","04022019T","TRUE","TGTYPESCREENREG");

        TAP.$("ELE_ADDBENEFIT_CONTINUEBUTTON","//input[@type='submit'][@name='outerForm:Continue']","TGTYPESCREENREG");

        TAP.$("ELE_ADDBENEFIT_BENEFITTYPE","//input[@type='radio'][@name='outerForm:testField']","TGTYPESCREENREG");

        SELECT.$("ELE_ADDBENEFIT_PLANTYPE","//select[@name='outerForm:testPlanCode']","TEST - test","TGTYPESCREENREG");

        TAP.$("ELE_ADDBENEFIT_CONTINUEBUTTON2","//input[@type='submit'][@name='outerForm:Continue']","TGTYPESCREENREG");

        TAP.$("ELE_ADDBENEFIT_SUBMITBUTTON","//input[@type='submit'][@name='outerForm:Submit']","TGTYPESCREENREG");


    }


    @Test
    public void t09addfreelookschedule() {

        // No need to declare / print method name here, handled by TestNg listener
        // declare("t09addfreelookschedule");

        CALL.$("loginGIAS","TGTYPESCREENREG");

        TYPE.$("ELE_LOGIN_USERNAME","//input[@type='text'][@name='outerForm:username']","Eagleton","TRUE","TGTYPESCREENREG");

        TYPE.$("ELE_LOGIN_PASSWORD","//input[@type='password'][@name='outerForm:password']","123456a","TRUE","TGTYPESCREENREG");

        TAP.$("ELE_LOGIN_LOGINBUTTON","//input[@type='submit'][@name='outerForm:loginButton']","TGTYPESCREENREG");

        CALL.$("AddFreeLookSched","TGTYPESCREENREG");

        HOVER.$("ELE_ADDFREELOOKSCHEDULE_PLANMAINTENANCENAV","//td[text()='Plan Maintenance']","TGTYPESCREENREG");

        HOVER.$("ELE_ADDFREELOOKSCHEDULE_NONTRADITIONALNAV","//td[text()='Nontraditional']","TGTYPESCREENREG");

        TAP.$("ELE_ADDFREELOOKSCHEDULE_FREELOOKSCHEDNAV","//td[text()='Free Look Provision Schedules']","TGTYPESCREENREG");

        TAP.$("ELE_ADDFREELOOKSCHEDULE_ADDBUTTON","//input[@type='submit'][@name='outerForm:Add']","TGTYPESCREENREG");

        TYPE.$("ELE_ADDFREELOOKSCHEDULE_TABLE","//input[@type='text'][@name='outerForm:freeLookProvSchTbl']","TEST01","TRUE","TGTYPESCREENREG");

        SELECT.$("ELE_ADDFREELOOKSCHEDULE_STATECOUNTRY","//select[@name='outerForm:countryAndStateCode']","All Countries and States","TGTYPESCREENREG");

        TYPE.$("ELE_ADDFREELOOKSCHEDULE_ENDINGAGE","//input[@type='text'][@name='outerForm:endingAge']","0","TRUE","TGTYPESCREENREG");

        SELECT.$("ELE_ADDFREELOOKSCHEDULE_TYPE","//select[@name='outerForm:freeLookType']","Refund Fund Balance","TGTYPESCREENREG");

        TYPE.$("ELE_ADDFREELOOKSCHEDULE_NUMOFDAYS","//input[@type='text'][@name='outerForm:freeLookNumberOfDays']","0","TRUE","TGTYPESCREENREG");

        SELECT.$("ELE_ADDFREELOOKSCHEDULE_ALLOCATIONRULE","//select[@name='outerForm:initialAllocRule']","Allocate Premium Immediately","TGTYPESCREENREG");


    }


    @Test
    public void t10addagentrank() {

        // No need to declare / print method name here, handled by TestNg listener
        // declare("t10addagentrank");

        CALL.$("loginGIAS","TGTYPESCREENREG");

        TYPE.$("ELE_LOGIN_USERNAME","//input[@type='text'][@name='outerForm:username']","Eagleton","TRUE","TGTYPESCREENREG");

        TYPE.$("ELE_LOGIN_PASSWORD","//input[@type='password'][@name='outerForm:password']","123456a","TRUE","TGTYPESCREENREG");

        TAP.$("ELE_LOGIN_LOGINBUTTON","//input[@type='submit'][@name='outerForm:loginButton']","TGTYPESCREENREG");

        CALL.$("AddAgentRank","TGTYPESCREENREG");

        HOVER.$("ELE_ADDAGENTRANKCODE_AGENTNAV","//td[text()='Agency']","TGTYPESCREENREG");

        TAP.$("ELE_ADDAGENTRANKCODE_AGENTRANKNAV","//td[text()='Agent Rank Codes']","TGTYPESCREENREG");

        TAP.$("ELE_ADDAGENTRANKCODE_ADDBUTTON","//input[@type='submit'][@name='outerForm:Add']","TGTYPESCREENREG");

        SELECT.$("ELE_ADDAGENTRANKCODE_DISTCH","//select[@name='outerForm:distributionChannel']","TESTGIASAUTO","TGTYPESCREENREG");

        TYPE.$("ELE_ADDAGENTRANKCODE_RANKLEVEL","//input[@type='text'][@name='outerForm:agencyRankLevel']","1","TRUE","TGTYPESCREENREG");

        TYPE.$("ELE_ADDAGENTRANK_RANKCODE","//input[@type='text'][@name='outerForm:agencyRankCode']","800","TRUE","TGTYPESCREENREG");

        TYPE.$("ELE_ADDAGENTRANK_DESCRIPTION","//input[@type='text'][@name='outerForm:description']","TestRank","TRUE","TGTYPESCREENREG");

        TAP.$("ELE_ADDAGENTRANK_SUBMITBUTTON","//input[@type='submit'][@name='outerForm:Submit']","TGTYPESCREENREG");


    }


    @Test
    public void t11addnumandmaskrules() {

        // No need to declare / print method name here, handled by TestNg listener
        // declare("t11addnumandmaskrules");

        CALL.$("loginGIAS","TGTYPESCREENREG");

        TYPE.$("ELE_LOGIN_USERNAME","//input[@type='text'][@name='outerForm:username']","Eagleton","TRUE","TGTYPESCREENREG");

        TYPE.$("ELE_LOGIN_PASSWORD","//input[@type='password'][@name='outerForm:password']","123456a","TRUE","TGTYPESCREENREG");

        TAP.$("ELE_LOGIN_LOGINBUTTON","//input[@type='submit'][@name='outerForm:loginButton']","TGTYPESCREENREG");

        CALL.$("AddNumandMaskingRule","TGTYPESCREENREG");

        HOVER.$("ELE_ADDNUMANDMASKRULES_COMPANYPROPERTIESNAV","//td[text()='Company Properties']","TGTYPESCREENREG");

        TAP.$("ELE_ADDNUMANDMASKINGRULES_NUMANDMASKNAV","//td[text()='Number And Masking Rules']","TGTYPESCREENREG");

        TAP.$("ELE_ADDNUMANDMASKINGRULES_ADDBUTTON","//input[@type='submit'][@name='outerForm:Add']","TGTYPESCREENREG");

        TYPE.$("ELE_ADDNUMANDMASKINGRULES_NUMTYPE","//input[@type='text'][@name='outerForm:controlType']","TEST","TRUE","TGTYPESCREENREG");

        TYPE.$("ELE_ADDNUMANDMASKINGRULES_DESC","//input[@type='text'][@name='outerForm:controlDescription']","TEST RULE","TRUE","TGTYPESCREENREG");

        TAP.$("ELE_ADDNUMANDMASKINGRULES_AUTOASSIGN","//input[@type='radio'][@name='outerForm:assignNext']","TGTYPESCREENREG");

        TYPE.$("ELE_ADDNUMANDMASKINGRULES_MASK","//input[@type='text'][@name='outerForm:maskingFormat']","XXXX","TRUE","TGTYPESCREENREG");

        TYPE.$("ELE_ADDNUMANDMASKINGRULES_LASTVALUE","//input[@type='text'][@name='outerForm:lastValueAssigned']","1111","TRUE","TGTYPESCREENREG");


    }


    @Test
    public void t12updateclientaddress() {

        // No need to declare / print method name here, handled by TestNg listener
        // declare("t12updateclientaddress");

        CALL.$("loginGIAS","TGTYPESCREENREG");

        TYPE.$("ELE_LOGIN_USERNAME","//input[@type='text'][@name='outerForm:username']","Eagleton","TRUE","TGTYPESCREENREG");

        TYPE.$("ELE_LOGIN_PASSWORD","//input[@type='password'][@name='outerForm:password']","123456a","TRUE","TGTYPESCREENREG");

        TAP.$("ELE_LOGIN_LOGINBUTTON","//input[@type='submit'][@name='outerForm:loginButton']","TGTYPESCREENREG");

        CALL.$("UpdateClientAddress","TGTYPESCREENREG");

        HOVER.$("ELE_UPDATECLIENTADDRESS_CLIENTNAV","//td[text()='Client']","TGTYPESCREENREG");

        TAP.$("ELE_UPDATECLIENTADDRESS_NAMESADDRESSESNAV","//td[text()='Names/Addresses']","TGTYPESCREENREG");

        TYPE.$("ELE_UPDATECLIENTADDRESS_SEARCHNAME","//input[@type='text'][@name='outerForm:grid:individualName']","Smith","TRUE","TGTYPESCREENREG");

        TAP.$("ELE_UPDATECLIENTADDRESS_FINDPOSITIONTO","//img[@id='outerForm:gnFindImage']","TGTYPESCREENREG");

        TAP.$("ELE_UPDATECLIENTADDRESS_SELECTNAME","//span[text()='Smith, Donna M']","TGTYPESCREENREG");

        TAP.$("ELE_UPDATECLIENTADDRESS_ADDRESSINFOBUTTON","//span[text()='Address Information']","TGTYPESCREENREG");

        TAP.$("ELE_UPDATECLIENTADDRESS_ADDBUTTON","//input[@type='submit'][@name='outerForm:Add']","TGTYPESCREENREG");

        TYPE.$("ELE_UPDATECLIENTADDRESS_ADDRESSLINE1","//input[@type='text'][@name='outerForm:addressLineOne']","10 Cherry Lane","TRUE","TGTYPESCREENREG");

        TYPE.$("ELE_UPDATECLIENTADDRESS_CITY","//input[@type='text'][@name='outerForm:addressCity']","Queensbury","TRUE","TGTYPESCREENREG");

        SELECT.$("ELE_UPDATECLIENTADDRESS_STATECOUNTRY","//select[@name='outerForm:countryAndStateCode']","New York US","TGTYPESCREENREG");

        TYPE.$("ELE_UPDATECLIENTADDRESS_ZIPCODE","//input[@type='text'][@name='outerForm:zipCode']","12804","TRUE","TGTYPESCREENREG");

        TAP.$("ELE_UPDATECLIENTADDRESS_SUBMITBUTTON","//input[@type='submit'][@name='outerForm:Submit']","TGTYPESCREENREG");


    }


    @Test
    public void t13addagent() {

        // No need to declare / print method name here, handled by TestNg listener
        // declare("t13addagent");

        CALL.$("loginGIAS","TGTYPESCREENREG");

        TYPE.$("ELE_LOGIN_USERNAME","//input[@type='text'][@name='outerForm:username']","Eagleton","TRUE","TGTYPESCREENREG");

        TYPE.$("ELE_LOGIN_PASSWORD","//input[@type='password'][@name='outerForm:password']","123456a","TRUE","TGTYPESCREENREG");

        TAP.$("ELE_LOGIN_LOGINBUTTON","//input[@type='submit'][@name='outerForm:loginButton']","TGTYPESCREENREG");

        CALL.$("AddAgent","TGTYPESCREENREG");

        HOVER.$("ELE_ADDAGENT_AGENCYNAV","//td[text()='Agency']","TGTYPESCREENREG");

        TAP.$("ELE_ADDAGENT_AGENTNAV","//td[text()='Agents']	","TGTYPESCREENREG");

        TAP.$("ELE_ADDAGENT_ADDBUTTON","//input[@type='submit'][@name='outerForm:Add']","TGTYPESCREENREG");

        TAP.$("ELE_ADDAGENT_SELECTCLIENT","//span[text()='Select Client']","TGTYPESCREENREG");

        TYPE.$("ELE_ADDAGENT_SEARCHNAME","//input[@type='text'][@name='outerForm:grid:individualName']","Smith","TRUE","TGTYPESCREENREG");

        TAP.$("ELE_ADDAGENT_FINDPOSITIONTO","//img[@id='outerForm:gnFindImage']","TGTYPESCREENREG");

        SELECT.$("ELE_ADDAGENT_CLIENTDROPDOWN","//select[@name='outerForm:grid:8:relationship']","Agent","TGTYPESCREENREG");

        TAP.$("ELE_ADDAGENT_SUBMITBUTTON1","//input[@type='submit'][@name='outerForm:Submit']","TGTYPESCREENREG");

        TYPE.$("ELE_ADDAGENT_AGENTNUMBER","//input[@type='text'][@name='outerForm:agentNumber']","8001","TRUE","TGTYPESCREENREG");

        SELECT.$("ELE_ADDAGENT_AGENTSTATUS","//select[@name='outerForm:agentStatus']","Active","TGTYPESCREENREG");

        TYPE.$("ELE_ADDAGENT_HIREDATE","//input[@type='text'][@name='outerForm:hireDate']","04/30/2019","TRUE","TGTYPESCREENREG");

        SELECT.$("ELE_ADDAGENT_VESTED","//select[@name='outerForm:commissionVestCode']","First Year","TGTYPESCREENREG");

        SELECT.$("ELE_ADDAGENT_STATECOUNTRY","//select[@name='outerForm:countryAndStateCode']","New York US","TGTYPESCREENREG");

        SELECT.$("ELE_ADDAGENT_DISTCH","//select[@name='outerForm:distributionChannel']","TESTGIASAUTO","TGTYPESCREENREG");

        SELECT.$("ELE_ADDAGENT_RANKCODE","//select[@name='outerForm:agencyRankCode']","Writing Agent","TGTYPESCREENREG");

        TAP.$("ELE_ADDAGENT_SUBMITBUTTON2","//input[@type='submit'][@name='outerForm:Submit']","TGTYPESCREENREG");


    }


    @Test
    public void t14policydetailinquiry() {

        // No need to declare / print method name here, handled by TestNg listener
        // declare("t14policydetailinquiry");

        CALL.$("loginGIAS","TGTYPESCREENREG");

        TYPE.$("ELE_LOGIN_USERNAME","//input[@type='text'][@name='outerForm:username']","Eagleton","TRUE","TGTYPESCREENREG");

        TYPE.$("ELE_LOGIN_PASSWORD","//input[@type='password'][@name='outerForm:password']","123456a","TRUE","TGTYPESCREENREG");

        TAP.$("ELE_LOGIN_LOGINBUTTON","//input[@type='submit'][@name='outerForm:loginButton']","TGTYPESCREENREG");

        CALL.$("PolicyDetailInquiry","TGTYPESCREENREG");

        HOVER.$("ELE_POLICYDETAILINQUIRY_INQURIYNAV","//td[text()='Inquiry']","TGTYPESCREENREG");

        TAP.$("ELE_POLICYDETAILINQUIRY_POLICYDETAIL","//td[text()='Policy Detail']","TGTYPESCREENREG");

        TYPE.$("ELE_POLICYDETAILINQUIRY_POLICYNUM","//input[@type='text'][@name='outerForm:policyNumber']","04022019T","TRUE","TGTYPESCREENREG");

        TAP.$("ELE_POLICYDETAILINQUIRY_CONTINUEBUTTON","//input[@type='submit'][@name='outerForm:Continue']","TGTYPESCREENREG");


    }


    @Test
    public void t15nfo() {

        // No need to declare / print method name here, handled by TestNg listener
        // declare("t15nfo");

        CALL.$("loginGIAS","TGTYPESCREENREG");

        TYPE.$("ELE_LOGIN_USERNAME","//input[@type='text'][@name='outerForm:username']","Eagleton","TRUE","TGTYPESCREENREG");

        TYPE.$("ELE_LOGIN_PASSWORD","//input[@type='password'][@name='outerForm:password']","123456a","TRUE","TGTYPESCREENREG");

        TAP.$("ELE_LOGIN_LOGINBUTTON","//input[@type='submit'][@name='outerForm:loginButton']","TGTYPESCREENREG");

		int var_Count;
                 LOGGER.info("Executed Step = VAR,Integer,var_Count,TGTYPESCREENREG");
		var_Count = 0;
                 LOGGER.info("Executed Step = STORE,var_Count,0,TGTYPESCREENREG");
        LOGGER.info("Executed Step = REPEAT IF");
    	while (check(var_Count,"<",1,"TGTYPESCREENREG")) {
          LOGGER.info("Executed Step = WHILE,var_Count,<,1,TGTYPESCREENREG");
        CALL.$("NFO","TGTYPESCREENREG");

		com.jayway.jsonpath.ReadContext var_CSVData = getJsonData("C:/GIASAutomation/3.6 BaseRegression/3.6 BaseRegression/InputData/3_6_nonforfeiture.csv");
                 LOGGER.info("Executed Step = VAR,Json,var_CSVData,C:/GIASAutomation/3.6 BaseRegression/3.6 BaseRegression/InputData/3_6_nonforfeiture.csv,TGTYPESCREENREG");
		String var_PolicyNumber;
                 LOGGER.info("Executed Step = VAR,String,var_PolicyNumber,TGTYPESCREENREG");
		var_PolicyNumber = getJsonData(var_CSVData , "$.records["+var_Count+"].PolicyNumber");
                 LOGGER.info("Executed Step = STORE,var_PolicyNumber,var_CSVData,$.records[{var_Count}].PolicyNumber,TGTYPESCREENREG");
		String var_ApplyDate;
                 LOGGER.info("Executed Step = VAR,String,var_ApplyDate,TGTYPESCREENREG");
		var_ApplyDate = getJsonData(var_CSVData , "$.records["+var_Count+"].ApplyDate");
                 LOGGER.info("Executed Step = STORE,var_ApplyDate,var_CSVData,$.records[{var_Count}].ApplyDate,TGTYPESCREENREG");
		String var_NFO;
                 LOGGER.info("Executed Step = VAR,String,var_NFO,TGTYPESCREENREG");
		var_NFO = getJsonData(var_CSVData , "$.records["+var_Count+"].NFO");
                 LOGGER.info("Executed Step = STORE,var_NFO,var_CSVData,$.records[{var_Count}].NFO,TGTYPESCREENREG");
        HOVER.$("ELE_NFO_BILLINGNAV","//td[text()='Billing']","TGTYPESCREENREG");

        TAP.$("ELE_NFO_POLICYCONTRACTSNAV","//td[text()='Policy Contracts']","TGTYPESCREENREG");

        TYPE.$("ELE_NFO_ENTERPOLICYNUM","//input[@type='text'][@name='outerForm:policyNumber']",var_PolicyNumber,"FALSE","TGTYPESCREENREG");

        TYPE.$("ELE_NFO_APPLYDATE","//input[@type='text'][@name='outerForm:promptDate']",var_ApplyDate,"FALSE","TGTYPESCREENREG");

        TAP.$("ELE_NFO_CONTINUEBUTTON","//input[@type='submit'][@name='outerForm:Continue']","TGTYPESCREENREG");

        SELECT.$("ELE_NFO_NFOOPTION","//select[@name='outerForm:nonForfeitureOption']",var_NFO,"TGTYPESCREENREG");

        TAP.$("ELE_NFO_SUBMITBUTTON","//input[@type='submit'][@name='outerForm:Submit']","TGTYPESCREENREG");

		var_Count = var_Count + ( 1 );
                 LOGGER.info("Executed Step = INCREASEVAR,var_Count,1,TGTYPESCREENREG");
    	}
          LOGGER.info("Executed Step = WEND,TGTYPESCREENREG");
    	resetAttemps();

    }


}
