package appium.tgo;

import com.tg.octopus.base.TGOWrapper;
import com.tg.octopus.libs.testng.listener.TestListener;
import static com.tg.octopus.base.TGOWrapper.Action.*;
import static com.tg.octopus.base.TGOWrapper.LOGGER;
import com.tg.octopus.wrapper.ActionWrapper;

import com.jayway.jsonpath.JsonPath;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.SessionNotCreatedException;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.Augmenter;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.remote.UnreachableBrowserException;

import org.testng.Assert;
import org.testng.ITestContext;
import org.testng.ITestResult;
import org.testng.Reporter;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;
import java.net.HttpURLConnection;
import java.net.URL;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import org.json.JSONArray;
import org.json.JSONObject;
import helper.WebTestUtility;
import java.sql.*;
import com.testautomationguru.utility.PDFUtil;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.multipdf.Splitter;

import java.util.*;
import org.openqa.selenium.*;
import java.io.*;
import java.net.*;

import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;


@Listeners(TestListener.class)
public class Test_Chris extends TGOWrapper {



    static {
       needsresetoncebeforetest ="false";
       needsresetbeforeeverymethod ="true";
       captureverystepsnap ="true";
       appendlastsnap ="false";
       ishybridapp ="false";

    }


    @Test
    public void logingias() {

        // No need to declare / print method name here, handled by TestNg listener
        // declare("logingias");

        CALL.$("loginGIAS","TGTYPESCREENREG");

        TYPE.$("ELE_LOGINUSERNAME","//input[@type='text'][@name='outerForm:username']","Cloutier","TRUE","TGTYPESCREENREG");

        TYPE.$("ELE_LOGINPASSWORD","//input[@type='password'][@name='outerForm:password']","123456a","TRUE","TGTYPESCREENREG");

        TAP.$("ELE_LOGINBUTTON_SUBMIT","//input[@type='submit'][@name='outerForm:loginButton']","TGTYPESCREENREG");


    }


    @Test
    public void createclient() {

        // No need to declare / print method name here, handled by TestNg listener
        // declare("createclient");

        CALL.$("loginGIAS","TGTYPESCREENREG");

        TYPE.$("ELE_LOGINUSERNAME","//input[@type='text'][@name='outerForm:username']","Cloutier","TRUE","TGTYPESCREENREG");

        TYPE.$("ELE_LOGINPASSWORD","//input[@type='password'][@name='outerForm:password']","123456a","TRUE","TGTYPESCREENREG");

        TAP.$("ELE_LOGINBUTTON_SUBMIT","//input[@type='submit'][@name='outerForm:loginButton']","TGTYPESCREENREG");

        CALL.$("CreateClient","TGTYPESCREENREG");

        HOVER.$("ELE_ADDCLIENT_NAV","//td[text()='Client']","TGTYPESCREENREG");

        TAP.$("ELE_ADDCLIENT_NAMESADDRESSNAV","//td[text()='Names/Addresses']	","TGTYPESCREENREG");

        TAP.$("ELE_CREATECLIENT_ADD","//input[@type='submit'][@name='outerForm:Add']","TGTYPESCREENREG");

        TYPE.$("ELE_CREATECLIENT_FIRSTNAME","//input[@type='text'][@name='outerForm:nameFirst']","Christian","TRUE","TGTYPESCREENREG");

        TYPE.$("ELE_CREATECLIENT_LASTNAME","//input[@type='text'][@name='outerForm:nameLast']","Thompson-McGrath","TRUE","TGTYPESCREENREG");

        SELECT.$("ELE_CREATECLIENT_STATE","//select[@name='outerForm:birthCountryAndState2']","New Jersey US","TGTYPESCREENREG");

        TYPE.$("ELE_CREATECLIENT_DATEOFBIRTH","//input[@type='text'][@name='outerForm:dateOfBirth2']","12/06/1994","TRUE","TGTYPESCREENREG");

        SELECT.$("ELE_CREATECLIENT_GENDER","//select[@name='outerForm:sexCode2']","Male","TGTYPESCREENREG");

        SELECT.$("ELE_CREATECLIENT_MARSTATUS","//select[@name='outerForm:maritalStatus2']","Married","TGTYPESCREENREG");

        SELECT.$("ELE_CREATECLIENT_ID","//select[@name='outerForm:taxIdenUsag2']","Social Security Number","TGTYPESCREENREG");

        TYPE.$("ELE_CREATECLIENT_IDNUMBER","//input[@type='text'][@name='outerForm:identificationNumber2']","874 28 1946","TRUE","TGTYPESCREENREG");

        TAP.$("ELE_CREATECLIENT_FIRSTSUBMITBUTTON","//input[@type='submit'][@name='outerForm:Submit']","TGTYPESCREENREG");

        TYPE.$("ELE_CREATECLIENT_ADDRESSLINE","//input[@type='text'][@name='outerForm:addressLineOne']","319 2nd Ave","TRUE","TGTYPESCREENREG");

        TYPE.$("ELE_CREATECLIENT_ADDRESSCITY","//input[@type='text'][@name='outerForm:addressCity']","Shrewsbury","TRUE","TGTYPESCREENREG");

        SELECT.$("ELE_CREATECLIENT_ADDRESSSTATECITY","//select[@name='outerForm:countryAndStateCode']","New Jersey US","TGTYPESCREENREG");

        TYPE.$("ELE_CREATECLIENT_ZIPCODE","//input[@type='text'][@name='outerForm:zipCode']","07702","TRUE","TGTYPESCREENREG");

        TAP.$("ELE_CREATECLIENT_FINALSUBMIT","//input[@type='submit'][@name='outerForm:Submit']","TGTYPESCREENREG");


    }


    @Test
    public void createpolicy() {

        // No need to declare / print method name here, handled by TestNg listener
        // declare("createpolicy");

        CALL.$("loginGIAS","TGTYPESCREENREG");

        TYPE.$("ELE_LOGINUSERNAME","//input[@type='text'][@name='outerForm:username']","Cloutier","TRUE","TGTYPESCREENREG");

        TYPE.$("ELE_LOGINPASSWORD","//input[@type='password'][@name='outerForm:password']","123456a","TRUE","TGTYPESCREENREG");

        TAP.$("ELE_LOGINBUTTON_SUBMIT","//input[@type='submit'][@name='outerForm:loginButton']","TGTYPESCREENREG");

        CALL.$("CreatePolicy","TGTYPESCREENREG");

        HOVER.$("ELE_CREATEPOLICY_ADDNEW","//td[text()='New Business']","TGTYPESCREENREG");

        TAP.$("ELE_CREATEPOLICY_ENTERNAV","//td[text()='Application Entry/Update']","TGTYPESCREENREG");

        TAP.$("ELE_CREATEPOLICY_ADDBUTTON","//input[@type='submit'][@name='outerForm:Add']","TGTYPESCREENREG");

        TAP.$("ELE_CREATEPOLICY_SELECTCLIENT","//span[text()='Select Client']","TGTYPESCREENREG");

        TYPE.$("ELE_CREATEPOLICY_SEARCHCLIENT","//input[@type='text'][@name='outerForm:grid:individualName']","Cloutier","TRUE","TGTYPESCREENREG");

        TAP.$("ELE_CREATEPOLICY_FIND","//img[@id='outerForm:gnFindImage']","TGTYPESCREENREG");

        SELECT.$("ELE_CREATEPOLICY_RELATIONSHIPDROPDOWN","//select[@name='outerForm:grid:2:relationship']","Owner 1","TGTYPESCREENREG");

        TAP.$("ELE_CREATEPOLICY_SUBMITOWNER","//input[@type='submit'][@name='outerForm:Submit']","TGTYPESCREENREG");

        TYPE.$("ELE_CREATEPOLICY_POLICYNUMBER","//input[@type='text'][@name='outerForm:policyNumber']","12061206","TRUE","TGTYPESCREENREG");

        TYPE.$("ELE_CREATEPOLICY_CASHAPP","//input[@type='text'][@name='outerForm:cashWithApplication']","1","000","000","TRUE","TGTYPESCREENREG");

        TAP.$("ELE_CREATEPOLICY_SUBMITFINAL","//input[@type='submit'][@name='outerForm:Submit']","TGTYPESCREENREG");


    }


    @Test
    public void createdistribchannel() {

        // No need to declare / print method name here, handled by TestNg listener
        // declare("createdistribchannel");

        CALL.$("loginGIAS","TGTYPESCREENREG");

        TYPE.$("ELE_LOGINUSERNAME","//input[@type='text'][@name='outerForm:username']","Cloutier","TRUE","TGTYPESCREENREG");

        TYPE.$("ELE_LOGINPASSWORD","//input[@type='password'][@name='outerForm:password']","123456a","TRUE","TGTYPESCREENREG");

        TAP.$("ELE_LOGINBUTTON_SUBMIT","//input[@type='submit'][@name='outerForm:loginButton']","TGTYPESCREENREG");

        CALL.$("CreateDistributionChannel","TGTYPESCREENREG");

        HOVER.$("ELE_CREATEDISTCHANNEL_AGENCYNAV","//td[text()='Agency']","TGTYPESCREENREG");

        TAP.$("ELE_CREATEDISTCHANNEL_DISTCHANNELSELECT","//td[text()='Distribution Channel']","TGTYPESCREENREG");

        TAP.$("ELE_CREATDISTCHANNEL_ADDBUTTON","//img[@id='outerForm:gnAddImage']","TGTYPESCREENREG");

        TYPE.$("ELE_CREATEDISTCHANNEL_DISTCHANNELNUMBER","//input[@type='text'][@name='outerForm:distributionChanName']","1738","TRUE","TGTYPESCREENREG");

        TYPE.$("ELE_CREATEDISTCHANNEL_DESCRIPTION","//input[@type='text'][@name='outerForm:channelDescription']","1738 Channel","TRUE","TGTYPESCREENREG");

        TYPE.$("ELE_CREATEDISTCHANNEL_DISTNUMBER2","//input[@type='text'][@name='outerForm:duf_dist_code']","1738","TRUE","TGTYPESCREENREG");

        TAP.$("ELE_CREATEDISTCHANNEL_SUBMIT","//input[@type='submit'][@name='outerForm:Submit']","TGTYPESCREENREG");


    }


    @Test
    public void benefitstatuschange() {

        // No need to declare / print method name here, handled by TestNg listener
        // declare("benefitstatuschange");

        CALL.$("loginGIAS","TGTYPESCREENREG");

        TYPE.$("ELE_LOGINUSERNAME","//input[@type='text'][@name='outerForm:username']","Cloutier","TRUE","TGTYPESCREENREG");

        TYPE.$("ELE_LOGINPASSWORD","//input[@type='password'][@name='outerForm:password']","123456a","TRUE","TGTYPESCREENREG");

        TAP.$("ELE_LOGINBUTTON_SUBMIT","//input[@type='submit'][@name='outerForm:loginButton']","TGTYPESCREENREG");

        CALL.$("BenefitStatusChange","TGTYPESCREENREG");

        HOVER.$("ELE_BENEFITSTATUSCHANGE_POLICYOWNERSERVNAV","//td[text()='Policyowner Services']","TGTYPESCREENREG");

        HOVER.$("ELE_BENEFITSTATUSCHANGE_BENEFITMAINT","//td[text()='Benefit Maintenance']","TGTYPESCREENREG");

        TAP.$("ELE_BENEFITSTATUSCHANGE_STATUSCHANGESELECT","//td[text()='Benefit Status Change']","TGTYPESCREENREG");

        TYPE.$("ELE_BENEFITSTATUSCHANGE_POLICYNUMBERTEXTBOX","//input[@type='text'][@name='outerForm:policyNumber']","DP0100000","TRUE","TGTYPESCREENREG");

        TAP.$("ELE_BENEFITSTATUSCHANGE_CONTINUE","//input[@type='submit'][@name='outerForm:Continue']","TGTYPESCREENREG");

        TAP.$("ELE_BENEFITSTATUSCHANGE_BASE","//span[text()='Base']","TGTYPESCREENREG");

        SELECT.$("ELE_BENEFITSTATUSCHANGE_NEWBENEFITSTATUS","//select[@name='outerForm:newBenefitStatus']","Cancelled","TGTYPESCREENREG");

        TYPE.$("ELE_BENEFITSTATUSCHANGE_TERMINATIONDATE","//input[@type='text'][@name='outerForm:terminationDate']","12/12/2020","TRUE","TGTYPESCREENREG");

        TAP.$("ELE_BENEFITSTATUSCHANGE_SUBMIT","//input[@type='submit'][@name='outerForm:Submit']","TGTYPESCREENREG");


    }


    @Test
    public void createagent() {

        // No need to declare / print method name here, handled by TestNg listener
        // declare("createagent");

        CALL.$("loginGIAS","TGTYPESCREENREG");

        TYPE.$("ELE_LOGINUSERNAME","//input[@type='text'][@name='outerForm:username']","Cloutier","TRUE","TGTYPESCREENREG");

        TYPE.$("ELE_LOGINPASSWORD","//input[@type='password'][@name='outerForm:password']","123456a","TRUE","TGTYPESCREENREG");

        TAP.$("ELE_LOGINBUTTON_SUBMIT","//input[@type='submit'][@name='outerForm:loginButton']","TGTYPESCREENREG");

        CALL.$("CreateAgent","TGTYPESCREENREG");

        HOVER.$("ELE_CREATEAGENT_AGENCYNAV","//td[text()='Agency']","TGTYPESCREENREG");

        TAP.$("ELE_CREATEAGENT_AGENTS","//td[text()='Agents']","TGTYPESCREENREG");

        TAP.$("ELE_CREATEAGENT_ADDBUTTON","//img[@id='outerForm:gnAddImage1']","TGTYPESCREENREG");

        TAP.$("ELE_CREATEAGENT_SELECTCLIENT","//span[text()='Select Client']","TGTYPESCREENREG");

        TYPE.$("ELE_CREATEAGENT_TEXTBOX","//input[@type='text'][@name='outerForm:grid:individualName']","Moody Hank","TRUE","TGTYPESCREENREG");

        TAP.$("ELE_CREATEAGENT_FINDBUTTON","//img[@id='outerForm:gnFindImage']","TGTYPESCREENREG");

        SELECT.$("ELE_CREATEAGENT_SELECTRELATIONSHIP","//select[@name='outerForm:grid:0:relationship']","Agent","TGTYPESCREENREG");

        SELECT.$("ELE_CREATEAGENT_AGENTRANKCODE","//select[@name='outerForm:agencyRankCode']","Writing Agent 9015","TGTYPESCREENREG");

        TAP.$("ELE_CREATEAGENT_SUBMIT","//input[@type='submit'][@name='outerForm:Submit']","TGTYPESCREENREG");

        TYPE.$("ELE_CREATEAGENT_AGENTNUMBER","//input[@type='text'][@name='outerForm:agentNumber']","1206","TRUE","TGTYPESCREENREG");

        SELECT.$("ELE_CREATEAGENT_AGENTSTATUS","//select[@name='outerForm:agentStatus']","Active","TGTYPESCREENREG");

        TYPE.$("ELE_CREATEAGENT_DATEHIRED","//input[@type='text'][@name='outerForm:hireDate']","04/29/2019","TRUE","TGTYPESCREENREG");

        SELECT.$("ELE_CREATEAGENT_DISTRIBUTIONCHANNEL","//select[@name='outerForm:distributionChannel']","BRAUTO9015Distribution Channel","TGTYPESCREENREG");

        TAP.$("ELE_CREATEAGENT_SUBMITFINAL","//input[@type='submit'][@name='outerForm:Submit']","TGTYPESCREENREG");


    }


    @Test
    public void freelook() {

        // No need to declare / print method name here, handled by TestNg listener
        // declare("freelook");

        CALL.$("loginGIAS","TGTYPESCREENREG");

        TYPE.$("ELE_LOGINUSERNAME","//input[@type='text'][@name='outerForm:username']","Cloutier","TRUE","TGTYPESCREENREG");

        TYPE.$("ELE_LOGINPASSWORD","//input[@type='password'][@name='outerForm:password']","123456a","TRUE","TGTYPESCREENREG");

        TAP.$("ELE_LOGINBUTTON_SUBMIT","//input[@type='submit'][@name='outerForm:loginButton']","TGTYPESCREENREG");

        CALL.$("FreeLook","TGTYPESCREENREG");

        HOVER.$("ELE_FREELOOK_PLANMAINTNAV","//td[text()='Plan Maintenance']","TGTYPESCREENREG");

        HOVER.$("ELE_FREELOOK_NONTRADNAV","//td[text()='Nontraditional']","TGTYPESCREENREG");

        TAP.$("ELE_FREELOOK_FREELOOKSCHEDSELECT","//td[text()='Free Look Provision Schedules']","TGTYPESCREENREG");

        TAP.$("ELE_FREELOOK_ADDBUTTON","//img[@id='outerForm:gnAddImage']","TGTYPESCREENREG");

        TYPE.$("ELE_FREELOOK_TABLE","//input[@type='text'][@name='outerForm:freeLookProvSchTbl']","1738","TRUE","TGTYPESCREENREG");

        SELECT.$("ELE_FREELOOK_STATECOUNTRY","//select[@name='outerForm:countryAndStateCode']","United States","TGTYPESCREENREG");

        TYPE.$("ELE_FREELOOK_ENDINGAGE","//input[@type='text'][@name='outerForm:endingAge']","99","TRUE","TGTYPESCREENREG");

        TYPE.$("ELE_FREELOOK_NUMBEROFDAYS","//input[@type='text'][@name='outerForm:freeLookNumberOfDays']","364","TRUE","TGTYPESCREENREG");

        SELECT.$("ELE_FREELOOK_INITIALALLOCATIONRULE","//select[@name='outerForm:initialAllocRule']","Allocate Premium Immediately","TGTYPESCREENREG");

        TAP.$("ELE_FREELOOK_SUBMIT","//input[@type='submit'][@name='outerForm:Submit']","TGTYPESCREENREG");


    }


    @Test
    public void addcommissionschedulerate() {

        // No need to declare / print method name here, handled by TestNg listener
        // declare("addcommissionschedulerate");

        CALL.$("loginGIAS","TGTYPESCREENREG");

        TYPE.$("ELE_LOGINUSERNAME","//input[@type='text'][@name='outerForm:username']","Cloutier","TRUE","TGTYPESCREENREG");

        TYPE.$("ELE_LOGINPASSWORD","//input[@type='password'][@name='outerForm:password']","123456a","TRUE","TGTYPESCREENREG");

        TAP.$("ELE_LOGINBUTTON_SUBMIT","//input[@type='submit'][@name='outerForm:loginButton']","TGTYPESCREENREG");

        CALL.$("CreateAgent","TGTYPESCREENREG");

        HOVER.$("ELE_CREATEAGENT_AGENCYNAV","//td[text()='Agency']","TGTYPESCREENREG");

        TAP.$("ELE_CREATEAGENT_AGENTS","//td[text()='Agents']","TGTYPESCREENREG");

        TAP.$("ELE_CREATEAGENT_ADDBUTTON","//img[@id='outerForm:gnAddImage1']","TGTYPESCREENREG");

        TAP.$("ELE_CREATEAGENT_SELECTCLIENT","//span[text()='Select Client']","TGTYPESCREENREG");

        TYPE.$("ELE_CREATEAGENT_TEXTBOX","//input[@type='text'][@name='outerForm:grid:individualName']","Moody Hank","TRUE","TGTYPESCREENREG");

        TAP.$("ELE_CREATEAGENT_FINDBUTTON","//img[@id='outerForm:gnFindImage']","TGTYPESCREENREG");

        SELECT.$("ELE_CREATEAGENT_SELECTRELATIONSHIP","//select[@name='outerForm:grid:0:relationship']","Agent","TGTYPESCREENREG");

        SELECT.$("ELE_CREATEAGENT_AGENTRANKCODE","//select[@name='outerForm:agencyRankCode']","Writing Agent 9015","TGTYPESCREENREG");

        TAP.$("ELE_CREATEAGENT_SUBMIT","//input[@type='submit'][@name='outerForm:Submit']","TGTYPESCREENREG");

        TYPE.$("ELE_CREATEAGENT_AGENTNUMBER","//input[@type='text'][@name='outerForm:agentNumber']","1206","TRUE","TGTYPESCREENREG");

        SELECT.$("ELE_CREATEAGENT_AGENTSTATUS","//select[@name='outerForm:agentStatus']","Active","TGTYPESCREENREG");

        TYPE.$("ELE_CREATEAGENT_DATEHIRED","//input[@type='text'][@name='outerForm:hireDate']","04/29/2019","TRUE","TGTYPESCREENREG");

        SELECT.$("ELE_CREATEAGENT_DISTRIBUTIONCHANNEL","//select[@name='outerForm:distributionChannel']","BRAUTO9015Distribution Channel","TGTYPESCREENREG");

        TAP.$("ELE_CREATEAGENT_SUBMITFINAL","//input[@type='submit'][@name='outerForm:Submit']","TGTYPESCREENREG");


    }


    @Test
    public void addevent1() {

        // No need to declare / print method name here, handled by TestNg listener
        // declare("addevent1");

        CALL.$("loginGIAS","TGTYPESCREENREG");

        TYPE.$("ELE_LOGINUSERNAME","//input[@type='text'][@name='outerForm:username']","Cloutier","TRUE","TGTYPESCREENREG");

        TYPE.$("ELE_LOGINPASSWORD","//input[@type='password'][@name='outerForm:password']","123456a","TRUE","TGTYPESCREENREG");

        TAP.$("ELE_LOGINBUTTON_SUBMIT","//input[@type='submit'][@name='outerForm:loginButton']","TGTYPESCREENREG");

        CALL.$("AddEvent","TGTYPESCREENREG");

        HOVER.$("ELE_ADDEVENT_COMPPROPNAV","//td[text()='Company Properties']","TGTYPESCREENREG");

        HOVER.$("ELE_ADDEVENT_NONTRADNAV","//td[text()='Nontraditional']","TGTYPESCREENREG");

        TAP.$("ELE_ADDEVENT_EVENTDESCRIPTIONS","//td[text()='Event Descriptions']","TGTYPESCREENREG");

        TAP.$("ELE_ADDEVENT_ADDBUTTON","//img[@id='outerForm:gnAddImage']","TGTYPESCREENREG");

        TYPE.$("ELE_ADDEVENT_TYPE","//input[@type='text'][@name='outerForm:eventType']","Event1206","TRUE","TGTYPESCREENREG");

        TYPE.$("ELE_ADDEVENT_DESCRIPTION","//input[@type='text'][@name='outerForm:eventDescription']","Event1206Event","TRUE","TGTYPESCREENREG");

        TAP.$("ELE_ADDEVENT_CREATECONFIRM","//input[@type='radio'][@name='outerForm:writeConfrimationRcd']","TGTYPESCREENREG");

        TAP.$("ELE_ADDEVENT_SUBMIT","//input[@type='submit'][@name='outerForm:Submit']","TGTYPESCREENREG");


    }


    @Test
    public void transactionhistorylookup1() {

        // No need to declare / print method name here, handled by TestNg listener
        // declare("transactionhistorylookup1");

        CALL.$("loginGIAS","TGTYPESCREENREG");

        TYPE.$("ELE_LOGINUSERNAME","//input[@type='text'][@name='outerForm:username']","Cloutier","TRUE","TGTYPESCREENREG");

        TYPE.$("ELE_LOGINPASSWORD","//input[@type='password'][@name='outerForm:password']","123456a","TRUE","TGTYPESCREENREG");

        TAP.$("ELE_LOGINBUTTON_SUBMIT","//input[@type='submit'][@name='outerForm:loginButton']","TGTYPESCREENREG");

        CALL.$("TransHistoryLookUp","TGTYPESCREENREG");

        HOVER.$("ELE_TRANSHISTORY_INQUIRYNAV","//td[text()='Inquiry']","TGTYPESCREENREG");

        TAP.$("ELE_TRANSHISTORY_QUICKINQUIRY","//td[text()='Quick Inquiry']","TGTYPESCREENREG");

        TYPE.$("ELE_TRANSHISTORY_POLICYNUMBERSEARCH","//input[@type='text'][@name='outerForm:policyNumber']","DP0100000","TRUE","TGTYPESCREENREG");

        TAP.$("ELE_TRANSHISTORY_CONTINUE","//input[@type='submit'][@name='outerForm:Continue']","TGTYPESCREENREG");

        TAP.$("ELE_TRANSHISTORY_POLICYINQUIRY","//span[text()='Policy Inquiry']","TGTYPESCREENREG");

        TAP.$("ELE_TRANSHISTORY_TRANSACTIONHISTORY","//span[text()='Transaction History']","TGTYPESCREENREG");


    }


    @Test
    public void addresschange() {

        // No need to declare / print method name here, handled by TestNg listener
        // declare("addresschange");

        CALL.$("loginGIAS","TGTYPESCREENREG");

        TYPE.$("ELE_LOGINUSERNAME","//input[@type='text'][@name='outerForm:username']","Cloutier","TRUE","TGTYPESCREENREG");

        TYPE.$("ELE_LOGINPASSWORD","//input[@type='password'][@name='outerForm:password']","123456a","TRUE","TGTYPESCREENREG");

        TAP.$("ELE_LOGINBUTTON_SUBMIT","//input[@type='submit'][@name='outerForm:loginButton']","TGTYPESCREENREG");

        CALL.$("AddressChange","TGTYPESCREENREG");

        HOVER.$("ELE_ADDRESSCHANGE_CLIENTNAV","//td[text()='Client']","TGTYPESCREENREG");

        TAP.$("ELE_ADDRESSCHANGE_NAMESANDADDRESS2","//td[text()='Names/Addresses']","TGTYPESCREENREG");

        TAP.$("ELE_ADDRESSCHANGE_223","//span[text()='223, 12']","TGTYPESCREENREG");

        TAP.$("ELE_ADDRESSCHANGE_ADDRESSINFORMATION2","//span[text()='Address Information']","TGTYPESCREENREG");

        TAP.$("ELE_ADDRESSCHANGE_ADDRESSLISTED2","//span[text()='901Boise Way, Shrewsbury New Jersey US 07702']","TGTYPESCREENREG");

        TAP.$("ELE_ADDRESSCHANGE_UPDATE2","//input[@type='submit'][@name='outerForm:Update']","TGTYPESCREENREG");

        TYPE.$("ELE_ADDRESSCHANGE_ADDRESSLINE","//input[@type='text'][@name='outerForm:addressLineOne']","123 Wild Way","TRUE","TGTYPESCREENREG");

        TAP.$("ELE_ADDRESSCHANGE_SUBMIT2","//input[@type='submit'][@name='outerForm:Submit']","TGTYPESCREENREG");


    }


    @Test
    public void dbochange() {

        // No need to declare / print method name here, handled by TestNg listener
        // declare("dbochange");

        CALL.$("loginGIAS","TGTYPESCREENREG");

        TYPE.$("ELE_LOGINUSERNAME","//input[@type='text'][@name='outerForm:username']","Cloutier","TRUE","TGTYPESCREENREG");

        TYPE.$("ELE_LOGINPASSWORD","//input[@type='password'][@name='outerForm:password']","123456a","TRUE","TGTYPESCREENREG");

        TAP.$("ELE_LOGINBUTTON_SUBMIT","//input[@type='submit'][@name='outerForm:loginButton']","TGTYPESCREENREG");

        CALL.$("DBOChange","TGTYPESCREENREG");

        HOVER.$("ELE_MENU_POLICYOWNERSERVICES","//td[text()='Policyowner Services']","TGTYPESCREENREG");

        HOVER.$("ELE_SUBMEN_NONTRADITIONAL","//td[text()='Nontraditional']","TGTYPESCREENREG");

        TAP.$("ELE_SUBMEN_DBOCHANGE","//td[text()='Nontraditional']","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_POLICYNUMBER","//input[@type='text'][@name='outerForm:policyNumber']	","TEST050119","TRUE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_CONTINUEBUTTON","//input[@type='submit'][@name='outerForm:Continue']","TGTYPESCREENREG");

        TAP.$("ELE_RDOBTN_FACEAMOUNT","//input[@type='radio'][@name='outerForm:deathBenefitOption']","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SUBMIT","//input[@type='submit'][@name='outerForm:Submit']","TGTYPESCREENREG");


    }


}
