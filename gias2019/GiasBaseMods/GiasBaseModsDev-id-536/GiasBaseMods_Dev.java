package appium.tgo;

import com.tg.octopus.base.TGOWrapper;
import com.tg.octopus.libs.testng.listener.TestListener;
import static com.tg.octopus.base.TGOWrapper.Action.*;
import static com.tg.octopus.base.TGOWrapper.LOGGER;
import com.tg.octopus.wrapper.ActionWrapper;

import com.jayway.jsonpath.JsonPath;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.SessionNotCreatedException;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.Augmenter;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.remote.UnreachableBrowserException;

import org.testng.Assert;
import org.testng.ITestContext;
import org.testng.ITestResult;
import org.testng.Reporter;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;
import java.net.HttpURLConnection;
import java.net.URL;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import org.json.JSONArray;
import org.json.JSONObject;
import helper.*;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import static helper.WebTestUtility.getSubStringFromString;
import static helper.WebTestUtility.getTextFileToArrayList;
import java.sql.*;
import com.testautomationguru.utility.PDFUtil;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.multipdf.Splitter;

import java.util.*;
import org.openqa.selenium.*;
import java.io.*;
import java.net.*;

import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;


@Listeners(TestListener.class)
public class GiasBaseMods_Dev extends TGOWrapper {



    static {
       needsresetoncebeforetest ="false";
       needsresetbeforeeverymethod ="true";
       captureverystepsnap ="true";
       appendlastsnap ="false";
       ishybridapp ="false";

    }


    @Test
    public void test1() {

        // No need to declare / print method name here, handled by TestNg listener
        // declare("test1");

		String var_dataToPass;
                 LOGGER.info("Executed Step = VAR,String,var_dataToPass,TGTYPESCREENREG");
        CALL.$("LoginGIAS","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_USERNAME","//input[@id='guestForm:username']","autoapi","FALSE","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_PASSWORD","//input[@id='guestForm:password']","Autoapi!23","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_LOGIN","//button[contains(.,'Login')]","TGTYPESCREENREG");

        WAIT.$("ELE_BUTTON_GIAS_LOGO","//img[@id='headerForm:headerLogoImage']","VISIBLE","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

        CALL.$("Test1UpdateExistingPlanCode","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_GIAS_LOGO","//img[@id='headerForm:headerLogoImage']","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        SWIPE.$("DOWN","ELE_TXTBOX_SEARCHTASKS","//input[@id='workbenchForm:workbenchTabs:taskSearchContainer:taskSearchSearch']","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_SEARCHTASKS","//input[@id='workbenchForm:workbenchTabs:taskSearchContainer:taskSearchSearch']","Plan Setup","FALSE","TGTYPESCREENREG");

        WAIT.$(3,"TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SEARCHTASKS","//a[@id='workbenchForm:workbenchTabs:taskSearchContainer:tb_buttonSearch']//span[@class='fa fa-search fa-lg']","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        TAP.$("ELE_LINK_FIRST_TASKS","//button[@id='workbenchForm:workbenchTabs:taskSearchContainer:0:availableTask']","TGTYPESCREENREG");

        WAIT.$("ELE_TXTBOX_FILTER_PLANCODE","//input[@id='workbenchForm:workbenchTabs:grid:planCode_Col:filter']","VISIBLE","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_FILTER_PLANCODE","//input[@id='workbenchForm:workbenchTabs:grid:planCode_Col:filter']","OLUSD2","FALSE","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        TAP.$("ELE_LINK_FIRST_PLANCODE","//span[@id='workbenchForm:workbenchTabs:grid:0:planCode']","TGTYPESCREENREG");

        WAIT.$("ELE_BUTTON_EDIT","//button[@id='workbenchForm:workbenchTabs:tb_buttonUpdate']","VISIBLE","TGTYPESCREENREG");

        TAP.$("ELE_LINK_DIVIDENDS","//span[contains(.,'Dividends')]","TGTYPESCREENREG");

        WAIT.$(3,"TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_EDIT","//button[@id='workbenchForm:workbenchTabs:tb_buttonUpdate']","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        SWIPE.$("UP","ELE_DRPDWN_DEFAULTOPETION1","//label[@id=\"workbenchForm:workbenchTabs:defaultOption1_label\"]","TGTYPESCREENREG");

        TAP.$("ELE_DRPDWN_DEFAULTOPETION1","//label[@id=\"workbenchForm:workbenchTabs:defaultOption1_label\"]","TGTYPESCREENREG");

		var_dataToPass = "Purchase One Year Term (Cash Value Limit)";
                 LOGGER.info("Executed Step = STORE,var_dataToPass,Purchase One Year Term (Cash Value Limit),TGTYPESCREENREG");
		try { 
		 			Thread.sleep(1000);
					for (int i = 0; i < 2000; i++){
						WebElement selectValueOfDropDown = driver.findElement(By.xpath("//li[@id='workbenchForm:workbenchTabs:defaultOption1_"+ i +"']"));
		                         System.out.println("Execution time: "  + selectValueOfDropDown.getText());                
						if (selectValueOfDropDown.getText().contains(var_dataToPass)){

		                                
		AddToReport("ASSERT," + selectValueOfDropDown.getText() +", CONTAINS,"+var_dataToPass);
				
							break;
						}
					}
					takeScreenshot();

					Thread.sleep(2000);
		 
		 ;} 
		 catch(Exception e) { PrintVal("Renderer Script Exception : "+e.toString()); } 
		 PrintVal("Executed Step = CALLRENDERERSCRIPT,CHECK_DEFAULTOPTION1"); 
        WAIT.$(5,"TGTYPESCREENREG");

		var_dataToPass = "Purchase One Year Term (Face Amount Limit)";
                 LOGGER.info("Executed Step = STORE,var_dataToPass,Purchase One Year Term (Face Amount Limit),TGTYPESCREENREG");
		try { 
		 			Thread.sleep(1000);
					for (int i = 0; i < 2000; i++){
						WebElement selectValueOfDropDown = driver.findElement(By.xpath("//li[@id='workbenchForm:workbenchTabs:defaultOption1_"+ i +"']"));
		                         System.out.println("Execution time: "  + selectValueOfDropDown.getText());                
						if (selectValueOfDropDown.getText().contains(var_dataToPass)){

		                                
		AddToReport("ASSERT," + selectValueOfDropDown.getText() +", CONTAINS,"+var_dataToPass);
				
							break;
						}
					}
					takeScreenshot();

					Thread.sleep(2000);
		 
		 ;} 
		 catch(Exception e) { PrintVal("Renderer Script Exception : "+e.toString()); } 
		 PrintVal("Executed Step = CALLRENDERERSCRIPT,CHECK_DEFAULTOPTION1"); 
        WAIT.$(5,"TGTYPESCREENREG");

        TAP.$("ELE_DRPDWN_DEFAULTOPETION1","//label[@id=\"workbenchForm:workbenchTabs:defaultOption1_label\"]","TGTYPESCREENREG");

        SWIPE.$("DOWN","ELE_DRPDWN_PURCHASE1YEARTERMFAALLOWED","//*[@id=\"workbenchForm:workbenchTabs:purchase1YearTermFAAllowed_label\"]","TGTYPESCREENREG");

		var_dataToPass = "Yes";
                 LOGGER.info("Executed Step = STORE,var_dataToPass,Yes,TGTYPESCREENREG");
        TAP.$("ELE_DRPDWN_PURCHASE1YEARTERMALLOWED","//*[@id=\"workbenchForm:workbenchTabs:purchase1YearTermAllowed_label\"]","TGTYPESCREENREG");

		try { 
		 			Thread.sleep(1000);
					for (int i = 0; i < 2000; i++){
						WebElement selectValueOfDropDown= driver.findElement(By.xpath("//li[@id='workbenchForm:workbenchTabs:purchase1YearTermAllowed_"+ i +"']"));

						if (selectValueOfDropDown.getText().contains(var_dataToPass)){

		                                
							   AddToReport("ASSERT," + selectValueOfDropDown.getText() +", CONTAINS,"+var_dataToPass);
							//selectValueOfDropDown.click();
							break;
						}
					}
					takeScreenshot();

					Thread.sleep(2000);
		 
		 ;} 
		 catch(Exception e) { PrintVal("Renderer Script Exception : "+e.toString()); } 
		 PrintVal("Executed Step = CALLRENDERERSCRIPT,CHECK_DRPDWN_PURCHASE1YEARTERMALLOWED_CASH_AMOUNT"); 
		var_dataToPass = "No";
                 LOGGER.info("Executed Step = STORE,var_dataToPass,No,TGTYPESCREENREG");
		try { 
		 			Thread.sleep(1000);
					for (int i = 0; i < 2000; i++){
						WebElement selectValueOfDropDown= driver.findElement(By.xpath("//li[@id='workbenchForm:workbenchTabs:purchase1YearTermAllowed_"+ i +"']"));

						if (selectValueOfDropDown.getText().contains(var_dataToPass)){

		                                
							   AddToReport("ASSERT," + selectValueOfDropDown.getText() +", CONTAINS,"+var_dataToPass);
							//selectValueOfDropDown.click();
							break;
						}
					}
					takeScreenshot();

					Thread.sleep(2000);
		 
		 ;} 
		 catch(Exception e) { PrintVal("Renderer Script Exception : "+e.toString()); } 
		 PrintVal("Executed Step = CALLRENDERERSCRIPT,CHECK_DRPDWN_PURCHASE1YEARTERMALLOWED_CASH_AMOUNT"); 
        WAIT.$(5,"TGTYPESCREENREG");

        TAP.$("ELE_DRPDWN_PURCHASE1YEARTERMALLOWED","//*[@id=\"workbenchForm:workbenchTabs:purchase1YearTermAllowed_label\"]","TGTYPESCREENREG");

        TAP.$("ELE_DRPDWN_PURCHASE1YEARTERMFAALLOWED","//*[@id=\"workbenchForm:workbenchTabs:purchase1YearTermFAAllowed_label\"]","TGTYPESCREENREG");

		var_dataToPass = "Yes";
                 LOGGER.info("Executed Step = STORE,var_dataToPass,Yes,TGTYPESCREENREG");
		try { 
		 			Thread.sleep(1000);
					for (int i = 0; i < 2000; i++){
						WebElement selectValueOfDropDown= driver.findElement(By.xpath("//li[@id='workbenchForm:workbenchTabs:purchase1YearTermFAAllowed_"+ i +"']"));

						if (selectValueOfDropDown.getText().contains(var_dataToPass)){

		                                
							AddToReport("ASSERT," + selectValueOfDropDown.getText() +", CONTAINS,"+var_dataToPass);
						
							break;
						}
					}
					takeScreenshot();

					Thread.sleep(2000);
		 
		 ;} 
		 catch(Exception e) { PrintVal("Renderer Script Exception : "+e.toString()); } 
		 PrintVal("Executed Step = CALLRENDERERSCRIPT,CHECK_DRPDWN_PURCHASE1YEARTERMFAALLOWED_FACE_AMOUNT"); 
        WAIT.$(5,"TGTYPESCREENREG");

		var_dataToPass = "No";
                 LOGGER.info("Executed Step = STORE,var_dataToPass,No,TGTYPESCREENREG");
		try { 
		 			Thread.sleep(1000);
					for (int i = 0; i < 2000; i++){
						WebElement selectValueOfDropDown= driver.findElement(By.xpath("//li[@id='workbenchForm:workbenchTabs:purchase1YearTermFAAllowed_"+ i +"']"));

						if (selectValueOfDropDown.getText().contains(var_dataToPass)){

		                                
							AddToReport("ASSERT," + selectValueOfDropDown.getText() +", CONTAINS,"+var_dataToPass);
						
							break;
						}
					}
					takeScreenshot();

					Thread.sleep(2000);
		 
		 ;} 
		 catch(Exception e) { PrintVal("Renderer Script Exception : "+e.toString()); } 
		 PrintVal("Executed Step = CALLRENDERERSCRIPT,CHECK_DRPDWN_PURCHASE1YEARTERMFAALLOWED_FACE_AMOUNT"); 
        TAP.$("ELE_DRPDWN_PURCHASE1YEARTERMFAALLOWED","//*[@id=\"workbenchForm:workbenchTabs:purchase1YearTermFAAllowed_label\"]","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        SWIPE.$("DOWN","ELE_LINK_COUPONS","//span[@class='ui-menuitem-text'][contains(text(),'Coupons')]","TGTYPESCREENREG");

        SWIPE.$("DOWN","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        TAP.$("ELE_LINK_COUPONS","//span[@class='ui-menuitem-text'][contains(text(),'Coupons')]","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        SWIPE.$("DOWN","ELE_DRPDWN_PURCHASE1YEARTERMFAALLOWED","//*[@id=\"workbenchForm:workbenchTabs:purchase1YearTermFAAllowed_label\"]","TGTYPESCREENREG");

		var_dataToPass = "Yes";
                 LOGGER.info("Executed Step = STORE,var_dataToPass,Yes,TGTYPESCREENREG");
        TAP.$("ELE_DRPDWN_PURCHASE1YEARTERMALLOWED","//*[@id=\"workbenchForm:workbenchTabs:purchase1YearTermAllowed_label\"]","TGTYPESCREENREG");

		try { 
		 			Thread.sleep(1000);
					for (int i = 0; i < 2000; i++){
						WebElement selectValueOfDropDown= driver.findElement(By.xpath("//li[@id='workbenchForm:workbenchTabs:purchase1YearTermAllowed_"+ i +"']"));

						if (selectValueOfDropDown.getText().contains(var_dataToPass)){

		                                
							   AddToReport("ASSERT," + selectValueOfDropDown.getText() +", CONTAINS,"+var_dataToPass);
							//selectValueOfDropDown.click();
							break;
						}
					}
					takeScreenshot();

					Thread.sleep(2000);
		 
		 ;} 
		 catch(Exception e) { PrintVal("Renderer Script Exception : "+e.toString()); } 
		 PrintVal("Executed Step = CALLRENDERERSCRIPT,CHECK_DRPDWN_PURCHASE1YEARTERMALLOWED_CASH_AMOUNT"); 
        WAIT.$(5,"TGTYPESCREENREG");

		var_dataToPass = "No";
                 LOGGER.info("Executed Step = STORE,var_dataToPass,No,TGTYPESCREENREG");
		try { 
		 			Thread.sleep(1000);
					for (int i = 0; i < 2000; i++){
						WebElement selectValueOfDropDown= driver.findElement(By.xpath("//li[@id='workbenchForm:workbenchTabs:purchase1YearTermAllowed_"+ i +"']"));

						if (selectValueOfDropDown.getText().contains(var_dataToPass)){

		                                
							   AddToReport("ASSERT," + selectValueOfDropDown.getText() +", CONTAINS,"+var_dataToPass);
							//selectValueOfDropDown.click();
							break;
						}
					}
					takeScreenshot();

					Thread.sleep(2000);
		 
		 ;} 
		 catch(Exception e) { PrintVal("Renderer Script Exception : "+e.toString()); } 
		 PrintVal("Executed Step = CALLRENDERERSCRIPT,CHECK_DRPDWN_PURCHASE1YEARTERMALLOWED_CASH_AMOUNT"); 
        TAP.$("ELE_DRPDWN_PURCHASE1YEARTERMALLOWED","//*[@id=\"workbenchForm:workbenchTabs:purchase1YearTermAllowed_label\"]","TGTYPESCREENREG");

        TAP.$("ELE_DRPDWN_PURCHASE1YEARTERMFAALLOWED","//*[@id=\"workbenchForm:workbenchTabs:purchase1YearTermFAAllowed_label\"]","TGTYPESCREENREG");

		var_dataToPass = "Yes";
                 LOGGER.info("Executed Step = STORE,var_dataToPass,Yes,TGTYPESCREENREG");
		try { 
		 			Thread.sleep(1000);
					for (int i = 0; i < 2000; i++){
						WebElement selectValueOfDropDown= driver.findElement(By.xpath("//li[@id='workbenchForm:workbenchTabs:purchase1YearTermFAAllowed_"+ i +"']"));

						if (selectValueOfDropDown.getText().contains(var_dataToPass)){

		                                
							AddToReport("ASSERT," + selectValueOfDropDown.getText() +", CONTAINS,"+var_dataToPass);
						
							break;
						}
					}
					takeScreenshot();

					Thread.sleep(2000);
		 
		 ;} 
		 catch(Exception e) { PrintVal("Renderer Script Exception : "+e.toString()); } 
		 PrintVal("Executed Step = CALLRENDERERSCRIPT,CHECK_DRPDWN_PURCHASE1YEARTERMFAALLOWED_FACE_AMOUNT"); 
		var_dataToPass = "No";
                 LOGGER.info("Executed Step = STORE,var_dataToPass,No,TGTYPESCREENREG");
		try { 
		 			Thread.sleep(1000);
					for (int i = 0; i < 2000; i++){
						WebElement selectValueOfDropDown= driver.findElement(By.xpath("//li[@id='workbenchForm:workbenchTabs:purchase1YearTermFAAllowed_"+ i +"']"));

						if (selectValueOfDropDown.getText().contains(var_dataToPass)){

		                                
							AddToReport("ASSERT," + selectValueOfDropDown.getText() +", CONTAINS,"+var_dataToPass);
						
							break;
						}
					}
					takeScreenshot();

					Thread.sleep(2000);
		 
		 ;} 
		 catch(Exception e) { PrintVal("Renderer Script Exception : "+e.toString()); } 
		 PrintVal("Executed Step = CALLRENDERERSCRIPT,CHECK_DRPDWN_PURCHASE1YEARTERMFAALLOWED_FACE_AMOUNT"); 
        WAIT.$(5,"TGTYPESCREENREG");

        TAP.$("ELE_DRPDWN_PURCHASE1YEARTERMFAALLOWED","//*[@id=\"workbenchForm:workbenchTabs:purchase1YearTermFAAllowed_label\"]","TGTYPESCREENREG");

        SWIPE.$("UP","ELE_DRPDWN_DEFAULTOPETION1","//label[@id=\"workbenchForm:workbenchTabs:defaultOption1_label\"]","TGTYPESCREENREG");

        TAP.$("ELE_DRPDWN_DEFAULTOPETION1","//label[@id=\"workbenchForm:workbenchTabs:defaultOption1_label\"]","TGTYPESCREENREG");

        WAIT.$(3,"TGTYPESCREENREG");

		var_dataToPass = "Purchase One Year Term (Cash Value Limit)";
                 LOGGER.info("Executed Step = STORE,var_dataToPass,Purchase One Year Term (Cash Value Limit),TGTYPESCREENREG");
		try { 
		 			Thread.sleep(1000);
					for (int i = 0; i < 2000; i++){
						WebElement selectValueOfDropDown = driver.findElement(By.xpath("//li[@id='workbenchForm:workbenchTabs:defaultOption1_"+ i +"']"));
		                         System.out.println("Execution time: "  + selectValueOfDropDown.getText());                
						if (selectValueOfDropDown.getText().contains(var_dataToPass)){

		                                
		AddToReport("ASSERT," + selectValueOfDropDown.getText() +", CONTAINS,"+var_dataToPass);
				
							break;
						}
					}
					takeScreenshot();

					Thread.sleep(2000);
		 
		 ;} 
		 catch(Exception e) { PrintVal("Renderer Script Exception : "+e.toString()); } 
		 PrintVal("Executed Step = CALLRENDERERSCRIPT,CHECK_DEFAULTOPTION1"); 
        WAIT.$(5,"TGTYPESCREENREG");

		var_dataToPass = "Purchase One Year Term (Face Amount Limit)";
                 LOGGER.info("Executed Step = STORE,var_dataToPass,Purchase One Year Term (Face Amount Limit),TGTYPESCREENREG");
		try { 
		 			Thread.sleep(1000);
					for (int i = 0; i < 2000; i++){
						WebElement selectValueOfDropDown = driver.findElement(By.xpath("//li[@id='workbenchForm:workbenchTabs:defaultOption1_"+ i +"']"));
		                         System.out.println("Execution time: "  + selectValueOfDropDown.getText());                
						if (selectValueOfDropDown.getText().contains(var_dataToPass)){

		                                
		AddToReport("ASSERT," + selectValueOfDropDown.getText() +", CONTAINS,"+var_dataToPass);
				
							break;
						}
					}
					takeScreenshot();

					Thread.sleep(2000);
		 
		 ;} 
		 catch(Exception e) { PrintVal("Renderer Script Exception : "+e.toString()); } 
		 PrintVal("Executed Step = CALLRENDERERSCRIPT,CHECK_DEFAULTOPTION1"); 
        WAIT.$(5,"TGTYPESCREENREG");

        TAP.$("ELE_DRPDWN_DEFAULTOPETION1","//label[@id=\"workbenchForm:workbenchTabs:defaultOption1_label\"]","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");


    }


    @Test
    public void sampletesting() {

        // No need to declare / print method name here, handled by TestNg listener
        // declare("sampletesting");

        CALL.$("LoginGIAS","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_USERNAME","//input[@id='guestForm:username']","autoapi","FALSE","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_PASSWORD","//input[@id='guestForm:password']","Autoapi!23","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_LOGIN","//button[contains(.,'Login')]","TGTYPESCREENREG");

        WAIT.$("ELE_BUTTON_GIAS_LOGO","//img[@id='headerForm:headerLogoImage']","VISIBLE","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

		String var_dataToPass;
                 LOGGER.info("Executed Step = VAR,String,var_dataToPass,TGTYPESCREENREG");
		int var_Count;
                 LOGGER.info("Executed Step = VAR,Integer,var_Count,TGTYPESCREENREG");
		int var_CountN;
                 LOGGER.info("Executed Step = VAR,Integer,var_CountN,TGTYPESCREENREG");
        LOGGER.info("Executed Step = REPEAT IF");
    	while (check(var_CountN,"<=",1,"TGTYPESCREENREG")) {
          LOGGER.info("Executed Step = WHILE,var_CountN,<=,1,TGTYPESCREENREG");
		String var_FileName = "C:/GIAS_Automation/4.0 BaseRegression/InputData/GIASBase40_IP_Reg184.csv";
                 LOGGER.info("Executed Step = VAR,String,var_FileName,C:/GIAS_Automation/4.0 BaseRegression/InputData/GIASBase40_IP_Reg184.csv,TGTYPESCREENREG");
		com.jayway.jsonpath.ReadContext var_CSVData = getJsonData("http://10.1.104.229/json/20210201/KWluNQ.json");
                 LOGGER.info("Executed Step = VAR,Json,var_CSVData,http://10.1.104.229/json/20210201/KWluNQ.json,TGTYPESCREENREG");
        CALL.$("Test1UpdateExistingPlanCode","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_GIAS_LOGO","//img[@id='headerForm:headerLogoImage']","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        SWIPE.$("DOWN","ELE_TXTBOX_SEARCHTASKS","//input[@id='workbenchForm:workbenchTabs:taskSearchContainer:taskSearchSearch']","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_SEARCHTASKS","//input[@id='workbenchForm:workbenchTabs:taskSearchContainer:taskSearchSearch']","Plan Setup","FALSE","TGTYPESCREENREG");

        WAIT.$(3,"TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SEARCHTASKS","//a[@id='workbenchForm:workbenchTabs:taskSearchContainer:tb_buttonSearch']//span[@class='fa fa-search fa-lg']","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        TAP.$("ELE_LINK_FIRST_TASKS","//button[@id='workbenchForm:workbenchTabs:taskSearchContainer:0:availableTask']","TGTYPESCREENREG");

        WAIT.$("ELE_TXTBOX_FILTER_PLANCODE","//input[@id='workbenchForm:workbenchTabs:grid:planCode_Col:filter']","VISIBLE","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_FILTER_PLANCODE","//input[@id='workbenchForm:workbenchTabs:grid:planCode_Col:filter']","OLUSD2","FALSE","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        TAP.$("ELE_LINK_FIRST_PLANCODE","//span[@id='workbenchForm:workbenchTabs:grid:0:planCode']","TGTYPESCREENREG");

        WAIT.$("ELE_BUTTON_EDIT","//button[@id='workbenchForm:workbenchTabs:tb_buttonUpdate']","VISIBLE","TGTYPESCREENREG");

        TAP.$("ELE_LINK_DIVIDENDS","//span[contains(.,'Dividends')]","TGTYPESCREENREG");

        WAIT.$(3,"TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_EDIT","//button[@id='workbenchForm:workbenchTabs:tb_buttonUpdate']","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        SWIPE.$("UP","ELE_DRPDWN_DEFAULTOPETION1","//label[@id=\\"workbenchForm:workbenchTabs:defaultOption1_label\\"]","TGTYPESCREENREG");

        TAP.$("ELE_DRPDWN_DEFAULTOPETION1","//label[@id=\\"workbenchForm:workbenchTabs:defaultOption1_label\\"]","TGTYPESCREENREG");

		var_dataToPass = "Purchase One Year Term (Cash Value Limit)";
                 LOGGER.info("Executed Step = STORE,var_dataToPass,Purchase One Year Term (Cash Value Limit),TGTYPESCREENREG");
		try { 
		 			Thread.sleep(1000);
					for (int i = 0; i < 2000; i++){
						WebElement selectValueOfDropDown = driver.findElement(By.xpath("//li[@id='workbenchForm:workbenchTabs:defaultOption1_"+ i +"']"));
		                         System.out.println("Execution time: "  + selectValueOfDropDown.getText());                
						if (selectValueOfDropDown.getText().contains(var_dataToPass)){

		                                
		AddToReport("ASSERT," + selectValueOfDropDown.getText() +", CONTAINS,"+var_dataToPass);
				
							break;
						}
					}
					takeScreenshot();

					Thread.sleep(2000);
		 
		 ;} 
		 catch(Exception e) { PrintVal("Renderer Script Exception : "+e.toString()); } 
		 PrintVal("Executed Step = CALLRENDERERSCRIPT,CHECK_DEFAULTOPTION1"); 
        WAIT.$(5,"TGTYPESCREENREG");

		var_dataToPass = "Purchase One Year Term (Face Amount Limit)";
                 LOGGER.info("Executed Step = STORE,var_dataToPass,Purchase One Year Term (Face Amount Limit),TGTYPESCREENREG");
		try { 
		 			Thread.sleep(1000);
					for (int i = 0; i < 2000; i++){
						WebElement selectValueOfDropDown = driver.findElement(By.xpath("//li[@id='workbenchForm:workbenchTabs:defaultOption1_"+ i +"']"));
		                         System.out.println("Execution time: "  + selectValueOfDropDown.getText());                
						if (selectValueOfDropDown.getText().contains(var_dataToPass)){

		                                
		AddToReport("ASSERT," + selectValueOfDropDown.getText() +", CONTAINS,"+var_dataToPass);
				
							break;
						}
					}
					takeScreenshot();

					Thread.sleep(2000);
		 
		 ;} 
		 catch(Exception e) { PrintVal("Renderer Script Exception : "+e.toString()); } 
		 PrintVal("Executed Step = CALLRENDERERSCRIPT,CHECK_DEFAULTOPTION1"); 
        WAIT.$(5,"TGTYPESCREENREG");

        TAP.$("ELE_DRPDWN_DEFAULTOPETION1","//label[@id=\\"workbenchForm:workbenchTabs:defaultOption1_label\\"]","TGTYPESCREENREG");

        SWIPE.$("DOWN","ELE_DRPDWN_PURCHASE1YEARTERMFAALLOWED","//*[@id=\\"workbenchForm:workbenchTabs:purchase1YearTermFAAllowed_label\\"]","TGTYPESCREENREG");

		var_dataToPass = "Yes";
                 LOGGER.info("Executed Step = STORE,var_dataToPass,Yes,TGTYPESCREENREG");
        TAP.$("ELE_DRPDWN_PURCHASE1YEARTERMALLOWED","//*[@id=\\"workbenchForm:workbenchTabs:purchase1YearTermAllowed_label\\"]","TGTYPESCREENREG");

		try { 
		 			Thread.sleep(1000);
					for (int i = 0; i < 2000; i++){
						WebElement selectValueOfDropDown= driver.findElement(By.xpath("//li[@id='workbenchForm:workbenchTabs:purchase1YearTermAllowed_"+ i +"']"));

						if (selectValueOfDropDown.getText().contains(var_dataToPass)){

		                                
							   AddToReport("ASSERT," + selectValueOfDropDown.getText() +", CONTAINS,"+var_dataToPass);
							//selectValueOfDropDown.click();
							break;
						}
					}
					takeScreenshot();

					Thread.sleep(2000);
		 
		 ;} 
		 catch(Exception e) { PrintVal("Renderer Script Exception : "+e.toString()); } 
		 PrintVal("Executed Step = CALLRENDERERSCRIPT,CHECK_DRPDWN_PURCHASE1YEARTERMALLOWED_CASH_AMOUNT"); 
		var_dataToPass = "No";
                 LOGGER.info("Executed Step = STORE,var_dataToPass,No,TGTYPESCREENREG");
		try { 
		 			Thread.sleep(1000);
					for (int i = 0; i < 2000; i++){
						WebElement selectValueOfDropDown= driver.findElement(By.xpath("//li[@id='workbenchForm:workbenchTabs:purchase1YearTermAllowed_"+ i +"']"));

						if (selectValueOfDropDown.getText().contains(var_dataToPass)){

		                                
							   AddToReport("ASSERT," + selectValueOfDropDown.getText() +", CONTAINS,"+var_dataToPass);
							//selectValueOfDropDown.click();
							break;
						}
					}
					takeScreenshot();

					Thread.sleep(2000);
		 
		 ;} 
		 catch(Exception e) { PrintVal("Renderer Script Exception : "+e.toString()); } 
		 PrintVal("Executed Step = CALLRENDERERSCRIPT,CHECK_DRPDWN_PURCHASE1YEARTERMALLOWED_CASH_AMOUNT"); 
        WAIT.$(5,"TGTYPESCREENREG");

        TAP.$("ELE_DRPDWN_PURCHASE1YEARTERMALLOWED","//*[@id=\\"workbenchForm:workbenchTabs:purchase1YearTermAllowed_label\\"]","TGTYPESCREENREG");

        TAP.$("ELE_DRPDWN_PURCHASE1YEARTERMFAALLOWED","//*[@id=\\"workbenchForm:workbenchTabs:purchase1YearTermFAAllowed_label\\"]","TGTYPESCREENREG");

		var_dataToPass = "Yes";
                 LOGGER.info("Executed Step = STORE,var_dataToPass,Yes,TGTYPESCREENREG");
		try { 
		 			Thread.sleep(1000);
					for (int i = 0; i < 2000; i++){
						WebElement selectValueOfDropDown= driver.findElement(By.xpath("//li[@id='workbenchForm:workbenchTabs:purchase1YearTermFAAllowed_"+ i +"']"));

						if (selectValueOfDropDown.getText().contains(var_dataToPass)){

		                                
							AddToReport("ASSERT," + selectValueOfDropDown.getText() +", CONTAINS,"+var_dataToPass);
						
							break;
						}
					}
					takeScreenshot();

					Thread.sleep(2000);
		 
		 ;} 
		 catch(Exception e) { PrintVal("Renderer Script Exception : "+e.toString()); } 
		 PrintVal("Executed Step = CALLRENDERERSCRIPT,CHECK_DRPDWN_PURCHASE1YEARTERMFAALLOWED_FACE_AMOUNT"); 
        WAIT.$(5,"TGTYPESCREENREG");

		var_dataToPass = "No";
                 LOGGER.info("Executed Step = STORE,var_dataToPass,No,TGTYPESCREENREG");
		try { 
		 			Thread.sleep(1000);
					for (int i = 0; i < 2000; i++){
						WebElement selectValueOfDropDown= driver.findElement(By.xpath("//li[@id='workbenchForm:workbenchTabs:purchase1YearTermFAAllowed_"+ i +"']"));

						if (selectValueOfDropDown.getText().contains(var_dataToPass)){

		                                
							AddToReport("ASSERT," + selectValueOfDropDown.getText() +", CONTAINS,"+var_dataToPass);
						
							break;
						}
					}
					takeScreenshot();

					Thread.sleep(2000);
		 
		 ;} 
		 catch(Exception e) { PrintVal("Renderer Script Exception : "+e.toString()); } 
		 PrintVal("Executed Step = CALLRENDERERSCRIPT,CHECK_DRPDWN_PURCHASE1YEARTERMFAALLOWED_FACE_AMOUNT"); 
        TAP.$("ELE_DRPDWN_PURCHASE1YEARTERMFAALLOWED","//*[@id=\\"workbenchForm:workbenchTabs:purchase1YearTermFAAllowed_label\\"]","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        SWIPE.$("DOWN","ELE_LINK_COUPONS","//span[@class='ui-menuitem-text'][contains(text(),'Coupons')]","TGTYPESCREENREG");

        SWIPE.$("DOWN","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        TAP.$("ELE_LINK_COUPONS","//span[@class='ui-menuitem-text'][contains(text(),'Coupons')]","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        SWIPE.$("DOWN","ELE_DRPDWN_PURCHASE1YEARTERMFAALLOWED","//*[@id=\\"workbenchForm:workbenchTabs:purchase1YearTermFAAllowed_label\\"]","TGTYPESCREENREG");

		var_dataToPass = "Yes";
                 LOGGER.info("Executed Step = STORE,var_dataToPass,Yes,TGTYPESCREENREG");
        TAP.$("ELE_DRPDWN_PURCHASE1YEARTERMALLOWED","//*[@id=\\"workbenchForm:workbenchTabs:purchase1YearTermAllowed_label\\"]","TGTYPESCREENREG");

		try { 
		 			Thread.sleep(1000);
					for (int i = 0; i < 2000; i++){
						WebElement selectValueOfDropDown= driver.findElement(By.xpath("//li[@id='workbenchForm:workbenchTabs:purchase1YearTermAllowed_"+ i +"']"));

						if (selectValueOfDropDown.getText().contains(var_dataToPass)){

		                                
							   AddToReport("ASSERT," + selectValueOfDropDown.getText() +", CONTAINS,"+var_dataToPass);
							//selectValueOfDropDown.click();
							break;
						}
					}
					takeScreenshot();

					Thread.sleep(2000);
		 
		 ;} 
		 catch(Exception e) { PrintVal("Renderer Script Exception : "+e.toString()); } 
		 PrintVal("Executed Step = CALLRENDERERSCRIPT,CHECK_DRPDWN_PURCHASE1YEARTERMALLOWED_CASH_AMOUNT"); 
        WAIT.$(5,"TGTYPESCREENREG");

		var_dataToPass = "No";
                 LOGGER.info("Executed Step = STORE,var_dataToPass,No,TGTYPESCREENREG");
		try { 
		 			Thread.sleep(1000);
					for (int i = 0; i < 2000; i++){
						WebElement selectValueOfDropDown= driver.findElement(By.xpath("//li[@id='workbenchForm:workbenchTabs:purchase1YearTermAllowed_"+ i +"']"));

						if (selectValueOfDropDown.getText().contains(var_dataToPass)){

		                                
							   AddToReport("ASSERT," + selectValueOfDropDown.getText() +", CONTAINS,"+var_dataToPass);
							//selectValueOfDropDown.click();
							break;
						}
					}
					takeScreenshot();

					Thread.sleep(2000);
		 
		 ;} 
		 catch(Exception e) { PrintVal("Renderer Script Exception : "+e.toString()); } 
		 PrintVal("Executed Step = CALLRENDERERSCRIPT,CHECK_DRPDWN_PURCHASE1YEARTERMALLOWED_CASH_AMOUNT"); 
        TAP.$("ELE_DRPDWN_PURCHASE1YEARTERMALLOWED","//*[@id=\\"workbenchForm:workbenchTabs:purchase1YearTermAllowed_label\\"]","TGTYPESCREENREG");

        TAP.$("ELE_DRPDWN_PURCHASE1YEARTERMFAALLOWED","//*[@id=\\"workbenchForm:workbenchTabs:purchase1YearTermFAAllowed_label\\"]","TGTYPESCREENREG");

		var_dataToPass = "Yes";
                 LOGGER.info("Executed Step = STORE,var_dataToPass,Yes,TGTYPESCREENREG");
		try { 
		 			Thread.sleep(1000);
					for (int i = 0; i < 2000; i++){
						WebElement selectValueOfDropDown= driver.findElement(By.xpath("//li[@id='workbenchForm:workbenchTabs:purchase1YearTermFAAllowed_"+ i +"']"));

						if (selectValueOfDropDown.getText().contains(var_dataToPass)){

		                                
							AddToReport("ASSERT," + selectValueOfDropDown.getText() +", CONTAINS,"+var_dataToPass);
						
							break;
						}
					}
					takeScreenshot();

					Thread.sleep(2000);
		 
		 ;} 
		 catch(Exception e) { PrintVal("Renderer Script Exception : "+e.toString()); } 
		 PrintVal("Executed Step = CALLRENDERERSCRIPT,CHECK_DRPDWN_PURCHASE1YEARTERMFAALLOWED_FACE_AMOUNT"); 
		var_dataToPass = "No";
                 LOGGER.info("Executed Step = STORE,var_dataToPass,No,TGTYPESCREENREG");
		try { 
		 			Thread.sleep(1000);
					for (int i = 0; i < 2000; i++){
						WebElement selectValueOfDropDown= driver.findElement(By.xpath("//li[@id='workbenchForm:workbenchTabs:purchase1YearTermFAAllowed_"+ i +"']"));

						if (selectValueOfDropDown.getText().contains(var_dataToPass)){

		                                
							AddToReport("ASSERT," + selectValueOfDropDown.getText() +", CONTAINS,"+var_dataToPass);
						
							break;
						}
					}
					takeScreenshot();

					Thread.sleep(2000);
		 
		 ;} 
		 catch(Exception e) { PrintVal("Renderer Script Exception : "+e.toString()); } 
		 PrintVal("Executed Step = CALLRENDERERSCRIPT,CHECK_DRPDWN_PURCHASE1YEARTERMFAALLOWED_FACE_AMOUNT"); 
        WAIT.$(5,"TGTYPESCREENREG");

        TAP.$("ELE_DRPDWN_PURCHASE1YEARTERMFAALLOWED","//*[@id=\\"workbenchForm:workbenchTabs:purchase1YearTermFAAllowed_label\\"]","TGTYPESCREENREG");

        SWIPE.$("UP","ELE_DRPDWN_DEFAULTOPETION1","//label[@id=\\"workbenchForm:workbenchTabs:defaultOption1_label\\"]","TGTYPESCREENREG");

        TAP.$("ELE_DRPDWN_DEFAULTOPETION1","//label[@id=\\"workbenchForm:workbenchTabs:defaultOption1_label\\"]","TGTYPESCREENREG");

        WAIT.$(3,"TGTYPESCREENREG");

		var_dataToPass = "Purchase One Year Term (Cash Value Limit)";
                 LOGGER.info("Executed Step = STORE,var_dataToPass,Purchase One Year Term (Cash Value Limit),TGTYPESCREENREG");
		try { 
		 			Thread.sleep(1000);
					for (int i = 0; i < 2000; i++){
						WebElement selectValueOfDropDown = driver.findElement(By.xpath("//li[@id='workbenchForm:workbenchTabs:defaultOption1_"+ i +"']"));
		                         System.out.println("Execution time: "  + selectValueOfDropDown.getText());                
						if (selectValueOfDropDown.getText().contains(var_dataToPass)){

		                                
		AddToReport("ASSERT," + selectValueOfDropDown.getText() +", CONTAINS,"+var_dataToPass);
				
							break;
						}
					}
					takeScreenshot();

					Thread.sleep(2000);
		 
		 ;} 
		 catch(Exception e) { PrintVal("Renderer Script Exception : "+e.toString()); } 
		 PrintVal("Executed Step = CALLRENDERERSCRIPT,CHECK_DEFAULTOPTION1"); 
        WAIT.$(5,"TGTYPESCREENREG");

		var_dataToPass = "Purchase One Year Term (Face Amount Limit)";
                 LOGGER.info("Executed Step = STORE,var_dataToPass,Purchase One Year Term (Face Amount Limit),TGTYPESCREENREG");
		try { 
		 			Thread.sleep(1000);
					for (int i = 0; i < 2000; i++){
						WebElement selectValueOfDropDown = driver.findElement(By.xpath("//li[@id='workbenchForm:workbenchTabs:defaultOption1_"+ i +"']"));
		                         System.out.println("Execution time: "  + selectValueOfDropDown.getText());                
						if (selectValueOfDropDown.getText().contains(var_dataToPass)){

		                                
		AddToReport("ASSERT," + selectValueOfDropDown.getText() +", CONTAINS,"+var_dataToPass);
				
							break;
						}
					}
					takeScreenshot();

					Thread.sleep(2000);
		 
		 ;} 
		 catch(Exception e) { PrintVal("Renderer Script Exception : "+e.toString()); } 
		 PrintVal("Executed Step = CALLRENDERERSCRIPT,CHECK_DEFAULTOPTION1"); 
        WAIT.$(5,"TGTYPESCREENREG");

        TAP.$("ELE_DRPDWN_DEFAULTOPETION1","//label[@id=\\"workbenchForm:workbenchTabs:defaultOption1_label\\"]","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

		var_CountN = var_CountN + ( 1 );
                 LOGGER.info("Executed Step = INCREASEVAR,var_CountN,1,TGTYPESCREENREG");
    	}
          LOGGER.info("Executed Step = WEND,TGTYPESCREENREG");
    	resetAttemps();

    }


    @Test
    public void test2() {

        // No need to declare / print method name here, handled by TestNg listener
        // declare("test2");

        WAIT.$(5,"TGTYPESCREENREG");

		String var_dataToPass;
                 LOGGER.info("Executed Step = VAR,String,var_dataToPass,TGTYPESCREENREG");
        CALL.$("LoginGIAS","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_USERNAME","//input[@id='guestForm:username']","autoapi","FALSE","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_PASSWORD","//input[@id='guestForm:password']","Autoapi!23","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_LOGIN","//button[contains(.,'Login')]","TGTYPESCREENREG");

        WAIT.$("ELE_BUTTON_GIAS_LOGO","//img[@id='headerForm:headerLogoImage']","VISIBLE","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

        CALL.$("Test2EditPlanSetUP","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_GIAS_LOGO","//img[@id='headerForm:headerLogoImage']","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        SWIPE.$("DOWN","ELE_TXTBOX_SEARCHTASKS","//input[@id='workbenchForm:workbenchTabs:taskSearchContainer:taskSearchSearch']","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_SEARCHTASKS","//input[@id='workbenchForm:workbenchTabs:taskSearchContainer:taskSearchSearch']","Plan Setup","FALSE","TGTYPESCREENREG");

        WAIT.$(3,"TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SEARCHTASKS","//a[@id='workbenchForm:workbenchTabs:taskSearchContainer:tb_buttonSearch']//span[@class='fa fa-search fa-lg']","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        TAP.$("ELE_LINK_FIRST_TASKS","//button[@id='workbenchForm:workbenchTabs:taskSearchContainer:0:availableTask']","TGTYPESCREENREG");

        WAIT.$("ELE_TXTBOX_FILTER_PLANCODE","//input[@id='workbenchForm:workbenchTabs:grid:planCode_Col:filter']","VISIBLE","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_FILTER_PLANCODE","//input[@id='workbenchForm:workbenchTabs:grid:planCode_Col:filter']","OLDEN","FALSE","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        TAP.$("ELE_LINK_FIRST_PLANCODE","//span[@id='workbenchForm:workbenchTabs:grid:0:planCode']","TGTYPESCREENREG");

        WAIT.$("ELE_BUTTON_EDIT","//button[@id='workbenchForm:workbenchTabs:tb_buttonUpdate']","VISIBLE","TGTYPESCREENREG");

        TAP.$("ELE_LINK_DIVIDENDS","//span[contains(.,'Dividends')]","TGTYPESCREENREG");

        WAIT.$(3,"TGTYPESCREENREG");

        LOGGER.info("Executed Step = START IF");
        if (check("ELE_GETTEXT_LABEL_PURCHASEONEYEARTERMFACEAMOUNTLIMIT","//span[@id='workbenchForm:workbenchTabs:purchase1YearTermFAAllowed']","<>","No","TGTYPESCREENREG")) {
         LOGGER.info("Executed Step = IF,ELE_GETTEXT_LABEL_PURCHASEONEYEARTERMFACEAMOUNTLIMIT,<>,No,TGTYPESCREENREG");
        TAP.$("ELE_BUTTON_EDIT","//button[@id='workbenchForm:workbenchTabs:tb_buttonUpdate']","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        TAP.$("ELE_DRPDWN_PURCHASE1YEARTERMFAALLOWED","//*[@id=\"workbenchForm:workbenchTabs:purchase1YearTermFAAllowed_label\"]","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

		var_dataToPass = "No";
                 LOGGER.info("Executed Step = STORE,var_dataToPass,No,TGTYPESCREENREG");
		try { 
		 	Thread.sleep(1000);
					for (int i = 0; i < 2000; i++){
						WebElement selectValueOfDropDown= driver.findElement(By.xpath("//li[@id='workbenchForm:workbenchTabs:purchase1YearTermFAAllowed_"+ i +"']"));

						if (selectValueOfDropDown.getText().contains(var_dataToPass)){

		                                selectValueOfDropDown.click();
							AddToReport("ASSERT," + selectValueOfDropDown.getText() +", CONTAINS,"+var_dataToPass);
						
							break;
						}
					}
					takeScreenshot();

					Thread.sleep(2000);
		 
		 ;} 
		 catch(Exception e) { PrintVal("Renderer Script Exception : "+e.toString()); } 
		 PrintVal("Executed Step = CALLRENDERERSCRIPT,SELECT_VALUE_FROM_PURCHASE1YEARTERMFACEAMOUNTALLOWED"); 
        WAIT.$(5,"TGTYPESCREENREG");

        TAP.$("ELE_DRPDWN_PURCHASE1YEARTERMFAALLOWED","//*[@id=\"workbenchForm:workbenchTabs:purchase1YearTermFAAllowed_label\"]","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SAVE","//span[contains(text(),'Save')]","TGTYPESCREENREG");

        }
          LOGGER.info("Executed Step = ENDIF,TGTYPESCREENREG");
        WAIT.$(5,"TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_GIAS_LOGO","//img[@id='headerForm:headerLogoImage']","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        CALL.$("Test2ChangePolicyContract","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_GIAS_LOGO","//img[@id='headerForm:headerLogoImage']","TGTYPESCREENREG");

        SWIPE.$("DOWN","ELE_TXTBOX_SEARCHTASKS","//input[@id='workbenchForm:workbenchTabs:taskSearchContainer:taskSearchSearch']","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_SEARCHTASKS","//input[@id='workbenchForm:workbenchTabs:taskSearchContainer:taskSearchSearch']","Change a Policy Contract","FALSE","TGTYPESCREENREG");

        WAIT.$(3,"TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SEARCHTASKS","//a[@id='workbenchForm:workbenchTabs:taskSearchContainer:tb_buttonSearch']//span[@class='fa fa-search fa-lg']","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        TAP.$("ELE_LINK_FIRST_TASKS","//button[@id='workbenchForm:workbenchTabs:taskSearchContainer:0:availableTask']","TGTYPESCREENREG");

        WAIT.$("ELE_TXTBOX_POLICYNUMBER","//input[@id='workbenchForm:workbenchTabs:policyNumber']","VISIBLE","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_POLICYNUMBER","//input[@id='workbenchForm:workbenchTabs:policyNumber']","SGD-62","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_CONTINUE","//span[contains(text(),'Continue')]","TGTYPESCREENREG");

        WAIT.$("ELE_BUTTON_SUBMIT","//span[contains(text(),'Submit')]","VISIBLE","TGTYPESCREENREG");

        SWIPE.$("UP","ELE_DRPDWN_PRIMARYDIVIDENDOPTION","//*[@id=\"workbenchForm:workbenchTabs:dividendOptionPrim_label\"]","TGTYPESCREENREG");

        TAP.$("ELE_DRPDWN_PRIMARYDIVIDENDOPTION","//*[@id=\"workbenchForm:workbenchTabs:dividendOptionPrim_label\"]","TGTYPESCREENREG");

		var_dataToPass = "Purchase One Year Term (Face Amount Limit)";
                 LOGGER.info("Executed Step = STORE,var_dataToPass,Purchase One Year Term (Face Amount Limit),TGTYPESCREENREG");
		try { 
		 //*[@id="workbenchForm:workbenchTabs:dividendOptionPrim_4"]


			Thread.sleep(1000);
					for (int i = 0; i < 2000; i++){
						WebElement selectValueOfDropDown= driver.findElement(By.xpath("//li[@id='workbenchForm:workbenchTabs:dividendOptionPrim_"+ i +"']"));

						if (selectValueOfDropDown.getText().contains(var_dataToPass)){

		                                selectValueOfDropDown.click();
							AddToReport("ASSERT," + selectValueOfDropDown.getText() +", CONTAINS,"+var_dataToPass);
						
							break;
						}
					}
					takeScreenshot();

					Thread.sleep(2000);
		 
		 ;} 
		 catch(Exception e) { PrintVal("Renderer Script Exception : "+e.toString()); } 
		 PrintVal("Executed Step = CALLRENDERERSCRIPT,SELECT_VALUE_FROM_PRIMARY_DIVIDEND_OPETION"); 
        WAIT.$(5,"TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_VALIDATE","//*[@id=\"workbenchForm:workbenchTabs:tb_buttonEdit\"]","TGTYPESCREENREG");

        WAIT.$(10,"TGTYPESCREENREG");

        WAIT.$("ELE_MESSAGE_ERROR","//span[@class='ui-message-error-detail']","VISIBLE","TGTYPESCREENREG");

        SWIPE.$("UP","ELE_MESSAGE_ERROR","//span[@class='ui-message-error-detail']","TGTYPESCREENREG");

        SWIPE.$("UP","TGTYPESCREENREG");

        ASSERT.$("ELE_MESSAGE_ERROR","//span[@class='ui-message-error-detail']","CONTAINS","Primary dividend option not valid","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_GIAS_LOGO","//img[@id='headerForm:headerLogoImage']","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");


    }


    @Test
    public void test3() {

        // No need to declare / print method name here, handled by TestNg listener
        // declare("test3");

        WAIT.$(5,"TGTYPESCREENREG");

		String var_dataToPass;
                 LOGGER.info("Executed Step = VAR,String,var_dataToPass,TGTYPESCREENREG");
        CALL.$("LoginGIAS","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_USERNAME","//input[@id='guestForm:username']","autoapi","FALSE","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_PASSWORD","//input[@id='guestForm:password']","Autoapi!23","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_LOGIN","//button[contains(.,'Login')]","TGTYPESCREENREG");

        WAIT.$("ELE_BUTTON_GIAS_LOGO","//img[@id='headerForm:headerLogoImage']","VISIBLE","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

        CALL.$("Test3VerifyPrimaryDiviend","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_GIAS_LOGO","//img[@id='headerForm:headerLogoImage']","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        SWIPE.$("DOWN","ELE_TXTBOX_SEARCHTASKS","//input[@id='workbenchForm:workbenchTabs:taskSearchContainer:taskSearchSearch']","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_SEARCHTASKS","//input[@id='workbenchForm:workbenchTabs:taskSearchContainer:taskSearchSearch']","New Policy Application","FALSE","TGTYPESCREENREG");

        WAIT.$(3,"TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SEARCHTASKS","//a[@id='workbenchForm:workbenchTabs:taskSearchContainer:tb_buttonSearch']//span[@class='fa fa-search fa-lg']","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        TAP.$("ELE_LINK_FIRST_TASKS","//button[@id='workbenchForm:workbenchTabs:taskSearchContainer:0:availableTask']","TGTYPESCREENREG");

        WAIT.$("ELE_TXTBOX_POLICYNUMBER_FILTER","//input[@id='workbenchForm:workbenchTabs:grid:policyNumber_Col:filter']","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_POLICYNUMBER_FILTER","//input[@id='workbenchForm:workbenchTabs:grid:policyNumber_Col:filter']","ULTM03","FALSE","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

		var_dataToPass = "ULTM03";
                 LOGGER.info("Executed Step = STORE,var_dataToPass,ULTM03,TGTYPESCREENREG");
		try { 
		 int totalRowCount = 0;

					Boolean isComplete = false;
					while (isComplete == false){
						List<WebElement> eleSupplementalCode = driver.findElements(By.xpath("//span[@id='workbenchForm:workbenchTabs:grid:"+totalRowCount+":policyNumber']"));

						if (eleSupplementalCode.size()>0){
							totalRowCount = totalRowCount + 1;
						} else {
							isComplete = true;
						}
					}
					for (int i = 0; i <= totalRowCount; i++){

						WebElement elepolicyNumber = driver.findElement(By.xpath("//span[@id='workbenchForm:workbenchTabs:grid:"+i+":policyNumber']"));

						String strpolicyNumber = elepolicyNumber.getText();

						if(elepolicyNumber.getText().contains(var_dataToPass) ){
							elepolicyNumber.click();
		                                 AddToReport("ASSERT," + elepolicyNumber.getText() +", CONTAINS,"+var_dataToPass);
							break;
						}

					}
		 
		 ;} 
		 catch(Exception e) { PrintVal("Renderer Script Exception : "+e.toString()); } 
		 PrintVal("Executed Step = CALLRENDERERSCRIPT,SELECT_POLICYNUMBER_FROM_TABLE"); 
        SWIPE.$("UP","ELE_LINK_BASE_RIDERCODE","//span[@id='workbenchForm:workbenchTabs:benefitGrid:0:baseRiderCode']","TGTYPESCREENREG");

        WAIT.$(3,"TGTYPESCREENREG");

        TAP.$("ELE_LINK_BASE_RIDERCODE","//span[@id='workbenchForm:workbenchTabs:benefitGrid:0:baseRiderCode']","TGTYPESCREENREG");

        WAIT.$("ELE_BUTTON_EDIT","//button[@id='workbenchForm:workbenchTabs:tb_buttonUpdate']","VISIBLE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_EDIT","//button[@id='workbenchForm:workbenchTabs:tb_buttonUpdate']","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        SWIPE.$("UP","ELE_DRPDWN_PRIMARYDIVIDENDOPETION_POLICY","//label[@id='workbenchForm:workbenchTabs:primaryDividendOption_label']","TGTYPESCREENREG");

        TAP.$("ELE_DRPDWN_PRIMARYDIVIDENDOPETION_POLICY","//label[@id='workbenchForm:workbenchTabs:primaryDividendOption_label']","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

		var_dataToPass = "Purchase One Year Term (Cash Value Limit)";
                 LOGGER.info("Executed Step = STORE,var_dataToPass,Purchase One Year Term (Cash Value Limit),TGTYPESCREENREG");
		try { 
		 //li[@id='workbenchForm:workbenchTabs:primaryDividendOption_2']

			Thread.sleep(1000);
					for (int i = 0; i < 2000; i++){
						WebElement selectValueOfDropDown= driver.findElement(By.xpath("//li[@id='workbenchForm:workbenchTabs:primaryDividendOption_"+ i +"']"));

						if (selectValueOfDropDown.getText().contains(var_dataToPass)){

		                              //  selectValueOfDropDown.click();
							AddToReport("ASSERT," + selectValueOfDropDown.getText() +", CONTAINS,"+var_dataToPass);
						
							break;
						}
					}
					takeScreenshot();

					Thread.sleep(2000);
		 
		 ;} 
		 catch(Exception e) { PrintVal("Renderer Script Exception : "+e.toString()); } 
		 PrintVal("Executed Step = CALLRENDERERSCRIPT,CHECK_PRIMARY_DIVIDEND_OPETION_POLICY"); 
        WAIT.$(5,"TGTYPESCREENREG");

		var_dataToPass = "Purchase One Year Term (Face Amount Limit)";
                 LOGGER.info("Executed Step = STORE,var_dataToPass,Purchase One Year Term (Face Amount Limit),TGTYPESCREENREG");
		try { 
		 //li[@id='workbenchForm:workbenchTabs:primaryDividendOption_2']

			Thread.sleep(1000);
					for (int i = 0; i < 2000; i++){
						WebElement selectValueOfDropDown= driver.findElement(By.xpath("//li[@id='workbenchForm:workbenchTabs:primaryDividendOption_"+ i +"']"));

						if (selectValueOfDropDown.getText().contains(var_dataToPass)){

		                              //  selectValueOfDropDown.click();
							AddToReport("ASSERT," + selectValueOfDropDown.getText() +", CONTAINS,"+var_dataToPass);
						
							break;
						}
					}
					takeScreenshot();

					Thread.sleep(2000);
		 
		 ;} 
		 catch(Exception e) { PrintVal("Renderer Script Exception : "+e.toString()); } 
		 PrintVal("Executed Step = CALLRENDERERSCRIPT,CHECK_PRIMARY_DIVIDEND_OPETION_POLICY"); 
        WAIT.$(5,"TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_GIAS_LOGO","//img[@id='headerForm:headerLogoImage']","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        CALL.$("Test3ChangePolicyContract","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_GIAS_LOGO","//img[@id='headerForm:headerLogoImage']","TGTYPESCREENREG");

        SWIPE.$("DOWN","ELE_TXTBOX_SEARCHTASKS","//input[@id='workbenchForm:workbenchTabs:taskSearchContainer:taskSearchSearch']","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_SEARCHTASKS","//input[@id='workbenchForm:workbenchTabs:taskSearchContainer:taskSearchSearch']","Change a Policy Contract","FALSE","TGTYPESCREENREG");

        WAIT.$(3,"TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SEARCHTASKS","//a[@id='workbenchForm:workbenchTabs:taskSearchContainer:tb_buttonSearch']//span[@class='fa fa-search fa-lg']","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        TAP.$("ELE_LINK_FIRST_TASKS","//button[@id='workbenchForm:workbenchTabs:taskSearchContainer:0:availableTask']","TGTYPESCREENREG");

        WAIT.$("ELE_TXTBOX_POLICYNUMBER","//input[@id='workbenchForm:workbenchTabs:policyNumber']","VISIBLE","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_POLICYNUMBER","//input[@id='workbenchForm:workbenchTabs:policyNumber']","SGD-62","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_CONTINUE","//span[contains(text(),'Continue')]","TGTYPESCREENREG");

        WAIT.$("ELE_BUTTON_SUBMIT","//span[contains(text(),'Submit')]","VISIBLE","TGTYPESCREENREG");

        SWIPE.$("UP","ELE_DRPDWN_PRIMARYDIVIDENDOPTION","//*[@id=\"workbenchForm:workbenchTabs:dividendOptionPrim_label\"]","TGTYPESCREENREG");

        TAP.$("ELE_DRPDWN_PRIMARYDIVIDENDOPTION","//*[@id=\"workbenchForm:workbenchTabs:dividendOptionPrim_label\"]","TGTYPESCREENREG");

		var_dataToPass = "Purchase One Year Term (Cash Value Limit)";
                 LOGGER.info("Executed Step = STORE,var_dataToPass,Purchase One Year Term (Cash Value Limit),TGTYPESCREENREG");
		try { 
		 //*[@id="workbenchForm:workbenchTabs:dividendOptionPrim_4"]


			Thread.sleep(1000);
					for (int i = 0; i < 2000; i++){
						WebElement selectValueOfDropDown= driver.findElement(By.xpath("//li[@id='workbenchForm:workbenchTabs:dividendOptionPrim_"+ i +"']"));

						if (selectValueOfDropDown.getText().contains(var_dataToPass)){

		     
							AddToReport("ASSERT," + selectValueOfDropDown.getText() +", CONTAINS,"+var_dataToPass);
						
							break;
						}
					}
					takeScreenshot();

					Thread.sleep(2000);
		 
		 ;} 
		 catch(Exception e) { PrintVal("Renderer Script Exception : "+e.toString()); } 
		 PrintVal("Executed Step = CALLRENDERERSCRIPT,VERIFY_PRIMARY_DIVIDEND_OPTION_POLICYCONTRACT"); 
        WAIT.$(5,"TGTYPESCREENREG");

		var_dataToPass = "Purchase One Year Term (Face Amount Limit)";
                 LOGGER.info("Executed Step = STORE,var_dataToPass,Purchase One Year Term (Face Amount Limit),TGTYPESCREENREG");
		try { 
		 //*[@id="workbenchForm:workbenchTabs:dividendOptionPrim_4"]


			Thread.sleep(1000);
					for (int i = 0; i < 2000; i++){
						WebElement selectValueOfDropDown= driver.findElement(By.xpath("//li[@id='workbenchForm:workbenchTabs:dividendOptionPrim_"+ i +"']"));

						if (selectValueOfDropDown.getText().contains(var_dataToPass)){

		     
							AddToReport("ASSERT," + selectValueOfDropDown.getText() +", CONTAINS,"+var_dataToPass);
						
							break;
						}
					}
					takeScreenshot();

					Thread.sleep(2000);
		 
		 ;} 
		 catch(Exception e) { PrintVal("Renderer Script Exception : "+e.toString()); } 
		 PrintVal("Executed Step = CALLRENDERERSCRIPT,VERIFY_PRIMARY_DIVIDEND_OPTION_POLICYCONTRACT"); 
        WAIT.$(5,"TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_GIAS_LOGO","//img[@id='headerForm:headerLogoImage']","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");


    }


    @Test
    public void testnw048() {

        // No need to declare / print method name here, handled by TestNg listener
        // declare("testnw048");

		String var_dataToPass;
                 LOGGER.info("Executed Step = VAR,String,var_dataToPass,TGTYPESCREENREG");
        CALL.$("LoginGIAS","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_USERNAME","//input[@id='guestForm:username']","autoapi","FALSE","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_PASSWORD","//input[@id='guestForm:password']","Autoapi!23","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_LOGIN","//button[contains(.,'Login')]","TGTYPESCREENREG");

        WAIT.$("ELE_BUTTON_GIAS_LOGO","//img[@id='headerForm:headerLogoImage']","VISIBLE","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

        CALL.$("Test1UpdateExistingPlanCode","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_GIAS_LOGO","//img[@id='headerForm:headerLogoImage']","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        SWIPE.$("DOWN","ELE_TXTBOX_SEARCHTASKS","//input[@id='workbenchForm:workbenchTabs:taskSearchContainer:taskSearchSearch']","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_SEARCHTASKS","//input[@id='workbenchForm:workbenchTabs:taskSearchContainer:taskSearchSearch']","Plan Setup","FALSE","TGTYPESCREENREG");

        WAIT.$(3,"TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SEARCHTASKS","//a[@id='workbenchForm:workbenchTabs:taskSearchContainer:tb_buttonSearch']//span[@class='fa fa-search fa-lg']","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        TAP.$("ELE_LINK_FIRST_TASKS","//button[@id='workbenchForm:workbenchTabs:taskSearchContainer:0:availableTask']","TGTYPESCREENREG");

        WAIT.$("ELE_TXTBOX_FILTER_PLANCODE","//input[@id='workbenchForm:workbenchTabs:grid:planCode_Col:filter']","VISIBLE","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_FILTER_PLANCODE","//input[@id='workbenchForm:workbenchTabs:grid:planCode_Col:filter']","OLUSD2","FALSE","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        TAP.$("ELE_LINK_FIRST_PLANCODE","//span[@id='workbenchForm:workbenchTabs:grid:0:planCode']","TGTYPESCREENREG");

        WAIT.$("ELE_BUTTON_EDIT","//button[@id='workbenchForm:workbenchTabs:tb_buttonUpdate']","VISIBLE","TGTYPESCREENREG");

        TAP.$("ELE_LINK_DIVIDENDS","//span[contains(.,'Dividends')]","TGTYPESCREENREG");

        WAIT.$(3,"TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_EDIT","//button[@id='workbenchForm:workbenchTabs:tb_buttonUpdate']","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        SWIPE.$("UP","ELE_DRPDWN_DEFAULTOPETION1","//label[@id=\\\"workbenchForm:workbenchTabs:defaultOption1_label\\\"]","TGTYPESCREENREG");

        TAP.$("ELE_DRPDWN_DEFAULTOPETION1","//label[@id=\\\"workbenchForm:workbenchTabs:defaultOption1_label\\\"]","TGTYPESCREENREG");

		var_dataToPass = "Purchase One Year Term (Cash Value Limit)";
                 LOGGER.info("Executed Step = STORE,var_dataToPass,Purchase One Year Term (Cash Value Limit),TGTYPESCREENREG");
		try { 
		 			Thread.sleep(1000);
					for (int i = 0; i < 2000; i++){
						WebElement selectValueOfDropDown = driver.findElement(By.xpath("//li[@id='workbenchForm:workbenchTabs:defaultOption1_"+ i +"']"));
		                         System.out.println("Execution time: "  + selectValueOfDropDown.getText());                
						if (selectValueOfDropDown.getText().contains(var_dataToPass)){

		                                
		AddToReport("ASSERT," + selectValueOfDropDown.getText() +", CONTAINS,"+var_dataToPass);
				
							break;
						}
					}
					takeScreenshot();

					Thread.sleep(2000);
		 
		 ;} 
		 catch(Exception e) { PrintVal("Renderer Script Exception : "+e.toString()); } 
		 PrintVal("Executed Step = CALLRENDERERSCRIPT,CHECK_DEFAULTOPTION1"); 
        WAIT.$(5,"TGTYPESCREENREG");

		var_dataToPass = "Purchase One Year Term (Face Amount Limit)";
                 LOGGER.info("Executed Step = STORE,var_dataToPass,Purchase One Year Term (Face Amount Limit),TGTYPESCREENREG");
		try { 
		 			Thread.sleep(1000);
					for (int i = 0; i < 2000; i++){
						WebElement selectValueOfDropDown = driver.findElement(By.xpath("//li[@id='workbenchForm:workbenchTabs:defaultOption1_"+ i +"']"));
		                         System.out.println("Execution time: "  + selectValueOfDropDown.getText());                
						if (selectValueOfDropDown.getText().contains(var_dataToPass)){

		                                
		AddToReport("ASSERT," + selectValueOfDropDown.getText() +", CONTAINS,"+var_dataToPass);
				
							break;
						}
					}
					takeScreenshot();

					Thread.sleep(2000);
		 
		 ;} 
		 catch(Exception e) { PrintVal("Renderer Script Exception : "+e.toString()); } 
		 PrintVal("Executed Step = CALLRENDERERSCRIPT,CHECK_DEFAULTOPTION1"); 
        WAIT.$(5,"TGTYPESCREENREG");

        TAP.$("ELE_DRPDWN_DEFAULTOPETION1","//label[@id=\\\"workbenchForm:workbenchTabs:defaultOption1_label\\\"]","TGTYPESCREENREG");

        SWIPE.$("DOWN","ELE_DRPDWN_PURCHASE1YEARTERMFAALLOWED","//*[@id=\\\"workbenchForm:workbenchTabs:purchase1YearTermFAAllowed_label\\\"]","TGTYPESCREENREG");

		var_dataToPass = "Yes";
                 LOGGER.info("Executed Step = STORE,var_dataToPass,Yes,TGTYPESCREENREG");
        TAP.$("ELE_DRPDWN_PURCHASE1YEARTERMALLOWED","//*[@id=\\\"workbenchForm:workbenchTabs:purchase1YearTermAllowed_label\\\"]","TGTYPESCREENREG");

		try { 
		 			Thread.sleep(1000);
					for (int i = 0; i < 2000; i++){
						WebElement selectValueOfDropDown= driver.findElement(By.xpath("//li[@id='workbenchForm:workbenchTabs:purchase1YearTermAllowed_"+ i +"']"));

						if (selectValueOfDropDown.getText().contains(var_dataToPass)){

		                                
							   AddToReport("ASSERT," + selectValueOfDropDown.getText() +", CONTAINS,"+var_dataToPass);
							//selectValueOfDropDown.click();
							break;
						}
					}
					takeScreenshot();

					Thread.sleep(2000);
		 
		 ;} 
		 catch(Exception e) { PrintVal("Renderer Script Exception : "+e.toString()); } 
		 PrintVal("Executed Step = CALLRENDERERSCRIPT,CHECK_DRPDWN_PURCHASE1YEARTERMALLOWED_CASH_AMOUNT"); 
		var_dataToPass = "No";
                 LOGGER.info("Executed Step = STORE,var_dataToPass,No,TGTYPESCREENREG");
		try { 
		 			Thread.sleep(1000);
					for (int i = 0; i < 2000; i++){
						WebElement selectValueOfDropDown= driver.findElement(By.xpath("//li[@id='workbenchForm:workbenchTabs:purchase1YearTermAllowed_"+ i +"']"));

						if (selectValueOfDropDown.getText().contains(var_dataToPass)){

		                                
							   AddToReport("ASSERT," + selectValueOfDropDown.getText() +", CONTAINS,"+var_dataToPass);
							//selectValueOfDropDown.click();
							break;
						}
					}
					takeScreenshot();

					Thread.sleep(2000);
		 
		 ;} 
		 catch(Exception e) { PrintVal("Renderer Script Exception : "+e.toString()); } 
		 PrintVal("Executed Step = CALLRENDERERSCRIPT,CHECK_DRPDWN_PURCHASE1YEARTERMALLOWED_CASH_AMOUNT"); 
        WAIT.$(5,"TGTYPESCREENREG");

        TAP.$("ELE_DRPDWN_PURCHASE1YEARTERMALLOWED","//*[@id=\\\"workbenchForm:workbenchTabs:purchase1YearTermAllowed_label\\\"]","TGTYPESCREENREG");

        TAP.$("ELE_DRPDWN_PURCHASE1YEARTERMFAALLOWED","//*[@id=\\\"workbenchForm:workbenchTabs:purchase1YearTermFAAllowed_label\\\"]","TGTYPESCREENREG");

		var_dataToPass = "Yes";
                 LOGGER.info("Executed Step = STORE,var_dataToPass,Yes,TGTYPESCREENREG");
		try { 
		 			Thread.sleep(1000);
					for (int i = 0; i < 2000; i++){
						WebElement selectValueOfDropDown= driver.findElement(By.xpath("//li[@id='workbenchForm:workbenchTabs:purchase1YearTermFAAllowed_"+ i +"']"));

						if (selectValueOfDropDown.getText().contains(var_dataToPass)){

		                                
							AddToReport("ASSERT," + selectValueOfDropDown.getText() +", CONTAINS,"+var_dataToPass);
						
							break;
						}
					}
					takeScreenshot();

					Thread.sleep(2000);
		 
		 ;} 
		 catch(Exception e) { PrintVal("Renderer Script Exception : "+e.toString()); } 
		 PrintVal("Executed Step = CALLRENDERERSCRIPT,CHECK_DRPDWN_PURCHASE1YEARTERMFAALLOWED_FACE_AMOUNT"); 
        WAIT.$(5,"TGTYPESCREENREG");

		var_dataToPass = "No";
                 LOGGER.info("Executed Step = STORE,var_dataToPass,No,TGTYPESCREENREG");
		try { 
		 			Thread.sleep(1000);
					for (int i = 0; i < 2000; i++){
						WebElement selectValueOfDropDown= driver.findElement(By.xpath("//li[@id='workbenchForm:workbenchTabs:purchase1YearTermFAAllowed_"+ i +"']"));

						if (selectValueOfDropDown.getText().contains(var_dataToPass)){

		                                
							AddToReport("ASSERT," + selectValueOfDropDown.getText() +", CONTAINS,"+var_dataToPass);
						
							break;
						}
					}
					takeScreenshot();

					Thread.sleep(2000);
		 
		 ;} 
		 catch(Exception e) { PrintVal("Renderer Script Exception : "+e.toString()); } 
		 PrintVal("Executed Step = CALLRENDERERSCRIPT,CHECK_DRPDWN_PURCHASE1YEARTERMFAALLOWED_FACE_AMOUNT"); 
        TAP.$("ELE_DRPDWN_PURCHASE1YEARTERMFAALLOWED","//*[@id=\\\"workbenchForm:workbenchTabs:purchase1YearTermFAAllowed_label\\\"]","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        SWIPE.$("DOWN","ELE_LINK_COUPONS","//span[@class='ui-menuitem-text'][contains(text(),'Coupons')]","TGTYPESCREENREG");

        SWIPE.$("DOWN","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        TAP.$("ELE_LINK_COUPONS","//span[@class='ui-menuitem-text'][contains(text(),'Coupons')]","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        SWIPE.$("DOWN","ELE_DRPDWN_PURCHASE1YEARTERMFAALLOWED","//*[@id=\\\"workbenchForm:workbenchTabs:purchase1YearTermFAAllowed_label\\\"]","TGTYPESCREENREG");

		var_dataToPass = "Yes";
                 LOGGER.info("Executed Step = STORE,var_dataToPass,Yes,TGTYPESCREENREG");
        TAP.$("ELE_DRPDWN_PURCHASE1YEARTERMALLOWED","//*[@id=\\\"workbenchForm:workbenchTabs:purchase1YearTermAllowed_label\\\"]","TGTYPESCREENREG");

		try { 
		 			Thread.sleep(1000);
					for (int i = 0; i < 2000; i++){
						WebElement selectValueOfDropDown= driver.findElement(By.xpath("//li[@id='workbenchForm:workbenchTabs:purchase1YearTermAllowed_"+ i +"']"));

						if (selectValueOfDropDown.getText().contains(var_dataToPass)){

		                                
							   AddToReport("ASSERT," + selectValueOfDropDown.getText() +", CONTAINS,"+var_dataToPass);
							//selectValueOfDropDown.click();
							break;
						}
					}
					takeScreenshot();

					Thread.sleep(2000);
		 
		 ;} 
		 catch(Exception e) { PrintVal("Renderer Script Exception : "+e.toString()); } 
		 PrintVal("Executed Step = CALLRENDERERSCRIPT,CHECK_DRPDWN_PURCHASE1YEARTERMALLOWED_CASH_AMOUNT"); 
        WAIT.$(5,"TGTYPESCREENREG");

		var_dataToPass = "No";
                 LOGGER.info("Executed Step = STORE,var_dataToPass,No,TGTYPESCREENREG");
		try { 
		 			Thread.sleep(1000);
					for (int i = 0; i < 2000; i++){
						WebElement selectValueOfDropDown= driver.findElement(By.xpath("//li[@id='workbenchForm:workbenchTabs:purchase1YearTermAllowed_"+ i +"']"));

						if (selectValueOfDropDown.getText().contains(var_dataToPass)){

		                                
							   AddToReport("ASSERT," + selectValueOfDropDown.getText() +", CONTAINS,"+var_dataToPass);
							//selectValueOfDropDown.click();
							break;
						}
					}
					takeScreenshot();

					Thread.sleep(2000);
		 
		 ;} 
		 catch(Exception e) { PrintVal("Renderer Script Exception : "+e.toString()); } 
		 PrintVal("Executed Step = CALLRENDERERSCRIPT,CHECK_DRPDWN_PURCHASE1YEARTERMALLOWED_CASH_AMOUNT"); 
        TAP.$("ELE_DRPDWN_PURCHASE1YEARTERMALLOWED","//*[@id=\\\"workbenchForm:workbenchTabs:purchase1YearTermAllowed_label\\\"]","TGTYPESCREENREG");

        TAP.$("ELE_DRPDWN_PURCHASE1YEARTERMFAALLOWED","//*[@id=\\\"workbenchForm:workbenchTabs:purchase1YearTermFAAllowed_label\\\"]","TGTYPESCREENREG");

		var_dataToPass = "Yes";
                 LOGGER.info("Executed Step = STORE,var_dataToPass,Yes,TGTYPESCREENREG");
		try { 
		 			Thread.sleep(1000);
					for (int i = 0; i < 2000; i++){
						WebElement selectValueOfDropDown= driver.findElement(By.xpath("//li[@id='workbenchForm:workbenchTabs:purchase1YearTermFAAllowed_"+ i +"']"));

						if (selectValueOfDropDown.getText().contains(var_dataToPass)){

		                                
							AddToReport("ASSERT," + selectValueOfDropDown.getText() +", CONTAINS,"+var_dataToPass);
						
							break;
						}
					}
					takeScreenshot();

					Thread.sleep(2000);
		 
		 ;} 
		 catch(Exception e) { PrintVal("Renderer Script Exception : "+e.toString()); } 
		 PrintVal("Executed Step = CALLRENDERERSCRIPT,CHECK_DRPDWN_PURCHASE1YEARTERMFAALLOWED_FACE_AMOUNT"); 
		var_dataToPass = "No";
                 LOGGER.info("Executed Step = STORE,var_dataToPass,No,TGTYPESCREENREG");
		try { 
		 			Thread.sleep(1000);
					for (int i = 0; i < 2000; i++){
						WebElement selectValueOfDropDown= driver.findElement(By.xpath("//li[@id='workbenchForm:workbenchTabs:purchase1YearTermFAAllowed_"+ i +"']"));

						if (selectValueOfDropDown.getText().contains(var_dataToPass)){

		                                
							AddToReport("ASSERT," + selectValueOfDropDown.getText() +", CONTAINS,"+var_dataToPass);
						
							break;
						}
					}
					takeScreenshot();

					Thread.sleep(2000);
		 
		 ;} 
		 catch(Exception e) { PrintVal("Renderer Script Exception : "+e.toString()); } 
		 PrintVal("Executed Step = CALLRENDERERSCRIPT,CHECK_DRPDWN_PURCHASE1YEARTERMFAALLOWED_FACE_AMOUNT"); 
        WAIT.$(5,"TGTYPESCREENREG");

        TAP.$("ELE_DRPDWN_PURCHASE1YEARTERMFAALLOWED","//*[@id=\\\"workbenchForm:workbenchTabs:purchase1YearTermFAAllowed_label\\\"]","TGTYPESCREENREG");

        SWIPE.$("UP","ELE_DRPDWN_DEFAULTOPETION1","//label[@id=\\\"workbenchForm:workbenchTabs:defaultOption1_label\\\"]","TGTYPESCREENREG");

        TAP.$("ELE_DRPDWN_DEFAULTOPETION1","//label[@id=\\\"workbenchForm:workbenchTabs:defaultOption1_label\\\"]","TGTYPESCREENREG");

        WAIT.$(3,"TGTYPESCREENREG");

		var_dataToPass = "Purchase One Year Term (Cash Value Limit)";
                 LOGGER.info("Executed Step = STORE,var_dataToPass,Purchase One Year Term (Cash Value Limit),TGTYPESCREENREG");
		try { 
		 			Thread.sleep(1000);
					for (int i = 0; i < 2000; i++){
						WebElement selectValueOfDropDown = driver.findElement(By.xpath("//li[@id='workbenchForm:workbenchTabs:defaultOption1_"+ i +"']"));
		                         System.out.println("Execution time: "  + selectValueOfDropDown.getText());                
						if (selectValueOfDropDown.getText().contains(var_dataToPass)){

		                                
		AddToReport("ASSERT," + selectValueOfDropDown.getText() +", CONTAINS,"+var_dataToPass);
				
							break;
						}
					}
					takeScreenshot();

					Thread.sleep(2000);
		 
		 ;} 
		 catch(Exception e) { PrintVal("Renderer Script Exception : "+e.toString()); } 
		 PrintVal("Executed Step = CALLRENDERERSCRIPT,CHECK_DEFAULTOPTION1"); 
        WAIT.$(5,"TGTYPESCREENREG");

		var_dataToPass = "Purchase One Year Term (Face Amount Limit)";
                 LOGGER.info("Executed Step = STORE,var_dataToPass,Purchase One Year Term (Face Amount Limit),TGTYPESCREENREG");
		try { 
		 			Thread.sleep(1000);
					for (int i = 0; i < 2000; i++){
						WebElement selectValueOfDropDown = driver.findElement(By.xpath("//li[@id='workbenchForm:workbenchTabs:defaultOption1_"+ i +"']"));
		                         System.out.println("Execution time: "  + selectValueOfDropDown.getText());                
						if (selectValueOfDropDown.getText().contains(var_dataToPass)){

		                                
		AddToReport("ASSERT," + selectValueOfDropDown.getText() +", CONTAINS,"+var_dataToPass);
				
							break;
						}
					}
					takeScreenshot();

					Thread.sleep(2000);
		 
		 ;} 
		 catch(Exception e) { PrintVal("Renderer Script Exception : "+e.toString()); } 
		 PrintVal("Executed Step = CALLRENDERERSCRIPT,CHECK_DEFAULTOPTION1"); 
        WAIT.$(5,"TGTYPESCREENREG");

        TAP.$("ELE_DRPDWN_DEFAULTOPETION1","//label[@id=\\\"workbenchForm:workbenchTabs:defaultOption1_label\\\"]","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        CALL.$("Test2EditPlanSetUP","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_GIAS_LOGO","//img[@id='headerForm:headerLogoImage']","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        SWIPE.$("DOWN","ELE_TXTBOX_SEARCHTASKS","//input[@id='workbenchForm:workbenchTabs:taskSearchContainer:taskSearchSearch']","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_SEARCHTASKS","//input[@id='workbenchForm:workbenchTabs:taskSearchContainer:taskSearchSearch']","Plan Setup","FALSE","TGTYPESCREENREG");

        WAIT.$(3,"TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SEARCHTASKS","//a[@id='workbenchForm:workbenchTabs:taskSearchContainer:tb_buttonSearch']//span[@class='fa fa-search fa-lg']","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        TAP.$("ELE_LINK_FIRST_TASKS","//button[@id='workbenchForm:workbenchTabs:taskSearchContainer:0:availableTask']","TGTYPESCREENREG");

        WAIT.$("ELE_TXTBOX_FILTER_PLANCODE","//input[@id='workbenchForm:workbenchTabs:grid:planCode_Col:filter']","VISIBLE","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_FILTER_PLANCODE","//input[@id='workbenchForm:workbenchTabs:grid:planCode_Col:filter']","OLDEN","FALSE","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        TAP.$("ELE_LINK_FIRST_PLANCODE","//span[@id='workbenchForm:workbenchTabs:grid:0:planCode']","TGTYPESCREENREG");

        WAIT.$("ELE_BUTTON_EDIT","//button[@id='workbenchForm:workbenchTabs:tb_buttonUpdate']","VISIBLE","TGTYPESCREENREG");

        TAP.$("ELE_LINK_DIVIDENDS","//span[contains(.,'Dividends')]","TGTYPESCREENREG");

        WAIT.$(3,"TGTYPESCREENREG");

        LOGGER.info("Executed Step = START IF");
        if (check("ELE_GETTEXT_LABEL_PURCHASEONEYEARTERMFACEAMOUNTLIMIT","//span[@id='workbenchForm:workbenchTabs:purchase1YearTermFAAllowed']","<>","No","TGTYPESCREENREG")) {
         LOGGER.info("Executed Step = IF,ELE_GETTEXT_LABEL_PURCHASEONEYEARTERMFACEAMOUNTLIMIT,<>,No,TGTYPESCREENREG");
        TAP.$("ELE_BUTTON_EDIT","//button[@id='workbenchForm:workbenchTabs:tb_buttonUpdate']","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        TAP.$("ELE_DRPDWN_PURCHASE1YEARTERMFAALLOWED","//*[@id=\\"workbenchForm:workbenchTabs:purchase1YearTermFAAllowed_label\\"]","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

		var_dataToPass = "No";
                 LOGGER.info("Executed Step = STORE,var_dataToPass,No,TGTYPESCREENREG");
		try { 
		 	Thread.sleep(1000);
					for (int i = 0; i < 2000; i++){
						WebElement selectValueOfDropDown= driver.findElement(By.xpath("//li[@id='workbenchForm:workbenchTabs:purchase1YearTermFAAllowed_"+ i +"']"));

						if (selectValueOfDropDown.getText().contains(var_dataToPass)){

		                                selectValueOfDropDown.click();
							AddToReport("ASSERT," + selectValueOfDropDown.getText() +", CONTAINS,"+var_dataToPass);
						
							break;
						}
					}
					takeScreenshot();

					Thread.sleep(2000);
		 
		 ;} 
		 catch(Exception e) { PrintVal("Renderer Script Exception : "+e.toString()); } 
		 PrintVal("Executed Step = CALLRENDERERSCRIPT,SELECT_VALUE_FROM_PURCHASE1YEARTERMFACEAMOUNTALLOWED"); 
        WAIT.$(5,"TGTYPESCREENREG");

        TAP.$("ELE_DRPDWN_PURCHASE1YEARTERMFAALLOWED","//*[@id=\\"workbenchForm:workbenchTabs:purchase1YearTermFAAllowed_label\\"]","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SAVE","//span[contains(text(),'Save')]","TGTYPESCREENREG");

        }
          LOGGER.info("Executed Step = ENDIF,TGTYPESCREENREG");
        WAIT.$(5,"TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_GIAS_LOGO","//img[@id='headerForm:headerLogoImage']","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        CALL.$("Test2ChangePolicyContract","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_GIAS_LOGO","//img[@id='headerForm:headerLogoImage']","TGTYPESCREENREG");

        SWIPE.$("DOWN","ELE_TXTBOX_SEARCHTASKS","//input[@id='workbenchForm:workbenchTabs:taskSearchContainer:taskSearchSearch']","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_SEARCHTASKS","//input[@id='workbenchForm:workbenchTabs:taskSearchContainer:taskSearchSearch']","Change a Policy Contract","FALSE","TGTYPESCREENREG");

        WAIT.$(3,"TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SEARCHTASKS","//a[@id='workbenchForm:workbenchTabs:taskSearchContainer:tb_buttonSearch']//span[@class='fa fa-search fa-lg']","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        TAP.$("ELE_LINK_FIRST_TASKS","//button[@id='workbenchForm:workbenchTabs:taskSearchContainer:0:availableTask']","TGTYPESCREENREG");

        WAIT.$("ELE_TXTBOX_POLICYNUMBER","//input[@id='workbenchForm:workbenchTabs:policyNumber']","VISIBLE","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_POLICYNUMBER","//input[@id='workbenchForm:workbenchTabs:policyNumber']","SGD-62","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_CONTINUE","//span[contains(text(),'Continue')]","TGTYPESCREENREG");

        WAIT.$("ELE_BUTTON_SUBMIT","//span[contains(text(),'Submit')]","VISIBLE","TGTYPESCREENREG");

        SWIPE.$("UP","ELE_DRPDWN_PRIMARYDIVIDENDOPTION","//*[@id=\\"workbenchForm:workbenchTabs:dividendOptionPrim_label\\"]","TGTYPESCREENREG");

        TAP.$("ELE_DRPDWN_PRIMARYDIVIDENDOPTION","//*[@id=\\"workbenchForm:workbenchTabs:dividendOptionPrim_label\\"]","TGTYPESCREENREG");

		var_dataToPass = "Purchase One Year Term (Face Amount Limit)";
                 LOGGER.info("Executed Step = STORE,var_dataToPass,Purchase One Year Term (Face Amount Limit),TGTYPESCREENREG");
		try { 
		 //*[@id="workbenchForm:workbenchTabs:dividendOptionPrim_4"]


			Thread.sleep(1000);
					for (int i = 0; i < 2000; i++){
						WebElement selectValueOfDropDown= driver.findElement(By.xpath("//li[@id='workbenchForm:workbenchTabs:dividendOptionPrim_"+ i +"']"));

						if (selectValueOfDropDown.getText().contains(var_dataToPass)){

		                                selectValueOfDropDown.click();
							AddToReport("ASSERT," + selectValueOfDropDown.getText() +", CONTAINS,"+var_dataToPass);
						
							break;
						}
					}
					takeScreenshot();

					Thread.sleep(2000);
		 
		 ;} 
		 catch(Exception e) { PrintVal("Renderer Script Exception : "+e.toString()); } 
		 PrintVal("Executed Step = CALLRENDERERSCRIPT,SELECT_VALUE_FROM_PRIMARY_DIVIDEND_OPETION"); 
        WAIT.$(5,"TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_VALIDATE","//*[@id=\\"workbenchForm:workbenchTabs:tb_buttonEdit\\"]","TGTYPESCREENREG");

        WAIT.$(10,"TGTYPESCREENREG");

        WAIT.$("ELE_MESSAGE_ERROR","//span[@class='ui-message-error-detail']","VISIBLE","TGTYPESCREENREG");

        SWIPE.$("UP","ELE_MESSAGE_ERROR","//span[@class='ui-message-error-detail']","TGTYPESCREENREG");

        SWIPE.$("UP","TGTYPESCREENREG");

        ASSERT.$("ELE_MESSAGE_ERROR","//span[@class='ui-message-error-detail']","CONTAINS","Primary dividend option not valid","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_GIAS_LOGO","//img[@id='headerForm:headerLogoImage']","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        CALL.$("Test3VerifyPrimaryDiviend","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_GIAS_LOGO","//img[@id='headerForm:headerLogoImage']","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        SWIPE.$("DOWN","ELE_TXTBOX_SEARCHTASKS","//input[@id='workbenchForm:workbenchTabs:taskSearchContainer:taskSearchSearch']","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_SEARCHTASKS","//input[@id='workbenchForm:workbenchTabs:taskSearchContainer:taskSearchSearch']","New Policy Application","FALSE","TGTYPESCREENREG");

        WAIT.$(3,"TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SEARCHTASKS","//a[@id='workbenchForm:workbenchTabs:taskSearchContainer:tb_buttonSearch']//span[@class='fa fa-search fa-lg']","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        TAP.$("ELE_LINK_FIRST_TASKS","//button[@id='workbenchForm:workbenchTabs:taskSearchContainer:0:availableTask']","TGTYPESCREENREG");

        WAIT.$("ELE_TXTBOX_POLICYNUMBER_FILTER","//input[@id='workbenchForm:workbenchTabs:grid:policyNumber_Col:filter']","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_POLICYNUMBER_FILTER","//input[@id='workbenchForm:workbenchTabs:grid:policyNumber_Col:filter']","ULTM03","FALSE","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

		var_dataToPass = "ULTM03";
                 LOGGER.info("Executed Step = STORE,var_dataToPass,ULTM03,TGTYPESCREENREG");
		try { 
		 int totalRowCount = 0;

					Boolean isComplete = false;
					while (isComplete == false){
						List<WebElement> eleSupplementalCode = driver.findElements(By.xpath("//span[@id='workbenchForm:workbenchTabs:grid:"+totalRowCount+":policyNumber']"));

						if (eleSupplementalCode.size()>0){
							totalRowCount = totalRowCount + 1;
						} else {
							isComplete = true;
						}
					}
					for (int i = 0; i <= totalRowCount; i++){

						WebElement elepolicyNumber = driver.findElement(By.xpath("//span[@id='workbenchForm:workbenchTabs:grid:"+i+":policyNumber']"));

						String strpolicyNumber = elepolicyNumber.getText();

						if(elepolicyNumber.getText().contains(var_dataToPass) ){
							elepolicyNumber.click();
		                                 AddToReport("ASSERT," + elepolicyNumber.getText() +", CONTAINS,"+var_dataToPass);
							break;
						}

					}
		 
		 ;} 
		 catch(Exception e) { PrintVal("Renderer Script Exception : "+e.toString()); } 
		 PrintVal("Executed Step = CALLRENDERERSCRIPT,SELECT_POLICYNUMBER_FROM_TABLE"); 
        SWIPE.$("UP","ELE_LINK_BASE_RIDERCODE","//span[@id='workbenchForm:workbenchTabs:benefitGrid:0:baseRiderCode']","TGTYPESCREENREG");

        WAIT.$(3,"TGTYPESCREENREG");

        TAP.$("ELE_LINK_BASE_RIDERCODE","//span[@id='workbenchForm:workbenchTabs:benefitGrid:0:baseRiderCode']","TGTYPESCREENREG");

        WAIT.$("ELE_BUTTON_EDIT","//button[@id='workbenchForm:workbenchTabs:tb_buttonUpdate']","VISIBLE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_EDIT","//button[@id='workbenchForm:workbenchTabs:tb_buttonUpdate']","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        SWIPE.$("UP","ELE_DRPDWN_PRIMARYDIVIDENDOPETION_POLICY","//label[@id='workbenchForm:workbenchTabs:primaryDividendOption_label']","TGTYPESCREENREG");

        TAP.$("ELE_DRPDWN_PRIMARYDIVIDENDOPETION_POLICY","//label[@id='workbenchForm:workbenchTabs:primaryDividendOption_label']","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

		var_dataToPass = "Purchase One Year Term (Cash Value Limit)";
                 LOGGER.info("Executed Step = STORE,var_dataToPass,Purchase One Year Term (Cash Value Limit),TGTYPESCREENREG");
		try { 
		 //li[@id='workbenchForm:workbenchTabs:primaryDividendOption_2']

			Thread.sleep(1000);
					for (int i = 0; i < 2000; i++){
						WebElement selectValueOfDropDown= driver.findElement(By.xpath("//li[@id='workbenchForm:workbenchTabs:primaryDividendOption_"+ i +"']"));

						if (selectValueOfDropDown.getText().contains(var_dataToPass)){

		                              //  selectValueOfDropDown.click();
							AddToReport("ASSERT," + selectValueOfDropDown.getText() +", CONTAINS,"+var_dataToPass);
						
							break;
						}
					}
					takeScreenshot();

					Thread.sleep(2000);
		 
		 ;} 
		 catch(Exception e) { PrintVal("Renderer Script Exception : "+e.toString()); } 
		 PrintVal("Executed Step = CALLRENDERERSCRIPT,CHECK_PRIMARY_DIVIDEND_OPETION_POLICY"); 
        WAIT.$(5,"TGTYPESCREENREG");

		var_dataToPass = "Purchase One Year Term (Face Amount Limit)";
                 LOGGER.info("Executed Step = STORE,var_dataToPass,Purchase One Year Term (Face Amount Limit),TGTYPESCREENREG");
		try { 
		 //li[@id='workbenchForm:workbenchTabs:primaryDividendOption_2']

			Thread.sleep(1000);
					for (int i = 0; i < 2000; i++){
						WebElement selectValueOfDropDown= driver.findElement(By.xpath("//li[@id='workbenchForm:workbenchTabs:primaryDividendOption_"+ i +"']"));

						if (selectValueOfDropDown.getText().contains(var_dataToPass)){

		                              //  selectValueOfDropDown.click();
							AddToReport("ASSERT," + selectValueOfDropDown.getText() +", CONTAINS,"+var_dataToPass);
						
							break;
						}
					}
					takeScreenshot();

					Thread.sleep(2000);
		 
		 ;} 
		 catch(Exception e) { PrintVal("Renderer Script Exception : "+e.toString()); } 
		 PrintVal("Executed Step = CALLRENDERERSCRIPT,CHECK_PRIMARY_DIVIDEND_OPETION_POLICY"); 
        WAIT.$(5,"TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_GIAS_LOGO","//img[@id='headerForm:headerLogoImage']","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        CALL.$("Test3ChangePolicyContract","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_GIAS_LOGO","//img[@id='headerForm:headerLogoImage']","TGTYPESCREENREG");

        SWIPE.$("DOWN","ELE_TXTBOX_SEARCHTASKS","//input[@id='workbenchForm:workbenchTabs:taskSearchContainer:taskSearchSearch']","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_SEARCHTASKS","//input[@id='workbenchForm:workbenchTabs:taskSearchContainer:taskSearchSearch']","Change a Policy Contract","FALSE","TGTYPESCREENREG");

        WAIT.$(3,"TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SEARCHTASKS","//a[@id='workbenchForm:workbenchTabs:taskSearchContainer:tb_buttonSearch']//span[@class='fa fa-search fa-lg']","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        TAP.$("ELE_LINK_FIRST_TASKS","//button[@id='workbenchForm:workbenchTabs:taskSearchContainer:0:availableTask']","TGTYPESCREENREG");

        WAIT.$("ELE_TXTBOX_POLICYNUMBER","//input[@id='workbenchForm:workbenchTabs:policyNumber']","VISIBLE","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_POLICYNUMBER","//input[@id='workbenchForm:workbenchTabs:policyNumber']","SGD-62","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_CONTINUE","//span[contains(text(),'Continue')]","TGTYPESCREENREG");

        WAIT.$("ELE_BUTTON_SUBMIT","//span[contains(text(),'Submit')]","VISIBLE","TGTYPESCREENREG");

        SWIPE.$("UP","ELE_DRPDWN_PRIMARYDIVIDENDOPTION","//*[@id=\\"workbenchForm:workbenchTabs:dividendOptionPrim_label\\"]","TGTYPESCREENREG");

        TAP.$("ELE_DRPDWN_PRIMARYDIVIDENDOPTION","//*[@id=\\"workbenchForm:workbenchTabs:dividendOptionPrim_label\\"]","TGTYPESCREENREG");

		var_dataToPass = "Purchase One Year Term (Cash Value Limit)";
                 LOGGER.info("Executed Step = STORE,var_dataToPass,Purchase One Year Term (Cash Value Limit),TGTYPESCREENREG");
		try { 
		 //*[@id="workbenchForm:workbenchTabs:dividendOptionPrim_4"]


			Thread.sleep(1000);
					for (int i = 0; i < 2000; i++){
						WebElement selectValueOfDropDown= driver.findElement(By.xpath("//li[@id='workbenchForm:workbenchTabs:dividendOptionPrim_"+ i +"']"));

						if (selectValueOfDropDown.getText().contains(var_dataToPass)){

		     
							AddToReport("ASSERT," + selectValueOfDropDown.getText() +", CONTAINS,"+var_dataToPass);
						
							break;
						}
					}
					takeScreenshot();

					Thread.sleep(2000);
		 
		 ;} 
		 catch(Exception e) { PrintVal("Renderer Script Exception : "+e.toString()); } 
		 PrintVal("Executed Step = CALLRENDERERSCRIPT,VERIFY_PRIMARY_DIVIDEND_OPTION_POLICYCONTRACT"); 
        WAIT.$(5,"TGTYPESCREENREG");

		var_dataToPass = "Purchase One Year Term (Face Amount Limit)";
                 LOGGER.info("Executed Step = STORE,var_dataToPass,Purchase One Year Term (Face Amount Limit),TGTYPESCREENREG");
		try { 
		 //*[@id="workbenchForm:workbenchTabs:dividendOptionPrim_4"]


			Thread.sleep(1000);
					for (int i = 0; i < 2000; i++){
						WebElement selectValueOfDropDown= driver.findElement(By.xpath("//li[@id='workbenchForm:workbenchTabs:dividendOptionPrim_"+ i +"']"));

						if (selectValueOfDropDown.getText().contains(var_dataToPass)){

		     
							AddToReport("ASSERT," + selectValueOfDropDown.getText() +", CONTAINS,"+var_dataToPass);
						
							break;
						}
					}
					takeScreenshot();

					Thread.sleep(2000);
		 
		 ;} 
		 catch(Exception e) { PrintVal("Renderer Script Exception : "+e.toString()); } 
		 PrintVal("Executed Step = CALLRENDERERSCRIPT,VERIFY_PRIMARY_DIVIDEND_OPTION_POLICYCONTRACT"); 
        WAIT.$(5,"TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_GIAS_LOGO","//img[@id='headerForm:headerLogoImage']","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");


    }


    @Test
    public void nw057updatehostsystemfunctionalityforpureendowmentswithasetlimitforautomaticnfo() {

        // No need to declare / print method name here, handled by TestNg listener
        // declare("nw057updatehostsystemfunctionalityforpureendowmentswithasetlimitforautomaticnfo");

        WAIT.$(5,"TGTYPESCREENREG");

		String var_dataToPass;
                 LOGGER.info("Executed Step = VAR,String,var_dataToPass,TGTYPESCREENREG");
        CALL.$("LoginGIAS","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_USERNAME","//input[@id='guestForm:username']","autoapi","FALSE","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_PASSWORD","//input[@id='guestForm:password']","Autoapi!23","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_LOGIN","//button[contains(.,'Login')]","TGTYPESCREENREG");

        WAIT.$("ELE_BUTTON_GIAS_LOGO","//img[@id='headerForm:headerLogoImage']","VISIBLE","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

        CALL.$("NW057SGD075VerifyPlanSetupNonforfeiture","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_GIAS_LOGO","//img[@id='headerForm:headerLogoImage']","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        SWIPE.$("DOWN","ELE_TXTBOX_SEARCHTASKS","//input[@id='workbenchForm:workbenchTabs:taskSearchContainer:taskSearchSearch']","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_SEARCHTASKS","//input[@id='workbenchForm:workbenchTabs:taskSearchContainer:taskSearchSearch']","Plan Setup","FALSE","TGTYPESCREENREG");

        WAIT.$(3,"TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SEARCHTASKS","//a[@id='workbenchForm:workbenchTabs:taskSearchContainer:tb_buttonSearch']//span[@class='fa fa-search fa-lg']","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        TAP.$("ELE_LINK_FIRST_TASKS","//button[@id='workbenchForm:workbenchTabs:taskSearchContainer:0:availableTask']","TGTYPESCREENREG");

        WAIT.$("ELE_TXTBOX_FILTER_PLANCODE","//input[@id='workbenchForm:workbenchTabs:grid:planCode_Col:filter']","VISIBLE","TGTYPESCREENREG");

		String var_PlanCode;
                 LOGGER.info("Executed Step = VAR,String,var_PlanCode,TGTYPESCREENREG");
		var_PlanCode = getJsonData(var_CSVData , "$.records["+var_Count+"].PlanCode");
                 LOGGER.info("Executed Step = STORE,var_PlanCode,var_CSVData,$.records[{var_Count}].PlanCode,TGTYPESCREENREG");
        TYPE.$("ELE_TXTBOX_FILTER_PLANCODE","//input[@id='workbenchForm:workbenchTabs:grid:planCode_Col:filter']",var_PlanCode,"FALSE","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        TAP.$("ELE_LINK_FIRST_PLANCODE","//span[@id='workbenchForm:workbenchTabs:grid:0:planCode']","TGTYPESCREENREG");

        WAIT.$("ELE_BUTTON_EDIT","//button[@id='workbenchForm:workbenchTabs:tb_buttonUpdate']","VISIBLE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_NONFORFEITURE","//span[@class='ui-menuitem-text'][contains(.,'Nonforfeiture')]","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        SWIPE.$("UP","ELE_LABEL_PUREENDOWMENTAMOUNTTOFORCEREVIEW","//label[@id='workbenchForm:workbenchTabs:pureEndowAmtReview_lbl']","TGTYPESCREENREG");

        ASSERT.$("ELE_LABEL_PUREENDOWMENTAMOUNTTOFORCEREVIEW","//label[@id='workbenchForm:workbenchTabs:pureEndowAmtReview_lbl']","CONTAINS","Pure Endowment Amount To Force Review","TGTYPESCREENREG");

        ASSERT.$("ELE_LABEL_VALUE_PUREENDOWMENTAMOUNTTOFORCEREVIEW","//span[@id='workbenchForm:workbenchTabs:pureEndowAmtReview']","CONTAINS",1,555,555,555,"555.00","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_GIAS_LOGO","//img[@id='headerForm:headerLogoImage']","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        CALL.$("NW0573SGD79NonforfeitureReviewProcess","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_GIAS_LOGO","//img[@id='headerForm:headerLogoImage']","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        SWIPE.$("DOWN","ELE_TXTBOX_SEARCHTASKS","//input[@id='workbenchForm:workbenchTabs:taskSearchContainer:taskSearchSearch']","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_SEARCHTASKS","//input[@id='workbenchForm:workbenchTabs:taskSearchContainer:taskSearchSearch']","Nonforfeiture Review","FALSE","TGTYPESCREENREG");

        WAIT.$(3,"TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SEARCHTASKS","//a[@id='workbenchForm:workbenchTabs:taskSearchContainer:tb_buttonSearch']//span[@class='fa fa-search fa-lg']","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        TAP.$("ELE_LINK_FIRST_TASKS","//button[@id='workbenchForm:workbenchTabs:taskSearchContainer:0:availableTask']","TGTYPESCREENREG");

        WAIT.$("ELE_TXTBOX_POLICYNUMBER_FILTER","//input[@id='workbenchForm:workbenchTabs:grid:policyNumber_Col:filter']","VISIBLE","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_POLICYNUMBER_FILTER","//input[@id='workbenchForm:workbenchTabs:grid:policyNumber_Col:filter']","NFOR3A","FALSE","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

		var_dataToPass = "NFOR3A";
                 LOGGER.info("Executed Step = STORE,var_dataToPass,NFOR3A,TGTYPESCREENREG");
		try { 
		 int totalRowCount = 0;

					Boolean isComplete = false;
					while (isComplete == false){
						List<WebElement> eleSupplementalCode = driver.findElements(By.xpath("//span[@id='workbenchForm:workbenchTabs:grid:"+totalRowCount+":policyNumber']"));

						if (eleSupplementalCode.size()>0){
							totalRowCount = totalRowCount + 1;
						} else {
							isComplete = true;
						}
					}
					for (int i = 0; i <= totalRowCount; i++){

						WebElement elepolicyNumber = driver.findElement(By.xpath("//span[@id='workbenchForm:workbenchTabs:grid:"+i+":policyNumber']"));

						String strpolicyNumber = elepolicyNumber.getText();

						if(elepolicyNumber.getText().contains(var_dataToPass) ){
							elepolicyNumber.click();
		                                 AddToReport("ASSERT," + elepolicyNumber.getText() +", CONTAINS,"+var_dataToPass);
							break;
						}

					}
		 
		 ;} 
		 catch(Exception e) { PrintVal("Renderer Script Exception : "+e.toString()); } 
		 PrintVal("Executed Step = CALLRENDERERSCRIPT,SELECT_POLICYNUMBER_FROM_TABLE"); 
        WAIT.$(5,"TGTYPESCREENREG");

        WAIT.$("ELE_MSG_ERROR_SUMMARY","//span[contains(@class,'ui-messages-error-summary')]","TGTYPESCREENREG");

        ASSERT.$("ELE_MSG_ERROR_SUMMARY","//span[contains(@class,'ui-messages-error-summary')]","CONTAINS","ETI cannot be calculated if cash value is zero. RC = 15","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_GIAS_LOGO","//img[@id='headerForm:headerLogoImage']","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        CALL.$("NW0575SGD82PureEndowentAmounttoForceReview","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_GIAS_LOGO","//img[@id='headerForm:headerLogoImage']","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        SWIPE.$("DOWN","ELE_TXTBOX_SEARCHTASKS","//input[@id='workbenchForm:workbenchTabs:taskSearchContainer:taskSearchSearch']","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_SEARCHTASKS","//input[@id='workbenchForm:workbenchTabs:taskSearchContainer:taskSearchSearch']","Plan Setup","FALSE","TGTYPESCREENREG");

        WAIT.$(3,"TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SEARCHTASKS","//a[@id='workbenchForm:workbenchTabs:taskSearchContainer:tb_buttonSearch']//span[@class='fa fa-search fa-lg']","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        TAP.$("ELE_LINK_FIRST_TASKS","//button[@id='workbenchForm:workbenchTabs:taskSearchContainer:0:availableTask']","TGTYPESCREENREG");

        WAIT.$("ELE_TXTBOX_FILTER_PLANCODE","//input[@id='workbenchForm:workbenchTabs:grid:planCode_Col:filter']","VISIBLE","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_FILTER_PLANCODE","//input[@id='workbenchForm:workbenchTabs:grid:planCode_Col:filter']","TST82","FALSE","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        TAP.$("ELE_LINK_FIRST_PLANCODE","//span[@id='workbenchForm:workbenchTabs:grid:0:planCode']","TGTYPESCREENREG");

        WAIT.$("ELE_BUTTON_EDIT","//button[@id='workbenchForm:workbenchTabs:tb_buttonUpdate']","VISIBLE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_NONFORFEITURE","//span[@class='ui-menuitem-text'][contains(.,'Nonforfeiture')]","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        SWIPE.$("UP","ELE_LABEL_PUREENDOWMENTAMOUNTTOFORCEREVIEW","//label[@id='workbenchForm:workbenchTabs:pureEndowAmtReview_lbl']","TGTYPESCREENREG");

        ASSERT.$("ELE_LABEL_PUREENDOWMENTAMOUNTTOFORCEREVIEW","//label[@id='workbenchForm:workbenchTabs:pureEndowAmtReview_lbl']","CONTAINS","Pure Endowment Amount To Force Review","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_GIAS_LOGO","//img[@id='headerForm:headerLogoImage']","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");


    }


    @Test
    public void tempnw057() {

        // No need to declare / print method name here, handled by TestNg listener
        // declare("tempnw057");

		String var_dataToPass;
                 LOGGER.info("Executed Step = VAR,String,var_dataToPass,TGTYPESCREENREG");
        CALL.$("LoginGIAS","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_USERNAME","//input[@id='guestForm:username']","autoapi","FALSE","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_PASSWORD","//input[@id='guestForm:password']","Autoapi!23","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_LOGIN","//button[contains(.,'Login')]","TGTYPESCREENREG");

        WAIT.$("ELE_BUTTON_GIAS_LOGO","//img[@id='headerForm:headerLogoImage']","VISIBLE","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

        CALL.$("NW057Test3VerifyPureEndowmentAmount","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_GIAS_LOGO","//img[@id='headerForm:headerLogoImage']","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        SWIPE.$("DOWN","ELE_TXTBOX_SEARCHTASKS","//input[@id='workbenchForm:workbenchTabs:taskSearchContainer:taskSearchSearch']","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_SEARCHTASKS","//input[@id='workbenchForm:workbenchTabs:taskSearchContainer:taskSearchSearch']","Plan Setup","FALSE","TGTYPESCREENREG");

        WAIT.$(3,"TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SEARCHTASKS","//a[@id='workbenchForm:workbenchTabs:taskSearchContainer:tb_buttonSearch']//span[@class='fa fa-search fa-lg']","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        TAP.$("ELE_LINK_FIRST_TASKS","//button[@id='workbenchForm:workbenchTabs:taskSearchContainer:0:availableTask']","TGTYPESCREENREG");

        WAIT.$("ELE_TXTBOX_FILTER_PLANCODE","//input[@id='workbenchForm:workbenchTabs:grid:planCode_Col:filter']","VISIBLE","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_FILTER_PLANCODE","//input[@id='workbenchForm:workbenchTabs:grid:planCode_Col:filter']","OLDEN1","FALSE","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        TAP.$("ELE_LINK_FIRST_PLANCODE","//span[@id='workbenchForm:workbenchTabs:grid:0:planCode']","TGTYPESCREENREG");

        WAIT.$("ELE_BUTTON_EDIT","//button[@id='workbenchForm:workbenchTabs:tb_buttonUpdate']","VISIBLE","TGTYPESCREENREG");

        SWIPE.$("UP","ELE_LABEL_AUTOMATIC_LAPSE_NFO","//label[@id='workbenchForm:workbenchTabs:automaticNFOProc_lbl']","TGTYPESCREENREG");

        ASSERT.$("ELE_LABEL_AUTOMATIC_LAPSE_NFO","//label[@id='workbenchForm:workbenchTabs:automaticNFOProc_lbl']","CONTAINS","Automatic Lapse/Nonforfeiture","TGTYPESCREENREG");

        ASSERT.$("ELE_LABEL__VALUE_AUTOMATIC_LAPSE_NFO","//span[@id='workbenchForm:workbenchTabs:automaticNFOProc']","CONTAINS","Yes","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        SWIPE.$("DOWN","ELE_BUTTON_NONFORFEITURE","//span[@class='ui-menuitem-text'][contains(.,'Nonforfeiture')]","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_NONFORFEITURE","//span[@class='ui-menuitem-text'][contains(.,'Nonforfeiture')]","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        LOGGER.info("Executed Step = START IF");
        if (check("ELE_LABEL_PUREENDOWMENTAMOUNTTOFORCEREVIEW","//label[@id='workbenchForm:workbenchTabs:pureEndowAmtReview_lbl']","INVISIBLE","TGTYPESCREENREG")) {
         LOGGER.info("Executed Step = IF,ELE_LABEL_PUREENDOWMENTAMOUNTTOFORCEREVIEW,INVISIBLE,TGTYPESCREENREG");
        TAP.$("ELE_BUTTON_FLAG","/html/body/div[1]/div[2]/div/form/div/div[2]/div/div[3]/div/div[1]/div[1]/button/span[1]","TGTYPESCREENREG");

        }
          LOGGER.info("Executed Step = ENDIF,TGTYPESCREENREG");
        SWIPE.$("UP","ELE_LABEL_PUREENDOWMENTAMOUNTTOFORCEREVIEW","//label[@id='workbenchForm:workbenchTabs:pureEndowAmtReview_lbl']","TGTYPESCREENREG");

        ASSERT.$("ELE_LABEL_PUREENDOWMENTAMOUNTTOFORCEREVIEW","//label[@id='workbenchForm:workbenchTabs:pureEndowAmtReview_lbl']","CONTAINS","Pure Endowment Amount To Force Review","TGTYPESCREENREG");

        ASSERT.$("ELE_LABEL_VALUE_PUREENDOWMENTAMOUNTTOFORCEREVIEW","//span[@id='workbenchForm:workbenchTabs:pureEndowAmtReview']","CONTAINS","0.00","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_GIAS_LOGO","//img[@id='headerForm:headerLogoImage']","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");


    }


    @Test
    public void nw057testplan() {

        // No need to declare / print method name here, handled by TestNg listener
        // declare("nw057testplan");

        CALL.$("LoginGIAS","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_USERNAME","//input[@id='guestForm:username']","autoapi","FALSE","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_PASSWORD","//input[@id='guestForm:password']","Autoapi!23","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_LOGIN","//button[contains(.,'Login')]","TGTYPESCREENREG");

        WAIT.$("ELE_BUTTON_GIAS_LOGO","//img[@id='headerForm:headerLogoImage']","VISIBLE","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

		String var_dataToPass;
                 LOGGER.info("Executed Step = VAR,String,var_dataToPass,TGTYPESCREENREG");
		int var_Count = 0;
                 LOGGER.info("Executed Step = VAR,Integer,var_Count,0,TGTYPESCREENREG");
		int var_CountN = 0;
                 LOGGER.info("Executed Step = VAR,Integer,var_CountN,0,TGTYPESCREENREG");
        LOGGER.info("Executed Step = REPEAT IF");
    	while (check(var_CountN,"<",1,"TGTYPESCREENREG")) {
          LOGGER.info("Executed Step = WHILE,var_CountN,<,1,TGTYPESCREENREG");
		String var_FileName = "http://10.1.104.229/json/20210525/y48BTv.json";
                 LOGGER.info("Executed Step = VAR,String,var_FileName,http://10.1.104.229/json/20210525/y48BTv.json,TGTYPESCREENREG");
		com.jayway.jsonpath.ReadContext var_CSVData = getJsonData("http://10.1.104.229/json/20210525/y48BTv.json");
                 LOGGER.info("Executed Step = VAR,Json,var_CSVData,http://10.1.104.229/json/20210525/y48BTv.json,TGTYPESCREENREG");
        CALL.$("NW057SGD075VerifyPlanSetupNonforfeiture","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_GIAS_LOGO","//img[@id='headerForm:headerLogoImage']","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        SWIPE.$("DOWN","ELE_TXTBOX_SEARCHTASKS","//input[@id='workbenchForm:workbenchTabs:taskSearchContainer:taskSearchSearch']","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_SEARCHTASKS","//input[@id='workbenchForm:workbenchTabs:taskSearchContainer:taskSearchSearch']","Plan Setup","FALSE","TGTYPESCREENREG");

        WAIT.$(3,"TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SEARCHTASKS","//a[@id='workbenchForm:workbenchTabs:taskSearchContainer:tb_buttonSearch']//span[@class='fa fa-search fa-lg']","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        TAP.$("ELE_LINK_FIRST_TASKS","//button[@id='workbenchForm:workbenchTabs:taskSearchContainer:0:availableTask']","TGTYPESCREENREG");

        WAIT.$("ELE_TXTBOX_FILTER_PLANCODE","//input[@id='workbenchForm:workbenchTabs:grid:planCode_Col:filter']","VISIBLE","TGTYPESCREENREG");

		String var_PlanCode;
                 LOGGER.info("Executed Step = VAR,String,var_PlanCode,TGTYPESCREENREG");
		var_PlanCode = getJsonData(var_CSVData , "$.records["+var_Count+"].PlanCode");
                 LOGGER.info("Executed Step = STORE,var_PlanCode,var_CSVData,$.records[{var_Count}].PlanCode,TGTYPESCREENREG");
        TYPE.$("ELE_TXTBOX_FILTER_PLANCODE","//input[@id='workbenchForm:workbenchTabs:grid:planCode_Col:filter']",var_PlanCode,"FALSE","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        TAP.$("ELE_LINK_FIRST_PLANCODE","//span[@id='workbenchForm:workbenchTabs:grid:0:planCode']","TGTYPESCREENREG");

        WAIT.$("ELE_BUTTON_EDIT","//button[@id='workbenchForm:workbenchTabs:tb_buttonUpdate']","VISIBLE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_NONFORFEITURE","//span[@class='ui-menuitem-text'][contains(.,'Nonforfeiture')]","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        SWIPE.$("UP","ELE_LABEL_PUREENDOWMENTAMOUNTTOFORCEREVIEW","//label[@id='workbenchForm:workbenchTabs:pureEndowAmtReview_lbl']","TGTYPESCREENREG");

        ASSERT.$("ELE_LABEL_PUREENDOWMENTAMOUNTTOFORCEREVIEW","//label[@id='workbenchForm:workbenchTabs:pureEndowAmtReview_lbl']","CONTAINS","Pure Endowment Amount To Force Review","TGTYPESCREENREG");

        ASSERT.$("ELE_LABEL_VALUE_PUREENDOWMENTAMOUNTTOFORCEREVIEW","//span[@id='workbenchForm:workbenchTabs:pureEndowAmtReview']","CONTAINS",1,555,555,555,"555.00","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_GIAS_LOGO","//img[@id='headerForm:headerLogoImage']","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        CALL.$("NW0573SGD79NonforfeitureReviewProcess","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_GIAS_LOGO","//img[@id='headerForm:headerLogoImage']","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        SWIPE.$("DOWN","ELE_TXTBOX_SEARCHTASKS","//input[@id='workbenchForm:workbenchTabs:taskSearchContainer:taskSearchSearch']","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_SEARCHTASKS","//input[@id='workbenchForm:workbenchTabs:taskSearchContainer:taskSearchSearch']","Nonforfeiture Review","FALSE","TGTYPESCREENREG");

        WAIT.$(3,"TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SEARCHTASKS","//a[@id='workbenchForm:workbenchTabs:taskSearchContainer:tb_buttonSearch']//span[@class='fa fa-search fa-lg']","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        TAP.$("ELE_LINK_FIRST_TASKS","//button[@id='workbenchForm:workbenchTabs:taskSearchContainer:0:availableTask']","TGTYPESCREENREG");

        WAIT.$("ELE_TXTBOX_POLICYNUMBER_FILTER","//input[@id='workbenchForm:workbenchTabs:grid:policyNumber_Col:filter']","VISIBLE","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_POLICYNUMBER_FILTER","//input[@id='workbenchForm:workbenchTabs:grid:policyNumber_Col:filter']","NFOR3A","FALSE","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

		var_dataToPass = "NFOR3A";
                 LOGGER.info("Executed Step = STORE,var_dataToPass,NFOR3A,TGTYPESCREENREG");
		try { 
		 int totalRowCount = 0;

					Boolean isComplete = false;
					while (isComplete == false){
						List<WebElement> eleSupplementalCode = driver.findElements(By.xpath("//span[@id='workbenchForm:workbenchTabs:grid:"+totalRowCount+":policyNumber']"));

						if (eleSupplementalCode.size()>0){
							totalRowCount = totalRowCount + 1;
						} else {
							isComplete = true;
						}
					}
					for (int i = 0; i <= totalRowCount; i++){

						WebElement elepolicyNumber = driver.findElement(By.xpath("//span[@id='workbenchForm:workbenchTabs:grid:"+i+":policyNumber']"));

						String strpolicyNumber = elepolicyNumber.getText();

						if(elepolicyNumber.getText().contains(var_dataToPass) ){
							elepolicyNumber.click();
		                                 AddToReport("ASSERT," + elepolicyNumber.getText() +", CONTAINS,"+var_dataToPass);
							break;
						}

					}
		 
		 ;} 
		 catch(Exception e) { PrintVal("Renderer Script Exception : "+e.toString()); } 
		 PrintVal("Executed Step = CALLRENDERERSCRIPT,SELECT_POLICYNUMBER_FROM_TABLE"); 
        WAIT.$(5,"TGTYPESCREENREG");

        WAIT.$("ELE_MSG_ERROR_SUMMARY","//span[contains(@class,'ui-messages-error-summary')]","TGTYPESCREENREG");

        ASSERT.$("ELE_MSG_ERROR_SUMMARY","//span[contains(@class,'ui-messages-error-summary')]","CONTAINS","ETI cannot be calculated if cash value is zero. RC = 15","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_GIAS_LOGO","//img[@id='headerForm:headerLogoImage']","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        CALL.$("NW0575SGD82PureEndowentAmounttoForceReview","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_GIAS_LOGO","//img[@id='headerForm:headerLogoImage']","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        SWIPE.$("DOWN","ELE_TXTBOX_SEARCHTASKS","//input[@id='workbenchForm:workbenchTabs:taskSearchContainer:taskSearchSearch']","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_SEARCHTASKS","//input[@id='workbenchForm:workbenchTabs:taskSearchContainer:taskSearchSearch']","Plan Setup","FALSE","TGTYPESCREENREG");

        WAIT.$(3,"TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SEARCHTASKS","//a[@id='workbenchForm:workbenchTabs:taskSearchContainer:tb_buttonSearch']//span[@class='fa fa-search fa-lg']","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        TAP.$("ELE_LINK_FIRST_TASKS","//button[@id='workbenchForm:workbenchTabs:taskSearchContainer:0:availableTask']","TGTYPESCREENREG");

        WAIT.$("ELE_TXTBOX_FILTER_PLANCODE","//input[@id='workbenchForm:workbenchTabs:grid:planCode_Col:filter']","VISIBLE","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_FILTER_PLANCODE","//input[@id='workbenchForm:workbenchTabs:grid:planCode_Col:filter']","TST82","FALSE","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        TAP.$("ELE_LINK_FIRST_PLANCODE","//span[@id='workbenchForm:workbenchTabs:grid:0:planCode']","TGTYPESCREENREG");

        WAIT.$("ELE_BUTTON_EDIT","//button[@id='workbenchForm:workbenchTabs:tb_buttonUpdate']","VISIBLE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_NONFORFEITURE","//span[@class='ui-menuitem-text'][contains(.,'Nonforfeiture')]","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        SWIPE.$("UP","ELE_LABEL_PUREENDOWMENTAMOUNTTOFORCEREVIEW","//label[@id='workbenchForm:workbenchTabs:pureEndowAmtReview_lbl']","TGTYPESCREENREG");

        ASSERT.$("ELE_LABEL_PUREENDOWMENTAMOUNTTOFORCEREVIEW","//label[@id='workbenchForm:workbenchTabs:pureEndowAmtReview_lbl']","CONTAINS","Pure Endowment Amount To Force Review","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_GIAS_LOGO","//img[@id='headerForm:headerLogoImage']","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        CALL.$("NW057Test3VerifyPureEndowmentAmount","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_GIAS_LOGO","//img[@id='headerForm:headerLogoImage']","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        SWIPE.$("DOWN","ELE_TXTBOX_SEARCHTASKS","//input[@id='workbenchForm:workbenchTabs:taskSearchContainer:taskSearchSearch']","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_SEARCHTASKS","//input[@id='workbenchForm:workbenchTabs:taskSearchContainer:taskSearchSearch']","Plan Setup","FALSE","TGTYPESCREENREG");

        WAIT.$(3,"TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SEARCHTASKS","//a[@id='workbenchForm:workbenchTabs:taskSearchContainer:tb_buttonSearch']//span[@class='fa fa-search fa-lg']","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        TAP.$("ELE_LINK_FIRST_TASKS","//button[@id='workbenchForm:workbenchTabs:taskSearchContainer:0:availableTask']","TGTYPESCREENREG");

        WAIT.$("ELE_TXTBOX_FILTER_PLANCODE","//input[@id='workbenchForm:workbenchTabs:grid:planCode_Col:filter']","VISIBLE","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_FILTER_PLANCODE","//input[@id='workbenchForm:workbenchTabs:grid:planCode_Col:filter']","OLDEN1","FALSE","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        TAP.$("ELE_LINK_FIRST_PLANCODE","//span[@id='workbenchForm:workbenchTabs:grid:0:planCode']","TGTYPESCREENREG");

        WAIT.$("ELE_BUTTON_EDIT","//button[@id='workbenchForm:workbenchTabs:tb_buttonUpdate']","VISIBLE","TGTYPESCREENREG");

        SWIPE.$("UP","ELE_LABEL_AUTOMATIC_LAPSE_NFO","//label[@id='workbenchForm:workbenchTabs:automaticNFOProc_lbl']","TGTYPESCREENREG");

        ASSERT.$("ELE_LABEL_AUTOMATIC_LAPSE_NFO","//label[@id='workbenchForm:workbenchTabs:automaticNFOProc_lbl']","CONTAINS","Automatic Lapse/Nonforfeiture","TGTYPESCREENREG");

        ASSERT.$("ELE_LABEL__VALUE_AUTOMATIC_LAPSE_NFO","//span[@id='workbenchForm:workbenchTabs:automaticNFOProc']","CONTAINS","Yes","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        SWIPE.$("DOWN","ELE_BUTTON_NONFORFEITURE","//span[@class='ui-menuitem-text'][contains(.,'Nonforfeiture')]","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_NONFORFEITURE","//span[@class='ui-menuitem-text'][contains(.,'Nonforfeiture')]","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        LOGGER.info("Executed Step = START IF");
        if (check("ELE_LABEL_PUREENDOWMENTAMOUNTTOFORCEREVIEW","//label[@id='workbenchForm:workbenchTabs:pureEndowAmtReview_lbl']","INVISIBLE","TGTYPESCREENREG")) {
         LOGGER.info("Executed Step = IF,ELE_LABEL_PUREENDOWMENTAMOUNTTOFORCEREVIEW,INVISIBLE,TGTYPESCREENREG");
        TAP.$("ELE_BUTTON_FLAG","/html/body/div[1]/div[2]/div/form/div/div[2]/div/div[3]/div/div[1]/div[1]/button/span[1]","TGTYPESCREENREG");

        }
          LOGGER.info("Executed Step = ENDIF,TGTYPESCREENREG");
        SWIPE.$("UP","ELE_LABEL_PUREENDOWMENTAMOUNTTOFORCEREVIEW","//label[@id='workbenchForm:workbenchTabs:pureEndowAmtReview_lbl']","TGTYPESCREENREG");

        ASSERT.$("ELE_LABEL_PUREENDOWMENTAMOUNTTOFORCEREVIEW","//label[@id='workbenchForm:workbenchTabs:pureEndowAmtReview_lbl']","CONTAINS","Pure Endowment Amount To Force Review","TGTYPESCREENREG");

        ASSERT.$("ELE_LABEL_VALUE_PUREENDOWMENTAMOUNTTOFORCEREVIEW","//span[@id='workbenchForm:workbenchTabs:pureEndowAmtReview']","CONTAINS","0.00","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_GIAS_LOGO","//img[@id='headerForm:headerLogoImage']","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        CALL.$("CreatePolicyForGIAS","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_GIAS_LOGO","//img[@id='headerForm:headerLogoImage']","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

		String var_PolicyNumber;
                 LOGGER.info("Executed Step = VAR,String,var_PolicyNumber,TGTYPESCREENREG");
		var_PolicyNumber = getJsonData(var_CSVData , "$.records["+var_Count+"].PolicyNumber");
                 LOGGER.info("Executed Step = STORE,var_PolicyNumber,var_CSVData,$.records[{var_Count}].PolicyNumber,TGTYPESCREENREG");
		String var_EffectiveDate;
                 LOGGER.info("Executed Step = VAR,String,var_EffectiveDate,TGTYPESCREENREG");
		var_EffectiveDate = getJsonData(var_CSVData , "$.records["+var_Count+"].EffectiveDate");
                 LOGGER.info("Executed Step = STORE,var_EffectiveDate,var_CSVData,$.records[{var_Count}].EffectiveDate,TGTYPESCREENREG");
		String var_CashWithApplication;
                 LOGGER.info("Executed Step = VAR,String,var_CashWithApplication,TGTYPESCREENREG");
		var_CashWithApplication = getJsonData(var_CSVData , "$.records["+var_Count+"].CashWithApplication");
                 LOGGER.info("Executed Step = STORE,var_CashWithApplication,var_CSVData,$.records[{var_Count}].CashWithApplication,TGTYPESCREENREG");
		String var_PaymentFrequency;
                 LOGGER.info("Executed Step = VAR,String,var_PaymentFrequency,TGTYPESCREENREG");
		var_PaymentFrequency = getJsonData(var_CSVData , "$.records["+var_Count+"].PaymentFrequency");
                 LOGGER.info("Executed Step = STORE,var_PaymentFrequency,var_CSVData,$.records[{var_Count}].PaymentFrequency,TGTYPESCREENREG");
		String var_PaymentMethod;
                 LOGGER.info("Executed Step = VAR,String,var_PaymentMethod,TGTYPESCREENREG");
		var_PaymentMethod = getJsonData(var_CSVData , "$.records["+var_Count+"].PaymentMethod");
                 LOGGER.info("Executed Step = STORE,var_PaymentMethod,var_CSVData,$.records[{var_Count}].PaymentMethod,TGTYPESCREENREG");
		String var_IssueState;
                 LOGGER.info("Executed Step = VAR,String,var_IssueState,TGTYPESCREENREG");
		var_IssueState = getJsonData(var_CSVData , "$.records["+var_Count+"].IssueState");
                 LOGGER.info("Executed Step = STORE,var_IssueState,var_CSVData,$.records[{var_Count}].IssueState,TGTYPESCREENREG");
		String var_AgentNumber;
                 LOGGER.info("Executed Step = VAR,String,var_AgentNumber,TGTYPESCREENREG");
		var_AgentNumber = getJsonData(var_CSVData , "$.records["+var_Count+"].AgentNumber");
                 LOGGER.info("Executed Step = STORE,var_AgentNumber,var_CSVData,$.records[{var_Count}].AgentNumber,TGTYPESCREENREG");
		String var_SplitPercentage;
                 LOGGER.info("Executed Step = VAR,String,var_SplitPercentage,TGTYPESCREENREG");
		var_SplitPercentage = getJsonData(var_CSVData , "$.records["+var_Count+"].SplitPercentage");
                 LOGGER.info("Executed Step = STORE,var_SplitPercentage,var_CSVData,$.records[{var_Count}].SplitPercentage,TGTYPESCREENREG");
		String var_ClientCheckName;
                 LOGGER.info("Executed Step = VAR,String,var_ClientCheckName,TGTYPESCREENREG");
		var_ClientCheckName = getJsonData(var_CSVData , "$.records["+var_Count+"].ClientCheckName");
                 LOGGER.info("Executed Step = STORE,var_ClientCheckName,var_CSVData,$.records[{var_Count}].ClientCheckName,TGTYPESCREENREG");
		String var_ClientFullName;
                 LOGGER.info("Executed Step = VAR,String,var_ClientFullName,TGTYPESCREENREG");
		var_ClientFullName = getJsonData(var_CSVData , "$.records["+var_Count+"].ClientFullName");
                 LOGGER.info("Executed Step = STORE,var_ClientFullName,var_CSVData,$.records[{var_Count}].ClientFullName,TGTYPESCREENREG");
		String var_ClientID;
                 LOGGER.info("Executed Step = VAR,String,var_ClientID,TGTYPESCREENREG");
		var_ClientID = getJsonData(var_CSVData , "$.records["+var_Count+"].ClientID");
                 LOGGER.info("Executed Step = STORE,var_ClientID,var_CSVData,$.records[{var_Count}].ClientID,TGTYPESCREENREG");
		String var_ClientBirthDate;
                 LOGGER.info("Executed Step = VAR,String,var_ClientBirthDate,TGTYPESCREENREG");
		var_ClientBirthDate = getJsonData(var_CSVData , "$.records["+var_Count+"].ClientBirthDate");
                 LOGGER.info("Executed Step = STORE,var_ClientBirthDate,var_CSVData,$.records[{var_Count}].ClientBirthDate,TGTYPESCREENREG");
        SWIPE.$("DOWN","ELE_TXTBOX_SEARCHTASKS","//input[@id='workbenchForm:workbenchTabs:taskSearchContainer:taskSearchSearch']","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_SEARCHTASKS","//input[@id='workbenchForm:workbenchTabs:taskSearchContainer:taskSearchSearch']","New Policy Application","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SEARCHTASKS","//a[@id='workbenchForm:workbenchTabs:taskSearchContainer:tb_buttonSearch']//span[@class='fa fa-search fa-lg']","TGTYPESCREENREG");

        WAIT.$("ELE_LINK_NEWPOLICYAPPLICATION","//span[contains(text(),'New Policy Application')]","VISIBLE","TGTYPESCREENREG");

        TAP.$("ELE_LINK_NEWPOLICYAPPLICATION","//span[contains(text(),'New Policy Application')]","TGTYPESCREENREG");

        WAIT.$("ELE_BUTTON_ADD","//button[@id='workbenchForm:workbenchTabs:tb_buttonAdd']//span[@class='ui-button-text ui-c'][contains(text(),'Add')]","VISIBLE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_ADD","//button[@id='workbenchForm:workbenchTabs:tb_buttonAdd']//span[@class='ui-button-text ui-c'][contains(text(),'Add')]","TGTYPESCREENREG");

        WAIT.$("ELE_TXTBOX_POLICYNUMBER","//input[@id='workbenchForm:workbenchTabs:policyNumber']","VISIBLE","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_POLICYNUMBER","//input[@id='workbenchForm:workbenchTabs:policyNumber']",var_PolicyNumber,"FALSE","TGTYPESCREENREG");

        TAP.$("ELE_DRPDWN_PAYMENTFREQUENCY","//label[@id='workbenchForm:workbenchTabs:paymentMode_label']","TGTYPESCREENREG");

		var_dataToPass = var_PaymentFrequency;
                 LOGGER.info("Executed Step = STORE,var_dataToPass,var_PaymentFrequency,TGTYPESCREENREG");
		try { 
		 Thread.sleep(1000);
		WebElement selectValueOfDropDown= driver.findElement(By.xpath("//li[@data-label='"+ var_dataToPass +"']"));

		takeScreenshot();

				selectValueOfDropDown.click();
		// Add Screenshot will only work for Android platform

		takeScreenshot();

				 Thread.sleep(1000);
		 
		 ;} 
		 catch(Exception e) { PrintVal("Renderer Script Exception : "+e.toString()); } 
		 PrintVal("Executed Step = CALLRENDERERSCRIPT,SELECTVALUEFROMDROPDOWN"); 
        WAIT.$(5,"TGTYPESCREENREG");

        TAP.$("ELE_DRPDWN_PAYMENTMETHOD","//label[@id='workbenchForm:workbenchTabs:paymentCode_label']","TGTYPESCREENREG");

		var_dataToPass = var_PaymentMethod;
                 LOGGER.info("Executed Step = STORE,var_dataToPass,var_PaymentMethod,TGTYPESCREENREG");
		try { 
		 Thread.sleep(1000);
		WebElement selectValueOfDropDown= driver.findElement(By.xpath("//li[@data-label='"+ var_dataToPass +"']"));

		takeScreenshot();

				selectValueOfDropDown.click();
		// Add Screenshot will only work for Android platform

		takeScreenshot();

				 Thread.sleep(1000);
		 
		 ;} 
		 catch(Exception e) { PrintVal("Renderer Script Exception : "+e.toString()); } 
		 PrintVal("Executed Step = CALLRENDERERSCRIPT,SELECTVALUEFROMDROPDOWN"); 
        WAIT.$(5,"TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_CASHWITHAPPLICATION","//input[@id='workbenchForm:workbenchTabs:cashWithApplication_input']",var_CashWithApplication,"FALSE","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_EFFECTIVEDATE","//input[@id='workbenchForm:workbenchTabs:effectiveDate_input']",var_EffectiveDate,"FALSE","TGTYPESCREENREG");

		var_dataToPass = var_IssueState;
                 LOGGER.info("Executed Step = STORE,var_dataToPass,var_IssueState,TGTYPESCREENREG");
        TAP.$("ELE_DRPDWN_ISSUESTATE","//label[@id='workbenchForm:workbenchTabs:countryAndStateCode_label']","TGTYPESCREENREG");

		try { 
		 Thread.sleep(1000);
		WebElement selectValueOfDropDown= driver.findElement(By.xpath("//li[@data-label='"+ var_dataToPass +"']"));

		takeScreenshot();

				selectValueOfDropDown.click();
		// Add Screenshot will only work for Android platform

		takeScreenshot();

				 Thread.sleep(1000);
		 
		 ;} 
		 catch(Exception e) { PrintVal("Renderer Script Exception : "+e.toString()); } 
		 PrintVal("Executed Step = CALLRENDERERSCRIPT,SELECTVALUEFROMDROPDOWN"); 
        WAIT.$(5,"TGTYPESCREENREG");

        SWIPE.$("UP","ELE_BUTTON_ADDAGENT_PLUS","//button[@id='workbenchForm:workbenchTabs:agentGrid:0:icon_button']","TGTYPESCREENREG");

        SWIPE.$("UP","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_ADDAGENT_PLUS","//button[@id='workbenchForm:workbenchTabs:agentGrid:0:icon_button']","TGTYPESCREENREG");

        WAIT.$("ELE_TXTBOX_AGENTNUMBER","//input[@id='workbenchForm:workbenchTabs:agentNumber']","VISIBLE","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_AGENTNUMBER","//input[@id='workbenchForm:workbenchTabs:agentNumber']",var_AgentNumber,"FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_AGENTSEARCH","//button[@id='workbenchForm:workbenchTabs:agentNumber_icon']","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_SPLITPERCENTAGE","//input[@id='workbenchForm:workbenchTabs:agentSplitPercentage_input']",var_SplitPercentage,"FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SAVE","//span[contains(text(),'Save')]","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SHOWLINKS","//button[@id='workbenchForm:workbenchTabs:tb_buttonLink']","TGTYPESCREENREG");

        TAP.$("ELE_LINK_SELECTCLIENT","//span[contains(text(),'Select Client')]","TGTYPESCREENREG");

        WAIT.$("ELE_TXTBOX_CLIENTSEARCHNAME","//input[@id='workbenchForm:workbenchTabs:searchName']","VISIBLE","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_CLIENTSEARCHNAME","//input[@id='workbenchForm:workbenchTabs:searchName']",var_ClientCheckName,"FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SEARCH","//button[@id='workbenchForm:workbenchTabs:button_search']","TGTYPESCREENREG");

        WAIT.$("ELE_TXTBOX_FILTER_CLIENTNAME","//input[@id='workbenchForm:workbenchTabs:clientGrid:individualName_Col:filter']","VISIBLE","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_FILTER_CLIENTID","//input[@id='workbenchForm:workbenchTabs:clientGrid:clientId_Col:filter']",var_ClientID,"FALSE","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

		try { 
		 		  int totalRowCount = 0;

						 LOGGER.info("aaaa");
									Boolean isComplete = false;
		            try {
		                while (isComplete == false) {
		                    List<WebElement> eleSupplementalCode = driver.findElements(By.xpath("//span[@id='workbenchForm:workbenchTabs:clientGrid:" + totalRowCount + ":individualName']"));
		                    LOGGER.info("bbbb========" + eleSupplementalCode.size());

		                    if (eleSupplementalCode.size() > 0) {
		                        totalRowCount = totalRowCount + 1;
		                    } else {
		                        isComplete = true;
		                    }
		                }
		            } catch (Exception e) {
		                e.printStackTrace();

		        }
		            for (int i = 0; i <=totalRowCount; i++){

										WebElement eleClientName = driver.findElement(By.xpath("//span[@id='workbenchForm:workbenchTabs:clientGrid:"+i+":individualName']"));

		                //span[@id='workbenchForm:workbenchTabs:clientGrid:0:dateOfBirthDate']
										try {
										     WebElement eleClientBirthDate = driver.findElement(By.xpath("//span[@id='workbenchForm:workbenchTabs:clientGrid:"+i+":dateOfBirthDate']"));

		                                LOGGER.info("ccccccccc========"+eleClientName.getText());
				                        LOGGER.info("ccccccccc========"+var_ClientCheckName);
										String strClientName = eleClientName.getText();
				                                                String strClientBirthDate = eleClientBirthDate .getText();

										if(strClientName.contains(var_ClientCheckName) &&  strClientBirthDate.contains(var_ClientBirthDate)){
											eleClientName.click();
											AddToReport("Tap, "+strClientName);
											break;
										}
		                                } catch(Exception e) { PrintVal("Renderer Script Exception : "+e.toString()); }

									}
				 
				 
		 
		 ;} 
		 catch(Exception e) { PrintVal("Renderer Script Exception : "+e.toString()); } 
		 PrintVal("Executed Step = CALLRENDERERSCRIPT,SELECTCLIENTFROMLIST"); 
        WAIT.$(5,"TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_TRUE","//button[@id='workbenchForm:workbenchTabs:button_select']//span[@class='ui-button-icon-left ui-icon ui-c fa fa-check']","TGTYPESCREENREG");

        WAIT.$("ELE_BUTTON_CONTINUE","//span[contains(text(),'Continue')]","VISIBLE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_CONTINUE","//span[contains(text(),'Continue')]","TGTYPESCREENREG");

        WAIT.$("ELE_LABEL_OWNERNAME","//span[@id='workbenchForm:workbenchTabs:ownerLabel']","VISIBLE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SAVE","//span[contains(text(),'Save')]","TGTYPESCREENREG");

        WAIT.$("ELE_MESSAGE_ITEMSUCCESFULLYADDED","//div[@id='workbenchForm:workbenchTabs:messages']//span[@class='ui-messages-info-summary'][contains(text(),'Item successfully added.')]","VISIBLE","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        CALL.$("BenefitPlanForCreatePolicyNW057","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

		String var_BenefitPlan;
                 LOGGER.info("Executed Step = VAR,String,var_BenefitPlan,TGTYPESCREENREG");
		var_BenefitPlan = getJsonData(var_CSVData , "$.records["+var_Count+"].BenefitPlan");
                 LOGGER.info("Executed Step = STORE,var_BenefitPlan,var_CSVData,$.records[{var_Count}].BenefitPlan,TGTYPESCREENREG");
		String var_Units;
                 LOGGER.info("Executed Step = VAR,String,var_Units,TGTYPESCREENREG");
		var_Units = getJsonData(var_CSVData , "$.records["+var_Count+"].Units");
                 LOGGER.info("Executed Step = STORE,var_Units,var_CSVData,$.records[{var_Count}].Units,TGTYPESCREENREG");
		String var_IssueAge;
                 LOGGER.info("Executed Step = VAR,String,var_IssueAge,TGTYPESCREENREG");
		var_IssueAge = getJsonData(var_CSVData , "$.records["+var_Count+"].IssueAge");
                 LOGGER.info("Executed Step = STORE,var_IssueAge,var_CSVData,$.records[{var_Count}].IssueAge,TGTYPESCREENREG");
		String var_RiskClass;
                 LOGGER.info("Executed Step = VAR,String,var_RiskClass,TGTYPESCREENREG");
		var_RiskClass = getJsonData(var_CSVData , "$.records["+var_Count+"].RiskClass");
                 LOGGER.info("Executed Step = STORE,var_RiskClass,var_CSVData,$.records[{var_Count}].RiskClass,TGTYPESCREENREG");
		String var_NonforfeitureOption;
                 LOGGER.info("Executed Step = VAR,String,var_NonforfeitureOption,TGTYPESCREENREG");
		var_NonforfeitureOption = getJsonData(var_CSVData , "$.records["+var_Count+"].NonforfeitureOption");
                 LOGGER.info("Executed Step = STORE,var_NonforfeitureOption,var_CSVData,$.records[{var_Count}].NonforfeitureOption,TGTYPESCREENREG");
		String var_PrimaryDividend;
                 LOGGER.info("Executed Step = VAR,String,var_PrimaryDividend,TGTYPESCREENREG");
		var_PrimaryDividend = getJsonData(var_CSVData , "$.records["+var_Count+"].PrimaryDividend");
                 LOGGER.info("Executed Step = STORE,var_PrimaryDividend,var_CSVData,$.records[{var_Count}].PrimaryDividend,TGTYPESCREENREG");
		String var_SecondaryDividend;
                 LOGGER.info("Executed Step = VAR,String,var_SecondaryDividend,TGTYPESCREENREG");
		var_SecondaryDividend = getJsonData(var_CSVData , "$.records["+var_Count+"].SecondaryDividend");
                 LOGGER.info("Executed Step = STORE,var_SecondaryDividend,var_CSVData,$.records[{var_Count}].SecondaryDividend,TGTYPESCREENREG");
        TAP.$("ELE_BUTTON_SHOWLINKS","//button[@id='workbenchForm:workbenchTabs:tb_buttonLink']","TGTYPESCREENREG");

        TAP.$("ELE_LINK_BENEFITS","//span[contains(text(),'Benefits')]","TGTYPESCREENREG");

        WAIT.$("ELE_DRPDWN_BENEFITPLAN","//label[@id='workbenchForm:workbenchTabs:initialPlanCode_label']","VISIBLE","TGTYPESCREENREG");

        TAP.$("ELE_DRPDWN_BENEFITPLAN","//label[@id='workbenchForm:workbenchTabs:initialPlanCode_label']","TGTYPESCREENREG");

		var_dataToPass = var_BenefitPlan;
                 LOGGER.info("Executed Step = STORE,var_dataToPass,var_BenefitPlan,TGTYPESCREENREG");
		try { 
		 Thread.sleep(1000);
		WebElement selectValueOfDropDown= driver.findElement(By.xpath("//li[@data-label='"+ var_dataToPass +"']"));

		takeScreenshot();

				selectValueOfDropDown.click();
		// Add Screenshot will only work for Android platform

		takeScreenshot();

				 Thread.sleep(1000);
		 
		 ;} 
		 catch(Exception e) { PrintVal("Renderer Script Exception : "+e.toString()); } 
		 PrintVal("Executed Step = CALLRENDERERSCRIPT,SELECTVALUEFROMDROPDOWN"); 
        WAIT.$(5,"TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_CONTINUE","//span[contains(text(),'Continue')]","TGTYPESCREENREG");

        WAIT.$("ELE_TXTBOX_UNITS","//input[@id='workbenchForm:workbenchTabs:unitsOfInsurance_input']","VISIBLE","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_UNITS","//input[@id='workbenchForm:workbenchTabs:unitsOfInsurance_input']",var_Units,"FALSE","TGTYPESCREENREG");

		var_dataToPass = var_RiskClass;
                 LOGGER.info("Executed Step = STORE,var_dataToPass,var_RiskClass,TGTYPESCREENREG");
        TAP.$("ELE_DRPDWN_RISK_CLASS","//label[@id='workbenchForm:workbenchTabs:smokerCode_label']","TGTYPESCREENREG");

		try { 
		 Thread.sleep(1000);
		WebElement selectValueOfDropDown= driver.findElement(By.xpath("//li[@data-label='"+ var_dataToPass +"']"));

		takeScreenshot();

				selectValueOfDropDown.click();
		// Add Screenshot will only work for Android platform

		takeScreenshot();

				 Thread.sleep(1000);
		 
		 ;} 
		 catch(Exception e) { PrintVal("Renderer Script Exception : "+e.toString()); } 
		 PrintVal("Executed Step = CALLRENDERERSCRIPT,SELECTVALUEFROMDROPDOWN"); 
        WAIT.$(5,"TGTYPESCREENREG");

		var_dataToPass = var_NonforfeitureOption;
                 LOGGER.info("Executed Step = STORE,var_dataToPass,var_NonforfeitureOption,TGTYPESCREENREG");
        TAP.$("ELE_DRPDWN_NONFORFEITUREOPTION","//label[@id='workbenchForm:workbenchTabs:defaultNonForfOpt_label']","TGTYPESCREENREG");

		try { 
		 Thread.sleep(1000);
		WebElement selectValueOfDropDown= driver.findElement(By.xpath("//li[@data-label='"+ var_dataToPass +"']"));

		takeScreenshot();

				selectValueOfDropDown.click();
		// Add Screenshot will only work for Android platform

		takeScreenshot();

				 Thread.sleep(1000);
		 
		 ;} 
		 catch(Exception e) { PrintVal("Renderer Script Exception : "+e.toString()); } 
		 PrintVal("Executed Step = CALLRENDERERSCRIPT,SELECTVALUEFROMDROPDOWN"); 
        WAIT.$(5,"TGTYPESCREENREG");

		var_dataToPass = var_PrimaryDividend;
                 LOGGER.info("Executed Step = STORE,var_dataToPass,var_PrimaryDividend,TGTYPESCREENREG");
        TAP.$("ELE_DRPDWN_PRIMARYDIVIDENDOPETION_POLICY","//label[@id='workbenchForm:workbenchTabs:primaryDividendOption_label']","TGTYPESCREENREG");

		try { 
		 Thread.sleep(1000);
		WebElement selectValueOfDropDown= driver.findElement(By.xpath("//li[@data-label='"+ var_dataToPass +"']"));

		takeScreenshot();

				selectValueOfDropDown.click();
		// Add Screenshot will only work for Android platform

		takeScreenshot();

				 Thread.sleep(1000);
		 
		 ;} 
		 catch(Exception e) { PrintVal("Renderer Script Exception : "+e.toString()); } 
		 PrintVal("Executed Step = CALLRENDERERSCRIPT,SELECTVALUEFROMDROPDOWN"); 
        WAIT.$(5,"TGTYPESCREENREG");

		var_dataToPass = var_SecondaryDividend;
                 LOGGER.info("Executed Step = STORE,var_dataToPass,var_SecondaryDividend,TGTYPESCREENREG");
        TAP.$("ELE_DRPDWN_SECONDARY_DIVIDEND","//label[@id='workbenchForm:workbenchTabs:secondaryDividendOption_label']","TGTYPESCREENREG");

		try { 
		 Thread.sleep(1000);
		WebElement selectValueOfDropDown= driver.findElement(By.xpath("//li[@data-label='"+ var_dataToPass +"']"));

		takeScreenshot();

				selectValueOfDropDown.click();
		// Add Screenshot will only work for Android platform

		takeScreenshot();

				 Thread.sleep(1000);
		 
		 ;} 
		 catch(Exception e) { PrintVal("Renderer Script Exception : "+e.toString()); } 
		 PrintVal("Executed Step = CALLRENDERERSCRIPT,SELECTVALUEFROMDROPDOWN"); 
        WAIT.$(5,"TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SAVE","//span[contains(text(),'Save')]","TGTYPESCREENREG");

        WAIT.$("ELE_MESSAGE_ITEMSUCCESFULLYADDED","//div[@id='workbenchForm:workbenchTabs:messages']//span[@class='ui-messages-info-summary'][contains(text(),'Item successfully added.')]","VISIBLE","TGTYPESCREENREG");

        TAP.$("ELE_TAB_CLOSE","//li[@class='ui-state-default ui-tabs-selected ui-state-active ui-corner-top']//span[@class='ui-icon ui-icon-close']","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        CALL.$("IssuePolicyForCreatePolicyNW057","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SHOWLINKS","//button[@id='workbenchForm:workbenchTabs:tb_buttonLink']","TGTYPESCREENREG");

        TAP.$("ELE_LINK_ISSUE","//span[@class='ui-menuitem-text'][contains(text(),'Issue')]","TGTYPESCREENREG");

        WAIT.$("ELE_BUTTON_SUBMIT","//span[contains(text(),'Submit')]","VISIBLE","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SUBMIT","//span[contains(text(),'Submit')]","TGTYPESCREENREG");

        WAIT.$("ELE_MSG_REQUESTCOMPLETEDSUCCESSFULLY","//div[@id='workbenchForm:workbenchTabs:messages']//span[@class='ui-messages-info-summary'][contains(text(),'Request completed successfully.')]","VISIBLE","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        TAP.$("ELE_TAB_CLOSE","//li[@class='ui-state-default ui-tabs-selected ui-state-active ui-corner-top']//span[@class='ui-icon ui-icon-close']","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        CALL.$("SettlePolicyForCreatePolicyNW057","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SHOWLINKS","//button[@id='workbenchForm:workbenchTabs:tb_buttonLink']","TGTYPESCREENREG");

        TAP.$("ELE_LINK_SETTLE","//span[contains(text(),'Settle')]","TGTYPESCREENREG");

        WAIT.$("ELE_BUTTON_SUBMIT","//span[contains(text(),'Submit')]","VISIBLE","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        SWIPE.$("DOWN","TGTYPESCREENREG");

        SWIPE.$("DOWN","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SUBMIT","//span[contains(text(),'Submit')]","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        WAIT.$("ELE_MSG_REQUESTCOMPLETEDSUCCESSFULLY","//div[@id='workbenchForm:workbenchTabs:messages']//span[@class='ui-messages-info-summary'][contains(text(),'Request completed successfully.')]","VISIBLE","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        TAP.$("ELE_TAB_CLOSE","//li[@class='ui-state-default ui-tabs-selected ui-state-active ui-corner-top']//span[@class='ui-icon ui-icon-close']","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        CALL.$("AdjustDividendsNW057","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_GIAS_LOGO","//img[@id='headerForm:headerLogoImage']","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

		String var_PaidUPTransactionAmount;
                 LOGGER.info("Executed Step = VAR,String,var_PaidUPTransactionAmount,TGTYPESCREENREG");
		var_PaidUPTransactionAmount = getJsonData(var_CSVData , "$.records["+var_Count+"].PaidUPTransactionAmount");
                 LOGGER.info("Executed Step = STORE,var_PaidUPTransactionAmount,var_CSVData,$.records[{var_Count}].PaidUPTransactionAmount,TGTYPESCREENREG");
		String var_PaidUPCalculate;
                 LOGGER.info("Executed Step = VAR,String,var_PaidUPCalculate,TGTYPESCREENREG");
		var_PaidUPCalculate = getJsonData(var_CSVData , "$.records["+var_Count+"].PaidUPCalculate");
                 LOGGER.info("Executed Step = STORE,var_PaidUPCalculate,var_CSVData,$.records[{var_Count}].PaidUPCalculate,TGTYPESCREENREG");
		String var_PaidUpDebitCardNumber;
                 LOGGER.info("Executed Step = VAR,String,var_PaidUpDebitCardNumber,TGTYPESCREENREG");
		var_PaidUpDebitCardNumber = getJsonData(var_CSVData , "$.records["+var_Count+"].PaidUpDebitCardNumber");
                 LOGGER.info("Executed Step = STORE,var_PaidUpDebitCardNumber,var_CSVData,$.records[{var_Count}].PaidUpDebitCardNumber,TGTYPESCREENREG");
		String var_PaidUpCreditCardNumber;
                 LOGGER.info("Executed Step = VAR,String,var_PaidUpCreditCardNumber,TGTYPESCREENREG");
		var_PaidUpCreditCardNumber = getJsonData(var_CSVData , "$.records["+var_Count+"].PaidUpCreditCardNumber");
                 LOGGER.info("Executed Step = STORE,var_PaidUpCreditCardNumber,var_CSVData,$.records[{var_Count}].PaidUpCreditCardNumber,TGTYPESCREENREG");
		String var_AccumulateionsTransactionAmount;
                 LOGGER.info("Executed Step = VAR,String,var_AccumulateionsTransactionAmount,TGTYPESCREENREG");
		var_AccumulateionsTransactionAmount = getJsonData(var_CSVData , "$.records["+var_Count+"].AccumulateionsTransactionAmount");
                 LOGGER.info("Executed Step = STORE,var_AccumulateionsTransactionAmount,var_CSVData,$.records[{var_Count}].AccumulateionsTransactionAmount,TGTYPESCREENREG");
		String var_AccumulateionsDebitCardNumber;
                 LOGGER.info("Executed Step = VAR,String,var_AccumulateionsDebitCardNumber,TGTYPESCREENREG");
		var_AccumulateionsDebitCardNumber = getJsonData(var_CSVData , "$.records["+var_Count+"].AccumulateionsDebitCardNumber");
                 LOGGER.info("Executed Step = STORE,var_AccumulateionsDebitCardNumber,var_CSVData,$.records[{var_Count}].AccumulateionsDebitCardNumber,TGTYPESCREENREG");
		String var_AccumulateionsCreditCardNumber;
                 LOGGER.info("Executed Step = VAR,String,var_AccumulateionsCreditCardNumber,TGTYPESCREENREG");
		var_AccumulateionsCreditCardNumber = getJsonData(var_CSVData , "$.records["+var_Count+"].AccumulateionsCreditCardNumber");
                 LOGGER.info("Executed Step = STORE,var_AccumulateionsCreditCardNumber,var_CSVData,$.records[{var_Count}].AccumulateionsCreditCardNumber,TGTYPESCREENREG");
        SWIPE.$("DOWN","ELE_TXTBOX_SEARCHTASKS","//input[@id='workbenchForm:workbenchTabs:taskSearchContainer:taskSearchSearch']","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_SEARCHTASKS","//input[@id='workbenchForm:workbenchTabs:taskSearchContainer:taskSearchSearch']","Adjust Dividends","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SEARCHTASKS","//a[@id='workbenchForm:workbenchTabs:taskSearchContainer:tb_buttonSearch']//span[@class='fa fa-search fa-lg']","TGTYPESCREENREG");

        WAIT.$("ELE_LINK_ADJUSTDIVIDENDSANDCOUPONS","//span[contains(text(),'Adjust Dividends and Coupons')]","VISIBLE","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        TAP.$("ELE_LINK_ADJUSTDIVIDENDSANDCOUPONS","//span[contains(text(),'Adjust Dividends and Coupons')]","TGTYPESCREENREG");

        WAIT.$("ELE_TXTBOX_POLICYNUMBER","//input[@id='workbenchForm:workbenchTabs:policyNumber']","VISIBLE","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_POLICYNUMBER","//input[@id='workbenchForm:workbenchTabs:policyNumber']",var_PolicyNumber,"FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_CONTINUE","//span[contains(text(),'Continue')]","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        WAIT.$("ELE_TXTBOX_TRANSACTIONAMOUNT_ONEYEARTERMADDITIONS","//input[@id='workbenchForm:workbenchTabs:year1TermAddiTransactionAmount_input']","VISIBLE","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_TRANSACTIONAMOUNT_ONEYEARTERMADDITIONS","//input[@id='workbenchForm:workbenchTabs:year1TermAddiTransactionAmount_input']",var_PaidUPTransactionAmount,"FALSE","TGTYPESCREENREG");

		var_dataToPass = var_PaidUPCalculate;
                 LOGGER.info("Executed Step = STORE,var_dataToPass,var_PaidUPCalculate,TGTYPESCREENREG");
        TAP.$("ELE_DRPDWN_CALCULATE_ONEYEAR","//label[@id='workbenchForm:workbenchTabs:year1TermAddiCalculate_label']","TGTYPESCREENREG");

		try { 
		 //li[@id='workbenchForm:workbenchTabs:year1TermAddiCalculate_1']

			Thread.sleep(1000);
					for (int i = 0; i < 2000; i++){
						WebElement selectValueOfDropDown= driver.findElement(By.xpath("//li[@id='workbenchForm:workbenchTabs:year1TermAddiCalculate_"+ i +"']"));

						if (selectValueOfDropDown.getText().contains(var_dataToPass)){

		                                selectValueOfDropDown.click();
							AddToReport("ASSERT," + selectValueOfDropDown.getText() +", CONTAINS,"+var_dataToPass);
						
							break;
						}
					}
					takeScreenshot();

					Thread.sleep(2000);
		 
		 ;} 
		 catch(Exception e) { PrintVal("Renderer Script Exception : "+e.toString()); } 
		 PrintVal("Executed Step = CALLRENDERERSCRIPT,SELECT_CALCULATE_ONETEARTERM"); 
        WAIT.$(5,"TGTYPESCREENREG");

		var_dataToPass = var_PaidUpDebitCardNumber;
                 LOGGER.info("Executed Step = STORE,var_dataToPass,var_PaidUpDebitCardNumber,TGTYPESCREENREG");
        TAP.$("ELE_DRPDWN_DEBITACCOUNTNUMBER_ONEYEARTERM","//label[@id='workbenchForm:workbenchTabs:year1TermAddiLedgerDebit_label']","TGTYPESCREENREG");

		try { 
		 //li[@id='workbenchForm:workbenchTabs:year1TermAddiLedgerDebit_1']


			Thread.sleep(1000);
					for (int i = 0; i < 2000; i++){
						WebElement selectValueOfDropDown= driver.findElement(By.xpath("//li[@id='workbenchForm:workbenchTabs:year1TermAddiLedgerDebit_"+ i +"']"));

						if (selectValueOfDropDown.getText().contains(var_dataToPass)){
		AddToReport("ASSERT," + selectValueOfDropDown.getText() +", CONTAINS,"+var_dataToPass);
		                                selectValueOfDropDown.click();
							
						
							break;
						}
					}
					takeScreenshot();

					Thread.sleep(2000);
		 
		 ;} 
		 catch(Exception e) { PrintVal("Renderer Script Exception : "+e.toString()); } 
		 PrintVal("Executed Step = CALLRENDERERSCRIPT,SELECT_DEBITCARDNUMBER_ONEYEARTERM"); 
        WAIT.$(5,"TGTYPESCREENREG");

		var_dataToPass = var_PaidUpCreditCardNumber;
                 LOGGER.info("Executed Step = STORE,var_dataToPass,var_PaidUpCreditCardNumber,TGTYPESCREENREG");
        TAP.$("ELE_DRPDWN_CREDITACCOUNTNUMBER_ONEYEARTERM","//label[@id='workbenchForm:workbenchTabs:year1TermAddiLedgerCredit_label']","TGTYPESCREENREG");

		try { 
		 //li[@id='workbenchForm:workbenchTabs:year1TermAddiLedgerCredit_1']


			Thread.sleep(1000);
					for (int i = 0; i < 2000; i++){
						WebElement selectValueOfDropDown= driver.findElement(By.xpath("//li[@id='workbenchForm:workbenchTabs:year1TermAddiLedgerCredit_"+ i +"']"));

						if (selectValueOfDropDown.getText().contains(var_dataToPass)){
		AddToReport("ASSERT," + selectValueOfDropDown.getText() +", CONTAINS,"+var_dataToPass);
		                                selectValueOfDropDown.click();
							
						
							break;
						}
					}
					takeScreenshot();

					Thread.sleep(2000);
		 
		 ;} 
		 catch(Exception e) { PrintVal("Renderer Script Exception : "+e.toString()); } 
		 PrintVal("Executed Step = CALLRENDERERSCRIPT,SELECT_CREDITCARDNUMBER_ONEYEARTERM"); 
        WAIT.$(5,"TGTYPESCREENREG");

        SWIPE.$("DOWN","TGTYPESCREENREG");

        SWIPE.$("DOWN","TGTYPESCREENREG");

        SCROLL.$("ELE_TXTBOX_TRANSACTIONAMOUNT_ACCUMULATIONS","//input[@id='workbenchForm:workbenchTabs:accumulationTransactionAmount_input']","RIGHT","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_TRANSACTIONAMOUNT_ACCUMULATIONS","//input[@id='workbenchForm:workbenchTabs:accumulationTransactionAmount_input']",var_AccumulateionsTransactionAmount,"FALSE","TGTYPESCREENREG");

		var_dataToPass = var_AccumulateionsDebitCardNumber;
                 LOGGER.info("Executed Step = STORE,var_dataToPass,var_AccumulateionsDebitCardNumber,TGTYPESCREENREG");
        TAP.$("ELE_DRPDWN_DEBITACCOUNTNUMBER_ACCUMULATION","//label[@id='workbenchForm:workbenchTabs:accumulationLedgerDebit_label']","TGTYPESCREENREG");

		try { 
		 //li[@id='workbenchForm:workbenchTabs:accumulationLedgerDebit_1']


			Thread.sleep(1000);
					for (int i = 0; i < 2000; i++){
						WebElement selectValueOfDropDown= driver.findElement(By.xpath("//li[@id='workbenchForm:workbenchTabs:accumulationLedgerDebit_"+ i +"']"));

						if (selectValueOfDropDown.getText().contains(var_dataToPass)){
		AddToReport("ASSERT," + selectValueOfDropDown.getText() +", CONTAINS,"+var_dataToPass);
		                                selectValueOfDropDown.click();
							
						
							break;
						}
					}
					takeScreenshot();

					Thread.sleep(2000);
		 
		 ;} 
		 catch(Exception e) { PrintVal("Renderer Script Exception : "+e.toString()); } 
		 PrintVal("Executed Step = CALLRENDERERSCRIPT,SELECT_DEBITCARDNUMBER_ACCUMULATION"); 
        WAIT.$(5,"TGTYPESCREENREG");

		var_dataToPass = var_AccumulateionsCreditCardNumber;
                 LOGGER.info("Executed Step = STORE,var_dataToPass,var_AccumulateionsCreditCardNumber,TGTYPESCREENREG");
        TAP.$("ELE_DRPDWN_CREDITACCOUNTNUMBER_ACCUMULATION","//label[@id='workbenchForm:workbenchTabs:accumulationLedgerCredit_label']","TGTYPESCREENREG");

		try { 
		 //li[@id='workbenchForm:workbenchTabs:accumulationLedgerCredit_1']

			Thread.sleep(1000);
					for (int i = 0; i < 2000; i++){
						WebElement selectValueOfDropDown= driver.findElement(By.xpath("//li[@id='workbenchForm:workbenchTabs:accumulationLedgerCredit_"+ i +"']"));

						if (selectValueOfDropDown.getText().contains(var_dataToPass)){
		AddToReport("ASSERT," + selectValueOfDropDown.getText() +", CONTAINS,"+var_dataToPass);
		                                selectValueOfDropDown.click();
							
						
							break;
						}
					}
					takeScreenshot();

					Thread.sleep(2000);
		 
		 ;} 
		 catch(Exception e) { PrintVal("Renderer Script Exception : "+e.toString()); } 
		 PrintVal("Executed Step = CALLRENDERERSCRIPT,SELECT_CREDITCARDNUMBER_ACCUMULATION"); 
        WAIT.$(5,"TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SAVE","//span[contains(text(),'Save')]","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_GIAS_LOGO","//img[@id='headerForm:headerLogoImage']","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

		var_CountN = var_CountN + ( 1 );
                 LOGGER.info("Executed Step = INCREASEVAR,var_CountN,1,TGTYPESCREENREG");
    	}
          LOGGER.info("Executed Step = WEND,TGTYPESCREENREG");
    	resetAttemps();

    }


    @Test
    public void nw020testplan() {

        // No need to declare / print method name here, handled by TestNg listener
        // declare("nw020testplan");

        CALL.$("LoginGIAS","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_USERNAME","//input[@id='guestForm:username']","autoapi","FALSE","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_PASSWORD","//input[@id='guestForm:password']","Autoapi!23","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_LOGIN","//button[contains(.,'Login')]","TGTYPESCREENREG");

        WAIT.$("ELE_BUTTON_GIAS_LOGO","//img[@id='headerForm:headerLogoImage']","VISIBLE","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

		String var_dataToPass;
                 LOGGER.info("Executed Step = VAR,String,var_dataToPass,TGTYPESCREENREG");
		int var_Count = 0;
                 LOGGER.info("Executed Step = VAR,Integer,var_Count,0,TGTYPESCREENREG");
		int var_CountN = 0;
                 LOGGER.info("Executed Step = VAR,Integer,var_CountN,0,TGTYPESCREENREG");
        LOGGER.info("Executed Step = REPEAT IF");
    	while (check(var_CountN,"<",1,"TGTYPESCREENREG")) {
          LOGGER.info("Executed Step = WHILE,var_CountN,<,1,TGTYPESCREENREG");
		String var_FileName = "http://10.1.104.229/json/20210524/3Zlaqn.json";
                 LOGGER.info("Executed Step = VAR,String,var_FileName,http://10.1.104.229/json/20210524/3Zlaqn.json,TGTYPESCREENREG");
		com.jayway.jsonpath.ReadContext var_CSVData = getJsonData("http://10.1.104.229/json/20210524/3Zlaqn.json");
                 LOGGER.info("Executed Step = VAR,Json,var_CSVData,http://10.1.104.229/json/20210524/3Zlaqn.json,TGTYPESCREENREG");
        CALL.$("NW020PlanSetup","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_GIAS_LOGO","//img[@id='headerForm:headerLogoImage']","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

		String var_PlanCode;
                 LOGGER.info("Executed Step = VAR,String,var_PlanCode,TGTYPESCREENREG");
		var_PlanCode = getJsonData(var_CSVData , "$.records["+var_Count+"].PlanCode");
                 LOGGER.info("Executed Step = STORE,var_PlanCode,var_CSVData,$.records[{var_Count}].PlanCode,TGTYPESCREENREG");
		String var_BenefitPlanName;
                 LOGGER.info("Executed Step = VAR,String,var_BenefitPlanName,TGTYPESCREENREG");
		var_BenefitPlanName = getJsonData(var_CSVData , "$.records["+var_Count+"].BenefitPlanName");
                 LOGGER.info("Executed Step = STORE,var_BenefitPlanName,var_CSVData,$.records[{var_Count}].BenefitPlanName,TGTYPESCREENREG");
		String var_ReduceFirstPremium;
                 LOGGER.info("Executed Step = VAR,String,var_ReduceFirstPremium,TGTYPESCREENREG");
		var_ReduceFirstPremium = getJsonData(var_CSVData , "$.records["+var_Count+"].ReduceFirstPremium");
                 LOGGER.info("Executed Step = STORE,var_ReduceFirstPremium,var_CSVData,$.records[{var_Count}].ReduceFirstPremium,TGTYPESCREENREG");
		String var_NoReduceFirstPremiumField;
                 LOGGER.info("Executed Step = VAR,String,var_NoReduceFirstPremiumField,TGTYPESCREENREG");
		var_NoReduceFirstPremiumField = getJsonData(var_CSVData , "$.records["+var_Count+"].NoReduceFirstPremiumField");
                 LOGGER.info("Executed Step = STORE,var_NoReduceFirstPremiumField,var_CSVData,$.records[{var_Count}].NoReduceFirstPremiumField,TGTYPESCREENREG");
		String var_YesReduceFirstPremiumField;
                 LOGGER.info("Executed Step = VAR,String,var_YesReduceFirstPremiumField,TGTYPESCREENREG");
		var_YesReduceFirstPremiumField = getJsonData(var_CSVData , "$.records["+var_Count+"].YesReduceFirstPremiumField");
                 LOGGER.info("Executed Step = STORE,var_YesReduceFirstPremiumField,var_CSVData,$.records[{var_Count}].YesReduceFirstPremiumField,TGTYPESCREENREG");
        SWIPE.$("DOWN","ELE_TXTBOX_SEARCHTASKS","//input[@id='workbenchForm:workbenchTabs:taskSearchContainer:taskSearchSearch']","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_SEARCHTASKS","//input[@id='workbenchForm:workbenchTabs:taskSearchContainer:taskSearchSearch']","Plan Setup","FALSE","TGTYPESCREENREG");

        WAIT.$(3,"TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SEARCHTASKS","//a[@id='workbenchForm:workbenchTabs:taskSearchContainer:tb_buttonSearch']//span[@class='fa fa-search fa-lg']","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        TAP.$("ELE_LINK_FIRST_TASKS","//button[@id='workbenchForm:workbenchTabs:taskSearchContainer:0:availableTask']","TGTYPESCREENREG");

        WAIT.$("ELE_TXTBOX_FILTER_PLANCODE","//input[@id='workbenchForm:workbenchTabs:grid:planCode_Col:filter']","VISIBLE","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_FILTER_PLANCODE","//input[@id='workbenchForm:workbenchTabs:grid:planCode_Col:filter']",var_PlanCode,"FALSE","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        TAP.$("ELE_LINK_FIRST_PLANCODE","//span[@id='workbenchForm:workbenchTabs:grid:0:planCode']","TGTYPESCREENREG");

        WAIT.$("ELE_LINK_COUPONS","//span[@class='ui-menuitem-text'][contains(text(),'Coupons')]","VISIBLE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_EDIT","//button[@id='workbenchForm:workbenchTabs:tb_buttonUpdate']","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        TAP.$("ELE_LINK_COUPONS","//span[@class='ui-menuitem-text'][contains(text(),'Coupons')]","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        ASSERT.$("ELE_LABEL_BENEFIT_PLAN","//span[@id='workbenchForm:workbenchTabs:planCode']","CONTAINS",var_BenefitPlanName,"TGTYPESCREENREG");

        ASSERT.$("ELE_LABEL_REDUCEFIRSTPREMIUM","//label[@id='workbenchForm:workbenchTabs:reducePremiumAllowed_lbl']","CONTAINS",var_ReduceFirstPremium,"TGTYPESCREENREG");

		var_dataToPass = var_NoReduceFirstPremiumField;
                 LOGGER.info("Executed Step = STORE,var_dataToPass,var_NoReduceFirstPremiumField,TGTYPESCREENREG");
        TAP.$("ELE_DRPDWN_REDUCEFIRSTPREMIUM","//label[@id='workbenchForm:workbenchTabs:reducePremiumAllowed_label']","TGTYPESCREENREG");

		try { 
		 

					Thread.sleep(1000);
					for (int i = 0; i < 2000; i++){
						WebElement selectValueOfDropDown= driver.findElement(By.xpath("//li[@id='workbenchForm:workbenchTabs:reducePremiumAllowed_"+ i +"']"));

						if (selectValueOfDropDown.getText().contains(var_dataToPass)){

		                                
							AddToReport("ASSERT," + selectValueOfDropDown.getText() +", CONTAINS,"+var_dataToPass);
						
							break;
						}
					}
					takeScreenshot();

					Thread.sleep(2000);
		 
		 ;} 
		 catch(Exception e) { PrintVal("Renderer Script Exception : "+e.toString()); } 
		 PrintVal("Executed Step = CALLRENDERERSCRIPT,CHECK_REDUCE_FIRST_PREMIUM"); 
        WAIT.$(5,"TGTYPESCREENREG");

		var_dataToPass = var_YesReduceFirstPremiumField;
                 LOGGER.info("Executed Step = STORE,var_dataToPass,var_YesReduceFirstPremiumField,TGTYPESCREENREG");
		try { 
		 

					Thread.sleep(1000);
					for (int i = 0; i < 2000; i++){
						WebElement selectValueOfDropDown= driver.findElement(By.xpath("//li[@id='workbenchForm:workbenchTabs:reducePremiumAllowed_"+ i +"']"));

						if (selectValueOfDropDown.getText().contains(var_dataToPass)){

		                                
							AddToReport("ASSERT," + selectValueOfDropDown.getText() +", CONTAINS,"+var_dataToPass);
						
							break;
						}
					}
					takeScreenshot();

					Thread.sleep(2000);
		 
		 ;} 
		 catch(Exception e) { PrintVal("Renderer Script Exception : "+e.toString()); } 
		 PrintVal("Executed Step = CALLRENDERERSCRIPT,CHECK_REDUCE_FIRST_PREMIUM"); 
        WAIT.$(5,"TGTYPESCREENREG");

        TAP.$("ELE_DRPDWN_REDUCEFIRSTPREMIUM","//label[@id='workbenchForm:workbenchTabs:reducePremiumAllowed_label']","TGTYPESCREENREG");

        TAP.$("ELE_LINK_DIVIDENDS","//span[contains(.,'Dividends')]","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        ASSERT.$("ELE_LABEL_BENEFIT_PLAN","//span[@id='workbenchForm:workbenchTabs:planCode']","CONTAINS",var_BenefitPlanName,"TGTYPESCREENREG");

        ASSERT.$("ELE_LABEL_REDUCEFIRSTPREMIUM","//label[@id='workbenchForm:workbenchTabs:reducePremiumAllowed_lbl']","CONTAINS",var_ReduceFirstPremium,"TGTYPESCREENREG");

		var_dataToPass = var_NoReduceFirstPremiumField;
                 LOGGER.info("Executed Step = STORE,var_dataToPass,var_NoReduceFirstPremiumField,TGTYPESCREENREG");
        TAP.$("ELE_DRPDWN_REDUCEFIRSTPREMIUM","//label[@id='workbenchForm:workbenchTabs:reducePremiumAllowed_label']","TGTYPESCREENREG");

		try { 
		 

					Thread.sleep(1000);
					for (int i = 0; i < 2000; i++){
						WebElement selectValueOfDropDown= driver.findElement(By.xpath("//li[@id='workbenchForm:workbenchTabs:reducePremiumAllowed_"+ i +"']"));

						if (selectValueOfDropDown.getText().contains(var_dataToPass)){

		                                
							AddToReport("ASSERT," + selectValueOfDropDown.getText() +", CONTAINS,"+var_dataToPass);
						
							break;
						}
					}
					takeScreenshot();

					Thread.sleep(2000);
		 
		 ;} 
		 catch(Exception e) { PrintVal("Renderer Script Exception : "+e.toString()); } 
		 PrintVal("Executed Step = CALLRENDERERSCRIPT,CHECK_REDUCE_FIRST_PREMIUM"); 
        WAIT.$(5,"TGTYPESCREENREG");

		var_dataToPass = var_YesReduceFirstPremiumField;
                 LOGGER.info("Executed Step = STORE,var_dataToPass,var_YesReduceFirstPremiumField,TGTYPESCREENREG");
		try { 
		 

					Thread.sleep(1000);
					for (int i = 0; i < 2000; i++){
						WebElement selectValueOfDropDown= driver.findElement(By.xpath("//li[@id='workbenchForm:workbenchTabs:reducePremiumAllowed_"+ i +"']"));

						if (selectValueOfDropDown.getText().contains(var_dataToPass)){

		                                
							AddToReport("ASSERT," + selectValueOfDropDown.getText() +", CONTAINS,"+var_dataToPass);
						
							break;
						}
					}
					takeScreenshot();

					Thread.sleep(2000);
		 
		 ;} 
		 catch(Exception e) { PrintVal("Renderer Script Exception : "+e.toString()); } 
		 PrintVal("Executed Step = CALLRENDERERSCRIPT,CHECK_REDUCE_FIRST_PREMIUM"); 
        WAIT.$(5,"TGTYPESCREENREG");

        TAP.$("ELE_DRPDWN_REDUCEFIRSTPREMIUM","//label[@id='workbenchForm:workbenchTabs:reducePremiumAllowed_label']","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_GIAS_LOGO","//img[@id='headerForm:headerLogoImage']","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        CALL.$("NW020ChangePolicyContractVerify","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_GIAS_LOGO","//img[@id='headerForm:headerLogoImage']","TGTYPESCREENREG");

		String var_PolicyNumberContract;
                 LOGGER.info("Executed Step = VAR,String,var_PolicyNumberContract,TGTYPESCREENREG");
		var_PolicyNumberContract = getJsonData(var_CSVData , "$.records["+var_Count+"].PolicyNumberContract");
                 LOGGER.info("Executed Step = STORE,var_PolicyNumberContract,var_CSVData,$.records[{var_Count}].PolicyNumberContract,TGTYPESCREENREG");
		String var_ContractPrimaryDividend;
                 LOGGER.info("Executed Step = VAR,String,var_ContractPrimaryDividend,TGTYPESCREENREG");
		var_ContractPrimaryDividend = getJsonData(var_CSVData , "$.records["+var_Count+"].ContractPrimaryDividend");
                 LOGGER.info("Executed Step = STORE,var_ContractPrimaryDividend,var_CSVData,$.records[{var_Count}].ContractPrimaryDividend,TGTYPESCREENREG");
        SWIPE.$("DOWN","ELE_TXTBOX_SEARCHTASKS","//input[@id='workbenchForm:workbenchTabs:taskSearchContainer:taskSearchSearch']","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_SEARCHTASKS","//input[@id='workbenchForm:workbenchTabs:taskSearchContainer:taskSearchSearch']","Change a Policy Contract","FALSE","TGTYPESCREENREG");

        WAIT.$(3,"TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SEARCHTASKS","//a[@id='workbenchForm:workbenchTabs:taskSearchContainer:tb_buttonSearch']//span[@class='fa fa-search fa-lg']","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        TAP.$("ELE_LINK_FIRST_TASKS","//button[@id='workbenchForm:workbenchTabs:taskSearchContainer:0:availableTask']","TGTYPESCREENREG");

        WAIT.$("ELE_TXTBOX_POLICYNUMBER","//input[@id='workbenchForm:workbenchTabs:policyNumber']","VISIBLE","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_POLICYNUMBER","//input[@id='workbenchForm:workbenchTabs:policyNumber']",var_PolicyNumberContract,"FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_CONTINUE","//span[contains(text(),'Continue')]","TGTYPESCREENREG");

        WAIT.$("ELE_BUTTON_SUBMIT","//span[contains(text(),'Submit')]","VISIBLE","TGTYPESCREENREG");

        SWIPE.$("UP","ELE_DRPDWN_PRIMARYDIVIDENDOPTION","//*[@id=\"workbenchForm:workbenchTabs:dividendOptionPrim_label\"]","TGTYPESCREENREG");

        TAP.$("ELE_DRPDWN_POLICYCONTRACT_PRIMARYDIVIDENDPRIMIUM","//label[@id='workbenchForm:workbenchTabs:dividendOptionPrim_label']","TGTYPESCREENREG");

		var_dataToPass = var_ContractPrimaryDividend;
                 LOGGER.info("Executed Step = STORE,var_dataToPass,var_ContractPrimaryDividend,TGTYPESCREENREG");
		try { 
		 



					Thread.sleep(1000);
					for (int i = 0; i < 2000; i++){
						WebElement selectValueOfDropDown= driver.findElement(By.xpath("//li[@id='workbenchForm:workbenchTabs:dividendOptionPrim_"+ i +"']"));

						if (selectValueOfDropDown.getText().contains(var_dataToPass)){

		                                
							AddToReport("ASSERT," + selectValueOfDropDown.getText() +", CONTAINS,"+var_dataToPass);
						
							break;
						}
					}
					takeScreenshot();

					Thread.sleep(2000);
		 
		 ;} 
		 catch(Exception e) { PrintVal("Renderer Script Exception : "+e.toString()); } 
		 PrintVal("Executed Step = CALLRENDERERSCRIPT,CHECK_VALUE_CONTRACT_PRIMARYDIVIDENDPREMIUM"); 
        WAIT.$(5,"TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_GIAS_LOGO","//img[@id='headerForm:headerLogoImage']","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        CALL.$("NW020BenefitPlanPrimaryDividendVerify","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_GIAS_LOGO","//img[@id='headerForm:headerLogoImage']","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

		String var_PolicyNumberBenefit;
                 LOGGER.info("Executed Step = VAR,String,var_PolicyNumberBenefit,TGTYPESCREENREG");
		var_PolicyNumberBenefit = getJsonData(var_CSVData , "$.records["+var_Count+"].PolicyNumberBenefit");
                 LOGGER.info("Executed Step = STORE,var_PolicyNumberBenefit,var_CSVData,$.records[{var_Count}].PolicyNumberBenefit,TGTYPESCREENREG");
		String var_BenefitPlan;
                 LOGGER.info("Executed Step = VAR,String,var_BenefitPlan,TGTYPESCREENREG");
		var_BenefitPlan = getJsonData(var_CSVData , "$.records["+var_Count+"].BenefitPlan");
                 LOGGER.info("Executed Step = STORE,var_BenefitPlan,var_CSVData,$.records[{var_Count}].BenefitPlan,TGTYPESCREENREG");
		String var_BenefitPrimaryDividend;
                 LOGGER.info("Executed Step = VAR,String,var_BenefitPrimaryDividend,TGTYPESCREENREG");
		var_BenefitPrimaryDividend = getJsonData(var_CSVData , "$.records["+var_Count+"].BenefitPrimaryDividend");
                 LOGGER.info("Executed Step = STORE,var_BenefitPrimaryDividend,var_CSVData,$.records[{var_Count}].BenefitPrimaryDividend,TGTYPESCREENREG");
        SWIPE.$("DOWN","ELE_TXTBOX_SEARCHTASKS","//input[@id='workbenchForm:workbenchTabs:taskSearchContainer:taskSearchSearch']","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_SEARCHTASKS","//input[@id='workbenchForm:workbenchTabs:taskSearchContainer:taskSearchSearch']","New Policy Application","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SEARCHTASKS","//a[@id='workbenchForm:workbenchTabs:taskSearchContainer:tb_buttonSearch']//span[@class='fa fa-search fa-lg']","TGTYPESCREENREG");

        WAIT.$("ELE_LINK_NEWPOLICYAPPLICATION","//span[contains(text(),'New Policy Application')]","VISIBLE","TGTYPESCREENREG");

        TAP.$("ELE_LINK_NEWPOLICYAPPLICATION","//span[contains(text(),'New Policy Application')]","TGTYPESCREENREG");

        WAIT.$("ELE_BUTTON_ADD","//button[@id='workbenchForm:workbenchTabs:tb_buttonAdd']//span[@class='ui-button-text ui-c'][contains(text(),'Add')]","VISIBLE","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_POLICYNUMBER_FILTER","//input[@id='workbenchForm:workbenchTabs:grid:policyNumber_Col:filter']",var_PolicyNumberBenefit,"FALSE","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        TAP.$("ELE_LINK_FIRST_POLICY_FROM_TABLE","//span[@id='workbenchForm:workbenchTabs:grid:0:policyNumber']","TGTYPESCREENREG");

        WAIT.$("ELE_LABEL_POLICYNUMBER","//span[@id='workbenchForm:workbenchTabs:policyNumber']","VISIBLE","TGTYPESCREENREG");

        SWIPE.$("UP","ELE_BUTTON_ADD_BENEFIT_PLUS","//button[@id='workbenchForm:workbenchTabs:benefitGrid:0:icon_button']","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_ADD_BENEFIT_PLUS","//button[@id='workbenchForm:workbenchTabs:benefitGrid:0:icon_button']","TGTYPESCREENREG");

        WAIT.$("ELE_DRPDWN_BENEFITPLAN","//label[@id='workbenchForm:workbenchTabs:initialPlanCode_label']","VISIBLE","TGTYPESCREENREG");

		var_dataToPass = var_BenefitPlan;
                 LOGGER.info("Executed Step = STORE,var_dataToPass,var_BenefitPlan,TGTYPESCREENREG");
        TAP.$("ELE_DRPDWN_BENEFITPLAN","//label[@id='workbenchForm:workbenchTabs:initialPlanCode_label']","TGTYPESCREENREG");

		try { 
		 Thread.sleep(1000);
		WebElement selectValueOfDropDown= driver.findElement(By.xpath("//li[@data-label='"+ var_dataToPass +"']"));

		takeScreenshot();

				selectValueOfDropDown.click();
		// Add Screenshot will only work for Android platform

		takeScreenshot();

				 Thread.sleep(1000);
		 
		 ;} 
		 catch(Exception e) { PrintVal("Renderer Script Exception : "+e.toString()); } 
		 PrintVal("Executed Step = CALLRENDERERSCRIPT,SELECTVALUEFROMDROPDOWN"); 
        WAIT.$(5,"TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_CONTINUE","//span[contains(text(),'Continue')]","TGTYPESCREENREG");

        WAIT.$("ELE_LABEL_POLICYNUMBER","//span[@id='workbenchForm:workbenchTabs:policyNumber']","VISIBLE","TGTYPESCREENREG");

        SWIPE.$("UP","ELE_DRPDWN_PRIMARYDIVIDENDOPETION_POLICY","//label[@id='workbenchForm:workbenchTabs:primaryDividendOption_label']","TGTYPESCREENREG");

		var_dataToPass = var_BenefitPrimaryDividend;
                 LOGGER.info("Executed Step = STORE,var_dataToPass,var_BenefitPrimaryDividend,TGTYPESCREENREG");
        TAP.$("ELE_DRPDWN_PRIMARYDIVIDENDOPETION_POLICY","//label[@id='workbenchForm:workbenchTabs:primaryDividendOption_label']","TGTYPESCREENREG");

		try { 
		 //li[@id='workbenchForm:workbenchTabs:primaryDividendOption_4']


			Thread.sleep(1000);
					for (int i = 0; i < 2000; i++){
						WebElement selectValueOfDropDown= driver.findElement(By.xpath("//li[@id='workbenchForm:workbenchTabs:primaryDividendOption_"+ i +"']"));

						if (selectValueOfDropDown.getText().contains(var_dataToPass)){

		                                
							AddToReport("ASSERT," + selectValueOfDropDown.getText() +", CONTAINS,"+var_dataToPass);
						
							break;
						}
					}
					takeScreenshot();

					Thread.sleep(2000);
		 
		 ;} 
		 catch(Exception e) { PrintVal("Renderer Script Exception : "+e.toString()); } 
		 PrintVal("Executed Step = CALLRENDERERSCRIPT,CHECK_VALUE_BENEFIT_PRIMARYDIVIDEND"); 
        WAIT.$(5,"TGTYPESCREENREG");

        TAP.$("ELE_DRPDWN_PRIMARYDIVIDENDOPETION_POLICY","//label[@id='workbenchForm:workbenchTabs:primaryDividendOption_label']","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_GIAS_LOGO","//img[@id='headerForm:headerLogoImage']","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

		var_CountN = var_CountN + ( 1 );
                 LOGGER.info("Executed Step = INCREASEVAR,var_CountN,1,TGTYPESCREENREG");
    	}
          LOGGER.info("Executed Step = WEND,TGTYPESCREENREG");
    	resetAttemps();

    }


    @Test
    public void nw056demoautomatedtest() {

        // No need to declare / print method name here, handled by TestNg listener
        // declare("nw056demoautomatedtest");

        CALL.$("LoginGIAS","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_USERNAME","//input[@id='guestForm:username']","autoapi","FALSE","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_PASSWORD","//input[@id='guestForm:password']","Autoapi!23","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_LOGIN","//button[contains(.,'Login')]","TGTYPESCREENREG");

        WAIT.$("ELE_BUTTON_GIAS_LOGO","//img[@id='headerForm:headerLogoImage']","VISIBLE","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

		String var_dataToPass;
                 LOGGER.info("Executed Step = VAR,String,var_dataToPass,TGTYPESCREENREG");
		int var_Count = 0;
                 LOGGER.info("Executed Step = VAR,Integer,var_Count,0,TGTYPESCREENREG");
		int var_CountN = 0;
                 LOGGER.info("Executed Step = VAR,Integer,var_CountN,0,TGTYPESCREENREG");
        LOGGER.info("Executed Step = REPEAT IF");
    	while (check(var_CountN,"<",1,"TGTYPESCREENREG")) {
          LOGGER.info("Executed Step = WHILE,var_CountN,<,1,TGTYPESCREENREG");
		String var_FileName = "http://10.1.104.229/json/20210524/ZXgxz5.json";
                 LOGGER.info("Executed Step = VAR,String,var_FileName,http://10.1.104.229/json/20210524/ZXgxz5.json,TGTYPESCREENREG");
		com.jayway.jsonpath.ReadContext var_CSVData = getJsonData("http://10.1.104.229/json/20210524/ZXgxz5.json");
                 LOGGER.info("Executed Step = VAR,Json,var_CSVData,http://10.1.104.229/json/20210524/ZXgxz5.json,TGTYPESCREENREG");
        CALL.$("PlanSetupForNW057DemoTestPlan","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_GIAS_LOGO","//img[@id='headerForm:headerLogoImage']","TGTYPESCREENREG");

		String var_BenefitPlanCode1;
                 LOGGER.info("Executed Step = VAR,String,var_BenefitPlanCode1,TGTYPESCREENREG");
		var_BenefitPlanCode1 = getJsonData(var_CSVData , "$.records["+var_Count+"].BenefitPlanCode1");
                 LOGGER.info("Executed Step = STORE,var_BenefitPlanCode1,var_CSVData,$.records[{var_Count}].BenefitPlanCode1,TGTYPESCREENREG");
		String var_SurrenderDividedToPayPremium1;
                 LOGGER.info("Executed Step = VAR,String,var_SurrenderDividedToPayPremium1,TGTYPESCREENREG");
		var_SurrenderDividedToPayPremium1 = getJsonData(var_CSVData , "$.records["+var_Count+"].SurrenderDividedToPayPremium1");
                 LOGGER.info("Executed Step = STORE,var_SurrenderDividedToPayPremium1,var_CSVData,$.records[{var_Count}].SurrenderDividedToPayPremium1,TGTYPESCREENREG");
        WAIT.$(5,"TGTYPESCREENREG");

        SWIPE.$("DOWN","ELE_TXTBOX_SEARCHTASKS","//input[@id='workbenchForm:workbenchTabs:taskSearchContainer:taskSearchSearch']","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_SEARCHTASKS","//input[@id='workbenchForm:workbenchTabs:taskSearchContainer:taskSearchSearch']","Plan Setup","FALSE","TGTYPESCREENREG");

        WAIT.$(3,"TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SEARCHTASKS","//a[@id='workbenchForm:workbenchTabs:taskSearchContainer:tb_buttonSearch']//span[@class='fa fa-search fa-lg']","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        TAP.$("ELE_LINK_FIRST_TASKS","//button[@id='workbenchForm:workbenchTabs:taskSearchContainer:0:availableTask']","TGTYPESCREENREG");

        WAIT.$("ELE_TXTBOX_FILTER_PLANCODE","//input[@id='workbenchForm:workbenchTabs:grid:planCode_Col:filter']","VISIBLE","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_FILTER_PLANCODE","//input[@id='workbenchForm:workbenchTabs:grid:planCode_Col:filter']",var_BenefitPlanCode1,"FALSE","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        TAP.$("ELE_LINK_FIRST_PLANCODE","//span[@id='workbenchForm:workbenchTabs:grid:0:planCode']","TGTYPESCREENREG");

        WAIT.$("ELE_BUTTON_EDIT","//button[@id='workbenchForm:workbenchTabs:tb_buttonUpdate']","VISIBLE","TGTYPESCREENREG");

        TAP.$("ELE_TAB_NONFORFEITURE","//span[contains(text(),'Nonforfeiture')]","TGTYPESCREENREG");

        WAIT.$(4,"TGTYPESCREENREG");

		String var_CheckSurrender = "PASS";
                 LOGGER.info("Executed Step = VAR,String,var_CheckSurrender,PASS,TGTYPESCREENREG");
        SWIPE.$("UP","TGTYPESCREENREG");

        LOGGER.info("Executed Step = START IF");
        if (check("ELE_LABEL_SURRENDERDIVIDENDSTOPAYPREMIUMS","//span[@id='workbenchForm:workbenchTabs:divSurrAlloForNFO']","<>",var_SurrenderDividedToPayPremium1,"TGTYPESCREENREG")) {
         LOGGER.info("Executed Step = IF,ELE_LABEL_SURRENDERDIVIDENDSTOPAYPREMIUMS,<>,var_SurrenderDividedToPayPremium1,TGTYPESCREENREG");
		var_CheckSurrender = "FAIL";
                 LOGGER.info("Executed Step = STORE,var_CheckSurrender,FAIL,TGTYPESCREENREG");
        }
          LOGGER.info("Executed Step = ENDIF,TGTYPESCREENREG");
        LOGGER.info("Executed Step = START IF");
        if (check("ELE_LABEL_DEFAULTNONFOROPETION","//span[@id='workbenchForm:workbenchTabs:defaultNonForfOpt']","<>",var_SurrenderDividedToPayPremium1,"TGTYPESCREENREG")) {
         LOGGER.info("Executed Step = IF,ELE_LABEL_DEFAULTNONFOROPETION,<>,var_SurrenderDividedToPayPremium1,TGTYPESCREENREG");
		var_CheckSurrender = "FAIL";
                 LOGGER.info("Executed Step = STORE,var_CheckSurrender,FAIL,TGTYPESCREENREG");
        }
          LOGGER.info("Executed Step = ENDIF,TGTYPESCREENREG");
        LOGGER.info("Executed Step = START IF");
        if (check(var_CheckSurrender,"=","FAIL","TGTYPESCREENREG")) {
         LOGGER.info("Executed Step = IF,var_CheckSurrender,=,FAIL,TGTYPESCREENREG");
        TAP.$("ELE_BUTTON_EDIT","//button[@id='workbenchForm:workbenchTabs:tb_buttonUpdate']","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

		var_dataToPass = var_SurrenderDividedToPayPremium1;
                 LOGGER.info("Executed Step = STORE,var_dataToPass,var_SurrenderDividedToPayPremium1,TGTYPESCREENREG");
        TAP.$("ELE_DRPDWN_SURRENDERDIVIDENDSTOPAYPREMIUMS","//label[@id='workbenchForm:workbenchTabs:divSurrAlloForNFO_label']","TGTYPESCREENREG");

		try { 
		 //li[@id='workbenchForm:workbenchTabs:divSurrAlloForNFO_3']


			Thread.sleep(1000);
					for (int i = 0; i < 2000; i++){
						WebElement selectValueOfDropDown= driver.findElement(By.xpath("//li[@id='workbenchForm:workbenchTabs:divSurrAlloForNFO_"+ i +"']"));

						if (selectValueOfDropDown.getText().contains(var_dataToPass)){
		AddToReport("ASSERT," + selectValueOfDropDown.getText() +", CONTAINS,"+var_dataToPass);
		                                selectValueOfDropDown.click();
							
						
							break;
						}
					}
					takeScreenshot();

					Thread.sleep(2000);
		 
		 ;} 
		 catch(Exception e) { PrintVal("Renderer Script Exception : "+e.toString()); } 
		 PrintVal("Executed Step = CALLRENDERERSCRIPT,SELECT_SURRENDERDIVIDENDSTOPAYPREMIUMS "); 
        WAIT.$(5,"TGTYPESCREENREG");

		var_dataToPass = var_SurrenderDividedToPayPremium1;
                 LOGGER.info("Executed Step = STORE,var_dataToPass,var_SurrenderDividedToPayPremium1,TGTYPESCREENREG");
        TAP.$("ELE_DRPDWN_NONFORFEITUREOPTION","//label[@id='workbenchForm:workbenchTabs:defaultNonForfOpt_label']","TGTYPESCREENREG");

		try { 
		 //li[@id='workbenchForm:workbenchTabs:defaultNonForfOpt_5']


			Thread.sleep(1000);
					for (int i = 0; i < 2000; i++){
						WebElement selectValueOfDropDown= driver.findElement(By.xpath("//li[@id='workbenchForm:workbenchTabs:defaultNonForfOpt_"+ i +"']"));

						if (selectValueOfDropDown.getText().contains(var_dataToPass)){
		AddToReport("ASSERT," + selectValueOfDropDown.getText() +", CONTAINS,"+var_dataToPass);
		                                selectValueOfDropDown.click();
							
						
							break;
						}
					}
					takeScreenshot();

					Thread.sleep(2000);
		 
		 ;} 
		 catch(Exception e) { PrintVal("Renderer Script Exception : "+e.toString()); } 
		 PrintVal("Executed Step = CALLRENDERERSCRIPT,SELECT_DEFAIULT_OPETION_NONFOR"); 
        WAIT.$(5,"TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SAVE","//span[contains(text(),'Save')]","TGTYPESCREENREG");

        SWIPE.$("DOWN","TGTYPESCREENREG");

        SWIPE.$("DOWN","TGTYPESCREENREG");

        SWIPE.$("DOWN","TGTYPESCREENREG");

        ASSERT.$("ELE_MSG_SUCCESSFULLY","//span[contains(@class,'ui-messages-info-summary')]","VISIBLE","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        }
          LOGGER.info("Executed Step = ENDIF,TGTYPESCREENREG");
        TAP.$("ELE_BUTTON_GIAS_LOGO","//img[@id='headerForm:headerLogoImage']","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        CALL.$("ChangeSessionDate1","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_GIAS_LOGO","//img[@id='headerForm:headerLogoImage']","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

		String var_SessionDate1;
                 LOGGER.info("Executed Step = VAR,String,var_SessionDate1,TGTYPESCREENREG");
		var_SessionDate1 = getJsonData(var_CSVData , "$.records["+var_Count+"].SessionDate1");
                 LOGGER.info("Executed Step = STORE,var_SessionDate1,var_CSVData,$.records[{var_Count}].SessionDate1,TGTYPESCREENREG");
        TAP.$("ELE_BUTTON_SESSIONDATE","//a[@id='headerForm:sessionDatelink']","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_SESSIONDATE","//input[@id='sessionDateForm:sessionDateInput_input']",var_SessionDate1,"FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_CHANGEDATE","//span[contains(text(),'Change Date')]","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        CALL.$("CreatePolicyForReg056","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_GIAS_LOGO","//img[@id='headerForm:headerLogoImage']","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

		String var_PolicyNumber;
                 LOGGER.info("Executed Step = VAR,String,var_PolicyNumber,TGTYPESCREENREG");
		var_PolicyNumber = getJsonData(var_CSVData , "$.records["+var_Count+"].PolicyNumber");
                 LOGGER.info("Executed Step = STORE,var_PolicyNumber,var_CSVData,$.records[{var_Count}].PolicyNumber,TGTYPESCREENREG");
		String var_EffectiveDate;
                 LOGGER.info("Executed Step = VAR,String,var_EffectiveDate,TGTYPESCREENREG");
		var_EffectiveDate = getJsonData(var_CSVData , "$.records["+var_Count+"].EffectiveDate");
                 LOGGER.info("Executed Step = STORE,var_EffectiveDate,var_CSVData,$.records[{var_Count}].EffectiveDate,TGTYPESCREENREG");
		String var_CashWithApplication;
                 LOGGER.info("Executed Step = VAR,String,var_CashWithApplication,TGTYPESCREENREG");
		var_CashWithApplication = getJsonData(var_CSVData , "$.records["+var_Count+"].CashWithApplication");
                 LOGGER.info("Executed Step = STORE,var_CashWithApplication,var_CSVData,$.records[{var_Count}].CashWithApplication,TGTYPESCREENREG");
		String var_PaymentFrequency;
                 LOGGER.info("Executed Step = VAR,String,var_PaymentFrequency,TGTYPESCREENREG");
		var_PaymentFrequency = getJsonData(var_CSVData , "$.records["+var_Count+"].PaymentFrequency");
                 LOGGER.info("Executed Step = STORE,var_PaymentFrequency,var_CSVData,$.records[{var_Count}].PaymentFrequency,TGTYPESCREENREG");
		String var_PaymentMethod;
                 LOGGER.info("Executed Step = VAR,String,var_PaymentMethod,TGTYPESCREENREG");
		var_PaymentMethod = getJsonData(var_CSVData , "$.records["+var_Count+"].PaymentMethod");
                 LOGGER.info("Executed Step = STORE,var_PaymentMethod,var_CSVData,$.records[{var_Count}].PaymentMethod,TGTYPESCREENREG");
		String var_IssueState;
                 LOGGER.info("Executed Step = VAR,String,var_IssueState,TGTYPESCREENREG");
		var_IssueState = getJsonData(var_CSVData , "$.records["+var_Count+"].IssueState");
                 LOGGER.info("Executed Step = STORE,var_IssueState,var_CSVData,$.records[{var_Count}].IssueState,TGTYPESCREENREG");
		String var_AgentNumber;
                 LOGGER.info("Executed Step = VAR,String,var_AgentNumber,TGTYPESCREENREG");
		var_AgentNumber = getJsonData(var_CSVData , "$.records["+var_Count+"].AgentNumber");
                 LOGGER.info("Executed Step = STORE,var_AgentNumber,var_CSVData,$.records[{var_Count}].AgentNumber,TGTYPESCREENREG");
		String var_SplitPercentage;
                 LOGGER.info("Executed Step = VAR,String,var_SplitPercentage,TGTYPESCREENREG");
		var_SplitPercentage = getJsonData(var_CSVData , "$.records["+var_Count+"].SplitPercentage");
                 LOGGER.info("Executed Step = STORE,var_SplitPercentage,var_CSVData,$.records[{var_Count}].SplitPercentage,TGTYPESCREENREG");
		String var_ClientCheckName;
                 LOGGER.info("Executed Step = VAR,String,var_ClientCheckName,TGTYPESCREENREG");
		var_ClientCheckName = getJsonData(var_CSVData , "$.records["+var_Count+"].ClientCheckName");
                 LOGGER.info("Executed Step = STORE,var_ClientCheckName,var_CSVData,$.records[{var_Count}].ClientCheckName,TGTYPESCREENREG");
		String var_ClientFullName;
                 LOGGER.info("Executed Step = VAR,String,var_ClientFullName,TGTYPESCREENREG");
		var_ClientFullName = getJsonData(var_CSVData , "$.records["+var_Count+"].ClientFullName");
                 LOGGER.info("Executed Step = STORE,var_ClientFullName,var_CSVData,$.records[{var_Count}].ClientFullName,TGTYPESCREENREG");
		String var_ClientID;
                 LOGGER.info("Executed Step = VAR,String,var_ClientID,TGTYPESCREENREG");
		var_ClientID = getJsonData(var_CSVData , "$.records["+var_Count+"].ClientID");
                 LOGGER.info("Executed Step = STORE,var_ClientID,var_CSVData,$.records[{var_Count}].ClientID,TGTYPESCREENREG");
		String var_ClientBirthDate;
                 LOGGER.info("Executed Step = VAR,String,var_ClientBirthDate,TGTYPESCREENREG");
		var_ClientBirthDate = getJsonData(var_CSVData , "$.records["+var_Count+"].ClientBirthDate");
                 LOGGER.info("Executed Step = STORE,var_ClientBirthDate,var_CSVData,$.records[{var_Count}].ClientBirthDate,TGTYPESCREENREG");
        SWIPE.$("DOWN","ELE_TXTBOX_SEARCHTASKS","//input[@id='workbenchForm:workbenchTabs:taskSearchContainer:taskSearchSearch']","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_SEARCHTASKS","//input[@id='workbenchForm:workbenchTabs:taskSearchContainer:taskSearchSearch']","New Policy Application","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SEARCHTASKS","//a[@id='workbenchForm:workbenchTabs:taskSearchContainer:tb_buttonSearch']//span[@class='fa fa-search fa-lg']","TGTYPESCREENREG");

        WAIT.$("ELE_LINK_NEWPOLICYAPPLICATION","//span[contains(text(),'New Policy Application')]","VISIBLE","TGTYPESCREENREG");

        TAP.$("ELE_LINK_NEWPOLICYAPPLICATION","//span[contains(text(),'New Policy Application')]","TGTYPESCREENREG");

        WAIT.$("ELE_BUTTON_ADD","//button[@id='workbenchForm:workbenchTabs:tb_buttonAdd']//span[@class='ui-button-text ui-c'][contains(text(),'Add')]","VISIBLE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_ADD","//button[@id='workbenchForm:workbenchTabs:tb_buttonAdd']//span[@class='ui-button-text ui-c'][contains(text(),'Add')]","TGTYPESCREENREG");

        WAIT.$("ELE_TXTBOX_POLICYNUMBER","//input[@id='workbenchForm:workbenchTabs:policyNumber']","VISIBLE","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_POLICYNUMBER","//input[@id='workbenchForm:workbenchTabs:policyNumber']",var_PolicyNumber,"FALSE","TGTYPESCREENREG");

        TAP.$("ELE_DRPDWN_PAYMENTFREQUENCY","//label[@id='workbenchForm:workbenchTabs:paymentMode_label']","TGTYPESCREENREG");

		var_dataToPass = var_PaymentFrequency;
                 LOGGER.info("Executed Step = STORE,var_dataToPass,var_PaymentFrequency,TGTYPESCREENREG");
		try { 
		 Thread.sleep(1000);
		WebElement selectValueOfDropDown= driver.findElement(By.xpath("//li[@data-label='"+ var_dataToPass +"']"));

		takeScreenshot();

				selectValueOfDropDown.click();
		// Add Screenshot will only work for Android platform

		takeScreenshot();

				 Thread.sleep(1000);
		 
		 ;} 
		 catch(Exception e) { PrintVal("Renderer Script Exception : "+e.toString()); } 
		 PrintVal("Executed Step = CALLRENDERERSCRIPT,SELECTVALUEFROMDROPDOWN"); 
        WAIT.$(5,"TGTYPESCREENREG");

        TAP.$("ELE_DRPDWN_PAYMENTMETHOD","//label[@id='workbenchForm:workbenchTabs:paymentCode_label']","TGTYPESCREENREG");

		var_dataToPass = var_PaymentMethod;
                 LOGGER.info("Executed Step = STORE,var_dataToPass,var_PaymentMethod,TGTYPESCREENREG");
		try { 
		 Thread.sleep(1000);
		WebElement selectValueOfDropDown= driver.findElement(By.xpath("//li[@data-label='"+ var_dataToPass +"']"));

		takeScreenshot();

				selectValueOfDropDown.click();
		// Add Screenshot will only work for Android platform

		takeScreenshot();

				 Thread.sleep(1000);
		 
		 ;} 
		 catch(Exception e) { PrintVal("Renderer Script Exception : "+e.toString()); } 
		 PrintVal("Executed Step = CALLRENDERERSCRIPT,SELECTVALUEFROMDROPDOWN"); 
        WAIT.$(5,"TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_CASHWITHAPPLICATION","//input[@id='workbenchForm:workbenchTabs:cashWithApplication_input']",var_CashWithApplication,"FALSE","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_EFFECTIVEDATE","//input[@id='workbenchForm:workbenchTabs:effectiveDate_input']",var_EffectiveDate,"FALSE","TGTYPESCREENREG");

		var_dataToPass = var_IssueState;
                 LOGGER.info("Executed Step = STORE,var_dataToPass,var_IssueState,TGTYPESCREENREG");
        TAP.$("ELE_DRPDWN_ISSUESTATE","//label[@id='workbenchForm:workbenchTabs:countryAndStateCode_label']","TGTYPESCREENREG");

		try { 
		 Thread.sleep(1000);
		WebElement selectValueOfDropDown= driver.findElement(By.xpath("//li[@data-label='"+ var_dataToPass +"']"));

		takeScreenshot();

				selectValueOfDropDown.click();
		// Add Screenshot will only work for Android platform

		takeScreenshot();

				 Thread.sleep(1000);
		 
		 ;} 
		 catch(Exception e) { PrintVal("Renderer Script Exception : "+e.toString()); } 
		 PrintVal("Executed Step = CALLRENDERERSCRIPT,SELECTVALUEFROMDROPDOWN"); 
        WAIT.$(5,"TGTYPESCREENREG");

        SWIPE.$("UP","ELE_BUTTON_ADDAGENT_PLUS","//button[@id='workbenchForm:workbenchTabs:agentGrid:0:icon_button']","TGTYPESCREENREG");

        SWIPE.$("UP","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_ADDAGENT_PLUS","//button[@id='workbenchForm:workbenchTabs:agentGrid:0:icon_button']","TGTYPESCREENREG");

        WAIT.$("ELE_TXTBOX_AGENTNUMBER","//input[@id='workbenchForm:workbenchTabs:agentNumber']","VISIBLE","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_AGENTNUMBER","//input[@id='workbenchForm:workbenchTabs:agentNumber']",var_AgentNumber,"FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_AGENTSEARCH","//button[@id='workbenchForm:workbenchTabs:agentNumber_icon']","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_SPLITPERCENTAGE","//input[@id='workbenchForm:workbenchTabs:agentSplitPercentage_input']",var_SplitPercentage,"FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SAVE","//span[contains(text(),'Save')]","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SHOWLINKS","//button[@id='workbenchForm:workbenchTabs:tb_buttonLink']","TGTYPESCREENREG");

        TAP.$("ELE_LINK_SELECTCLIENT","//span[contains(text(),'Select Client')]","TGTYPESCREENREG");

        WAIT.$("ELE_TXTBOX_CLIENTSEARCHNAME","//input[@id='workbenchForm:workbenchTabs:searchName']","VISIBLE","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_CLIENTSEARCHNAME","//input[@id='workbenchForm:workbenchTabs:searchName']",var_ClientCheckName,"FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SEARCH","//button[@id='workbenchForm:workbenchTabs:button_search']","TGTYPESCREENREG");

        WAIT.$("ELE_TXTBOX_FILTER_CLIENTNAME","//input[@id='workbenchForm:workbenchTabs:clientGrid:individualName_Col:filter']","VISIBLE","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_FILTER_CLIENTID","//input[@id='workbenchForm:workbenchTabs:clientGrid:clientId_Col:filter']",var_ClientID,"FALSE","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        TAP.$("ELE_LINK_FIRST_CLIENT","//span[@id='workbenchForm:workbenchTabs:clientGrid:0:individualName']","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_TRUE","//button[@id='workbenchForm:workbenchTabs:button_select']//span[@class='ui-button-icon-left ui-icon ui-c fa fa-check']","TGTYPESCREENREG");

        WAIT.$("ELE_BUTTON_CONTINUE","//span[contains(text(),'Continue')]","VISIBLE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_CONTINUE","//span[contains(text(),'Continue')]","TGTYPESCREENREG");

        WAIT.$("ELE_LABEL_OWNERNAME","//span[@id='workbenchForm:workbenchTabs:ownerLabel']","VISIBLE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SAVE","//span[contains(text(),'Save')]","TGTYPESCREENREG");

        WAIT.$("ELE_MESSAGE_ITEMSUCCESFULLYADDED","//div[@id='workbenchForm:workbenchTabs:messages']//span[@class='ui-messages-info-summary'][contains(text(),'Item successfully added.')]","VISIBLE","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        CALL.$("BenefitPlanForCreatePolicyNW056","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

		String var_BenefitPlan;
                 LOGGER.info("Executed Step = VAR,String,var_BenefitPlan,TGTYPESCREENREG");
		var_BenefitPlan = getJsonData(var_CSVData , "$.records["+var_Count+"].BenefitPlan");
                 LOGGER.info("Executed Step = STORE,var_BenefitPlan,var_CSVData,$.records[{var_Count}].BenefitPlan,TGTYPESCREENREG");
		String var_Units;
                 LOGGER.info("Executed Step = VAR,String,var_Units,TGTYPESCREENREG");
		var_Units = getJsonData(var_CSVData , "$.records["+var_Count+"].Units");
                 LOGGER.info("Executed Step = STORE,var_Units,var_CSVData,$.records[{var_Count}].Units,TGTYPESCREENREG");
		String var_IssueAge;
                 LOGGER.info("Executed Step = VAR,String,var_IssueAge,TGTYPESCREENREG");
		var_IssueAge = getJsonData(var_CSVData , "$.records["+var_Count+"].IssueAge");
                 LOGGER.info("Executed Step = STORE,var_IssueAge,var_CSVData,$.records[{var_Count}].IssueAge,TGTYPESCREENREG");
		String var_RiskClass;
                 LOGGER.info("Executed Step = VAR,String,var_RiskClass,TGTYPESCREENREG");
		var_RiskClass = getJsonData(var_CSVData , "$.records["+var_Count+"].RiskClass");
                 LOGGER.info("Executed Step = STORE,var_RiskClass,var_CSVData,$.records[{var_Count}].RiskClass,TGTYPESCREENREG");
		String var_NonforfeitureOption;
                 LOGGER.info("Executed Step = VAR,String,var_NonforfeitureOption,TGTYPESCREENREG");
		var_NonforfeitureOption = getJsonData(var_CSVData , "$.records["+var_Count+"].NonforfeitureOption");
                 LOGGER.info("Executed Step = STORE,var_NonforfeitureOption,var_CSVData,$.records[{var_Count}].NonforfeitureOption,TGTYPESCREENREG");
		String var_PrimaryDividend;
                 LOGGER.info("Executed Step = VAR,String,var_PrimaryDividend,TGTYPESCREENREG");
		var_PrimaryDividend = getJsonData(var_CSVData , "$.records["+var_Count+"].PrimaryDividend");
                 LOGGER.info("Executed Step = STORE,var_PrimaryDividend,var_CSVData,$.records[{var_Count}].PrimaryDividend,TGTYPESCREENREG");
		String var_SecondaryDividend;
                 LOGGER.info("Executed Step = VAR,String,var_SecondaryDividend,TGTYPESCREENREG");
		var_SecondaryDividend = getJsonData(var_CSVData , "$.records["+var_Count+"].SecondaryDividend");
                 LOGGER.info("Executed Step = STORE,var_SecondaryDividend,var_CSVData,$.records[{var_Count}].SecondaryDividend,TGTYPESCREENREG");
        TAP.$("ELE_BUTTON_SHOWLINKS","//button[@id='workbenchForm:workbenchTabs:tb_buttonLink']","TGTYPESCREENREG");

        TAP.$("ELE_LINK_BENEFITS","//span[contains(text(),'Benefits')]","TGTYPESCREENREG");

        WAIT.$("ELE_DRPDWN_BENEFITPLAN","//label[@id='workbenchForm:workbenchTabs:initialPlanCode_label']","VISIBLE","TGTYPESCREENREG");

        TAP.$("ELE_DRPDWN_BENEFITPLAN","//label[@id='workbenchForm:workbenchTabs:initialPlanCode_label']","TGTYPESCREENREG");

		var_dataToPass = var_BenefitPlan;
                 LOGGER.info("Executed Step = STORE,var_dataToPass,var_BenefitPlan,TGTYPESCREENREG");
		try { 
		 Thread.sleep(1000);
		WebElement selectValueOfDropDown= driver.findElement(By.xpath("//li[@data-label='"+ var_dataToPass +"']"));

		takeScreenshot();

				selectValueOfDropDown.click();
		// Add Screenshot will only work for Android platform

		takeScreenshot();

				 Thread.sleep(1000);
		 
		 ;} 
		 catch(Exception e) { PrintVal("Renderer Script Exception : "+e.toString()); } 
		 PrintVal("Executed Step = CALLRENDERERSCRIPT,SELECTVALUEFROMDROPDOWN"); 
        WAIT.$(5,"TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_CONTINUE","//span[contains(text(),'Continue')]","TGTYPESCREENREG");

        WAIT.$("ELE_TXTBOX_UNITS","//input[@id='workbenchForm:workbenchTabs:unitsOfInsurance_input']","VISIBLE","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_UNITS","//input[@id='workbenchForm:workbenchTabs:unitsOfInsurance_input']",var_Units,"FALSE","TGTYPESCREENREG");

		var_dataToPass = var_RiskClass;
                 LOGGER.info("Executed Step = STORE,var_dataToPass,var_RiskClass,TGTYPESCREENREG");
        TAP.$("ELE_DRPDWN_RISK_CLASS","//label[@id='workbenchForm:workbenchTabs:smokerCode_label']","TGTYPESCREENREG");

		try { 
		 Thread.sleep(1000);
		WebElement selectValueOfDropDown= driver.findElement(By.xpath("//li[@data-label='"+ var_dataToPass +"']"));

		takeScreenshot();

				selectValueOfDropDown.click();
		// Add Screenshot will only work for Android platform

		takeScreenshot();

				 Thread.sleep(1000);
		 
		 ;} 
		 catch(Exception e) { PrintVal("Renderer Script Exception : "+e.toString()); } 
		 PrintVal("Executed Step = CALLRENDERERSCRIPT,SELECTVALUEFROMDROPDOWN"); 
        WAIT.$(5,"TGTYPESCREENREG");

		var_dataToPass = var_NonforfeitureOption;
                 LOGGER.info("Executed Step = STORE,var_dataToPass,var_NonforfeitureOption,TGTYPESCREENREG");
        TAP.$("ELE_DRPDWN_NONFORFEITUREOPTION","//label[@id='workbenchForm:workbenchTabs:defaultNonForfOpt_label']","TGTYPESCREENREG");

		try { 
		 Thread.sleep(1000);
		WebElement selectValueOfDropDown= driver.findElement(By.xpath("//li[@data-label='"+ var_dataToPass +"']"));

		takeScreenshot();

				selectValueOfDropDown.click();
		// Add Screenshot will only work for Android platform

		takeScreenshot();

				 Thread.sleep(1000);
		 
		 ;} 
		 catch(Exception e) { PrintVal("Renderer Script Exception : "+e.toString()); } 
		 PrintVal("Executed Step = CALLRENDERERSCRIPT,SELECTVALUEFROMDROPDOWN"); 
        WAIT.$(5,"TGTYPESCREENREG");

		var_dataToPass = var_PrimaryDividend;
                 LOGGER.info("Executed Step = STORE,var_dataToPass,var_PrimaryDividend,TGTYPESCREENREG");
        TAP.$("ELE_DRPDWN_PRIMARYDIVIDENDOPETION_POLICY","//label[@id='workbenchForm:workbenchTabs:primaryDividendOption_label']","TGTYPESCREENREG");

		try { 
		 Thread.sleep(1000);
		WebElement selectValueOfDropDown= driver.findElement(By.xpath("//li[@data-label='"+ var_dataToPass +"']"));

		takeScreenshot();

				selectValueOfDropDown.click();
		// Add Screenshot will only work for Android platform

		takeScreenshot();

				 Thread.sleep(1000);
		 
		 ;} 
		 catch(Exception e) { PrintVal("Renderer Script Exception : "+e.toString()); } 
		 PrintVal("Executed Step = CALLRENDERERSCRIPT,SELECTVALUEFROMDROPDOWN"); 
        WAIT.$(5,"TGTYPESCREENREG");

		var_dataToPass = var_SecondaryDividend;
                 LOGGER.info("Executed Step = STORE,var_dataToPass,var_SecondaryDividend,TGTYPESCREENREG");
        TAP.$("ELE_DRPDWN_SECONDARY_DIVIDEND","//label[@id='workbenchForm:workbenchTabs:secondaryDividendOption_label']","TGTYPESCREENREG");

		try { 
		 Thread.sleep(1000);
		WebElement selectValueOfDropDown= driver.findElement(By.xpath("//li[@data-label='"+ var_dataToPass +"']"));

		takeScreenshot();

				selectValueOfDropDown.click();
		// Add Screenshot will only work for Android platform

		takeScreenshot();

				 Thread.sleep(1000);
		 
		 ;} 
		 catch(Exception e) { PrintVal("Renderer Script Exception : "+e.toString()); } 
		 PrintVal("Executed Step = CALLRENDERERSCRIPT,SELECTVALUEFROMDROPDOWN"); 
        WAIT.$(5,"TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SAVE","//span[contains(text(),'Save')]","TGTYPESCREENREG");

        WAIT.$("ELE_MESSAGE_ITEMSUCCESFULLYADDED","//div[@id='workbenchForm:workbenchTabs:messages']//span[@class='ui-messages-info-summary'][contains(text(),'Item successfully added.')]","VISIBLE","TGTYPESCREENREG");

        TAP.$("ELE_TAB_CLOSE","//li[@class='ui-state-default ui-tabs-selected ui-state-active ui-corner-top']//span[@class='ui-icon ui-icon-close']","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        CALL.$("IssuePolicyForCreatePolicyNW056","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SHOWLINKS","//button[@id='workbenchForm:workbenchTabs:tb_buttonLink']","TGTYPESCREENREG");

        TAP.$("ELE_LINK_ISSUE","//span[@class='ui-menuitem-text'][contains(text(),'Issue')]","TGTYPESCREENREG");

        WAIT.$("ELE_BUTTON_SUBMIT","//span[contains(text(),'Submit')]","VISIBLE","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SUBMIT","//span[contains(text(),'Submit')]","TGTYPESCREENREG");

        WAIT.$("ELE_MSG_REQUESTCOMPLETEDSUCCESSFULLY","//div[@id='workbenchForm:workbenchTabs:messages']//span[@class='ui-messages-info-summary'][contains(text(),'Request completed successfully.')]","VISIBLE","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        TAP.$("ELE_TAB_CLOSE","//li[@class='ui-state-default ui-tabs-selected ui-state-active ui-corner-top']//span[@class='ui-icon ui-icon-close']","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        CALL.$("SettlePolicyForCreatePolicyNW056","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SHOWLINKS","//button[@id='workbenchForm:workbenchTabs:tb_buttonLink']","TGTYPESCREENREG");

        TAP.$("ELE_LINK_SETTLE","//span[contains(text(),'Settle')]","TGTYPESCREENREG");

        WAIT.$("ELE_BUTTON_SUBMIT","//span[contains(text(),'Submit')]","VISIBLE","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        SWIPE.$("DOWN","TGTYPESCREENREG");

        SWIPE.$("DOWN","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SUBMIT","//span[contains(text(),'Submit')]","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        WAIT.$("ELE_MSG_REQUESTCOMPLETEDSUCCESSFULLY","//div[@id='workbenchForm:workbenchTabs:messages']//span[@class='ui-messages-info-summary'][contains(text(),'Request completed successfully.')]","VISIBLE","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        TAP.$("ELE_TAB_CLOSE","//li[@class='ui-state-default ui-tabs-selected ui-state-active ui-corner-top']//span[@class='ui-icon ui-icon-close']","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        CALL.$("PremiumPolicyForCreatePolicyNW056","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

		String var_PremiumAnnual;
                 LOGGER.info("Executed Step = VAR,String,var_PremiumAnnual,TGTYPESCREENREG");
		var_PremiumAnnual = getJsonData(var_CSVData , "$.records["+var_Count+"].PremiumAnnual");
                 LOGGER.info("Executed Step = STORE,var_PremiumAnnual,var_CSVData,$.records[{var_Count}].PremiumAnnual,TGTYPESCREENREG");
		String var_PremiumSemiannual;
                 LOGGER.info("Executed Step = VAR,String,var_PremiumSemiannual,TGTYPESCREENREG");
		var_PremiumSemiannual = getJsonData(var_CSVData , "$.records["+var_Count+"].PremiumSemiannual");
                 LOGGER.info("Executed Step = STORE,var_PremiumSemiannual,var_CSVData,$.records[{var_Count}].PremiumSemiannual,TGTYPESCREENREG");
		String var_PremiumQuarterly;
                 LOGGER.info("Executed Step = VAR,String,var_PremiumQuarterly,TGTYPESCREENREG");
		var_PremiumQuarterly = getJsonData(var_CSVData , "$.records["+var_Count+"].PremiumQuarterly");
                 LOGGER.info("Executed Step = STORE,var_PremiumQuarterly,var_CSVData,$.records[{var_Count}].PremiumQuarterly,TGTYPESCREENREG");
		String var_PremiumMonthly;
                 LOGGER.info("Executed Step = VAR,String,var_PremiumMonthly,TGTYPESCREENREG");
		var_PremiumMonthly = getJsonData(var_CSVData , "$.records["+var_Count+"].PremiumMonthly");
                 LOGGER.info("Executed Step = STORE,var_PremiumMonthly,var_CSVData,$.records[{var_Count}].PremiumMonthly,TGTYPESCREENREG");
        TAP.$("ELE_BUTTON_SHOWLINKS","//button[@id='workbenchForm:workbenchTabs:tb_buttonLink']","TGTYPESCREENREG");

        TAP.$("ELE_LINK_PREMIUMS","//span[contains(text(),'Premiums')]","TGTYPESCREENREG");

        WAIT.$("ELE_LINK_FIRST_ANNUAL_PREMIUM","//span[@id='workbenchForm:workbenchTabs:gridNavGrid:0:benefitAnnualPremium']","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        ASSERT.$("ELE_LINK_FIRST_ANNUAL_PREMIUM","//span[@id='workbenchForm:workbenchTabs:gridNavGrid:0:benefitAnnualPremium']","CONTAINS",var_PremiumAnnual,"TGTYPESCREENREG");

        ASSERT.$("ELE_LINK_FIRST_SEMI_ANNUAL","//span[@id='workbenchForm:workbenchTabs:gridNavGrid:0:benefitSemiAnnuPrem']","CONTAINS",var_PremiumSemiannual,"TGTYPESCREENREG");

        ASSERT.$("ELE_LINK_FIRST_QUATERLY_PREMIUM","//span[@id='workbenchForm:workbenchTabs:gridNavGrid:0:benefitQuarterlyPrem']","CONTAINS",var_PremiumQuarterly,"TGTYPESCREENREG");

        ASSERT.$("ELE_LINK_FIRST_MONTHLY_PREMIUM","//span[@id='workbenchForm:workbenchTabs:gridNavGrid:0:benefitMonthlyPrem']","CONTAINS",var_PremiumMonthly,"TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_GIAS_LOGO","//img[@id='headerForm:headerLogoImage']","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_GIAS_LOGO","//img[@id='headerForm:headerLogoImage']","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        CALL.$("PolicyPaymentForNW056","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_GIAS_LOGO","//img[@id='headerForm:headerLogoImage']","TGTYPESCREENREG");

		String var_PaidToDate2;
                 LOGGER.info("Executed Step = VAR,String,var_PaidToDate2,TGTYPESCREENREG");
		var_PaidToDate2 = getJsonData(var_CSVData , "$.records["+var_Count+"].PaidToDate2");
                 LOGGER.info("Executed Step = STORE,var_PaidToDate2,var_CSVData,$.records[{var_Count}].PaidToDate2,TGTYPESCREENREG");
		String var_ModalPremiums;
                 LOGGER.info("Executed Step = VAR,String,var_ModalPremiums,TGTYPESCREENREG");
		var_ModalPremiums = getJsonData(var_CSVData , "$.records["+var_Count+"].ModalPremiums");
                 LOGGER.info("Executed Step = STORE,var_ModalPremiums,var_CSVData,$.records[{var_Count}].ModalPremiums,TGTYPESCREENREG");
        SWIPE.$("DOWN","ELE_TXTBOX_SEARCHTASKS","//input[@id='workbenchForm:workbenchTabs:taskSearchContainer:taskSearchSearch']","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_SEARCHTASKS","//input[@id='workbenchForm:workbenchTabs:taskSearchContainer:taskSearchSearch']","Policy Payment","FALSE","TGTYPESCREENREG");

        WAIT.$(3,"TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SEARCHTASKS","//a[@id='workbenchForm:workbenchTabs:taskSearchContainer:tb_buttonSearch']//span[@class='fa fa-search fa-lg']","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        TAP.$("ELE_LINK_POLICYPAYMENT","//body/div[@id='Outer40']/div[@id='ContentPanel']/div[@id='Content40']/form[@id='workbenchForm']/div[@id='PageTiles']/div[@id='workbenchForm:workbenchTabs']/div[1]/div[1]/div[1]/div[2]/div[2]/div[1]/div[1]/div[1]/div[3]/div[1]/div[3]/button[1]/span[2]","TGTYPESCREENREG");

        WAIT.$("ELE_BUTTON_CONTINUE","//span[contains(text(),'Continue')]","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_POLICYNUMBER","//input[@id='workbenchForm:workbenchTabs:policyNumber']",var_PolicyNumber,"FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_CONTINUE","//span[contains(text(),'Continue')]","TGTYPESCREENREG");

        WAIT.$("ELE_BUTTON_SUBMIT","//span[contains(text(),'Submit')]","VISIBLE","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SUBMIT","//span[contains(text(),'Submit')]","TGTYPESCREENREG");

        WAIT.$("ELE_MSG_REQUESTCOMPLETEDSUCCESSFULLY","//div[@id='workbenchForm:workbenchTabs:messages']//span[@class='ui-messages-info-summary'][contains(text(),'Request completed successfully.')]","VISIBLE","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        TAP.$("ELE_TAB_CLOSE","//li[@class='ui-state-default ui-tabs-selected ui-state-active ui-corner-top']//span[@class='ui-icon ui-icon-close']","TGTYPESCREENREG");

        WAIT.$("ELE_BUTTON_CONTINUE","//span[contains(text(),'Continue')]","VISIBLE","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_CONTINUE","//span[contains(text(),'Continue')]","TGTYPESCREENREG");

        WAIT.$("ELE_BUTTON_SUBMIT","//span[contains(text(),'Submit')]","VISIBLE","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SUBMIT","//span[contains(text(),'Submit')]","TGTYPESCREENREG");

        WAIT.$("ELE_MSG_REQUESTCOMPLETEDSUCCESSFULLY","//div[@id='workbenchForm:workbenchTabs:messages']//span[@class='ui-messages-info-summary'][contains(text(),'Request completed successfully.')]","VISIBLE","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        TAP.$("ELE_TAB_CLOSE","//li[@class='ui-state-default ui-tabs-selected ui-state-active ui-corner-top']//span[@class='ui-icon ui-icon-close']","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_CONTINUE","//span[contains(text(),'Continue')]","TGTYPESCREENREG");

        WAIT.$("ELE_BUTTON_SUBMIT","//span[contains(text(),'Submit')]","VISIBLE","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SUBMIT","//span[contains(text(),'Submit')]","TGTYPESCREENREG");

        WAIT.$("ELE_MSG_REQUESTCOMPLETEDSUCCESSFULLY","//div[@id='workbenchForm:workbenchTabs:messages']//span[@class='ui-messages-info-summary'][contains(text(),'Request completed successfully.')]","VISIBLE","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        ASSERT.$("ELE_LABEL_PAIDTODATE","//span[@id='workbenchForm:workbenchTabs:paidToDate']","CONTAINS",var_PaidToDate2,"TGTYPESCREENREG");

        SWIPE.$("UP","ELE_LABEL_MODELPREMIUM","//span[@id='workbenchForm:workbenchTabs:modalPremium']","TGTYPESCREENREG");

        ASSERT.$("ELE_LABEL_MODELPREMIUM","//span[@id='workbenchForm:workbenchTabs:modalPremium']","CONTAINS",var_ModalPremiums,"TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_GIAS_LOGO","//img[@id='headerForm:headerLogoImage']","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        CALL.$("ChangeSessionDate2","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_GIAS_LOGO","//img[@id='headerForm:headerLogoImage']","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

		String var_SessionDate2;
                 LOGGER.info("Executed Step = VAR,String,var_SessionDate2,TGTYPESCREENREG");
		var_SessionDate2 = getJsonData(var_CSVData , "$.records["+var_Count+"].SessionDate2");
                 LOGGER.info("Executed Step = STORE,var_SessionDate2,var_CSVData,$.records[{var_Count}].SessionDate2,TGTYPESCREENREG");
        TAP.$("ELE_BUTTON_SESSIONDATE","//a[@id='headerForm:sessionDatelink']","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_SESSIONDATE","//input[@id='sessionDateForm:sessionDateInput_input']",var_SessionDate2,"FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_CHANGEDATE","//span[contains(text(),'Change Date')]","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        CALL.$("PolicyInquiryForNW056","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_GIAS_LOGO","//img[@id='headerForm:headerLogoImage']","TGTYPESCREENREG");

		String var_LastDividendEarnedAmount;
                 LOGGER.info("Executed Step = VAR,String,var_LastDividendEarnedAmount,TGTYPESCREENREG");
		var_LastDividendEarnedAmount = getJsonData(var_CSVData , "$.records["+var_Count+"].LastDividendEarnedAmount");
                 LOGGER.info("Executed Step = STORE,var_LastDividendEarnedAmount,var_CSVData,$.records[{var_Count}].LastDividendEarnedAmount,TGTYPESCREENREG");
		String var_PaymentFrequencyNew;
                 LOGGER.info("Executed Step = VAR,String,var_PaymentFrequencyNew,TGTYPESCREENREG");
		var_PaymentFrequencyNew = getJsonData(var_CSVData , "$.records["+var_Count+"].PaymentFrequencyNew");
                 LOGGER.info("Executed Step = STORE,var_PaymentFrequencyNew,var_CSVData,$.records[{var_Count}].PaymentFrequencyNew,TGTYPESCREENREG");
        SWIPE.$("DOWN","ELE_TXTBOX_SEARCHTASKS","//input[@id='workbenchForm:workbenchTabs:taskSearchContainer:taskSearchSearch']","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_SEARCHTASKS","//input[@id='workbenchForm:workbenchTabs:taskSearchContainer:taskSearchSearch']","Policy Inquiry","FALSE","TGTYPESCREENREG");

        WAIT.$(3,"TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SEARCHTASKS","//a[@id='workbenchForm:workbenchTabs:taskSearchContainer:tb_buttonSearch']//span[@class='fa fa-search fa-lg']","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        TAP.$("ELE_LINK_POLICYINQUIRY","//span[contains(text(),'Policy Inquiry')]","TGTYPESCREENREG");

        WAIT.$("ELE_TXTBOX_POLICYNUMBER","//input[@id='workbenchForm:workbenchTabs:policyNumber']","VISIBLE","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_POLICYNUMBER","//input[@id='workbenchForm:workbenchTabs:policyNumber']",var_PolicyNumber,"FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_CONTINUE","//span[contains(text(),'Continue')]","TGTYPESCREENREG");

        WAIT.$("ELE_LABEL_POLICYNUMBER","//span[@id='workbenchForm:workbenchTabs:policyNumber']","VISIBLE","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        SWIPE.$("UP","ELE_LABEL_NONFORFEITUREOPTION","//span[@id='workbenchForm:workbenchTabs:nonforfeitureOptionText']","TGTYPESCREENREG");

        SWIPE.$("UP","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        ASSERT.$("ELE_LABEL_NONFORFEITUREOPTION","//span[@id='workbenchForm:workbenchTabs:nonforfeitureOptionText']","VISIBLE",var_NonforfeitureOption,"TGTYPESCREENREG");

        ASSERT.$("ELE_LABEL_LASTDIVIDENDEARNEDAMOUNT","//span[@id='workbenchForm:workbenchTabs:lastDividendAmount']","CONTAINS",var_LastDividendEarnedAmount,"TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SHOWLINKS","//button[@id='workbenchForm:workbenchTabs:tb_buttonLink']","TGTYPESCREENREG");

        TAP.$("ELE_LINK_CONTRACT","//span[contains(text(),'Contract')]","TGTYPESCREENREG");

        WAIT.$("ELE_BUTTON_EDIT","//button[@id='workbenchForm:workbenchTabs:tb_buttonUpdate']","TGTYPESCREENREG");

        SWIPE.$("DOWN","TGTYPESCREENREG");

        SWIPE.$("DOWN","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_EDIT","//button[@id='workbenchForm:workbenchTabs:tb_buttonUpdate']","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        SWIPE.$("UP","ELE_DRPDWN_PAYMENTFREQUENCY_CONTRACT","//label[@id='workbenchForm:workbenchTabs:billingMode_label']","TGTYPESCREENREG");

        TAP.$("ELE_DRPDWN_PAYMENTFREQUENCY_CONTRACT","//label[@id='workbenchForm:workbenchTabs:billingMode_label']","TGTYPESCREENREG");

		var_dataToPass = var_PaymentFrequencyNew;
                 LOGGER.info("Executed Step = STORE,var_dataToPass,var_PaymentFrequencyNew,TGTYPESCREENREG");
		try { 
		 //li[@id='workbenchForm:workbenchTabs:billingMode_2']


			Thread.sleep(1000);
					for (int i = 0; i < 2000; i++){
						WebElement selectValueOfDropDown= driver.findElement(By.xpath("//li[@id='workbenchForm:workbenchTabs:billingMode_"+ i +"']"));

						if (selectValueOfDropDown.getText().contains(var_dataToPass)){
		AddToReport("ASSERT," + selectValueOfDropDown.getText() +", CONTAINS,"+var_dataToPass);
		                                selectValueOfDropDown.click();
							
						
							break;
						}
					}
					takeScreenshot();

					Thread.sleep(2000);
		 
		 ;} 
		 catch(Exception e) { PrintVal("Renderer Script Exception : "+e.toString()); } 
		 PrintVal("Executed Step = CALLRENDERERSCRIPT,SELECT_PAYMENTFREQUENCY_CONTRACT"); 
        WAIT.$(5,"TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SUBMIT","//span[contains(text(),'Submit')]","TGTYPESCREENREG");

        WAIT.$(10,"TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_GIAS_LOGO","//img[@id='headerForm:headerLogoImage']","TGTYPESCREENREG");

        WAIT.$(10,"TGTYPESCREENREG");

        CALL.$("AdjustDividendsNW056","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_GIAS_LOGO","//img[@id='headerForm:headerLogoImage']","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

		String var_PaidUPTransactionAmount;
                 LOGGER.info("Executed Step = VAR,String,var_PaidUPTransactionAmount,TGTYPESCREENREG");
		var_PaidUPTransactionAmount = getJsonData(var_CSVData , "$.records["+var_Count+"].PaidUPTransactionAmount");
                 LOGGER.info("Executed Step = STORE,var_PaidUPTransactionAmount,var_CSVData,$.records[{var_Count}].PaidUPTransactionAmount,TGTYPESCREENREG");
		String var_PaidUPCalculate;
                 LOGGER.info("Executed Step = VAR,String,var_PaidUPCalculate,TGTYPESCREENREG");
		var_PaidUPCalculate = getJsonData(var_CSVData , "$.records["+var_Count+"].PaidUPCalculate");
                 LOGGER.info("Executed Step = STORE,var_PaidUPCalculate,var_CSVData,$.records[{var_Count}].PaidUPCalculate,TGTYPESCREENREG");
		String var_PaidUpDebitCardNumber;
                 LOGGER.info("Executed Step = VAR,String,var_PaidUpDebitCardNumber,TGTYPESCREENREG");
		var_PaidUpDebitCardNumber = getJsonData(var_CSVData , "$.records["+var_Count+"].PaidUpDebitCardNumber");
                 LOGGER.info("Executed Step = STORE,var_PaidUpDebitCardNumber,var_CSVData,$.records[{var_Count}].PaidUpDebitCardNumber,TGTYPESCREENREG");
		String var_PaidUpCreditCardNumber;
                 LOGGER.info("Executed Step = VAR,String,var_PaidUpCreditCardNumber,TGTYPESCREENREG");
		var_PaidUpCreditCardNumber = getJsonData(var_CSVData , "$.records["+var_Count+"].PaidUpCreditCardNumber");
                 LOGGER.info("Executed Step = STORE,var_PaidUpCreditCardNumber,var_CSVData,$.records[{var_Count}].PaidUpCreditCardNumber,TGTYPESCREENREG");
		String var_AccumulateionsTransactionAmount;
                 LOGGER.info("Executed Step = VAR,String,var_AccumulateionsTransactionAmount,TGTYPESCREENREG");
		var_AccumulateionsTransactionAmount = getJsonData(var_CSVData , "$.records["+var_Count+"].AccumulateionsTransactionAmount");
                 LOGGER.info("Executed Step = STORE,var_AccumulateionsTransactionAmount,var_CSVData,$.records[{var_Count}].AccumulateionsTransactionAmount,TGTYPESCREENREG");
		String var_AccumulateionsDebitCardNumber;
                 LOGGER.info("Executed Step = VAR,String,var_AccumulateionsDebitCardNumber,TGTYPESCREENREG");
		var_AccumulateionsDebitCardNumber = getJsonData(var_CSVData , "$.records["+var_Count+"].AccumulateionsDebitCardNumber");
                 LOGGER.info("Executed Step = STORE,var_AccumulateionsDebitCardNumber,var_CSVData,$.records[{var_Count}].AccumulateionsDebitCardNumber,TGTYPESCREENREG");
		String var_AccumulateionsCreditCardNumber;
                 LOGGER.info("Executed Step = VAR,String,var_AccumulateionsCreditCardNumber,TGTYPESCREENREG");
		var_AccumulateionsCreditCardNumber = getJsonData(var_CSVData , "$.records["+var_Count+"].AccumulateionsCreditCardNumber");
                 LOGGER.info("Executed Step = STORE,var_AccumulateionsCreditCardNumber,var_CSVData,$.records[{var_Count}].AccumulateionsCreditCardNumber,TGTYPESCREENREG");
        SWIPE.$("DOWN","ELE_TXTBOX_SEARCHTASKS","//input[@id='workbenchForm:workbenchTabs:taskSearchContainer:taskSearchSearch']","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_SEARCHTASKS","//input[@id='workbenchForm:workbenchTabs:taskSearchContainer:taskSearchSearch']","Adjust Dividends","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SEARCHTASKS","//a[@id='workbenchForm:workbenchTabs:taskSearchContainer:tb_buttonSearch']//span[@class='fa fa-search fa-lg']","TGTYPESCREENREG");

        WAIT.$("ELE_LINK_ADJUSTDIVIDENDSANDCOUPONS","//span[contains(text(),'Adjust Dividends and Coupons')]","VISIBLE","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        TAP.$("ELE_LINK_ADJUSTDIVIDENDSANDCOUPONS","//span[contains(text(),'Adjust Dividends and Coupons')]","TGTYPESCREENREG");

        WAIT.$("ELE_TXTBOX_POLICYNUMBER","//input[@id='workbenchForm:workbenchTabs:policyNumber']","VISIBLE","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_POLICYNUMBER","//input[@id='workbenchForm:workbenchTabs:policyNumber']",var_PolicyNumber,"FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_CONTINUE","//span[contains(text(),'Continue')]","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        WAIT.$("ELE_TXTBOX_TRANSACTIONAMOUNT_ONEYEARTERMADDITIONS","//input[@id='workbenchForm:workbenchTabs:year1TermAddiTransactionAmount_input']","VISIBLE","TGTYPESCREENREG");

        SWIPE.$("UP","ELE_TXTBOX_PAIDUPADDITIONAL_TRANSACTIONAMOUNT","//input[@id='workbenchForm:workbenchTabs:paidUpAdditionsTransactionAmount_input']","TGTYPESCREENREG");

        SWIPE.$("UP","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_PAIDUPADDITIONAL_TRANSACTIONAMOUNT","//input[@id='workbenchForm:workbenchTabs:paidUpAdditionsTransactionAmount_input']",var_PaidUPTransactionAmount,"FALSE","TGTYPESCREENREG");

		var_dataToPass = var_PaidUPCalculate;
                 LOGGER.info("Executed Step = STORE,var_dataToPass,var_PaidUPCalculate,TGTYPESCREENREG");
        TAP.$("ELE_DRPDWN_PAIDUPADDITIONALCALCULATE","//label[@id='workbenchForm:workbenchTabs:paidUpAdditionsCalculate_label']","TGTYPESCREENREG");

		try { 
		 //li[@id='workbenchForm:workbenchTabs:paidUpAdditionsCalculate_1']

			Thread.sleep(1000);
					for (int i = 0; i < 2000; i++){
						WebElement selectValueOfDropDown= driver.findElement(By.xpath("//li[@id='workbenchForm:workbenchTabs:paidUpAdditionsCalculate_"+ i +"']"));

						if (selectValueOfDropDown.getText().contains(var_dataToPass)){

		                                selectValueOfDropDown.click();
							AddToReport("ASSERT," + selectValueOfDropDown.getText() +", CONTAINS,"+var_dataToPass);
						
							break;
						}
					}
					takeScreenshot();

					Thread.sleep(2000);
		 
		 ;} 
		 catch(Exception e) { PrintVal("Renderer Script Exception : "+e.toString()); } 
		 PrintVal("Executed Step = CALLRENDERERSCRIPT,SELECT_CALCULATE_PAIDUP"); 
        WAIT.$(5,"TGTYPESCREENREG");

		var_dataToPass = var_PaidUpDebitCardNumber;
                 LOGGER.info("Executed Step = STORE,var_dataToPass,var_PaidUpDebitCardNumber,TGTYPESCREENREG");
        TAP.$("ELE_DRPDWN_PAIDUPADDITONAL_DEBITCARD","//label[@id='workbenchForm:workbenchTabs:paidUpAdditionsLedgerDebit_label']","TGTYPESCREENREG");

		try { 
		 //li[@id='workbenchForm:workbenchTabs:paidUpAdditionsLedgerDebit_1']


			Thread.sleep(1000);
					for (int i = 0; i < 2000; i++){
						WebElement selectValueOfDropDown= driver.findElement(By.xpath("//li[@id='workbenchForm:workbenchTabs:paidUpAdditionsLedgerDebit_"+ i +"']"));

						if (selectValueOfDropDown.getText().contains(var_dataToPass)){
		AddToReport("ASSERT," + selectValueOfDropDown.getText() +", CONTAINS,"+var_dataToPass);
		                                selectValueOfDropDown.click();
							
						
							break;
						}
					}
					takeScreenshot();

					Thread.sleep(2000);
		 
		 ;} 
		 catch(Exception e) { PrintVal("Renderer Script Exception : "+e.toString()); } 
		 PrintVal("Executed Step = CALLRENDERERSCRIPT,SELECT_DEBITCARDNUMBER_PAIDUP"); 
        WAIT.$(5,"TGTYPESCREENREG");

		var_dataToPass = var_PaidUpCreditCardNumber;
                 LOGGER.info("Executed Step = STORE,var_dataToPass,var_PaidUpCreditCardNumber,TGTYPESCREENREG");
        TAP.$("ELE_DRPDWN_PAIDUPADDITIONAL_CREDITCARD","//label[@id='workbenchForm:workbenchTabs:paidUpAdditionsLedgerCredit_label']","TGTYPESCREENREG");

		try { 
		 //li[@id='workbenchForm:workbenchTabs:paidUpAdditionsLedgerCredit_1']


			Thread.sleep(1000);
					for (int i = 0; i < 2000; i++){
						WebElement selectValueOfDropDown= driver.findElement(By.xpath("//li[@id='workbenchForm:workbenchTabs:paidUpAdditionsLedgerCredit_"+ i +"']"));

						if (selectValueOfDropDown.getText().contains(var_dataToPass)){
		AddToReport("ASSERT," + selectValueOfDropDown.getText() +", CONTAINS,"+var_dataToPass);
		                                selectValueOfDropDown.click();
							
						
							break;
						}
					}
					takeScreenshot();

					Thread.sleep(2000);
		 
		 ;} 
		 catch(Exception e) { PrintVal("Renderer Script Exception : "+e.toString()); } 
		 PrintVal("Executed Step = CALLRENDERERSCRIPT,SELECT_CREDITCARDNUMBER_PAIDUP"); 
        WAIT.$(5,"TGTYPESCREENREG");

        SWIPE.$("DOWN","ELE_TXTBOX_TRANSACTIONAMOUNT_ACCUMULATIONS","//input[@id='workbenchForm:workbenchTabs:accumulationTransactionAmount_input']","TGTYPESCREENREG");

        SWIPE.$("DOWN","TGTYPESCREENREG");

        SCROLL.$("ELE_TXTBOX_TRANSACTIONAMOUNT_ACCUMULATIONS","//input[@id='workbenchForm:workbenchTabs:accumulationTransactionAmount_input']","RIGHT","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_TRANSACTIONAMOUNT_ACCUMULATIONS","//input[@id='workbenchForm:workbenchTabs:accumulationTransactionAmount_input']",var_AccumulateionsTransactionAmount,"FALSE","TGTYPESCREENREG");

		var_dataToPass = var_AccumulateionsDebitCardNumber;
                 LOGGER.info("Executed Step = STORE,var_dataToPass,var_AccumulateionsDebitCardNumber,TGTYPESCREENREG");
        TAP.$("ELE_DRPDWN_DEBITACCOUNTNUMBER_ACCUMULATION","//label[@id='workbenchForm:workbenchTabs:accumulationLedgerDebit_label']","TGTYPESCREENREG");

		try { 
		 //li[@id='workbenchForm:workbenchTabs:accumulationLedgerDebit_1']


			Thread.sleep(1000);
					for (int i = 0; i < 2000; i++){
						WebElement selectValueOfDropDown= driver.findElement(By.xpath("//li[@id='workbenchForm:workbenchTabs:accumulationLedgerDebit_"+ i +"']"));

						if (selectValueOfDropDown.getText().contains(var_dataToPass)){
		AddToReport("ASSERT," + selectValueOfDropDown.getText() +", CONTAINS,"+var_dataToPass);
		                                selectValueOfDropDown.click();
							
						
							break;
						}
					}
					takeScreenshot();

					Thread.sleep(2000);
		 
		 ;} 
		 catch(Exception e) { PrintVal("Renderer Script Exception : "+e.toString()); } 
		 PrintVal("Executed Step = CALLRENDERERSCRIPT,SELECT_DEBITCARDNUMBER_ACCUMULATION"); 
        WAIT.$(5,"TGTYPESCREENREG");

		var_dataToPass = var_AccumulateionsCreditCardNumber;
                 LOGGER.info("Executed Step = STORE,var_dataToPass,var_AccumulateionsCreditCardNumber,TGTYPESCREENREG");
        TAP.$("ELE_DRPDWN_CREDITACCOUNTNUMBER_ACCUMULATION","//label[@id='workbenchForm:workbenchTabs:accumulationLedgerCredit_label']","TGTYPESCREENREG");

		try { 
		 //li[@id='workbenchForm:workbenchTabs:accumulationLedgerCredit_1']

			Thread.sleep(1000);
					for (int i = 0; i < 2000; i++){
						WebElement selectValueOfDropDown= driver.findElement(By.xpath("//li[@id='workbenchForm:workbenchTabs:accumulationLedgerCredit_"+ i +"']"));

						if (selectValueOfDropDown.getText().contains(var_dataToPass)){
		AddToReport("ASSERT," + selectValueOfDropDown.getText() +", CONTAINS,"+var_dataToPass);
		                                selectValueOfDropDown.click();
							
						
							break;
						}
					}
					takeScreenshot();

					Thread.sleep(2000);
		 
		 ;} 
		 catch(Exception e) { PrintVal("Renderer Script Exception : "+e.toString()); } 
		 PrintVal("Executed Step = CALLRENDERERSCRIPT,SELECT_CREDITCARDNUMBER_ACCUMULATION"); 
        WAIT.$(5,"TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SAVE","//span[contains(text(),'Save')]","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_GIAS_LOGO","//img[@id='headerForm:headerLogoImage']","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        CALL.$("PolicyInquirySecoundForNW056","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_GIAS_LOGO","//img[@id='headerForm:headerLogoImage']","TGTYPESCREENREG");

		String var_SuspenseBalance;
                 LOGGER.info("Executed Step = VAR,String,var_SuspenseBalance,TGTYPESCREENREG");
		var_SuspenseBalance = getJsonData(var_CSVData , "$.records["+var_Count+"].SuspenseBalance");
                 LOGGER.info("Executed Step = STORE,var_SuspenseBalance,var_CSVData,$.records[{var_Count}].SuspenseBalance,TGTYPESCREENREG");
		String var_AccumulationsonDeposit;
                 LOGGER.info("Executed Step = VAR,String,var_AccumulationsonDeposit,TGTYPESCREENREG");
		var_AccumulationsonDeposit = getJsonData(var_CSVData , "$.records["+var_Count+"].AccumulationsonDeposit");
                 LOGGER.info("Executed Step = STORE,var_AccumulationsonDeposit,var_CSVData,$.records[{var_Count}].AccumulationsonDeposit,TGTYPESCREENREG");
		String var_PaidUpAdditions;
                 LOGGER.info("Executed Step = VAR,String,var_PaidUpAdditions,TGTYPESCREENREG");
		var_PaidUpAdditions = getJsonData(var_CSVData , "$.records["+var_Count+"].PaidUpAdditions");
                 LOGGER.info("Executed Step = STORE,var_PaidUpAdditions,var_CSVData,$.records[{var_Count}].PaidUpAdditions,TGTYPESCREENREG");
        SWIPE.$("DOWN","ELE_TXTBOX_SEARCHTASKS","//input[@id='workbenchForm:workbenchTabs:taskSearchContainer:taskSearchSearch']","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_SEARCHTASKS","//input[@id='workbenchForm:workbenchTabs:taskSearchContainer:taskSearchSearch']","Policy Inquiry","FALSE","TGTYPESCREENREG");

        WAIT.$(3,"TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SEARCHTASKS","//a[@id='workbenchForm:workbenchTabs:taskSearchContainer:tb_buttonSearch']//span[@class='fa fa-search fa-lg']","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        TAP.$("ELE_LINK_POLICYINQUIRY","//span[contains(text(),'Policy Inquiry')]","TGTYPESCREENREG");

        WAIT.$("ELE_TXTBOX_POLICYNUMBER","//input[@id='workbenchForm:workbenchTabs:policyNumber']","VISIBLE","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_POLICYNUMBER","//input[@id='workbenchForm:workbenchTabs:policyNumber']",var_PolicyNumber,"FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_CONTINUE","//span[contains(text(),'Continue')]","TGTYPESCREENREG");

        WAIT.$("ELE_LABEL_POLICYNUMBER","//span[@id='workbenchForm:workbenchTabs:policyNumber']","VISIBLE","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        SWIPE.$("UP","ELE_LABEL_SUSPENSE_BALANCE","//span[@id='workbenchForm:workbenchTabs:suspenseBalance']","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        ASSERT.$("ELE_LABEL_SUSPENSE_BALANCE","//span[@id='workbenchForm:workbenchTabs:suspenseBalance']","CONTAINS",var_SuspenseBalance,"TGTYPESCREENREG");

        ASSERT.$("ELE_LABEL_ACCUMULATIONSONDEPOSIT","//span[@id='workbenchForm:workbenchTabs:divCpnOnDepositAmt']","CONTAINS",var_AccumulationsonDeposit,"TGTYPESCREENREG");

        ASSERT.$("ELE_LABEL_PAIDUPADDITIONS","//span[@id='workbenchForm:workbenchTabs:paidUpAdditionsAmt']","CONTAINS",var_PaidUpAdditions,"TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_GIAS_LOGO","//img[@id='headerForm:headerLogoImage']","TGTYPESCREENREG");

        WAIT.$(10,"TGTYPESCREENREG");

        CALL.$("ChangeSessionDate3","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_GIAS_LOGO","//img[@id='headerForm:headerLogoImage']","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

		String var_SessionDate3;
                 LOGGER.info("Executed Step = VAR,String,var_SessionDate3,TGTYPESCREENREG");
		var_SessionDate3 = getJsonData(var_CSVData , "$.records["+var_Count+"].SessionDate3");
                 LOGGER.info("Executed Step = STORE,var_SessionDate3,var_CSVData,$.records[{var_Count}].SessionDate3,TGTYPESCREENREG");
        TAP.$("ELE_BUTTON_SESSIONDATE","//a[@id='headerForm:sessionDatelink']","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_SESSIONDATE","//input[@id='sessionDateForm:sessionDateInput_input']",var_SessionDate3,"FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_CHANGEDATE","//span[contains(text(),'Change Date')]","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        CALL.$("DailyOfflineProcessingNW056","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_GIAS_LOGO","//img[@id='headerForm:headerLogoImage']","TGTYPESCREENREG");

		String var_StartingTask;
                 LOGGER.info("Executed Step = VAR,String,var_StartingTask,TGTYPESCREENREG");
		var_StartingTask = getJsonData(var_CSVData , "$.records["+var_Count+"].StartingTask");
                 LOGGER.info("Executed Step = STORE,var_StartingTask,var_CSVData,$.records[{var_Count}].StartingTask,TGTYPESCREENREG");
        SWIPE.$("DOWN","ELE_TXTBOX_SEARCHTASKS","//input[@id='workbenchForm:workbenchTabs:taskSearchContainer:taskSearchSearch']","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_SEARCHTASKS","//input[@id='workbenchForm:workbenchTabs:taskSearchContainer:taskSearchSearch']","Daily offline","FALSE","TGTYPESCREENREG");

        WAIT.$(3,"TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SEARCHTASKS","//a[@id='workbenchForm:workbenchTabs:taskSearchContainer:tb_buttonSearch']//span[@class='fa fa-search fa-lg']","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        TAP.$("ELE_LINK_DAILY_OFFLINE_PROCESSING","//span[contains(text(),'Daily Offline Processing')]","TGTYPESCREENREG");

        WAIT.$("ELE_BUTTON_SUBMIT","//span[contains(text(),'Submit')]","VISIBLE","TGTYPESCREENREG");

        TAP.$("ELE_DRPDWN_STARTINGTASK","//label[@id='workbenchForm:workbenchTabs:startingTask_label']","TGTYPESCREENREG");

		var_dataToPass = var_StartingTask;
                 LOGGER.info("Executed Step = STORE,var_dataToPass,var_StartingTask,TGTYPESCREENREG");
		try { 
		 //li[@id='workbenchForm:workbenchTabs:startingTask_49']


			Thread.sleep(1000);
					for (int i = 0; i < 2000; i++){
						WebElement selectValueOfDropDown= driver.findElement(By.xpath("//li[@id='workbenchForm:workbenchTabs:startingTask_"+ i +"']"));

						if (selectValueOfDropDown.getText().contains(var_dataToPass)){
		AddToReport("ASSERT," + selectValueOfDropDown.getText() +", CONTAINS,"+var_dataToPass);
		                                selectValueOfDropDown.click();
							
						
							break;
						}
					}
					takeScreenshot();

					Thread.sleep(2000);
		 
		 ;} 
		 catch(Exception e) { PrintVal("Renderer Script Exception : "+e.toString()); } 
		 PrintVal("Executed Step = CALLRENDERERSCRIPT,SELECT_STARTINGTASK"); 
        WAIT.$(5,"TGTYPESCREENREG");

        TAP.$("ELE_RADIO_BUTTON_NO_HOLDJOB","//body[1]/div[1]/div[2]/div[1]/form[1]/div[1]/div[2]/div[1]/div[2]/div[1]/table[1]/tbody[1]/tr[1]/td[2]/table[1]/tbody[1]/tr[1]/td[1]/fieldset[1]/div[1]/table[1]/tbody[1]/tr[5]/td[2]/table[1]/tbody[1]/tr[2]/td[1]/label[1]","TGTYPESCREENREG");

        WAIT.$(3,"TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SUBMIT","//span[contains(text(),'Submit')]","TGTYPESCREENREG");

        WAIT.$(10,"TGTYPESCREENREG");

        SWIPE.$("DOWN","TGTYPESCREENREG");

        ASSERT.$("ELE_MSG_SUCCESSFULLY","//span[contains(@class,'ui-messages-info-summary')]","VISIBLE","TGTYPESCREENREG");

        WAIT.$(10,"TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SHOWLINKS","//button[@id='workbenchForm:workbenchTabs:tb_buttonLink']","TGTYPESCREENREG");

        TAP.$("ELE_LINK_SUBMITTED_JOB_LOGS","//span[contains(text(),'Submitted Job Logs')]","TGTYPESCREENREG");

        WAIT.$("ELE_BUTTON_REFRESH","//span[contains(text(),'Refresh')]","VISIBLE","TGTYPESCREENREG");

        WAIT.$(30,"TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_REFRESH","//span[contains(text(),'Refresh')]","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        LOGGER.info("Executed Step = START IF");
        if (check("ELE_LINK_FIRST_JOB_STATUS","//span[@id='workbenchForm:workbenchTabs:grid:0:status']","CONTAINS","Active","TGTYPESCREENREG")) {
         LOGGER.info("Executed Step = IF,ELE_LINK_FIRST_JOB_STATUS,CONTAINS,Active,TGTYPESCREENREG");
        WAIT.$(45,"TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_REFRESH","//span[contains(text(),'Refresh')]","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        }
          LOGGER.info("Executed Step = ENDIF,TGTYPESCREENREG");
        WAIT.$(5,"TGTYPESCREENREG");

        WAIT.$(10,"TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_GIAS_LOGO","//img[@id='headerForm:headerLogoImage']","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        CALL.$("PolicyInquiryThirdForReg056","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_GIAS_LOGO","//img[@id='headerForm:headerLogoImage']","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

		String var_TransactionDescription1;
                 LOGGER.info("Executed Step = VAR,String,var_TransactionDescription1,TGTYPESCREENREG");
		var_TransactionDescription1 = getJsonData(var_CSVData , "$.records["+var_Count+"].TransactionDescription1");
                 LOGGER.info("Executed Step = STORE,var_TransactionDescription1,var_CSVData,$.records[{var_Count}].TransactionDescription1,TGTYPESCREENREG");
		String var_TransactionAmount1;
                 LOGGER.info("Executed Step = VAR,String,var_TransactionAmount1,TGTYPESCREENREG");
		var_TransactionAmount1 = getJsonData(var_CSVData , "$.records["+var_Count+"].TransactionAmount1");
                 LOGGER.info("Executed Step = STORE,var_TransactionAmount1,var_CSVData,$.records[{var_Count}].TransactionAmount1,TGTYPESCREENREG");
		String var_TransactionDescription2;
                 LOGGER.info("Executed Step = VAR,String,var_TransactionDescription2,TGTYPESCREENREG");
		var_TransactionDescription2 = getJsonData(var_CSVData , "$.records["+var_Count+"].TransactionDescription2");
                 LOGGER.info("Executed Step = STORE,var_TransactionDescription2,var_CSVData,$.records[{var_Count}].TransactionDescription2,TGTYPESCREENREG");
		String var_TransactionAmount2;
                 LOGGER.info("Executed Step = VAR,String,var_TransactionAmount2,TGTYPESCREENREG");
		var_TransactionAmount2 = getJsonData(var_CSVData , "$.records["+var_Count+"].TransactionAmount2");
                 LOGGER.info("Executed Step = STORE,var_TransactionAmount2,var_CSVData,$.records[{var_Count}].TransactionAmount2,TGTYPESCREENREG");
		String var_TransactionDescription3;
                 LOGGER.info("Executed Step = VAR,String,var_TransactionDescription3,TGTYPESCREENREG");
		var_TransactionDescription3 = getJsonData(var_CSVData , "$.records["+var_Count+"].TransactionDescription3");
                 LOGGER.info("Executed Step = STORE,var_TransactionDescription3,var_CSVData,$.records[{var_Count}].TransactionDescription3,TGTYPESCREENREG");
		String var_TransactionAmount3;
                 LOGGER.info("Executed Step = VAR,String,var_TransactionAmount3,TGTYPESCREENREG");
		var_TransactionAmount3 = getJsonData(var_CSVData , "$.records["+var_Count+"].TransactionAmount3");
                 LOGGER.info("Executed Step = STORE,var_TransactionAmount3,var_CSVData,$.records[{var_Count}].TransactionAmount3,TGTYPESCREENREG");
		String var_TransactionDescription4;
                 LOGGER.info("Executed Step = VAR,String,var_TransactionDescription4,TGTYPESCREENREG");
		var_TransactionDescription4 = getJsonData(var_CSVData , "$.records["+var_Count+"].TransactionDescription4");
                 LOGGER.info("Executed Step = STORE,var_TransactionDescription4,var_CSVData,$.records[{var_Count}].TransactionDescription4,TGTYPESCREENREG");
		String var_TransactionAmount4;
                 LOGGER.info("Executed Step = VAR,String,var_TransactionAmount4,TGTYPESCREENREG");
		var_TransactionAmount4 = getJsonData(var_CSVData , "$.records["+var_Count+"].TransactionAmount4");
                 LOGGER.info("Executed Step = STORE,var_TransactionAmount4,var_CSVData,$.records[{var_Count}].TransactionAmount4,TGTYPESCREENREG");
        SWIPE.$("DOWN","ELE_TXTBOX_SEARCHTASKS","//input[@id='workbenchForm:workbenchTabs:taskSearchContainer:taskSearchSearch']","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_SEARCHTASKS","//input[@id='workbenchForm:workbenchTabs:taskSearchContainer:taskSearchSearch']","Policy Inquiry","FALSE","TGTYPESCREENREG");

        WAIT.$(3,"TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SEARCHTASKS","//a[@id='workbenchForm:workbenchTabs:taskSearchContainer:tb_buttonSearch']//span[@class='fa fa-search fa-lg']","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        TAP.$("ELE_LINK_POLICYINQUIRY","//span[contains(text(),'Policy Inquiry')]","TGTYPESCREENREG");

        WAIT.$("ELE_TXTBOX_POLICYNUMBER","//input[@id='workbenchForm:workbenchTabs:policyNumber']","VISIBLE","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_POLICYNUMBER","//input[@id='workbenchForm:workbenchTabs:policyNumber']",var_PolicyNumber,"FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_CONTINUE","//span[contains(text(),'Continue')]","TGTYPESCREENREG");

        WAIT.$("ELE_LABEL_POLICYNUMBER","//span[@id='workbenchForm:workbenchTabs:policyNumber']","VISIBLE","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SHOWLINKS","//button[@id='workbenchForm:workbenchTabs:tb_buttonLink']","TGTYPESCREENREG");

        SWIPE.$("UP","ELE_LINK_TRANSACTIONHISTORY","//span[contains(text(),'Transaction History')]","TGTYPESCREENREG");

        SWIPE.$("UP","TGTYPESCREENREG");

        TAP.$("ELE_LINK_TRANSACTIONHISTORY","//span[contains(text(),'Transaction History')]","TGTYPESCREENREG");

        WAIT.$("ELE_LABEL_POLICYNUMBER","//span[@id='workbenchForm:workbenchTabs:policyNumber']","VISIBLE","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

		var_dataToPass = var_TransactionDescription1;
                 LOGGER.info("Executed Step = STORE,var_dataToPass,var_TransactionDescription1,TGTYPESCREENREG");
		String var_Amount;
                 LOGGER.info("Executed Step = VAR,String,var_Amount,TGTYPESCREENREG");
		var_Amount = var_TransactionAmount1;
                 LOGGER.info("Executed Step = STORE,var_Amount,var_TransactionAmount1,TGTYPESCREENREG");
		try { 
		 //span[@id='workbenchForm:workbenchTabs:grid:0:typeText']
		//span[@id='workbenchForm:workbenchTabs:grid:0:transactionAmountText']

			Thread.sleep(1000);
					for (int i = 0; i < 10; i++){
						WebElement selectValueOfDropDown= driver.findElement(By.xpath("//span[@id='workbenchForm:workbenchTabs:grid:"+ i +":typeText']"));
		WebElement selectValueOfAmount= driver.findElement(By.xpath("//span[@id='workbenchForm:workbenchTabs:grid:"+ i +":transactionAmountText']"));

						if (selectValueOfDropDown.getText().contains(var_dataToPass) && selectValueOfAmount.getText().contains(var_Amount)){

		     
							AddToReport("ASSERT," + selectValueOfDropDown.getText() +", CONTAINS,"+var_dataToPass +" Amount = " + var_Amount);
						
							break;
						}
					}
					takeScreenshot();

					Thread.sleep(2000);
		 
		 ;} 
		 catch(Exception e) { PrintVal("Renderer Script Exception : "+e.toString()); } 
		 PrintVal("Executed Step = CALLRENDERERSCRIPT,VERIFYTRANSACTIONHISTORYFORNW056"); 
		var_dataToPass = var_TransactionDescription2;
                 LOGGER.info("Executed Step = STORE,var_dataToPass,var_TransactionDescription2,TGTYPESCREENREG");
		var_Amount = var_TransactionAmount2;
                 LOGGER.info("Executed Step = STORE,var_Amount,var_TransactionAmount2,TGTYPESCREENREG");
		try { 
		 //span[@id='workbenchForm:workbenchTabs:grid:0:typeText']
		//span[@id='workbenchForm:workbenchTabs:grid:0:transactionAmountText']

			Thread.sleep(1000);
					for (int i = 0; i < 10; i++){
						WebElement selectValueOfDropDown= driver.findElement(By.xpath("//span[@id='workbenchForm:workbenchTabs:grid:"+ i +":typeText']"));
		WebElement selectValueOfAmount= driver.findElement(By.xpath("//span[@id='workbenchForm:workbenchTabs:grid:"+ i +":transactionAmountText']"));

						if (selectValueOfDropDown.getText().contains(var_dataToPass) && selectValueOfAmount.getText().contains(var_Amount)){

		     
							AddToReport("ASSERT," + selectValueOfDropDown.getText() +", CONTAINS,"+var_dataToPass +" Amount = " + var_Amount);
						
							break;
						}
					}
					takeScreenshot();

					Thread.sleep(2000);
		 
		 ;} 
		 catch(Exception e) { PrintVal("Renderer Script Exception : "+e.toString()); } 
		 PrintVal("Executed Step = CALLRENDERERSCRIPT,VERIFYTRANSACTIONHISTORYFORNW056"); 
		var_dataToPass = var_TransactionDescription3;
                 LOGGER.info("Executed Step = STORE,var_dataToPass,var_TransactionDescription3,TGTYPESCREENREG");
		var_Amount = var_TransactionAmount3;
                 LOGGER.info("Executed Step = STORE,var_Amount,var_TransactionAmount3,TGTYPESCREENREG");
		try { 
		 //span[@id='workbenchForm:workbenchTabs:grid:0:typeText']
		//span[@id='workbenchForm:workbenchTabs:grid:0:transactionAmountText']

			Thread.sleep(1000);
					for (int i = 0; i < 10; i++){
						WebElement selectValueOfDropDown= driver.findElement(By.xpath("//span[@id='workbenchForm:workbenchTabs:grid:"+ i +":typeText']"));
		WebElement selectValueOfAmount= driver.findElement(By.xpath("//span[@id='workbenchForm:workbenchTabs:grid:"+ i +":transactionAmountText']"));

						if (selectValueOfDropDown.getText().contains(var_dataToPass) && selectValueOfAmount.getText().contains(var_Amount)){

		     
							AddToReport("ASSERT," + selectValueOfDropDown.getText() +", CONTAINS,"+var_dataToPass +" Amount = " + var_Amount);
						
							break;
						}
					}
					takeScreenshot();

					Thread.sleep(2000);
		 
		 ;} 
		 catch(Exception e) { PrintVal("Renderer Script Exception : "+e.toString()); } 
		 PrintVal("Executed Step = CALLRENDERERSCRIPT,VERIFYTRANSACTIONHISTORYFORNW056"); 
		var_dataToPass = var_TransactionDescription4;
                 LOGGER.info("Executed Step = STORE,var_dataToPass,var_TransactionDescription4,TGTYPESCREENREG");
		var_Amount = var_TransactionAmount4;
                 LOGGER.info("Executed Step = STORE,var_Amount,var_TransactionAmount4,TGTYPESCREENREG");
		try { 
		 //span[@id='workbenchForm:workbenchTabs:grid:0:typeText']
		//span[@id='workbenchForm:workbenchTabs:grid:0:transactionAmountText']

			Thread.sleep(1000);
					for (int i = 0; i < 10; i++){
						WebElement selectValueOfDropDown= driver.findElement(By.xpath("//span[@id='workbenchForm:workbenchTabs:grid:"+ i +":typeText']"));
		WebElement selectValueOfAmount= driver.findElement(By.xpath("//span[@id='workbenchForm:workbenchTabs:grid:"+ i +":transactionAmountText']"));

						if (selectValueOfDropDown.getText().contains(var_dataToPass) && selectValueOfAmount.getText().contains(var_Amount)){

		     
							AddToReport("ASSERT," + selectValueOfDropDown.getText() +", CONTAINS,"+var_dataToPass +" Amount = " + var_Amount);
						
							break;
						}
					}
					takeScreenshot();

					Thread.sleep(2000);
		 
		 ;} 
		 catch(Exception e) { PrintVal("Renderer Script Exception : "+e.toString()); } 
		 PrintVal("Executed Step = CALLRENDERERSCRIPT,VERIFYTRANSACTIONHISTORYFORNW056"); 
        WAIT.$(10,"TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_GIAS_LOGO","//img[@id='headerForm:headerLogoImage']","TGTYPESCREENREG");

        WAIT.$(10,"TGTYPESCREENREG");

        CALL.$("CorrespondenceGeneratedForNW056","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_GIAS_LOGO","//img[@id='headerForm:headerLogoImage']","TGTYPESCREENREG");

		String var_CorrespondenceType;
                 LOGGER.info("Executed Step = VAR,String,var_CorrespondenceType,TGTYPESCREENREG");
		var_CorrespondenceType = getJsonData(var_CSVData , "$.records["+var_Count+"].CorrespondenceType");
                 LOGGER.info("Executed Step = STORE,var_CorrespondenceType,var_CSVData,$.records[{var_Count}].CorrespondenceType,TGTYPESCREENREG");
		String var_CorrespondenceStatus;
                 LOGGER.info("Executed Step = VAR,String,var_CorrespondenceStatus,TGTYPESCREENREG");
		var_CorrespondenceStatus = getJsonData(var_CSVData , "$.records["+var_Count+"].CorrespondenceStatus");
                 LOGGER.info("Executed Step = STORE,var_CorrespondenceStatus,var_CSVData,$.records[{var_Count}].CorrespondenceStatus,TGTYPESCREENREG");
        SWIPE.$("DOWN","ELE_TXTBOX_SEARCHTASKS","//input[@id='workbenchForm:workbenchTabs:taskSearchContainer:taskSearchSearch']","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_SEARCHTASKS","//input[@id='workbenchForm:workbenchTabs:taskSearchContainer:taskSearchSearch']","Correspondence G","FALSE","TGTYPESCREENREG");

        WAIT.$(3,"TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SEARCHTASKS","//a[@id='workbenchForm:workbenchTabs:taskSearchContainer:tb_buttonSearch']//span[@class='fa fa-search fa-lg']","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        TAP.$("ELE_LINK_CORRESPONDENCE_GENERATED","//span[contains(text(),'Correspondence Generated')]","TGTYPESCREENREG");

        WAIT.$(10,"TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_LETTER_DATE","//input[@id='workbenchForm:workbenchTabs:grid:clientCorrLetterDate_filter_input']",var_SessionDate3,"FALSE","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_POLICYNUMBER_FILTER","//input[@id='workbenchForm:workbenchTabs:grid:policyNumber_Col:filter']",var_PolicyNumber,"FALSE","TGTYPESCREENREG");

        WAIT.$(10,"TGTYPESCREENREG");

        ASSERT.$("ELE_LINK_FIRST_CLIENT_CORRESPONSE_TYPE","//span[@id='workbenchForm:workbenchTabs:grid:0:clientCorrType']","CONTAINS",var_CorrespondenceType,"TGTYPESCREENREG");

        ASSERT.$("ELE_LINK_FIRST_POLICY_FROM_TABLE","//span[@id='workbenchForm:workbenchTabs:grid:0:policyNumber']","CONTAINS",var_PolicyNumber,"TGTYPESCREENREG");

        SCROLL.$("ELE_LINK_FIRST_PRINT_STATUS","//span[@id='workbenchForm:workbenchTabs:grid:0:clientExtrPrinFlag']","RIGHT","TGTYPESCREENREG");

        ASSERT.$("ELE_LINK_FIRST_PRINT_STATUS","//span[@id='workbenchForm:workbenchTabs:grid:0:clientExtrPrinFlag']","CONTAINS",var_CorrespondenceStatus,"TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_GIAS_LOGO","//img[@id='headerForm:headerLogoImage']","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

		try { 
		 	java.sql.Connection con = null;
					// Load SQL Server JDBC driver and establish connection.
					String connectionUrl = "jdbc:sqlserver://CIS-giasdbt4:2433;" +
							"databaseName=NWT1DTA;"
							+
							"IntegratedSecurity = true";
					System.out.print("Connecting to SQL Server ... ");
					try {
						con = DriverManager.getConnection(connectionUrl);
					} catch (SQLException e) {
						System.out.println("SQLException to database.");
						e.getErrorCode();
						System.out.println("SQLException getErrorCode." + e.getErrorCode());
						e.printStackTrace();
					}
					System.out.println("Connected to database.");

					String sql = "SELECT TOP 1000 RECID , VERSIONID , companyCode , clientCorrType , clientCorrLetterDateYear , clientCorrLetterDateMonth , clientCorrLetterDateDay , userID , duplicateNotice , clientExtrPrinFlag , policyNumber , baseRiderCode , currencyCode , productCode , policyPrintDesc , riderPlanCode , riderPlanDescription , insuredNameCoprInd , insuredSexCode , insuredNamePrefix , insuredFirstName , insuredMiddleInitial , insuredLastName , insuredNameSuffix , insuredAddressLine1 , insuredAddressLine2 , insuredAddressLine3 , insuredCity , insuredState , insuredZipCode , insuredCountry , insuredSpecialHandle , insuredEmail , ownerNameCoprInd , ownerSexCode , ownerNamePrefix , ownerFirstName , ownerMiddleInitial , ownerLastName , ownerNameSuffix , ownerAddressLine1 , ownerAddressLine2 , ownerAddressLine3 , ownerCity , ownerState , ownerZipCode , ownerCountry , ownerSpecialHandle , ownerEmail , addtlNameCoprInd , addtlSexCode , addtlNamePrefix , addtlFirstName , addtlMiddleInitial , addtlLastName , addtlNameSuffix , addtlAddressLine1 , addtlAddressLine2 , addtlAddressLine3 , addtlCity , addtlState , addtlZipCode , addtlCountry , addtlSpecialHandle , addtlEmail , day1Year , day1Month , day1Day , day2Year , day2Month , day2Day , day3Year , day3Month , day3Day , day4Year , day4Month , day4Day , day5Year , day5Month , day5Day , type1 , type2 , type3 , type4 , type5 , description1 , description2 , description3 , faceAmount1 , faceAmount2 , faceAmount3 , faceAmount4 , amount1 , amount2 , amount3 , amount4 , amount5 , amount6 , amount7 , amount8 , amount9 , amount10 , amount11 , amount12 , amount13 , amount14 , amount15 , amount16 , amount17 , amount18 , amount19 , amount20 , interestRate1 , interestRate2 , interestRate3 , interestRate4 , interestRate5 , fundCode , fundId , fundType , nofUnits , sequenceNumber , correspondenceLang , formLetterNumber , groupNumber , participantIDNumber , identificationNumber , userName , userRoleDescription , applicationNumber , promotionCode , providerName , attentionLine , providerAddress1 , providerAddress2 , providerCity , providerState , providerZipCode , providerTelephoneNumber , providerFaxTelephoneNumber , providerPhoneCountryCode , providerEmailAddress , providerTaxIdNumber , providerNationalProviderId , julianYear , julianDay , claimSequenceNumber , claimSuffix , caseNumber , referralNumber FROM ClientCorrespondence WHERE policyNumber LIKE '"+var_PolicyNumber+"';";

					System.out.println("SQL Query" + sql);

					Statement statement = con.createStatement();
					System.out.println("statement to database.");
					ResultSet rs = null;

					ArrayList<String> clientCorrTypeFromDB  = new ArrayList<String>();
					ArrayList<String> clientCorrLetterDateYearFromDB  = new ArrayList<String>();
					ArrayList<String> policyNumberFromDB  = new ArrayList<String>();
					rs = statement.executeQuery(sql);
					System.out.println("result set to database.");
					System.out.println("rs  ===" + rs);
					System.out.println("rs  ===" + rs.toString());
					while (rs.next()) {

						clientCorrTypeFromDB.add(rs.getString("clientCorrType"));
						clientCorrLetterDateYearFromDB.add(rs.getString("clientCorrLetterDateYear"));
						policyNumberFromDB.add(rs.getString("policyNumber"));
					}
					statement.close();

					con.close();

					String clientCorrTypeFromDB1 = "";
					clientCorrTypeFromDB1 = clientCorrTypeFromDB.get(0);

					writeToCSV("Policy Number",var_PolicyNumber);
					writeToCSV("FieldName","ClientCorrType");
					writeToCSV("Value From DB",clientCorrTypeFromDB1);
					writeToCSV("Value From GIAS",var_CorrespondenceType);

					if (var_CorrespondenceType.contains(clientCorrTypeFromDB1) && clientCorrTypeFromDB1 != ""){
						writeToCSV("Status","PASS");
					}else{
						writeToCSV("Status","FAIL");
					}

					String clientCorrLetterDateYearFromDB1 = clientCorrLetterDateYearFromDB.get(0);
					String yearFromGIAS = var_SessionDate3.substring(var_SessionDate3.length() - 4);
					writeToCSV("Policy Number",var_PolicyNumber);
					writeToCSV("FieldName","ClientCorrLetterDateYear");
					writeToCSV("Value From DB",clientCorrLetterDateYearFromDB1);
					writeToCSV("Value From GIAS",yearFromGIAS);

					if (yearFromGIAS.contains(clientCorrLetterDateYearFromDB1) && clientCorrLetterDateYearFromDB1 != ""){
						writeToCSV("Status","PASS");
					}else{
						writeToCSV("Status","FAIL");
					}


					String policyNumberFromDB1 = policyNumberFromDB.get(0).trim();
					writeToCSV("Policy Number",var_PolicyNumber);
					writeToCSV("FieldName","PolicyNumber");
					writeToCSV("Value From DB",policyNumberFromDB1);
					writeToCSV("Value From GIAS",var_PolicyNumber);
					if (var_PolicyNumber.equalsIgnoreCase(policyNumberFromDB1) && policyNumberFromDB1 != ""){
						writeToCSV("Status","PASS");
					}else{
						writeToCSV("Status","FAIL");
					}
		 
		 ;} 
		 catch(Exception e) { PrintVal("Renderer Script Exception : "+e.toString()); } 
		 PrintVal("Executed Step = CALLRENDERERSCRIPT,DATABSEREPORTFORNW056"); 
		var_CountN = var_CountN + ( 1 );
                 LOGGER.info("Executed Step = INCREASEVAR,var_CountN,1,TGTYPESCREENREG");
    	}
          LOGGER.info("Executed Step = WEND,TGTYPESCREENREG");
    	resetAttemps();

    }


    @Test
    public void nw056automatedtestplan() {

        // No need to declare / print method name here, handled by TestNg listener
        // declare("nw056automatedtestplan");

        CALL.$("LoginGIAS","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_USERNAME","//input[@id='guestForm:username']","autoapi","FALSE","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_PASSWORD","//input[@id='guestForm:password']","Autoapi!23","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_LOGIN","//button[contains(.,'Login')]","TGTYPESCREENREG");

        WAIT.$("ELE_BUTTON_GIAS_LOGO","//img[@id='headerForm:headerLogoImage']","VISIBLE","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

		String var_dataToPass;
                 LOGGER.info("Executed Step = VAR,String,var_dataToPass,TGTYPESCREENREG");
		int var_Count = 0;
                 LOGGER.info("Executed Step = VAR,Integer,var_Count,0,TGTYPESCREENREG");
		int var_CountN = 0;
                 LOGGER.info("Executed Step = VAR,Integer,var_CountN,0,TGTYPESCREENREG");
        LOGGER.info("Executed Step = REPEAT IF");
    	while (check(var_CountN,"<",1,"TGTYPESCREENREG")) {
          LOGGER.info("Executed Step = WHILE,var_CountN,<,1,TGTYPESCREENREG");
		String var_FileName = "http://10.1.104.229/json/20210524/vg9Maw.json";
                 LOGGER.info("Executed Step = VAR,String,var_FileName,http://10.1.104.229/json/20210524/vg9Maw.json,TGTYPESCREENREG");
		com.jayway.jsonpath.ReadContext var_CSVData = getJsonData("http://10.1.104.229/json/20210524/vg9Maw.json");
                 LOGGER.info("Executed Step = VAR,Json,var_CSVData,http://10.1.104.229/json/20210524/vg9Maw.json,TGTYPESCREENREG");
        CALL.$("SGD178ForNW056Automation","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_GIAS_LOGO","//img[@id='headerForm:headerLogoImage']","TGTYPESCREENREG");

		String var_PlanCodeForSGD178;
                 LOGGER.info("Executed Step = VAR,String,var_PlanCodeForSGD178,TGTYPESCREENREG");
		var_PlanCodeForSGD178 = getJsonData(var_CSVData , "$.records["+var_Count+"].PlanCodeForSGD178");
                 LOGGER.info("Executed Step = STORE,var_PlanCodeForSGD178,var_CSVData,$.records[{var_Count}].PlanCodeForSGD178,TGTYPESCREENREG");
        WAIT.$(5,"TGTYPESCREENREG");

        SWIPE.$("DOWN","ELE_TXTBOX_SEARCHTASKS","//input[@id='workbenchForm:workbenchTabs:taskSearchContainer:taskSearchSearch']","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_SEARCHTASKS","//input[@id='workbenchForm:workbenchTabs:taskSearchContainer:taskSearchSearch']","Plan Setup","FALSE","TGTYPESCREENREG");

        WAIT.$(3,"TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SEARCHTASKS","//a[@id='workbenchForm:workbenchTabs:taskSearchContainer:tb_buttonSearch']//span[@class='fa fa-search fa-lg']","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        TAP.$("ELE_LINK_FIRST_TASKS","//button[@id='workbenchForm:workbenchTabs:taskSearchContainer:0:availableTask']","TGTYPESCREENREG");

        WAIT.$("ELE_TXTBOX_FILTER_PLANCODE","//input[@id='workbenchForm:workbenchTabs:grid:planCode_Col:filter']","VISIBLE","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_FILTER_PLANCODE","//input[@id='workbenchForm:workbenchTabs:grid:planCode_Col:filter']",var_PlanCodeForSGD178,"FALSE","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        TAP.$("ELE_LINK_FIRST_PLANCODE","//span[@id='workbenchForm:workbenchTabs:grid:0:planCode']","TGTYPESCREENREG");

        WAIT.$("ELE_BUTTON_EDIT","//button[@id='workbenchForm:workbenchTabs:tb_buttonUpdate']","VISIBLE","TGTYPESCREENREG");

        TAP.$("ELE_TAB_NONFORFEITURE","//span[contains(text(),'Nonforfeiture')]","TGTYPESCREENREG");

        WAIT.$(4,"TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_EDIT","//button[@id='workbenchForm:workbenchTabs:tb_buttonUpdate']","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        TAP.$("ELE_DRPDWN_AUTOMATICPREMIUMLOANMETHOD","//label[@id='workbenchForm:workbenchTabs:aplCalculationMethod_label']","TGTYPESCREENREG");

		try { 
		 

					Thread.sleep(1000);

					for (int i = 0; i < 4; i++){
						WebElement selectValueOfDropDown= driver.findElement(By.xpath("//li[@id='workbenchForm:workbenchTabs:aplCalculationMethod_"+ i +"']"));

							AddToReport("ASSERT,Default Option  = "+selectValueOfDropDown.getText());
					    	takeScreenshot();
						    Thread.sleep(2000);

					}


		//
		//			//li[@id='workbenchForm:workbenchTabs:aplCalculationMethod_1']
		//			int totalRowCount = 0;
		//
		//			Boolean isComplete = false;
		//			Boolean isPlanAvail = false;
		//
		//			while (isComplete == false){
		//				List<WebElement> eleTransactionDate = driver.findElements(By.xpath("//li[@id='workbenchForm:workbenchTabs:aplCalculationMethod_"+totalRowCount+"']"));
		//
		//				if (eleTransactionDate.size()>0){
		//					totalRowCount = totalRowCount + 1;
		//				} else {
		//					isComplete = true;
		//				}
		//			}
		//
		//			LOGGER.info("Total Row count : "+ totalRowCount);
		//
		//			for (int i = 0; i < totalRowCount; i++){
		//
		//				WebElement eleTransactionDate = driver.findElement(By.xpath("//li[@id='workbenchForm:workbenchTabs:aplCalculationMethod_"+totalRowCount+"']"));
		//
		//				String strTransactionDate = eleTransactionDate.getText();
		//
		//
		//				AddToReport("ASSERT, Automatic Premium Loan Method , =, "+strTransactionDate);
		//				LOGGER.info("Automatic Premium Loan Method , =, "+ strTransactionDate);
		//				takeScreenshot();
		//				Thread.sleep(2000);
		//
		//			}
		 
		 ;} 
		 catch(Exception e) { PrintVal("Renderer Script Exception : "+e.toString()); } 
		 PrintVal("Executed Step = CALLRENDERERSCRIPT,VERIFFY_ALL_AUTOMATICPREMIUMLOANMETHOD "); 
        WAIT.$(5,"TGTYPESCREENREG");

        TAP.$("ELE_DRPDWN_SURRENDERDIVIDENDSTOPAYPREMIUMS","//label[@id='workbenchForm:workbenchTabs:divSurrAlloForNFO_label']","TGTYPESCREENREG");

		try { 
		 

					Thread.sleep(1000);

					for (int i = 0; i < 4; i++){
						WebElement selectValueOfDropDown= driver.findElement(By.xpath("//li[@id='workbenchForm:workbenchTabs:divSurrAlloForNFO_"+ i +"']"));

							AddToReport("ASSERT,Surrender Dividends to Pay Premiums  = "+selectValueOfDropDown.getText());
					    	takeScreenshot();
						    Thread.sleep(2000);

					}



					//li[@id='workbenchForm:workbenchTabs:divSurrAlloForNFO_3']

		//			int totalRowCount = 0;
		//
		//			Boolean isComplete = false;
		//			Boolean isPlanAvail = false;
		//
		//			while (isComplete == false){
		//				List<WebElement> eleTransactionDate = driver.findElements(By.xpath("//li[@id='workbenchForm:workbenchTabs:divSurrAlloForNFO_"+totalRowCount+"']"));
		//
		//				if (eleTransactionDate.size()>0){
		//					totalRowCount = totalRowCount + 1;
		//				} else {
		//					isComplete = true;
		//				}
		//			}
		//
		//			LOGGER.info("Total Row count : "+ totalRowCount);
		//
		//			for (int i = 0; i < totalRowCount; i++){
		//
		//				WebElement eleTransactionDate = driver.findElement(By.xpath("//li[@id='workbenchForm:workbenchTabs:divSurrAlloForNFO_"+totalRowCount+"']"));
		//
		//				String strTransactionDate = eleTransactionDate.getText();
		//
		//				LOGGER.info("Surrender Dividends to Pay Premiums = "+strTransactionDate);
		//				AddToReport("ASSERT,Surrender Dividends to Pay Premiums =, "+strTransactionDate);
		//				takeScreenshot();
		//				Thread.sleep(2000);
		//
		//			}
		 
		 ;} 
		 catch(Exception e) { PrintVal("Renderer Script Exception : "+e.toString()); } 
		 PrintVal("Executed Step = CALLRENDERERSCRIPT,VERIFY_ALL_SURRENDERDIVIDENDSTOPAYPREMIUMS"); 
        WAIT.$(5,"TGTYPESCREENREG");

        TAP.$("ELE_DRPDWN_NONFORFEITUREOPTION","//label[@id='workbenchForm:workbenchTabs:defaultNonForfOpt_label']","TGTYPESCREENREG");

		try { 
		 
					Thread.sleep(1000);

					for (int i = 0; i < 7; i++){
						WebElement selectValueOfDropDown= driver.findElement(By.xpath("//li[@id='workbenchForm:workbenchTabs:defaultNonForfOpt_"+ i +"']"));

							AddToReport("ASSERT,Default Option = "+selectValueOfDropDown.getText());
					    	takeScreenshot();
						    Thread.sleep(2000);

					}



					//li[@id='workbenchForm:workbenchTabs:defaultNonForfOpt_5']

		//			int totalRowCount = 0;
		//
		//			Boolean isComplete = false;
		//			Boolean isPlanAvail = false;
		//
		//			while (isComplete == false){
		//				List<WebElement> eleTransactionDate = driver.findElements(By.xpath("//li[@id='workbenchForm:workbenchTabs:defaultNonForfOpt_"+totalRowCount+"']"));
		//
		//				if (eleTransactionDate.size()>0){
		//					totalRowCount = totalRowCount + 1;
		//				} else {
		//					isComplete = true;
		//				}
		//			}
		//
		//			LOGGER.info("Total Row count : "+ totalRowCount);
		//
		//			for (int i = 0; i < totalRowCount; i++){
		//
		//				WebElement eleTransactionDate = driver.findElement(By.xpath("//li[@id='workbenchForm:workbenchTabs:defaultNonForfOpt_"+totalRowCount+"']"));
		//
		//				String strTransactionDate = eleTransactionDate.getText();
		//
		//				LOGGER.info("Default Option "+ strTransactionDate);
		//				AddToReport("ASSERT,Default Option , =, "+ strTransactionDate);
		//				takeScreenshot();
		//				Thread.sleep(2000);
		//
		//			}
		 
		 ;} 
		 catch(Exception e) { PrintVal("Renderer Script Exception : "+e.toString()); } 
		 PrintVal("Executed Step = CALLRENDERERSCRIPT,VERIFY_ALL_DEFAULTOPTION_PLAN"); 
        WAIT.$(5,"TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_GIAS_LOGO","//img[@id='headerForm:headerLogoImage']","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        CALL.$("SGD179ForNW056AutomationTestPlan","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_GIAS_LOGO","//img[@id='headerForm:headerLogoImage']","TGTYPESCREENREG");

		String var_PlanCode;
                 LOGGER.info("Executed Step = VAR,String,var_PlanCode,TGTYPESCREENREG");
		var_PlanCode = getJsonData(var_CSVData , "$.records["+var_Count+"].PlanCode");
                 LOGGER.info("Executed Step = STORE,var_PlanCode,var_CSVData,$.records[{var_Count}].PlanCode,TGTYPESCREENREG");
		String var_AutomaticPremiumLoanMethod;
                 LOGGER.info("Executed Step = VAR,String,var_AutomaticPremiumLoanMethod,TGTYPESCREENREG");
		var_AutomaticPremiumLoanMethod = getJsonData(var_CSVData , "$.records["+var_Count+"].AutomaticPremiumLoanMethod");
                 LOGGER.info("Executed Step = STORE,var_AutomaticPremiumLoanMethod,var_CSVData,$.records[{var_Count}].AutomaticPremiumLoanMethod,TGTYPESCREENREG");
		String var_SurrenderDividendstoPayPremiums;
                 LOGGER.info("Executed Step = VAR,String,var_SurrenderDividendstoPayPremiums,TGTYPESCREENREG");
		var_SurrenderDividendstoPayPremiums = getJsonData(var_CSVData , "$.records["+var_Count+"].SurrenderDividendstoPayPremiums");
                 LOGGER.info("Executed Step = STORE,var_SurrenderDividendstoPayPremiums,var_CSVData,$.records[{var_Count}].SurrenderDividendstoPayPremiums,TGTYPESCREENREG");
		String var_DefaultOption;
                 LOGGER.info("Executed Step = VAR,String,var_DefaultOption,TGTYPESCREENREG");
		var_DefaultOption = getJsonData(var_CSVData , "$.records["+var_Count+"].DefaultOption");
                 LOGGER.info("Executed Step = STORE,var_DefaultOption,var_CSVData,$.records[{var_Count}].DefaultOption,TGTYPESCREENREG");
        WAIT.$(5,"TGTYPESCREENREG");

        SWIPE.$("DOWN","ELE_TXTBOX_SEARCHTASKS","//input[@id='workbenchForm:workbenchTabs:taskSearchContainer:taskSearchSearch']","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_SEARCHTASKS","//input[@id='workbenchForm:workbenchTabs:taskSearchContainer:taskSearchSearch']","Plan Setup","FALSE","TGTYPESCREENREG");

        WAIT.$(3,"TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SEARCHTASKS","//a[@id='workbenchForm:workbenchTabs:taskSearchContainer:tb_buttonSearch']//span[@class='fa fa-search fa-lg']","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        TAP.$("ELE_LINK_FIRST_TASKS","//button[@id='workbenchForm:workbenchTabs:taskSearchContainer:0:availableTask']","TGTYPESCREENREG");

        WAIT.$("ELE_TXTBOX_FILTER_PLANCODE","//input[@id='workbenchForm:workbenchTabs:grid:planCode_Col:filter']","VISIBLE","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_FILTER_PLANCODE","//input[@id='workbenchForm:workbenchTabs:grid:planCode_Col:filter']",var_PlanCode,"FALSE","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        TAP.$("ELE_LINK_FIRST_PLANCODE","//span[@id='workbenchForm:workbenchTabs:grid:0:planCode']","TGTYPESCREENREG");

        WAIT.$("ELE_BUTTON_EDIT","//button[@id='workbenchForm:workbenchTabs:tb_buttonUpdate']","VISIBLE","TGTYPESCREENREG");

        TAP.$("ELE_TAB_NONFORFEITURE","//span[contains(text(),'Nonforfeiture')]","TGTYPESCREENREG");

        WAIT.$(4,"TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_EDIT","//button[@id='workbenchForm:workbenchTabs:tb_buttonUpdate']","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

		var_dataToPass = var_AutomaticPremiumLoanMethod;
                 LOGGER.info("Executed Step = STORE,var_dataToPass,var_AutomaticPremiumLoanMethod,TGTYPESCREENREG");
        TAP.$("ELE_DRPDWN_AUTOMATICPREMIUMLOANMETHOD","//label[@id='workbenchForm:workbenchTabs:aplCalculationMethod_label']","TGTYPESCREENREG");

		try { 
		 //li[@id='workbenchForm:workbenchTabs:aplCalculationMethod_1']



			Thread.sleep(1000);
					for (int i = 0; i < 2000; i++){
						WebElement selectValueOfDropDown= driver.findElement(By.xpath("//li[@id='workbenchForm:workbenchTabs:aplCalculationMethod_"+ i +"']"));

						if (selectValueOfDropDown.getText().contains(var_dataToPass)){
		AddToReport("ASSERT," + selectValueOfDropDown.getText() +", CONTAINS,"+var_dataToPass);
		                                selectValueOfDropDown.click();
							
						
							break;
						}
					}
					takeScreenshot();

					Thread.sleep(2000);
		 
		 ;} 
		 catch(Exception e) { PrintVal("Renderer Script Exception : "+e.toString()); } 
		 PrintVal("Executed Step = CALLRENDERERSCRIPT,SELECT_AUTOMATICPREMIUMLOANMETHOD"); 
        WAIT.$(5,"TGTYPESCREENREG");

		var_dataToPass = var_SurrenderDividendstoPayPremiums;
                 LOGGER.info("Executed Step = STORE,var_dataToPass,var_SurrenderDividendstoPayPremiums,TGTYPESCREENREG");
        TAP.$("ELE_DRPDWN_SURRENDERDIVIDENDSTOPAYPREMIUMS","//label[@id='workbenchForm:workbenchTabs:divSurrAlloForNFO_label']","TGTYPESCREENREG");

		try { 
		 //li[@id='workbenchForm:workbenchTabs:divSurrAlloForNFO_3']


			Thread.sleep(1000);
					for (int i = 0; i < 2000; i++){
						WebElement selectValueOfDropDown= driver.findElement(By.xpath("//li[@id='workbenchForm:workbenchTabs:divSurrAlloForNFO_"+ i +"']"));

						if (selectValueOfDropDown.getText().contains(var_dataToPass)){
		AddToReport("ASSERT," + selectValueOfDropDown.getText() +", CONTAINS,"+var_dataToPass);
		                                selectValueOfDropDown.click();
							
						
							break;
						}
					}
					takeScreenshot();

					Thread.sleep(2000);
		 
		 ;} 
		 catch(Exception e) { PrintVal("Renderer Script Exception : "+e.toString()); } 
		 PrintVal("Executed Step = CALLRENDERERSCRIPT,SELECT_SURRENDERDIVIDENDSTOPAYPREMIUMS "); 
        WAIT.$(5,"TGTYPESCREENREG");

		var_dataToPass = var_DefaultOption;
                 LOGGER.info("Executed Step = STORE,var_dataToPass,var_DefaultOption,TGTYPESCREENREG");
        TAP.$("ELE_DRPDWN_NONFORFEITUREOPTION","//label[@id='workbenchForm:workbenchTabs:defaultNonForfOpt_label']","TGTYPESCREENREG");

		try { 
		 //li[@id='workbenchForm:workbenchTabs:defaultNonForfOpt_5']


			Thread.sleep(1000);
					for (int i = 0; i < 2000; i++){
						WebElement selectValueOfDropDown= driver.findElement(By.xpath("//li[@id='workbenchForm:workbenchTabs:defaultNonForfOpt_"+ i +"']"));

						if (selectValueOfDropDown.getText().contains(var_dataToPass)){
		AddToReport("ASSERT," + selectValueOfDropDown.getText() +", CONTAINS,"+var_dataToPass);
		                                selectValueOfDropDown.click();
							
						
							break;
						}
					}
					takeScreenshot();

					Thread.sleep(2000);
		 
		 ;} 
		 catch(Exception e) { PrintVal("Renderer Script Exception : "+e.toString()); } 
		 PrintVal("Executed Step = CALLRENDERERSCRIPT,SELECT_DEFAIULT_OPETION_NONFOR"); 
        WAIT.$(5,"TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_VALIDATE","//*[@id=\"workbenchForm:workbenchTabs:tb_buttonEdit\"]","TGTYPESCREENREG");

        SWIPE.$("DOWN","TGTYPESCREENREG");

        SWIPE.$("DOWN","TGTYPESCREENREG");

        SWIPE.$("DOWN","TGTYPESCREENREG");

        SWIPE.$("DOWN","TGTYPESCREENREG");

        ASSERT.$("ELE_MSG_SUCCESSFULLY","//span[contains(@class,'ui-messages-info-summary')]","VISIBLE","TGTYPESCREENREG");

		var_dataToPass = getJsonData(var_CSVData , "$.records["+var_Count+"].SurrenderDividendstoPayPremiumsNew");
                 LOGGER.info("Executed Step = STORE,var_dataToPass,var_CSVData,$.records[{var_Count}].SurrenderDividendstoPayPremiumsNew,TGTYPESCREENREG");
        TAP.$("ELE_DRPDWN_SURRENDERDIVIDENDSTOPAYPREMIUMS","//label[@id='workbenchForm:workbenchTabs:divSurrAlloForNFO_label']","TGTYPESCREENREG");

		try { 
		 //li[@id='workbenchForm:workbenchTabs:divSurrAlloForNFO_3']


			Thread.sleep(1000);
					for (int i = 0; i < 2000; i++){
						WebElement selectValueOfDropDown= driver.findElement(By.xpath("//li[@id='workbenchForm:workbenchTabs:divSurrAlloForNFO_"+ i +"']"));

						if (selectValueOfDropDown.getText().contains(var_dataToPass)){
		AddToReport("ASSERT," + selectValueOfDropDown.getText() +", CONTAINS,"+var_dataToPass);
		                                selectValueOfDropDown.click();
							
						
							break;
						}
					}
					takeScreenshot();

					Thread.sleep(2000);
		 
		 ;} 
		 catch(Exception e) { PrintVal("Renderer Script Exception : "+e.toString()); } 
		 PrintVal("Executed Step = CALLRENDERERSCRIPT,SELECT_SURRENDERDIVIDENDSTOPAYPREMIUMS "); 
        WAIT.$(5,"TGTYPESCREENREG");

		var_dataToPass = getJsonData(var_CSVData , "$.records["+var_Count+"].DefaultOptionNew");
                 LOGGER.info("Executed Step = STORE,var_dataToPass,var_CSVData,$.records[{var_Count}].DefaultOptionNew,TGTYPESCREENREG");
        TAP.$("ELE_DRPDWN_NONFORFEITUREOPTION","//label[@id='workbenchForm:workbenchTabs:defaultNonForfOpt_label']","TGTYPESCREENREG");

		try { 
		 //li[@id='workbenchForm:workbenchTabs:defaultNonForfOpt_5']


			Thread.sleep(1000);
					for (int i = 0; i < 2000; i++){
						WebElement selectValueOfDropDown= driver.findElement(By.xpath("//li[@id='workbenchForm:workbenchTabs:defaultNonForfOpt_"+ i +"']"));

						if (selectValueOfDropDown.getText().contains(var_dataToPass)){
		AddToReport("ASSERT," + selectValueOfDropDown.getText() +", CONTAINS,"+var_dataToPass);
		                                selectValueOfDropDown.click();
							
						
							break;
						}
					}
					takeScreenshot();

					Thread.sleep(2000);
		 
		 ;} 
		 catch(Exception e) { PrintVal("Renderer Script Exception : "+e.toString()); } 
		 PrintVal("Executed Step = CALLRENDERERSCRIPT,SELECT_DEFAIULT_OPETION_NONFOR"); 
        WAIT.$(5,"TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_VALIDATE","//*[@id=\"workbenchForm:workbenchTabs:tb_buttonEdit\"]","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        ASSERT.$("ELE_MSG_ERROR_SUMMARY","//span[contains(@class,'ui-messages-error-summary')]","VISIBLE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_GIAS_LOGO","//img[@id='headerForm:headerLogoImage']","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        CALL.$("SGD198ForNW056AutomationTestPlan","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_GIAS_LOGO","//img[@id='headerForm:headerLogoImage']","TGTYPESCREENREG");

		String var_PlanCode1;
                 LOGGER.info("Executed Step = VAR,String,var_PlanCode1,TGTYPESCREENREG");
		var_PlanCode1 = getJsonData(var_CSVData , "$.records["+var_Count+"].PlanCode1");
                 LOGGER.info("Executed Step = STORE,var_PlanCode1,var_CSVData,$.records[{var_Count}].PlanCode1,TGTYPESCREENREG");
		String var_AutomaticPremiumLoanMethod1;
                 LOGGER.info("Executed Step = VAR,String,var_AutomaticPremiumLoanMethod1,TGTYPESCREENREG");
		var_AutomaticPremiumLoanMethod1 = getJsonData(var_CSVData , "$.records["+var_Count+"].AutomaticPremiumLoanMethod1");
                 LOGGER.info("Executed Step = STORE,var_AutomaticPremiumLoanMethod1,var_CSVData,$.records[{var_Count}].AutomaticPremiumLoanMethod1,TGTYPESCREENREG");
		String var_SurrenderDividendstoPayPremiums1;
                 LOGGER.info("Executed Step = VAR,String,var_SurrenderDividendstoPayPremiums1,TGTYPESCREENREG");
		var_SurrenderDividendstoPayPremiums1 = getJsonData(var_CSVData , "$.records["+var_Count+"].SurrenderDividendstoPayPremiums1");
                 LOGGER.info("Executed Step = STORE,var_SurrenderDividendstoPayPremiums1,var_CSVData,$.records[{var_Count}].SurrenderDividendstoPayPremiums1,TGTYPESCREENREG");
		String var_DefaultOption1;
                 LOGGER.info("Executed Step = VAR,String,var_DefaultOption1,TGTYPESCREENREG");
		var_DefaultOption1 = getJsonData(var_CSVData , "$.records["+var_Count+"].DefaultOption1");
                 LOGGER.info("Executed Step = STORE,var_DefaultOption1,var_CSVData,$.records[{var_Count}].DefaultOption1,TGTYPESCREENREG");
        WAIT.$(5,"TGTYPESCREENREG");

        SWIPE.$("DOWN","ELE_TXTBOX_SEARCHTASKS","//input[@id='workbenchForm:workbenchTabs:taskSearchContainer:taskSearchSearch']","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_SEARCHTASKS","//input[@id='workbenchForm:workbenchTabs:taskSearchContainer:taskSearchSearch']","Plan Setup","FALSE","TGTYPESCREENREG");

        WAIT.$(3,"TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SEARCHTASKS","//a[@id='workbenchForm:workbenchTabs:taskSearchContainer:tb_buttonSearch']//span[@class='fa fa-search fa-lg']","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        TAP.$("ELE_LINK_FIRST_TASKS","//button[@id='workbenchForm:workbenchTabs:taskSearchContainer:0:availableTask']","TGTYPESCREENREG");

        WAIT.$("ELE_TXTBOX_FILTER_PLANCODE","//input[@id='workbenchForm:workbenchTabs:grid:planCode_Col:filter']","VISIBLE","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_FILTER_PLANCODE","//input[@id='workbenchForm:workbenchTabs:grid:planCode_Col:filter']",var_PlanCode1,"FALSE","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        TAP.$("ELE_LINK_FIRST_PLANCODE","//span[@id='workbenchForm:workbenchTabs:grid:0:planCode']","TGTYPESCREENREG");

        WAIT.$("ELE_BUTTON_EDIT","//button[@id='workbenchForm:workbenchTabs:tb_buttonUpdate']","VISIBLE","TGTYPESCREENREG");

        TAP.$("ELE_TAB_NONFORFEITURE","//span[contains(text(),'Nonforfeiture')]","TGTYPESCREENREG");

        WAIT.$(4,"TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_EDIT","//button[@id='workbenchForm:workbenchTabs:tb_buttonUpdate']","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

		var_dataToPass = var_SurrenderDividendstoPayPremiums1;
                 LOGGER.info("Executed Step = STORE,var_dataToPass,var_SurrenderDividendstoPayPremiums1,TGTYPESCREENREG");
        TAP.$("ELE_DRPDWN_SURRENDERDIVIDENDSTOPAYPREMIUMS","//label[@id='workbenchForm:workbenchTabs:divSurrAlloForNFO_label']","TGTYPESCREENREG");

		try { 
		 //li[@id='workbenchForm:workbenchTabs:divSurrAlloForNFO_3']


			Thread.sleep(1000);
					for (int i = 0; i < 2000; i++){
						WebElement selectValueOfDropDown= driver.findElement(By.xpath("//li[@id='workbenchForm:workbenchTabs:divSurrAlloForNFO_"+ i +"']"));

						if (selectValueOfDropDown.getText().contains(var_dataToPass)){
		AddToReport("ASSERT," + selectValueOfDropDown.getText() +", CONTAINS,"+var_dataToPass);
		                                selectValueOfDropDown.click();
							
						
							break;
						}
					}
					takeScreenshot();

					Thread.sleep(2000);
		 
		 ;} 
		 catch(Exception e) { PrintVal("Renderer Script Exception : "+e.toString()); } 
		 PrintVal("Executed Step = CALLRENDERERSCRIPT,SELECT_SURRENDERDIVIDENDSTOPAYPREMIUMS "); 
        WAIT.$(5,"TGTYPESCREENREG");

		var_dataToPass = var_DefaultOption1;
                 LOGGER.info("Executed Step = STORE,var_dataToPass,var_DefaultOption1,TGTYPESCREENREG");
        TAP.$("ELE_DRPDWN_NONFORFEITUREOPTION","//label[@id='workbenchForm:workbenchTabs:defaultNonForfOpt_label']","TGTYPESCREENREG");

		try { 
		 //li[@id='workbenchForm:workbenchTabs:defaultNonForfOpt_5']


			Thread.sleep(1000);
					for (int i = 0; i < 2000; i++){
						WebElement selectValueOfDropDown= driver.findElement(By.xpath("//li[@id='workbenchForm:workbenchTabs:defaultNonForfOpt_"+ i +"']"));

						if (selectValueOfDropDown.getText().contains(var_dataToPass)){
		AddToReport("ASSERT," + selectValueOfDropDown.getText() +", CONTAINS,"+var_dataToPass);
		                                selectValueOfDropDown.click();
							
						
							break;
						}
					}
					takeScreenshot();

					Thread.sleep(2000);
		 
		 ;} 
		 catch(Exception e) { PrintVal("Renderer Script Exception : "+e.toString()); } 
		 PrintVal("Executed Step = CALLRENDERERSCRIPT,SELECT_DEFAIULT_OPETION_NONFOR"); 
        WAIT.$(5,"TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SAVE","//span[contains(text(),'Save')]","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        SWIPE.$("DOWN","TGTYPESCREENREG");

        SWIPE.$("DOWN","ELE_MSG_SUCCESSFULLY","//span[contains(@class,'ui-messages-info-summary')]","TGTYPESCREENREG");

        ASSERT.$("ELE_MSG_SUCCESSFULLY","//span[contains(@class,'ui-messages-info-summary')]","VISIBLE","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_GIAS_LOGO","//img[@id='headerForm:headerLogoImage']","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        CALL.$("SGD198VerifyBenefitFirst","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

		String var_BenefitPlan;
                 LOGGER.info("Executed Step = VAR,String,var_BenefitPlan,TGTYPESCREENREG");
		var_BenefitPlan = getJsonData(var_CSVData , "$.records["+var_Count+"].BenefitPlan");
                 LOGGER.info("Executed Step = STORE,var_BenefitPlan,var_CSVData,$.records[{var_Count}].BenefitPlan,TGTYPESCREENREG");
		String var_PolicyNumber;
                 LOGGER.info("Executed Step = VAR,String,var_PolicyNumber,TGTYPESCREENREG");
		var_PolicyNumber = getJsonData(var_CSVData , "$.records["+var_Count+"].PolicyNumber");
                 LOGGER.info("Executed Step = STORE,var_PolicyNumber,var_CSVData,$.records[{var_Count}].PolicyNumber,TGTYPESCREENREG");
		String var_BenefitNonforfeitureOption;
                 LOGGER.info("Executed Step = VAR,String,var_BenefitNonforfeitureOption,TGTYPESCREENREG");
		var_BenefitNonforfeitureOption = getJsonData(var_CSVData , "$.records["+var_Count+"].BenefitNonforfeitureOption");
                 LOGGER.info("Executed Step = STORE,var_BenefitNonforfeitureOption,var_CSVData,$.records[{var_Count}].BenefitNonforfeitureOption,TGTYPESCREENREG");
        SWIPE.$("DOWN","ELE_TXTBOX_SEARCHTASKS","//input[@id='workbenchForm:workbenchTabs:taskSearchContainer:taskSearchSearch']","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_SEARCHTASKS","//input[@id='workbenchForm:workbenchTabs:taskSearchContainer:taskSearchSearch']","Policy inquiry","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SEARCHTASKS","//a[@id='workbenchForm:workbenchTabs:taskSearchContainer:tb_buttonSearch']//span[@class='fa fa-search fa-lg']","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        TAP.$("ELE_LINK_POLICYINQUIRY","//span[contains(text(),'Policy Inquiry')]","TGTYPESCREENREG");

        WAIT.$("ELE_TXTBOX_POLICYNUMBER","//input[@id='workbenchForm:workbenchTabs:policyNumber']","VISIBLE","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_POLICYNUMBER","//input[@id='workbenchForm:workbenchTabs:policyNumber']",var_PolicyNumber,"FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_CONTINUE","//span[contains(text(),'Continue')]","TGTYPESCREENREG");

        WAIT.$("ELE_LABEL_POLICYNUMBER","//span[@id='workbenchForm:workbenchTabs:policyNumber']","VISIBLE","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SHOWLINKS","//button[@id='workbenchForm:workbenchTabs:tb_buttonLink']","TGTYPESCREENREG");

        TAP.$("ELE_LINK_BENEFITS","//span[contains(text(),'Benefits')]","TGTYPESCREENREG");

        WAIT.$("ELE_DRPDWN_BENEFITPLAN","//label[@id='workbenchForm:workbenchTabs:initialPlanCode_label']","VISIBLE","TGTYPESCREENREG");

        TAP.$("ELE_DRPDWN_BENEFITPLAN","//label[@id='workbenchForm:workbenchTabs:initialPlanCode_label']","TGTYPESCREENREG");

		var_dataToPass = var_BenefitPlan;
                 LOGGER.info("Executed Step = STORE,var_dataToPass,var_BenefitPlan,TGTYPESCREENREG");
		try { 
		 Thread.sleep(1000);
		WebElement selectValueOfDropDown= driver.findElement(By.xpath("//li[@data-label='"+ var_dataToPass +"']"));

		takeScreenshot();

				selectValueOfDropDown.click();
		// Add Screenshot will only work for Android platform

		takeScreenshot();

				 Thread.sleep(1000);
		 
		 ;} 
		 catch(Exception e) { PrintVal("Renderer Script Exception : "+e.toString()); } 
		 PrintVal("Executed Step = CALLRENDERERSCRIPT,SELECTVALUEFROMDROPDOWN"); 
        WAIT.$(5,"TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_CONTINUE","//span[contains(text(),'Continue')]","TGTYPESCREENREG");

        WAIT.$("ELE_TXTBOX_UNITS","//input[@id='workbenchForm:workbenchTabs:unitsOfInsurance_input']","VISIBLE","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

		var_dataToPass = var_BenefitNonforfeitureOption;
                 LOGGER.info("Executed Step = STORE,var_dataToPass,var_BenefitNonforfeitureOption,TGTYPESCREENREG");
        TAP.$("ELE_DRPDWN_NONFORFEITUREOPTION","//label[@id='workbenchForm:workbenchTabs:defaultNonForfOpt_label']","TGTYPESCREENREG");

		try { 
		 Thread.sleep(1000);
		WebElement selectValueOfDropDown= driver.findElement(By.xpath("//li[@data-label='"+ var_dataToPass +"']"));

		takeScreenshot();

				selectValueOfDropDown.click();
		// Add Screenshot will only work for Android platform

		takeScreenshot();

				 Thread.sleep(1000);
		 
		 ;} 
		 catch(Exception e) { PrintVal("Renderer Script Exception : "+e.toString()); } 
		 PrintVal("Executed Step = CALLRENDERERSCRIPT,SELECTVALUEFROMDROPDOWN"); 
        WAIT.$(5,"TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_VALIDATE","//*[@id=\"workbenchForm:workbenchTabs:tb_buttonEdit\"]","TGTYPESCREENREG");

        WAIT.$("ELE_MSG_SUCCESSFULLY","//span[contains(@class,'ui-messages-info-summary')]","VISIBLE","TGTYPESCREENREG");

        ASSERT.$("ELE_MSG_SUCCESSFULLY","//span[contains(@class,'ui-messages-info-summary')]","VISIBLE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_GIAS_LOGO","//img[@id='headerForm:headerLogoImage']","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        CALL.$("SGD198ForSecoundNW056AutomationTestPlan","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_GIAS_LOGO","//img[@id='headerForm:headerLogoImage']","TGTYPESCREENREG");

		var_PlanCode1 = getJsonData(var_CSVData , "$.records["+var_Count+"].PlanCode1");
                 LOGGER.info("Executed Step = STORE,var_PlanCode1,var_CSVData,$.records[{var_Count}].PlanCode1,TGTYPESCREENREG");
		var_AutomaticPremiumLoanMethod1 = getJsonData(var_CSVData , "$.records["+var_Count+"].AutomaticPremiumLoanMethod1");
                 LOGGER.info("Executed Step = STORE,var_AutomaticPremiumLoanMethod1,var_CSVData,$.records[{var_Count}].AutomaticPremiumLoanMethod1,TGTYPESCREENREG");
		var_SurrenderDividendstoPayPremiums1 = getJsonData(var_CSVData , "$.records["+var_Count+"].SurrenderDividendstoPayPremiums1New");
                 LOGGER.info("Executed Step = STORE,var_SurrenderDividendstoPayPremiums1,var_CSVData,$.records[{var_Count}].SurrenderDividendstoPayPremiums1New,TGTYPESCREENREG");
		var_DefaultOption1 = getJsonData(var_CSVData , "$.records["+var_Count+"].DefaultOption1New");
                 LOGGER.info("Executed Step = STORE,var_DefaultOption1,var_CSVData,$.records[{var_Count}].DefaultOption1New,TGTYPESCREENREG");
        WAIT.$(5,"TGTYPESCREENREG");

        SWIPE.$("DOWN","ELE_TXTBOX_SEARCHTASKS","//input[@id='workbenchForm:workbenchTabs:taskSearchContainer:taskSearchSearch']","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_SEARCHTASKS","//input[@id='workbenchForm:workbenchTabs:taskSearchContainer:taskSearchSearch']","Plan Setup","FALSE","TGTYPESCREENREG");

        WAIT.$(3,"TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SEARCHTASKS","//a[@id='workbenchForm:workbenchTabs:taskSearchContainer:tb_buttonSearch']//span[@class='fa fa-search fa-lg']","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        TAP.$("ELE_LINK_FIRST_TASKS","//button[@id='workbenchForm:workbenchTabs:taskSearchContainer:0:availableTask']","TGTYPESCREENREG");

        WAIT.$("ELE_TXTBOX_FILTER_PLANCODE","//input[@id='workbenchForm:workbenchTabs:grid:planCode_Col:filter']","VISIBLE","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_FILTER_PLANCODE","//input[@id='workbenchForm:workbenchTabs:grid:planCode_Col:filter']",var_PlanCode1,"FALSE","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        TAP.$("ELE_LINK_FIRST_PLANCODE","//span[@id='workbenchForm:workbenchTabs:grid:0:planCode']","TGTYPESCREENREG");

        WAIT.$("ELE_BUTTON_EDIT","//button[@id='workbenchForm:workbenchTabs:tb_buttonUpdate']","VISIBLE","TGTYPESCREENREG");

        TAP.$("ELE_TAB_NONFORFEITURE","//span[contains(text(),'Nonforfeiture')]","TGTYPESCREENREG");

        WAIT.$(4,"TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_EDIT","//button[@id='workbenchForm:workbenchTabs:tb_buttonUpdate']","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

		var_dataToPass = var_SurrenderDividendstoPayPremiums1;
                 LOGGER.info("Executed Step = STORE,var_dataToPass,var_SurrenderDividendstoPayPremiums1,TGTYPESCREENREG");
        TAP.$("ELE_DRPDWN_SURRENDERDIVIDENDSTOPAYPREMIUMS","//label[@id='workbenchForm:workbenchTabs:divSurrAlloForNFO_label']","TGTYPESCREENREG");

		try { 
		 //li[@id='workbenchForm:workbenchTabs:divSurrAlloForNFO_3']


			Thread.sleep(1000);
					for (int i = 0; i < 2000; i++){
						WebElement selectValueOfDropDown= driver.findElement(By.xpath("//li[@id='workbenchForm:workbenchTabs:divSurrAlloForNFO_"+ i +"']"));

						if (selectValueOfDropDown.getText().contains(var_dataToPass)){
		AddToReport("ASSERT," + selectValueOfDropDown.getText() +", CONTAINS,"+var_dataToPass);
		                                selectValueOfDropDown.click();
							
						
							break;
						}
					}
					takeScreenshot();

					Thread.sleep(2000);
		 
		 ;} 
		 catch(Exception e) { PrintVal("Renderer Script Exception : "+e.toString()); } 
		 PrintVal("Executed Step = CALLRENDERERSCRIPT,SELECT_SURRENDERDIVIDENDSTOPAYPREMIUMS "); 
        WAIT.$(5,"TGTYPESCREENREG");

		var_dataToPass = var_DefaultOption1;
                 LOGGER.info("Executed Step = STORE,var_dataToPass,var_DefaultOption1,TGTYPESCREENREG");
        TAP.$("ELE_DRPDWN_NONFORFEITUREOPTION","//label[@id='workbenchForm:workbenchTabs:defaultNonForfOpt_label']","TGTYPESCREENREG");

		try { 
		 //li[@id='workbenchForm:workbenchTabs:defaultNonForfOpt_5']


			Thread.sleep(1000);
					for (int i = 0; i < 2000; i++){
						WebElement selectValueOfDropDown= driver.findElement(By.xpath("//li[@id='workbenchForm:workbenchTabs:defaultNonForfOpt_"+ i +"']"));

						if (selectValueOfDropDown.getText().contains(var_dataToPass)){
		AddToReport("ASSERT," + selectValueOfDropDown.getText() +", CONTAINS,"+var_dataToPass);
		                                selectValueOfDropDown.click();
							
						
							break;
						}
					}
					takeScreenshot();

					Thread.sleep(2000);
		 
		 ;} 
		 catch(Exception e) { PrintVal("Renderer Script Exception : "+e.toString()); } 
		 PrintVal("Executed Step = CALLRENDERERSCRIPT,SELECT_DEFAIULT_OPETION_NONFOR"); 
        WAIT.$(5,"TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SAVE","//span[contains(text(),'Save')]","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        SWIPE.$("DOWN","TGTYPESCREENREG");

        SWIPE.$("DOWN","TGTYPESCREENREG");

        SWIPE.$("DOWN","TGTYPESCREENREG");

        SWIPE.$("DOWN","TGTYPESCREENREG");

        ASSERT.$("ELE_MSG_SUCCESSFULLY","//span[contains(@class,'ui-messages-info-summary')]","VISIBLE","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_GIAS_LOGO","//img[@id='headerForm:headerLogoImage']","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        CALL.$("SGD198VerifyBenefitSecound","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

		var_BenefitPlan = getJsonData(var_CSVData , "$.records["+var_Count+"].BenefitPlan");
                 LOGGER.info("Executed Step = STORE,var_BenefitPlan,var_CSVData,$.records[{var_Count}].BenefitPlan,TGTYPESCREENREG");
		var_PolicyNumber = getJsonData(var_CSVData , "$.records["+var_Count+"].PolicyNumber");
                 LOGGER.info("Executed Step = STORE,var_PolicyNumber,var_CSVData,$.records[{var_Count}].PolicyNumber,TGTYPESCREENREG");
		var_BenefitNonforfeitureOption = getJsonData(var_CSVData , "$.records["+var_Count+"].BenefitNonforfeitureOption");
                 LOGGER.info("Executed Step = STORE,var_BenefitNonforfeitureOption,var_CSVData,$.records[{var_Count}].BenefitNonforfeitureOption,TGTYPESCREENREG");
        SWIPE.$("DOWN","ELE_TXTBOX_SEARCHTASKS","//input[@id='workbenchForm:workbenchTabs:taskSearchContainer:taskSearchSearch']","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_SEARCHTASKS","//input[@id='workbenchForm:workbenchTabs:taskSearchContainer:taskSearchSearch']","Policy inquiry","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SEARCHTASKS","//a[@id='workbenchForm:workbenchTabs:taskSearchContainer:tb_buttonSearch']//span[@class='fa fa-search fa-lg']","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        TAP.$("ELE_LINK_POLICYINQUIRY","//span[contains(text(),'Policy Inquiry')]","TGTYPESCREENREG");

        WAIT.$("ELE_TXTBOX_POLICYNUMBER","//input[@id='workbenchForm:workbenchTabs:policyNumber']","VISIBLE","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_POLICYNUMBER","//input[@id='workbenchForm:workbenchTabs:policyNumber']",var_PolicyNumber,"FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_CONTINUE","//span[contains(text(),'Continue')]","TGTYPESCREENREG");

        WAIT.$("ELE_LABEL_POLICYNUMBER","//span[@id='workbenchForm:workbenchTabs:policyNumber']","VISIBLE","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SHOWLINKS","//button[@id='workbenchForm:workbenchTabs:tb_buttonLink']","TGTYPESCREENREG");

        TAP.$("ELE_LINK_BENEFITS","//span[contains(text(),'Benefits')]","TGTYPESCREENREG");

        WAIT.$("ELE_DRPDWN_BENEFITPLAN","//label[@id='workbenchForm:workbenchTabs:initialPlanCode_label']","VISIBLE","TGTYPESCREENREG");

        TAP.$("ELE_DRPDWN_BENEFITPLAN","//label[@id='workbenchForm:workbenchTabs:initialPlanCode_label']","TGTYPESCREENREG");

		var_dataToPass = var_BenefitPlan;
                 LOGGER.info("Executed Step = STORE,var_dataToPass,var_BenefitPlan,TGTYPESCREENREG");
		try { 
		 Thread.sleep(1000);
		WebElement selectValueOfDropDown= driver.findElement(By.xpath("//li[@data-label='"+ var_dataToPass +"']"));

		takeScreenshot();

				selectValueOfDropDown.click();
		// Add Screenshot will only work for Android platform

		takeScreenshot();

				 Thread.sleep(1000);
		 
		 ;} 
		 catch(Exception e) { PrintVal("Renderer Script Exception : "+e.toString()); } 
		 PrintVal("Executed Step = CALLRENDERERSCRIPT,SELECTVALUEFROMDROPDOWN"); 
        WAIT.$(5,"TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_CONTINUE","//span[contains(text(),'Continue')]","TGTYPESCREENREG");

        WAIT.$("ELE_TXTBOX_UNITS","//input[@id='workbenchForm:workbenchTabs:unitsOfInsurance_input']","VISIBLE","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

		var_dataToPass = var_BenefitNonforfeitureOption;
                 LOGGER.info("Executed Step = STORE,var_dataToPass,var_BenefitNonforfeitureOption,TGTYPESCREENREG");
        TAP.$("ELE_DRPDWN_NONFORFEITUREOPTION","//label[@id='workbenchForm:workbenchTabs:defaultNonForfOpt_label']","TGTYPESCREENREG");

		try { 
		 Thread.sleep(1000);
		WebElement selectValueOfDropDown= driver.findElement(By.xpath("//li[@data-label='"+ var_dataToPass +"']"));

		takeScreenshot();

				selectValueOfDropDown.click();
		// Add Screenshot will only work for Android platform

		takeScreenshot();

				 Thread.sleep(1000);
		 
		 ;} 
		 catch(Exception e) { PrintVal("Renderer Script Exception : "+e.toString()); } 
		 PrintVal("Executed Step = CALLRENDERERSCRIPT,SELECTVALUEFROMDROPDOWN"); 
        WAIT.$(5,"TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_GIAS_LOGO","//img[@id='headerForm:headerLogoImage']","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        CALL.$("SGD261NW056PlanSetup","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_GIAS_LOGO","//img[@id='headerForm:headerLogoImage']","TGTYPESCREENREG");

		String var_PlanCode2;
                 LOGGER.info("Executed Step = VAR,String,var_PlanCode2,TGTYPESCREENREG");
		var_PlanCode2 = getJsonData(var_CSVData , "$.records["+var_Count+"].PlanCode2");
                 LOGGER.info("Executed Step = STORE,var_PlanCode2,var_CSVData,$.records[{var_Count}].PlanCode2,TGTYPESCREENREG");
		String var_PolicyLoanAllowed2;
                 LOGGER.info("Executed Step = VAR,String,var_PolicyLoanAllowed2,TGTYPESCREENREG");
		var_PolicyLoanAllowed2 = getJsonData(var_CSVData , "$.records["+var_Count+"].PolicyLoanAllowed2");
                 LOGGER.info("Executed Step = STORE,var_PolicyLoanAllowed2,var_CSVData,$.records[{var_Count}].PolicyLoanAllowed2,TGTYPESCREENREG");
		String var_AutomaticPremiumLoanMethod2;
                 LOGGER.info("Executed Step = VAR,String,var_AutomaticPremiumLoanMethod2,TGTYPESCREENREG");
		var_AutomaticPremiumLoanMethod2 = getJsonData(var_CSVData , "$.records["+var_Count+"].AutomaticPremiumLoanMethod2");
                 LOGGER.info("Executed Step = STORE,var_AutomaticPremiumLoanMethod2,var_CSVData,$.records[{var_Count}].AutomaticPremiumLoanMethod2,TGTYPESCREENREG");
		String var_SurrenderDividendstoPayPremiums2;
                 LOGGER.info("Executed Step = VAR,String,var_SurrenderDividendstoPayPremiums2,TGTYPESCREENREG");
		var_SurrenderDividendstoPayPremiums2 = getJsonData(var_CSVData , "$.records["+var_Count+"].SurrenderDividendstoPayPremiums2");
                 LOGGER.info("Executed Step = STORE,var_SurrenderDividendstoPayPremiums2,var_CSVData,$.records[{var_Count}].SurrenderDividendstoPayPremiums2,TGTYPESCREENREG");
		String var_DefaultOption2;
                 LOGGER.info("Executed Step = VAR,String,var_DefaultOption2,TGTYPESCREENREG");
		var_DefaultOption2 = getJsonData(var_CSVData , "$.records["+var_Count+"].DefaultOption2");
                 LOGGER.info("Executed Step = STORE,var_DefaultOption2,var_CSVData,$.records[{var_Count}].DefaultOption2,TGTYPESCREENREG");
		String var_PlanCode3;
                 LOGGER.info("Executed Step = VAR,String,var_PlanCode3,TGTYPESCREENREG");
		var_PlanCode3 = getJsonData(var_CSVData , "$.records["+var_Count+"].PlanCode3");
                 LOGGER.info("Executed Step = STORE,var_PlanCode3,var_CSVData,$.records[{var_Count}].PlanCode3,TGTYPESCREENREG");
		String var_PolicyLoanAllowed3;
                 LOGGER.info("Executed Step = VAR,String,var_PolicyLoanAllowed3,TGTYPESCREENREG");
		var_PolicyLoanAllowed3 = getJsonData(var_CSVData , "$.records["+var_Count+"].PolicyLoanAllowed3");
                 LOGGER.info("Executed Step = STORE,var_PolicyLoanAllowed3,var_CSVData,$.records[{var_Count}].PolicyLoanAllowed3,TGTYPESCREENREG");
        WAIT.$(5,"TGTYPESCREENREG");

        SWIPE.$("DOWN","ELE_TXTBOX_SEARCHTASKS","//input[@id='workbenchForm:workbenchTabs:taskSearchContainer:taskSearchSearch']","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_SEARCHTASKS","//input[@id='workbenchForm:workbenchTabs:taskSearchContainer:taskSearchSearch']","Plan Setup","FALSE","TGTYPESCREENREG");

        WAIT.$(3,"TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SEARCHTASKS","//a[@id='workbenchForm:workbenchTabs:taskSearchContainer:tb_buttonSearch']//span[@class='fa fa-search fa-lg']","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        TAP.$("ELE_LINK_FIRST_TASKS","//button[@id='workbenchForm:workbenchTabs:taskSearchContainer:0:availableTask']","TGTYPESCREENREG");

        WAIT.$("ELE_TXTBOX_FILTER_PLANCODE","//input[@id='workbenchForm:workbenchTabs:grid:planCode_Col:filter']","VISIBLE","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_FILTER_PLANCODE","//input[@id='workbenchForm:workbenchTabs:grid:planCode_Col:filter']",var_PlanCode3,"FALSE","TGTYPESCREENREG");

        WAIT.$(7,"TGTYPESCREENREG");

        TAP.$("ELE_LINK_FIRST_PLANCODE","//span[@id='workbenchForm:workbenchTabs:grid:0:planCode']","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        SWIPE.$("UP","ELE_LABEL_POLICYLOANSALLOWED","//span[@id='workbenchForm:workbenchTabs:policyLoansAllowed']","TGTYPESCREENREG");

        ASSERT.$("ELE_LABEL_POLICYLOANSALLOWED","//span[@id='workbenchForm:workbenchTabs:policyLoansAllowed']","CONTAINS",var_PolicyLoanAllowed3,"TGTYPESCREENREG");

        SWIPE.$("DOWN","TGTYPESCREENREG");

        SWIPE.$("DOWN","TGTYPESCREENREG");

        SWIPE.$("DOWN","TGTYPESCREENREG");

        SWIPE.$("DOWN","ELE_TAB_NONFORFEITURE","//span[contains(text(),'Nonforfeiture')]","TGTYPESCREENREG");

        SWIPE.$("DOWN","TGTYPESCREENREG");

        SWIPE.$("DOWN","TGTYPESCREENREG");

        SWIPE.$("DOWN","TGTYPESCREENREG");

        WAIT.$(3,"TGTYPESCREENREG");

        TAP.$("ELE_TAB_NONFORFEITURE","//span[contains(text(),'Nonforfeiture')]","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        SWIPE.$("UP","TGTYPESCREENREG");

        SWIPE.$("UP","TGTYPESCREENREG");

        ASSERT.$("ELE_LABEL_SURRENDERDIVIDENDSTOPAYPREMIUMS","//span[@id='workbenchForm:workbenchTabs:divSurrAlloForNFO']","CONTAINS","No","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        TAP.$("ELE_TAB_CLOSE","//li[@class='ui-state-default ui-tabs-selected ui-state-active ui-corner-top']//span[@class='ui-icon ui-icon-close']","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_FILTER_PLANCODE","//input[@id='workbenchForm:workbenchTabs:grid:planCode_Col:filter']",var_PlanCode2,"FALSE","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        TAP.$("ELE_LINK_FIRST_PLANCODE","//span[@id='workbenchForm:workbenchTabs:grid:0:planCode']","TGTYPESCREENREG");

        WAIT.$("ELE_BUTTON_EDIT","//button[@id='workbenchForm:workbenchTabs:tb_buttonUpdate']","VISIBLE","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_EDIT","//button[@id='workbenchForm:workbenchTabs:tb_buttonUpdate']","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

		var_dataToPass = var_PolicyLoanAllowed2;
                 LOGGER.info("Executed Step = STORE,var_dataToPass,var_PolicyLoanAllowed2,TGTYPESCREENREG");
        SWIPE.$("UP","ELE_DRPDWN_POLICY_LOAN_ALLOWED","//label[@id='workbenchForm:workbenchTabs:policyLoansAllowed_label']","TGTYPESCREENREG");

        TAP.$("ELE_DRPDWN_POLICY_LOAN_ALLOWED","//label[@id='workbenchForm:workbenchTabs:policyLoansAllowed_label']","TGTYPESCREENREG");

		try { 
		 //li[@id='workbenchForm:workbenchTabs:policyLoansAllowed_2']



			Thread.sleep(1000);
					for (int i = 0; i < 2000; i++){
						WebElement selectValueOfDropDown= driver.findElement(By.xpath("//li[@id='workbenchForm:workbenchTabs:policyLoansAllowed_"+ i +"']"));

						if (selectValueOfDropDown.getText().contains(var_dataToPass)){
		AddToReport("ASSERT," + selectValueOfDropDown.getText() +", CONTAINS,"+var_dataToPass);
		                                selectValueOfDropDown.click();
							
						
							break;
						}
					}
					takeScreenshot();

					Thread.sleep(2000);
		 
		 ;} 
		 catch(Exception e) { PrintVal("Renderer Script Exception : "+e.toString()); } 
		 PrintVal("Executed Step = CALLRENDERERSCRIPT,SELECT_POLICY_LOAN_ALLOWED"); 
        WAIT.$(5,"TGTYPESCREENREG");

        SWIPE.$("DOWN","TGTYPESCREENREG");

        SWIPE.$("DOWN","TGTYPESCREENREG");

        SWIPE.$("DOWN","TGTYPESCREENREG");

        SWIPE.$("DOWN","TGTYPESCREENREG");

        SWIPE.$("DOWN","TGTYPESCREENREG");

        TAP.$("ELE_TAB_NONFORFEITURE","//span[contains(text(),'Nonforfeiture')]","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

		var_dataToPass = var_SurrenderDividendstoPayPremiums2;
                 LOGGER.info("Executed Step = STORE,var_dataToPass,var_SurrenderDividendstoPayPremiums2,TGTYPESCREENREG");
        TAP.$("ELE_DRPDWN_SURRENDERDIVIDENDSTOPAYPREMIUMS","//label[@id='workbenchForm:workbenchTabs:divSurrAlloForNFO_label']","TGTYPESCREENREG");

		try { 
		 //li[@id='workbenchForm:workbenchTabs:divSurrAlloForNFO_3']


			Thread.sleep(1000);
					for (int i = 0; i < 2000; i++){
						WebElement selectValueOfDropDown= driver.findElement(By.xpath("//li[@id='workbenchForm:workbenchTabs:divSurrAlloForNFO_"+ i +"']"));

						if (selectValueOfDropDown.getText().contains(var_dataToPass)){
		AddToReport("ASSERT," + selectValueOfDropDown.getText() +", CONTAINS,"+var_dataToPass);
		                                selectValueOfDropDown.click();
							
						
							break;
						}
					}
					takeScreenshot();

					Thread.sleep(2000);
		 
		 ;} 
		 catch(Exception e) { PrintVal("Renderer Script Exception : "+e.toString()); } 
		 PrintVal("Executed Step = CALLRENDERERSCRIPT,SELECT_SURRENDERDIVIDENDSTOPAYPREMIUMS "); 
        WAIT.$(5,"TGTYPESCREENREG");

		var_dataToPass = var_DefaultOption2;
                 LOGGER.info("Executed Step = STORE,var_dataToPass,var_DefaultOption2,TGTYPESCREENREG");
        TAP.$("ELE_DRPDWN_NONFORFEITUREOPTION","//label[@id='workbenchForm:workbenchTabs:defaultNonForfOpt_label']","TGTYPESCREENREG");

		try { 
		 //li[@id='workbenchForm:workbenchTabs:defaultNonForfOpt_5']


			Thread.sleep(1000);
					for (int i = 0; i < 2000; i++){
						WebElement selectValueOfDropDown= driver.findElement(By.xpath("//li[@id='workbenchForm:workbenchTabs:defaultNonForfOpt_"+ i +"']"));

						if (selectValueOfDropDown.getText().contains(var_dataToPass)){
		AddToReport("ASSERT," + selectValueOfDropDown.getText() +", CONTAINS,"+var_dataToPass);
		                                selectValueOfDropDown.click();
							
						
							break;
						}
					}
					takeScreenshot();

					Thread.sleep(2000);
		 
		 ;} 
		 catch(Exception e) { PrintVal("Renderer Script Exception : "+e.toString()); } 
		 PrintVal("Executed Step = CALLRENDERERSCRIPT,SELECT_DEFAIULT_OPETION_NONFOR"); 
        WAIT.$(5,"TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SAVE","//span[contains(text(),'Save')]","TGTYPESCREENREG");

        WAIT.$(10,"TGTYPESCREENREG");

        SWIPE.$("DOWN","TGTYPESCREENREG");

        SWIPE.$("DOWN","TGTYPESCREENREG");

        TAP.$("ELE_TAB_NONFORFEITURE","//span[contains(text(),'Nonforfeiture')]","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        SWIPE.$("DOWN","TGTYPESCREENREG");

        SWIPE.$("DOWN","TGTYPESCREENREG");

        SWIPE.$("DOWN","ELE_MSG_FIRST_ERROR_SUMMARY","//body/div[@id='Outer40']/div[@id='ContentPanel']/div[@id='Content40']/form[@id='workbenchForm']/div[@id='PageTiles']/div[@id='workbenchForm:workbenchTabs']/div[1]/div[3]/div[1]/div[5]/div[2]/ul[1]/li[1]/span[1]","TGTYPESCREENREG");

        ASSERT.$("ELE_MSG_FIRST_ERROR_SUMMARY","//body/div[@id='Outer40']/div[@id='ContentPanel']/div[@id='Content40']/form[@id='workbenchForm']/div[@id='PageTiles']/div[@id='workbenchForm:workbenchTabs']/div[1]/div[3]/div[1]/div[5]/div[2]/ul[1]/li[1]/span[1]","CONTAINS","Policy loans not allowed therefore APL nonforfeiture option must be","TGTYPESCREENREG");

        ASSERT.$("ELE_MSG_SECOUNF_ERROR_SUMMARY","//body/div[@id='Outer40']/div[@id='ContentPanel']/div[@id='Content40']/form[@id='workbenchForm']/div[@id='PageTiles']/div[@id='workbenchForm:workbenchTabs']/div[1]/div[3]/div[1]/div[5]/div[2]/ul[1]/li[2]/span[1]","CONTAINS","Nonforfeiture option is invalid.","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_GIAS_LOGO","//img[@id='headerForm:headerLogoImage']","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

		var_CountN = var_CountN + ( 1 );
                 LOGGER.info("Executed Step = INCREASEVAR,var_CountN,1,TGTYPESCREENREG");
    	}
          LOGGER.info("Executed Step = WEND,TGTYPESCREENREG");
    	resetAttemps();

    }


    @Test
    public void nw048testplan() {

        // No need to declare / print method name here, handled by TestNg listener
        // declare("nw048testplan");

        CALL.$("LoginGIAS","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_USERNAME","//input[@id='guestForm:username']","autoapi","FALSE","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_PASSWORD","//input[@id='guestForm:password']","Autoapi!23","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_LOGIN","//button[contains(.,'Login')]","TGTYPESCREENREG");

        WAIT.$("ELE_BUTTON_GIAS_LOGO","//img[@id='headerForm:headerLogoImage']","VISIBLE","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

		String var_dataToPass;
                 LOGGER.info("Executed Step = VAR,String,var_dataToPass,TGTYPESCREENREG");
		String var_Option;
                 LOGGER.info("Executed Step = VAR,String,var_Option,TGTYPESCREENREG");
		int var_Count = 0;
                 LOGGER.info("Executed Step = VAR,Integer,var_Count,0,TGTYPESCREENREG");
		int var_CountN = 0;
                 LOGGER.info("Executed Step = VAR,Integer,var_CountN,0,TGTYPESCREENREG");
        LOGGER.info("Executed Step = REPEAT IF");
    	while (check(var_CountN,"<",1,"TGTYPESCREENREG")) {
          LOGGER.info("Executed Step = WHILE,var_CountN,<,1,TGTYPESCREENREG");
		String var_FileName = "http://10.1.104.229/json/20210524/5WThNo.json";
                 LOGGER.info("Executed Step = VAR,String,var_FileName,http://10.1.104.229/json/20210524/5WThNo.json,TGTYPESCREENREG");
		com.jayway.jsonpath.ReadContext var_CSVData = getJsonData("http://10.1.104.229/json/20210524/5WThNo.json");
                 LOGGER.info("Executed Step = VAR,Json,var_CSVData,http://10.1.104.229/json/20210524/5WThNo.json,TGTYPESCREENREG");
        CALL.$("SGD62","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_GIAS_LOGO","//img[@id='headerForm:headerLogoImage']","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        SWIPE.$("DOWN","ELE_TXTBOX_SEARCHTASKS","//input[@id='workbenchForm:workbenchTabs:taskSearchContainer:taskSearchSearch']","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_SEARCHTASKS","//input[@id='workbenchForm:workbenchTabs:taskSearchContainer:taskSearchSearch']","Plan Setup","FALSE","TGTYPESCREENREG");

        WAIT.$(3,"TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SEARCHTASKS","//a[@id='workbenchForm:workbenchTabs:taskSearchContainer:tb_buttonSearch']//span[@class='fa fa-search fa-lg']","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        TAP.$("ELE_LINK_FIRST_TASKS","//button[@id='workbenchForm:workbenchTabs:taskSearchContainer:0:availableTask']","TGTYPESCREENREG");

        WAIT.$("ELE_TXTBOX_FILTER_PLANCODE","//input[@id='workbenchForm:workbenchTabs:grid:planCode_Col:filter']","VISIBLE","TGTYPESCREENREG");

		var_dataToPass = getJsonData(var_CSVData , "$.records["+var_Count+"].Plan");
                 LOGGER.info("Executed Step = STORE,var_dataToPass,var_CSVData,$.records[{var_Count}].Plan,TGTYPESCREENREG");
        TYPE.$("ELE_TXTBOX_FILTER_PLANCODE","//input[@id='workbenchForm:workbenchTabs:grid:planCode_Col:filter']",var_dataToPass,"FALSE","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        TAP.$("ELE_LINK_FIRST_PLANCODE","//span[@id='workbenchForm:workbenchTabs:grid:0:planCode']","TGTYPESCREENREG");

        WAIT.$("ELE_BUTTON_EDIT","//button[@id='workbenchForm:workbenchTabs:tb_buttonUpdate']","VISIBLE","TGTYPESCREENREG");

        TAP.$("ELE_LINK_DIVIDENDS","//span[contains(.,'Dividends')]","TGTYPESCREENREG");

        WAIT.$(3,"TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_EDIT","//button[@id='workbenchForm:workbenchTabs:tb_buttonUpdate']","TGTYPESCREENREG");

        WAIT.$("ELE_BUTTON_SAVE","//span[contains(text(),'Save')]","VISIBLE","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        SWIPE.$("UP","ELE_DRPDWN_PURCHASE_FACEAMTLIMIT","//label[@id='workbenchForm:workbenchTabs:purchase1YearTermFAAllowed_label']","TGTYPESCREENREG");

        TAP.$("ELE_DRPDWN_PURCHASE_FACEAMTLIMIT","//label[@id='workbenchForm:workbenchTabs:purchase1YearTermFAAllowed_label']","TGTYPESCREENREG");

        WAIT.$("ELE_OPTION_NO","//li[@id='workbenchForm:workbenchTabs:purchase1YearTermFAAllowed_2']","VISIBLE","TGTYPESCREENREG");

        TAP.$("ELE_OPTION_NO","//li[@id='workbenchForm:workbenchTabs:purchase1YearTermFAAllowed_2']","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SAVE","//span[contains(text(),'Save')]","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_GIAS_LOGO","//img[@id='headerForm:headerLogoImage']","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        SWIPE.$("DOWN","ELE_TXTBOX_SEARCHTASKS","//input[@id='workbenchForm:workbenchTabs:taskSearchContainer:taskSearchSearch']","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_SEARCHTASKS","//input[@id='workbenchForm:workbenchTabs:taskSearchContainer:taskSearchSearch']","Policy Contract","FALSE","TGTYPESCREENREG");

        WAIT.$(3,"TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SEARCHTASKS","//a[@id='workbenchForm:workbenchTabs:taskSearchContainer:tb_buttonSearch']//span[@class='fa fa-search fa-lg']","TGTYPESCREENREG");

        WAIT.$("ELE_LINK_CHANGEPOLICYCONTRACT","//span[contains(text(),'Change a Policy Contract')]","VISIBLE","TGTYPESCREENREG");

        WAIT.$(3,"TGTYPESCREENREG");

        TAP.$("ELE_LINK_CHANGEPOLICYCONTRACT","//span[contains(text(),'Change a Policy Contract')]","TGTYPESCREENREG");

        WAIT.$("ELE_TXTBOX_POLICYNUMBER","//input[@id='workbenchForm:workbenchTabs:policyNumber']","VISIBLE","TGTYPESCREENREG");

		var_dataToPass = getJsonData(var_CSVData , "$.records["+var_Count+"].PolicyNumber");
                 LOGGER.info("Executed Step = STORE,var_dataToPass,var_CSVData,$.records[{var_Count}].PolicyNumber,TGTYPESCREENREG");
        TYPE.$("ELE_TXTBOX_POLICYNUMBER","//input[@id='workbenchForm:workbenchTabs:policyNumber']",var_dataToPass,"FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_CONTINUE","//span[contains(text(),'Continue')]","TGTYPESCREENREG");

        WAIT.$("ELE_BUTTON_SUBMIT","//span[contains(text(),'Submit')]","VISIBLE","TGTYPESCREENREG");

        WAIT.$(3,"TGTYPESCREENREG");

        SWIPE.$("UP","ELE_DRPDWN_POLICYCONTRACT_PRIMARYDIVIDENDPRIMIUM","//label[@id='workbenchForm:workbenchTabs:dividendOptionPrim_label']","TGTYPESCREENREG");

        WAIT.$(3,"TGTYPESCREENREG");

		var_dataToPass = getJsonData(var_CSVData , "$.records["+var_Count+"].PrimaryDividendOption");
                 LOGGER.info("Executed Step = STORE,var_dataToPass,var_CSVData,$.records[{var_Count}].PrimaryDividendOption,TGTYPESCREENREG");
        TAP.$("ELE_DRPDWN_POLICYCONTRACT_PRIMARYDIVIDENDPRIMIUM","//label[@id='workbenchForm:workbenchTabs:dividendOptionPrim_label']","TGTYPESCREENREG");

        WAIT.$(3,"TGTYPESCREENREG");

		try { 
		 //li[@id='workbenchForm:workbenchTabs:dividendOptionPrim_4']


			Thread.sleep(1000);
					for (int i = 0; i < 2000; i++){
						WebElement selectValueOfDropDown= driver.findElement(By.xpath("//li[@id='workbenchForm:workbenchTabs:dividendOptionPrim_"+ i +"']"));

						if (selectValueOfDropDown.getText().contains(var_dataToPass)){
		AddToReport("ASSERT," + selectValueOfDropDown.getText() +", CONTAINS,"+var_dataToPass);
		                                selectValueOfDropDown.click();
							
						
							break;
						}
					}
					takeScreenshot();

					Thread.sleep(2000);
		 
		 ;} 
		 catch(Exception e) { PrintVal("Renderer Script Exception : "+e.toString()); } 
		 PrintVal("Executed Step = CALLRENDERERSCRIPT,SELECT_PRIMARY_DIVIDEND_OPTION"); 
        WAIT.$(3,"TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SUBMIT","//span[contains(text(),'Submit')]","TGTYPESCREENREG");

        WAIT.$(3,"TGTYPESCREENREG");

        SWIPE.$("DOWN","ELE_MSG_ERROR_SUMMARY","//span[contains(@class,'ui-messages-error-summary')]","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        ASSERT.$("ELE_MSG_ERROR_SUMMARY","//span[contains(@class,'ui-messages-error-summary')]","VISIBLE","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        CALL.$("SGD73","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_GIAS_LOGO","//img[@id='headerForm:headerLogoImage']","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        SWIPE.$("DOWN","ELE_TXTBOX_SEARCHTASKS","//input[@id='workbenchForm:workbenchTabs:taskSearchContainer:taskSearchSearch']","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_SEARCHTASKS","//input[@id='workbenchForm:workbenchTabs:taskSearchContainer:taskSearchSearch']","Plan Setup","FALSE","TGTYPESCREENREG");

        WAIT.$(3,"TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SEARCHTASKS","//a[@id='workbenchForm:workbenchTabs:taskSearchContainer:tb_buttonSearch']//span[@class='fa fa-search fa-lg']","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        TAP.$("ELE_LINK_FIRST_TASKS","//button[@id='workbenchForm:workbenchTabs:taskSearchContainer:0:availableTask']","TGTYPESCREENREG");

        WAIT.$("ELE_TXTBOX_FILTER_PLANCODE","//input[@id='workbenchForm:workbenchTabs:grid:planCode_Col:filter']","VISIBLE","TGTYPESCREENREG");

		var_dataToPass = getJsonData(var_CSVData , "$.records["+var_Count+"].Plan");
                 LOGGER.info("Executed Step = STORE,var_dataToPass,var_CSVData,$.records[{var_Count}].Plan,TGTYPESCREENREG");
        TYPE.$("ELE_TXTBOX_FILTER_PLANCODE","//input[@id='workbenchForm:workbenchTabs:grid:planCode_Col:filter']",var_dataToPass,"FALSE","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        TAP.$("ELE_LINK_FIRST_PLANCODE","//span[@id='workbenchForm:workbenchTabs:grid:0:planCode']","TGTYPESCREENREG");

        WAIT.$("ELE_BUTTON_EDIT","//button[@id='workbenchForm:workbenchTabs:tb_buttonUpdate']","VISIBLE","TGTYPESCREENREG");

        TAP.$("ELE_LINK_DIVIDENDS","//span[contains(.,'Dividends')]","TGTYPESCREENREG");

        WAIT.$(3,"TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_EDIT","//button[@id='workbenchForm:workbenchTabs:tb_buttonUpdate']","TGTYPESCREENREG");

        WAIT.$("ELE_BUTTON_SAVE","//span[contains(text(),'Save')]","VISIBLE","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        SWIPE.$("UP","ELE_DRPDWN_DEFAULTOPTION1","//label[@id='workbenchForm:workbenchTabs:defaultOption1_label']","TGTYPESCREENREG");

        TAP.$("ELE_DRPDWN_DEFAULTOPTION1","//label[@id='workbenchForm:workbenchTabs:defaultOption1_label']","TGTYPESCREENREG");

        WAIT.$(3,"TGTYPESCREENREG");

		var_dataToPass = getJsonData(var_CSVData , "$.records["+var_Count+"].DefaultOption");
                 LOGGER.info("Executed Step = STORE,var_dataToPass,var_CSVData,$.records[{var_Count}].DefaultOption,TGTYPESCREENREG");
		var_Option = getJsonData(var_CSVData , "$.records["+var_Count+"].PrimaryDividendOption");
                 LOGGER.info("Executed Step = STORE,var_Option,var_CSVData,$.records[{var_Count}].PrimaryDividendOption,TGTYPESCREENREG");
		try { 
		 //li[@id='workbenchForm:workbenchTabs:defaultOption1_3']
		//li[@id='workbenchForm:workbenchTabs:defaultOption1_4']			

		boolean opt1=false;
		boolean opt2=false;
		Thread.sleep(1000);
					for (int i = 0; i < 2000; i++){
						WebElement selectValueOfDropDown = driver.findElement(By.xpath("//li[@id='workbenchForm:workbenchTabs:defaultOption1_"+ i +"']"));
		                         System.out.println("Execution time: "  + selectValueOfDropDown.getText());                
						if (selectValueOfDropDown.getText().contains(var_dataToPass)){

		                                
		                                      AddToReport("ASSERT," + selectValueOfDropDown.getText() +", CONTAINS,"+var_dataToPass);
				                        opt1 = true;
							//break;
						}

		                              if (selectValueOfDropDown.getText().contains(var_Option)){

		                                
		                                    AddToReport("ASSERT," + selectValueOfDropDown.getText() +", CONTAINS,"+var_Option);
				                        		                        opt2 = true;
							//break;
						}
		                             if(opt1 && opt2){
		                                      break;
		                                }
					}
					takeScreenshot();

					Thread.sleep(2000);
		 
		 ;} 
		 catch(Exception e) { PrintVal("Renderer Script Exception : "+e.toString()); } 
		 PrintVal("Executed Step = CALLRENDERERSCRIPT,VERIFY_DEFAULT_OPTION1"); 
        WAIT.$(5,"TGTYPESCREENREG");

        SWIPE.$("DOWN","ELE_LINK_COUPONS","//span[@class='ui-menuitem-text'][contains(text(),'Coupons')]","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        SWIPE.$("DOWN","ELE_LINK_COUPONS","//span[@class='ui-menuitem-text'][contains(text(),'Coupons')]","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        SWIPE.$("DOWN","ELE_LINK_COUPONS","//span[@class='ui-menuitem-text'][contains(text(),'Coupons')]","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        TAP.$("ELE_LINK_COUPONS","//span[@class='ui-menuitem-text'][contains(text(),'Coupons')]","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        SWIPE.$("UP","ELE_DRPDWN_DEFAULTOPTION1","//label[@id='workbenchForm:workbenchTabs:defaultOption1_label']","TGTYPESCREENREG");

        WAIT.$(3,"TGTYPESCREENREG");

        TAP.$("ELE_DRPDWN_DEFAULTOPTION1","//label[@id='workbenchForm:workbenchTabs:defaultOption1_label']","TGTYPESCREENREG");

        WAIT.$(3,"TGTYPESCREENREG");

		try { 
		 //li[@id='workbenchForm:workbenchTabs:defaultOption1_3']
		//li[@id='workbenchForm:workbenchTabs:defaultOption1_4']			

		boolean opt1=false;
		boolean opt2=false;
		Thread.sleep(1000);
					for (int i = 0; i < 2000; i++){
						WebElement selectValueOfDropDown = driver.findElement(By.xpath("//li[@id='workbenchForm:workbenchTabs:defaultOption1_"+ i +"']"));
		                         System.out.println("Execution time: "  + selectValueOfDropDown.getText());                
						if (selectValueOfDropDown.getText().contains(var_dataToPass)){

		                                
		                                      AddToReport("ASSERT," + selectValueOfDropDown.getText() +", CONTAINS,"+var_dataToPass);
				                        opt1 = true;
							//break;
						}

		                              if (selectValueOfDropDown.getText().contains(var_Option)){

		                                
		                                    AddToReport("ASSERT," + selectValueOfDropDown.getText() +", CONTAINS,"+var_Option);
				                        		                        opt2 = true;
							//break;
						}
		                             if(opt1 && opt2){
		                                      break;
		                                }
					}
					takeScreenshot();

					Thread.sleep(2000);
		 
		 ;} 
		 catch(Exception e) { PrintVal("Renderer Script Exception : "+e.toString()); } 
		 PrintVal("Executed Step = CALLRENDERERSCRIPT,VERIFY_DEFAULT_OPTION1"); 
        WAIT.$(5,"TGTYPESCREENREG");

        CALL.$("SGD48","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_GIAS_LOGO","//img[@id='headerForm:headerLogoImage']","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        SWIPE.$("DOWN","ELE_TXTBOX_SEARCHTASKS","//input[@id='workbenchForm:workbenchTabs:taskSearchContainer:taskSearchSearch']","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_SEARCHTASKS","//input[@id='workbenchForm:workbenchTabs:taskSearchContainer:taskSearchSearch']","Plan Setup","FALSE","TGTYPESCREENREG");

        WAIT.$(3,"TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SEARCHTASKS","//a[@id='workbenchForm:workbenchTabs:taskSearchContainer:tb_buttonSearch']//span[@class='fa fa-search fa-lg']","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        TAP.$("ELE_LINK_FIRST_TASKS","//button[@id='workbenchForm:workbenchTabs:taskSearchContainer:0:availableTask']","TGTYPESCREENREG");

        WAIT.$("ELE_TXTBOX_FILTER_PLANCODE","//input[@id='workbenchForm:workbenchTabs:grid:planCode_Col:filter']","VISIBLE","TGTYPESCREENREG");

		var_dataToPass = getJsonData(var_CSVData , "$.records["+var_Count+"].Plan1");
                 LOGGER.info("Executed Step = STORE,var_dataToPass,var_CSVData,$.records[{var_Count}].Plan1,TGTYPESCREENREG");
        TYPE.$("ELE_TXTBOX_FILTER_PLANCODE","//input[@id='workbenchForm:workbenchTabs:grid:planCode_Col:filter']",var_dataToPass,"FALSE","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        TAP.$("ELE_LINK_FIRST_PLANCODE","//span[@id='workbenchForm:workbenchTabs:grid:0:planCode']","TGTYPESCREENREG");

        WAIT.$("ELE_BUTTON_EDIT","//button[@id='workbenchForm:workbenchTabs:tb_buttonUpdate']","VISIBLE","TGTYPESCREENREG");

        TAP.$("ELE_LINK_DIVIDENDS","//span[contains(.,'Dividends')]","TGTYPESCREENREG");

        WAIT.$(3,"TGTYPESCREENREG");

        SWIPE.$("UP","TGTYPESCREENREG");

        WAIT.$(3,"TGTYPESCREENREG");

        SWIPE.$("UP","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        ASSERT.$("ELE_GETTEXT_LABEL_POYFAL","//body[1]/div[1]/div[2]/div[1]/form[1]/div[1]/div[2]/div[1]/div[3]/div[1]/table[1]/tbody[1]/tr[1]/td[1]/table[1]/tbody[1]/tr[2]/td[1]/fieldset[1]/div[1]/table[1]/tbody[1]/tr[13]/td[1]/label[1]","VISIBLE","TGTYPESCREENREG");

		var_dataToPass = getJsonData(var_CSVData , "$.records["+var_Count+"].PrimaryDividendOption");
                 LOGGER.info("Executed Step = STORE,var_dataToPass,var_CSVData,$.records[{var_Count}].PrimaryDividendOption,TGTYPESCREENREG");
		String var_OptionNo;
                 LOGGER.info("Executed Step = VAR,String,var_OptionNo,TGTYPESCREENREG");
		var_OptionNo = getJsonData(var_CSVData , "$.records["+var_Count+"].OptionNo");
                 LOGGER.info("Executed Step = STORE,var_OptionNo,var_CSVData,$.records[{var_Count}].OptionNo,TGTYPESCREENREG");
		String var_OptionYes;
                 LOGGER.info("Executed Step = VAR,String,var_OptionYes,TGTYPESCREENREG");
		var_OptionYes = getJsonData(var_CSVData , "$.records["+var_Count+"].OptionYes");
                 LOGGER.info("Executed Step = STORE,var_OptionYes,var_CSVData,$.records[{var_Count}].OptionYes,TGTYPESCREENREG");
		try { 
		 //span[@id='workbenchForm:workbenchTabs:purchase1YearTermFAAllowed']

		//workbenchForm:workbenchTabs:purchase1YearTermFAAllowed_lbl


			Thread.sleep(1000);

					/*	WebElement POYTFaceAmtLimit= driver.findElement(By.xpath("//span[@id='workbenchForm:workbenchTabs:purchase1YearTermFAAllowed_lbl']"));
		                         System.out.println("Execution time: "  + POYTFaceAmtLimit.getText());   
		LOGGER.info("Total Row count : "+ POYTFaceAmtLimit.getText());
		LOGGER.info("Total Row count : "+ var_dataToPass);
						if (POYTFaceAmtLimit.getText().contains(var_dataToPass)){

		                            //    selectValueOfDropDown.click();
							AddToReport("ASSERT," + POYTFaceAmtLimit.getText() +", CONTAINS,"+var_dataToPass);
						
							//break;
						}*/

		                                  WebElement POYTFaceAmtLimitYESNO= 
		                                     driver.findElement(By.xpath("//span[@id='workbenchForm:workbenchTabs:purchase1YearTermFAAllowed']"));

		LOGGER.info("Total Row count : "+ POYTFaceAmtLimitYESNO.getText());
		LOGGER.info("Total Row count : "+ var_OptionNo + " "+ var_OptionYes);

		                               if (POYTFaceAmtLimitYESNO.getText().contains(var_OptionNo)){

		                              //  selectValueOfDropDown.click();
							AddToReport("ASSERT," + POYTFaceAmtLimitYESNO.getText() +", CONTAINS,"+var_OptionNo);
						
							//break;
						}
		                            if (POYTFaceAmtLimitYESNO.getText().contains(var_OptionYes)){

		                              //  selectValueOfDropDown.click();
							AddToReport("ASSERT," + POYTFaceAmtLimitYESNO.getText() +", CONTAINS,"+var_OptionYes);
						
							//break;
						}
					
					takeScreenshot();

					Thread.sleep(2000);
		 
		 ;} 
		 catch(Exception e) { PrintVal("Renderer Script Exception : "+e.toString()); } 
		 PrintVal("Executed Step = CALLRENDERERSCRIPT,VERIFY_POYTFACEAMTLIMIT"); 
        WAIT.$(5,"TGTYPESCREENREG");

        SWIPE.$("DOWN","TGTYPESCREENREG");

        WAIT.$(3,"TGTYPESCREENREG");

        SWIPE.$("DOWN","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

        SWIPE.$("DOWN","TGTYPESCREENREG");

        TAP.$("ELE_LINK_COUPONS","//span[@class='ui-menuitem-text'][contains(text(),'Coupons')]","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        SWIPE.$("DOWN","TGTYPESCREENREG");

        WAIT.$(3,"TGTYPESCREENREG");

        SWIPE.$("DOWN","TGTYPESCREENREG");

        WAIT.$(3,"TGTYPESCREENREG");

        SWIPE.$("DOWN","TGTYPESCREENREG");

        WAIT.$(3,"TGTYPESCREENREG");

        ASSERT.$("ELE_GETTEXT_LABEL_POYFAL","//body[1]/div[1]/div[2]/div[1]/form[1]/div[1]/div[2]/div[1]/div[3]/div[1]/table[1]/tbody[1]/tr[1]/td[1]/table[1]/tbody[1]/tr[2]/td[1]/fieldset[1]/div[1]/table[1]/tbody[1]/tr[13]/td[1]/label[1]","VISIBLE","TGTYPESCREENREG");

        WAIT.$(3,"TGTYPESCREENREG");

		try { 
		 //span[@id='workbenchForm:workbenchTabs:purchase1YearTermFAAllowed']

		//workbenchForm:workbenchTabs:purchase1YearTermFAAllowed_lbl


			Thread.sleep(1000);

					/*	WebElement POYTFaceAmtLimit= driver.findElement(By.xpath("//span[@id='workbenchForm:workbenchTabs:purchase1YearTermFAAllowed_lbl']"));
		                         System.out.println("Execution time: "  + POYTFaceAmtLimit.getText());   
		LOGGER.info("Total Row count : "+ POYTFaceAmtLimit.getText());
		LOGGER.info("Total Row count : "+ var_dataToPass);
						if (POYTFaceAmtLimit.getText().contains(var_dataToPass)){

		                            //    selectValueOfDropDown.click();
							AddToReport("ASSERT," + POYTFaceAmtLimit.getText() +", CONTAINS,"+var_dataToPass);
						
							//break;
						}*/

		                                  WebElement POYTFaceAmtLimitYESNO= 
		                                     driver.findElement(By.xpath("//span[@id='workbenchForm:workbenchTabs:purchase1YearTermFAAllowed']"));

		LOGGER.info("Total Row count : "+ POYTFaceAmtLimitYESNO.getText());
		LOGGER.info("Total Row count : "+ var_OptionNo + " "+ var_OptionYes);

		                               if (POYTFaceAmtLimitYESNO.getText().contains(var_OptionNo)){

		                              //  selectValueOfDropDown.click();
							AddToReport("ASSERT," + POYTFaceAmtLimitYESNO.getText() +", CONTAINS,"+var_OptionNo);
						
							//break;
						}
		                            if (POYTFaceAmtLimitYESNO.getText().contains(var_OptionYes)){

		                              //  selectValueOfDropDown.click();
							AddToReport("ASSERT," + POYTFaceAmtLimitYESNO.getText() +", CONTAINS,"+var_OptionYes);
						
							//break;
						}
					
					takeScreenshot();

					Thread.sleep(2000);
		 
		 ;} 
		 catch(Exception e) { PrintVal("Renderer Script Exception : "+e.toString()); } 
		 PrintVal("Executed Step = CALLRENDERERSCRIPT,VERIFY_POYTFACEAMTLIMIT"); 
        WAIT.$(5,"TGTYPESCREENREG");

        CALL.$("SGD49","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_GIAS_LOGO","//img[@id='headerForm:headerLogoImage']","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        SWIPE.$("DOWN","ELE_TXTBOX_SEARCHTASKS","//input[@id='workbenchForm:workbenchTabs:taskSearchContainer:taskSearchSearch']","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_SEARCHTASKS","//input[@id='workbenchForm:workbenchTabs:taskSearchContainer:taskSearchSearch']","Plan Setup","FALSE","TGTYPESCREENREG");

        WAIT.$(3,"TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SEARCHTASKS","//a[@id='workbenchForm:workbenchTabs:taskSearchContainer:tb_buttonSearch']//span[@class='fa fa-search fa-lg']","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        TAP.$("ELE_LINK_FIRST_TASKS","//button[@id='workbenchForm:workbenchTabs:taskSearchContainer:0:availableTask']","TGTYPESCREENREG");

        WAIT.$("ELE_TXTBOX_FILTER_PLANCODE","//input[@id='workbenchForm:workbenchTabs:grid:planCode_Col:filter']","VISIBLE","TGTYPESCREENREG");

		var_dataToPass = getJsonData(var_CSVData , "$.records["+var_Count+"].Plan2");
                 LOGGER.info("Executed Step = STORE,var_dataToPass,var_CSVData,$.records[{var_Count}].Plan2,TGTYPESCREENREG");
        TYPE.$("ELE_TXTBOX_FILTER_PLANCODE","//input[@id='workbenchForm:workbenchTabs:grid:planCode_Col:filter']",var_dataToPass,"FALSE","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        TAP.$("ELE_LINK_FIRST_PLANCODE","//span[@id='workbenchForm:workbenchTabs:grid:0:planCode']","TGTYPESCREENREG");

        WAIT.$("ELE_BUTTON_EDIT","//button[@id='workbenchForm:workbenchTabs:tb_buttonUpdate']","VISIBLE","TGTYPESCREENREG");

        TAP.$("ELE_LINK_DIVIDENDS","//span[contains(.,'Dividends')]","TGTYPESCREENREG");

        WAIT.$(3,"TGTYPESCREENREG");

        SWIPE.$("UP","TGTYPESCREENREG");

        WAIT.$(3,"TGTYPESCREENREG");

        SWIPE.$("UP","TGTYPESCREENREG");

		var_dataToPass = getJsonData(var_CSVData , "$.records["+var_Count+"].DefaultOption");
                 LOGGER.info("Executed Step = STORE,var_dataToPass,var_CSVData,$.records[{var_Count}].DefaultOption,TGTYPESCREENREG");
        WAIT.$(3,"TGTYPESCREENREG");

		try { 
		 //label[@id='workbenchForm:workbenchTabs:purchase1YearTermAllowed_lbl']

			Thread.sleep(1000);

						WebElement POYTCashAmtLimit= driver.findElement(By.xpath("//label[@id='workbenchForm:workbenchTabs:purchase1YearTermAllowed_lbl']"));

						if (POYTCashAmtLimit.getText().contains(var_dataToPass)){

		                            //    selectValueOfDropDown.click();
							AddToReport("ASSERT," + POYTCashAmtLimit.getText() +", CONTAINS,"+var_dataToPass);
						
							//break;
						}			
					takeScreenshot();

					Thread.sleep(2000);
		 
		 ;} 
		 catch(Exception e) { PrintVal("Renderer Script Exception : "+e.toString()); } 
		 PrintVal("Executed Step = CALLRENDERERSCRIPT,VERIFY_TEXT_CASHVALUELIMIT"); 
        WAIT.$(5,"TGTYPESCREENREG");

        SWIPE.$("DOWN","TGTYPESCREENREG");

        WAIT.$(3,"TGTYPESCREENREG");

        SWIPE.$("DOWN","TGTYPESCREENREG");

        WAIT.$(3,"TGTYPESCREENREG");

        SWIPE.$("DOWN","TGTYPESCREENREG");

        WAIT.$(3,"TGTYPESCREENREG");

        TAP.$("ELE_LINK_COUPONS","//span[@class='ui-menuitem-text'][contains(text(),'Coupons')]","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        SWIPE.$("UP","TGTYPESCREENREG");

        WAIT.$(3,"TGTYPESCREENREG");

        SWIPE.$("UP","TGTYPESCREENREG");

        WAIT.$(3,"TGTYPESCREENREG");

		try { 
		 //label[@id='workbenchForm:workbenchTabs:purchase1YearTermAllowed_lbl']

			Thread.sleep(1000);

						WebElement POYTCashAmtLimit= driver.findElement(By.xpath("//label[@id='workbenchForm:workbenchTabs:purchase1YearTermAllowed_lbl']"));

						if (POYTCashAmtLimit.getText().contains(var_dataToPass)){

		                            //    selectValueOfDropDown.click();
							AddToReport("ASSERT," + POYTCashAmtLimit.getText() +", CONTAINS,"+var_dataToPass);
						
							//break;
						}			
					takeScreenshot();

					Thread.sleep(2000);
		 
		 ;} 
		 catch(Exception e) { PrintVal("Renderer Script Exception : "+e.toString()); } 
		 PrintVal("Executed Step = CALLRENDERERSCRIPT,VERIFY_TEXT_CASHVALUELIMIT"); 
        WAIT.$(5,"TGTYPESCREENREG");

        CALL.$("SGD50","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_GIAS_LOGO","//img[@id='headerForm:headerLogoImage']","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        SWIPE.$("DOWN","ELE_TXTBOX_SEARCHTASKS","//input[@id='workbenchForm:workbenchTabs:taskSearchContainer:taskSearchSearch']","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_SEARCHTASKS","//input[@id='workbenchForm:workbenchTabs:taskSearchContainer:taskSearchSearch']","Policy Contract","FALSE","TGTYPESCREENREG");

        WAIT.$(3,"TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SEARCHTASKS","//a[@id='workbenchForm:workbenchTabs:taskSearchContainer:tb_buttonSearch']//span[@class='fa fa-search fa-lg']","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        TAP.$("ELE_LINK_POLICYCONTRACT","//body/div[@id='Outer40']/div[@id='ContentPanel']/div[@id='Content40']/form[@id='workbenchForm']/div[@id='PageTiles']/div[@id='workbenchForm:workbenchTabs']/div[1]/div[1]/div[1]/div[2]/div[2]/div[1]/div[1]/div[1]/div[3]/div[1]/div[2]/button[1]/span[2]","TGTYPESCREENREG");

        WAIT.$("ELE_TXTBOX_POLICYNUMBER","//input[@id='workbenchForm:workbenchTabs:policyNumber']","TGTYPESCREENREG");

		var_dataToPass = getJsonData(var_CSVData , "$.records["+var_Count+"].PolicyNumber1");
                 LOGGER.info("Executed Step = STORE,var_dataToPass,var_CSVData,$.records[{var_Count}].PolicyNumber1,TGTYPESCREENREG");
        TYPE.$("ELE_TXTBOX_POLICYNUMBER","//input[@id='workbenchForm:workbenchTabs:policyNumber']",var_dataToPass,"FALSE","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_CONTINUE","//span[contains(text(),'Continue')]","TGTYPESCREENREG");

        WAIT.$("ELE_BUTTON_EDIT","//button[@id='workbenchForm:workbenchTabs:tb_buttonUpdate']","VISIBLE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_MORE","//body[1]/div[1]/div[2]/div[1]/form[1]/div[1]/div[2]/div[1]/div[3]/div[1]/div[1]/div[2]/button[2]/span[1]","TGTYPESCREENREG");

        WAIT.$("ELE_LINK_BENEFITS","//span[contains(text(),'Benefits')]","VISIBLE","TGTYPESCREENREG");

        WAIT.$(3,"TGTYPESCREENREG");

        TAP.$("ELE_LINK_BENEFITS","//span[contains(text(),'Benefits')]","TGTYPESCREENREG");

        WAIT.$("ELE_LABEL_POLICYNUMBER","//span[@id='workbenchForm:workbenchTabs:policyNumber']","VISIBLE","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        TAP.$("ELE_LINK_FIRST_PLANCODE","//span[@id='workbenchForm:workbenchTabs:grid:0:planCode']","TGTYPESCREENREG");

        WAIT.$("ELE_BUTTON_EDIT","//button[@id='workbenchForm:workbenchTabs:tb_buttonUpdate']","VISIBLE","TGTYPESCREENREG");

        WAIT.$(3,"TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_EDIT","//button[@id='workbenchForm:workbenchTabs:tb_buttonUpdate']","TGTYPESCREENREG");

        WAIT.$("ELE_BUTTON_SAVE","//span[contains(text(),'Save')]","VISIBLE","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        SWIPE.$("UP","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        TAP.$("ELE_DRPDWN_PRIMARY_DIVIDEND","//label[@id='workbenchForm:workbenchTabs:primaryDividendOption_label']","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

		var_dataToPass = getJsonData(var_CSVData , "$.records["+var_Count+"].DefaultOption");
                 LOGGER.info("Executed Step = STORE,var_dataToPass,var_CSVData,$.records[{var_Count}].DefaultOption,TGTYPESCREENREG");
		var_Option = getJsonData(var_CSVData , "$.records["+var_Count+"].PrimaryDividendOption");
                 LOGGER.info("Executed Step = STORE,var_Option,var_CSVData,$.records[{var_Count}].PrimaryDividendOption,TGTYPESCREENREG");
		try { 
		 //workbenchForm:workbenchTabs:primaryDividendOption_0
				

		boolean opt1=false;
		boolean opt2=false;
		Thread.sleep(1000);
					for (int i = 0; i < 2000; i++){
						WebElement selectValueOfDropDown = driver.findElement(By.xpath("//li[@id='workbenchForm:workbenchTabs:primaryDividendOption_"+ i +"']"));
		                         System.out.println("Execution time: "  + selectValueOfDropDown.getText());                
						if (selectValueOfDropDown.getText().contains(var_dataToPass)){

		                                
		                                      AddToReport("ASSERT," + selectValueOfDropDown.getText() +", CONTAINS,"+var_dataToPass);
				                        opt1 = true;
							//break;
						}

		                              if (selectValueOfDropDown.getText().contains(var_Option)){

		                                
		                                    AddToReport("ASSERT," + selectValueOfDropDown.getText() +", CONTAINS,"+var_Option);
				                        		                        opt2 = true;
							//break;
						}
		                             if(opt1 && opt2){
		                                      break;
		                                }
					}
					takeScreenshot();

					Thread.sleep(2000);
		 
		 ;} 
		 catch(Exception e) { PrintVal("Renderer Script Exception : "+e.toString()); } 
		 PrintVal("Executed Step = CALLRENDERERSCRIPT,VERIFY_PRIMARY_DIVIDEND_OPTION"); 
        WAIT.$(5,"TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_GIAS_LOGO","//img[@id='headerForm:headerLogoImage']","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        SWIPE.$("DOWN","ELE_TXTBOX_SEARCHTASKS","//input[@id='workbenchForm:workbenchTabs:taskSearchContainer:taskSearchSearch']","TGTYPESCREENREG");

        WAIT.$(3,"TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_SEARCHTASKS","//input[@id='workbenchForm:workbenchTabs:taskSearchContainer:taskSearchSearch']","Change a policy contract","FALSE","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SEARCHTASKS","//a[@id='workbenchForm:workbenchTabs:taskSearchContainer:tb_buttonSearch']//span[@class='fa fa-search fa-lg']","TGTYPESCREENREG");

        WAIT.$("ELE_LINK_FIRST_TASKS","//button[@id='workbenchForm:workbenchTabs:taskSearchContainer:0:availableTask']","VISIBLE","TGTYPESCREENREG");

        WAIT.$(3,"TGTYPESCREENREG");

        TAP.$("ELE_LINK_FIRST_TASKS","//button[@id='workbenchForm:workbenchTabs:taskSearchContainer:0:availableTask']","TGTYPESCREENREG");

        WAIT.$("ELE_TXTBOX_POLICYNUMBER","//input[@id='workbenchForm:workbenchTabs:policyNumber']","VISIBLE","TGTYPESCREENREG");

        WAIT.$(3,"TGTYPESCREENREG");

		var_dataToPass = getJsonData(var_CSVData , "$.records["+var_Count+"].PolicyNumber");
                 LOGGER.info("Executed Step = STORE,var_dataToPass,var_CSVData,$.records[{var_Count}].PolicyNumber,TGTYPESCREENREG");
        TYPE.$("ELE_TXTBOX_POLICYNUMBER","//input[@id='workbenchForm:workbenchTabs:policyNumber']",var_dataToPass,"FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_CONTINUE","//span[contains(text(),'Continue')]","TGTYPESCREENREG");

        WAIT.$("ELE_BUTTON_SUBMIT","//span[contains(text(),'Submit')]","VISIBLE","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        SWIPE.$("UP","TGTYPESCREENREG");

        WAIT.$(3,"TGTYPESCREENREG");

        SWIPE.$("UP","TGTYPESCREENREG");

        WAIT.$(3,"TGTYPESCREENREG");

        TAP.$("ELE_DRPDWN_PRIMARYDIVIDENDOPTION","//*[@id=\"workbenchForm:workbenchTabs:dividendOptionPrim_label\"]","TGTYPESCREENREG");

		var_dataToPass = getJsonData(var_CSVData , "$.records["+var_Count+"].DefaultOption");
                 LOGGER.info("Executed Step = STORE,var_dataToPass,var_CSVData,$.records[{var_Count}].DefaultOption,TGTYPESCREENREG");
		var_Option = getJsonData(var_CSVData , "$.records["+var_Count+"].PrimaryDividendOption");
                 LOGGER.info("Executed Step = STORE,var_Option,var_CSVData,$.records[{var_Count}].PrimaryDividendOption,TGTYPESCREENREG");
		try { 
		 //li[@id='workbenchForm:workbenchTabs:dividendOptionPrim_4']

		boolean firstrecord = false;
		boolean secrecord = false;
			Thread.sleep(1000);
					for (int i = 0; i < 2000; i++){
						WebElement selectValueOfDropDown= driver.findElement(By.xpath("//li[@id='workbenchForm:workbenchTabs:dividendOptionPrim_"+ i +"']"));

						if (selectValueOfDropDown.getText().contains(var_dataToPass)){
		AddToReport("ASSERT," + selectValueOfDropDown.getText() +", CONTAINS,"+var_dataToPass);
		                               // selectValueOfDropDown.click();
							firstrecord =true;
						
							//break;
						}
		                              if (selectValueOfDropDown.getText().contains(var_dataToPass)){
		                                     AddToReport("ASSERT," + selectValueOfDropDown.getText() +", CONTAINS,"+var_dataToPass);
		                               // selectValueOfDropDown.click();
							
						secrecord =true;
							//break;
						}

		                           if(secrecord   && firstrecord  ){
		                                       break;
		                            }
					}
					takeScreenshot();

					Thread.sleep(2000);
		 
		 ;} 
		 catch(Exception e) { PrintVal("Renderer Script Exception : "+e.toString()); } 
		 PrintVal("Executed Step = CALLRENDERERSCRIPT,VERIFY_PRIMARY_DIVIDEND_OPTION_PRIME"); 
        WAIT.$(5,"TGTYPESCREENREG");

        CALL.$("SGD64","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_GIAS_LOGO","//img[@id='headerForm:headerLogoImage']","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        SWIPE.$("DOWN","ELE_TXTBOX_SEARCHTASKS","//input[@id='workbenchForm:workbenchTabs:taskSearchContainer:taskSearchSearch']","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_SEARCHTASKS","//input[@id='workbenchForm:workbenchTabs:taskSearchContainer:taskSearchSearch']","Plan Setup","FALSE","TGTYPESCREENREG");

        WAIT.$(3,"TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SEARCHTASKS","//a[@id='workbenchForm:workbenchTabs:taskSearchContainer:tb_buttonSearch']//span[@class='fa fa-search fa-lg']","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        TAP.$("ELE_LINK_FIRST_TASKS","//button[@id='workbenchForm:workbenchTabs:taskSearchContainer:0:availableTask']","TGTYPESCREENREG");

        WAIT.$("ELE_TXTBOX_FILTER_PLANCODE","//input[@id='workbenchForm:workbenchTabs:grid:planCode_Col:filter']","VISIBLE","TGTYPESCREENREG");

		var_dataToPass = getJsonData(var_CSVData , "$.records["+var_Count+"].Plan");
                 LOGGER.info("Executed Step = STORE,var_dataToPass,var_CSVData,$.records[{var_Count}].Plan,TGTYPESCREENREG");
        TYPE.$("ELE_TXTBOX_FILTER_PLANCODE","//input[@id='workbenchForm:workbenchTabs:grid:planCode_Col:filter']",var_dataToPass,"FALSE","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        TAP.$("ELE_LINK_FIRST_PLANCODE","//span[@id='workbenchForm:workbenchTabs:grid:0:planCode']","TGTYPESCREENREG");

        WAIT.$("ELE_BUTTON_EDIT","//button[@id='workbenchForm:workbenchTabs:tb_buttonUpdate']","VISIBLE","TGTYPESCREENREG");

        TAP.$("ELE_LINK_DIVIDENDS","//span[contains(.,'Dividends')]","TGTYPESCREENREG");

        WAIT.$(3,"TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_EDIT","//button[@id='workbenchForm:workbenchTabs:tb_buttonUpdate']","TGTYPESCREENREG");

        WAIT.$("ELE_BUTTON_SAVE","//span[contains(text(),'Save')]","VISIBLE","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        SWIPE.$("UP","ELE_DRPDWN_PURCHASE_FACEAMTLIMIT","//label[@id='workbenchForm:workbenchTabs:purchase1YearTermFAAllowed_label']","TGTYPESCREENREG");

        TAP.$("ELE_DRPDWN_PURCHASE_FACEAMTLIMIT","//label[@id='workbenchForm:workbenchTabs:purchase1YearTermFAAllowed_label']","TGTYPESCREENREG");

        WAIT.$("ELE_OPTION_NO","//li[@id='workbenchForm:workbenchTabs:purchase1YearTermFAAllowed_2']","VISIBLE","TGTYPESCREENREG");

        TAP.$("ELE_OPTION_NO","//li[@id='workbenchForm:workbenchTabs:purchase1YearTermFAAllowed_2']","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SAVE","//span[contains(text(),'Save')]","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_GIAS_LOGO","//img[@id='headerForm:headerLogoImage']","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        SWIPE.$("DOWN","ELE_TXTBOX_SEARCHTASKS","//input[@id='workbenchForm:workbenchTabs:taskSearchContainer:taskSearchSearch']","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_SEARCHTASKS","//input[@id='workbenchForm:workbenchTabs:taskSearchContainer:taskSearchSearch']","Policy Contract","FALSE","TGTYPESCREENREG");

        WAIT.$(3,"TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SEARCHTASKS","//a[@id='workbenchForm:workbenchTabs:taskSearchContainer:tb_buttonSearch']//span[@class='fa fa-search fa-lg']","TGTYPESCREENREG");

        WAIT.$("ELE_LINK_FIRST_TASKS","//button[@id='workbenchForm:workbenchTabs:taskSearchContainer:0:availableTask']","TGTYPESCREENREG");

        WAIT.$(3,"TGTYPESCREENREG");

        TAP.$("ELE_LINK_FIRST_TASKS","//button[@id='workbenchForm:workbenchTabs:taskSearchContainer:0:availableTask']","TGTYPESCREENREG");

        WAIT.$("ELE_TXTBOX_POLICYNUMBER","//input[@id='workbenchForm:workbenchTabs:policyNumber']","VISIBLE","TGTYPESCREENREG");

		var_dataToPass = getJsonData(var_CSVData , "$.records["+var_Count+"].PolicyNumber");
                 LOGGER.info("Executed Step = STORE,var_dataToPass,var_CSVData,$.records[{var_Count}].PolicyNumber,TGTYPESCREENREG");
        TYPE.$("ELE_TXTBOX_POLICYNUMBER","//input[@id='workbenchForm:workbenchTabs:policyNumber']",var_dataToPass,"FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_CONTINUE","//span[contains(text(),'Continue')]","TGTYPESCREENREG");

        WAIT.$("ELE_BUTTON_SUBMIT","//span[contains(text(),'Submit')]","VISIBLE","TGTYPESCREENREG");

        WAIT.$(3,"TGTYPESCREENREG");

        SWIPE.$("UP","ELE_DRPDWN_POLICYCONTRACT_PRIMARYDIVIDENDPRIMIUM","//label[@id='workbenchForm:workbenchTabs:dividendOptionPrim_label']","TGTYPESCREENREG");

        WAIT.$(3,"TGTYPESCREENREG");

		var_dataToPass = getJsonData(var_CSVData , "$.records["+var_Count+"].PrimaryDividendOption");
                 LOGGER.info("Executed Step = STORE,var_dataToPass,var_CSVData,$.records[{var_Count}].PrimaryDividendOption,TGTYPESCREENREG");
        TAP.$("ELE_DRPDWN_POLICYCONTRACT_PRIMARYDIVIDENDPRIMIUM","//label[@id='workbenchForm:workbenchTabs:dividendOptionPrim_label']","TGTYPESCREENREG");

        WAIT.$(3,"TGTYPESCREENREG");

		try { 
		 //li[@id='workbenchForm:workbenchTabs:dividendOptionPrim_4']


			Thread.sleep(1000);
					for (int i = 0; i < 2000; i++){
						WebElement selectValueOfDropDown= driver.findElement(By.xpath("//li[@id='workbenchForm:workbenchTabs:dividendOptionPrim_"+ i +"']"));

						if (selectValueOfDropDown.getText().contains(var_dataToPass)){
		AddToReport("ASSERT," + selectValueOfDropDown.getText() +", CONTAINS,"+var_dataToPass);
		                                selectValueOfDropDown.click();
							
						
							break;
						}
					}
					takeScreenshot();

					Thread.sleep(2000);
		 
		 ;} 
		 catch(Exception e) { PrintVal("Renderer Script Exception : "+e.toString()); } 
		 PrintVal("Executed Step = CALLRENDERERSCRIPT,SELECT_PRIMARY_DIVIDEND_OPTION"); 
        WAIT.$(3,"TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SUBMIT","//span[contains(text(),'Submit')]","TGTYPESCREENREG");

        WAIT.$(3,"TGTYPESCREENREG");

        SWIPE.$("DOWN","ELE_MSG_ERROR_SUMMARY","//span[contains(@class,'ui-messages-error-summary')]","TGTYPESCREENREG");

        WAIT.$(3,"TGTYPESCREENREG");

        ASSERT.$("ELE_MSG_ERROR_SUMMARY","//span[contains(@class,'ui-messages-error-summary')]","VISIBLE","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

		var_CountN = var_CountN + ( 1 );
                 LOGGER.info("Executed Step = INCREASEVAR,var_CountN,1,TGTYPESCREENREG");
    	}
          LOGGER.info("Executed Step = WEND,TGTYPESCREENREG");
    	resetAttemps();

    }


    @Test
    public void nw048demotestplan() {

        // No need to declare / print method name here, handled by TestNg listener
        // declare("nw048demotestplan");

        CALL.$("LoginGIAS","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_USERNAME","//input[@id='guestForm:username']","autoapi","FALSE","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_PASSWORD","//input[@id='guestForm:password']","Autoapi!23","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_LOGIN","//button[contains(.,'Login')]","TGTYPESCREENREG");

        WAIT.$("ELE_BUTTON_GIAS_LOGO","//img[@id='headerForm:headerLogoImage']","VISIBLE","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

		String var_dataToPass;
                 LOGGER.info("Executed Step = VAR,String,var_dataToPass,TGTYPESCREENREG");
		int var_Count = 0;
                 LOGGER.info("Executed Step = VAR,Integer,var_Count,0,TGTYPESCREENREG");
		int var_CountN = 0;
                 LOGGER.info("Executed Step = VAR,Integer,var_CountN,0,TGTYPESCREENREG");
        LOGGER.info("Executed Step = REPEAT IF");
    	while (check(var_CountN,"<",1,"TGTYPESCREENREG")) {
          LOGGER.info("Executed Step = WHILE,var_CountN,<,1,TGTYPESCREENREG");
		String var_FileName = "http://10.1.104.229/json/20210524/9fcC9D.json";
                 LOGGER.info("Executed Step = VAR,String,var_FileName,http://10.1.104.229/json/20210524/9fcC9D.json,TGTYPESCREENREG");
		com.jayway.jsonpath.ReadContext var_CSVData = getJsonData("http://10.1.104.229/json/20210524/9fcC9D.json");
                 LOGGER.info("Executed Step = VAR,Json,var_CSVData,http://10.1.104.229/json/20210524/9fcC9D.json,TGTYPESCREENREG");
        CALL.$("ChangeSessionDate1","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_GIAS_LOGO","//img[@id='headerForm:headerLogoImage']","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

		String var_SessionDate1;
                 LOGGER.info("Executed Step = VAR,String,var_SessionDate1,TGTYPESCREENREG");
		var_SessionDate1 = getJsonData(var_CSVData , "$.records["+var_Count+"].SessionDate1");
                 LOGGER.info("Executed Step = STORE,var_SessionDate1,var_CSVData,$.records[{var_Count}].SessionDate1,TGTYPESCREENREG");
        TAP.$("ELE_BUTTON_SESSIONDATE","//a[@id='headerForm:sessionDatelink']","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_SESSIONDATE","//input[@id='sessionDateForm:sessionDateInput_input']",var_SessionDate1,"FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_CHANGEDATE","//span[contains(text(),'Change Date')]","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        CALL.$("CreatePolicyForReg048","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_GIAS_LOGO","//img[@id='headerForm:headerLogoImage']","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

		String var_PolicyNumber;
                 LOGGER.info("Executed Step = VAR,String,var_PolicyNumber,TGTYPESCREENREG");
		var_PolicyNumber = getJsonData(var_CSVData , "$.records["+var_Count+"].PolicyNumber");
                 LOGGER.info("Executed Step = STORE,var_PolicyNumber,var_CSVData,$.records[{var_Count}].PolicyNumber,TGTYPESCREENREG");
		String var_EffectiveDate;
                 LOGGER.info("Executed Step = VAR,String,var_EffectiveDate,TGTYPESCREENREG");
		var_EffectiveDate = getJsonData(var_CSVData , "$.records["+var_Count+"].EffectiveDate");
                 LOGGER.info("Executed Step = STORE,var_EffectiveDate,var_CSVData,$.records[{var_Count}].EffectiveDate,TGTYPESCREENREG");
		String var_CashWithApplication;
                 LOGGER.info("Executed Step = VAR,String,var_CashWithApplication,TGTYPESCREENREG");
		var_CashWithApplication = getJsonData(var_CSVData , "$.records["+var_Count+"].CashWithApplication");
                 LOGGER.info("Executed Step = STORE,var_CashWithApplication,var_CSVData,$.records[{var_Count}].CashWithApplication,TGTYPESCREENREG");
		String var_PaymentFrequency;
                 LOGGER.info("Executed Step = VAR,String,var_PaymentFrequency,TGTYPESCREENREG");
		var_PaymentFrequency = getJsonData(var_CSVData , "$.records["+var_Count+"].PaymentFrequency");
                 LOGGER.info("Executed Step = STORE,var_PaymentFrequency,var_CSVData,$.records[{var_Count}].PaymentFrequency,TGTYPESCREENREG");
		String var_PaymentMethod;
                 LOGGER.info("Executed Step = VAR,String,var_PaymentMethod,TGTYPESCREENREG");
		var_PaymentMethod = getJsonData(var_CSVData , "$.records["+var_Count+"].PaymentMethod");
                 LOGGER.info("Executed Step = STORE,var_PaymentMethod,var_CSVData,$.records[{var_Count}].PaymentMethod,TGTYPESCREENREG");
		String var_IssueState;
                 LOGGER.info("Executed Step = VAR,String,var_IssueState,TGTYPESCREENREG");
		var_IssueState = getJsonData(var_CSVData , "$.records["+var_Count+"].IssueState");
                 LOGGER.info("Executed Step = STORE,var_IssueState,var_CSVData,$.records[{var_Count}].IssueState,TGTYPESCREENREG");
		String var_AgentNumber;
                 LOGGER.info("Executed Step = VAR,String,var_AgentNumber,TGTYPESCREENREG");
		var_AgentNumber = getJsonData(var_CSVData , "$.records["+var_Count+"].AgentNumber");
                 LOGGER.info("Executed Step = STORE,var_AgentNumber,var_CSVData,$.records[{var_Count}].AgentNumber,TGTYPESCREENREG");
		String var_SplitPercentage;
                 LOGGER.info("Executed Step = VAR,String,var_SplitPercentage,TGTYPESCREENREG");
		var_SplitPercentage = getJsonData(var_CSVData , "$.records["+var_Count+"].SplitPercentage");
                 LOGGER.info("Executed Step = STORE,var_SplitPercentage,var_CSVData,$.records[{var_Count}].SplitPercentage,TGTYPESCREENREG");
		String var_ClientCheckName;
                 LOGGER.info("Executed Step = VAR,String,var_ClientCheckName,TGTYPESCREENREG");
		var_ClientCheckName = getJsonData(var_CSVData , "$.records["+var_Count+"].ClientCheckName");
                 LOGGER.info("Executed Step = STORE,var_ClientCheckName,var_CSVData,$.records[{var_Count}].ClientCheckName,TGTYPESCREENREG");
		String var_ClientFullName;
                 LOGGER.info("Executed Step = VAR,String,var_ClientFullName,TGTYPESCREENREG");
		var_ClientFullName = getJsonData(var_CSVData , "$.records["+var_Count+"].ClientFullName");
                 LOGGER.info("Executed Step = STORE,var_ClientFullName,var_CSVData,$.records[{var_Count}].ClientFullName,TGTYPESCREENREG");
		String var_ClientID;
                 LOGGER.info("Executed Step = VAR,String,var_ClientID,TGTYPESCREENREG");
		var_ClientID = getJsonData(var_CSVData , "$.records["+var_Count+"].ClientID");
                 LOGGER.info("Executed Step = STORE,var_ClientID,var_CSVData,$.records[{var_Count}].ClientID,TGTYPESCREENREG");
		String var_ClientBirthDate;
                 LOGGER.info("Executed Step = VAR,String,var_ClientBirthDate,TGTYPESCREENREG");
		var_ClientBirthDate = getJsonData(var_CSVData , "$.records["+var_Count+"].ClientBirthDate");
                 LOGGER.info("Executed Step = STORE,var_ClientBirthDate,var_CSVData,$.records[{var_Count}].ClientBirthDate,TGTYPESCREENREG");
        SWIPE.$("DOWN","ELE_TXTBOX_SEARCHTASKS","//input[@id='workbenchForm:workbenchTabs:taskSearchContainer:taskSearchSearch']","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_SEARCHTASKS","//input[@id='workbenchForm:workbenchTabs:taskSearchContainer:taskSearchSearch']","New Policy Application","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SEARCHTASKS","//a[@id='workbenchForm:workbenchTabs:taskSearchContainer:tb_buttonSearch']//span[@class='fa fa-search fa-lg']","TGTYPESCREENREG");

        WAIT.$("ELE_LINK_NEWPOLICYAPPLICATION","//span[contains(text(),'New Policy Application')]","VISIBLE","TGTYPESCREENREG");

        TAP.$("ELE_LINK_NEWPOLICYAPPLICATION","//span[contains(text(),'New Policy Application')]","TGTYPESCREENREG");

        WAIT.$("ELE_BUTTON_ADD","//button[@id='workbenchForm:workbenchTabs:tb_buttonAdd']//span[@class='ui-button-text ui-c'][contains(text(),'Add')]","VISIBLE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_ADD","//button[@id='workbenchForm:workbenchTabs:tb_buttonAdd']//span[@class='ui-button-text ui-c'][contains(text(),'Add')]","TGTYPESCREENREG");

        WAIT.$("ELE_TXTBOX_POLICYNUMBER","//input[@id='workbenchForm:workbenchTabs:policyNumber']","VISIBLE","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_POLICYNUMBER","//input[@id='workbenchForm:workbenchTabs:policyNumber']",var_PolicyNumber,"FALSE","TGTYPESCREENREG");

        TAP.$("ELE_DRPDWN_PAYMENTFREQUENCY","//label[@id='workbenchForm:workbenchTabs:paymentMode_label']","TGTYPESCREENREG");

		var_dataToPass = var_PaymentFrequency;
                 LOGGER.info("Executed Step = STORE,var_dataToPass,var_PaymentFrequency,TGTYPESCREENREG");
		try { 
		 Thread.sleep(1000);
		WebElement selectValueOfDropDown= driver.findElement(By.xpath("//li[@data-label='"+ var_dataToPass +"']"));

		takeScreenshot();

				selectValueOfDropDown.click();
		// Add Screenshot will only work for Android platform

		takeScreenshot();

				 Thread.sleep(1000);
		 
		 ;} 
		 catch(Exception e) { PrintVal("Renderer Script Exception : "+e.toString()); } 
		 PrintVal("Executed Step = CALLRENDERERSCRIPT,SELECTVALUEFROMDROPDOWN"); 
        WAIT.$(5,"TGTYPESCREENREG");

        TAP.$("ELE_DRPDWN_PAYMENTMETHOD","//label[@id='workbenchForm:workbenchTabs:paymentCode_label']","TGTYPESCREENREG");

		var_dataToPass = var_PaymentMethod;
                 LOGGER.info("Executed Step = STORE,var_dataToPass,var_PaymentMethod,TGTYPESCREENREG");
		try { 
		 Thread.sleep(1000);
		WebElement selectValueOfDropDown= driver.findElement(By.xpath("//li[@data-label='"+ var_dataToPass +"']"));

		takeScreenshot();

				selectValueOfDropDown.click();
		// Add Screenshot will only work for Android platform

		takeScreenshot();

				 Thread.sleep(1000);
		 
		 ;} 
		 catch(Exception e) { PrintVal("Renderer Script Exception : "+e.toString()); } 
		 PrintVal("Executed Step = CALLRENDERERSCRIPT,SELECTVALUEFROMDROPDOWN"); 
        WAIT.$(5,"TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_CASHWITHAPPLICATION","//input[@id='workbenchForm:workbenchTabs:cashWithApplication_input']",var_CashWithApplication,"FALSE","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_EFFECTIVEDATE","//input[@id='workbenchForm:workbenchTabs:effectiveDate_input']",var_EffectiveDate,"FALSE","TGTYPESCREENREG");

		var_dataToPass = var_IssueState;
                 LOGGER.info("Executed Step = STORE,var_dataToPass,var_IssueState,TGTYPESCREENREG");
        TAP.$("ELE_DRPDWN_ISSUESTATE","//label[@id='workbenchForm:workbenchTabs:countryAndStateCode_label']","TGTYPESCREENREG");

		try { 
		 Thread.sleep(1000);
		WebElement selectValueOfDropDown= driver.findElement(By.xpath("//li[@data-label='"+ var_dataToPass +"']"));

		takeScreenshot();

				selectValueOfDropDown.click();
		// Add Screenshot will only work for Android platform

		takeScreenshot();

				 Thread.sleep(1000);
		 
		 ;} 
		 catch(Exception e) { PrintVal("Renderer Script Exception : "+e.toString()); } 
		 PrintVal("Executed Step = CALLRENDERERSCRIPT,SELECTVALUEFROMDROPDOWN"); 
        WAIT.$(5,"TGTYPESCREENREG");

        SWIPE.$("UP","ELE_BUTTON_ADDAGENT_PLUS","//button[@id='workbenchForm:workbenchTabs:agentGrid:0:icon_button']","TGTYPESCREENREG");

        SWIPE.$("UP","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_ADDAGENT_PLUS","//button[@id='workbenchForm:workbenchTabs:agentGrid:0:icon_button']","TGTYPESCREENREG");

        WAIT.$("ELE_TXTBOX_AGENTNUMBER","//input[@id='workbenchForm:workbenchTabs:agentNumber']","VISIBLE","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_AGENTNUMBER","//input[@id='workbenchForm:workbenchTabs:agentNumber']",var_AgentNumber,"FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_AGENTSEARCH","//button[@id='workbenchForm:workbenchTabs:agentNumber_icon']","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_SPLITPERCENTAGE","//input[@id='workbenchForm:workbenchTabs:agentSplitPercentage_input']",var_SplitPercentage,"FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SAVE","//span[contains(text(),'Save')]","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SHOWLINKS","//button[@id='workbenchForm:workbenchTabs:tb_buttonLink']","TGTYPESCREENREG");

        TAP.$("ELE_LINK_SELECTCLIENT","//span[contains(text(),'Select Client')]","TGTYPESCREENREG");

        WAIT.$("ELE_TXTBOX_CLIENTSEARCHNAME","//input[@id='workbenchForm:workbenchTabs:searchName']","VISIBLE","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_CLIENTSEARCHNAME","//input[@id='workbenchForm:workbenchTabs:searchName']",var_ClientCheckName,"FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SEARCH","//button[@id='workbenchForm:workbenchTabs:button_search']","TGTYPESCREENREG");

        WAIT.$("ELE_TXTBOX_FILTER_CLIENTNAME","//input[@id='workbenchForm:workbenchTabs:clientGrid:individualName_Col:filter']","VISIBLE","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_FILTER_CLIENTID","//input[@id='workbenchForm:workbenchTabs:clientGrid:clientId_Col:filter']",var_ClientID,"FALSE","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        TAP.$("ELE_LINK_FIRST_CLIENT","//span[@id='workbenchForm:workbenchTabs:clientGrid:0:individualName']","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_TRUE","//button[@id='workbenchForm:workbenchTabs:button_select']//span[@class='ui-button-icon-left ui-icon ui-c fa fa-check']","TGTYPESCREENREG");

        WAIT.$("ELE_BUTTON_CONTINUE","//span[contains(text(),'Continue')]","VISIBLE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_CONTINUE","//span[contains(text(),'Continue')]","TGTYPESCREENREG");

        WAIT.$("ELE_LABEL_OWNERNAME","//span[@id='workbenchForm:workbenchTabs:ownerLabel']","VISIBLE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SAVE","//span[contains(text(),'Save')]","TGTYPESCREENREG");

        WAIT.$("ELE_MESSAGE_ITEMSUCCESFULLYADDED","//div[@id='workbenchForm:workbenchTabs:messages']//span[@class='ui-messages-info-summary'][contains(text(),'Item successfully added.')]","VISIBLE","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        CALL.$("BenefitPlanForCreatePolicyNW048","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

		String var_BenefitPlan;
                 LOGGER.info("Executed Step = VAR,String,var_BenefitPlan,TGTYPESCREENREG");
		var_BenefitPlan = getJsonData(var_CSVData , "$.records["+var_Count+"].BenefitPlan");
                 LOGGER.info("Executed Step = STORE,var_BenefitPlan,var_CSVData,$.records[{var_Count}].BenefitPlan,TGTYPESCREENREG");
		String var_Units;
                 LOGGER.info("Executed Step = VAR,String,var_Units,TGTYPESCREENREG");
		var_Units = getJsonData(var_CSVData , "$.records["+var_Count+"].Units");
                 LOGGER.info("Executed Step = STORE,var_Units,var_CSVData,$.records[{var_Count}].Units,TGTYPESCREENREG");
		String var_IssueAge;
                 LOGGER.info("Executed Step = VAR,String,var_IssueAge,TGTYPESCREENREG");
		var_IssueAge = getJsonData(var_CSVData , "$.records["+var_Count+"].IssueAge");
                 LOGGER.info("Executed Step = STORE,var_IssueAge,var_CSVData,$.records[{var_Count}].IssueAge,TGTYPESCREENREG");
		String var_RiskClass;
                 LOGGER.info("Executed Step = VAR,String,var_RiskClass,TGTYPESCREENREG");
		var_RiskClass = getJsonData(var_CSVData , "$.records["+var_Count+"].RiskClass");
                 LOGGER.info("Executed Step = STORE,var_RiskClass,var_CSVData,$.records[{var_Count}].RiskClass,TGTYPESCREENREG");
		String var_NonforfeitureOption;
                 LOGGER.info("Executed Step = VAR,String,var_NonforfeitureOption,TGTYPESCREENREG");
		var_NonforfeitureOption = getJsonData(var_CSVData , "$.records["+var_Count+"].NonforfeitureOption");
                 LOGGER.info("Executed Step = STORE,var_NonforfeitureOption,var_CSVData,$.records[{var_Count}].NonforfeitureOption,TGTYPESCREENREG");
		String var_PrimaryDividend;
                 LOGGER.info("Executed Step = VAR,String,var_PrimaryDividend,TGTYPESCREENREG");
		var_PrimaryDividend = getJsonData(var_CSVData , "$.records["+var_Count+"].PrimaryDividend");
                 LOGGER.info("Executed Step = STORE,var_PrimaryDividend,var_CSVData,$.records[{var_Count}].PrimaryDividend,TGTYPESCREENREG");
		String var_SecondaryDividend;
                 LOGGER.info("Executed Step = VAR,String,var_SecondaryDividend,TGTYPESCREENREG");
		var_SecondaryDividend = getJsonData(var_CSVData , "$.records["+var_Count+"].SecondaryDividend");
                 LOGGER.info("Executed Step = STORE,var_SecondaryDividend,var_CSVData,$.records[{var_Count}].SecondaryDividend,TGTYPESCREENREG");
        TAP.$("ELE_BUTTON_SHOWLINKS","//button[@id='workbenchForm:workbenchTabs:tb_buttonLink']","TGTYPESCREENREG");

        TAP.$("ELE_LINK_BENEFITS","//span[contains(text(),'Benefits')]","TGTYPESCREENREG");

        WAIT.$("ELE_DRPDWN_BENEFITPLAN","//label[@id='workbenchForm:workbenchTabs:initialPlanCode_label']","VISIBLE","TGTYPESCREENREG");

        TAP.$("ELE_DRPDWN_BENEFITPLAN","//label[@id='workbenchForm:workbenchTabs:initialPlanCode_label']","TGTYPESCREENREG");

		var_dataToPass = var_BenefitPlan;
                 LOGGER.info("Executed Step = STORE,var_dataToPass,var_BenefitPlan,TGTYPESCREENREG");
		try { 
		 Thread.sleep(1000);
		WebElement selectValueOfDropDown= driver.findElement(By.xpath("//li[@data-label='"+ var_dataToPass +"']"));

		takeScreenshot();

				selectValueOfDropDown.click();
		// Add Screenshot will only work for Android platform

		takeScreenshot();

				 Thread.sleep(1000);
		 
		 ;} 
		 catch(Exception e) { PrintVal("Renderer Script Exception : "+e.toString()); } 
		 PrintVal("Executed Step = CALLRENDERERSCRIPT,SELECTVALUEFROMDROPDOWN"); 
        WAIT.$(5,"TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_CONTINUE","//span[contains(text(),'Continue')]","TGTYPESCREENREG");

        WAIT.$("ELE_TXTBOX_UNITS","//input[@id='workbenchForm:workbenchTabs:unitsOfInsurance_input']","VISIBLE","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_UNITS","//input[@id='workbenchForm:workbenchTabs:unitsOfInsurance_input']",var_Units,"FALSE","TGTYPESCREENREG");

		var_dataToPass = var_RiskClass;
                 LOGGER.info("Executed Step = STORE,var_dataToPass,var_RiskClass,TGTYPESCREENREG");
        TAP.$("ELE_DRPDWN_RISK_CLASS","//label[@id='workbenchForm:workbenchTabs:smokerCode_label']","TGTYPESCREENREG");

		try { 
		 Thread.sleep(1000);
		WebElement selectValueOfDropDown= driver.findElement(By.xpath("//li[@data-label='"+ var_dataToPass +"']"));

		takeScreenshot();

				selectValueOfDropDown.click();
		// Add Screenshot will only work for Android platform

		takeScreenshot();

				 Thread.sleep(1000);
		 
		 ;} 
		 catch(Exception e) { PrintVal("Renderer Script Exception : "+e.toString()); } 
		 PrintVal("Executed Step = CALLRENDERERSCRIPT,SELECTVALUEFROMDROPDOWN"); 
        WAIT.$(5,"TGTYPESCREENREG");

		var_dataToPass = var_NonforfeitureOption;
                 LOGGER.info("Executed Step = STORE,var_dataToPass,var_NonforfeitureOption,TGTYPESCREENREG");
        TAP.$("ELE_DRPDWN_NONFORFEITUREOPTION","//label[@id='workbenchForm:workbenchTabs:defaultNonForfOpt_label']","TGTYPESCREENREG");

		try { 
		 Thread.sleep(1000);
		WebElement selectValueOfDropDown= driver.findElement(By.xpath("//li[@data-label='"+ var_dataToPass +"']"));

		takeScreenshot();

				selectValueOfDropDown.click();
		// Add Screenshot will only work for Android platform

		takeScreenshot();

				 Thread.sleep(1000);
		 
		 ;} 
		 catch(Exception e) { PrintVal("Renderer Script Exception : "+e.toString()); } 
		 PrintVal("Executed Step = CALLRENDERERSCRIPT,SELECTVALUEFROMDROPDOWN"); 
        WAIT.$(5,"TGTYPESCREENREG");

		var_dataToPass = var_PrimaryDividend;
                 LOGGER.info("Executed Step = STORE,var_dataToPass,var_PrimaryDividend,TGTYPESCREENREG");
        TAP.$("ELE_DRPDWN_PRIMARYDIVIDENDOPETION_POLICY","//label[@id='workbenchForm:workbenchTabs:primaryDividendOption_label']","TGTYPESCREENREG");

		try { 
		 Thread.sleep(1000);
		WebElement selectValueOfDropDown= driver.findElement(By.xpath("//li[@data-label='"+ var_dataToPass +"']"));

		takeScreenshot();

				selectValueOfDropDown.click();
		// Add Screenshot will only work for Android platform

		takeScreenshot();

				 Thread.sleep(1000);
		 
		 ;} 
		 catch(Exception e) { PrintVal("Renderer Script Exception : "+e.toString()); } 
		 PrintVal("Executed Step = CALLRENDERERSCRIPT,SELECTVALUEFROMDROPDOWN"); 
        WAIT.$(5,"TGTYPESCREENREG");

		var_dataToPass = var_SecondaryDividend;
                 LOGGER.info("Executed Step = STORE,var_dataToPass,var_SecondaryDividend,TGTYPESCREENREG");
        TAP.$("ELE_DRPDWN_SECONDARY_DIVIDEND","//label[@id='workbenchForm:workbenchTabs:secondaryDividendOption_label']","TGTYPESCREENREG");

		try { 
		 //li[@id='workbenchForm:workbenchTabs:secondaryDividendOption_0']



			Thread.sleep(1000);
					for (int i = 0; i < 2000; i++){
						WebElement selectValueOfDropDown= driver.findElement(By.xpath("//li[@id='workbenchForm:workbenchTabs:secondaryDividendOption_"+ i +"']"));

						if (selectValueOfDropDown.getText().contains(var_dataToPass)){
		AddToReport("ASSERT," + selectValueOfDropDown.getText() +", CONTAINS,"+var_dataToPass);
		                                selectValueOfDropDown.click();
							
						
							break;
						}
					}
					takeScreenshot();

					Thread.sleep(2000);
		 
		 ;} 
		 catch(Exception e) { PrintVal("Renderer Script Exception : "+e.toString()); } 
		 PrintVal("Executed Step = CALLRENDERERSCRIPT,SELECT_SECONDARYDIVIDENDOPTION"); 
        WAIT.$(5,"TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SAVE","//span[contains(text(),'Save')]","TGTYPESCREENREG");

        WAIT.$("ELE_MESSAGE_ITEMSUCCESFULLYADDED","//div[@id='workbenchForm:workbenchTabs:messages']//span[@class='ui-messages-info-summary'][contains(text(),'Item successfully added.')]","VISIBLE","TGTYPESCREENREG");

        TAP.$("ELE_TAB_CLOSE","//li[@class='ui-state-default ui-tabs-selected ui-state-active ui-corner-top']//span[@class='ui-icon ui-icon-close']","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        CALL.$("IssuePolicyForCreatePolicyNW048","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SHOWLINKS","//button[@id='workbenchForm:workbenchTabs:tb_buttonLink']","TGTYPESCREENREG");

        TAP.$("ELE_LINK_ISSUE","//span[@class='ui-menuitem-text'][contains(text(),'Issue')]","TGTYPESCREENREG");

        WAIT.$("ELE_BUTTON_SUBMIT","//span[contains(text(),'Submit')]","VISIBLE","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SUBMIT","//span[contains(text(),'Submit')]","TGTYPESCREENREG");

        WAIT.$("ELE_MSG_REQUESTCOMPLETEDSUCCESSFULLY","//div[@id='workbenchForm:workbenchTabs:messages']//span[@class='ui-messages-info-summary'][contains(text(),'Request completed successfully.')]","VISIBLE","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        TAP.$("ELE_TAB_CLOSE","//li[@class='ui-state-default ui-tabs-selected ui-state-active ui-corner-top']//span[@class='ui-icon ui-icon-close']","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        CALL.$("SettlePolicyForCreatePolicyNW048","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SHOWLINKS","//button[@id='workbenchForm:workbenchTabs:tb_buttonLink']","TGTYPESCREENREG");

        TAP.$("ELE_LINK_SETTLE","//span[contains(text(),'Settle')]","TGTYPESCREENREG");

        WAIT.$("ELE_BUTTON_SUBMIT","//span[contains(text(),'Submit')]","VISIBLE","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        SWIPE.$("DOWN","TGTYPESCREENREG");

        SWIPE.$("DOWN","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SUBMIT","//span[contains(text(),'Submit')]","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        WAIT.$("ELE_MSG_REQUESTCOMPLETEDSUCCESSFULLY","//div[@id='workbenchForm:workbenchTabs:messages']//span[@class='ui-messages-info-summary'][contains(text(),'Request completed successfully.')]","VISIBLE","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        TAP.$("ELE_TAB_CLOSE","//li[@class='ui-state-default ui-tabs-selected ui-state-active ui-corner-top']//span[@class='ui-icon ui-icon-close']","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        CALL.$("PremiumPolicyForCreatePolicyNW048","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

		String var_PremiumAnnual;
                 LOGGER.info("Executed Step = VAR,String,var_PremiumAnnual,TGTYPESCREENREG");
		var_PremiumAnnual = getJsonData(var_CSVData , "$.records["+var_Count+"].PremiumAnnual");
                 LOGGER.info("Executed Step = STORE,var_PremiumAnnual,var_CSVData,$.records[{var_Count}].PremiumAnnual,TGTYPESCREENREG");
		String var_PremiumSemiannual;
                 LOGGER.info("Executed Step = VAR,String,var_PremiumSemiannual,TGTYPESCREENREG");
		var_PremiumSemiannual = getJsonData(var_CSVData , "$.records["+var_Count+"].PremiumSemiannual");
                 LOGGER.info("Executed Step = STORE,var_PremiumSemiannual,var_CSVData,$.records[{var_Count}].PremiumSemiannual,TGTYPESCREENREG");
		String var_PremiumQuarterly;
                 LOGGER.info("Executed Step = VAR,String,var_PremiumQuarterly,TGTYPESCREENREG");
		var_PremiumQuarterly = getJsonData(var_CSVData , "$.records["+var_Count+"].PremiumQuarterly");
                 LOGGER.info("Executed Step = STORE,var_PremiumQuarterly,var_CSVData,$.records[{var_Count}].PremiumQuarterly,TGTYPESCREENREG");
		String var_PremiumMonthly;
                 LOGGER.info("Executed Step = VAR,String,var_PremiumMonthly,TGTYPESCREENREG");
		var_PremiumMonthly = getJsonData(var_CSVData , "$.records["+var_Count+"].PremiumMonthly");
                 LOGGER.info("Executed Step = STORE,var_PremiumMonthly,var_CSVData,$.records[{var_Count}].PremiumMonthly,TGTYPESCREENREG");
        TAP.$("ELE_BUTTON_SHOWLINKS","//button[@id='workbenchForm:workbenchTabs:tb_buttonLink']","TGTYPESCREENREG");

        TAP.$("ELE_LINK_PREMIUMS","//span[contains(text(),'Premiums')]","TGTYPESCREENREG");

        WAIT.$("ELE_LINK_FIRST_ANNUAL_PREMIUM","//span[@id='workbenchForm:workbenchTabs:gridNavGrid:0:benefitAnnualPremium']","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        ASSERT.$("ELE_LINK_FIRST_ANNUAL_PREMIUM","//span[@id='workbenchForm:workbenchTabs:gridNavGrid:0:benefitAnnualPremium']","CONTAINS",var_PremiumAnnual,"TGTYPESCREENREG");

        ASSERT.$("ELE_LINK_FIRST_SEMI_ANNUAL","//span[@id='workbenchForm:workbenchTabs:gridNavGrid:0:benefitSemiAnnuPrem']","CONTAINS",var_PremiumSemiannual,"TGTYPESCREENREG");

        ASSERT.$("ELE_LINK_FIRST_QUATERLY_PREMIUM","//span[@id='workbenchForm:workbenchTabs:gridNavGrid:0:benefitQuarterlyPrem']","CONTAINS",var_PremiumQuarterly,"TGTYPESCREENREG");

        ASSERT.$("ELE_LINK_FIRST_MONTHLY_PREMIUM","//span[@id='workbenchForm:workbenchTabs:gridNavGrid:0:benefitMonthlyPrem']","CONTAINS",var_PremiumMonthly,"TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_GIAS_LOGO","//img[@id='headerForm:headerLogoImage']","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_GIAS_LOGO","//img[@id='headerForm:headerLogoImage']","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        CALL.$("ChangeSessionDate2","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_GIAS_LOGO","//img[@id='headerForm:headerLogoImage']","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

		String var_SessionDate2;
                 LOGGER.info("Executed Step = VAR,String,var_SessionDate2,TGTYPESCREENREG");
		var_SessionDate2 = getJsonData(var_CSVData , "$.records["+var_Count+"].SessionDate2");
                 LOGGER.info("Executed Step = STORE,var_SessionDate2,var_CSVData,$.records[{var_Count}].SessionDate2,TGTYPESCREENREG");
        TAP.$("ELE_BUTTON_SESSIONDATE","//a[@id='headerForm:sessionDatelink']","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_SESSIONDATE","//input[@id='sessionDateForm:sessionDateInput_input']",var_SessionDate2,"FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_CHANGEDATE","//span[contains(text(),'Change Date')]","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        CALL.$("PolicyInquiryFirstForNW048","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_GIAS_LOGO","//img[@id='headerForm:headerLogoImage']","TGTYPESCREENREG");

		String var_TotalAmount;
                 LOGGER.info("Executed Step = VAR,String,var_TotalAmount,TGTYPESCREENREG");
		var_TotalAmount = getJsonData(var_CSVData , "$.records["+var_Count+"].TotalAmount1");
                 LOGGER.info("Executed Step = STORE,var_TotalAmount,var_CSVData,$.records[{var_Count}].TotalAmount1,TGTYPESCREENREG");
		String var_AmountAccumulatewithInterest;
                 LOGGER.info("Executed Step = VAR,String,var_AmountAccumulatewithInterest,TGTYPESCREENREG");
		var_AmountAccumulatewithInterest = getJsonData(var_CSVData , "$.records["+var_Count+"].AmountAccumulatewithInterest1");
                 LOGGER.info("Executed Step = STORE,var_AmountAccumulatewithInterest,var_CSVData,$.records[{var_Count}].AmountAccumulatewithInterest1,TGTYPESCREENREG");
		String var_AmountPurchaseOneYearTerm;
                 LOGGER.info("Executed Step = VAR,String,var_AmountPurchaseOneYearTerm,TGTYPESCREENREG");
		var_AmountPurchaseOneYearTerm = getJsonData(var_CSVData , "$.records["+var_Count+"].AmountPurchaseOneYearTerm1");
                 LOGGER.info("Executed Step = STORE,var_AmountPurchaseOneYearTerm,var_CSVData,$.records[{var_Count}].AmountPurchaseOneYearTerm1,TGTYPESCREENREG");
		String var_BenefitPurchaseOneYearTermAdditions;
                 LOGGER.info("Executed Step = VAR,String,var_BenefitPurchaseOneYearTermAdditions,TGTYPESCREENREG");
		var_BenefitPurchaseOneYearTermAdditions = getJsonData(var_CSVData , "$.records["+var_Count+"].BenefitPurchaseOneYearTermAdditions1");
                 LOGGER.info("Executed Step = STORE,var_BenefitPurchaseOneYearTermAdditions,var_CSVData,$.records[{var_Count}].BenefitPurchaseOneYearTermAdditions1,TGTYPESCREENREG");
        SWIPE.$("DOWN","ELE_TXTBOX_SEARCHTASKS","//input[@id='workbenchForm:workbenchTabs:taskSearchContainer:taskSearchSearch']","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_SEARCHTASKS","//input[@id='workbenchForm:workbenchTabs:taskSearchContainer:taskSearchSearch']","Policy Inquiry","FALSE","TGTYPESCREENREG");

        WAIT.$(3,"TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SEARCHTASKS","//a[@id='workbenchForm:workbenchTabs:taskSearchContainer:tb_buttonSearch']//span[@class='fa fa-search fa-lg']","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        TAP.$("ELE_LINK_POLICYINQUIRY","//span[contains(text(),'Policy Inquiry')]","TGTYPESCREENREG");

        WAIT.$("ELE_TXTBOX_POLICYNUMBER","//input[@id='workbenchForm:workbenchTabs:policyNumber']","VISIBLE","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_POLICYNUMBER","//input[@id='workbenchForm:workbenchTabs:policyNumber']",var_PolicyNumber,"FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_CONTINUE","//span[contains(text(),'Continue')]","TGTYPESCREENREG");

        WAIT.$("ELE_LABEL_POLICYNUMBER","//span[@id='workbenchForm:workbenchTabs:policyNumber']","VISIBLE","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SHOWLINKS","//button[@id='workbenchForm:workbenchTabs:tb_buttonLink']","TGTYPESCREENREG");

        SWIPE.$("UP","TGTYPESCREENREG");

        TAP.$("ELE_LINK_DIVIDENDS_COUPONS","//span[contains(text(),'Dividends/Coupons')]","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SHOWLINKS","//button[@id='workbenchForm:workbenchTabs:tb_buttonLink']","TGTYPESCREENREG");

        TAP.$("ELE_LINK_TRANSACTIONHISTORY","//span[contains(text(),'Transaction History')]","TGTYPESCREENREG");

        WAIT.$("ELE_LINK_FIRST_DIVIDEND","//span[@id='workbenchForm:workbenchTabs:grid:0:dividendCouponIndi']","VISIBLE","TGTYPESCREENREG");

        TAP.$("ELE_LINK_FIRST_DIVIDEND","//span[@id='workbenchForm:workbenchTabs:grid:0:dividendCouponIndi']","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        WAIT.$("ELE_LABEL_DIVIDEND_TOTAL_AMOUNT","//span[@id='workbenchForm:workbenchTabs:divCpnCashValueAmt']","VISIBLE","TGTYPESCREENREG");

        ASSERT.$("ELE_LABEL_DIVIDEND_TOTAL_AMOUNT","//span[@id='workbenchForm:workbenchTabs:divCpnCashValueAmt']","CONTAINS",var_TotalAmount,"TGTYPESCREENREG");

        ASSERT.$("ELE_LABEL_DIVIDEND_ACCUMULATE_WITH_INTEREST","//span[@id='workbenchForm:workbenchTabs:amountAccumulateWithInterest']","CONTAINS",var_AmountAccumulatewithInterest,"TGTYPESCREENREG");

        ASSERT.$("ELE_LABEL_DIVIDEND_PURCHASE_ONE_YEAR_TERM","//span[@id='workbenchForm:workbenchTabs:amountPurchase1YearTerm']","CONTAINS",var_AmountPurchaseOneYearTerm,"TGTYPESCREENREG");

        ASSERT.$("ELE_LABEL_DIVIDEND_PURCHASE_ONE_YEAR_TERM_ADDITIONS","//span[@id='workbenchForm:workbenchTabs:benefitPurchase1YearTerm']","CONTAINS",var_BenefitPurchaseOneYearTermAdditions,"TGTYPESCREENREG");

        WAIT.$(10,"TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_GIAS_LOGO","//img[@id='headerForm:headerLogoImage']","TGTYPESCREENREG");

        WAIT.$(10,"TGTYPESCREENREG");

        CALL.$("ChangeSessionDate3","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_GIAS_LOGO","//img[@id='headerForm:headerLogoImage']","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

		String var_SessionDate3;
                 LOGGER.info("Executed Step = VAR,String,var_SessionDate3,TGTYPESCREENREG");
		var_SessionDate3 = getJsonData(var_CSVData , "$.records["+var_Count+"].SessionDate3");
                 LOGGER.info("Executed Step = STORE,var_SessionDate3,var_CSVData,$.records[{var_Count}].SessionDate3,TGTYPESCREENREG");
        TAP.$("ELE_BUTTON_SESSIONDATE","//a[@id='headerForm:sessionDatelink']","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_SESSIONDATE","//input[@id='sessionDateForm:sessionDateInput_input']",var_SessionDate3,"FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_CHANGEDATE","//span[contains(text(),'Change Date')]","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        CALL.$("PolicyInquirySecoundForNW048","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_GIAS_LOGO","//img[@id='headerForm:headerLogoImage']","TGTYPESCREENREG");

		var_TotalAmount = getJsonData(var_CSVData , "$.records["+var_Count+"].TotalAmount2");
                 LOGGER.info("Executed Step = STORE,var_TotalAmount,var_CSVData,$.records[{var_Count}].TotalAmount2,TGTYPESCREENREG");
		var_AmountPurchaseOneYearTerm = getJsonData(var_CSVData , "$.records["+var_Count+"].AmountPurchaseOneYearTerm2");
                 LOGGER.info("Executed Step = STORE,var_AmountPurchaseOneYearTerm,var_CSVData,$.records[{var_Count}].AmountPurchaseOneYearTerm2,TGTYPESCREENREG");
		var_BenefitPurchaseOneYearTermAdditions = getJsonData(var_CSVData , "$.records["+var_Count+"].BenefitPurchaseOneYearTermAdditions2");
                 LOGGER.info("Executed Step = STORE,var_BenefitPurchaseOneYearTermAdditions,var_CSVData,$.records[{var_Count}].BenefitPurchaseOneYearTermAdditions2,TGTYPESCREENREG");
        SWIPE.$("DOWN","ELE_TXTBOX_SEARCHTASKS","//input[@id='workbenchForm:workbenchTabs:taskSearchContainer:taskSearchSearch']","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_SEARCHTASKS","//input[@id='workbenchForm:workbenchTabs:taskSearchContainer:taskSearchSearch']","Policy Inquiry","FALSE","TGTYPESCREENREG");

        WAIT.$(3,"TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SEARCHTASKS","//a[@id='workbenchForm:workbenchTabs:taskSearchContainer:tb_buttonSearch']//span[@class='fa fa-search fa-lg']","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        TAP.$("ELE_LINK_POLICYINQUIRY","//span[contains(text(),'Policy Inquiry')]","TGTYPESCREENREG");

        WAIT.$("ELE_TXTBOX_POLICYNUMBER","//input[@id='workbenchForm:workbenchTabs:policyNumber']","VISIBLE","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_POLICYNUMBER","//input[@id='workbenchForm:workbenchTabs:policyNumber']",var_PolicyNumber,"FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_CONTINUE","//span[contains(text(),'Continue')]","TGTYPESCREENREG");

        WAIT.$("ELE_LABEL_POLICYNUMBER","//span[@id='workbenchForm:workbenchTabs:policyNumber']","VISIBLE","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SHOWLINKS","//button[@id='workbenchForm:workbenchTabs:tb_buttonLink']","TGTYPESCREENREG");

        SWIPE.$("UP","TGTYPESCREENREG");

        TAP.$("ELE_LINK_DIVIDENDS_COUPONS","//span[contains(text(),'Dividends/Coupons')]","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SHOWLINKS","//button[@id='workbenchForm:workbenchTabs:tb_buttonLink']","TGTYPESCREENREG");

        TAP.$("ELE_LINK_TRANSACTIONHISTORY","//span[contains(text(),'Transaction History')]","TGTYPESCREENREG");

        WAIT.$("ELE_LINK_FIRST_DIVIDEND","//span[@id='workbenchForm:workbenchTabs:grid:0:dividendCouponIndi']","VISIBLE","TGTYPESCREENREG");

        TAP.$("ELE_LINK_FIRST_DIVIDEND","//span[@id='workbenchForm:workbenchTabs:grid:0:dividendCouponIndi']","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        WAIT.$("ELE_LABEL_DIVIDEND_TOTAL_AMOUNT","//span[@id='workbenchForm:workbenchTabs:divCpnCashValueAmt']","VISIBLE","TGTYPESCREENREG");

        ASSERT.$("ELE_LABEL_DIVIDEND_TOTAL_AMOUNT","//span[@id='workbenchForm:workbenchTabs:divCpnCashValueAmt']","CONTAINS",var_TotalAmount,"TGTYPESCREENREG");

        ASSERT.$("ELE_LABEL_DIVIDEND_PURCHASE_ONE_YEAR_TERM","//span[@id='workbenchForm:workbenchTabs:amountPurchase1YearTerm']","CONTAINS",var_AmountPurchaseOneYearTerm,"TGTYPESCREENREG");

        ASSERT.$("ELE_LABEL_DIVIDEND_PURCHASE_ONE_YEAR_TERM_ADDITIONS","//span[@id='workbenchForm:workbenchTabs:benefitPurchase1YearTerm']","CONTAINS",var_BenefitPurchaseOneYearTermAdditions,"TGTYPESCREENREG");

        WAIT.$(10,"TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_GIAS_LOGO","//img[@id='headerForm:headerLogoImage']","TGTYPESCREENREG");

        WAIT.$(10,"TGTYPESCREENREG");

		var_CountN = var_CountN + ( 1 );
                 LOGGER.info("Executed Step = INCREASEVAR,var_CountN,1,TGTYPESCREENREG");
    	}
          LOGGER.info("Executed Step = WEND,TGTYPESCREENREG");
    	resetAttemps();

    }


    @Test
    public void nw056dsn() {

        // No need to declare / print method name here, handled by TestNg listener
        // declare("nw056dsn");

        CALL.$("LoginGIAS","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_USERNAME","//input[@id='guestForm:username']","autoapi","FALSE","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_PASSWORD","//input[@id='guestForm:password']","Autoapi!23","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_LOGIN","//button[contains(.,'Login')]","TGTYPESCREENREG");

        WAIT.$("ELE_BUTTON_GIAS_LOGO","//img[@id='headerForm:headerLogoImage']","VISIBLE","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

		String var_dataToPass;
                 LOGGER.info("Executed Step = VAR,String,var_dataToPass,TGTYPESCREENREG");
		int var_Count = 0;
                 LOGGER.info("Executed Step = VAR,Integer,var_Count,0,TGTYPESCREENREG");
		int var_CountN = 0;
                 LOGGER.info("Executed Step = VAR,Integer,var_CountN,0,TGTYPESCREENREG");
        LOGGER.info("Executed Step = REPEAT IF");
    	while (check(var_CountN,"<",1,"TGTYPESCREENREG")) {
          LOGGER.info("Executed Step = WHILE,var_CountN,<,1,TGTYPESCREENREG");
		String var_FileName = "http://10.1.104.229/json/20210524/eDWIt1.json";
                 LOGGER.info("Executed Step = VAR,String,var_FileName,http://10.1.104.229/json/20210524/eDWIt1.json,TGTYPESCREENREG");
		com.jayway.jsonpath.ReadContext var_CSVData = getJsonData("http://10.1.104.229/json/20210524/eDWIt1.json");
                 LOGGER.info("Executed Step = VAR,Json,var_CSVData,http://10.1.104.229/json/20210524/eDWIt1.json,TGTYPESCREENREG");
        CALL.$("NW056DsnPlanSetup","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_GIAS_LOGO","//img[@id='headerForm:headerLogoImage']","TGTYPESCREENREG");

		String var_BenefitPlanCode1;
                 LOGGER.info("Executed Step = VAR,String,var_BenefitPlanCode1,TGTYPESCREENREG");
		var_BenefitPlanCode1 = getJsonData(var_CSVData , "$.records["+var_Count+"].BenefitPlanCode1");
                 LOGGER.info("Executed Step = STORE,var_BenefitPlanCode1,var_CSVData,$.records[{var_Count}].BenefitPlanCode1,TGTYPESCREENREG");
		String var_SurrenderDividedToPayPremium1;
                 LOGGER.info("Executed Step = VAR,String,var_SurrenderDividedToPayPremium1,TGTYPESCREENREG");
		var_SurrenderDividedToPayPremium1 = getJsonData(var_CSVData , "$.records["+var_Count+"].SurrenderDividedToPayPremium1");
                 LOGGER.info("Executed Step = STORE,var_SurrenderDividedToPayPremium1,var_CSVData,$.records[{var_Count}].SurrenderDividedToPayPremium1,TGTYPESCREENREG");
		String var_AutomaticLapseNonforfeiture1;
                 LOGGER.info("Executed Step = VAR,String,var_AutomaticLapseNonforfeiture1,TGTYPESCREENREG");
		var_AutomaticLapseNonforfeiture1 = getJsonData(var_CSVData , "$.records["+var_Count+"].AutomaticLapseNonforfeiture1");
                 LOGGER.info("Executed Step = STORE,var_AutomaticLapseNonforfeiture1,var_CSVData,$.records[{var_Count}].AutomaticLapseNonforfeiture1,TGTYPESCREENREG");
        WAIT.$(5,"TGTYPESCREENREG");

        SWIPE.$("DOWN","ELE_TXTBOX_SEARCHTASKS","//input[@id='workbenchForm:workbenchTabs:taskSearchContainer:taskSearchSearch']","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_SEARCHTASKS","//input[@id='workbenchForm:workbenchTabs:taskSearchContainer:taskSearchSearch']","Plan Setup","FALSE","TGTYPESCREENREG");

        WAIT.$(3,"TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SEARCHTASKS","//a[@id='workbenchForm:workbenchTabs:taskSearchContainer:tb_buttonSearch']//span[@class='fa fa-search fa-lg']","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        TAP.$("ELE_LINK_FIRST_TASKS","//button[@id='workbenchForm:workbenchTabs:taskSearchContainer:0:availableTask']","TGTYPESCREENREG");

        WAIT.$("ELE_TXTBOX_FILTER_PLANCODE","//input[@id='workbenchForm:workbenchTabs:grid:planCode_Col:filter']","VISIBLE","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_FILTER_PLANCODE","//input[@id='workbenchForm:workbenchTabs:grid:planCode_Col:filter']",var_BenefitPlanCode1,"FALSE","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        TAP.$("ELE_LINK_FIRST_PLANCODE","//span[@id='workbenchForm:workbenchTabs:grid:0:planCode']","TGTYPESCREENREG");

        WAIT.$("ELE_BUTTON_EDIT","//button[@id='workbenchForm:workbenchTabs:tb_buttonUpdate']","VISIBLE","TGTYPESCREENREG");

        TAP.$("ELE_TAB_NONFORFEITURE","//span[contains(text(),'Nonforfeiture')]","TGTYPESCREENREG");

        WAIT.$(4,"TGTYPESCREENREG");

		String var_CheckSurrender = "PASS";
                 LOGGER.info("Executed Step = VAR,String,var_CheckSurrender,PASS,TGTYPESCREENREG");
        SWIPE.$("UP","TGTYPESCREENREG");

        LOGGER.info("Executed Step = START IF");
        if (check("ELE_LABEL_SURRENDERDIVIDENDSTOPAYPREMIUMS","//span[@id='workbenchForm:workbenchTabs:divSurrAlloForNFO']","<>",var_SurrenderDividedToPayPremium1,"TGTYPESCREENREG")) {
         LOGGER.info("Executed Step = IF,ELE_LABEL_SURRENDERDIVIDENDSTOPAYPREMIUMS,<>,var_SurrenderDividedToPayPremium1,TGTYPESCREENREG");
		var_CheckSurrender = "FAIL";
                 LOGGER.info("Executed Step = STORE,var_CheckSurrender,FAIL,TGTYPESCREENREG");
        }
          LOGGER.info("Executed Step = ENDIF,TGTYPESCREENREG");
        LOGGER.info("Executed Step = START IF");
        if (check("ELE_LABEL_DEFAULTNONFOROPETION","//span[@id='workbenchForm:workbenchTabs:defaultNonForfOpt']","<>",var_SurrenderDividedToPayPremium1,"TGTYPESCREENREG")) {
         LOGGER.info("Executed Step = IF,ELE_LABEL_DEFAULTNONFOROPETION,<>,var_SurrenderDividedToPayPremium1,TGTYPESCREENREG");
		var_CheckSurrender = "FAIL";
                 LOGGER.info("Executed Step = STORE,var_CheckSurrender,FAIL,TGTYPESCREENREG");
        }
          LOGGER.info("Executed Step = ENDIF,TGTYPESCREENREG");
        SWIPE.$("DOWN","ELE_TAB_BASIC_INSURANCE_INFORMATION","//span[contains(text(),'Basic Insurance Information')]","TGTYPESCREENREG");

        SWIPE.$("DOWN","TGTYPESCREENREG");

        TAP.$("ELE_TAB_BASIC_INSURANCE_INFORMATION","//span[contains(text(),'Basic Insurance Information')]","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        LOGGER.info("Executed Step = START IF");
        if (check("ELE_LABEL__VALUE_AUTOMATIC_LAPSE_NFO","//span[@id='workbenchForm:workbenchTabs:automaticNFOProc']","<>",var_AutomaticLapseNonforfeiture1,"TGTYPESCREENREG")) {
         LOGGER.info("Executed Step = IF,ELE_LABEL__VALUE_AUTOMATIC_LAPSE_NFO,<>,var_AutomaticLapseNonforfeiture1,TGTYPESCREENREG");
		var_CheckSurrender = "FAIL";
                 LOGGER.info("Executed Step = STORE,var_CheckSurrender,FAIL,TGTYPESCREENREG");
        }
          LOGGER.info("Executed Step = ENDIF,TGTYPESCREENREG");
        LOGGER.info("Executed Step = START IF");
        if (check(var_CheckSurrender,"=","FAIL","TGTYPESCREENREG")) {
         LOGGER.info("Executed Step = IF,var_CheckSurrender,=,FAIL,TGTYPESCREENREG");
        TAP.$("ELE_BUTTON_EDIT","//button[@id='workbenchForm:workbenchTabs:tb_buttonUpdate']","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

		var_dataToPass = var_AutomaticLapseNonforfeiture1;
                 LOGGER.info("Executed Step = STORE,var_dataToPass,var_AutomaticLapseNonforfeiture1,TGTYPESCREENREG");
        TAP.$("ELE_DRPDWN_AUTOMATICLAPSE_NONFORFEITURE","//label[@id='workbenchForm:workbenchTabs:automaticNFOProc_label']","TGTYPESCREENREG");

		try { 
		 //li[@id='workbenchForm:workbenchTabs:automaticNFOProc_1']




			Thread.sleep(1000);
					for (int i = 0; i < 2000; i++){
						WebElement selectValueOfDropDown= driver.findElement(By.xpath("//li[@id='workbenchForm:workbenchTabs:automaticNFOProc_"+ i +"']"));

						if (selectValueOfDropDown.getText().contains(var_dataToPass)){
		AddToReport("ASSERT," + selectValueOfDropDown.getText() +", CONTAINS,"+var_dataToPass);
		                                selectValueOfDropDown.click();
							
						
							break;
						}
					}
					takeScreenshot();

					Thread.sleep(2000);
		 
		 ;} 
		 catch(Exception e) { PrintVal("Renderer Script Exception : "+e.toString()); } 
		 PrintVal("Executed Step = CALLRENDERERSCRIPT,SELECT_AUTOMATIC LAPSE_NONFORFEITURE "); 
        WAIT.$(5,"TGTYPESCREENREG");

        SWIPE.$("DOWN","TGTYPESCREENREG");

        SWIPE.$("DOWN","TGTYPESCREENREG");

        TAP.$("ELE_TAB_NONFORFEITURE","//span[contains(text(),'Nonforfeiture')]","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

		var_dataToPass = var_SurrenderDividedToPayPremium1;
                 LOGGER.info("Executed Step = STORE,var_dataToPass,var_SurrenderDividedToPayPremium1,TGTYPESCREENREG");
        TAP.$("ELE_DRPDWN_SURRENDERDIVIDENDSTOPAYPREMIUMS","//label[@id='workbenchForm:workbenchTabs:divSurrAlloForNFO_label']","TGTYPESCREENREG");

		try { 
		 //li[@id='workbenchForm:workbenchTabs:divSurrAlloForNFO_3']


			Thread.sleep(1000);
					for (int i = 0; i < 2000; i++){
						WebElement selectValueOfDropDown= driver.findElement(By.xpath("//li[@id='workbenchForm:workbenchTabs:divSurrAlloForNFO_"+ i +"']"));

						if (selectValueOfDropDown.getText().contains(var_dataToPass)){
		AddToReport("ASSERT," + selectValueOfDropDown.getText() +", CONTAINS,"+var_dataToPass);
		                                selectValueOfDropDown.click();
							
						
							break;
						}
					}
					takeScreenshot();

					Thread.sleep(2000);
		 
		 ;} 
		 catch(Exception e) { PrintVal("Renderer Script Exception : "+e.toString()); } 
		 PrintVal("Executed Step = CALLRENDERERSCRIPT,SELECT_SURRENDERDIVIDENDSTOPAYPREMIUMS "); 
        WAIT.$(5,"TGTYPESCREENREG");

		var_dataToPass = var_SurrenderDividedToPayPremium1;
                 LOGGER.info("Executed Step = STORE,var_dataToPass,var_SurrenderDividedToPayPremium1,TGTYPESCREENREG");
        TAP.$("ELE_DRPDWN_NONFORFEITUREOPTION","//label[@id='workbenchForm:workbenchTabs:defaultNonForfOpt_label']","TGTYPESCREENREG");

		try { 
		 //li[@id='workbenchForm:workbenchTabs:defaultNonForfOpt_5']


			Thread.sleep(1000);
					for (int i = 0; i < 2000; i++){
						WebElement selectValueOfDropDown= driver.findElement(By.xpath("//li[@id='workbenchForm:workbenchTabs:defaultNonForfOpt_"+ i +"']"));

						if (selectValueOfDropDown.getText().contains(var_dataToPass)){
		AddToReport("ASSERT," + selectValueOfDropDown.getText() +", CONTAINS,"+var_dataToPass);
		                                selectValueOfDropDown.click();
							
						
							break;
						}
					}
					takeScreenshot();

					Thread.sleep(2000);
		 
		 ;} 
		 catch(Exception e) { PrintVal("Renderer Script Exception : "+e.toString()); } 
		 PrintVal("Executed Step = CALLRENDERERSCRIPT,SELECT_DEFAIULT_OPETION_NONFOR"); 
        WAIT.$(5,"TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SAVE","//span[contains(text(),'Save')]","TGTYPESCREENREG");

        SWIPE.$("DOWN","TGTYPESCREENREG");

        SWIPE.$("DOWN","TGTYPESCREENREG");

        SWIPE.$("DOWN","TGTYPESCREENREG");

        ASSERT.$("ELE_MSG_SUCCESSFULLY","//span[contains(@class,'ui-messages-info-summary')]","VISIBLE","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        }
          LOGGER.info("Executed Step = ENDIF,TGTYPESCREENREG");
        TAP.$("ELE_BUTTON_GIAS_LOGO","//img[@id='headerForm:headerLogoImage']","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        CALL.$("ChangeSessionDate1","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_GIAS_LOGO","//img[@id='headerForm:headerLogoImage']","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

		String var_SessionDate1;
                 LOGGER.info("Executed Step = VAR,String,var_SessionDate1,TGTYPESCREENREG");
		var_SessionDate1 = getJsonData(var_CSVData , "$.records["+var_Count+"].SessionDate1");
                 LOGGER.info("Executed Step = STORE,var_SessionDate1,var_CSVData,$.records[{var_Count}].SessionDate1,TGTYPESCREENREG");
        TAP.$("ELE_BUTTON_SESSIONDATE","//a[@id='headerForm:sessionDatelink']","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_SESSIONDATE","//input[@id='sessionDateForm:sessionDateInput_input']",var_SessionDate1,"FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_CHANGEDATE","//span[contains(text(),'Change Date')]","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        CALL.$("NW056DsnCreateNewPolicy","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_GIAS_LOGO","//img[@id='headerForm:headerLogoImage']","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

		String var_PolicyNumber;
                 LOGGER.info("Executed Step = VAR,String,var_PolicyNumber,TGTYPESCREENREG");
		var_PolicyNumber = getJsonData(var_CSVData , "$.records["+var_Count+"].PolicyNumber");
                 LOGGER.info("Executed Step = STORE,var_PolicyNumber,var_CSVData,$.records[{var_Count}].PolicyNumber,TGTYPESCREENREG");
		String var_EffectiveDate;
                 LOGGER.info("Executed Step = VAR,String,var_EffectiveDate,TGTYPESCREENREG");
		var_EffectiveDate = getJsonData(var_CSVData , "$.records["+var_Count+"].EffectiveDate");
                 LOGGER.info("Executed Step = STORE,var_EffectiveDate,var_CSVData,$.records[{var_Count}].EffectiveDate,TGTYPESCREENREG");
		String var_CashWithApplication;
                 LOGGER.info("Executed Step = VAR,String,var_CashWithApplication,TGTYPESCREENREG");
		var_CashWithApplication = getJsonData(var_CSVData , "$.records["+var_Count+"].CashWithApplication");
                 LOGGER.info("Executed Step = STORE,var_CashWithApplication,var_CSVData,$.records[{var_Count}].CashWithApplication,TGTYPESCREENREG");
		String var_PaymentFrequency;
                 LOGGER.info("Executed Step = VAR,String,var_PaymentFrequency,TGTYPESCREENREG");
		var_PaymentFrequency = getJsonData(var_CSVData , "$.records["+var_Count+"].PaymentFrequency");
                 LOGGER.info("Executed Step = STORE,var_PaymentFrequency,var_CSVData,$.records[{var_Count}].PaymentFrequency,TGTYPESCREENREG");
		String var_PaymentMethod;
                 LOGGER.info("Executed Step = VAR,String,var_PaymentMethod,TGTYPESCREENREG");
		var_PaymentMethod = getJsonData(var_CSVData , "$.records["+var_Count+"].PaymentMethod");
                 LOGGER.info("Executed Step = STORE,var_PaymentMethod,var_CSVData,$.records[{var_Count}].PaymentMethod,TGTYPESCREENREG");
		String var_IssueState;
                 LOGGER.info("Executed Step = VAR,String,var_IssueState,TGTYPESCREENREG");
		var_IssueState = getJsonData(var_CSVData , "$.records["+var_Count+"].IssueState");
                 LOGGER.info("Executed Step = STORE,var_IssueState,var_CSVData,$.records[{var_Count}].IssueState,TGTYPESCREENREG");
		String var_AgentNumber;
                 LOGGER.info("Executed Step = VAR,String,var_AgentNumber,TGTYPESCREENREG");
		var_AgentNumber = getJsonData(var_CSVData , "$.records["+var_Count+"].AgentNumber");
                 LOGGER.info("Executed Step = STORE,var_AgentNumber,var_CSVData,$.records[{var_Count}].AgentNumber,TGTYPESCREENREG");
		String var_SplitPercentage;
                 LOGGER.info("Executed Step = VAR,String,var_SplitPercentage,TGTYPESCREENREG");
		var_SplitPercentage = getJsonData(var_CSVData , "$.records["+var_Count+"].SplitPercentage");
                 LOGGER.info("Executed Step = STORE,var_SplitPercentage,var_CSVData,$.records[{var_Count}].SplitPercentage,TGTYPESCREENREG");
		String var_ClientCheckName;
                 LOGGER.info("Executed Step = VAR,String,var_ClientCheckName,TGTYPESCREENREG");
		var_ClientCheckName = getJsonData(var_CSVData , "$.records["+var_Count+"].ClientCheckName");
                 LOGGER.info("Executed Step = STORE,var_ClientCheckName,var_CSVData,$.records[{var_Count}].ClientCheckName,TGTYPESCREENREG");
		String var_ClientFullName;
                 LOGGER.info("Executed Step = VAR,String,var_ClientFullName,TGTYPESCREENREG");
		var_ClientFullName = getJsonData(var_CSVData , "$.records["+var_Count+"].ClientFullName");
                 LOGGER.info("Executed Step = STORE,var_ClientFullName,var_CSVData,$.records[{var_Count}].ClientFullName,TGTYPESCREENREG");
		String var_ClientID;
                 LOGGER.info("Executed Step = VAR,String,var_ClientID,TGTYPESCREENREG");
		var_ClientID = getJsonData(var_CSVData , "$.records["+var_Count+"].ClientID");
                 LOGGER.info("Executed Step = STORE,var_ClientID,var_CSVData,$.records[{var_Count}].ClientID,TGTYPESCREENREG");
		String var_ClientBirthDate;
                 LOGGER.info("Executed Step = VAR,String,var_ClientBirthDate,TGTYPESCREENREG");
		var_ClientBirthDate = getJsonData(var_CSVData , "$.records["+var_Count+"].ClientBirthDate");
                 LOGGER.info("Executed Step = STORE,var_ClientBirthDate,var_CSVData,$.records[{var_Count}].ClientBirthDate,TGTYPESCREENREG");
        SWIPE.$("DOWN","ELE_TXTBOX_SEARCHTASKS","//input[@id='workbenchForm:workbenchTabs:taskSearchContainer:taskSearchSearch']","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_SEARCHTASKS","//input[@id='workbenchForm:workbenchTabs:taskSearchContainer:taskSearchSearch']","New Policy Application","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SEARCHTASKS","//a[@id='workbenchForm:workbenchTabs:taskSearchContainer:tb_buttonSearch']//span[@class='fa fa-search fa-lg']","TGTYPESCREENREG");

        WAIT.$("ELE_LINK_NEWPOLICYAPPLICATION","//span[contains(text(),'New Policy Application')]","VISIBLE","TGTYPESCREENREG");

        TAP.$("ELE_LINK_NEWPOLICYAPPLICATION","//span[contains(text(),'New Policy Application')]","TGTYPESCREENREG");

        WAIT.$("ELE_BUTTON_ADD","//button[@id='workbenchForm:workbenchTabs:tb_buttonAdd']//span[@class='ui-button-text ui-c'][contains(text(),'Add')]","VISIBLE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_ADD","//button[@id='workbenchForm:workbenchTabs:tb_buttonAdd']//span[@class='ui-button-text ui-c'][contains(text(),'Add')]","TGTYPESCREENREG");

        WAIT.$("ELE_TXTBOX_POLICYNUMBER","//input[@id='workbenchForm:workbenchTabs:policyNumber']","VISIBLE","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_POLICYNUMBER","//input[@id='workbenchForm:workbenchTabs:policyNumber']",var_PolicyNumber,"FALSE","TGTYPESCREENREG");

        TAP.$("ELE_DRPDWN_PAYMENTFREQUENCY","//label[@id='workbenchForm:workbenchTabs:paymentMode_label']","TGTYPESCREENREG");

		var_dataToPass = var_PaymentFrequency;
                 LOGGER.info("Executed Step = STORE,var_dataToPass,var_PaymentFrequency,TGTYPESCREENREG");
		try { 
		 Thread.sleep(1000);
		WebElement selectValueOfDropDown= driver.findElement(By.xpath("//li[@data-label='"+ var_dataToPass +"']"));

		takeScreenshot();

				selectValueOfDropDown.click();
		// Add Screenshot will only work for Android platform

		takeScreenshot();

				 Thread.sleep(1000);
		 
		 ;} 
		 catch(Exception e) { PrintVal("Renderer Script Exception : "+e.toString()); } 
		 PrintVal("Executed Step = CALLRENDERERSCRIPT,SELECTVALUEFROMDROPDOWN"); 
        WAIT.$(5,"TGTYPESCREENREG");

        TAP.$("ELE_DRPDWN_PAYMENTMETHOD","//label[@id='workbenchForm:workbenchTabs:paymentCode_label']","TGTYPESCREENREG");

		var_dataToPass = var_PaymentMethod;
                 LOGGER.info("Executed Step = STORE,var_dataToPass,var_PaymentMethod,TGTYPESCREENREG");
		try { 
		 Thread.sleep(1000);
		WebElement selectValueOfDropDown= driver.findElement(By.xpath("//li[@data-label='"+ var_dataToPass +"']"));

		takeScreenshot();

				selectValueOfDropDown.click();
		// Add Screenshot will only work for Android platform

		takeScreenshot();

				 Thread.sleep(1000);
		 
		 ;} 
		 catch(Exception e) { PrintVal("Renderer Script Exception : "+e.toString()); } 
		 PrintVal("Executed Step = CALLRENDERERSCRIPT,SELECTVALUEFROMDROPDOWN"); 
        WAIT.$(5,"TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_CASHWITHAPPLICATION","//input[@id='workbenchForm:workbenchTabs:cashWithApplication_input']",var_CashWithApplication,"FALSE","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_EFFECTIVEDATE","//input[@id='workbenchForm:workbenchTabs:effectiveDate_input']",var_EffectiveDate,"FALSE","TGTYPESCREENREG");

		var_dataToPass = var_IssueState;
                 LOGGER.info("Executed Step = STORE,var_dataToPass,var_IssueState,TGTYPESCREENREG");
        TAP.$("ELE_DRPDWN_ISSUESTATE","//label[@id='workbenchForm:workbenchTabs:countryAndStateCode_label']","TGTYPESCREENREG");

		try { 
		 Thread.sleep(1000);
		WebElement selectValueOfDropDown= driver.findElement(By.xpath("//li[@data-label='"+ var_dataToPass +"']"));

		takeScreenshot();

				selectValueOfDropDown.click();
		// Add Screenshot will only work for Android platform

		takeScreenshot();

				 Thread.sleep(1000);
		 
		 ;} 
		 catch(Exception e) { PrintVal("Renderer Script Exception : "+e.toString()); } 
		 PrintVal("Executed Step = CALLRENDERERSCRIPT,SELECTVALUEFROMDROPDOWN"); 
        WAIT.$(5,"TGTYPESCREENREG");

        SWIPE.$("UP","ELE_BUTTON_ADDAGENT_PLUS","//button[@id='workbenchForm:workbenchTabs:agentGrid:0:icon_button']","TGTYPESCREENREG");

        SWIPE.$("UP","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_ADDAGENT_PLUS","//button[@id='workbenchForm:workbenchTabs:agentGrid:0:icon_button']","TGTYPESCREENREG");

        WAIT.$("ELE_TXTBOX_AGENTNUMBER","//input[@id='workbenchForm:workbenchTabs:agentNumber']","VISIBLE","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_AGENTNUMBER","//input[@id='workbenchForm:workbenchTabs:agentNumber']",var_AgentNumber,"FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_AGENTSEARCH","//button[@id='workbenchForm:workbenchTabs:agentNumber_icon']","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_SPLITPERCENTAGE","//input[@id='workbenchForm:workbenchTabs:agentSplitPercentage_input']",var_SplitPercentage,"FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SAVE","//span[contains(text(),'Save')]","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SHOWLINKS","//button[@id='workbenchForm:workbenchTabs:tb_buttonLink']","TGTYPESCREENREG");

        TAP.$("ELE_LINK_SELECTCLIENT","//span[contains(text(),'Select Client')]","TGTYPESCREENREG");

        WAIT.$("ELE_TXTBOX_CLIENTSEARCHNAME","//input[@id='workbenchForm:workbenchTabs:searchName']","VISIBLE","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_CLIENTSEARCHNAME","//input[@id='workbenchForm:workbenchTabs:searchName']",var_ClientCheckName,"FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SEARCH","//button[@id='workbenchForm:workbenchTabs:button_search']","TGTYPESCREENREG");

        WAIT.$("ELE_TXTBOX_FILTER_CLIENTNAME","//input[@id='workbenchForm:workbenchTabs:clientGrid:individualName_Col:filter']","VISIBLE","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_FILTER_CLIENTID","//input[@id='workbenchForm:workbenchTabs:clientGrid:clientId_Col:filter']",var_ClientID,"FALSE","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        TAP.$("ELE_LINK_FIRST_CLIENT","//span[@id='workbenchForm:workbenchTabs:clientGrid:0:individualName']","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_TRUE","//button[@id='workbenchForm:workbenchTabs:button_select']//span[@class='ui-button-icon-left ui-icon ui-c fa fa-check']","TGTYPESCREENREG");

        WAIT.$("ELE_BUTTON_CONTINUE","//span[contains(text(),'Continue')]","VISIBLE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_CONTINUE","//span[contains(text(),'Continue')]","TGTYPESCREENREG");

        WAIT.$("ELE_LABEL_OWNERNAME","//span[@id='workbenchForm:workbenchTabs:ownerLabel']","VISIBLE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SAVE","//span[contains(text(),'Save')]","TGTYPESCREENREG");

        WAIT.$("ELE_MESSAGE_ITEMSUCCESFULLYADDED","//div[@id='workbenchForm:workbenchTabs:messages']//span[@class='ui-messages-info-summary'][contains(text(),'Item successfully added.')]","VISIBLE","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        CALL.$("NW056DsnBenefitPlanForPolicy","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

		String var_BenefitPlan;
                 LOGGER.info("Executed Step = VAR,String,var_BenefitPlan,TGTYPESCREENREG");
		var_BenefitPlan = getJsonData(var_CSVData , "$.records["+var_Count+"].BenefitPlan");
                 LOGGER.info("Executed Step = STORE,var_BenefitPlan,var_CSVData,$.records[{var_Count}].BenefitPlan,TGTYPESCREENREG");
		String var_Units;
                 LOGGER.info("Executed Step = VAR,String,var_Units,TGTYPESCREENREG");
		var_Units = getJsonData(var_CSVData , "$.records["+var_Count+"].Units");
                 LOGGER.info("Executed Step = STORE,var_Units,var_CSVData,$.records[{var_Count}].Units,TGTYPESCREENREG");
		String var_IssueAge;
                 LOGGER.info("Executed Step = VAR,String,var_IssueAge,TGTYPESCREENREG");
		var_IssueAge = getJsonData(var_CSVData , "$.records["+var_Count+"].IssueAge");
                 LOGGER.info("Executed Step = STORE,var_IssueAge,var_CSVData,$.records[{var_Count}].IssueAge,TGTYPESCREENREG");
		String var_RiskClass;
                 LOGGER.info("Executed Step = VAR,String,var_RiskClass,TGTYPESCREENREG");
		var_RiskClass = getJsonData(var_CSVData , "$.records["+var_Count+"].RiskClass");
                 LOGGER.info("Executed Step = STORE,var_RiskClass,var_CSVData,$.records[{var_Count}].RiskClass,TGTYPESCREENREG");
		String var_NonforfeitureOption;
                 LOGGER.info("Executed Step = VAR,String,var_NonforfeitureOption,TGTYPESCREENREG");
		var_NonforfeitureOption = getJsonData(var_CSVData , "$.records["+var_Count+"].NonforfeitureOption");
                 LOGGER.info("Executed Step = STORE,var_NonforfeitureOption,var_CSVData,$.records[{var_Count}].NonforfeitureOption,TGTYPESCREENREG");
		String var_PrimaryDividend;
                 LOGGER.info("Executed Step = VAR,String,var_PrimaryDividend,TGTYPESCREENREG");
		var_PrimaryDividend = getJsonData(var_CSVData , "$.records["+var_Count+"].PrimaryDividend");
                 LOGGER.info("Executed Step = STORE,var_PrimaryDividend,var_CSVData,$.records[{var_Count}].PrimaryDividend,TGTYPESCREENREG");
		String var_SecondaryDividend;
                 LOGGER.info("Executed Step = VAR,String,var_SecondaryDividend,TGTYPESCREENREG");
		var_SecondaryDividend = getJsonData(var_CSVData , "$.records["+var_Count+"].SecondaryDividend");
                 LOGGER.info("Executed Step = STORE,var_SecondaryDividend,var_CSVData,$.records[{var_Count}].SecondaryDividend,TGTYPESCREENREG");
        TAP.$("ELE_BUTTON_SHOWLINKS","//button[@id='workbenchForm:workbenchTabs:tb_buttonLink']","TGTYPESCREENREG");

        TAP.$("ELE_LINK_BENEFITS","//span[contains(text(),'Benefits')]","TGTYPESCREENREG");

        WAIT.$("ELE_DRPDWN_BENEFITPLAN","//label[@id='workbenchForm:workbenchTabs:initialPlanCode_label']","VISIBLE","TGTYPESCREENREG");

        TAP.$("ELE_DRPDWN_BENEFITPLAN","//label[@id='workbenchForm:workbenchTabs:initialPlanCode_label']","TGTYPESCREENREG");

		var_dataToPass = var_BenefitPlan;
                 LOGGER.info("Executed Step = STORE,var_dataToPass,var_BenefitPlan,TGTYPESCREENREG");
		try { 
		 Thread.sleep(1000);
		WebElement selectValueOfDropDown= driver.findElement(By.xpath("//li[@data-label='"+ var_dataToPass +"']"));

		takeScreenshot();

				selectValueOfDropDown.click();
		// Add Screenshot will only work for Android platform

		takeScreenshot();

				 Thread.sleep(1000);
		 
		 ;} 
		 catch(Exception e) { PrintVal("Renderer Script Exception : "+e.toString()); } 
		 PrintVal("Executed Step = CALLRENDERERSCRIPT,SELECTVALUEFROMDROPDOWN"); 
        WAIT.$(5,"TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_CONTINUE","//span[contains(text(),'Continue')]","TGTYPESCREENREG");

        WAIT.$("ELE_TXTBOX_UNITS","//input[@id='workbenchForm:workbenchTabs:unitsOfInsurance_input']","VISIBLE","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_UNITS","//input[@id='workbenchForm:workbenchTabs:unitsOfInsurance_input']",var_Units,"FALSE","TGTYPESCREENREG");

		var_dataToPass = var_RiskClass;
                 LOGGER.info("Executed Step = STORE,var_dataToPass,var_RiskClass,TGTYPESCREENREG");
        TAP.$("ELE_DRPDWN_RISK_CLASS","//label[@id='workbenchForm:workbenchTabs:smokerCode_label']","TGTYPESCREENREG");

		try { 
		 Thread.sleep(1000);
		WebElement selectValueOfDropDown= driver.findElement(By.xpath("//li[@data-label='"+ var_dataToPass +"']"));

		takeScreenshot();

				selectValueOfDropDown.click();
		// Add Screenshot will only work for Android platform

		takeScreenshot();

				 Thread.sleep(1000);
		 
		 ;} 
		 catch(Exception e) { PrintVal("Renderer Script Exception : "+e.toString()); } 
		 PrintVal("Executed Step = CALLRENDERERSCRIPT,SELECTVALUEFROMDROPDOWN"); 
        WAIT.$(5,"TGTYPESCREENREG");

		var_dataToPass = var_NonforfeitureOption;
                 LOGGER.info("Executed Step = STORE,var_dataToPass,var_NonforfeitureOption,TGTYPESCREENREG");
        TAP.$("ELE_DRPDWN_NONFORFEITUREOPTION","//label[@id='workbenchForm:workbenchTabs:defaultNonForfOpt_label']","TGTYPESCREENREG");

		try { 
		 Thread.sleep(1000);
		WebElement selectValueOfDropDown= driver.findElement(By.xpath("//li[@data-label='"+ var_dataToPass +"']"));

		takeScreenshot();

				selectValueOfDropDown.click();
		// Add Screenshot will only work for Android platform

		takeScreenshot();

				 Thread.sleep(1000);
		 
		 ;} 
		 catch(Exception e) { PrintVal("Renderer Script Exception : "+e.toString()); } 
		 PrintVal("Executed Step = CALLRENDERERSCRIPT,SELECTVALUEFROMDROPDOWN"); 
        WAIT.$(5,"TGTYPESCREENREG");

		var_dataToPass = var_PrimaryDividend;
                 LOGGER.info("Executed Step = STORE,var_dataToPass,var_PrimaryDividend,TGTYPESCREENREG");
        TAP.$("ELE_DRPDWN_PRIMARYDIVIDENDOPETION_POLICY","//label[@id='workbenchForm:workbenchTabs:primaryDividendOption_label']","TGTYPESCREENREG");

		try { 
		 Thread.sleep(1000);
		WebElement selectValueOfDropDown= driver.findElement(By.xpath("//li[@data-label='"+ var_dataToPass +"']"));

		takeScreenshot();

				selectValueOfDropDown.click();
		// Add Screenshot will only work for Android platform

		takeScreenshot();

				 Thread.sleep(1000);
		 
		 ;} 
		 catch(Exception e) { PrintVal("Renderer Script Exception : "+e.toString()); } 
		 PrintVal("Executed Step = CALLRENDERERSCRIPT,SELECTVALUEFROMDROPDOWN"); 
        WAIT.$(5,"TGTYPESCREENREG");

		var_dataToPass = var_SecondaryDividend;
                 LOGGER.info("Executed Step = STORE,var_dataToPass,var_SecondaryDividend,TGTYPESCREENREG");
        TAP.$("ELE_DRPDWN_SECONDARY_DIVIDEND","//label[@id='workbenchForm:workbenchTabs:secondaryDividendOption_label']","TGTYPESCREENREG");

		try { 
		 //li[@id='workbenchForm:workbenchTabs:secondaryDividendOption_0']



			Thread.sleep(1000);
					for (int i = 0; i < 2000; i++){
						WebElement selectValueOfDropDown= driver.findElement(By.xpath("//li[@id='workbenchForm:workbenchTabs:secondaryDividendOption_"+ i +"']"));

						if (selectValueOfDropDown.getText().contains(var_dataToPass)){
		AddToReport("ASSERT," + selectValueOfDropDown.getText() +", CONTAINS,"+var_dataToPass);
		                                selectValueOfDropDown.click();
							
						
							break;
						}
					}
					takeScreenshot();

					Thread.sleep(2000);
		 
		 ;} 
		 catch(Exception e) { PrintVal("Renderer Script Exception : "+e.toString()); } 
		 PrintVal("Executed Step = CALLRENDERERSCRIPT,SELECT_SECONDARYDIVIDENDOPTION"); 
        WAIT.$(5,"TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SAVE","//span[contains(text(),'Save')]","TGTYPESCREENREG");

        WAIT.$("ELE_MESSAGE_ITEMSUCCESFULLYADDED","//div[@id='workbenchForm:workbenchTabs:messages']//span[@class='ui-messages-info-summary'][contains(text(),'Item successfully added.')]","VISIBLE","TGTYPESCREENREG");

        TAP.$("ELE_TAB_CLOSE","//li[@class='ui-state-default ui-tabs-selected ui-state-active ui-corner-top']//span[@class='ui-icon ui-icon-close']","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        CALL.$("NW056DsnIssuePolicy","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SHOWLINKS","//button[@id='workbenchForm:workbenchTabs:tb_buttonLink']","TGTYPESCREENREG");

        TAP.$("ELE_LINK_ISSUE","//span[@class='ui-menuitem-text'][contains(text(),'Issue')]","TGTYPESCREENREG");

        WAIT.$("ELE_BUTTON_SUBMIT","//span[contains(text(),'Submit')]","VISIBLE","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SUBMIT","//span[contains(text(),'Submit')]","TGTYPESCREENREG");

        WAIT.$("ELE_MSG_REQUESTCOMPLETEDSUCCESSFULLY","//div[@id='workbenchForm:workbenchTabs:messages']//span[@class='ui-messages-info-summary'][contains(text(),'Request completed successfully.')]","VISIBLE","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        TAP.$("ELE_TAB_CLOSE","//li[@class='ui-state-default ui-tabs-selected ui-state-active ui-corner-top']//span[@class='ui-icon ui-icon-close']","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        CALL.$("NW056DsnSettlePolicy","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SHOWLINKS","//button[@id='workbenchForm:workbenchTabs:tb_buttonLink']","TGTYPESCREENREG");

        TAP.$("ELE_LINK_SETTLE","//span[contains(text(),'Settle')]","TGTYPESCREENREG");

        WAIT.$("ELE_BUTTON_SUBMIT","//span[contains(text(),'Submit')]","VISIBLE","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        SWIPE.$("DOWN","TGTYPESCREENREG");

        SWIPE.$("DOWN","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SUBMIT","//span[contains(text(),'Submit')]","TGTYPESCREENREG");

        WAIT.$(10,"TGTYPESCREENREG");

        WAIT.$("ELE_MSG_REQUESTCOMPLETEDSUCCESSFULLY","//div[@id='workbenchForm:workbenchTabs:messages']//span[@class='ui-messages-info-summary'][contains(text(),'Request completed successfully.')]","VISIBLE","TGTYPESCREENREG");

        WAIT.$(10,"TGTYPESCREENREG");

        TAP.$("ELE_TAB_CLOSE","//li[@class='ui-state-default ui-tabs-selected ui-state-active ui-corner-top']//span[@class='ui-icon ui-icon-close']","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        CALL.$("NW056DsnPremiumForPolicy","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

		String var_PremiumAnnual;
                 LOGGER.info("Executed Step = VAR,String,var_PremiumAnnual,TGTYPESCREENREG");
		var_PremiumAnnual = getJsonData(var_CSVData , "$.records["+var_Count+"].PremiumAnnual");
                 LOGGER.info("Executed Step = STORE,var_PremiumAnnual,var_CSVData,$.records[{var_Count}].PremiumAnnual,TGTYPESCREENREG");
		String var_PremiumSemiannual;
                 LOGGER.info("Executed Step = VAR,String,var_PremiumSemiannual,TGTYPESCREENREG");
		var_PremiumSemiannual = getJsonData(var_CSVData , "$.records["+var_Count+"].PremiumSemiannual");
                 LOGGER.info("Executed Step = STORE,var_PremiumSemiannual,var_CSVData,$.records[{var_Count}].PremiumSemiannual,TGTYPESCREENREG");
		String var_PremiumQuarterly;
                 LOGGER.info("Executed Step = VAR,String,var_PremiumQuarterly,TGTYPESCREENREG");
		var_PremiumQuarterly = getJsonData(var_CSVData , "$.records["+var_Count+"].PremiumQuarterly");
                 LOGGER.info("Executed Step = STORE,var_PremiumQuarterly,var_CSVData,$.records[{var_Count}].PremiumQuarterly,TGTYPESCREENREG");
		String var_PremiumMonthly;
                 LOGGER.info("Executed Step = VAR,String,var_PremiumMonthly,TGTYPESCREENREG");
		var_PremiumMonthly = getJsonData(var_CSVData , "$.records["+var_Count+"].PremiumMonthly");
                 LOGGER.info("Executed Step = STORE,var_PremiumMonthly,var_CSVData,$.records[{var_Count}].PremiumMonthly,TGTYPESCREENREG");
        TAP.$("ELE_BUTTON_SHOWLINKS","//button[@id='workbenchForm:workbenchTabs:tb_buttonLink']","TGTYPESCREENREG");

        TAP.$("ELE_LINK_PREMIUMS","//span[contains(text(),'Premiums')]","TGTYPESCREENREG");

        WAIT.$("ELE_LINK_FIRST_ANNUAL_PREMIUM","//span[@id='workbenchForm:workbenchTabs:gridNavGrid:0:benefitAnnualPremium']","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        ASSERT.$("ELE_LINK_FIRST_ANNUAL_PREMIUM","//span[@id='workbenchForm:workbenchTabs:gridNavGrid:0:benefitAnnualPremium']","CONTAINS",var_PremiumAnnual,"TGTYPESCREENREG");

        ASSERT.$("ELE_LINK_FIRST_SEMI_ANNUAL","//span[@id='workbenchForm:workbenchTabs:gridNavGrid:0:benefitSemiAnnuPrem']","CONTAINS",var_PremiumSemiannual,"TGTYPESCREENREG");

        ASSERT.$("ELE_LINK_FIRST_QUATERLY_PREMIUM","//span[@id='workbenchForm:workbenchTabs:gridNavGrid:0:benefitQuarterlyPrem']","CONTAINS",var_PremiumQuarterly,"TGTYPESCREENREG");

        ASSERT.$("ELE_LINK_FIRST_MONTHLY_PREMIUM","//span[@id='workbenchForm:workbenchTabs:gridNavGrid:0:benefitMonthlyPrem']","CONTAINS",var_PremiumMonthly,"TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_GIAS_LOGO","//img[@id='headerForm:headerLogoImage']","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_GIAS_LOGO","//img[@id='headerForm:headerLogoImage']","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        CALL.$("NW056DsnPolicyPayment","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_GIAS_LOGO","//img[@id='headerForm:headerLogoImage']","TGTYPESCREENREG");

		String var_PaidToDate2;
                 LOGGER.info("Executed Step = VAR,String,var_PaidToDate2,TGTYPESCREENREG");
		var_PaidToDate2 = getJsonData(var_CSVData , "$.records["+var_Count+"].PaidToDate2");
                 LOGGER.info("Executed Step = STORE,var_PaidToDate2,var_CSVData,$.records[{var_Count}].PaidToDate2,TGTYPESCREENREG");
		String var_ModalPremiums;
                 LOGGER.info("Executed Step = VAR,String,var_ModalPremiums,TGTYPESCREENREG");
		var_ModalPremiums = getJsonData(var_CSVData , "$.records["+var_Count+"].ModalPremiums");
                 LOGGER.info("Executed Step = STORE,var_ModalPremiums,var_CSVData,$.records[{var_Count}].ModalPremiums,TGTYPESCREENREG");
        SWIPE.$("DOWN","ELE_TXTBOX_SEARCHTASKS","//input[@id='workbenchForm:workbenchTabs:taskSearchContainer:taskSearchSearch']","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_SEARCHTASKS","//input[@id='workbenchForm:workbenchTabs:taskSearchContainer:taskSearchSearch']","Policy Payment","FALSE","TGTYPESCREENREG");

        WAIT.$(3,"TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SEARCHTASKS","//a[@id='workbenchForm:workbenchTabs:taskSearchContainer:tb_buttonSearch']//span[@class='fa fa-search fa-lg']","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        TAP.$("ELE_LINK_POLICYPAYMENT","//body/div[@id='Outer40']/div[@id='ContentPanel']/div[@id='Content40']/form[@id='workbenchForm']/div[@id='PageTiles']/div[@id='workbenchForm:workbenchTabs']/div[1]/div[1]/div[1]/div[2]/div[2]/div[1]/div[1]/div[1]/div[3]/div[1]/div[3]/button[1]/span[2]","TGTYPESCREENREG");

        WAIT.$("ELE_BUTTON_CONTINUE","//span[contains(text(),'Continue')]","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_POLICYNUMBER","//input[@id='workbenchForm:workbenchTabs:policyNumber']",var_PolicyNumber,"FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_CONTINUE","//span[contains(text(),'Continue')]","TGTYPESCREENREG");

        WAIT.$("ELE_BUTTON_SUBMIT","//span[contains(text(),'Submit')]","VISIBLE","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SUBMIT","//span[contains(text(),'Submit')]","TGTYPESCREENREG");

        WAIT.$("ELE_MSG_REQUESTCOMPLETEDSUCCESSFULLY","//div[@id='workbenchForm:workbenchTabs:messages']//span[@class='ui-messages-info-summary'][contains(text(),'Request completed successfully.')]","VISIBLE","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        TAP.$("ELE_TAB_CLOSE","//li[@class='ui-state-default ui-tabs-selected ui-state-active ui-corner-top']//span[@class='ui-icon ui-icon-close']","TGTYPESCREENREG");

        WAIT.$("ELE_BUTTON_CONTINUE","//span[contains(text(),'Continue')]","VISIBLE","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_CONTINUE","//span[contains(text(),'Continue')]","TGTYPESCREENREG");

        WAIT.$("ELE_BUTTON_SUBMIT","//span[contains(text(),'Submit')]","VISIBLE","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SUBMIT","//span[contains(text(),'Submit')]","TGTYPESCREENREG");

        WAIT.$("ELE_MSG_REQUESTCOMPLETEDSUCCESSFULLY","//div[@id='workbenchForm:workbenchTabs:messages']//span[@class='ui-messages-info-summary'][contains(text(),'Request completed successfully.')]","VISIBLE","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        TAP.$("ELE_TAB_CLOSE","//li[@class='ui-state-default ui-tabs-selected ui-state-active ui-corner-top']//span[@class='ui-icon ui-icon-close']","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_CONTINUE","//span[contains(text(),'Continue')]","TGTYPESCREENREG");

        WAIT.$("ELE_BUTTON_SUBMIT","//span[contains(text(),'Submit')]","VISIBLE","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SUBMIT","//span[contains(text(),'Submit')]","TGTYPESCREENREG");

        WAIT.$("ELE_MSG_REQUESTCOMPLETEDSUCCESSFULLY","//div[@id='workbenchForm:workbenchTabs:messages']//span[@class='ui-messages-info-summary'][contains(text(),'Request completed successfully.')]","VISIBLE","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        ASSERT.$("ELE_LABEL_PAIDTODATE","//span[@id='workbenchForm:workbenchTabs:paidToDate']","CONTAINS",var_PaidToDate2,"TGTYPESCREENREG");

        SWIPE.$("UP","ELE_LABEL_MODELPREMIUM","//span[@id='workbenchForm:workbenchTabs:modalPremium']","TGTYPESCREENREG");

        ASSERT.$("ELE_LABEL_MODELPREMIUM","//span[@id='workbenchForm:workbenchTabs:modalPremium']","CONTAINS",var_ModalPremiums,"TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_GIAS_LOGO","//img[@id='headerForm:headerLogoImage']","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        CALL.$("ChangeSessionDate2","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_GIAS_LOGO","//img[@id='headerForm:headerLogoImage']","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

		String var_SessionDate2;
                 LOGGER.info("Executed Step = VAR,String,var_SessionDate2,TGTYPESCREENREG");
		var_SessionDate2 = getJsonData(var_CSVData , "$.records["+var_Count+"].SessionDate2");
                 LOGGER.info("Executed Step = STORE,var_SessionDate2,var_CSVData,$.records[{var_Count}].SessionDate2,TGTYPESCREENREG");
        TAP.$("ELE_BUTTON_SESSIONDATE","//a[@id='headerForm:sessionDatelink']","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_SESSIONDATE","//input[@id='sessionDateForm:sessionDateInput_input']",var_SessionDate2,"FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_CHANGEDATE","//span[contains(text(),'Change Date')]","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        CALL.$("NW056DsnFirstPolicyInquiry","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_GIAS_LOGO","//img[@id='headerForm:headerLogoImage']","TGTYPESCREENREG");

		String var_LastDividendEarnedAmount;
                 LOGGER.info("Executed Step = VAR,String,var_LastDividendEarnedAmount,TGTYPESCREENREG");
		var_LastDividendEarnedAmount = getJsonData(var_CSVData , "$.records["+var_Count+"].LastDividendEarnedAmount");
                 LOGGER.info("Executed Step = STORE,var_LastDividendEarnedAmount,var_CSVData,$.records[{var_Count}].LastDividendEarnedAmount,TGTYPESCREENREG");
		String var_AccumulateWithDeposit;
                 LOGGER.info("Executed Step = VAR,String,var_AccumulateWithDeposit,TGTYPESCREENREG");
		var_AccumulateWithDeposit = getJsonData(var_CSVData , "$.records["+var_Count+"].AccumulateWithDeposit");
                 LOGGER.info("Executed Step = STORE,var_AccumulateWithDeposit,var_CSVData,$.records[{var_Count}].AccumulateWithDeposit,TGTYPESCREENREG");
		String var_LastDividendEarnedDate;
                 LOGGER.info("Executed Step = VAR,String,var_LastDividendEarnedDate,TGTYPESCREENREG");
		var_LastDividendEarnedDate = getJsonData(var_CSVData , "$.records["+var_Count+"].LastDividendEarnedDate");
                 LOGGER.info("Executed Step = STORE,var_LastDividendEarnedDate,var_CSVData,$.records[{var_Count}].LastDividendEarnedDate,TGTYPESCREENREG");
        SWIPE.$("DOWN","ELE_TXTBOX_SEARCHTASKS","//input[@id='workbenchForm:workbenchTabs:taskSearchContainer:taskSearchSearch']","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_SEARCHTASKS","//input[@id='workbenchForm:workbenchTabs:taskSearchContainer:taskSearchSearch']","Policy Inquiry","FALSE","TGTYPESCREENREG");

        WAIT.$(3,"TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SEARCHTASKS","//a[@id='workbenchForm:workbenchTabs:taskSearchContainer:tb_buttonSearch']//span[@class='fa fa-search fa-lg']","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        TAP.$("ELE_LINK_POLICYINQUIRY","//span[contains(text(),'Policy Inquiry')]","TGTYPESCREENREG");

        WAIT.$("ELE_TXTBOX_POLICYNUMBER","//input[@id='workbenchForm:workbenchTabs:policyNumber']","VISIBLE","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_POLICYNUMBER","//input[@id='workbenchForm:workbenchTabs:policyNumber']",var_PolicyNumber,"FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_CONTINUE","//span[contains(text(),'Continue')]","TGTYPESCREENREG");

        WAIT.$("ELE_LABEL_POLICYNUMBER","//span[@id='workbenchForm:workbenchTabs:policyNumber']","VISIBLE","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        SWIPE.$("UP","ELE_LABEL_LASTDIVIDENDEARNEDAMOUNT","//span[@id='workbenchForm:workbenchTabs:lastDividendAmount']","TGTYPESCREENREG");

        SWIPE.$("UP","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        ASSERT.$("ELE_LABEL_ACCUMULATIONSONDEPOSIT","//span[@id='workbenchForm:workbenchTabs:divCpnOnDepositAmt']","CONTAINS",var_AccumulateWithDeposit,"TGTYPESCREENREG");

        ASSERT.$("ELE_LABEL_LASTDIVIDENDDATE","//span[@id='workbenchForm:workbenchTabs:lastDividendDate']","CONTAINS",var_LastDividendEarnedDate,"TGTYPESCREENREG");

        ASSERT.$("ELE_LABEL_LASTDIVIDENDEARNEDAMOUNT","//span[@id='workbenchForm:workbenchTabs:lastDividendAmount']","CONTAINS",var_LastDividendEarnedAmount,"TGTYPESCREENREG");

        ASSERT.$("ELE_LABEL_PRIMARYOPETION_INQUIRY","//span[@id='workbenchForm:workbenchTabs:dividendOptionPrim']","CONTAINS",var_PrimaryDividend,"TGTYPESCREENREG");

        ASSERT.$("ELE_LABEL_SECOUNDARYOPETION_INQUIRY","//span[@id='workbenchForm:workbenchTabs:dividendOptionSeco']","CONTAINS",var_SecondaryDividend,"TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        WAIT.$(10,"TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_GIAS_LOGO","//img[@id='headerForm:headerLogoImage']","TGTYPESCREENREG");

        WAIT.$(10,"TGTYPESCREENREG");

        CALL.$("NW056DsnAdjustDividends","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_GIAS_LOGO","//img[@id='headerForm:headerLogoImage']","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

		String var_PaidUPTransactionAmount;
                 LOGGER.info("Executed Step = VAR,String,var_PaidUPTransactionAmount,TGTYPESCREENREG");
		var_PaidUPTransactionAmount = getJsonData(var_CSVData , "$.records["+var_Count+"].PaidUPTransactionAmount");
                 LOGGER.info("Executed Step = STORE,var_PaidUPTransactionAmount,var_CSVData,$.records[{var_Count}].PaidUPTransactionAmount,TGTYPESCREENREG");
		String var_PaidUPCalculate;
                 LOGGER.info("Executed Step = VAR,String,var_PaidUPCalculate,TGTYPESCREENREG");
		var_PaidUPCalculate = getJsonData(var_CSVData , "$.records["+var_Count+"].PaidUPCalculate");
                 LOGGER.info("Executed Step = STORE,var_PaidUPCalculate,var_CSVData,$.records[{var_Count}].PaidUPCalculate,TGTYPESCREENREG");
		String var_PaidUpDebitCardNumber;
                 LOGGER.info("Executed Step = VAR,String,var_PaidUpDebitCardNumber,TGTYPESCREENREG");
		var_PaidUpDebitCardNumber = getJsonData(var_CSVData , "$.records["+var_Count+"].PaidUpDebitCardNumber");
                 LOGGER.info("Executed Step = STORE,var_PaidUpDebitCardNumber,var_CSVData,$.records[{var_Count}].PaidUpDebitCardNumber,TGTYPESCREENREG");
		String var_PaidUpCreditCardNumber;
                 LOGGER.info("Executed Step = VAR,String,var_PaidUpCreditCardNumber,TGTYPESCREENREG");
		var_PaidUpCreditCardNumber = getJsonData(var_CSVData , "$.records["+var_Count+"].PaidUpCreditCardNumber");
                 LOGGER.info("Executed Step = STORE,var_PaidUpCreditCardNumber,var_CSVData,$.records[{var_Count}].PaidUpCreditCardNumber,TGTYPESCREENREG");
		String var_AccumulateionsTransactionAmount;
                 LOGGER.info("Executed Step = VAR,String,var_AccumulateionsTransactionAmount,TGTYPESCREENREG");
		var_AccumulateionsTransactionAmount = getJsonData(var_CSVData , "$.records["+var_Count+"].AccumulateionsTransactionAmount");
                 LOGGER.info("Executed Step = STORE,var_AccumulateionsTransactionAmount,var_CSVData,$.records[{var_Count}].AccumulateionsTransactionAmount,TGTYPESCREENREG");
		String var_AccumulateionsDebitCardNumber;
                 LOGGER.info("Executed Step = VAR,String,var_AccumulateionsDebitCardNumber,TGTYPESCREENREG");
		var_AccumulateionsDebitCardNumber = getJsonData(var_CSVData , "$.records["+var_Count+"].AccumulateionsDebitCardNumber");
                 LOGGER.info("Executed Step = STORE,var_AccumulateionsDebitCardNumber,var_CSVData,$.records[{var_Count}].AccumulateionsDebitCardNumber,TGTYPESCREENREG");
		String var_AccumulateionsCreditCardNumber;
                 LOGGER.info("Executed Step = VAR,String,var_AccumulateionsCreditCardNumber,TGTYPESCREENREG");
		var_AccumulateionsCreditCardNumber = getJsonData(var_CSVData , "$.records["+var_Count+"].AccumulateionsCreditCardNumber");
                 LOGGER.info("Executed Step = STORE,var_AccumulateionsCreditCardNumber,var_CSVData,$.records[{var_Count}].AccumulateionsCreditCardNumber,TGTYPESCREENREG");
        SWIPE.$("DOWN","ELE_TXTBOX_SEARCHTASKS","//input[@id='workbenchForm:workbenchTabs:taskSearchContainer:taskSearchSearch']","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_SEARCHTASKS","//input[@id='workbenchForm:workbenchTabs:taskSearchContainer:taskSearchSearch']","Adjust Dividends","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SEARCHTASKS","//a[@id='workbenchForm:workbenchTabs:taskSearchContainer:tb_buttonSearch']//span[@class='fa fa-search fa-lg']","TGTYPESCREENREG");

        WAIT.$("ELE_LINK_ADJUSTDIVIDENDSANDCOUPONS","//span[contains(text(),'Adjust Dividends and Coupons')]","VISIBLE","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        TAP.$("ELE_LINK_ADJUSTDIVIDENDSANDCOUPONS","//span[contains(text(),'Adjust Dividends and Coupons')]","TGTYPESCREENREG");

        WAIT.$("ELE_TXTBOX_POLICYNUMBER","//input[@id='workbenchForm:workbenchTabs:policyNumber']","VISIBLE","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_POLICYNUMBER","//input[@id='workbenchForm:workbenchTabs:policyNumber']",var_PolicyNumber,"FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_CONTINUE","//span[contains(text(),'Continue')]","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        WAIT.$("ELE_TXTBOX_TRANSACTIONAMOUNT_ONEYEARTERMADDITIONS","//input[@id='workbenchForm:workbenchTabs:year1TermAddiTransactionAmount_input']","VISIBLE","TGTYPESCREENREG");

        SWIPE.$("UP","ELE_TXTBOX_PAIDUPADDITIONAL_TRANSACTIONAMOUNT","//input[@id='workbenchForm:workbenchTabs:paidUpAdditionsTransactionAmount_input']","TGTYPESCREENREG");

        SWIPE.$("UP","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_PAIDUPADDITIONAL_TRANSACTIONAMOUNT","//input[@id='workbenchForm:workbenchTabs:paidUpAdditionsTransactionAmount_input']",var_PaidUPTransactionAmount,"FALSE","TGTYPESCREENREG");

		var_dataToPass = var_PaidUPCalculate;
                 LOGGER.info("Executed Step = STORE,var_dataToPass,var_PaidUPCalculate,TGTYPESCREENREG");
        TAP.$("ELE_DRPDWN_PAIDUPADDITIONALCALCULATE","//label[@id='workbenchForm:workbenchTabs:paidUpAdditionsCalculate_label']","TGTYPESCREENREG");

		try { 
		 //li[@id='workbenchForm:workbenchTabs:paidUpAdditionsCalculate_1']

			Thread.sleep(1000);
					for (int i = 0; i < 2000; i++){
						WebElement selectValueOfDropDown= driver.findElement(By.xpath("//li[@id='workbenchForm:workbenchTabs:paidUpAdditionsCalculate_"+ i +"']"));

						if (selectValueOfDropDown.getText().contains(var_dataToPass)){

		                                selectValueOfDropDown.click();
							AddToReport("ASSERT," + selectValueOfDropDown.getText() +", CONTAINS,"+var_dataToPass);
						
							break;
						}
					}
					takeScreenshot();

					Thread.sleep(2000);
		 
		 ;} 
		 catch(Exception e) { PrintVal("Renderer Script Exception : "+e.toString()); } 
		 PrintVal("Executed Step = CALLRENDERERSCRIPT,SELECT_CALCULATE_PAIDUP"); 
        WAIT.$(5,"TGTYPESCREENREG");

		var_dataToPass = var_PaidUpDebitCardNumber;
                 LOGGER.info("Executed Step = STORE,var_dataToPass,var_PaidUpDebitCardNumber,TGTYPESCREENREG");
        TAP.$("ELE_DRPDWN_PAIDUPADDITONAL_DEBITCARD","//label[@id='workbenchForm:workbenchTabs:paidUpAdditionsLedgerDebit_label']","TGTYPESCREENREG");

		try { 
		 //li[@id='workbenchForm:workbenchTabs:paidUpAdditionsLedgerDebit_1']


			Thread.sleep(1000);
					for (int i = 0; i < 2000; i++){
						WebElement selectValueOfDropDown= driver.findElement(By.xpath("//li[@id='workbenchForm:workbenchTabs:paidUpAdditionsLedgerDebit_"+ i +"']"));

						if (selectValueOfDropDown.getText().contains(var_dataToPass)){
		AddToReport("ASSERT," + selectValueOfDropDown.getText() +", CONTAINS,"+var_dataToPass);
		                                selectValueOfDropDown.click();
							
						
							break;
						}
					}
					takeScreenshot();

					Thread.sleep(2000);
		 
		 ;} 
		 catch(Exception e) { PrintVal("Renderer Script Exception : "+e.toString()); } 
		 PrintVal("Executed Step = CALLRENDERERSCRIPT,SELECT_DEBITCARDNUMBER_PAIDUP"); 
        WAIT.$(5,"TGTYPESCREENREG");

		var_dataToPass = var_PaidUpCreditCardNumber;
                 LOGGER.info("Executed Step = STORE,var_dataToPass,var_PaidUpCreditCardNumber,TGTYPESCREENREG");
        TAP.$("ELE_DRPDWN_PAIDUPADDITIONAL_CREDITCARD","//label[@id='workbenchForm:workbenchTabs:paidUpAdditionsLedgerCredit_label']","TGTYPESCREENREG");

		try { 
		 //li[@id='workbenchForm:workbenchTabs:paidUpAdditionsLedgerCredit_1']


			Thread.sleep(1000);
					for (int i = 0; i < 2000; i++){
						WebElement selectValueOfDropDown= driver.findElement(By.xpath("//li[@id='workbenchForm:workbenchTabs:paidUpAdditionsLedgerCredit_"+ i +"']"));

						if (selectValueOfDropDown.getText().contains(var_dataToPass)){
		AddToReport("ASSERT," + selectValueOfDropDown.getText() +", CONTAINS,"+var_dataToPass);
		                                selectValueOfDropDown.click();
							
						
							break;
						}
					}
					takeScreenshot();

					Thread.sleep(2000);
		 
		 ;} 
		 catch(Exception e) { PrintVal("Renderer Script Exception : "+e.toString()); } 
		 PrintVal("Executed Step = CALLRENDERERSCRIPT,SELECT_CREDITCARDNUMBER_PAIDUP"); 
        WAIT.$(5,"TGTYPESCREENREG");

        SWIPE.$("DOWN","ELE_TXTBOX_TRANSACTIONAMOUNT_ACCUMULATIONS","//input[@id='workbenchForm:workbenchTabs:accumulationTransactionAmount_input']","TGTYPESCREENREG");

        SWIPE.$("DOWN","TGTYPESCREENREG");

        SCROLL.$("ELE_TXTBOX_TRANSACTIONAMOUNT_ACCUMULATIONS","//input[@id='workbenchForm:workbenchTabs:accumulationTransactionAmount_input']","RIGHT","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_TRANSACTIONAMOUNT_ACCUMULATIONS","//input[@id='workbenchForm:workbenchTabs:accumulationTransactionAmount_input']",var_AccumulateionsTransactionAmount,"FALSE","TGTYPESCREENREG");

		var_dataToPass = var_AccumulateionsDebitCardNumber;
                 LOGGER.info("Executed Step = STORE,var_dataToPass,var_AccumulateionsDebitCardNumber,TGTYPESCREENREG");
        TAP.$("ELE_DRPDWN_DEBITACCOUNTNUMBER_ACCUMULATION","//label[@id='workbenchForm:workbenchTabs:accumulationLedgerDebit_label']","TGTYPESCREENREG");

		try { 
		 //li[@id='workbenchForm:workbenchTabs:accumulationLedgerDebit_1']


			Thread.sleep(1000);
					for (int i = 0; i < 2000; i++){
						WebElement selectValueOfDropDown= driver.findElement(By.xpath("//li[@id='workbenchForm:workbenchTabs:accumulationLedgerDebit_"+ i +"']"));

						if (selectValueOfDropDown.getText().contains(var_dataToPass)){
		AddToReport("ASSERT," + selectValueOfDropDown.getText() +", CONTAINS,"+var_dataToPass);
		                                selectValueOfDropDown.click();
							
						
							break;
						}
					}
					takeScreenshot();

					Thread.sleep(2000);
		 
		 ;} 
		 catch(Exception e) { PrintVal("Renderer Script Exception : "+e.toString()); } 
		 PrintVal("Executed Step = CALLRENDERERSCRIPT,SELECT_DEBITCARDNUMBER_ACCUMULATION"); 
        WAIT.$(5,"TGTYPESCREENREG");

		var_dataToPass = var_AccumulateionsCreditCardNumber;
                 LOGGER.info("Executed Step = STORE,var_dataToPass,var_AccumulateionsCreditCardNumber,TGTYPESCREENREG");
        TAP.$("ELE_DRPDWN_CREDITACCOUNTNUMBER_ACCUMULATION","//label[@id='workbenchForm:workbenchTabs:accumulationLedgerCredit_label']","TGTYPESCREENREG");

		try { 
		 //li[@id='workbenchForm:workbenchTabs:accumulationLedgerCredit_1']

			Thread.sleep(1000);
					for (int i = 0; i < 2000; i++){
						WebElement selectValueOfDropDown= driver.findElement(By.xpath("//li[@id='workbenchForm:workbenchTabs:accumulationLedgerCredit_"+ i +"']"));

						if (selectValueOfDropDown.getText().contains(var_dataToPass)){
		AddToReport("ASSERT," + selectValueOfDropDown.getText() +", CONTAINS,"+var_dataToPass);
		                                selectValueOfDropDown.click();
							
						
							break;
						}
					}
					takeScreenshot();

					Thread.sleep(2000);
		 
		 ;} 
		 catch(Exception e) { PrintVal("Renderer Script Exception : "+e.toString()); } 
		 PrintVal("Executed Step = CALLRENDERERSCRIPT,SELECT_CREDITCARDNUMBER_ACCUMULATION"); 
        WAIT.$(5,"TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SAVE","//span[contains(text(),'Save')]","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_GIAS_LOGO","//img[@id='headerForm:headerLogoImage']","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        CALL.$("ChangeSessionDate3","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_GIAS_LOGO","//img[@id='headerForm:headerLogoImage']","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

		String var_SessionDate3;
                 LOGGER.info("Executed Step = VAR,String,var_SessionDate3,TGTYPESCREENREG");
		var_SessionDate3 = getJsonData(var_CSVData , "$.records["+var_Count+"].SessionDate3");
                 LOGGER.info("Executed Step = STORE,var_SessionDate3,var_CSVData,$.records[{var_Count}].SessionDate3,TGTYPESCREENREG");
        TAP.$("ELE_BUTTON_SESSIONDATE","//a[@id='headerForm:sessionDatelink']","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_SESSIONDATE","//input[@id='sessionDateForm:sessionDateInput_input']",var_SessionDate3,"FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_CHANGEDATE","//span[contains(text(),'Change Date')]","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        CALL.$("DailyOfflineProcessingNW056","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_GIAS_LOGO","//img[@id='headerForm:headerLogoImage']","TGTYPESCREENREG");

		String var_StartingTask;
                 LOGGER.info("Executed Step = VAR,String,var_StartingTask,TGTYPESCREENREG");
		var_StartingTask = getJsonData(var_CSVData , "$.records["+var_Count+"].StartingTask");
                 LOGGER.info("Executed Step = STORE,var_StartingTask,var_CSVData,$.records[{var_Count}].StartingTask,TGTYPESCREENREG");
        SWIPE.$("DOWN","ELE_TXTBOX_SEARCHTASKS","//input[@id='workbenchForm:workbenchTabs:taskSearchContainer:taskSearchSearch']","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_SEARCHTASKS","//input[@id='workbenchForm:workbenchTabs:taskSearchContainer:taskSearchSearch']","Daily offline","FALSE","TGTYPESCREENREG");

        WAIT.$(3,"TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SEARCHTASKS","//a[@id='workbenchForm:workbenchTabs:taskSearchContainer:tb_buttonSearch']//span[@class='fa fa-search fa-lg']","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        TAP.$("ELE_LINK_DAILY_OFFLINE_PROCESSING","//span[contains(text(),'Daily Offline Processing')]","TGTYPESCREENREG");

        WAIT.$("ELE_BUTTON_SUBMIT","//span[contains(text(),'Submit')]","VISIBLE","TGTYPESCREENREG");

        TAP.$("ELE_DRPDWN_STARTINGTASK","//label[@id='workbenchForm:workbenchTabs:startingTask_label']","TGTYPESCREENREG");

		var_dataToPass = var_StartingTask;
                 LOGGER.info("Executed Step = STORE,var_dataToPass,var_StartingTask,TGTYPESCREENREG");
		try { 
		 //li[@id='workbenchForm:workbenchTabs:startingTask_49']


			Thread.sleep(1000);
					for (int i = 0; i < 2000; i++){
						WebElement selectValueOfDropDown= driver.findElement(By.xpath("//li[@id='workbenchForm:workbenchTabs:startingTask_"+ i +"']"));

						if (selectValueOfDropDown.getText().contains(var_dataToPass)){
		AddToReport("ASSERT," + selectValueOfDropDown.getText() +", CONTAINS,"+var_dataToPass);
		                                selectValueOfDropDown.click();
							
						
							break;
						}
					}
					takeScreenshot();

					Thread.sleep(2000);
		 
		 ;} 
		 catch(Exception e) { PrintVal("Renderer Script Exception : "+e.toString()); } 
		 PrintVal("Executed Step = CALLRENDERERSCRIPT,SELECT_STARTINGTASK"); 
        WAIT.$(5,"TGTYPESCREENREG");

        TAP.$("ELE_RADIO_BUTTON_NO_HOLDJOB","//body[1]/div[1]/div[2]/div[1]/form[1]/div[1]/div[2]/div[1]/div[2]/div[1]/table[1]/tbody[1]/tr[1]/td[2]/table[1]/tbody[1]/tr[1]/td[1]/fieldset[1]/div[1]/table[1]/tbody[1]/tr[5]/td[2]/table[1]/tbody[1]/tr[2]/td[1]/label[1]","TGTYPESCREENREG");

        WAIT.$(3,"TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SUBMIT","//span[contains(text(),'Submit')]","TGTYPESCREENREG");

        WAIT.$(10,"TGTYPESCREENREG");

        SWIPE.$("DOWN","TGTYPESCREENREG");

        ASSERT.$("ELE_MSG_SUCCESSFULLY","//span[contains(@class,'ui-messages-info-summary')]","VISIBLE","TGTYPESCREENREG");

        WAIT.$(10,"TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SHOWLINKS","//button[@id='workbenchForm:workbenchTabs:tb_buttonLink']","TGTYPESCREENREG");

        TAP.$("ELE_LINK_SUBMITTED_JOB_LOGS","//span[contains(text(),'Submitted Job Logs')]","TGTYPESCREENREG");

        WAIT.$("ELE_BUTTON_REFRESH","//span[contains(text(),'Refresh')]","VISIBLE","TGTYPESCREENREG");

        WAIT.$(30,"TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_REFRESH","//span[contains(text(),'Refresh')]","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        LOGGER.info("Executed Step = START IF");
        if (check("ELE_LINK_FIRST_JOB_STATUS","//span[@id='workbenchForm:workbenchTabs:grid:0:status']","CONTAINS","Active","TGTYPESCREENREG")) {
         LOGGER.info("Executed Step = IF,ELE_LINK_FIRST_JOB_STATUS,CONTAINS,Active,TGTYPESCREENREG");
        WAIT.$(45,"TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_REFRESH","//span[contains(text(),'Refresh')]","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        }
          LOGGER.info("Executed Step = ENDIF,TGTYPESCREENREG");
        WAIT.$(5,"TGTYPESCREENREG");

        WAIT.$(10,"TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_GIAS_LOGO","//img[@id='headerForm:headerLogoImage']","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        CALL.$("NW056DsnSecoundPolicyInquiry","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_GIAS_LOGO","//img[@id='headerForm:headerLogoImage']","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

		String var_User;
                 LOGGER.info("Executed Step = VAR,String,var_User,TGTYPESCREENREG");
		var_User = getJsonData(var_CSVData , "$.records["+var_Count+"].User");
                 LOGGER.info("Executed Step = STORE,var_User,var_CSVData,$.records[{var_Count}].User,TGTYPESCREENREG");
		String var_TransactionDescription1;
                 LOGGER.info("Executed Step = VAR,String,var_TransactionDescription1,TGTYPESCREENREG");
		var_TransactionDescription1 = getJsonData(var_CSVData , "$.records["+var_Count+"].TransactionDescription1");
                 LOGGER.info("Executed Step = STORE,var_TransactionDescription1,var_CSVData,$.records[{var_Count}].TransactionDescription1,TGTYPESCREENREG");
		String var_TransactionAmount1;
                 LOGGER.info("Executed Step = VAR,String,var_TransactionAmount1,TGTYPESCREENREG");
		var_TransactionAmount1 = getJsonData(var_CSVData , "$.records["+var_Count+"].TransactionAmount1");
                 LOGGER.info("Executed Step = STORE,var_TransactionAmount1,var_CSVData,$.records[{var_Count}].TransactionAmount1,TGTYPESCREENREG");
		String var_TransactionDescription2;
                 LOGGER.info("Executed Step = VAR,String,var_TransactionDescription2,TGTYPESCREENREG");
		var_TransactionDescription2 = getJsonData(var_CSVData , "$.records["+var_Count+"].TransactionDescription2");
                 LOGGER.info("Executed Step = STORE,var_TransactionDescription2,var_CSVData,$.records[{var_Count}].TransactionDescription2,TGTYPESCREENREG");
		String var_TransactionAmount2;
                 LOGGER.info("Executed Step = VAR,String,var_TransactionAmount2,TGTYPESCREENREG");
		var_TransactionAmount2 = getJsonData(var_CSVData , "$.records["+var_Count+"].TransactionAmount2");
                 LOGGER.info("Executed Step = STORE,var_TransactionAmount2,var_CSVData,$.records[{var_Count}].TransactionAmount2,TGTYPESCREENREG");
        SWIPE.$("DOWN","ELE_TXTBOX_SEARCHTASKS","//input[@id='workbenchForm:workbenchTabs:taskSearchContainer:taskSearchSearch']","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_SEARCHTASKS","//input[@id='workbenchForm:workbenchTabs:taskSearchContainer:taskSearchSearch']","Policy Inquiry","FALSE","TGTYPESCREENREG");

        WAIT.$(3,"TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SEARCHTASKS","//a[@id='workbenchForm:workbenchTabs:taskSearchContainer:tb_buttonSearch']//span[@class='fa fa-search fa-lg']","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        TAP.$("ELE_LINK_POLICYINQUIRY","//span[contains(text(),'Policy Inquiry')]","TGTYPESCREENREG");

        WAIT.$("ELE_TXTBOX_POLICYNUMBER","//input[@id='workbenchForm:workbenchTabs:policyNumber']","VISIBLE","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_POLICYNUMBER","//input[@id='workbenchForm:workbenchTabs:policyNumber']",var_PolicyNumber,"FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_CONTINUE","//span[contains(text(),'Continue')]","TGTYPESCREENREG");

        WAIT.$("ELE_LABEL_POLICYNUMBER","//span[@id='workbenchForm:workbenchTabs:policyNumber']","VISIBLE","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SHOWLINKS","//button[@id='workbenchForm:workbenchTabs:tb_buttonLink']","TGTYPESCREENREG");

        SWIPE.$("UP","ELE_LINK_TRANSACTIONHISTORY","//span[contains(text(),'Transaction History')]","TGTYPESCREENREG");

        SWIPE.$("UP","TGTYPESCREENREG");

        TAP.$("ELE_LINK_TRANSACTIONHISTORY","//span[contains(text(),'Transaction History')]","TGTYPESCREENREG");

        WAIT.$("ELE_LABEL_POLICYNUMBER","//span[@id='workbenchForm:workbenchTabs:policyNumber']","VISIBLE","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

		var_dataToPass = var_TransactionDescription1;
                 LOGGER.info("Executed Step = STORE,var_dataToPass,var_TransactionDescription1,TGTYPESCREENREG");
		String var_Amount;
                 LOGGER.info("Executed Step = VAR,String,var_Amount,TGTYPESCREENREG");
		var_Amount = var_TransactionAmount1;
                 LOGGER.info("Executed Step = STORE,var_Amount,var_TransactionAmount1,TGTYPESCREENREG");
		try { 
		 //span[@id='workbenchForm:workbenchTabs:grid:0:typeText']
		//span[@id='workbenchForm:workbenchTabs:grid:0:transactionAmountText']
		//span[@id='workbenchForm:workbenchTabs:grid:0:userID']

			Thread.sleep(1000);
					for (int i = 0; i < 10; i++){
						WebElement selectValueOfDropDown= driver.findElement(By.xpath("//span[@id='workbenchForm:workbenchTabs:grid:"+ i +":typeText']"));
		WebElement selectValueOfAmount= driver.findElement(By.xpath("//span[@id='workbenchForm:workbenchTabs:grid:"+ i +":transactionAmountText']"));

		WebElement ValueOfUser = driver.findElement(By.xpath("//span[@id='workbenchForm:workbenchTabs:grid:"+ i +":userID']"));

						if (selectValueOfDropDown.getText().contains(var_dataToPass) && selectValueOfAmount.getText().contains(var_Amount)){

		     
							AddToReport("ASSERT," + selectValueOfDropDown.getText() +", CONTAINS,"+var_dataToPass +" Amount = " + var_Amount +"User = " + var_User);
						
							break;
						}
					}
					takeScreenshot();

					Thread.sleep(2000);
		 
		 ;} 
		 catch(Exception e) { PrintVal("Renderer Script Exception : "+e.toString()); } 
		 PrintVal("Executed Step = CALLRENDERERSCRIPT,NW056DSNCHECKTRANSACTIONHISTORY"); 
		var_dataToPass = var_TransactionDescription2;
                 LOGGER.info("Executed Step = STORE,var_dataToPass,var_TransactionDescription2,TGTYPESCREENREG");
		var_Amount = var_TransactionAmount2;
                 LOGGER.info("Executed Step = STORE,var_Amount,var_TransactionAmount2,TGTYPESCREENREG");
		try { 
		 //span[@id='workbenchForm:workbenchTabs:grid:0:typeText']
		//span[@id='workbenchForm:workbenchTabs:grid:0:transactionAmountText']
		//span[@id='workbenchForm:workbenchTabs:grid:0:userID']

			Thread.sleep(1000);
					for (int i = 0; i < 10; i++){
						WebElement selectValueOfDropDown= driver.findElement(By.xpath("//span[@id='workbenchForm:workbenchTabs:grid:"+ i +":typeText']"));
		WebElement selectValueOfAmount= driver.findElement(By.xpath("//span[@id='workbenchForm:workbenchTabs:grid:"+ i +":transactionAmountText']"));

		WebElement ValueOfUser = driver.findElement(By.xpath("//span[@id='workbenchForm:workbenchTabs:grid:"+ i +":userID']"));

						if (selectValueOfDropDown.getText().contains(var_dataToPass) && selectValueOfAmount.getText().contains(var_Amount)){

		     
							AddToReport("ASSERT," + selectValueOfDropDown.getText() +", CONTAINS,"+var_dataToPass +" Amount = " + var_Amount +"User = " + var_User);
						
							break;
						}
					}
					takeScreenshot();

					Thread.sleep(2000);
		 
		 ;} 
		 catch(Exception e) { PrintVal("Renderer Script Exception : "+e.toString()); } 
		 PrintVal("Executed Step = CALLRENDERERSCRIPT,NW056DSNCHECKTRANSACTIONHISTORY"); 
        WAIT.$(10,"TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_GIAS_LOGO","//img[@id='headerForm:headerLogoImage']","TGTYPESCREENREG");

        WAIT.$(10,"TGTYPESCREENREG");

        CALL.$("NW056DsnCorrespondenceGenerated","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_GIAS_LOGO","//img[@id='headerForm:headerLogoImage']","TGTYPESCREENREG");

		String var_CorrespondenceType1;
                 LOGGER.info("Executed Step = VAR,String,var_CorrespondenceType1,TGTYPESCREENREG");
		var_CorrespondenceType1 = getJsonData(var_CSVData , "$.records["+var_Count+"].CorrespondenceType1");
                 LOGGER.info("Executed Step = STORE,var_CorrespondenceType1,var_CSVData,$.records[{var_Count}].CorrespondenceType1,TGTYPESCREENREG");
		String var_CorrespondenceType;
                 LOGGER.info("Executed Step = VAR,String,var_CorrespondenceType,TGTYPESCREENREG");
		var_CorrespondenceType = getJsonData(var_CSVData , "$.records["+var_Count+"].CorrespondenceType");
                 LOGGER.info("Executed Step = STORE,var_CorrespondenceType,var_CSVData,$.records[{var_Count}].CorrespondenceType,TGTYPESCREENREG");
		String var_CorrespondenceStatus;
                 LOGGER.info("Executed Step = VAR,String,var_CorrespondenceStatus,TGTYPESCREENREG");
		var_CorrespondenceStatus = getJsonData(var_CSVData , "$.records["+var_Count+"].CorrespondenceStatus");
                 LOGGER.info("Executed Step = STORE,var_CorrespondenceStatus,var_CSVData,$.records[{var_Count}].CorrespondenceStatus,TGTYPESCREENREG");
        SWIPE.$("DOWN","ELE_TXTBOX_SEARCHTASKS","//input[@id='workbenchForm:workbenchTabs:taskSearchContainer:taskSearchSearch']","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_SEARCHTASKS","//input[@id='workbenchForm:workbenchTabs:taskSearchContainer:taskSearchSearch']","Correspondence G","FALSE","TGTYPESCREENREG");

        WAIT.$(3,"TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SEARCHTASKS","//a[@id='workbenchForm:workbenchTabs:taskSearchContainer:tb_buttonSearch']//span[@class='fa fa-search fa-lg']","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        TAP.$("ELE_LINK_CORRESPONDENCE_GENERATED","//span[contains(text(),'Correspondence Generated')]","TGTYPESCREENREG");

        WAIT.$(10,"TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_LETTER_DATE","//input[@id='workbenchForm:workbenchTabs:grid:clientCorrLetterDate_filter_input']",var_SessionDate3,"FALSE","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_POLICYNUMBER_FILTER","//input[@id='workbenchForm:workbenchTabs:grid:policyNumber_Col:filter']",var_PolicyNumber,"FALSE","TGTYPESCREENREG");

        WAIT.$(10,"TGTYPESCREENREG");

		try { 
		 //span[@id='workbenchForm:workbenchTabs:grid:0:clientCorrType']
		//span[@id='workbenchForm:workbenchTabs:grid:0:policyNumber']
		//span[@id='workbenchForm:workbenchTabs:grid:0:clientExtrPrinFlag']
		//span[@id='workbenchForm:workbenchTabs:grid:1:clientCorrLetterDate']

			Thread.sleep(1000);
					for (int i = 0; i < 10; i++){
						WebElement selectValueOfCorrType = driver.findElement(By.xpath("//span[@id='workbenchForm:workbenchTabs:grid:"+ i +":clientCorrType']"));
		WebElement selectValueOfPolicyNumber= driver.findElement(By.xpath("//span[@id='workbenchForm:workbenchTabs:grid:"+ i +":policyNumber']"));

		WebElement selectValueOfPrinFlag = driver.findElement(By.xpath("//span[@id='workbenchForm:workbenchTabs:grid:"+ i +":clientExtrPrinFlag']"));
		WebElement selectValueOfLetterDate= driver.findElement(By.xpath("//span[@id='workbenchForm:workbenchTabs:grid:"+ i +":clientCorrLetterDate']"));

						if (selectValueOfCorrType.getText().contains(var_CorrespondenceType1) && selectValueOfPolicyNumber.getText().contains(var_PolicyNumber)){

		     
							AddToReport("ASSERT," + selectValueOfCorrType.getText() +", CONTAINS,"+var_CorrespondenceType1+" Policy Number = " + var_PolicyNumber +"  Status = " + var_CorrespondenceStatus +"fLetterDate = "+ selectValueOfLetterDate.getText());
						
							break;
						}
					}
					takeScreenshot();

					Thread.sleep(2000);
		 
		 ;} 
		 catch(Exception e) { PrintVal("Renderer Script Exception : "+e.toString()); } 
		 PrintVal("Executed Step = CALLRENDERERSCRIPT,NW056DSN_CHECK_CORRESPONDENCEGENERATED"); 
        WAIT.$(5,"TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_GIAS_LOGO","//img[@id='headerForm:headerLogoImage']","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

		try { 
		 

					java.sql.Connection con = null;
					// Load SQL Server JDBC driver and establish connection.
					String connectionUrl = "jdbc:sqlserver://CIS-giasdbt4:2433;" +
							"databaseName=NWT1DTA;"
							+
							"IntegratedSecurity = true";
					System.out.print("Connecting to SQL Server ... ");
					try {
						con = DriverManager.getConnection(connectionUrl);
					} catch (SQLException e) {
						System.out.println("SQLException to database.");
						e.getErrorCode();
						System.out.println("SQLException getErrorCode." + e.getErrorCode());
						e.printStackTrace();
					}
					System.out.println("Connected to database.");

					String sql = "SELECT TOP 1000 RECID , VERSIONID , companyCode , clientCorrType , clientCorrLetterDateYear , clientCorrLetterDateMonth , clientCorrLetterDateDay , userID , duplicateNotice , clientExtrPrinFlag , policyNumber , baseRiderCode , currencyCode , productCode , policyPrintDesc , riderPlanCode , riderPlanDescription , insuredNameCoprInd , insuredSexCode , insuredNamePrefix , insuredFirstName , insuredMiddleInitial , insuredLastName , insuredNameSuffix , insuredAddressLine1 , insuredAddressLine2 , insuredAddressLine3 , insuredCity , insuredState , insuredZipCode , insuredCountry , insuredSpecialHandle , insuredEmail , ownerNameCoprInd , ownerSexCode , ownerNamePrefix , ownerFirstName , ownerMiddleInitial , ownerLastName , ownerNameSuffix , ownerAddressLine1 , ownerAddressLine2 , ownerAddressLine3 , ownerCity , ownerState , ownerZipCode , ownerCountry , ownerSpecialHandle , ownerEmail , addtlNameCoprInd , addtlSexCode , addtlNamePrefix , addtlFirstName , addtlMiddleInitial , addtlLastName , addtlNameSuffix , addtlAddressLine1 , addtlAddressLine2 , addtlAddressLine3 , addtlCity , addtlState , addtlZipCode , addtlCountry , addtlSpecialHandle , addtlEmail , day1Year , day1Month , day1Day , day2Year , day2Month , day2Day , day3Year , day3Month , day3Day , day4Year , day4Month , day4Day , day5Year , day5Month , day5Day , type1 , type2 , type3 , type4 , type5 , description1 , description2 , description3 , faceAmount1 , faceAmount2 , faceAmount3 , faceAmount4 , amount1 , amount2 , amount3 , amount4 , amount5 , amount6 , amount7 , amount8 , amount9 , amount10 , amount11 , amount12 , amount13 , amount14 , amount15 , amount16 , amount17 , amount18 , amount19 , amount20 , interestRate1 , interestRate2 , interestRate3 , interestRate4 , interestRate5 , fundCode , fundId , fundType , nofUnits , sequenceNumber , correspondenceLang , formLetterNumber , groupNumber , participantIDNumber , identificationNumber , userName , userRoleDescription , applicationNumber , promotionCode , providerName , attentionLine , providerAddress1 , providerAddress2 , providerCity , providerState , providerZipCode , providerTelephoneNumber , providerFaxTelephoneNumber , providerPhoneCountryCode , providerEmailAddress , providerTaxIdNumber , providerNationalProviderId , julianYear , julianDay , claimSequenceNumber , claimSuffix , caseNumber , referralNumber FROM ClientCorrespondence WHERE policyNumber LIKE '"+var_PolicyNumber+"';";

					System.out.println("SQL Query" + sql);

					Statement statement = con.createStatement();
					System.out.println("statement to database.");
					ResultSet rs = null;

					ArrayList<String> clientCorrTypeFromDB  = new ArrayList<String>();
					ArrayList<String> clientCorrLetterDateYearFromDB  = new ArrayList<String>();
					ArrayList<String> policyNumberFromDB  = new ArrayList<String>();
					rs = statement.executeQuery(sql);
					System.out.println("result set to database.");
					System.out.println("rs  ===" + rs);
					System.out.println("rs  ===" + rs.toString());
					while (rs.next()) {

						if(rs.getString("clientCorrType").equals(var_CorrespondenceType)) {
							clientCorrTypeFromDB.add(rs.getString("clientCorrType"));
							clientCorrLetterDateYearFromDB.add(rs.getString("clientCorrLetterDateYear"));
							policyNumberFromDB.add(rs.getString("policyNumber"));
						}
					}
					statement.close();

					con.close();

					String clientCorrTypeFromDB1 = "";
					clientCorrTypeFromDB1 = clientCorrTypeFromDB.get(0);

					writeToCSV("Policy Number",var_PolicyNumber);
					writeToCSV("FieldName","ClientCorrType");
					writeToCSV("Value From DB",clientCorrTypeFromDB1);
					writeToCSV("Value From GIAS",var_CorrespondenceType);

					if (var_CorrespondenceType.contains(clientCorrTypeFromDB1) && clientCorrTypeFromDB1 != ""){
						writeToCSV("Status","PASS");
					}else{
						writeToCSV("Status","FAIL");
					}

					String clientCorrLetterDateYearFromDB1 = clientCorrLetterDateYearFromDB.get(0);
					String yearFromGIAS = var_SessionDate3.substring(var_SessionDate3.length() - 4);
					writeToCSV("Policy Number",var_PolicyNumber);
					writeToCSV("FieldName","ClientCorrLetterDateYear");
					writeToCSV("Value From DB",clientCorrLetterDateYearFromDB1);
					writeToCSV("Value From GIAS",yearFromGIAS);

					if (yearFromGIAS.contains(clientCorrLetterDateYearFromDB1) && clientCorrLetterDateYearFromDB1 != ""){
						writeToCSV("Status","PASS");
					}else{
						writeToCSV("Status","FAIL");
					}


					String policyNumberFromDB1 = policyNumberFromDB.get(0).trim();
					writeToCSV("Policy Number",var_PolicyNumber);
					writeToCSV("FieldName","PolicyNumber");
					writeToCSV("Value From DB",policyNumberFromDB1);
					writeToCSV("Value From GIAS",var_PolicyNumber);
					if (var_PolicyNumber.equalsIgnoreCase(policyNumberFromDB1) && policyNumberFromDB1 != ""){
						writeToCSV("Status","PASS");
					}else{
						writeToCSV("Status","FAIL");
					}




				
		 
		 ;} 
		 catch(Exception e) { PrintVal("Renderer Script Exception : "+e.toString()); } 
		 PrintVal("Executed Step = CALLRENDERERSCRIPT,NW056DSN_DATABASE_REPORT"); 
		var_CountN = var_CountN + ( 1 );
                 LOGGER.info("Executed Step = INCREASEVAR,var_CountN,1,TGTYPESCREENREG");
    	}
          LOGGER.info("Executed Step = WEND,TGTYPESCREENREG");
    	resetAttemps();

    }


    @Test
    public void nw020e2e() {

        // No need to declare / print method name here, handled by TestNg listener
        // declare("nw020e2e");

        CALL.$("LoginGIAS","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_USERNAME","//input[@id='guestForm:username']","autoapi","FALSE","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_PASSWORD","//input[@id='guestForm:password']","Autoapi!23","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_LOGIN","//button[contains(.,'Login')]","TGTYPESCREENREG");

        WAIT.$("ELE_BUTTON_GIAS_LOGO","//img[@id='headerForm:headerLogoImage']","VISIBLE","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

		String var_dataToPass;
                 LOGGER.info("Executed Step = VAR,String,var_dataToPass,TGTYPESCREENREG");
		int var_Count = 0;
                 LOGGER.info("Executed Step = VAR,Integer,var_Count,0,TGTYPESCREENREG");
		int var_CountN = 0;
                 LOGGER.info("Executed Step = VAR,Integer,var_CountN,0,TGTYPESCREENREG");
        LOGGER.info("Executed Step = REPEAT IF");
    	while (check(var_CountN,"<",1,"TGTYPESCREENREG")) {
          LOGGER.info("Executed Step = WHILE,var_CountN,<,1,TGTYPESCREENREG");
		String var_FileName = "http://10.1.104.229/json/20210524/w22zQj.json";
                 LOGGER.info("Executed Step = VAR,String,var_FileName,http://10.1.104.229/json/20210524/w22zQj.json,TGTYPESCREENREG");
		com.jayway.jsonpath.ReadContext var_CSVData = getJsonData("http://10.1.104.229/json/20210524/w22zQj.json");
                 LOGGER.info("Executed Step = VAR,Json,var_CSVData,http://10.1.104.229/json/20210524/w22zQj.json,TGTYPESCREENREG");
        CALL.$("PlanSetupForNW020E2E1","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_GIAS_LOGO","//img[@id='headerForm:headerLogoImage']","TGTYPESCREENREG");

		String var_BenefitPlanCode1;
                 LOGGER.info("Executed Step = VAR,String,var_BenefitPlanCode1,TGTYPESCREENREG");
		var_BenefitPlanCode1 = getJsonData(var_CSVData , "$.records["+var_Count+"].BenefitPlanCode1");
                 LOGGER.info("Executed Step = STORE,var_BenefitPlanCode1,var_CSVData,$.records[{var_Count}].BenefitPlanCode1,TGTYPESCREENREG");
		String var_SurrenderDividedToPayPremium1;
                 LOGGER.info("Executed Step = VAR,String,var_SurrenderDividedToPayPremium1,TGTYPESCREENREG");
        WAIT.$(5,"TGTYPESCREENREG");

        SWIPE.$("DOWN","ELE_TXTBOX_SEARCHTASKS","//input[@id='workbenchForm:workbenchTabs:taskSearchContainer:taskSearchSearch']","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_SEARCHTASKS","//input[@id='workbenchForm:workbenchTabs:taskSearchContainer:taskSearchSearch']","Plan Setup","FALSE","TGTYPESCREENREG");

        WAIT.$(3,"TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SEARCHTASKS","//a[@id='workbenchForm:workbenchTabs:taskSearchContainer:tb_buttonSearch']//span[@class='fa fa-search fa-lg']","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        TAP.$("ELE_LINK_FIRST_TASKS","//button[@id='workbenchForm:workbenchTabs:taskSearchContainer:0:availableTask']","TGTYPESCREENREG");

        WAIT.$("ELE_TXTBOX_FILTER_PLANCODE","//input[@id='workbenchForm:workbenchTabs:grid:planCode_Col:filter']","VISIBLE","TGTYPESCREENREG");

        WAIT.$(3,"TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_FILTER_PLANCODE","//input[@id='workbenchForm:workbenchTabs:grid:planCode_Col:filter']",var_BenefitPlanCode1,"FALSE","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        TAP.$("ELE_LINK_FIRST_PLANCODE","//span[@id='workbenchForm:workbenchTabs:grid:0:planCode']","TGTYPESCREENREG");

        WAIT.$("ELE_BUTTON_EDIT","//button[@id='workbenchForm:workbenchTabs:tb_buttonUpdate']","VISIBLE","TGTYPESCREENREG");

        TAP.$("ELE_LINK_DIVIDENDS","//span[contains(.,'Dividends')]","TGTYPESCREENREG");

        WAIT.$(4,"TGTYPESCREENREG");

        ASSERT.$("ELE_LABEL_REDUCEFIRSTPREMIUM","//label[@id='workbenchForm:workbenchTabs:reducePremiumAllowed_lbl']","VISIBLE","TGTYPESCREENREG");

        WAIT.$(3,"TGTYPESCREENREG");

        ASSERT.$("ELE_LABEL_REDUCEMODALPREMIUM","//label[@id='workbenchForm:workbenchTabs:reduceModalPremiumAllowed_lbl']","VISIBLE","TGTYPESCREENREG");

        WAIT.$(3,"TGTYPESCREENREG");

		String var_Option;
                 LOGGER.info("Executed Step = VAR,String,var_Option,TGTYPESCREENREG");
		var_Option = getJsonData(var_CSVData , "$.records["+var_Count+"].Option");
                 LOGGER.info("Executed Step = STORE,var_Option,var_CSVData,$.records[{var_Count}].Option,TGTYPESCREENREG");
        LOGGER.info("Executed Step = START IF");
        if (check("ELE_OPTION_REDUCEMODALPREMIUM","//span[@id='workbenchForm:workbenchTabs:reduceModalPremiumAllowed']","<>",var_Option,"TGTYPESCREENREG")) {
         LOGGER.info("Executed Step = IF,ELE_OPTION_REDUCEMODALPREMIUM,<>,var_Option,TGTYPESCREENREG");
		var_Option = "No";
                 LOGGER.info("Executed Step = STORE,var_Option,No,TGTYPESCREENREG");
        }
          LOGGER.info("Executed Step = ENDIF,TGTYPESCREENREG");
        LOGGER.info("Executed Step = START IF");
        if (check(var_Option,"=","No","TGTYPESCREENREG")) {
         LOGGER.info("Executed Step = IF,var_Option,=,No,TGTYPESCREENREG");
        TAP.$("ELE_BUTTON_EDIT","//button[@id='workbenchForm:workbenchTabs:tb_buttonUpdate']","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

		var_dataToPass = "Yes";
                 LOGGER.info("Executed Step = STORE,var_dataToPass,Yes,TGTYPESCREENREG");
        TAP.$("ELE_DRPDWN_REDUCEMODALPREMIUM","//label[@id='workbenchForm:workbenchTabs:reduceModalPremiumAllowed_label']","TGTYPESCREENREG");

		try { 
		 //li[@id='workbenchForm:workbenchTabs:reduceModalPremiumAllowed_1']
		//workbenchForm:workbenchTabs:reduceModalPremiumAllowed_1


			Thread.sleep(1000);
					for (int i = 0; i < 2000; i++){
						WebElement selectValueOfDropDown= driver.findElement(By.xpath("//li[@id='workbenchForm:workbenchTabs:reduceModalPremiumAllowed_"+ i +"']"));

						if (selectValueOfDropDown.getText().contains(var_dataToPass)){
		AddToReport("ASSERT," + selectValueOfDropDown.getText() +", CONTAINS,"+var_dataToPass);
		                                selectValueOfDropDown.click();
							
						
							break;
						}
					}
					takeScreenshot();

					Thread.sleep(2000);
		 
		 ;} 
		 catch(Exception e) { PrintVal("Renderer Script Exception : "+e.toString()); } 
		 PrintVal("Executed Step = CALLRENDERERSCRIPT,SELECT_REDUCEMODALPREMIUMOPTION"); 
        WAIT.$(5,"TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SAVE","//span[contains(text(),'Save')]","TGTYPESCREENREG");

        SWIPE.$("DOWN","TGTYPESCREENREG");

        SWIPE.$("DOWN","TGTYPESCREENREG");

        ASSERT.$("ELE_MSG_SUCCESSFULLY","//span[contains(@class,'ui-messages-info-summary')]","VISIBLE","TGTYPESCREENREG");

        }
          LOGGER.info("Executed Step = ENDIF,TGTYPESCREENREG");
        WAIT.$(5,"TGTYPESCREENREG");

        TAP.$("ELE_LINK_COUPONS","//span[@class='ui-menuitem-text'][contains(text(),'Coupons')]","TGTYPESCREENREG");

        WAIT.$("ELE_BUTTON_EDIT","//button[@id='workbenchForm:workbenchTabs:tb_buttonUpdate']","VISIBLE","TGTYPESCREENREG");

        ASSERT.$("ELE_LABEL_REDUCEFIRSTPREMIUM","//label[@id='workbenchForm:workbenchTabs:reducePremiumAllowed_lbl']","VISIBLE","TGTYPESCREENREG");

        WAIT.$(3,"TGTYPESCREENREG");

        ASSERT.$("ELE_LABEL_REDUCEMODALPREMIUM","//label[@id='workbenchForm:workbenchTabs:reduceModalPremiumAllowed_lbl']","VISIBLE","TGTYPESCREENREG");

		var_Option = getJsonData(var_CSVData , "$.records["+var_Count+"].Option");
                 LOGGER.info("Executed Step = STORE,var_Option,var_CSVData,$.records[{var_Count}].Option,TGTYPESCREENREG");
        LOGGER.info("Executed Step = START IF");
        if (check("ELE_OPTION_REDUCEMODALPREMIUM","//span[@id='workbenchForm:workbenchTabs:reduceModalPremiumAllowed']","<>",var_Option,"TGTYPESCREENREG")) {
         LOGGER.info("Executed Step = IF,ELE_OPTION_REDUCEMODALPREMIUM,<>,var_Option,TGTYPESCREENREG");
		var_Option = "No";
                 LOGGER.info("Executed Step = STORE,var_Option,No,TGTYPESCREENREG");
        }
          LOGGER.info("Executed Step = ENDIF,TGTYPESCREENREG");
        LOGGER.info("Executed Step = START IF");
        if (check(var_Option,"=","No","TGTYPESCREENREG")) {
         LOGGER.info("Executed Step = IF,var_Option,=,No,TGTYPESCREENREG");
        TAP.$("ELE_BUTTON_EDIT","//button[@id='workbenchForm:workbenchTabs:tb_buttonUpdate']","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

		var_dataToPass = getJsonData(var_CSVData , "$.records["+var_Count+"].Option");
                 LOGGER.info("Executed Step = STORE,var_dataToPass,var_CSVData,$.records[{var_Count}].Option,TGTYPESCREENREG");
        TAP.$("ELE_DRPDWN_REDUCEMODALPREMIUM","//label[@id='workbenchForm:workbenchTabs:reduceModalPremiumAllowed_label']","TGTYPESCREENREG");

        WAIT.$(3,"TGTYPESCREENREG");

		try { 
		 //li[@id='workbenchForm:workbenchTabs:reduceModalPremiumAllowed_1']
		//workbenchForm:workbenchTabs:reduceModalPremiumAllowed_1


			Thread.sleep(1000);
					for (int i = 0; i < 2000; i++){
						WebElement selectValueOfDropDown= driver.findElement(By.xpath("//li[@id='workbenchForm:workbenchTabs:reduceModalPremiumAllowed_"+ i +"']"));

						if (selectValueOfDropDown.getText().contains(var_dataToPass)){
		AddToReport("ASSERT," + selectValueOfDropDown.getText() +", CONTAINS,"+var_dataToPass);
		                                selectValueOfDropDown.click();
							
						
							break;
						}
					}
					takeScreenshot();

					Thread.sleep(2000);
		 
		 ;} 
		 catch(Exception e) { PrintVal("Renderer Script Exception : "+e.toString()); } 
		 PrintVal("Executed Step = CALLRENDERERSCRIPT,SELECT_REDUCEMODALPREMIUMOPTION"); 
        WAIT.$(5,"TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SAVE","//span[contains(text(),'Save')]","TGTYPESCREENREG");

        SWIPE.$("DOWN","TGTYPESCREENREG");

        SWIPE.$("DOWN","TGTYPESCREENREG");

        ASSERT.$("ELE_MSG_SUCCESSFULLY","//span[contains(@class,'ui-messages-info-summary')]","VISIBLE","TGTYPESCREENREG");

        }
          LOGGER.info("Executed Step = ENDIF,TGTYPESCREENREG");
        TAP.$("ELE_BUTTON_GIAS_LOGO","//img[@id='headerForm:headerLogoImage']","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        CALL.$("ChangeSessionDate1","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_GIAS_LOGO","//img[@id='headerForm:headerLogoImage']","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

		String var_SessionDate1;
                 LOGGER.info("Executed Step = VAR,String,var_SessionDate1,TGTYPESCREENREG");
		var_SessionDate1 = getJsonData(var_CSVData , "$.records["+var_Count+"].SessionDate1");
                 LOGGER.info("Executed Step = STORE,var_SessionDate1,var_CSVData,$.records[{var_Count}].SessionDate1,TGTYPESCREENREG");
        TAP.$("ELE_BUTTON_SESSIONDATE","//a[@id='headerForm:sessionDatelink']","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_SESSIONDATE","//input[@id='sessionDateForm:sessionDateInput_input']",var_SessionDate1,"FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_CHANGEDATE","//span[contains(text(),'Change Date')]","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        CALL.$("CreatePolicyForNW020E2E","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_GIAS_LOGO","//img[@id='headerForm:headerLogoImage']","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

		String var_PolicyNumber;
                 LOGGER.info("Executed Step = VAR,String,var_PolicyNumber,TGTYPESCREENREG");
		var_PolicyNumber = getJsonData(var_CSVData , "$.records["+var_Count+"].PolicyNumber");
                 LOGGER.info("Executed Step = STORE,var_PolicyNumber,var_CSVData,$.records[{var_Count}].PolicyNumber,TGTYPESCREENREG");
		String var_EffectiveDate;
                 LOGGER.info("Executed Step = VAR,String,var_EffectiveDate,TGTYPESCREENREG");
		var_EffectiveDate = getJsonData(var_CSVData , "$.records["+var_Count+"].EffectiveDate");
                 LOGGER.info("Executed Step = STORE,var_EffectiveDate,var_CSVData,$.records[{var_Count}].EffectiveDate,TGTYPESCREENREG");
		String var_CashWithApplication;
                 LOGGER.info("Executed Step = VAR,String,var_CashWithApplication,TGTYPESCREENREG");
		var_CashWithApplication = getJsonData(var_CSVData , "$.records["+var_Count+"].CashWithApplication");
                 LOGGER.info("Executed Step = STORE,var_CashWithApplication,var_CSVData,$.records[{var_Count}].CashWithApplication,TGTYPESCREENREG");
		String var_PaymentFrequency;
                 LOGGER.info("Executed Step = VAR,String,var_PaymentFrequency,TGTYPESCREENREG");
		var_PaymentFrequency = getJsonData(var_CSVData , "$.records["+var_Count+"].PaymentFrequency");
                 LOGGER.info("Executed Step = STORE,var_PaymentFrequency,var_CSVData,$.records[{var_Count}].PaymentFrequency,TGTYPESCREENREG");
		String var_PaymentMethod;
                 LOGGER.info("Executed Step = VAR,String,var_PaymentMethod,TGTYPESCREENREG");
		var_PaymentMethod = getJsonData(var_CSVData , "$.records["+var_Count+"].PaymentMethod");
                 LOGGER.info("Executed Step = STORE,var_PaymentMethod,var_CSVData,$.records[{var_Count}].PaymentMethod,TGTYPESCREENREG");
		String var_IssueState;
                 LOGGER.info("Executed Step = VAR,String,var_IssueState,TGTYPESCREENREG");
		var_IssueState = getJsonData(var_CSVData , "$.records["+var_Count+"].IssueState");
                 LOGGER.info("Executed Step = STORE,var_IssueState,var_CSVData,$.records[{var_Count}].IssueState,TGTYPESCREENREG");
		String var_AgentNumber;
                 LOGGER.info("Executed Step = VAR,String,var_AgentNumber,TGTYPESCREENREG");
		var_AgentNumber = getJsonData(var_CSVData , "$.records["+var_Count+"].AgentNumber");
                 LOGGER.info("Executed Step = STORE,var_AgentNumber,var_CSVData,$.records[{var_Count}].AgentNumber,TGTYPESCREENREG");
		String var_SplitPercentage;
                 LOGGER.info("Executed Step = VAR,String,var_SplitPercentage,TGTYPESCREENREG");
		var_SplitPercentage = getJsonData(var_CSVData , "$.records["+var_Count+"].SplitPercentage");
                 LOGGER.info("Executed Step = STORE,var_SplitPercentage,var_CSVData,$.records[{var_Count}].SplitPercentage,TGTYPESCREENREG");
		String var_ClientCheckName;
                 LOGGER.info("Executed Step = VAR,String,var_ClientCheckName,TGTYPESCREENREG");
		var_ClientCheckName = getJsonData(var_CSVData , "$.records["+var_Count+"].ClientCheckName");
                 LOGGER.info("Executed Step = STORE,var_ClientCheckName,var_CSVData,$.records[{var_Count}].ClientCheckName,TGTYPESCREENREG");
		String var_ClientFullName;
                 LOGGER.info("Executed Step = VAR,String,var_ClientFullName,TGTYPESCREENREG");
		var_ClientFullName = getJsonData(var_CSVData , "$.records["+var_Count+"].ClientFullName");
                 LOGGER.info("Executed Step = STORE,var_ClientFullName,var_CSVData,$.records[{var_Count}].ClientFullName,TGTYPESCREENREG");
		String var_ClientID;
                 LOGGER.info("Executed Step = VAR,String,var_ClientID,TGTYPESCREENREG");
		var_ClientID = getJsonData(var_CSVData , "$.records["+var_Count+"].ClientID");
                 LOGGER.info("Executed Step = STORE,var_ClientID,var_CSVData,$.records[{var_Count}].ClientID,TGTYPESCREENREG");
		String var_ClientBirthDate;
                 LOGGER.info("Executed Step = VAR,String,var_ClientBirthDate,TGTYPESCREENREG");
		var_ClientBirthDate = getJsonData(var_CSVData , "$.records["+var_Count+"].ClientBirthDate");
                 LOGGER.info("Executed Step = STORE,var_ClientBirthDate,var_CSVData,$.records[{var_Count}].ClientBirthDate,TGTYPESCREENREG");
        SWIPE.$("DOWN","ELE_TXTBOX_SEARCHTASKS","//input[@id='workbenchForm:workbenchTabs:taskSearchContainer:taskSearchSearch']","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_SEARCHTASKS","//input[@id='workbenchForm:workbenchTabs:taskSearchContainer:taskSearchSearch']","New Policy Application","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SEARCHTASKS","//a[@id='workbenchForm:workbenchTabs:taskSearchContainer:tb_buttonSearch']//span[@class='fa fa-search fa-lg']","TGTYPESCREENREG");

        WAIT.$("ELE_LINK_NEWPOLICYAPPLICATION","//span[contains(text(),'New Policy Application')]","VISIBLE","TGTYPESCREENREG");

        TAP.$("ELE_LINK_NEWPOLICYAPPLICATION","//span[contains(text(),'New Policy Application')]","TGTYPESCREENREG");

        WAIT.$("ELE_BUTTON_ADD","//button[@id='workbenchForm:workbenchTabs:tb_buttonAdd']//span[@class='ui-button-text ui-c'][contains(text(),'Add')]","VISIBLE","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_ADD","//button[@id='workbenchForm:workbenchTabs:tb_buttonAdd']//span[@class='ui-button-text ui-c'][contains(text(),'Add')]","TGTYPESCREENREG");

        WAIT.$("ELE_TXTBOX_POLICYNUMBER","//input[@id='workbenchForm:workbenchTabs:policyNumber']","VISIBLE","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_POLICYNUMBER","//input[@id='workbenchForm:workbenchTabs:policyNumber']",var_PolicyNumber,"FALSE","TGTYPESCREENREG");

        TAP.$("ELE_DRPDWN_PAYMENTFREQUENCY","//label[@id='workbenchForm:workbenchTabs:paymentMode_label']","TGTYPESCREENREG");

		var_dataToPass = var_PaymentFrequency;
                 LOGGER.info("Executed Step = STORE,var_dataToPass,var_PaymentFrequency,TGTYPESCREENREG");
		try { 
		 Thread.sleep(1000);
		WebElement selectValueOfDropDown= driver.findElement(By.xpath("//li[@data-label='"+ var_dataToPass +"']"));

		takeScreenshot();

				selectValueOfDropDown.click();
		// Add Screenshot will only work for Android platform

		takeScreenshot();

				 Thread.sleep(1000);
		 
		 ;} 
		 catch(Exception e) { PrintVal("Renderer Script Exception : "+e.toString()); } 
		 PrintVal("Executed Step = CALLRENDERERSCRIPT,SELECTVALUEFROMDROPDOWN"); 
        WAIT.$(5,"TGTYPESCREENREG");

        TAP.$("ELE_DRPDWN_PAYMENTMETHOD","//label[@id='workbenchForm:workbenchTabs:paymentCode_label']","TGTYPESCREENREG");

		var_dataToPass = var_PaymentMethod;
                 LOGGER.info("Executed Step = STORE,var_dataToPass,var_PaymentMethod,TGTYPESCREENREG");
		try { 
		 Thread.sleep(1000);
		WebElement selectValueOfDropDown= driver.findElement(By.xpath("//li[@data-label='"+ var_dataToPass +"']"));

		takeScreenshot();

				selectValueOfDropDown.click();
		// Add Screenshot will only work for Android platform

		takeScreenshot();

				 Thread.sleep(1000);
		 
		 ;} 
		 catch(Exception e) { PrintVal("Renderer Script Exception : "+e.toString()); } 
		 PrintVal("Executed Step = CALLRENDERERSCRIPT,SELECTVALUEFROMDROPDOWN"); 
        WAIT.$(5,"TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_CASHWITHAPPLICATION","//input[@id='workbenchForm:workbenchTabs:cashWithApplication_input']",var_CashWithApplication,"FALSE","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_EFFECTIVEDATE","//input[@id='workbenchForm:workbenchTabs:effectiveDate_input']",var_EffectiveDate,"FALSE","TGTYPESCREENREG");

		var_dataToPass = var_IssueState;
                 LOGGER.info("Executed Step = STORE,var_dataToPass,var_IssueState,TGTYPESCREENREG");
        TAP.$("ELE_DRPDWN_ISSUESTATE","//label[@id='workbenchForm:workbenchTabs:countryAndStateCode_label']","TGTYPESCREENREG");

		try { 
		 Thread.sleep(1000);
		WebElement selectValueOfDropDown= driver.findElement(By.xpath("//li[@data-label='"+ var_dataToPass +"']"));

		takeScreenshot();

				selectValueOfDropDown.click();
		// Add Screenshot will only work for Android platform

		takeScreenshot();

				 Thread.sleep(1000);
		 
		 ;} 
		 catch(Exception e) { PrintVal("Renderer Script Exception : "+e.toString()); } 
		 PrintVal("Executed Step = CALLRENDERERSCRIPT,SELECTVALUEFROMDROPDOWN"); 
        WAIT.$(5,"TGTYPESCREENREG");

        SWIPE.$("UP","ELE_BUTTON_ADDAGENT_PLUS","//button[@id='workbenchForm:workbenchTabs:agentGrid:0:icon_button']","TGTYPESCREENREG");

        SWIPE.$("UP","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_ADDAGENT_PLUS","//button[@id='workbenchForm:workbenchTabs:agentGrid:0:icon_button']","TGTYPESCREENREG");

        WAIT.$("ELE_TXTBOX_AGENTNUMBER","//input[@id='workbenchForm:workbenchTabs:agentNumber']","VISIBLE","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_AGENTNUMBER","//input[@id='workbenchForm:workbenchTabs:agentNumber']",var_AgentNumber,"FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_AGENTSEARCH","//button[@id='workbenchForm:workbenchTabs:agentNumber_icon']","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_SPLITPERCENTAGE","//input[@id='workbenchForm:workbenchTabs:agentSplitPercentage_input']",var_SplitPercentage,"FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SAVE","//span[contains(text(),'Save')]","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SHOWLINKS","//button[@id='workbenchForm:workbenchTabs:tb_buttonLink']","TGTYPESCREENREG");

        TAP.$("ELE_LINK_SELECTCLIENT","//span[contains(text(),'Select Client')]","TGTYPESCREENREG");

        WAIT.$("ELE_TXTBOX_CLIENTSEARCHNAME","//input[@id='workbenchForm:workbenchTabs:searchName']","VISIBLE","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_CLIENTSEARCHNAME","//input[@id='workbenchForm:workbenchTabs:searchName']",var_ClientCheckName,"FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SEARCH","//button[@id='workbenchForm:workbenchTabs:button_search']","TGTYPESCREENREG");

        WAIT.$("ELE_TXTBOX_FILTER_CLIENTNAME","//input[@id='workbenchForm:workbenchTabs:clientGrid:individualName_Col:filter']","VISIBLE","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_FILTER_CLIENTID","//input[@id='workbenchForm:workbenchTabs:clientGrid:clientId_Col:filter']",var_ClientID,"FALSE","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        TAP.$("ELE_LINK_FIRST_CLIENT","//span[@id='workbenchForm:workbenchTabs:clientGrid:0:individualName']","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_TRUE","//button[@id='workbenchForm:workbenchTabs:button_select']//span[@class='ui-button-icon-left ui-icon ui-c fa fa-check']","TGTYPESCREENREG");

        WAIT.$("ELE_BUTTON_CONTINUE","//span[contains(text(),'Continue')]","VISIBLE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_CONTINUE","//span[contains(text(),'Continue')]","TGTYPESCREENREG");

        WAIT.$("ELE_LABEL_OWNERNAME","//span[@id='workbenchForm:workbenchTabs:ownerLabel']","VISIBLE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SAVE","//span[contains(text(),'Save')]","TGTYPESCREENREG");

        WAIT.$("ELE_MESSAGE_ITEMSUCCESFULLYADDED","//div[@id='workbenchForm:workbenchTabs:messages']//span[@class='ui-messages-info-summary'][contains(text(),'Item successfully added.')]","VISIBLE","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        CALL.$("BenefitPlanForNW020E2E","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

		String var_BenefitPlan;
                 LOGGER.info("Executed Step = VAR,String,var_BenefitPlan,TGTYPESCREENREG");
		var_BenefitPlan = getJsonData(var_CSVData , "$.records["+var_Count+"].BenefitPlan");
                 LOGGER.info("Executed Step = STORE,var_BenefitPlan,var_CSVData,$.records[{var_Count}].BenefitPlan,TGTYPESCREENREG");
		String var_Units;
                 LOGGER.info("Executed Step = VAR,String,var_Units,TGTYPESCREENREG");
		var_Units = getJsonData(var_CSVData , "$.records["+var_Count+"].Units");
                 LOGGER.info("Executed Step = STORE,var_Units,var_CSVData,$.records[{var_Count}].Units,TGTYPESCREENREG");
		String var_IssueAge;
                 LOGGER.info("Executed Step = VAR,String,var_IssueAge,TGTYPESCREENREG");
		var_IssueAge = getJsonData(var_CSVData , "$.records["+var_Count+"].IssueAge");
                 LOGGER.info("Executed Step = STORE,var_IssueAge,var_CSVData,$.records[{var_Count}].IssueAge,TGTYPESCREENREG");
		String var_RiskClass;
                 LOGGER.info("Executed Step = VAR,String,var_RiskClass,TGTYPESCREENREG");
		var_RiskClass = getJsonData(var_CSVData , "$.records["+var_Count+"].RiskClass");
                 LOGGER.info("Executed Step = STORE,var_RiskClass,var_CSVData,$.records[{var_Count}].RiskClass,TGTYPESCREENREG");
		String var_NonforfeitureOption;
                 LOGGER.info("Executed Step = VAR,String,var_NonforfeitureOption,TGTYPESCREENREG");
		var_NonforfeitureOption = getJsonData(var_CSVData , "$.records["+var_Count+"].NonforfeitureOption");
                 LOGGER.info("Executed Step = STORE,var_NonforfeitureOption,var_CSVData,$.records[{var_Count}].NonforfeitureOption,TGTYPESCREENREG");
		String var_PrimaryDividend;
                 LOGGER.info("Executed Step = VAR,String,var_PrimaryDividend,TGTYPESCREENREG");
		var_PrimaryDividend = getJsonData(var_CSVData , "$.records["+var_Count+"].PrimaryDividend");
                 LOGGER.info("Executed Step = STORE,var_PrimaryDividend,var_CSVData,$.records[{var_Count}].PrimaryDividend,TGTYPESCREENREG");
		String var_SecondaryDividend;
                 LOGGER.info("Executed Step = VAR,String,var_SecondaryDividend,TGTYPESCREENREG");
		var_SecondaryDividend = getJsonData(var_CSVData , "$.records["+var_Count+"].SecondaryDividend");
                 LOGGER.info("Executed Step = STORE,var_SecondaryDividend,var_CSVData,$.records[{var_Count}].SecondaryDividend,TGTYPESCREENREG");
        TAP.$("ELE_BUTTON_SHOWLINKS","//button[@id='workbenchForm:workbenchTabs:tb_buttonLink']","TGTYPESCREENREG");

        TAP.$("ELE_LINK_BENEFITS","//span[contains(text(),'Benefits')]","TGTYPESCREENREG");

        WAIT.$("ELE_DRPDWN_BENEFITPLAN","//label[@id='workbenchForm:workbenchTabs:initialPlanCode_label']","VISIBLE","TGTYPESCREENREG");

        TAP.$("ELE_DRPDWN_BENEFITPLAN","//label[@id='workbenchForm:workbenchTabs:initialPlanCode_label']","TGTYPESCREENREG");

		var_dataToPass = var_BenefitPlan;
                 LOGGER.info("Executed Step = STORE,var_dataToPass,var_BenefitPlan,TGTYPESCREENREG");
		try { 
		 Thread.sleep(1000);
		WebElement selectValueOfDropDown= driver.findElement(By.xpath("//li[@data-label='"+ var_dataToPass +"']"));

		takeScreenshot();

				selectValueOfDropDown.click();
		// Add Screenshot will only work for Android platform

		takeScreenshot();

				 Thread.sleep(1000);
		 
		 ;} 
		 catch(Exception e) { PrintVal("Renderer Script Exception : "+e.toString()); } 
		 PrintVal("Executed Step = CALLRENDERERSCRIPT,SELECTVALUEFROMDROPDOWN"); 
        WAIT.$(5,"TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_CONTINUE","//span[contains(text(),'Continue')]","TGTYPESCREENREG");

        WAIT.$("ELE_TXTBOX_UNITS","//input[@id='workbenchForm:workbenchTabs:unitsOfInsurance_input']","VISIBLE","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_UNITS","//input[@id='workbenchForm:workbenchTabs:unitsOfInsurance_input']",var_Units,"FALSE","TGTYPESCREENREG");

		var_dataToPass = var_RiskClass;
                 LOGGER.info("Executed Step = STORE,var_dataToPass,var_RiskClass,TGTYPESCREENREG");
        TAP.$("ELE_DRPDWN_RISK_CLASS","//label[@id='workbenchForm:workbenchTabs:smokerCode_label']","TGTYPESCREENREG");

		try { 
		 Thread.sleep(1000);
		WebElement selectValueOfDropDown= driver.findElement(By.xpath("//li[@data-label='"+ var_dataToPass +"']"));

		takeScreenshot();

				selectValueOfDropDown.click();
		// Add Screenshot will only work for Android platform

		takeScreenshot();

				 Thread.sleep(1000);
		 
		 ;} 
		 catch(Exception e) { PrintVal("Renderer Script Exception : "+e.toString()); } 
		 PrintVal("Executed Step = CALLRENDERERSCRIPT,SELECTVALUEFROMDROPDOWN"); 
        WAIT.$(5,"TGTYPESCREENREG");

		var_dataToPass = var_NonforfeitureOption;
                 LOGGER.info("Executed Step = STORE,var_dataToPass,var_NonforfeitureOption,TGTYPESCREENREG");
        TAP.$("ELE_DRPDWN_NONFORFEITUREOPTION","//label[@id='workbenchForm:workbenchTabs:defaultNonForfOpt_label']","TGTYPESCREENREG");

		try { 
		 Thread.sleep(1000);
		WebElement selectValueOfDropDown= driver.findElement(By.xpath("//li[@data-label='"+ var_dataToPass +"']"));

		takeScreenshot();

				selectValueOfDropDown.click();
		// Add Screenshot will only work for Android platform

		takeScreenshot();

				 Thread.sleep(1000);
		 
		 ;} 
		 catch(Exception e) { PrintVal("Renderer Script Exception : "+e.toString()); } 
		 PrintVal("Executed Step = CALLRENDERERSCRIPT,SELECTVALUEFROMDROPDOWN"); 
        WAIT.$(5,"TGTYPESCREENREG");

		var_dataToPass = var_PrimaryDividend;
                 LOGGER.info("Executed Step = STORE,var_dataToPass,var_PrimaryDividend,TGTYPESCREENREG");
        TAP.$("ELE_DRPDWN_PRIMARYDIVIDENDOPETION_POLICY","//label[@id='workbenchForm:workbenchTabs:primaryDividendOption_label']","TGTYPESCREENREG");

		try { 
		 Thread.sleep(1000);
		WebElement selectValueOfDropDown= driver.findElement(By.xpath("//li[@data-label='"+ var_dataToPass +"']"));

		takeScreenshot();

				selectValueOfDropDown.click();
		// Add Screenshot will only work for Android platform

		takeScreenshot();

				 Thread.sleep(1000);
		 
		 ;} 
		 catch(Exception e) { PrintVal("Renderer Script Exception : "+e.toString()); } 
		 PrintVal("Executed Step = CALLRENDERERSCRIPT,SELECTVALUEFROMDROPDOWN"); 
        WAIT.$(5,"TGTYPESCREENREG");

		var_dataToPass = var_SecondaryDividend;
                 LOGGER.info("Executed Step = STORE,var_dataToPass,var_SecondaryDividend,TGTYPESCREENREG");
        TAP.$("ELE_DRPDWN_SECONDARY_DIVIDEND","//label[@id='workbenchForm:workbenchTabs:secondaryDividendOption_label']","TGTYPESCREENREG");

		try { 
		 Thread.sleep(1000);
		WebElement selectValueOfDropDown= driver.findElement(By.xpath("//li[@data-label='"+ var_dataToPass +"']"));

		takeScreenshot();

				selectValueOfDropDown.click();
		// Add Screenshot will only work for Android platform

		takeScreenshot();

				 Thread.sleep(1000);
		 
		 ;} 
		 catch(Exception e) { PrintVal("Renderer Script Exception : "+e.toString()); } 
		 PrintVal("Executed Step = CALLRENDERERSCRIPT,SELECTVALUEFROMDROPDOWN"); 
        WAIT.$(5,"TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SAVE","//span[contains(text(),'Save')]","TGTYPESCREENREG");

        WAIT.$("ELE_MESSAGE_ITEMSUCCESFULLYADDED","//div[@id='workbenchForm:workbenchTabs:messages']//span[@class='ui-messages-info-summary'][contains(text(),'Item successfully added.')]","VISIBLE","TGTYPESCREENREG");

        TAP.$("ELE_TAB_CLOSE","//li[@class='ui-state-default ui-tabs-selected ui-state-active ui-corner-top']//span[@class='ui-icon ui-icon-close']","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        CALL.$("IssuePolicyForNW020E2E","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SHOWLINKS","//button[@id='workbenchForm:workbenchTabs:tb_buttonLink']","TGTYPESCREENREG");

        TAP.$("ELE_LINK_ISSUE","//span[@class='ui-menuitem-text'][contains(text(),'Issue')]","TGTYPESCREENREG");

        WAIT.$("ELE_BUTTON_SUBMIT","//span[contains(text(),'Submit')]","VISIBLE","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SUBMIT","//span[contains(text(),'Submit')]","TGTYPESCREENREG");

        WAIT.$("ELE_MSG_REQUESTCOMPLETEDSUCCESSFULLY","//div[@id='workbenchForm:workbenchTabs:messages']//span[@class='ui-messages-info-summary'][contains(text(),'Request completed successfully.')]","VISIBLE","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        TAP.$("ELE_TAB_CLOSE","//li[@class='ui-state-default ui-tabs-selected ui-state-active ui-corner-top']//span[@class='ui-icon ui-icon-close']","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        CALL.$("SettlePolicyForNW020E2E","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SHOWLINKS","//button[@id='workbenchForm:workbenchTabs:tb_buttonLink']","TGTYPESCREENREG");

        TAP.$("ELE_LINK_SETTLE","//span[contains(text(),'Settle')]","TGTYPESCREENREG");

        WAIT.$("ELE_BUTTON_SUBMIT","//span[contains(text(),'Submit')]","VISIBLE","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        SWIPE.$("DOWN","TGTYPESCREENREG");

        SWIPE.$("DOWN","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SUBMIT","//span[contains(text(),'Submit')]","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        WAIT.$("ELE_MSG_REQUESTCOMPLETEDSUCCESSFULLY","//div[@id='workbenchForm:workbenchTabs:messages']//span[@class='ui-messages-info-summary'][contains(text(),'Request completed successfully.')]","VISIBLE","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        TAP.$("ELE_TAB_CLOSE","//li[@class='ui-state-default ui-tabs-selected ui-state-active ui-corner-top']//span[@class='ui-icon ui-icon-close']","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        CALL.$("PremiumPolicyForNW020E2E","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

		String var_PremiumAnnual;
                 LOGGER.info("Executed Step = VAR,String,var_PremiumAnnual,TGTYPESCREENREG");
		var_PremiumAnnual = getJsonData(var_CSVData , "$.records["+var_Count+"].PremiumAnnual");
                 LOGGER.info("Executed Step = STORE,var_PremiumAnnual,var_CSVData,$.records[{var_Count}].PremiumAnnual,TGTYPESCREENREG");
		String var_PremiumSemiannual;
                 LOGGER.info("Executed Step = VAR,String,var_PremiumSemiannual,TGTYPESCREENREG");
		var_PremiumSemiannual = getJsonData(var_CSVData , "$.records["+var_Count+"].PremiumSemiannual");
                 LOGGER.info("Executed Step = STORE,var_PremiumSemiannual,var_CSVData,$.records[{var_Count}].PremiumSemiannual,TGTYPESCREENREG");
		String var_PremiumQuarterly;
                 LOGGER.info("Executed Step = VAR,String,var_PremiumQuarterly,TGTYPESCREENREG");
		var_PremiumQuarterly = getJsonData(var_CSVData , "$.records["+var_Count+"].PremiumQuarterly");
                 LOGGER.info("Executed Step = STORE,var_PremiumQuarterly,var_CSVData,$.records[{var_Count}].PremiumQuarterly,TGTYPESCREENREG");
		String var_PremiumMonthly;
                 LOGGER.info("Executed Step = VAR,String,var_PremiumMonthly,TGTYPESCREENREG");
		var_PremiumMonthly = getJsonData(var_CSVData , "$.records["+var_Count+"].PremiumMonthly");
                 LOGGER.info("Executed Step = STORE,var_PremiumMonthly,var_CSVData,$.records[{var_Count}].PremiumMonthly,TGTYPESCREENREG");
        TAP.$("ELE_BUTTON_SHOWLINKS","//button[@id='workbenchForm:workbenchTabs:tb_buttonLink']","TGTYPESCREENREG");

        TAP.$("ELE_LINK_PREMIUMS","//span[contains(text(),'Premiums')]","TGTYPESCREENREG");

        WAIT.$("ELE_LINK_FIRST_ANNUAL_PREMIUM","//span[@id='workbenchForm:workbenchTabs:gridNavGrid:0:benefitAnnualPremium']","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        ASSERT.$("ELE_LINK_FIRST_ANNUAL_PREMIUM","//span[@id='workbenchForm:workbenchTabs:gridNavGrid:0:benefitAnnualPremium']","CONTAINS",var_PremiumAnnual,"TGTYPESCREENREG");

        ASSERT.$("ELE_LINK_FIRST_SEMI_ANNUAL","//span[@id='workbenchForm:workbenchTabs:gridNavGrid:0:benefitSemiAnnuPrem']","CONTAINS",var_PremiumSemiannual,"TGTYPESCREENREG");

        ASSERT.$("ELE_LINK_FIRST_QUATERLY_PREMIUM","//span[@id='workbenchForm:workbenchTabs:gridNavGrid:0:benefitQuarterlyPrem']","CONTAINS",var_PremiumQuarterly,"TGTYPESCREENREG");

        ASSERT.$("ELE_LINK_FIRST_MONTHLY_PREMIUM","//span[@id='workbenchForm:workbenchTabs:gridNavGrid:0:benefitMonthlyPrem']","CONTAINS",var_PremiumMonthly,"TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_GIAS_LOGO","//img[@id='headerForm:headerLogoImage']","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_GIAS_LOGO","//img[@id='headerForm:headerLogoImage']","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        CALL.$("PolicyPaymentForNW020E2E","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_GIAS_LOGO","//img[@id='headerForm:headerLogoImage']","TGTYPESCREENREG");

		String var_PaidToDate2;
                 LOGGER.info("Executed Step = VAR,String,var_PaidToDate2,TGTYPESCREENREG");
		var_PaidToDate2 = getJsonData(var_CSVData , "$.records["+var_Count+"].PaidToDate2");
                 LOGGER.info("Executed Step = STORE,var_PaidToDate2,var_CSVData,$.records[{var_Count}].PaidToDate2,TGTYPESCREENREG");
		String var_ModalPremiums;
                 LOGGER.info("Executed Step = VAR,String,var_ModalPremiums,TGTYPESCREENREG");
		var_ModalPremiums = getJsonData(var_CSVData , "$.records["+var_Count+"].ModalPremiums");
                 LOGGER.info("Executed Step = STORE,var_ModalPremiums,var_CSVData,$.records[{var_Count}].ModalPremiums,TGTYPESCREENREG");
        SWIPE.$("DOWN","ELE_TXTBOX_SEARCHTASKS","//input[@id='workbenchForm:workbenchTabs:taskSearchContainer:taskSearchSearch']","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_SEARCHTASKS","//input[@id='workbenchForm:workbenchTabs:taskSearchContainer:taskSearchSearch']","Policy Payment","FALSE","TGTYPESCREENREG");

        WAIT.$(3,"TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SEARCHTASKS","//a[@id='workbenchForm:workbenchTabs:taskSearchContainer:tb_buttonSearch']//span[@class='fa fa-search fa-lg']","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        TAP.$("ELE_LINK_POLICYPAYMENT","//body/div[@id='Outer40']/div[@id='ContentPanel']/div[@id='Content40']/form[@id='workbenchForm']/div[@id='PageTiles']/div[@id='workbenchForm:workbenchTabs']/div[1]/div[1]/div[1]/div[2]/div[2]/div[1]/div[1]/div[1]/div[3]/div[1]/div[3]/button[1]/span[2]","TGTYPESCREENREG");

        WAIT.$("ELE_BUTTON_CONTINUE","//span[contains(text(),'Continue')]","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_POLICYNUMBER","//input[@id='workbenchForm:workbenchTabs:policyNumber']",var_PolicyNumber,"FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_CONTINUE","//span[contains(text(),'Continue')]","TGTYPESCREENREG");

        WAIT.$("ELE_BUTTON_SUBMIT","//span[contains(text(),'Submit')]","VISIBLE","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SUBMIT","//span[contains(text(),'Submit')]","TGTYPESCREENREG");

        WAIT.$("ELE_MSG_REQUESTCOMPLETEDSUCCESSFULLY","//div[@id='workbenchForm:workbenchTabs:messages']//span[@class='ui-messages-info-summary'][contains(text(),'Request completed successfully.')]","VISIBLE","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        ASSERT.$("ELE_LABEL_PAIDTODATE","//span[@id='workbenchForm:workbenchTabs:paidToDate']","CONTAINS",var_PaidToDate2,"TGTYPESCREENREG");

        SWIPE.$("UP","ELE_LABEL_MODELPREMIUM","//span[@id='workbenchForm:workbenchTabs:modalPremium']","TGTYPESCREENREG");

        ASSERT.$("ELE_LABEL_MODELPREMIUM","//span[@id='workbenchForm:workbenchTabs:modalPremium']","CONTAINS",var_ModalPremiums,"TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_GIAS_LOGO","//img[@id='headerForm:headerLogoImage']","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        CALL.$("ChangeSessionDate3","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_GIAS_LOGO","//img[@id='headerForm:headerLogoImage']","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

		String var_SessionDate3;
                 LOGGER.info("Executed Step = VAR,String,var_SessionDate3,TGTYPESCREENREG");
		var_SessionDate3 = getJsonData(var_CSVData , "$.records["+var_Count+"].SessionDate3");
                 LOGGER.info("Executed Step = STORE,var_SessionDate3,var_CSVData,$.records[{var_Count}].SessionDate3,TGTYPESCREENREG");
        TAP.$("ELE_BUTTON_SESSIONDATE","//a[@id='headerForm:sessionDatelink']","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_SESSIONDATE","//input[@id='sessionDateForm:sessionDateInput_input']",var_SessionDate3,"FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_CHANGEDATE","//span[contains(text(),'Change Date')]","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        CALL.$("PolicyInquiryForNW020E2e","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_GIAS_LOGO","//img[@id='headerForm:headerLogoImage']","TGTYPESCREENREG");

		String var_LastDividendEarnedAmount;
                 LOGGER.info("Executed Step = VAR,String,var_LastDividendEarnedAmount,TGTYPESCREENREG");
		var_LastDividendEarnedAmount = getJsonData(var_CSVData , "$.records["+var_Count+"].LastDividendEarnedAmount");
                 LOGGER.info("Executed Step = STORE,var_LastDividendEarnedAmount,var_CSVData,$.records[{var_Count}].LastDividendEarnedAmount,TGTYPESCREENREG");
		String var_PaymentFrequencyNew;
                 LOGGER.info("Executed Step = VAR,String,var_PaymentFrequencyNew,TGTYPESCREENREG");
		var_PaymentFrequencyNew = getJsonData(var_CSVData , "$.records["+var_Count+"].PaymentFrequencyNew");
                 LOGGER.info("Executed Step = STORE,var_PaymentFrequencyNew,var_CSVData,$.records[{var_Count}].PaymentFrequencyNew,TGTYPESCREENREG");
        SWIPE.$("DOWN","ELE_TXTBOX_SEARCHTASKS","//input[@id='workbenchForm:workbenchTabs:taskSearchContainer:taskSearchSearch']","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_SEARCHTASKS","//input[@id='workbenchForm:workbenchTabs:taskSearchContainer:taskSearchSearch']","Policy Inquiry","FALSE","TGTYPESCREENREG");

        WAIT.$(3,"TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SEARCHTASKS","//a[@id='workbenchForm:workbenchTabs:taskSearchContainer:tb_buttonSearch']//span[@class='fa fa-search fa-lg']","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        TAP.$("ELE_LINK_POLICYINQUIRY","//span[contains(text(),'Policy Inquiry')]","TGTYPESCREENREG");

        WAIT.$("ELE_TXTBOX_POLICYNUMBER","//input[@id='workbenchForm:workbenchTabs:policyNumber']","VISIBLE","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_POLICYNUMBER","//input[@id='workbenchForm:workbenchTabs:policyNumber']",var_PolicyNumber,"FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_CONTINUE","//span[contains(text(),'Continue')]","TGTYPESCREENREG");

        WAIT.$("ELE_LABEL_POLICYNUMBER","//span[@id='workbenchForm:workbenchTabs:policyNumber']","VISIBLE","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SHOWLINKS","//button[@id='workbenchForm:workbenchTabs:tb_buttonLink']","TGTYPESCREENREG");

        WAIT.$(3,"TGTYPESCREENREG");

        TAP.$("ELE_LINK_CONTRACT","//span[contains(text(),'Contract')]","TGTYPESCREENREG");

        WAIT.$("ELE_BUTTON_EDIT","//button[@id='workbenchForm:workbenchTabs:tb_buttonUpdate']","TGTYPESCREENREG");

        SWIPE.$("DOWN","TGTYPESCREENREG");

        SWIPE.$("DOWN","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_EDIT","//button[@id='workbenchForm:workbenchTabs:tb_buttonUpdate']","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        SWIPE.$("UP","ELE_DRPDWN_PAYMENTFREQUENCY_CONTRACT","//label[@id='workbenchForm:workbenchTabs:billingMode_label']","TGTYPESCREENREG");

        TAP.$("ELE_DRPDWN_PAYMENTFREQUENCY_CONTRACT","//label[@id='workbenchForm:workbenchTabs:billingMode_label']","TGTYPESCREENREG");

		var_dataToPass = var_PaymentFrequencyNew;
                 LOGGER.info("Executed Step = STORE,var_dataToPass,var_PaymentFrequencyNew,TGTYPESCREENREG");
		try { 
		 //li[@id='workbenchForm:workbenchTabs:billingMode_2']


			Thread.sleep(1000);
					for (int i = 0; i < 2000; i++){
						WebElement selectValueOfDropDown= driver.findElement(By.xpath("//li[@id='workbenchForm:workbenchTabs:billingMode_"+ i +"']"));

						if (selectValueOfDropDown.getText().contains(var_dataToPass)){
		AddToReport("ASSERT," + selectValueOfDropDown.getText() +", CONTAINS,"+var_dataToPass);
		                                selectValueOfDropDown.click();
							
						
							break;
						}
					}
					takeScreenshot();

					Thread.sleep(2000);
		 
		 ;} 
		 catch(Exception e) { PrintVal("Renderer Script Exception : "+e.toString()); } 
		 PrintVal("Executed Step = CALLRENDERERSCRIPT,SELECT_PAYMENTFREQUENCY_CONTRACT"); 
        WAIT.$(5,"TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SUBMIT","//span[contains(text(),'Submit')]","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

		try { 
		 driver.switchTo().frame(0);
		driver.findElement(By.xpath("//form[@id='confirmDialogForm']")).click();
		Thread.sleep(1000);
		driver.findElement(By.xpath("//button[@id='j_idt8']")).click();
		// Add Screenshot will only work for Android platform
		//
		takeScreenshot();

		 
		 ;} 
		 catch(Exception e) { PrintVal("Renderer Script Exception : "+e.toString()); } 
		 PrintVal("Executed Step = CALLRENDERERSCRIPT,CLICKYESONCONFIRMACTION"); 
        WAIT.$(5,"TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_GIAS_LOGO","//img[@id='headerForm:headerLogoImage']","TGTYPESCREENREG");

        WAIT.$(10,"TGTYPESCREENREG");

        CALL.$("ChangeSessionDate2","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_GIAS_LOGO","//img[@id='headerForm:headerLogoImage']","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

		String var_SessionDate2;
                 LOGGER.info("Executed Step = VAR,String,var_SessionDate2,TGTYPESCREENREG");
		var_SessionDate2 = getJsonData(var_CSVData , "$.records["+var_Count+"].SessionDate2");
                 LOGGER.info("Executed Step = STORE,var_SessionDate2,var_CSVData,$.records[{var_Count}].SessionDate2,TGTYPESCREENREG");
        TAP.$("ELE_BUTTON_SESSIONDATE","//a[@id='headerForm:sessionDatelink']","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_SESSIONDATE","//input[@id='sessionDateForm:sessionDateInput_input']",var_SessionDate2,"FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_CHANGEDATE","//span[contains(text(),'Change Date')]","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        CALL.$("PolicyInquiryToCheckDividendsDataForNW020","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_GIAS_LOGO","//img[@id='headerForm:headerLogoImage']","TGTYPESCREENREG");

		String var_LastDividendDate;
                 LOGGER.info("Executed Step = VAR,String,var_LastDividendDate,TGTYPESCREENREG");
		var_LastDividendDate = getJsonData(var_CSVData , "$.records["+var_Count+"].LastDividendEarnedDate");
                 LOGGER.info("Executed Step = STORE,var_LastDividendDate,var_CSVData,$.records[{var_Count}].LastDividendEarnedDate,TGTYPESCREENREG");
		String var_LastDividendEarnAmount;
                 LOGGER.info("Executed Step = VAR,String,var_LastDividendEarnAmount,TGTYPESCREENREG");
		var_LastDividendEarnAmount = getJsonData(var_CSVData , "$.records["+var_Count+"].LastDividendEarnedAmount");
                 LOGGER.info("Executed Step = STORE,var_LastDividendEarnAmount,var_CSVData,$.records[{var_Count}].LastDividendEarnedAmount,TGTYPESCREENREG");
        SWIPE.$("DOWN","ELE_TXTBOX_SEARCHTASKS","//input[@id='workbenchForm:workbenchTabs:taskSearchContainer:taskSearchSearch']","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_SEARCHTASKS","//input[@id='workbenchForm:workbenchTabs:taskSearchContainer:taskSearchSearch']","Policy Inquiry","FALSE","TGTYPESCREENREG");

        WAIT.$(3,"TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SEARCHTASKS","//a[@id='workbenchForm:workbenchTabs:taskSearchContainer:tb_buttonSearch']//span[@class='fa fa-search fa-lg']","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        TAP.$("ELE_LINK_POLICYINQUIRY","//span[contains(text(),'Policy Inquiry')]","TGTYPESCREENREG");

        WAIT.$("ELE_TXTBOX_POLICYNUMBER","//input[@id='workbenchForm:workbenchTabs:policyNumber']","VISIBLE","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_POLICYNUMBER","//input[@id='workbenchForm:workbenchTabs:policyNumber']",var_PolicyNumber,"FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_CONTINUE","//span[contains(text(),'Continue')]","TGTYPESCREENREG");

        WAIT.$("ELE_LABEL_POLICYNUMBER","//span[@id='workbenchForm:workbenchTabs:policyNumber']","VISIBLE","TGTYPESCREENREG");

        WAIT.$(3,"TGTYPESCREENREG");

        SWIPE.$("UP","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        SWIPE.$("UP","ELE_LABEL_LASTDIVIDENDDATE","//span[@id='workbenchForm:workbenchTabs:lastDividendDate']","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        ASSERT.$("ELE_LABEL_LASTDIVIDENDDATE","//span[@id='workbenchForm:workbenchTabs:lastDividendDate']","CONTAINS",var_LastDividendDate,"TGTYPESCREENREG");

        ASSERT.$("ELE_LABEL_LASTDIVIDENDEARNEDAMOUNT","//span[@id='workbenchForm:workbenchTabs:lastDividendAmount']","CONTAINS",var_LastDividendEarnAmount,"TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        CALL.$("LinkDividendForTransactionHistoryNW020E2E","TGTYPESCREENREG");

		String var_ApplyDate;
                 LOGGER.info("Executed Step = VAR,String,var_ApplyDate,TGTYPESCREENREG");
		var_ApplyDate = getJsonData(var_CSVData , "$.records["+var_Count+"].ApplyDate");
                 LOGGER.info("Executed Step = STORE,var_ApplyDate,var_CSVData,$.records[{var_Count}].ApplyDate,TGTYPESCREENREG");
		String var_Type;
                 LOGGER.info("Executed Step = VAR,String,var_Type,TGTYPESCREENREG");
		var_Type = getJsonData(var_CSVData , "$.records["+var_Count+"].Type");
                 LOGGER.info("Executed Step = STORE,var_Type,var_CSVData,$.records[{var_Count}].Type,TGTYPESCREENREG");
		String var_Status;
                 LOGGER.info("Executed Step = VAR,String,var_Status,TGTYPESCREENREG");
		var_Status = getJsonData(var_CSVData , "$.records["+var_Count+"].Status");
                 LOGGER.info("Executed Step = STORE,var_Status,var_CSVData,$.records[{var_Count}].Status,TGTYPESCREENREG");
		String var_TotalAmount;
                 LOGGER.info("Executed Step = VAR,String,var_TotalAmount,TGTYPESCREENREG");
		var_TotalAmount = getJsonData(var_CSVData , "$.records["+var_Count+"].TotalAmount");
                 LOGGER.info("Executed Step = STORE,var_TotalAmount,var_CSVData,$.records[{var_Count}].TotalAmount,TGTYPESCREENREG");
        TAP.$("ELE_BUTTON_SHOWLINKS","//button[@id='workbenchForm:workbenchTabs:tb_buttonLink']","TGTYPESCREENREG");

        WAIT.$(3,"TGTYPESCREENREG");

        TAP.$("ELE_LINK_DIVIDEND","//body/div[@id='workbenchForm:workbenchTabs:link_menu']/ul[1]/li[13]/a[1]/span[1]","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        WAIT.$("ELE_LABEL_POLICYNUMBER","//span[@id='workbenchForm:workbenchTabs:policyNumber']","TGTYPESCREENREG");

        WAIT.$(3,"TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SHOWLINKS","//button[@id='workbenchForm:workbenchTabs:tb_buttonLink']","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        TAP.$("ELE_LINK_TRANSACTIONHISTORY","//span[contains(text(),'Transaction History')]","TGTYPESCREENREG");

        WAIT.$("ELE_BUTTON_SEARCH","//button[@id='workbenchForm:workbenchTabs:button_search']","VISIBLE","TGTYPESCREENREG");

        WAIT.$(3,"TGTYPESCREENREG");

		try { 
		 
		//workbenchForm:workbenchTabs:grid:0:anniversaryDate
		//workbenchForm:workbenchTabs:grid:0:dividendCouponIndi
		//workbenchForm:workbenchTabs:grid:0:applyStatus
		//workbenchForm:workbenchTabs:grid:0:divCpnCashValueAmt

			Thread.sleep(1000);
					for (int i = 0; i < 10; i++){
						WebElement anniversaryDate = driver.findElement(By.xpath("//span[@id='workbenchForm:workbenchTabs:grid:"+ i +":anniversaryDate']"));
		WebElement dividendCouponIndi= driver.findElement(By.xpath("//span[@id='workbenchForm:workbenchTabs:grid:"+ i +":dividendCouponIndi']"));
		WebElement applyStatus= driver.findElement(By.xpath("//span[@id='workbenchForm:workbenchTabs:grid:"+ i +":applyStatus']"));
		WebElement divCpnCashValueAmt= driver.findElement(By.xpath("//span[@id='workbenchForm:workbenchTabs:grid:"+ i +":divCpnCashValueAmt']"));

						if (anniversaryDate .getText().contains(var_ApplyDate) && dividendCouponIndi.getText().contains(var_Type) 
		                                     && applyStatus.getText().contains(var_Status) && divCpnCashValueAmt.getText().contains(var_TotalAmount) ){

		     
							AddToReport("ASSERT, Apply Date:" + anniversaryDate .getText() +", CONTAINS,"+var_ApplyDate);
							AddToReport("ASSERT, Type:" + dividendCouponIndi.getText() +", CONTAINS,"+var_Type);
							AddToReport("ASSERT, Status:" + applyStatus.getText() +", CONTAINS,"+var_Status);
							AddToReport("ASSERT, Total Amount:" + divCpnCashValueAmt.getText() +", CONTAINS,"+var_TotalAmount);
						
							break;
						}
					}
					takeScreenshot();

					Thread.sleep(2000);
		 
		 ;} 
		 catch(Exception e) { PrintVal("Renderer Script Exception : "+e.toString()); } 
		 PrintVal("Executed Step = CALLRENDERERSCRIPT,VERIFYTRANSACTIONHISTORYDATAFORNW020E2E"); 
        WAIT.$(5,"TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_GIAS_LOGO","//img[@id='headerForm:headerLogoImage']","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        CALL.$("VerifyDataForecastDividendOfflineProcessingNW020E2E","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_GIAS_LOGO","//img[@id='headerForm:headerLogoImage']","TGTYPESCREENREG");

		String var_FromDate;
                 LOGGER.info("Executed Step = VAR,String,var_FromDate,TGTYPESCREENREG");
		String var_ThroughDate;
                 LOGGER.info("Executed Step = VAR,String,var_ThroughDate,TGTYPESCREENREG");
		var_FromDate = getJsonData(var_CSVData , "$.records["+var_Count+"].FromDate");
                 LOGGER.info("Executed Step = STORE,var_FromDate,var_CSVData,$.records[{var_Count}].FromDate,TGTYPESCREENREG");
		var_ThroughDate = getJsonData(var_CSVData , "$.records["+var_Count+"].ThroughDate");
                 LOGGER.info("Executed Step = STORE,var_ThroughDate,var_CSVData,$.records[{var_Count}].ThroughDate,TGTYPESCREENREG");
        SWIPE.$("DOWN","ELE_TXTBOX_SEARCHTASKS","//input[@id='workbenchForm:workbenchTabs:taskSearchContainer:taskSearchSearch']","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_SEARCHTASKS","//input[@id='workbenchForm:workbenchTabs:taskSearchContainer:taskSearchSearch']","Forecast Dividend","FALSE","TGTYPESCREENREG");

        WAIT.$(3,"TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SEARCHTASKS","//a[@id='workbenchForm:workbenchTabs:taskSearchContainer:tb_buttonSearch']//span[@class='fa fa-search fa-lg']","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        TAP.$("ELE_LINK_FORECAST_DIVIDEND","//span[contains(text(),'Forecast Dividends Offline Processing')]","TGTYPESCREENREG");

        WAIT.$("ELE_BUTTON_SUBMIT","//span[contains(text(),'Submit')]","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_FROMDATE","//input[@id='workbenchForm:workbenchTabs:transactionDate_input']   ",var_FromDate,"FALSE","TGTYPESCREENREG");

        WAIT.$(3,"TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_THROUGHDATE","//input[@id='workbenchForm:workbenchTabs:thruDate_input']",var_ThroughDate,"FALSE","TGTYPESCREENREG");

        TAP.$("ELE_LABEL_NO","//label[contains(text(),'No')]","TGTYPESCREENREG");

        WAIT.$(3,"TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SUBMIT","//span[contains(text(),'Submit')]","TGTYPESCREENREG");

        WAIT.$("ELE_LABEL_POLICYNUMBER","//span[@id='workbenchForm:workbenchTabs:policyNumber']","VISIBLE","TGTYPESCREENREG");

        WAIT.$(3,"TGTYPESCREENREG");

        WAIT.$("ELE_BUTTON_SHOWLINKS","//button[@id='workbenchForm:workbenchTabs:tb_buttonLink']","VISIBLE","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SHOWLINKS","//button[@id='workbenchForm:workbenchTabs:tb_buttonLink']","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        TAP.$("ELE_LINK_SUBMITTED_JOB_LOGS","//span[contains(text(),'Submitted Job Logs')]","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

		String var_JobIdentifier;
                 LOGGER.info("Executed Step = VAR,String,var_JobIdentifier,TGTYPESCREENREG");
		String var_JobStatus;
                 LOGGER.info("Executed Step = VAR,String,var_JobStatus,TGTYPESCREENREG");
		var_JobIdentifier = getJsonData(var_CSVData , "$.records["+var_Count+"].JobIdentifier");
                 LOGGER.info("Executed Step = STORE,var_JobIdentifier,var_CSVData,$.records[{var_Count}].JobIdentifier,TGTYPESCREENREG");
		var_JobStatus = getJsonData(var_CSVData , "$.records["+var_Count+"].JobStatus");
                 LOGGER.info("Executed Step = STORE,var_JobStatus,var_CSVData,$.records[{var_Count}].JobStatus,TGTYPESCREENREG");
		try { 
		 
		//workbenchForm:workbenchTabs:grid:3:processorId
		//workbenchForm:workbenchTabs:grid:3:status


			Thread.sleep(1000);
					for (int i = 0; i < 30; i++){
						WebElement jobIdentifier= driver.findElement(By.xpath("//span[@id='workbenchForm:workbenchTabs:grid:"+ i +":processorId']"));
		WebElement jobStatus= driver.findElement(By.xpath("//span[@id='workbenchForm:workbenchTabs:grid:"+ i +":status']"));


						if (jobIdentifier.getText().contains(var_JobIdentifier) && jobStatus.getText().contains(var_JobStatus) ){

		     
							AddToReport("ASSERT, jobIdentifier:" + jobIdentifier.getText() +", CONTAINS,"+var_JobIdentifier);
							AddToReport("ASSERT, jobStatus:" + jobStatus.getText() +", CONTAINS,"+var_JobStatus);
							
						
							break;
						}
					}
					takeScreenshot();

					Thread.sleep(2000);
		 
		 ;} 
		 catch(Exception e) { PrintVal("Renderer Script Exception : "+e.toString()); } 
		 PrintVal("Executed Step = CALLRENDERERSCRIPT,VERIFY_JOBLOGSNW020E2E"); 
        WAIT.$(5,"TGTYPESCREENREG");

        CALL.$("VerifyPDFDataToGIASNW020E2E","TGTYPESCREENREG");

        WAIT.$(45,"TGTYPESCREENREG");

		String var_ProjectedDividend;
                 LOGGER.info("Executed Step = VAR,String,var_ProjectedDividend,TGTYPESCREENREG");
		var_ProjectedDividend = getJsonData(var_CSVData , "$.records["+var_Count+"].ProjectedDividend");
                 LOGGER.info("Executed Step = STORE,var_ProjectedDividend,var_CSVData,$.records[{var_Count}].ProjectedDividend,TGTYPESCREENREG");
		String var_PrimaryApplied;
                 LOGGER.info("Executed Step = VAR,String,var_PrimaryApplied,TGTYPESCREENREG");
		var_PrimaryApplied = getJsonData(var_CSVData , "$.records["+var_Count+"].PrimaryApplied");
                 LOGGER.info("Executed Step = STORE,var_PrimaryApplied,var_CSVData,$.records[{var_Count}].PrimaryApplied,TGTYPESCREENREG");
		String var_Opt;
                 LOGGER.info("Executed Step = VAR,String,var_Opt,TGTYPESCREENREG");
		var_Opt = getJsonData(var_CSVData , "$.records["+var_Count+"].Opt");
                 LOGGER.info("Executed Step = STORE,var_Opt,var_CSVData,$.records[{var_Count}].Opt,TGTYPESCREENREG");
        WAIT.$(45,"TGTYPESCREENREG");

		try { 
		 
		            //String var_PolicyNumber = "TEST2007";
		//            String var_ProjectedDividend = "100.00";
		//            String var_PrimaryApplied = "100.00";
		//            String var_Opt = "M";

		            File dir = new File("\\\\cis-coreapa1\\Core\\NWAUT1\\REPORTS\\NW\\Service\\DIV310P\\OnDemand\\");
		            File[] files = dir.listFiles();
		            if (files == null || files.length == 0) {

		            }
		            File lastModifiedFile = files[0];
		            for (int i = 1; i < files.length; i++) {
		                System.out.println("File List  === "+files[i].getName());
		                if (files[i].getName().contains(".pdf")) {

		                    System.out.println("PDF File List  === "+files[i].getName());
		                    if (lastModifiedFile.lastModified() < files[i].lastModified()) {
		                        lastModifiedFile = files[i];
		                    }
		                }
		            }
		            System.out.println("Latest File === "+lastModifiedFile.getName());


		            String PDFFile = "\\\\cis-coreapa1\\Core\\NWAUT1\\REPORTS\\NW\\Service\\DIV310P\\OnDemand\\"+lastModifiedFile.getName();

		            ArrayList<String> pdfFileInText = helper.WebTestUtility.readLinesFromPDFFile(PDFFile);

		            for (String line : pdfFileInText) {
		                System.out.println("line === "+line);
		                if (line.contains(var_PolicyNumber)) {

		                   // TEST2007    I OLUSD  L 06/11/19   50               .00      .000               .00                .00            100.00               100.00  M              0                   .00                0
		                    System.out.println("TEST2007 line === === "+line);

		                    String[] parts = line.replaceAll("\\s{2,}", " ").trim().split(" ");



		                    String policyNumberFromPDF = parts[0].trim();
		                    String ProjectedDividendFromPDF = parts[10].trim();
		                    String PrimaryAppliedFromPDF = parts[11].trim();
		                    String OptFromPDF = parts[12].trim();

		                    writeToCSV("Policy Number",var_PolicyNumber);
		                    writeToCSV("FieldName","Policy Number");
		                    writeToCSV("Value From PDF",policyNumberFromPDF);
		                    writeToCSV("Value From GIAS",var_PolicyNumber);

		                    if (var_PolicyNumber.equalsIgnoreCase(policyNumberFromPDF) && policyNumberFromPDF != ""){
		                        writeToCSV("Status","PASS");
		                    }else{

		                        writeToCSV("Status","FAIL");
		                    }

		                    writeToCSV("Policy Number",var_PolicyNumber);
		                    writeToCSV("FieldName","Projected Dividend");
		                    writeToCSV("Value From PDF",ProjectedDividendFromPDF);
		                    writeToCSV("Value From GIAS",var_ProjectedDividend);

		                    if (ProjectedDividendFromPDF.equalsIgnoreCase(var_ProjectedDividend) && ProjectedDividendFromPDF != ""){
		                        writeToCSV("Status","PASS");
		                    }else{
		                        writeToCSV("Status","FAIL");
		                    }

		                    writeToCSV("Policy Number",var_PolicyNumber);
		                    writeToCSV("FieldName","Primary Applied");
		                    writeToCSV("Value From PDF",PrimaryAppliedFromPDF);
		                    writeToCSV("Value From GIAS",var_PrimaryApplied);

		                    if (PrimaryAppliedFromPDF.equalsIgnoreCase(var_PrimaryApplied) && PrimaryAppliedFromPDF != ""){
		                        writeToCSV("Status","PASS");
		                    }else{
		                        writeToCSV("Status","FAIL");
		                    }

		                    writeToCSV("Policy Number",var_PolicyNumber);
		                    writeToCSV("FieldName","Primary Option");
		                    writeToCSV("Value From PDF",OptFromPDF);
		                    writeToCSV("Value From GIAS",var_Opt);

		                    if (OptFromPDF.equalsIgnoreCase(var_Opt) && OptFromPDF != ""){
		                        writeToCSV("Status","PASS");
		                    }else{
		                        writeToCSV("Status","FAIL");
		                    }




		                }

		            }



		        
		 
		 ;} 
		 catch(Exception e) { PrintVal("Renderer Script Exception : "+e.toString()); } 
		 PrintVal("Executed Step = CALLRENDERERSCRIPT,PDFCOMPARISIONSCRIPTNW020E2E"); 
		var_CountN = var_CountN + ( 1 );
                 LOGGER.info("Executed Step = INCREASEVAR,var_CountN,1,TGTYPESCREENREG");
    	}
          LOGGER.info("Executed Step = WEND,TGTYPESCREENREG");
    	resetAttemps();

    }


    @Test
    public void testingscript() {

        // No need to declare / print method name here, handled by TestNg listener
        // declare("testingscript");

		try { 
		 
		            //String var_PolicyNumber = "TEST2007";
		//            String var_ProjectedDividend = "100.00";
		//            String var_PrimaryApplied = "100.00";
		//            String var_Opt = "M";

		            File dir = new File("\\\\cis-coreapa1\\Core\\NWAUT1\\REPORTS\\NW\\Service\\DIV310P\\OnDemand\\");
		            File[] files = dir.listFiles();
		            if (files == null || files.length == 0) {

		            }
		            File lastModifiedFile = files[0];
		            for (int i = 1; i < files.length; i++) {
		                System.out.println("File List  === "+files[i].getName());
		                if (files[i].getName().contains(".pdf")) {

		                    System.out.println("PDF File List  === "+files[i].getName());
		                    if (lastModifiedFile.lastModified() < files[i].lastModified()) {
		                        lastModifiedFile = files[i];
		                    }
		                }
		            }
		            System.out.println("Latest File === "+lastModifiedFile.getName());


		            String PDFFile = "\\\\cis-coreapa1\\Core\\NWAUT1\\REPORTS\\NW\\Service\\DIV310P\\OnDemand\\"+lastModifiedFile.getName();

		            ArrayList<String> pdfFileInText = helper.WebTestUtility.readLinesFromPDFFile(PDFFile);

		            for (String line : pdfFileInText) {
		                System.out.println("line === "+line);
		                if (line.contains(var_PolicyNumber)) {

		                   // TEST2007    I OLUSD  L 06/11/19   50               .00      .000               .00                .00            100.00               100.00  M              0                   .00                0
		                    System.out.println("TEST2007 line === === "+line);

		                    String[] parts = line.replaceAll("\\s{2,}", " ").trim().split(" ");



		                    String policyNumberFromPDF = parts[0].trim();
		                    String ProjectedDividendFromPDF = parts[10].trim();
		                    String PrimaryAppliedFromPDF = parts[11].trim();
		                    String OptFromPDF = parts[12].trim();

		                    writeToCSV("Policy Number",var_PolicyNumber);
		                    writeToCSV("FieldName","Policy Number");
		                    writeToCSV("Value From PDF",policyNumberFromPDF);
		                    writeToCSV("Value From GIAS",var_PolicyNumber);

		                    if (var_PolicyNumber.equalsIgnoreCase(policyNumberFromPDF) && policyNumberFromPDF != ""){
		                        writeToCSV("Status","PASS");
		                    }else{

		                        writeToCSV("Status","FAIL");
		                    }

		                    writeToCSV("Policy Number",var_PolicyNumber);
		                    writeToCSV("FieldName","Projected Dividend");
		                    writeToCSV("Value From PDF",ProjectedDividendFromPDF);
		                    writeToCSV("Value From GIAS",var_ProjectedDividend);

		                    if (ProjectedDividendFromPDF.equalsIgnoreCase(var_ProjectedDividend) && ProjectedDividendFromPDF != ""){
		                        writeToCSV("Status","PASS");
		                    }else{
		                        writeToCSV("Status","FAIL");
		                    }

		                    writeToCSV("Policy Number",var_PolicyNumber);
		                    writeToCSV("FieldName","Primary Applied");
		                    writeToCSV("Value From PDF",PrimaryAppliedFromPDF);
		                    writeToCSV("Value From GIAS",var_PrimaryApplied);

		                    if (PrimaryAppliedFromPDF.equalsIgnoreCase(var_PrimaryApplied) && PrimaryAppliedFromPDF != ""){
		                        writeToCSV("Status","PASS");
		                    }else{
		                        writeToCSV("Status","FAIL");
		                    }

		                    writeToCSV("Policy Number",var_PolicyNumber);
		                    writeToCSV("FieldName","Primary Option");
		                    writeToCSV("Value From PDF",OptFromPDF);
		                    writeToCSV("Value From GIAS",var_Opt);

		                    if (OptFromPDF.equalsIgnoreCase(var_Opt) && OptFromPDF != ""){
		                        writeToCSV("Status","PASS");
		                    }else{
		                        writeToCSV("Status","FAIL");
		                    }




		                }

		            }



		        
		 
		 ;} 
		 catch(Exception e) { PrintVal("Renderer Script Exception : "+e.toString()); } 
		 PrintVal("Executed Step = CALLRENDERERSCRIPT,PDFCOMPARISIONSCRIPTNW020E2E"); 

    }


}
