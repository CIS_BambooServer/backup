package appium.tgo;

import com.tg.octopus.base.TGOWrapper;
import com.tg.octopus.libs.testng.listener.TestListener;
import static com.tg.octopus.base.TGOWrapper.Action.*;
import static com.tg.octopus.base.TGOWrapper.LOGGER;
import com.tg.octopus.wrapper.ActionWrapper;

import com.jayway.jsonpath.JsonPath;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.SessionNotCreatedException;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.Augmenter;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.remote.UnreachableBrowserException;

import org.testng.Assert;
import org.testng.ITestContext;
import org.testng.ITestResult;
import org.testng.Reporter;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;
import java.net.HttpURLConnection;
import java.net.URL;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import org.json.JSONArray;
import org.json.JSONObject;
import helper.WebTestUtility;
import java.sql.*;
import com.testautomationguru.utility.PDFUtil;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.multipdf.Splitter;

import java.util.*;
import org.openqa.selenium.*;
import java.io.*;
import java.net.*;

import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;


@Listeners(TestListener.class)
public class login extends TGOWrapper {



    static {
       needsresetoncebeforetest ="false";
       needsresetbeforeeverymethod ="true";
       captureverystepsnap ="true";
       appendlastsnap ="false";
       ishybridapp ="true";

    }


    @Test
    public void edelivery01() {

        // No need to declare / print method name here, handled by TestNg listener
        // declare("edelivery01");

        CALL.$("LoginGWP","TGTYPESCREENREG");

        TYPE.$("ELE_USERID","userIDTGWEBCOMMAIdTGWEBCOMMA0","FD_Owner_GARRET","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_LOGINBUTTONNEXT","login.button.nextTGWEBCOMMAIdTGWEBCOMMA0","TGTYPESCREENREG");

		takeFullScreenShot();
                 LOGGER.info("Executed Step = TAKEFULLSCREENSHOT,TGTYPESCREENREG");
        WAIT.$("ELE_PASSWORD","passwordTGWEBCOMMAIdTGWEBCOMMA0","VISIBLE","TGTYPESCREENREG");

        TYPE.$("ELE_PASSWORD","passwordTGWEBCOMMAIdTGWEBCOMMA0","123123c!","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_LOGINBUTTONLOGIN","login.button.loginTGWEBCOMMAIdTGWEBCOMMA0","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

		try { 
		 driver.navigate().to("http://mmdev362portal2.ins-portal.com:10039/eDelivery/");
		driver.navigate().refresh();
		 
		 ;} 
		 catch(Exception e) { PrintVal("Renderer Script Exception : "+e.toString()); } 
		 PrintVal("Executed Step = CALLRENDERERSCRIPT,CHANGEURLTOEDELIVERY"); 
        WAIT.$(2,"TGTYPESCREENREG");

		takeFullScreenShot();
                 LOGGER.info("Executed Step = TAKEFULLSCREENSHOT,TGTYPESCREENREG");
		try { 
		 List<WebElement> col = driver.findElements(By.xpath("//body/app-root[1]/div[1]/div[1]/div[1]/app-document-list[1]/div[1]/div[2]/table[1]/tr"));
				 
				for(int i = 1; i< col.size();i++) {
		            String date ="//body/app-root[1]/div[1]/div[1]/div[1]/app-document-list[1]/div[1]/div[2]/table[1]/tr[X]/td[1]";
		            String type ="//body/app-root[1]/div[1]/div[1]/div[1]/app-document-list[1]/div[1]/div[2]/table[1]/tr[X]/td[2]";
		            String des ="//body/app-root[1]/div[1]/div[1]/div[1]/app-document-list[1]/div[1]/div[2]/table[1]/tr[X]/td[3]";
		            String policynumber ="//body/app-root[1]/div[1]/div[1]/div[1]/app-document-list[1]/div[1]/div[2]/table[1]/tr[X]/td[4]";
		            String a = Integer.toString(i);
		            String dateXpath = date.replaceAll("X",a);
		            WebElement datewebele = driver.findElement(By.xpath(dateXpath));
		            String applydate = datewebele.getText();
		            writeToCSV("Date",applydate);
		            String typeXpath = type.replaceAll("X", a);
		            WebElement typewebele = driver.findElement(By.xpath(typeXpath));
		            String typecsv = typewebele.getText();
		            writeToCSV("Type",typecsv);
		            String descXpath = des.replaceAll("X", a);
		            WebElement deswebele = driver.findElement(By.xpath(descXpath));
		            String desccsv = deswebele.getText();
		            writeToCSV("Description",desccsv);
		            String policyXpath = policynumber.replaceAll("X", a);
		            WebElement policywebele = driver.findElement(By.xpath(policyXpath));
		            String policynumbercsv = policywebele.getText();
		            writeToCSV("Policynumber",policynumbercsv);
		            
		            
				}
		 
		 ;} 
		 catch(Exception e) { PrintVal("Renderer Script Exception : "+e.toString()); } 
		 PrintVal("Executed Step = CALLRENDERERSCRIPT,READVALUESFROMEDELIVERYANDEXPORTTOCSV"); 
        ASSERT.$("ELE_FAQ","//a[contains(text(),'FAQ')]","CONTAINS","http://mmdev362portal2.ins-portal.com/","TGTYPESCREENREG");


    }


}
