package appium.tgo;

import com.tg.octopus.base.TGOWrapper;
import com.tg.octopus.libs.testng.listener.TestListener;
import static com.tg.octopus.base.TGOWrapper.Action.*;
import static com.tg.octopus.base.TGOWrapper.LOGGER;
import com.tg.octopus.wrapper.ActionWrapper;

import com.jayway.jsonpath.JsonPath;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.SessionNotCreatedException;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.Augmenter;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.remote.UnreachableBrowserException;

import org.testng.Assert;
import org.testng.ITestContext;
import org.testng.ITestResult;
import org.testng.Reporter;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;
import java.net.HttpURLConnection;
import java.net.URL;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import org.json.JSONArray;
import org.json.JSONObject;
import helper.WebTestUtility;
import java.sql.*;
import com.testautomationguru.utility.PDFUtil;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.multipdf.Splitter;

import java.util.*;
import org.openqa.selenium.*;
import java.io.*;
import java.net.*;

import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;


@Listeners(TestListener.class)
public class FTJ_Master_V_3_1 extends TGOWrapper {



    static {
       needsresetoncebeforetest ="false";
       needsresetbeforeeverymethod ="true";
       captureverystepsnap ="true";
       appendlastsnap ="false";
       ishybridapp ="true";

    }


    @Test
    public void ftj41reg002useridnotmodifiable() {

        // No need to declare / print method name here, handled by TestNg listener
        // declare("ftj41reg002useridnotmodifiable");

		int var_Count = 0;
                 LOGGER.info("Executed Step = VAR,Integer,var_Count,0,TGTYPESCREENREG");
		com.jayway.jsonpath.ReadContext var_CSVData = getJsonData("http://10.1.104.229/json/20201009/xuyVgY.json");
                 LOGGER.info("Executed Step = VAR,Json,var_CSVData,http://10.1.104.229/json/20201009/xuyVgY.json,TGTYPESCREENREG");
        LOGGER.info("Executed Step = REPEAT IF");
    	while (check(var_Count,"<",1,"TGTYPESCREENREG")) {
          LOGGER.info("Executed Step = WHILE,var_Count,<,1,TGTYPESCREENREG");
		String var_PolicyNumber = "null";
                 LOGGER.info("Executed Step = VAR,String,var_PolicyNumber,null,TGTYPESCREENREG");
		String var_Action = "null";
                 LOGGER.info("Executed Step = VAR,String,var_Action,null,TGTYPESCREENREG");
		String var_Notes;
                 LOGGER.info("Executed Step = VAR,String,var_Notes,TGTYPESCREENREG");
		String var_NotifyDate;
                 LOGGER.info("Executed Step = VAR,String,var_NotifyDate,TGTYPESCREENREG");
		var_Notes = getJsonData(var_CSVData , "$.records["+var_Count+"].Notes");
                 LOGGER.info("Executed Step = STORE,var_Notes,var_CSVData,$.records[{var_Count}].Notes,TGTYPESCREENREG");
		var_PolicyNumber = getJsonData(var_CSVData , "$.records["+var_Count+"].PolicyNumber");
                 LOGGER.info("Executed Step = STORE,var_PolicyNumber,var_CSVData,$.records[{var_Count}].PolicyNumber,TGTYPESCREENREG");
		var_NotifyDate = getJsonData(var_CSVData , "$.records["+var_Count+"].NotifyDate");
                 LOGGER.info("Executed Step = STORE,var_NotifyDate,var_CSVData,$.records[{var_Count}].NotifyDate,TGTYPESCREENREG");
        CALL.$("FTJQA2Login","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

		try { 
		 WebElement html = driver.findElement(By.tagName("html"));
		    	
		    	System.out.println("html size" + html.getSize());
		    	System.out.println("html" + html.getTagName());

		    	for( int i =0 ;i<4;i++) {
		        html.sendKeys(Keys.chord(Keys.CONTROL, Keys.SUBTRACT));
		    	}
		 
		 ;} 
		 catch(Exception e) { PrintVal("Renderer Script Exception : "+e.toString()); } 
		 PrintVal("Executed Step = CALLRENDERERSCRIPT,FTJ41ZOOMMAGNIFICATIONCHANGE"); 
        TYPE.$("ELE_TXTBOX_USERNAME","//input[@id='guestForm:username']","x5080","FALSE","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_PASSWORD","//input[@id='guestForm:password']","Welcome123$","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_LOGIN","//button[@id='guestForm:loginButton']","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        CALL.$("PREVALIDATEUSERIDNOTMODIFIABLE","TGTYPESCREENREG");

        TAP.$("ELE_TXTBOX_SEARCHTASKS","//input[@id='workbenchForm:workbenchTabs:taskSearchContainer:taskSearchSearch']","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_SEARCHTASKS","//input[@id='workbenchForm:workbenchTabs:taskSearchContainer:taskSearchSearch']","Profiles","FALSE","TGTYPESCREENREG");

        WAIT.$(1,"TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_TASKSEARCH","//a[@id='workbenchForm:workbenchTabs:taskSearchContainer:tb_buttonSearch']//span[@class='fa fa-search fa-lg']","TGTYPESCREENREG");

        WAIT.$(1,"TGTYPESCREENREG");

        TAP.$("ELE_LINK_SECURITYUSERPROFILES","//span[contains(text(),'Security User Profiles')]","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_USER_SECURITYGROUPS","//input[@id='workbenchForm:workbenchTabs:grid:username_Col:filter']","x5080","FALSE","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

        TAP.$("ELE_LINK_USERNAMESECUIRTYGROUPSROW1","//span[@id='workbenchForm:workbenchTabs:grid:0:username']","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SHOWLINKS","//span[@class='ui-button-icon-left ui-icon ui-c fa fa-ellipsis-v fa-2x']","TGTYPESCREENREG");

        TAP.$("ELE_LINK_ROLES","//span[contains(text(),'Roles')]","TGTYPESCREENREG");

        WAIT.$(3,"TGTYPESCREENREG");

        TAP.$("ELE_LINK_DISPLAYUSERPROFILE","//a[contains(text(),'Display User Profile')]","TGTYPESCREENREG");

        WAIT.$(1,"TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SHOWLINKS","//span[@class='ui-button-icon-left ui-icon ui-c fa fa-ellipsis-v fa-2x']","TGTYPESCREENREG");

        TAP.$("ELE_LINK_OVERRIDES_SHOWLINK","//span[contains(text(),'Overrides')]","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

        TAP.$("ELE_TXTBOX_SECURITYGROUPISSUESTATE","//input[@id='workbenchForm:workbenchTabs:grid:securityGroupName_Col:filter']","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_SECURITYGROUPISSUESTATE","//input[@id='workbenchForm:workbenchTabs:grid:securityGroupName_Col:filter']","NBS830","FALSE","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

        TAP.$("ELE_LINK_BASEBENEFIT_DONOTCHANGE","//div[@id='workbenchForm:workbenchTabs:grid']//td[1]","TGTYPESCREENREG");

		String var_Modifiable;
                 LOGGER.info("Executed Step = VAR,String,var_Modifiable,TGTYPESCREENREG");
		var_Modifiable = ActionWrapper.getElementValue("ELE_GETVAL_PTDCANBEMODIFIEDSTATUS", "//span[@id='workbenchForm:workbenchTabs:securityFlag1']");
                 LOGGER.info("Executed Step = STORE,var_Modifiable,ELE_GETVAL_PTDCANBEMODIFIEDSTATUS,TGTYPESCREENREG");
        TAP.$("ELE_LOGO_GIAS1","//span[contains(text(),'GIAS')]","TGTYPESCREENREG");

        TAP.$("ELE_TXTBOX_SEARCHTASKS","//input[@id='workbenchForm:workbenchTabs:taskSearchContainer:taskSearchSearch']","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_SEARCHTASKS","//input[@id='workbenchForm:workbenchTabs:taskSearchContainer:taskSearchSearch']","Roles","FALSE","TGTYPESCREENREG");

        WAIT.$(1,"TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_TASKSEARCH","//a[@id='workbenchForm:workbenchTabs:taskSearchContainer:tb_buttonSearch']//span[@class='fa fa-search fa-lg']","TGTYPESCREENREG");

        TAP.$("ELE_LINK_SECURITYROLES","//span[contains(text(),'Security Roles')]","TGTYPESCREENREG");

        WAIT.$(1,"TGTYPESCREENREG");

        TAP.$("ELE_TXTBOX_SECURITYGROUP","//input[@id='workbenchForm:workbenchTabs:grid:name_Col:filter']","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_SECURITYGROUP","//input[@id='workbenchForm:workbenchTabs:grid:name_Col:filter']","sysAdmin","FALSE","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

        TAP.$("ELE_LINK_SECURITYGROUPSFIRSTROW","//span[@id='workbenchForm:workbenchTabs:grid:0:name']","TGTYPESCREENREG");

        WAIT.$(1,"TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SHOWLINKS","//span[@class='ui-button-icon-left ui-icon ui-c fa fa-ellipsis-v fa-2x']","TGTYPESCREENREG");

        TAP.$("ELE_LINK_AUTHORIZATION","//span[contains(text(),'Authorizations')]","TGTYPESCREENREG");

		takeFullScreenShot();
                 LOGGER.info("Executed Step = TAKEFULLSCREENSHOT,TGTYPESCREENREG");
        TYPE.$("ELE_TXTBOX_SECURITYGROUPISSUESTATE","//input[@id='workbenchForm:workbenchTabs:grid:securityGroupName_Col:filter']","*ALL","FALSE","TGTYPESCREENREG");

        WAIT.$(1,"TGTYPESCREENREG");

        LOGGER.info("Executed Step = START IF");
        if (check("ELE_LINK_USERSECURITYGROUPROW1","//span[@id='workbenchForm:workbenchTabs:grid:0:securityGroupName']","VISIBLE","TGTYPESCREENREG")) {
         LOGGER.info("Executed Step = IF,ELE_LINK_USERSECURITYGROUPROW1,VISIBLE,TGTYPESCREENREG");
        TAP.$("ELE_LINK_USERSECURITYGROUPROW1","//span[@id='workbenchForm:workbenchTabs:grid:0:securityGroupName']","TGTYPESCREENREG");

        }
          LOGGER.info("Executed Step = ENDIF,TGTYPESCREENREG");
        WAIT.$(2,"TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_GIASHOME","//span[text()='GIAS']","TGTYPESCREENREG");

        TAP.$("ELE_TXTBOX_SEARCHTASKS","//input[@id='workbenchForm:workbenchTabs:taskSearchContainer:taskSearchSearch']","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_SEARCHTASKS","//input[@id='workbenchForm:workbenchTabs:taskSearchContainer:taskSearchSearch']","POLICY INQUIRY","FALSE","TGTYPESCREENREG");

        WAIT.$(1,"TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_TASKSEARCH","//a[@id='workbenchForm:workbenchTabs:taskSearchContainer:tb_buttonSearch']//span[@class='fa fa-search fa-lg']","TGTYPESCREENREG");

        WAIT.$(1,"TGTYPESCREENREG");

        TAP.$("ELE_LINK_POLICYINQUIRY","//span[contains(text(),'Policy Inquiry')]","TGTYPESCREENREG");

        WAIT.$(1,"TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_POLICYNUMBER","//input[@id='workbenchForm:workbenchTabs:policyNumber']",var_PolicyNumber,"FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_CONTINUE","//span[contains(text(),'Continue')]","TGTYPESCREENREG");

        WAIT.$(6,"TGTYPESCREENREG");

        ASSERT.$("ELE_GETVAL_POLICYSTATUS","//span[@id='workbenchForm:workbenchTabs:statusText']","CONTAINS","Premium Paying","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_GIASHOME","//span[text()='GIAS']","TGTYPESCREENREG");

        TAP.$("ELE_TXTBOX_SEARCHTASKS","//input[@id='workbenchForm:workbenchTabs:taskSearchContainer:taskSearchSearch']","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_SEARCHTASKS","//input[@id='workbenchForm:workbenchTabs:taskSearchContainer:taskSearchSearch']","Policy Notes","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_TASKSEARCH","//a[@id='workbenchForm:workbenchTabs:taskSearchContainer:tb_buttonSearch']//span[@class='fa fa-search fa-lg']","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

        TAP.$("ELE_LINK_POLICYNOTES","//span[contains(text(),'Policy Notes')]","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_POLICYNUMBER","//input[@id='workbenchForm:workbenchTabs:policyNumber']",var_PolicyNumber,"FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_CONTINUE","//span[contains(text(),'Continue')]","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_GIASHOME","//span[text()='GIAS']","TGTYPESCREENREG");

        WAIT.$(1,"TGTYPESCREENREG");

        CALL.$("VALIDATEUSERIDNOTMODIFIABLE","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_SEARCHTASKS","//input[@id='workbenchForm:workbenchTabs:taskSearchContainer:taskSearchSearch']","Policy Notes","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_TASKSEARCH","//a[@id='workbenchForm:workbenchTabs:taskSearchContainer:tb_buttonSearch']//span[@class='fa fa-search fa-lg']","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

        TAP.$("ELE_LINK_POLICYNOTES","//span[contains(text(),'Policy Notes')]","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_POLICYNUMBER","//input[@id='workbenchForm:workbenchTabs:policyNumber']",var_PolicyNumber,"FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_CONTINUE","//span[contains(text(),'Continue')]","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

        LOGGER.info("Executed Step = START IF");
        if (check("ELE_LINK_BASEBENEFIT_DONOTCHANGE","//div[@id='workbenchForm:workbenchTabs:grid']//td[1]","VISIBLE","TGTYPESCREENREG")) {
         LOGGER.info("Executed Step = IF,ELE_LINK_BASEBENEFIT_DONOTCHANGE,VISIBLE,TGTYPESCREENREG");
        TAP.$("ELE_LINK_BASEBENEFIT_DONOTCHANGE","//div[@id='workbenchForm:workbenchTabs:grid']//td[1]","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_EDITCLIENT","//span[@class='ui-button-text ui-c'][contains(text(),'Edit')]","TGTYPESCREENREG");

        }
         else {
          LOGGER.info("Executed Step = ELSE,TGTYPESCREENREG");
        TAP.$("ELE_BUTTON_ADD","//span[contains(text(),'Add')]","TGTYPESCREENREG");

        }
          LOGGER.info("Executed Step = ENDIF,TGTYPESCREENREG");
        WAIT.$(1,"TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_USER","//input[@id='workbenchForm:workbenchTabs:userID']",var_Notes,"FALSE","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_NOTIFYDATE","//input[@name='workbenchForm:workbenchTabs:notifyDate_input']",var_NotifyDate,"FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SAVE1","//span[contains(text(),'Save')]","TGTYPESCREENREG");

        WAIT.$(10,"TGTYPESCREENREG");

        ASSERT.$("ELE_MSG_ITEMSUCCESSUPDATE","//div[@id='workbenchForm:workbenchTabs:messages']//span[@class='ui-messages-info-summary'][contains(text(),'Item successfully updated.')]","CONTAINS","Item successfully updated.","TGTYPESCREENREG");

        CALL.$("POSTVALIDATEUSERIDNOTMODIFIABLE","TGTYPESCREENREG");

        TAP.$("ELE_LOGO_GIAS1","//span[contains(text(),'GIAS')]","TGTYPESCREENREG");

        WAIT.$(1,"TGTYPESCREENREG");

        TAP.$("ELE_TXTBOX_SEARCHTASKS","//input[@id='workbenchForm:workbenchTabs:taskSearchContainer:taskSearchSearch']","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_SEARCHTASKS","//input[@id='workbenchForm:workbenchTabs:taskSearchContainer:taskSearchSearch']","Policy Notes","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_TASKSEARCH","//a[@id='workbenchForm:workbenchTabs:taskSearchContainer:tb_buttonSearch']//span[@class='fa fa-search fa-lg']","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

        TAP.$("ELE_LINK_POLICYNOTES","//span[contains(text(),'Policy Notes')]","TGTYPESCREENREG");

        WAIT.$(1,"TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_POLICYNUMBER","//input[@id='workbenchForm:workbenchTabs:policyNumber']",var_PolicyNumber,"FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_CONTINUE","//span[contains(text(),'Continue')]","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

        ASSERT.$("ELE_GETVAL_USER","//span[@id='workbenchForm:workbenchTabs:grid:0:userID']","CONTAINS",var_Notes,"TGTYPESCREENREG");

		var_Count = var_Count + ( 1 );
                 LOGGER.info("Executed Step = INCREASEVAR,var_Count,1,TGTYPESCREENREG");
    	}
          LOGGER.info("Executed Step = WEND,TGTYPESCREENREG");
    	resetAttemps();

    }


    @Test
    public void ftj41reg003transactionihistorycreation() {

        // No need to declare / print method name here, handled by TestNg listener
        // declare("ftj41reg003transactionihistorycreation");

		int var_Count = 0;
                 LOGGER.info("Executed Step = VAR,Integer,var_Count,0,TGTYPESCREENREG");
		com.jayway.jsonpath.ReadContext var_CSVData = getJsonData("http://10.1.104.229/json/20200814/SeTsT6.json");
                 LOGGER.info("Executed Step = VAR,Json,var_CSVData,http://10.1.104.229/json/20200814/SeTsT6.json,TGTYPESCREENREG");
        LOGGER.info("Executed Step = REPEAT IF");
    	while (check(var_Count,"<",1,"TGTYPESCREENREG")) {
          LOGGER.info("Executed Step = WHILE,var_Count,<,1,TGTYPESCREENREG");
		String var_PolicyNumber = "null";
                 LOGGER.info("Executed Step = VAR,String,var_PolicyNumber,null,TGTYPESCREENREG");
		var_PolicyNumber = getJsonData(var_CSVData , "$.records["+var_Count+"].PolicyNumber");
                 LOGGER.info("Executed Step = STORE,var_PolicyNumber,var_CSVData,$.records[{var_Count}].PolicyNumber,TGTYPESCREENREG");
        CALL.$("FTJQA2Login","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

		try { 
		 WebElement html = driver.findElement(By.tagName("html"));
		    	
		    	System.out.println("html size" + html.getSize());
		    	System.out.println("html" + html.getTagName());

		    	for( int i =0 ;i<4;i++) {
		        html.sendKeys(Keys.chord(Keys.CONTROL, Keys.SUBTRACT));
		    	}
		 
		 ;} 
		 catch(Exception e) { PrintVal("Renderer Script Exception : "+e.toString()); } 
		 PrintVal("Executed Step = CALLRENDERERSCRIPT,FTJ41ZOOMMAGNIFICATIONCHANGE"); 
        TYPE.$("ELE_TXTBOX_USERNAME","//input[@id='guestForm:username']","x5080","FALSE","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_PASSWORD","//input[@id='guestForm:password']","Welcome123$","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_LOGIN","//button[@id='guestForm:loginButton']","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        CALL.$("VerifyPolicyStatus","TGTYPESCREENREG");

        TAP.$("ELE_TXTBOX_SEARCHTASKS","//input[@id='workbenchForm:workbenchTabs:taskSearchContainer:taskSearchSearch']","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_SEARCHTASKS","//input[@id='workbenchForm:workbenchTabs:taskSearchContainer:taskSearchSearch']","POLICY INQUIRY","FALSE","TGTYPESCREENREG");

        WAIT.$(1,"TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_TASKSEARCH","//a[@id='workbenchForm:workbenchTabs:taskSearchContainer:tb_buttonSearch']//span[@class='fa fa-search fa-lg']","TGTYPESCREENREG");

        WAIT.$(1,"TGTYPESCREENREG");

        TAP.$("ELE_LINK_POLICYINQUIRY","//span[contains(text(),'Policy Inquiry')]","TGTYPESCREENREG");

        WAIT.$(1,"TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_POLICYNUMBER","//input[@id='workbenchForm:workbenchTabs:policyNumber']",var_PolicyNumber,"FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_CONTINUE","//span[contains(text(),'Continue')]","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        ASSERT.$("ELE_GETVAL_POLICYSTATUS","//span[@id='workbenchForm:workbenchTabs:statusText']","CONTAINS","Premium Paying","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

		String var_effectiveDate;
                 LOGGER.info("Executed Step = VAR,String,var_effectiveDate,TGTYPESCREENREG");
		var_effectiveDate = ActionWrapper.getElementValue("ELE_LABEL_EFFECTIVE_DATEGRID", "//span[@id='workbenchForm:workbenchTabs:benefitTable:0:effectiveDate']");
                 LOGGER.info("Executed Step = STORE,var_effectiveDate,ELE_LABEL_EFFECTIVE_DATEGRID,TGTYPESCREENREG");
		int var_NoHistoryYears = 0;
                 LOGGER.info("Executed Step = VAR,Integer,var_NoHistoryYears,0,TGTYPESCREENREG");
		try { 
		  java.text.DateFormat formatter = new java.text.SimpleDateFormat("MM/dd/yyyy");
		        		 try {
		        			 java.util.Date date = formatter.parse(var_effectiveDate);
		        			 Calendar calendar = Calendar.getInstance();
		        			 calendar.setTime(date);
		        			 System.out.println("calendar:::::::::"+calendar);
		        			 Calendar today = Calendar.getInstance();
		        			 int curYear = today.get(Calendar.YEAR);
		        			 int curMonth = today.get(Calendar.MONTH);
		        			 int curDay = today.get(Calendar.DAY_OF_MONTH);

		        			 int year = calendar.get(Calendar.YEAR);
		        			 int month = calendar.get(Calendar.MONTH);
		        			 int day = calendar.get(Calendar.DAY_OF_MONTH);

		        			 int age = curYear - year;
		        			 if (curMonth < month || (month == curMonth && curDay < day)) {
		        			     age--;
		        			 }
		        			 var_NoHistoryYears = age;
		        			 System.out.println("var_NoHistoryYears::::::::"+var_NoHistoryYears);
		        			 
		        		 } catch (java.text.ParseException e) {
		        		   e.printStackTrace();
		}
		 
		 ;} 
		 catch(Exception e) { PrintVal("Renderer Script Exception : "+e.toString()); } 
		 PrintVal("Executed Step = CALLRENDERERSCRIPT,CURRENTDATEANDANVERSARYDATECALUCATION"); 
        TAP.$("ELE_LINK_BASE","//div[@id='workbenchForm:workbenchTabs:benefitTable']//td[1]","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

		String var_NoOfUnits;
                 LOGGER.info("Executed Step = VAR,String,var_NoOfUnits,TGTYPESCREENREG");
		var_NoOfUnits = ActionWrapper.getElementValue("ELE_LABEL_UNITOFINSURED", "//span[@id='workbenchForm:workbenchTabs:unitsOfInsurance']");
                 LOGGER.info("Executed Step = STORE,var_NoOfUnits,ELE_LABEL_UNITOFINSURED,TGTYPESCREENREG");
        TAP.$("ELE_BUTTON_SHOWLINKS","//span[@class='ui-button-icon-left ui-icon ui-c fa fa-ellipsis-v fa-2x']","TGTYPESCREENREG");

        TAP.$("ELE_LINK_PLANS","//span[@class='ui-menuitem-text'][contains(text(),'Plan')]","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

		String var_tableName;
                 LOGGER.info("Executed Step = VAR,String,var_tableName,TGTYPESCREENREG");
		var_tableName = ActionWrapper.getElementValue("ELE_GETVAL_VALUETABLE", "//span[@id='workbenchForm:workbenchTabs:unitValueTable']");
                 LOGGER.info("Executed Step = STORE,var_tableName,ELE_GETVAL_VALUETABLE,TGTYPESCREENREG");
        SWIPE.$("UP","TGTYPESCREENREG");

        SWIPE.$("UP","TGTYPESCREENREG");

        TAP.$("ELE_TAB_MYDASHBOARD","//a[contains(text(),'My Dashboard')]","TGTYPESCREENREG");

        WAIT.$(1,"TGTYPESCREENREG");

        SWIPE.$("DOWN","TGTYPESCREENREG");

        SWIPE.$("DOWN","TGTYPESCREENREG");

        TAP.$("ELE_TXTBOX_SEARCHTASKS","//input[@id='workbenchForm:workbenchTabs:taskSearchContainer:taskSearchSearch']","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_SEARCHTASKS","//input[@id='workbenchForm:workbenchTabs:taskSearchContainer:taskSearchSearch']","Value per Unit","FALSE","TGTYPESCREENREG");

        WAIT.$(1,"TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_TASKSEARCH","//a[@id='workbenchForm:workbenchTabs:taskSearchContainer:tb_buttonSearch']//span[@class='fa fa-search fa-lg']","TGTYPESCREENREG");

        WAIT.$(1,"TGTYPESCREENREG");

        TAP.$("ELE_LINK_VALUEPERUNIT","//span[contains(text(),'Value Per Unit')]","TGTYPESCREENREG");

        WAIT.$(1,"TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_VALUEPERTABLE","//input[@id='workbenchForm:workbenchTabs:grid:unitValueTable_Col:filter']",var_tableName,"FALSE","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

		String var_ValuePerUnit1;
                 LOGGER.info("Executed Step = VAR,String,var_ValuePerUnit1,TGTYPESCREENREG");
		var_ValuePerUnit1 = "null";
                 LOGGER.info("Executed Step = STORE,var_ValuePerUnit1,null,TGTYPESCREENREG");
		String var_ValuePerUnit2;
                 LOGGER.info("Executed Step = VAR,String,var_ValuePerUnit2,TGTYPESCREENREG");
		var_ValuePerUnit2 = "null";
                 LOGGER.info("Executed Step = STORE,var_ValuePerUnit2,null,TGTYPESCREENREG");
		String var_ValuePerUnit3;
                 LOGGER.info("Executed Step = VAR,String,var_ValuePerUnit3,TGTYPESCREENREG");
		var_ValuePerUnit3 = "null";
                 LOGGER.info("Executed Step = STORE,var_ValuePerUnit3,null,TGTYPESCREENREG");
		String var_ValuePerUnit4;
                 LOGGER.info("Executed Step = VAR,String,var_ValuePerUnit4,TGTYPESCREENREG");
		var_ValuePerUnit4 = "null";
                 LOGGER.info("Executed Step = STORE,var_ValuePerUnit4,null,TGTYPESCREENREG");
		String var_ValuePerUnit5;
                 LOGGER.info("Executed Step = VAR,String,var_ValuePerUnit5,TGTYPESCREENREG");
		var_ValuePerUnit5 = "null";
                 LOGGER.info("Executed Step = STORE,var_ValuePerUnit5,null,TGTYPESCREENREG");
		String var_ValuePerUnit0;
                 LOGGER.info("Executed Step = VAR,String,var_ValuePerUnit0,TGTYPESCREENREG");
		var_ValuePerUnit0 = "null";
                 LOGGER.info("Executed Step = STORE,var_ValuePerUnit0,null,TGTYPESCREENREG");
		try { 
		  int prefix =1;
				WebElement valueforzero = driver.findElement(By.xpath("//span[@id='workbenchForm:workbenchTabs:grid:1:unitValue']"));
				          var_ValuePerUnit0 = valueforzero.getText();
				         for(int i= 2; i<=(var_NoHistoryYears+1) ;i++) {
					        	
					        	String a = Integer.toString(i);
					        
					        	String prefixstring = Integer.toString(prefix);
					        	
					        	String XpathFordateRow ="//span[@id='workbenchForm:workbenchTabs:grid:R:unitValue']";
					        	String XpathForRowR = XpathFordateRow.replace("R", a);
					          	WebElement value = driver.findElement(By.xpath(XpathForRowR));
					            String d =value.getText();
					           
					            if(i==2) {
					            	var_ValuePerUnit1 =d;
					            	
					            }
					            if(i==3)
					            {
					            	var_ValuePerUnit2=d;
					            }
					            if(i==4)
					            {
					            	var_ValuePerUnit3=d;
					            }
					            if(i==5)
					            {
					            	var_ValuePerUnit4=d;
					            }
					            if(i==6)
					            {
					            	var_ValuePerUnit5=d;
					            }
					        
					           System.out.println("var_ValuePerUnit1:"+var_ValuePerUnit1);
					           System.out.println("var_ValuePerUnit2::"+var_ValuePerUnit2);
					            
				         }
		 
		 ;} 
		 catch(Exception e) { PrintVal("Renderer Script Exception : "+e.toString()); } 
		 PrintVal("Executed Step = CALLRENDERERSCRIPT,ASIGNVALUESTOVARIABLES"); 
        CALL.$("ValidateTransactionHistory","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_GIASHOME","//span[text()='GIAS']","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

        SWIPE.$("DOWN","TGTYPESCREENREG");

        SWIPE.$("DOWN","TGTYPESCREENREG");

        TAP.$("ELE_TXTBOX_SEARCHTASKS","//input[@id='workbenchForm:workbenchTabs:taskSearchContainer:taskSearchSearch']","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_SEARCHTASKS","//input[@id='workbenchForm:workbenchTabs:taskSearchContainer:taskSearchSearch']","POLICY INQUIRY","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_TASKSEARCH","//a[@id='workbenchForm:workbenchTabs:taskSearchContainer:tb_buttonSearch']//span[@class='fa fa-search fa-lg']","TGTYPESCREENREG");

        TAP.$("ELE_LINK_POLICYINQUIRY","//span[contains(text(),'Policy Inquiry')]","TGTYPESCREENREG");

        WAIT.$(1,"TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_POLICYNUMBER","//input[@id='workbenchForm:workbenchTabs:policyNumber']",var_PolicyNumber,"FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_CONTINUE","//span[contains(text(),'Continue')]","TGTYPESCREENREG");

        WAIT.$(1,"TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SHOWLINKS","//span[@class='ui-button-icon-left ui-icon ui-c fa fa-ellipsis-v fa-2x']","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_CLIENTTRANSACTIONHISTORY","//span[contains(text(),'Transaction History')]","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_USER_HISTORY","//input[@id='workbenchForm:workbenchTabs:grid:userID_Col:filter']","CAS910","FALSE","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

		try { 
		  List<WebElement> col = driver.findElements(By.xpath("//tbody[@id='workbenchForm:workbenchTabs:grid_data']//tr"));
		        System.out.println("No of cols are : " +col.size()); 
		        int b = col.size()-1;
		        
		       
		        
		        for(int i= 0; i<col.size();i++) {
		        	
		        	String a = Integer.toString(i);
		        	  	
		        	String XpathFordateRow ="//span[@id='workbenchForm:workbenchTabs:grid:R:transactionDate']";
		        	String XpathForRowR = XpathFordateRow.replace("R",a);
		          	WebElement value = driver.findElement(By.xpath(XpathForRowR));
		          	value.click();
		            WAIT.$(2,"TGTYPESCREENREG");
		            takeFullScreenShot();
		            WebElement close = driver.findElement(By.xpath("//li[@class='ui-state-default ui-tabs-selected ui-state-active ui-corner-top']//span[@class='ui-icon ui-icon-close']"));
		            close.click();
		            WAIT.$(2,"TGTYPESCREENREG");
		          	
		          	
		          	
		        }
		 
		 ;} 
		 catch(Exception e) { PrintVal("Renderer Script Exception : "+e.toString()); } 
		 PrintVal("Executed Step = CALLRENDERERSCRIPT,TABONTRASACTIONDATE"); 
		var_Count = var_Count + ( 1 );
                 LOGGER.info("Executed Step = INCREASEVAR,var_Count,1,TGTYPESCREENREG");
    	}
          LOGGER.info("Executed Step = WEND,TGTYPESCREENREG");
    	resetAttemps();

    }


    @Test
    public void ftj41modf234validationswhenminimumreuiredpremiumequalszero() {

        // No need to declare / print method name here, handled by TestNg listener
        // declare("ftj41modf234validationswhenminimumreuiredpremiumequalszero");

		int var_Count = 0;
                 LOGGER.info("Executed Step = VAR,Integer,var_Count,0,TGTYPESCREENREG");
		com.jayway.jsonpath.ReadContext var_CSVData = getJsonData("http://10.1.104.229/json/20201009/PzuVO8.json");
                 LOGGER.info("Executed Step = VAR,Json,var_CSVData,http://10.1.104.229/json/20201009/PzuVO8.json,TGTYPESCREENREG");
        LOGGER.info("Executed Step = REPEAT IF");
    	while (check(var_Count,"<",1,"TGTYPESCREENREG")) {
          LOGGER.info("Executed Step = WHILE,var_Count,<,1,TGTYPESCREENREG");
		String var_User;
                 LOGGER.info("Executed Step = VAR,String,var_User,TGTYPESCREENREG");
		var_User = getJsonData(var_CSVData , "$.records["+var_Count+"].User");
                 LOGGER.info("Executed Step = STORE,var_User,var_CSVData,$.records[{var_Count}].User,TGTYPESCREENREG");
		String var_PolicyNumber;
                 LOGGER.info("Executed Step = VAR,String,var_PolicyNumber,TGTYPESCREENREG");
		var_PolicyNumber = getJsonData(var_CSVData , "$.records["+var_Count+"].PolicyNumber");
                 LOGGER.info("Executed Step = STORE,var_PolicyNumber,var_CSVData,$.records[{var_Count}].PolicyNumber,TGTYPESCREENREG");
		String var_MonthsPTDCanAdvanceAtSettle;
                 LOGGER.info("Executed Step = VAR,String,var_MonthsPTDCanAdvanceAtSettle,TGTYPESCREENREG");
		var_MonthsPTDCanAdvanceAtSettle = getJsonData(var_CSVData , "$.records["+var_Count+"].MonthsPTDCanAdvanceAtSettle");
                 LOGGER.info("Executed Step = STORE,var_MonthsPTDCanAdvanceAtSettle,var_CSVData,$.records[{var_Count}].MonthsPTDCanAdvanceAtSettle,TGTYPESCREENREG");
        CALL.$("FTJQA2Login","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

		try { 
		 WebElement html = driver.findElement(By.tagName("html"));
		    	
		    	System.out.println("html size" + html.getSize());
		    	System.out.println("html" + html.getTagName());

		    	for( int i =0 ;i<4;i++) {
		        html.sendKeys(Keys.chord(Keys.CONTROL, Keys.SUBTRACT));
		    	}
		 
		 ;} 
		 catch(Exception e) { PrintVal("Renderer Script Exception : "+e.toString()); } 
		 PrintVal("Executed Step = CALLRENDERERSCRIPT,FTJ41ZOOMMAGNIFICATIONCHANGE"); 
        TYPE.$("ELE_TXTBOX_USERNAME","//input[@id='guestForm:username']","x5080","FALSE","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_PASSWORD","//input[@id='guestForm:password']","Welcome123$","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_LOGIN","//button[@id='guestForm:loginButton']","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        CALL.$("ValidatinggUserAuthorizationEqualsYES","TGTYPESCREENREG");

        TAP.$("ELE_TXTBOX_SEARCHTASKS","//input[@id='workbenchForm:workbenchTabs:taskSearchContainer:taskSearchSearch']","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_SEARCHTASKS","//input[@id='workbenchForm:workbenchTabs:taskSearchContainer:taskSearchSearch']","Security User Profiles","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_TASKSEARCH","//a[@id='workbenchForm:workbenchTabs:taskSearchContainer:tb_buttonSearch']//span[@class='fa fa-search fa-lg']","TGTYPESCREENREG");

        TAP.$("ELE_LINK_SECURITYUSERPROFILES","//span[contains(text(),'Security User Profiles')]","TGTYPESCREENREG");

        TAP.$("ELE_TXTBOX_USER_SECURITYGROUPS","//input[@id='workbenchForm:workbenchTabs:grid:username_Col:filter']","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_USER_SECURITYGROUPS","//input[@id='workbenchForm:workbenchTabs:grid:username_Col:filter']",var_User,"FALSE","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

        TAP.$("ELE_LINK_USERNAMESECUIRTYGROUPSROW1","//span[@id='workbenchForm:workbenchTabs:grid:0:username']","TGTYPESCREENREG");

        WAIT.$(3,"TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SHOWLINK","//span[@class='ui-button-icon-left ui-icon ui-c fa fa-ellipsis-v fa-2x']","TGTYPESCREENFULL");

        WAIT.$(1,"TGTYPESCREENREG");

        TAP.$("ELE_LINK_OVERRIDES_SHOWLINK","//span[contains(text(),'Overrides')]","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

        TAP.$("ELE_TXTBOX_SECURITYGROUPISSUESTATE","//input[@id='workbenchForm:workbenchTabs:grid:securityGroupName_Col:filter']","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_SECURITYGROUPISSUESTATE","//input[@id='workbenchForm:workbenchTabs:grid:securityGroupName_Col:filter']","NBS830","FALSE","TGTYPESCREENREG");

        WAIT.$(1,"TGTYPESCREENREG");

        ASSERT.$("ELE_LINK_USERSECURITYGROUPROW1","//span[@id='workbenchForm:workbenchTabs:grid:0:securityGroupName']","CONTAINS","NBS830","TGTYPESCREENREG");

        TAP.$("ELE_LINK_USERSECURITYGROUPROW1","//span[@id='workbenchForm:workbenchTabs:grid:0:securityGroupName']","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

        LOGGER.info("Executed Step = START IF");
        if (check("ELE_GETVAL_PTDCANBEMODIFIEDSTATUS","//span[@id='workbenchForm:workbenchTabs:securityFlag1']","CONTAINS","No","TGTYPESCREENFULL")) {
         LOGGER.info("Executed Step = IF,ELE_GETVAL_PTDCANBEMODIFIEDSTATUS,CONTAINS,No,TGTYPESCREENFULL");
        CALL.$("UpdateUserAuthorityForPTDCanBeModifiedToYES","TGTYPESCREENREG");

        WAIT.$(1,"TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_EDIT","//span[text()='Edit']","TGTYPESCREENFULL");

        TAP.$("ELE_BUTTON_PTDCANBEMODIFIED_OPPOSITE","//table[@id='workbenchForm:workbenchTabs:securityFlag1']//span[@class='ui-radiobutton-icon ui-icon ui-icon-blank ui-c']","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SAVE","//span[contains(text(),'Save')]","TGTYPESCREENREG");

        ASSERT.$("ELE_GETVAL_PTDCANBEMODIFIEDSTATUS","//span[@id='workbenchForm:workbenchTabs:securityFlag1']","CONTAINS","Yes","TGTYPESCREENFULL");

        TAP.$("ELE_LABEL_MENUAVATAR","//span[@id='headerForm:avatarInitialsText']","TGTYPESCREENREG");

        TAP.$("ELE_MENU_LOGOUT","//span[contains(text(),'Log Out')]","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_GOTOLOGIN","//span[@class='ui-button-text ui-c']","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_USERNAME","//input[@id='guestForm:username']","x5080","FALSE","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_PASSWORD","//input[@id='guestForm:password']","Welcome123$","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_LOGIN","//button[@id='guestForm:loginButton']","TGTYPESCREENREG");

        }
          LOGGER.info("Executed Step = ENDIF,TGTYPESCREENREG");
        CALL.$("ModesNotVisAndPaymentSettleFieldValidations","TGTYPESCREENREG");

        TAP.$("ELE_LOGO_GIAS","//span[text()='GIAS']","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

        SWIPE.$("DOWN","TGTYPESCREENREG");

        SWIPE.$("DOWN","TGTYPESCREENREG");

        TAP.$("ELE_TXTBOX_SEARCHTASKS","//input[@id='workbenchForm:workbenchTabs:taskSearchContainer:taskSearchSearch']","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_SEARCHTASKS","//input[@id='workbenchForm:workbenchTabs:taskSearchContainer:taskSearchSearch']","New Business Policy Prop","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_TASKSEARCH","//a[@id='workbenchForm:workbenchTabs:taskSearchContainer:tb_buttonSearch']//span[@class='fa fa-search fa-lg']","TGTYPESCREENREG");

        TAP.$("ELE_LINK_NEWBUSINESSPOLICYPROPERTIES","//span[contains(text(),'New Business Policy Properties')]","TGTYPESCREENREG");

        WAIT.$(1,"TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_EDIT","//span[text()='Edit']","TGTYPESCREENREG");

        WAIT.$(1,"TGTYPESCREENREG");

        TAP.$("ELE_TXTBOX_MONTHSPTDCANBEADVANCEDATSETTLE","//input[@id='workbenchForm:workbenchTabs:settlePtdAdvanceMonths_input']","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_MONTHSPTDCANBEADVANCEDATSETTLE","//input[@id='workbenchForm:workbenchTabs:settlePtdAdvanceMonths_input']",var_MonthsPTDCanAdvanceAtSettle,"FALSE","TGTYPESCREENFULL");

        TAP.$("ELE_BUTTON_SAVE","//span[contains(text(),'Save')]","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

        ASSERT.$("ELE_GETVAL_MONTHSPTDCANADVANCEATSETTLE","//span[@id='workbenchForm:workbenchTabs:settlePtdAdvanceMonths']","CONTAINS",var_MonthsPTDCanAdvanceAtSettle,"TGTYPESCREENFULL");

        TAP.$("ELE_LOGO_GIAS","//span[text()='GIAS']","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

        SWIPE.$("DOWN","TGTYPESCREENREG");

        SWIPE.$("DOWN","TGTYPESCREENREG");

        TAP.$("ELE_TXTBOX_SEARCHTASKS","//input[@id='workbenchForm:workbenchTabs:taskSearchContainer:taskSearchSearch']","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_SEARCHTASKS","//input[@id='workbenchForm:workbenchTabs:taskSearchContainer:taskSearchSearch']","Create Inforce Policy","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_TASKSEARCH","//a[@id='workbenchForm:workbenchTabs:taskSearchContainer:tb_buttonSearch']//span[@class='fa fa-search fa-lg']","TGTYPESCREENREG");

        TAP.$("ELE_LINK_CREATEINFROCEPOLICYFROMPOLICYAPPLICATION","//body/div[@id='Outer40']/div[@id='ContentPanel']/div[@id='Content40']/form[@id='workbenchForm']/div[@id='PageTiles']/div[@id='workbenchForm:workbenchTabs']/div[1]/div[1]/div[1]/div[2]/div[2]/div[1]/div[1]/div[1]/div[3]/div[1]/div[1]/button[1]/span[2]","TGTYPESCREENREG");

        WAIT.$(3,"TGTYPESCREENREG");

        TAP.$("ELE_TXTBOX_SEARCHPOLICYNUMBER","//*[@name='workbenchForm:workbenchTabs:grid:policyNumber_Col:filter']","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_SEARCHPOLICYNUMBER","//*[@name='workbenchForm:workbenchTabs:grid:policyNumber_Col:filter']",var_PolicyNumber,"FALSE","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

		// Makes Months PTD greater than zero (input data). Brings navigation to Create Inforce Policy page;
        WAIT.$(1,"TGTYPESCREENREG");

        TAP.$("ELE_LINK_FIRSTPOLICYNUMBER","//span[@id='workbenchForm:workbenchTabs:grid:0:policyNumber']",1,0,"TGTYPESCREENREG");

        WAIT.$(3,"TGTYPESCREENREG");

        ASSERT.$("ELE_TXTBOX_MODESTOADVANCEPAIDTODATE","//input[@id='workbenchForm:workbenchTabs:modesToAdvancePtd_input']","INVISIBLE","TGTYPESCREENFULL");

        TAP.$("ELE_BUTTON_VALIDATE","//span[contains(text(),'Validate')]",1,0,"TGTYPESCREENREG");

        ASSERT.$("ELE_GETMSG_SUCCESS","//div[@id='workbenchForm:workbenchTabs:messages']//span[@class='ui-messages-info-summary']","VISIBLE","TGTYPESCREENFULL");

        WAIT.$(1,"TGTYPESCREENREG");

        TAP.$("ELE_TXTBOX_PAYMENTSETTLE","//input[@id='workbenchForm:workbenchTabs:paymentAtSettle_input']",1,0,"TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_PAYMENTSETTLE","//input[@id='workbenchForm:workbenchTabs:paymentAtSettle_input']","0","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SUBMIT","//span[contains(text(),'Submit')]",1,0,"TGTYPESCREENFULL");

        WAIT.$(2,"TGTYPESCREENREG");

		try { 
		 driver.switchTo().frame(0);
		driver.findElement(By.xpath("//form[@id='confirmDialogForm']")).click();
		Thread.sleep(1000);
		driver.findElement(By.xpath("//button[@id='j_idt9']")).click();
		// Add Screenshot will only work for Android platform

		takeScreenshot();

		 
		 ;} 
		 catch(Exception e) { PrintVal("Renderer Script Exception : "+e.toString()); } 
		 PrintVal("Executed Step = CALLRENDERERSCRIPT,CLICKYESONCONFIRMACTION"); 
        WAIT.$(6,"TGTYPESCREENREG");

        ASSERT.$("ELE_GETVAL_SUCCESSMESSAGE","//div[@id='workbenchForm:workbenchTabs:messages']//span[@class='ui-messages-info-summary'][contains(text(),'Request completed successfully.')]","VISIBLE","TGTYPESCREENFULL");

		var_Count = var_Count + ( 1 );
                 LOGGER.info("Executed Step = INCREASEVAR,var_Count,1,TGTYPESCREENREG");
    	}
          LOGGER.info("Executed Step = WEND,TGTYPESCREENREG");
    	resetAttemps();

    }


    @Test
    public void ftj41modf234userroleauthorityandadvanceptdvalidations() {

        // No need to declare / print method name here, handled by TestNg listener
        // declare("ftj41modf234userroleauthorityandadvanceptdvalidations");

		int var_Count = 0;
                 LOGGER.info("Executed Step = VAR,Integer,var_Count,0,TGTYPESCREENREG");
		com.jayway.jsonpath.ReadContext var_CSVData = getJsonData("http://10.1.104.229/json/20201009/p0HDv0.json");
                 LOGGER.info("Executed Step = VAR,Json,var_CSVData,http://10.1.104.229/json/20201009/p0HDv0.json,TGTYPESCREENREG");
        LOGGER.info("Executed Step = REPEAT IF");
    	while (check(var_Count,"<",1,"TGTYPESCREENREG")) {
          LOGGER.info("Executed Step = WHILE,var_Count,<,1,TGTYPESCREENREG");
		String var_User;
                 LOGGER.info("Executed Step = VAR,String,var_User,TGTYPESCREENREG");
		var_User = getJsonData(var_CSVData , "$.records["+var_Count+"].User");
                 LOGGER.info("Executed Step = STORE,var_User,var_CSVData,$.records[{var_Count}].User,TGTYPESCREENREG");
		String var_PolicyNumber;
                 LOGGER.info("Executed Step = VAR,String,var_PolicyNumber,TGTYPESCREENREG");
		var_PolicyNumber = getJsonData(var_CSVData , "$.records["+var_Count+"].PolicyNumber");
                 LOGGER.info("Executed Step = STORE,var_PolicyNumber,var_CSVData,$.records[{var_Count}].PolicyNumber,TGTYPESCREENREG");
		String var_MonthsPTDCanAdvanceAtSettle;
                 LOGGER.info("Executed Step = VAR,String,var_MonthsPTDCanAdvanceAtSettle,TGTYPESCREENREG");
		var_MonthsPTDCanAdvanceAtSettle = getJsonData(var_CSVData , "$.records["+var_Count+"].MonthsPTDCanAdvanceAtSettle");
                 LOGGER.info("Executed Step = STORE,var_MonthsPTDCanAdvanceAtSettle,var_CSVData,$.records[{var_Count}].MonthsPTDCanAdvanceAtSettle,TGTYPESCREENREG");
        CALL.$("FTJQA2Login","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

		try { 
		 WebElement html = driver.findElement(By.tagName("html"));
		    	
		    	System.out.println("html size" + html.getSize());
		    	System.out.println("html" + html.getTagName());

		    	for( int i =0 ;i<4;i++) {
		        html.sendKeys(Keys.chord(Keys.CONTROL, Keys.SUBTRACT));
		    	}
		 
		 ;} 
		 catch(Exception e) { PrintVal("Renderer Script Exception : "+e.toString()); } 
		 PrintVal("Executed Step = CALLRENDERERSCRIPT,FTJ41ZOOMMAGNIFICATIONCHANGE"); 
        TYPE.$("ELE_TXTBOX_USERNAME","//input[@id='guestForm:username']","x5080","FALSE","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_PASSWORD","//input[@id='guestForm:password']","Welcome123$","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_LOGIN","//button[@id='guestForm:loginButton']","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        CALL.$("SecurityGroupValidationsAndUserAuthorityCheck","TGTYPESCREENREG");

        TAP.$("ELE_TXTBOX_SEARCHTASKS","//input[@id='workbenchForm:workbenchTabs:taskSearchContainer:taskSearchSearch']","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_SEARCHTASKS","//input[@id='workbenchForm:workbenchTabs:taskSearchContainer:taskSearchSearch']","Security Groups","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_TASKSEARCH","//a[@id='workbenchForm:workbenchTabs:taskSearchContainer:tb_buttonSearch']//span[@class='fa fa-search fa-lg']","TGTYPESCREENREG");

        TAP.$("ELE_LINK_SECURITYGROUPS","//span[contains(text(),'Security Groups')]","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_SECURITYGROUP","//input[@id='workbenchForm:workbenchTabs:grid:name_Col:filter']","NBS830","FALSE","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

        TAP.$("ELE_LINK_SECURITYGROUPSFIRSTROW","//span[@id='workbenchForm:workbenchTabs:grid:0:name']","TGTYPESCREENREG");

        ASSERT.$("ELE_GETVAL_SECURITYFLAG1","//span[@id='workbenchForm:workbenchTabs:securityFlag1Text']","CONTAINS","Paid To Date Can Be Modified","TGTYPESCREENFULL");

        TAP.$("ELE_LOGO_GIAS","//span[text()='GIAS']","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

        SWIPE.$("DOWN","TGTYPESCREENREG");

        SWIPE.$("DOWN","TGTYPESCREENREG");

        TAP.$("ELE_TXTBOX_SEARCHTASKS","//input[@id='workbenchForm:workbenchTabs:taskSearchContainer:taskSearchSearch']","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_SEARCHTASKS","//input[@id='workbenchForm:workbenchTabs:taskSearchContainer:taskSearchSearch']","Security User Profiles","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_TASKSEARCH","//a[@id='workbenchForm:workbenchTabs:taskSearchContainer:tb_buttonSearch']//span[@class='fa fa-search fa-lg']","TGTYPESCREENREG");

        TAP.$("ELE_LINK_SECURITYUSERPROFILES","//span[contains(text(),'Security User Profiles')]","TGTYPESCREENREG");

        TAP.$("ELE_TXTBOX_USER_SECURITYGROUPS","//input[@id='workbenchForm:workbenchTabs:grid:username_Col:filter']","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_USER_SECURITYGROUPS","//input[@id='workbenchForm:workbenchTabs:grid:username_Col:filter']",var_User,"FALSE","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

        TAP.$("ELE_LINK_USERNAMESECUIRTYGROUPSROW1","//span[@id='workbenchForm:workbenchTabs:grid:0:username']","TGTYPESCREENREG");

        WAIT.$(1,"TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SHOWLINK","//span[@class='ui-button-icon-left ui-icon ui-c fa fa-ellipsis-v fa-2x']","TGTYPESCREENFULL");

        WAIT.$(1,"TGTYPESCREENREG");

        TAP.$("ELE_LINK_OVERRIDES_SHOWLINK","//span[contains(text(),'Overrides')]","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

        TAP.$("ELE_TXTBOX_SECURITYGROUPISSUESTATE","//input[@id='workbenchForm:workbenchTabs:grid:securityGroupName_Col:filter']","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_SECURITYGROUPISSUESTATE","//input[@id='workbenchForm:workbenchTabs:grid:securityGroupName_Col:filter']","NBS830","FALSE","TGTYPESCREENREG");

        WAIT.$(1,"TGTYPESCREENREG");

        ASSERT.$("ELE_LINK_USERSECURITYGROUPROW1","//span[@id='workbenchForm:workbenchTabs:grid:0:securityGroupName']","CONTAINS","NBS830","TGTYPESCREENREG");

        TAP.$("ELE_LINK_USERSECURITYGROUPROW1","//span[@id='workbenchForm:workbenchTabs:grid:0:securityGroupName']","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

        LOGGER.info("Executed Step = START IF");
        if (check("ELE_GETVAL_PTDCANBEMODIFIEDSTATUS","//span[@id='workbenchForm:workbenchTabs:securityFlag1']","CONTAINS","Yes","TGTYPESCREENFULL")) {
         LOGGER.info("Executed Step = IF,ELE_GETVAL_PTDCANBEMODIFIEDSTATUS,CONTAINS,Yes,TGTYPESCREENFULL");
        CALL.$("SwitchPTDcanBeModifiedUserAuthorityToNO","TGTYPESCREENREG");

        WAIT.$(1,"TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_EDIT","//span[text()='Edit']","TGTYPESCREENFULL");

        TAP.$("ELE_BUTTON_PTDCANBEMODIFIED_OPPOSITE","//table[@id='workbenchForm:workbenchTabs:securityFlag1']//span[@class='ui-radiobutton-icon ui-icon ui-icon-blank ui-c']","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SAVE","//span[contains(text(),'Save')]","TGTYPESCREENREG");

        ASSERT.$("ELE_GETVAL_PTDCANBEMODIFIEDSTATUS","//span[@id='workbenchForm:workbenchTabs:securityFlag1']","CONTAINS","No","TGTYPESCREENFULL");

        TAP.$("ELE_LABEL_MENUAVATAR","//span[@id='headerForm:avatarInitialsText']","TGTYPESCREENREG");

        TAP.$("ELE_MENU_LOGOUT","//span[contains(text(),'Log Out')]","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_GOTOLOGIN","//span[@class='ui-button-text ui-c']","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_USERNAME","//input[@id='guestForm:username']","x5080","FALSE","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_PASSWORD","//input[@id='guestForm:password']","Welcome123$","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_LOGIN","//button[@id='guestForm:loginButton']","TGTYPESCREENREG");

        }
          LOGGER.info("Executed Step = ENDIF,TGTYPESCREENREG");
        CALL.$("UserAuthorizationEqualsNOvalidations","TGTYPESCREENREG");

        TAP.$("ELE_LOGO_GIAS","//span[text()='GIAS']","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

        SWIPE.$("DOWN","TGTYPESCREENREG");

        SWIPE.$("DOWN","TGTYPESCREENREG");

        TAP.$("ELE_TXTBOX_SEARCHTASKS","//input[@id='workbenchForm:workbenchTabs:taskSearchContainer:taskSearchSearch']","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_SEARCHTASKS","//input[@id='workbenchForm:workbenchTabs:taskSearchContainer:taskSearchSearch']","New Business Policy Prop","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_TASKSEARCH","//a[@id='workbenchForm:workbenchTabs:taskSearchContainer:tb_buttonSearch']//span[@class='fa fa-search fa-lg']","TGTYPESCREENREG");

        TAP.$("ELE_LINK_NEWBUSINESSPOLICYPROPERTIES","//span[contains(text(),'New Business Policy Properties')]","TGTYPESCREENREG");

        WAIT.$(1,"TGTYPESCREENREG");

        ASSERT.$("ELE_GETVAL_MONTHSPTDCANADVANCEATSETTLE","//span[@id='workbenchForm:workbenchTabs:settlePtdAdvanceMonths']","VISIBLE","TGTYPESCREENFULL");

        LOGGER.info("Executed Step = START IF");
        if (check("ELE_GETVAL_MONTHSPTDCANADVANCEATSETTLE","//span[@id='workbenchForm:workbenchTabs:settlePtdAdvanceMonths']","<>",0,"TGTYPESCREENFULL")) {
         LOGGER.info("Executed Step = IF,ELE_GETVAL_MONTHSPTDCANADVANCEATSETTLE,<>,0,TGTYPESCREENFULL");
        TAP.$("ELE_BUTTON_EDIT","//span[text()='Edit']","TGTYPESCREENREG");

        WAIT.$(1,"TGTYPESCREENREG");

        TAP.$("ELE_TXTBOX_MONTHSPTDCANBEADVANCEDATSETTLE","//input[@id='workbenchForm:workbenchTabs:settlePtdAdvanceMonths_input']","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_MONTHSPTDCANBEADVANCEDATSETTLE","//input[@id='workbenchForm:workbenchTabs:settlePtdAdvanceMonths_input']","0","FALSE","TGTYPESCREENFULL");

        TAP.$("ELE_BUTTON_SAVE","//span[contains(text(),'Save')]","TGTYPESCREENREG");

        }
          LOGGER.info("Executed Step = ENDIF,TGTYPESCREENREG");
        ASSERT.$("ELE_GETVAL_MONTHSPTDCANADVANCEATSETTLE","//span[@id='workbenchForm:workbenchTabs:settlePtdAdvanceMonths']","CONTAINS",0,"TGTYPESCREENFULL");

        TAP.$("ELE_LOGO_GIAS","//span[text()='GIAS']","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

        SWIPE.$("DOWN","TGTYPESCREENREG");

        SWIPE.$("DOWN","TGTYPESCREENREG");

        TAP.$("ELE_TXTBOX_SEARCHTASKS","//input[@id='workbenchForm:workbenchTabs:taskSearchContainer:taskSearchSearch']","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_SEARCHTASKS","//input[@id='workbenchForm:workbenchTabs:taskSearchContainer:taskSearchSearch']","Create Inforce Policy","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_TASKSEARCH","//a[@id='workbenchForm:workbenchTabs:taskSearchContainer:tb_buttonSearch']//span[@class='fa fa-search fa-lg']","TGTYPESCREENREG");

        TAP.$("ELE_LINK_CREATEINFROCEPOLICYFROMPOLICYAPPLICATION","//body/div[@id='Outer40']/div[@id='ContentPanel']/div[@id='Content40']/form[@id='workbenchForm']/div[@id='PageTiles']/div[@id='workbenchForm:workbenchTabs']/div[1]/div[1]/div[1]/div[2]/div[2]/div[1]/div[1]/div[1]/div[3]/div[1]/div[1]/button[1]/span[2]","TGTYPESCREENREG");

        WAIT.$(3,"TGTYPESCREENREG");

        TAP.$("ELE_TXTBOX_SEARCHPOLICYNUMBER","//*[@name='workbenchForm:workbenchTabs:grid:policyNumber_Col:filter']","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_SEARCHPOLICYNUMBER","//*[@name='workbenchForm:workbenchTabs:grid:policyNumber_Col:filter']",var_PolicyNumber,"FALSE","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

        TAP.$("ELE_LINK_FIRSTPOLICYNUMBER","//span[@id='workbenchForm:workbenchTabs:grid:0:policyNumber']","TGTYPESCREENREG");

        ASSERT.$("ELE_GETVAL_NONEDITABLEZERO_MODESTOADVANCEPTD","//span[@id='workbenchForm:workbenchTabs:modesToAdvancePtd']","INVISIBLE","TGTYPESCREENFULL");

        TAP.$("ELE_LOGO_GIAS","//span[text()='GIAS']","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

        SWIPE.$("DOWN","TGTYPESCREENREG");

        SWIPE.$("DOWN","TGTYPESCREENREG");

        TAP.$("ELE_TXTBOX_SEARCHTASKS","//input[@id='workbenchForm:workbenchTabs:taskSearchContainer:taskSearchSearch']","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_SEARCHTASKS","//input[@id='workbenchForm:workbenchTabs:taskSearchContainer:taskSearchSearch']","New Business Policy Prop","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_TASKSEARCH","//a[@id='workbenchForm:workbenchTabs:taskSearchContainer:tb_buttonSearch']//span[@class='fa fa-search fa-lg']","TGTYPESCREENREG");

        TAP.$("ELE_LINK_NEWBUSINESSPOLICYPROPERTIES","//span[contains(text(),'New Business Policy Properties')]","TGTYPESCREENREG");

        WAIT.$(1,"TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_EDIT","//span[text()='Edit']","TGTYPESCREENREG");

        TAP.$("ELE_TXTBOX_MONTHSPTDCANBEADVANCEDATSETTLE","//input[@id='workbenchForm:workbenchTabs:settlePtdAdvanceMonths_input']","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_MONTHSPTDCANBEADVANCEDATSETTLE","//input[@id='workbenchForm:workbenchTabs:settlePtdAdvanceMonths_input']",var_MonthsPTDCanAdvanceAtSettle,"FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SAVE","//span[contains(text(),'Save')]","TGTYPESCREENFULL");

        TAP.$("ELE_LOGO_GIAS","//span[text()='GIAS']","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

        SWIPE.$("DOWN","TGTYPESCREENREG");

        SWIPE.$("DOWN","TGTYPESCREENREG");

        TAP.$("ELE_TXTBOX_SEARCHTASKS","//input[@id='workbenchForm:workbenchTabs:taskSearchContainer:taskSearchSearch']","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_SEARCHTASKS","//input[@id='workbenchForm:workbenchTabs:taskSearchContainer:taskSearchSearch']","Create Inforce Policy","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_TASKSEARCH","//a[@id='workbenchForm:workbenchTabs:taskSearchContainer:tb_buttonSearch']//span[@class='fa fa-search fa-lg']","TGTYPESCREENREG");

        TAP.$("ELE_LINK_CREATEINFROCEPOLICYFROMPOLICYAPPLICATION","//body/div[@id='Outer40']/div[@id='ContentPanel']/div[@id='Content40']/form[@id='workbenchForm']/div[@id='PageTiles']/div[@id='workbenchForm:workbenchTabs']/div[1]/div[1]/div[1]/div[2]/div[2]/div[1]/div[1]/div[1]/div[3]/div[1]/div[1]/button[1]/span[2]","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

        TAP.$("ELE_TXTBOX_POLICYNUMBER_CREATEINFORCEPOLICYPAGE","//input[@id='workbenchForm:workbenchTabs:grid:policyNumber_Col:filter']","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_POLICYNUMBER_CREATEINFORCEPOLICYPAGE","//input[@id='workbenchForm:workbenchTabs:grid:policyNumber_Col:filter']",var_PolicyNumber,"FALSE","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

        TAP.$("ELE_LINK_FIRSTPOLICYNUMBER","//span[@id='workbenchForm:workbenchTabs:grid:0:policyNumber']","TGTYPESCREENREG");

        WAIT.$(1,"TGTYPESCREENREG");

        ASSERT.$("ELE_GETVAL_NONEDITABLEZERO_MODESTOADVANCEPTD","//span[@id='workbenchForm:workbenchTabs:modesToAdvancePtd']","VISIBLE","TGTYPESCREENFULL");

        CALL.$("GrantUserPTDcanBeModifiedAuthority","TGTYPESCREENREG");

        TAP.$("ELE_LOGO_GIAS","//span[text()='GIAS']","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

        SWIPE.$("DOWN","TGTYPESCREENREG");

        SWIPE.$("DOWN","TGTYPESCREENREG");

        TAP.$("ELE_TXTBOX_SEARCHTASKS","//input[@id='workbenchForm:workbenchTabs:taskSearchContainer:taskSearchSearch']","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_SEARCHTASKS","//input[@id='workbenchForm:workbenchTabs:taskSearchContainer:taskSearchSearch']","Security User Profiles","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_TASKSEARCH","//a[@id='workbenchForm:workbenchTabs:taskSearchContainer:tb_buttonSearch']//span[@class='fa fa-search fa-lg']","TGTYPESCREENREG");

        TAP.$("ELE_LINK_SECURITYUSERPROFILES","//span[contains(text(),'Security User Profiles')]","TGTYPESCREENREG");

        TAP.$("ELE_TXTBOX_USER_SECURITYGROUPS","//input[@id='workbenchForm:workbenchTabs:grid:username_Col:filter']","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_USER_SECURITYGROUPS","//input[@id='workbenchForm:workbenchTabs:grid:username_Col:filter']",var_User,"FALSE","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

        TAP.$("ELE_LINK_USERNAMESECUIRTYGROUPSROW1","//span[@id='workbenchForm:workbenchTabs:grid:0:username']","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SHOWLINK","//span[@class='ui-button-icon-left ui-icon ui-c fa fa-ellipsis-v fa-2x']","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

        TAP.$("ELE_LINK_OVERRIDES_SHOWLINK","//span[contains(text(),'Overrides')]","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

        TAP.$("ELE_TXTBOX_SECURITYGROUPISSUESTATE","//input[@id='workbenchForm:workbenchTabs:grid:securityGroupName_Col:filter']","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_SECURITYGROUPISSUESTATE","//input[@id='workbenchForm:workbenchTabs:grid:securityGroupName_Col:filter']","NBS830","FALSE","TGTYPESCREENREG");

        WAIT.$(1,"TGTYPESCREENREG");

        ASSERT.$("ELE_LINK_USERSECURITYGROUPROW1","//span[@id='workbenchForm:workbenchTabs:grid:0:securityGroupName']","CONTAINS","NBS830","TGTYPESCREENREG");

        TAP.$("ELE_LINK_USERSECURITYGROUPROW1","//span[@id='workbenchForm:workbenchTabs:grid:0:securityGroupName']","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_EDIT","//span[text()='Edit']","TGTYPESCREENFULL");

        TAP.$("ELE_BUTTON_PTDCANBEMODIFIED_OPPOSITE","//table[@id='workbenchForm:workbenchTabs:securityFlag1']//span[@class='ui-radiobutton-icon ui-icon ui-icon-blank ui-c']","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SAVE","//span[contains(text(),'Save')]","TGTYPESCREENREG");

        ASSERT.$("ELE_GETVAL_PTDCANBEMODIFIEDSTATUS","//span[@id='workbenchForm:workbenchTabs:securityFlag1']","CONTAINS","Yes","TGTYPESCREENFULL");

        TAP.$("ELE_LABEL_MENUAVATAR","//span[@id='headerForm:avatarInitialsText']","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        TAP.$("ELE_MENU_LOGOUT","//span[contains(text(),'Log Out')]","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_GOTOLOGIN","//span[@class='ui-button-text ui-c']","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_USERNAME","//input[@id='guestForm:username']","x5080","FALSE","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_PASSWORD","//input[@id='guestForm:password']","Welcome123$","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_LOGIN","//button[@id='guestForm:loginButton']","TGTYPESCREENREG");

        CALL.$("UserAuthorizationAndMonthsPTDcanAdvanceSetToZEROValidations","TGTYPESCREENREG");

        TAP.$("ELE_LOGO_GIAS","//span[text()='GIAS']","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

        SWIPE.$("DOWN","TGTYPESCREENREG");

        SWIPE.$("DOWN","TGTYPESCREENREG");

        TAP.$("ELE_TXTBOX_SEARCHTASKS","//input[@id='workbenchForm:workbenchTabs:taskSearchContainer:taskSearchSearch']","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_SEARCHTASKS","//input[@id='workbenchForm:workbenchTabs:taskSearchContainer:taskSearchSearch']","New Business Policy Prop","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_TASKSEARCH","//a[@id='workbenchForm:workbenchTabs:taskSearchContainer:tb_buttonSearch']//span[@class='fa fa-search fa-lg']","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

        TAP.$("ELE_LINK_NEWBUSINESSPOLICYPROPERTIES","//span[contains(text(),'New Business Policy Properties')]","TGTYPESCREENREG");

        WAIT.$(1,"TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_EDIT","//span[text()='Edit']","TGTYPESCREENREG");

        WAIT.$(1,"TGTYPESCREENREG");

        TAP.$("ELE_TXTBOX_MONTHSPTDCANBEADVANCEDATSETTLE","//input[@id='workbenchForm:workbenchTabs:settlePtdAdvanceMonths_input']","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_MONTHSPTDCANBEADVANCEDATSETTLE","//input[@id='workbenchForm:workbenchTabs:settlePtdAdvanceMonths_input']","0","FALSE","TGTYPESCREENFULL");

        TAP.$("ELE_BUTTON_SAVE","//span[contains(text(),'Save')]","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

        ASSERT.$("ELE_GETVAL_MONTHSPTDCANADVANCEATSETTLE","//span[@id='workbenchForm:workbenchTabs:settlePtdAdvanceMonths']","CONTAINS",0,"TGTYPESCREENFULL");

        TAP.$("ELE_LOGO_GIAS","//span[text()='GIAS']","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

        SWIPE.$("DOWN","TGTYPESCREENREG");

        SWIPE.$("DOWN","TGTYPESCREENREG");

        TAP.$("ELE_TXTBOX_SEARCHTASKS","//input[@id='workbenchForm:workbenchTabs:taskSearchContainer:taskSearchSearch']","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_SEARCHTASKS","//input[@id='workbenchForm:workbenchTabs:taskSearchContainer:taskSearchSearch']","Create Inforce Policy","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_TASKSEARCH","//a[@id='workbenchForm:workbenchTabs:taskSearchContainer:tb_buttonSearch']//span[@class='fa fa-search fa-lg']","TGTYPESCREENREG");

        TAP.$("ELE_LINK_CREATEINFROCEPOLICYFROMPOLICYAPPLICATION","//body/div[@id='Outer40']/div[@id='ContentPanel']/div[@id='Content40']/form[@id='workbenchForm']/div[@id='PageTiles']/div[@id='workbenchForm:workbenchTabs']/div[1]/div[1]/div[1]/div[2]/div[2]/div[1]/div[1]/div[1]/div[3]/div[1]/div[1]/button[1]/span[2]","TGTYPESCREENREG");

        WAIT.$(3,"TGTYPESCREENREG");

        TAP.$("ELE_TXTBOX_SEARCHPOLICYNUMBER","//*[@name='workbenchForm:workbenchTabs:grid:policyNumber_Col:filter']","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_SEARCHPOLICYNUMBER","//*[@name='workbenchForm:workbenchTabs:grid:policyNumber_Col:filter']",var_PolicyNumber,"FALSE","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

        TAP.$("ELE_LINK_FIRSTPOLICYNUMBER","//span[@id='workbenchForm:workbenchTabs:grid:0:policyNumber']","TGTYPESCREENREG");

        ASSERT.$("ELE_GETVAL_NONEDITABLEZERO_MODESTOADVANCEPTD","//span[@id='workbenchForm:workbenchTabs:modesToAdvancePtd']","INVISIBLE","TGTYPESCREENFULL");

        ASSERT.$("ELE_GETVAL_NEWPAIDTODATE","//span[@id='workbenchForm:workbenchTabs:newPaidToDate']","INVISIBLE","TGTYPESCREENREG");

        TAP.$("ELE_LOGO_GIAS","//span[text()='GIAS']","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

        SWIPE.$("DOWN","TGTYPESCREENREG");

        SWIPE.$("DOWN","TGTYPESCREENREG");

        TAP.$("ELE_TXTBOX_SEARCHTASKS","//input[@id='workbenchForm:workbenchTabs:taskSearchContainer:taskSearchSearch']","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_SEARCHTASKS","//input[@id='workbenchForm:workbenchTabs:taskSearchContainer:taskSearchSearch']","New Business Policy Prop","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_TASKSEARCH","//a[@id='workbenchForm:workbenchTabs:taskSearchContainer:tb_buttonSearch']//span[@class='fa fa-search fa-lg']","TGTYPESCREENREG");

        TAP.$("ELE_LINK_NEWBUSINESSPOLICYPROPERTIES","//span[contains(text(),'New Business Policy Properties')]","TGTYPESCREENREG");

        WAIT.$(1,"TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_EDIT","//span[text()='Edit']","TGTYPESCREENREG");

        TAP.$("ELE_TXTBOX_MONTHSPTDCANBEADVANCEDATSETTLE","//input[@id='workbenchForm:workbenchTabs:settlePtdAdvanceMonths_input']","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_MONTHSPTDCANBEADVANCEDATSETTLE","//input[@id='workbenchForm:workbenchTabs:settlePtdAdvanceMonths_input']",var_MonthsPTDCanAdvanceAtSettle,"FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SAVE","//span[contains(text(),'Save')]","TGTYPESCREENREG");

        TAP.$("ELE_LOGO_GIAS","//span[text()='GIAS']","TGTYPESCREENREG");

		// ^ also updates Months PTD can Advance to Greater Than Zero;
        CALL.$("ErrorMessageValidations","TGTYPESCREENREG");

        SWIPE.$("DOWN","TGTYPESCREENREG");

        SWIPE.$("DOWN","TGTYPESCREENREG");

        TAP.$("ELE_TXTBOX_SEARCHTASKS","//input[@id='workbenchForm:workbenchTabs:taskSearchContainer:taskSearchSearch']","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_SEARCHTASKS","//input[@id='workbenchForm:workbenchTabs:taskSearchContainer:taskSearchSearch']","Create Inforce Policy","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_TASKSEARCH","//a[@id='workbenchForm:workbenchTabs:taskSearchContainer:tb_buttonSearch']//span[@class='fa fa-search fa-lg']","TGTYPESCREENREG");

        TAP.$("ELE_LINK_CREATEINFROCEPOLICYFROMPOLICYAPPLICATION","//body/div[@id='Outer40']/div[@id='ContentPanel']/div[@id='Content40']/form[@id='workbenchForm']/div[@id='PageTiles']/div[@id='workbenchForm:workbenchTabs']/div[1]/div[1]/div[1]/div[2]/div[2]/div[1]/div[1]/div[1]/div[3]/div[1]/div[1]/button[1]/span[2]","TGTYPESCREENREG");

        WAIT.$(3,"TGTYPESCREENREG");

        TAP.$("ELE_TXTBOX_SEARCHPOLICYNUMBER","//*[@name='workbenchForm:workbenchTabs:grid:policyNumber_Col:filter']","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_SEARCHPOLICYNUMBER","//*[@name='workbenchForm:workbenchTabs:grid:policyNumber_Col:filter']",var_PolicyNumber,"FALSE","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

        TAP.$("ELE_LINK_FIRSTPOLICYNUMBER","//span[@id='workbenchForm:workbenchTabs:grid:0:policyNumber']","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

        TAP.$("ELE_TXTBOX_DATETOAPPLYPOLICYPAYMENT","//input[@id='workbenchForm:workbenchTabs:applyDate_input']","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_DATETOAPPLYPOLICYPAYMENT","//input[@id='workbenchForm:workbenchTabs:applyDate_input']","04192149","FALSE","TGTYPESCREENFULL");

		try { 
		 WebElement eleTerminationDate = driver.findElement(By.xpath("//input[@id='workbenchForm:workbenchTabs:modesToAdvancePtd_input']"));
		       
		        eleTerminationDate.sendKeys("111");
		 
		 ;} 
		 catch(Exception e) { PrintVal("Renderer Script Exception : "+e.toString()); } 
		 PrintVal("Executed Step = CALLRENDERERSCRIPT,MODESTOADVANCEPTDEQUALS111"); 
        WAIT.$(2,"TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_VALIDATE","//span[contains(text(),'Validate')]","TGTYPESCREENREG");

        WAIT.$(1,"TGTYPESCREENREG");

        ASSERT.$("ELE_GETMSG_ERROR_PAYMENTATSETTLEMUSTBEZEROWHENPTDDEFINED","//div[@id='workbenchForm:workbenchTabs:messages']//span[@class='ui-messages-error-summary'][contains(text(),'Payment At Settle must be zero when the Paid To Da')]","VISIBLE","TGTYPESCREENFULL");

        ASSERT.$("ELE_GETMSG_ERROR_APPLYDATECANNOTBEGREATERTHANTODAYSDATE","//div[@class='ui-tabs-panels']//li[2]//span[1]","VISIBLE","TGTYPESCREENFULL");

        ASSERT.$("ELE_GETMSG_ERROR_PTDCANNOTBEGREATERTHANMODELIMIT","//div[@class='ui-tabs-panels']//li[3]//span[1]","VISIBLE","TGTYPESCREENFULL");

        TAP.$("ELE_TAB_CLOSE3","//li[@class='ui-state-default ui-tabs-selected ui-state-active ui-corner-top']//span[@class='ui-icon ui-icon-close']","TGTYPESCREENREG");

        WAIT.$(4,"TGTYPESCREENREG");

        TAP.$("ELE_LINK_FIRSTPOLICYNUMBER","//span[@id='workbenchForm:workbenchTabs:grid:0:policyNumber']",1,0,"TGTYPESCREENFULL");

        WAIT.$(1,"TGTYPESCREENREG");

        CALL.$("AdvancePTDbasedOnPaymentFrequency","TGTYPESCREENREG");

        LOGGER.info("Executed Step = START IF");
        if (check("ELE_GETVAL_PAYMENTFREQUENCY_INQUIRYPAGE","//span[@id='workbenchForm:workbenchTabs:paymentMode']","CONTAINS","Annual","TGTYPESCREENFULL")) {
         LOGGER.info("Executed Step = IF,ELE_GETVAL_PAYMENTFREQUENCY_INQUIRYPAGE,CONTAINS,Annual,TGTYPESCREENFULL");
        CALL.$("PolicyWithANNUALPaymentFrequency","TGTYPESCREENREG");

		try { 
		 WebElement eleTerminationDate = driver.findElement(By.xpath("//input[@id='workbenchForm:workbenchTabs:modesToAdvancePtd_input']"));
		       
		        eleTerminationDate.sendKeys("2");
		 
		 ;} 
		 catch(Exception e) { PrintVal("Renderer Script Exception : "+e.toString()); } 
		 PrintVal("Executed Step = CALLRENDERERSCRIPT,MODESTOADVANCEPTDEQUALS2"); 
        WAIT.$(1,"TGTYPESCREENFULL");

        WAIT.$(1,"TGTYPESCREENREG");

        TAP.$("ELE_TXTBOX_PAYMENTSETTLE","//input[@id='workbenchForm:workbenchTabs:paymentAtSettle_input']","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_PAYMENTSETTLE","//input[@id='workbenchForm:workbenchTabs:paymentAtSettle_input']","0","FALSE","TGTYPESCREENREG");

        WAIT.$(1,"TGTYPESCREENFULL");

		String var_NewPaidToDate;
                 LOGGER.info("Executed Step = VAR,String,var_NewPaidToDate,TGTYPESCREENREG");
		var_NewPaidToDate = ActionWrapper.getElementValue("ELE_GETVAL_NEWPAIDTODATE", "//span[@id='workbenchForm:workbenchTabs:newPaidToDate']");
                 LOGGER.info("Executed Step = STORE,var_NewPaidToDate,ELE_GETVAL_NEWPAIDTODATE,TGTYPESCREENREG");
        ASSERT.$("ELE_GETVAL_NEWPAIDTODATE","//span[@id='workbenchForm:workbenchTabs:newPaidToDate']","VISIBLE","TGTYPESCREENFULL");

        TAP.$("ELE_BUTTON_SUBMIT","//span[contains(text(),'Submit')]","TGTYPESCREENREG");

        WAIT.$(10,"TGTYPESCREENREG");

        WAIT.$("ELE_GETMSG_SUCCESS","//div[@id='workbenchForm:workbenchTabs:messages']//span[@class='ui-messages-info-summary']","VISIBLE","TGTYPESCREENREG");

        ASSERT.$("ELE_GETMSG_SUCCESS","//div[@id='workbenchForm:workbenchTabs:messages']//span[@class='ui-messages-info-summary']","CONTAINS","Request completed successfully.","TGTYPESCREENFULL");

        TAP.$("ELE_LOGO_GIAS","//span[text()='GIAS']","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

        SWIPE.$("DOWN","TGTYPESCREENREG");

        SWIPE.$("DOWN","TGTYPESCREENREG");

        TAP.$("ELE_TXTBOX_SEARCHTASKS","//input[@id='workbenchForm:workbenchTabs:taskSearchContainer:taskSearchSearch']","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_SEARCHTASKS","//input[@id='workbenchForm:workbenchTabs:taskSearchContainer:taskSearchSearch']","Policy Inquiry","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_TASKSEARCH","//a[@id='workbenchForm:workbenchTabs:taskSearchContainer:tb_buttonSearch']//span[@class='fa fa-search fa-lg']","TGTYPESCREENREG");

        TAP.$("ELE_LINK_POLICYINQUIRY","//span[contains(text(),'Policy Inquiry')]","TGTYPESCREENREG");

        WAIT.$(1,"TGTYPESCREENREG");

        TAP.$("ELE_TXTBOX_POLICYNUMBER","//input[@id='workbenchForm:workbenchTabs:policyNumber']","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_POLICYNUMBER","//input[@id='workbenchForm:workbenchTabs:policyNumber']",var_PolicyNumber,"FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_CONTINUE","//span[contains(text(),'Continue')]","TGTYPESCREENREG");

        WAIT.$(15,"TGTYPESCREENREG");

        ASSERT.$("ELE_GETVAL_PAIDTODATE","//span[@id='workbenchForm:workbenchTabs:paidToDate']","=",var_NewPaidToDate,"TGTYPESCREENFULL");

        CALL.$("ValidateNewPTDandTransactionHistoryForANNUAL","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SHOWLINK","//span[@class='ui-button-icon-left ui-icon ui-c fa fa-ellipsis-v fa-2x']","TGTYPESCREENREG");

        WAIT.$(1,"TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_CLIENTTRANSACTIONHISTORY","//span[contains(text(),'Transaction History')]","TGTYPESCREENREG");

        WAIT.$(3,"TGTYPESCREENREG");

        ASSERT.$("ELE_LINK_FIRST_TRANSACTIONHISTORYLINK","//span[@id='workbenchForm:workbenchTabs:grid:0:transactionDate']","VISIBLE","TGTYPESCREENREG");

        ASSERT.$("ELE_LINK_SECOND_TRANSACTIONHISTORYLINK","//span[@id='workbenchForm:workbenchTabs:grid:1:transactionDate']","VISIBLE","TGTYPESCREENREG");

        }
          LOGGER.info("Executed Step = ENDIF,TGTYPESCREENREG");
        LOGGER.info("Executed Step = START IF");
        if (check("ELE_GETVAL_PAYMENTFREQUENCY_INQUIRYPAGE","//span[@id='workbenchForm:workbenchTabs:paymentMode']","CONTAINS","Semiannual","TGTYPESCREENFULL")) {
         LOGGER.info("Executed Step = IF,ELE_GETVAL_PAYMENTFREQUENCY_INQUIRYPAGE,CONTAINS,Semiannual,TGTYPESCREENFULL");
        CALL.$("PolicyWithSEMIpaymentFrequency","TGTYPESCREENREG");

		try { 
		 WebElement eleTerminationDate = driver.findElement(By.xpath("//input[@id='workbenchForm:workbenchTabs:modesToAdvancePtd_input']"));
		       
		        eleTerminationDate.sendKeys("1");
		 
		 ;} 
		 catch(Exception e) { PrintVal("Renderer Script Exception : "+e.toString()); } 
		 PrintVal("Executed Step = CALLRENDERERSCRIPT,MODESTOADVANCEPTDEQUALS1"); 
        WAIT.$(1,"TGTYPESCREENFULL");

        WAIT.$(1,"TGTYPESCREENREG");

        TAP.$("ELE_TXTBOX_PAYMENTSETTLE","//input[@id='workbenchForm:workbenchTabs:paymentAtSettle_input']","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_PAYMENTSETTLE","//input[@id='workbenchForm:workbenchTabs:paymentAtSettle_input']","0","FALSE","TGTYPESCREENREG");

        WAIT.$(1,"TGTYPESCREENFULL");

		String var_NewPaidToDate;
                 LOGGER.info("Executed Step = VAR,String,var_NewPaidToDate,TGTYPESCREENREG");
		var_NewPaidToDate = ActionWrapper.getElementValue("ELE_GETVAL_NEWPAIDTODATE", "//span[@id='workbenchForm:workbenchTabs:newPaidToDate']");
                 LOGGER.info("Executed Step = STORE,var_NewPaidToDate,ELE_GETVAL_NEWPAIDTODATE,TGTYPESCREENREG");
        ASSERT.$("ELE_GETVAL_NEWPAIDTODATE","//span[@id='workbenchForm:workbenchTabs:newPaidToDate']","VISIBLE","TGTYPESCREENFULL");

        TAP.$("ELE_BUTTON_SUBMIT","//span[contains(text(),'Submit')]","TGTYPESCREENREG");

        WAIT.$(10,"TGTYPESCREENREG");

        WAIT.$("ELE_GETMSG_SUCCESS","//div[@id='workbenchForm:workbenchTabs:messages']//span[@class='ui-messages-info-summary']","VISIBLE","TGTYPESCREENREG");

        ASSERT.$("ELE_GETMSG_SUCCESS","//div[@id='workbenchForm:workbenchTabs:messages']//span[@class='ui-messages-info-summary']","CONTAINS","Request completed successfully.","TGTYPESCREENFULL");

        TAP.$("ELE_LOGO_GIAS","//span[text()='GIAS']","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

        TAP.$("ELE_TXTBOX_SEARCHTASKS","//input[@id='workbenchForm:workbenchTabs:taskSearchContainer:taskSearchSearch']","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_SEARCHTASKS","//input[@id='workbenchForm:workbenchTabs:taskSearchContainer:taskSearchSearch']","Policy Inquiry","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_TASKSEARCH","//a[@id='workbenchForm:workbenchTabs:taskSearchContainer:tb_buttonSearch']//span[@class='fa fa-search fa-lg']","TGTYPESCREENREG");

        TAP.$("ELE_LINK_POLICYINQUIRY","//span[contains(text(),'Policy Inquiry')]","TGTYPESCREENREG");

        WAIT.$(1,"TGTYPESCREENREG");

        TAP.$("ELE_TXTBOX_POLICYNUMBER","//input[@id='workbenchForm:workbenchTabs:policyNumber']","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_POLICYNUMBER","//input[@id='workbenchForm:workbenchTabs:policyNumber']",var_PolicyNumber,"FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_CONTINUE","//span[contains(text(),'Continue')]","TGTYPESCREENREG");

        WAIT.$(15,"TGTYPESCREENREG");

        ASSERT.$("ELE_GETVAL_PAIDTODATE","//span[@id='workbenchForm:workbenchTabs:paidToDate']","=",var_NewPaidToDate,"TGTYPESCREENFULL");

        CALL.$("ValidateNewPTDandTransactionHistoryForSEMI","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SHOWLINK","//span[@class='ui-button-icon-left ui-icon ui-c fa fa-ellipsis-v fa-2x']","TGTYPESCREENREG");

        WAIT.$(1,"TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_CLIENTTRANSACTIONHISTORY","//span[contains(text(),'Transaction History')]","TGTYPESCREENREG");

        WAIT.$(3,"TGTYPESCREENREG");

        ASSERT.$("ELE_LINK_FIRST_TRANSACTIONHISTORYLINK","//span[@id='workbenchForm:workbenchTabs:grid:0:transactionDate']","VISIBLE","TGTYPESCREENREG");

        }
          LOGGER.info("Executed Step = ENDIF,TGTYPESCREENREG");
        LOGGER.info("Executed Step = START IF");
        if (check("ELE_GETVAL_PAYMENTFREQUENCY_INQUIRYPAGE","//span[@id='workbenchForm:workbenchTabs:paymentMode']","CONTAINS","Quarterly","TGTYPESCREENFULL")) {
         LOGGER.info("Executed Step = IF,ELE_GETVAL_PAYMENTFREQUENCY_INQUIRYPAGE,CONTAINS,Quarterly,TGTYPESCREENFULL");
        CALL.$("PolicyWithQUARTERLYpaymentFrequency","TGTYPESCREENREG");

		try { 
		 WebElement eleTerminationDate = driver.findElement(By.xpath("//input[@id='workbenchForm:workbenchTabs:modesToAdvancePtd_input']"));
		       
		        eleTerminationDate.sendKeys("1");
		 
		 ;} 
		 catch(Exception e) { PrintVal("Renderer Script Exception : "+e.toString()); } 
		 PrintVal("Executed Step = CALLRENDERERSCRIPT,MODESTOADVANCEPTDEQUALS1"); 
        WAIT.$(1,"TGTYPESCREENFULL");

        WAIT.$(1,"TGTYPESCREENREG");

        TAP.$("ELE_TXTBOX_PAYMENTSETTLE","//input[@id='workbenchForm:workbenchTabs:paymentAtSettle_input']","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_PAYMENTSETTLE","//input[@id='workbenchForm:workbenchTabs:paymentAtSettle_input']","0","FALSE","TGTYPESCREENREG");

        WAIT.$(1,"TGTYPESCREENFULL");

		String var_NewPaidToDate;
                 LOGGER.info("Executed Step = VAR,String,var_NewPaidToDate,TGTYPESCREENREG");
		var_NewPaidToDate = ActionWrapper.getElementValue("ELE_GETVAL_NEWPAIDTODATE", "//span[@id='workbenchForm:workbenchTabs:newPaidToDate']");
                 LOGGER.info("Executed Step = STORE,var_NewPaidToDate,ELE_GETVAL_NEWPAIDTODATE,TGTYPESCREENREG");
        ASSERT.$("ELE_GETVAL_NEWPAIDTODATE","//span[@id='workbenchForm:workbenchTabs:newPaidToDate']","VISIBLE","TGTYPESCREENFULL");

        TAP.$("ELE_BUTTON_SUBMIT","//span[contains(text(),'Submit')]","TGTYPESCREENREG");

        WAIT.$(10,"TGTYPESCREENREG");

        WAIT.$("ELE_GETMSG_SUCCESS","//div[@id='workbenchForm:workbenchTabs:messages']//span[@class='ui-messages-info-summary']","VISIBLE","TGTYPESCREENREG");

        ASSERT.$("ELE_GETMSG_SUCCESS","//div[@id='workbenchForm:workbenchTabs:messages']//span[@class='ui-messages-info-summary']","CONTAINS","Request completed successfully.","TGTYPESCREENFULL");

        TAP.$("ELE_LOGO_GIAS","//span[text()='GIAS']","TGTYPESCREENREG");

        WAIT.$(1,"TGTYPESCREENREG");

        TAP.$("ELE_TXTBOX_SEARCHTASKS","//input[@id='workbenchForm:workbenchTabs:taskSearchContainer:taskSearchSearch']","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_SEARCHTASKS","//input[@id='workbenchForm:workbenchTabs:taskSearchContainer:taskSearchSearch']","Policy Inquiry","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_TASKSEARCH","//a[@id='workbenchForm:workbenchTabs:taskSearchContainer:tb_buttonSearch']//span[@class='fa fa-search fa-lg']","TGTYPESCREENREG");

        TAP.$("ELE_LINK_POLICYINQUIRY","//span[contains(text(),'Policy Inquiry')]","TGTYPESCREENREG");

        WAIT.$(1,"TGTYPESCREENREG");

        TAP.$("ELE_TXTBOX_POLICYNUMBER","//input[@id='workbenchForm:workbenchTabs:policyNumber']","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_POLICYNUMBER","//input[@id='workbenchForm:workbenchTabs:policyNumber']",var_PolicyNumber,"FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_CONTINUE","//span[contains(text(),'Continue')]","TGTYPESCREENREG");

        WAIT.$(15,"TGTYPESCREENREG");

        ASSERT.$("ELE_GETVAL_PAIDTODATE","//span[@id='workbenchForm:workbenchTabs:paidToDate']","=",var_NewPaidToDate,"TGTYPESCREENFULL");

        CALL.$("ValidateNewPTDandTransactionHistoryForQUARTERLY","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SHOWLINK","//span[@class='ui-button-icon-left ui-icon ui-c fa fa-ellipsis-v fa-2x']","TGTYPESCREENREG");

        WAIT.$(1,"TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_CLIENTTRANSACTIONHISTORY","//span[contains(text(),'Transaction History')]","TGTYPESCREENREG");

        WAIT.$(3,"TGTYPESCREENREG");

        ASSERT.$("ELE_LINK_FIRST_TRANSACTIONHISTORYLINK","//span[@id='workbenchForm:workbenchTabs:grid:0:transactionDate']","VISIBLE","TGTYPESCREENREG");

        }
          LOGGER.info("Executed Step = ENDIF,TGTYPESCREENREG");
        LOGGER.info("Executed Step = START IF");
        if (check("ELE_GETVAL_PAYMENTFREQUENCY_INQUIRYPAGE","//span[@id='workbenchForm:workbenchTabs:paymentMode']","CONTAINS","Monthly","TGTYPESCREENFULL")) {
         LOGGER.info("Executed Step = IF,ELE_GETVAL_PAYMENTFREQUENCY_INQUIRYPAGE,CONTAINS,Monthly,TGTYPESCREENFULL");
        CALL.$("PolicyWithMONTHLYpaymentFrequency","TGTYPESCREENREG");

		try { 
		 WebElement eleTerminationDate = driver.findElement(By.xpath("//input[@id='workbenchForm:workbenchTabs:modesToAdvancePtd_input']"));
		       
		        eleTerminationDate.sendKeys("12");
		 
		 ;} 
		 catch(Exception e) { PrintVal("Renderer Script Exception : "+e.toString()); } 
		 PrintVal("Executed Step = CALLRENDERERSCRIPT,MODESTOADVANCEPTDEQUALS12"); 
        WAIT.$(1,"TGTYPESCREENFULL");

        WAIT.$(1,"TGTYPESCREENREG");

        TAP.$("ELE_TXTBOX_PAYMENTSETTLE","//input[@id='workbenchForm:workbenchTabs:paymentAtSettle_input']","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_PAYMENTSETTLE","//input[@id='workbenchForm:workbenchTabs:paymentAtSettle_input']","0","FALSE","TGTYPESCREENREG");

        WAIT.$(1,"TGTYPESCREENFULL");

		String var_NewPaidToDate;
                 LOGGER.info("Executed Step = VAR,String,var_NewPaidToDate,TGTYPESCREENREG");
		var_NewPaidToDate = ActionWrapper.getElementValue("ELE_GETVAL_NEWPAIDTODATE", "//span[@id='workbenchForm:workbenchTabs:newPaidToDate']");
                 LOGGER.info("Executed Step = STORE,var_NewPaidToDate,ELE_GETVAL_NEWPAIDTODATE,TGTYPESCREENREG");
        ASSERT.$("ELE_GETVAL_NEWPAIDTODATE","//span[@id='workbenchForm:workbenchTabs:newPaidToDate']","VISIBLE","TGTYPESCREENFULL");

        TAP.$("ELE_BUTTON_SUBMIT","//span[contains(text(),'Submit')]","TGTYPESCREENREG");

        WAIT.$("ELE_GETMSG_SUCCESS","//div[@id='workbenchForm:workbenchTabs:messages']//span[@class='ui-messages-info-summary']","VISIBLE","TGTYPESCREENREG");

        ASSERT.$("ELE_GETMSG_SUCCESS","//div[@id='workbenchForm:workbenchTabs:messages']//span[@class='ui-messages-info-summary']","CONTAINS","Request completed successfully.","TGTYPESCREENFULL");

        TAP.$("ELE_LOGO_GIAS","//span[text()='GIAS']","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

        TAP.$("ELE_TXTBOX_SEARCHTASKS","//input[@id='workbenchForm:workbenchTabs:taskSearchContainer:taskSearchSearch']","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_SEARCHTASKS","//input[@id='workbenchForm:workbenchTabs:taskSearchContainer:taskSearchSearch']","Policy Inquiry","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_TASKSEARCH","//a[@id='workbenchForm:workbenchTabs:taskSearchContainer:tb_buttonSearch']//span[@class='fa fa-search fa-lg']","TGTYPESCREENREG");

        TAP.$("ELE_LINK_POLICYINQUIRY","//span[contains(text(),'Policy Inquiry')]","TGTYPESCREENREG");

        WAIT.$(1,"TGTYPESCREENREG");

        TAP.$("ELE_TXTBOX_POLICYNUMBER","//input[@id='workbenchForm:workbenchTabs:policyNumber']","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_POLICYNUMBER","//input[@id='workbenchForm:workbenchTabs:policyNumber']",var_PolicyNumber,"FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_CONTINUE","//span[contains(text(),'Continue')]","TGTYPESCREENREG");

        WAIT.$(11,"TGTYPESCREENREG");

        ASSERT.$("ELE_GETVAL_PAIDTODATE","//span[@id='workbenchForm:workbenchTabs:paidToDate']","=",var_NewPaidToDate,"TGTYPESCREENFULL");

        CALL.$("ValidateNewPTDandTransactionHistoryForMONTHLY","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SHOWLINK","//span[@class='ui-button-icon-left ui-icon ui-c fa fa-ellipsis-v fa-2x']","TGTYPESCREENREG");

        WAIT.$(1,"TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_CLIENTTRANSACTIONHISTORY","//span[contains(text(),'Transaction History')]","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        ASSERT.$("ELE_LINK_FIRST_TRANSACTIONHISTORYLINK","//span[@id='workbenchForm:workbenchTabs:grid:0:transactionDate']","VISIBLE","TGTYPESCREENREG");

        ASSERT.$("ELE_LINK_SECOND_TRANSACTIONHISTORYLINK","//span[@id='workbenchForm:workbenchTabs:grid:1:transactionDate']","VISIBLE","TGTYPESCREENREG");

        ASSERT.$("ELE_LINK_THIRD_TRANSACTIONHISTORYLINK","//span[@id='workbenchForm:workbenchTabs:grid:2:transactionDate']","VISIBLE","TGTYPESCREENREG");

        ASSERT.$("ELE_LINK_FOURTH_TRANSACTIONHISTORYLINK","//span[@id='workbenchForm:workbenchTabs:grid:3:transactionDate']","VISIBLE","TGTYPESCREENREG");

        ASSERT.$("ELE_LINK_FIFTH_TRANSACTIONHISTORYLINK","//span[@id='workbenchForm:workbenchTabs:grid:4:transactionDate']","VISIBLE","TGTYPESCREENREG");

        ASSERT.$("ELE_LINK_SIXTH_TRANSACTIONHISTORYLINK","//span[@id='workbenchForm:workbenchTabs:grid:5:transactionDate']","VISIBLE","TGTYPESCREENREG");

        ASSERT.$("ELE_LINK_SEVENTH_TRANSACTIONHISTORYLINK","//span[@id='workbenchForm:workbenchTabs:grid:6:transactionDate']","VISIBLE","TGTYPESCREENREG");

        ASSERT.$("ELE_LINK_EIGHTH_TRANSACTIONHISTORYLINK","//span[@id='workbenchForm:workbenchTabs:grid:7:transactionDate']","VISIBLE","TGTYPESCREENREG");

        ASSERT.$("ELE_LINK_NINTH_TRANSACTIONHISTORYLINK","//span[@id='workbenchForm:workbenchTabs:grid:8:transactionDate']","VISIBLE","TGTYPESCREENREG");

        ASSERT.$("ELE_LINK_TENTH_TRANSACTIONHISTORYLINK","//span[@id='workbenchForm:workbenchTabs:grid:9:transactionDate']","VISIBLE","TGTYPESCREENREG");

        ASSERT.$("ELE_LINK_ELEVENTH_TRANSACTIONHISTORYLINK","//span[@id='workbenchForm:workbenchTabs:grid:10:transactionDate']","VISIBLE","TGTYPESCREENREG");

        ASSERT.$("ELE_LINK_TWELFTH_TRANSACTIONHISTORYLINK","//span[@id='workbenchForm:workbenchTabs:grid:11:transactionDate']","VISIBLE","TGTYPESCREENREG");

        }
          LOGGER.info("Executed Step = ENDIF,TGTYPESCREENREG");
		// ^Test Function vessel that holds the 4 payment frequency advancement options and their transaction hist lookups;
		var_Count = var_Count + ( 1 );
                 LOGGER.info("Executed Step = INCREASEVAR,var_Count,1,TGTYPESCREENREG");
    	}
          LOGGER.info("Executed Step = WEND,TGTYPESCREENREG");
    	resetAttemps();

    }


    @Test
    public void issuestatetable() {

        // No need to declare / print method name here, handled by TestNg listener
        // declare("issuestatetable");

		int var_Count = 0;
                 LOGGER.info("Executed Step = VAR,Integer,var_Count,0,TGTYPESCREENREG");
		com.jayway.jsonpath.ReadContext var_CSVData = getJsonData("http://10.1.104.229/json/20200814/Own0VD.json");
                 LOGGER.info("Executed Step = VAR,Json,var_CSVData,http://10.1.104.229/json/20200814/Own0VD.json,TGTYPESCREENREG");
        LOGGER.info("Executed Step = REPEAT IF");
    	while (check(var_Count,"<",1,"TGTYPESCREENREG")) {
          LOGGER.info("Executed Step = WHILE,var_Count,<,1,TGTYPESCREENREG");
		String var_PolicyNumber = "null";
                 LOGGER.info("Executed Step = VAR,String,var_PolicyNumber,null,TGTYPESCREENREG");
		var_PolicyNumber = getJsonData(var_CSVData , "$.records["+var_Count+"].PolicyNumber");
                 LOGGER.info("Executed Step = STORE,var_PolicyNumber,var_CSVData,$.records[{var_Count}].PolicyNumber,TGTYPESCREENREG");
        CALL.$("FTJQA2Login","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

		try { 
		 WebElement html = driver.findElement(By.tagName("html"));
		    	
		    	System.out.println("html size" + html.getSize());
		    	System.out.println("html" + html.getTagName());

		    	for( int i =0 ;i<4;i++) {
		        html.sendKeys(Keys.chord(Keys.CONTROL, Keys.SUBTRACT));
		    	}
		 
		 ;} 
		 catch(Exception e) { PrintVal("Renderer Script Exception : "+e.toString()); } 
		 PrintVal("Executed Step = CALLRENDERERSCRIPT,FTJ41ZOOMMAGNIFICATIONCHANGE"); 
        TYPE.$("ELE_TXTBOX_USERNAME","//input[@id='guestForm:username']","x5080","FALSE","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_PASSWORD","//input[@id='guestForm:password']","Welcome123$","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_LOGIN","//button[@id='guestForm:loginButton']","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        CALL.$("VerifyPolicyStatusIsPending","TGTYPESCREENREG");

        TAP.$("ELE_TXTBOX_SEARCHTASKS","//input[@id='workbenchForm:workbenchTabs:taskSearchContainer:taskSearchSearch']","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_SEARCHTASKS","//input[@id='workbenchForm:workbenchTabs:taskSearchContainer:taskSearchSearch']","POLICY INQUIRY","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_TASKSEARCH","//a[@id='workbenchForm:workbenchTabs:taskSearchContainer:tb_buttonSearch']//span[@class='fa fa-search fa-lg']","TGTYPESCREENREG");

        WAIT.$(1,"TGTYPESCREENREG");

        TAP.$("ELE_LINK_POLICYINQUIRY","//span[contains(text(),'Policy Inquiry')]","TGTYPESCREENREG");

        WAIT.$(1,"TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_POLICYNUMBER","//input[@id='workbenchForm:workbenchTabs:policyNumber']",var_PolicyNumber,"FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_CONTINUE","//span[contains(text(),'Continue')]","TGTYPESCREENREG");

        WAIT.$(1,"TGTYPESCREENREG");

        ASSERT.$("ELE_GETVAL_POLICYSTATUSPENDINGISSUE","//span[@id='workbenchForm:workbenchTabs:contractStatusNewBusText']","CONTAINS","Pending","TGTYPESCREENREG");

		String var_PolicyEffectiveDate;
                 LOGGER.info("Executed Step = VAR,String,var_PolicyEffectiveDate,TGTYPESCREENREG");
		var_PolicyEffectiveDate = ActionWrapper.getElementValue("ELE_GETTEXT_EFFECTIVEDATE", "//span[@id='workbenchForm:workbenchTabs:effectiveDate']");
                 LOGGER.info("Executed Step = STORE,var_PolicyEffectiveDate,ELE_GETTEXT_EFFECTIVEDATE,TGTYPESCREENREG");
		String var_PolicyState;
                 LOGGER.info("Executed Step = VAR,String,var_PolicyState,TGTYPESCREENREG");
		var_PolicyState = ActionWrapper.getElementValue("ELE_GETVAL_ISSUESTATE", "//span[@id='workbenchForm:workbenchTabs:countryAndStateCode']");
                 LOGGER.info("Executed Step = STORE,var_PolicyState,ELE_GETVAL_ISSUESTATE,TGTYPESCREENREG");
        TAP.$("ELE_BUTTON_GIASHOME","//span[text()='GIAS']","TGTYPESCREENREG");

        TAP.$("ELE_TXTBOX_SEARCHTASKS","//input[@id='workbenchForm:workbenchTabs:taskSearchContainer:taskSearchSearch']","TGTYPESCREENREG");

        WAIT.$(1,"TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_SEARCHTASKS","//input[@id='workbenchForm:workbenchTabs:taskSearchContainer:taskSearchSearch']","ISSUE STATE APPROVAL","FALSE","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_TASKSEARCH","//a[@id='workbenchForm:workbenchTabs:taskSearchContainer:tb_buttonSearch']//span[@class='fa fa-search fa-lg']","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

        TAP.$("ELE_LINK_ISSUESTATEAPPROVAL","//span[contains(text(),'Issue State Approval')]","TGTYPESCREENREG");

        WAIT.$(1,"TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_TABLE","//input[@id='workbenchForm:workbenchTabs:grid:issueStateValiTabl_Col:filter']","ATSTDT","FALSE","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

		String var_Efftxt;
                 LOGGER.info("Executed Step = VAR,String,var_Efftxt,TGTYPESCREENREG");
		var_Efftxt = "null";
                 LOGGER.info("Executed Step = STORE,var_Efftxt,null,TGTYPESCREENREG");
		String var_Apptext;
                 LOGGER.info("Executed Step = VAR,String,var_Apptext,TGTYPESCREENREG");
		var_Apptext = "null";
                 LOGGER.info("Executed Step = STORE,var_Apptext,null,TGTYPESCREENREG");
		String var_Efftxtforall;
                 LOGGER.info("Executed Step = VAR,String,var_Efftxtforall,TGTYPESCREENREG");
		var_Efftxtforall = "null";
                 LOGGER.info("Executed Step = STORE,var_Efftxtforall,null,TGTYPESCREENREG");
		String var_Apptextforall;
                 LOGGER.info("Executed Step = VAR,String,var_Apptextforall,TGTYPESCREENREG");
		var_Apptextforall = "null";
                 LOGGER.info("Executed Step = STORE,var_Apptextforall,null,TGTYPESCREENREG");
		String var_planshouldbevisible;
                 LOGGER.info("Executed Step = VAR,String,var_planshouldbevisible,TGTYPESCREENREG");
		var_planshouldbevisible = "null";
                 LOGGER.info("Executed Step = STORE,var_planshouldbevisible,null,TGTYPESCREENREG");
		try { 
		 List<WebElement> col = driver.findElements(By.xpath("//tbody[@id='workbenchForm:workbenchTabs:grid_data']//tr"));
		        System.out.println("No of cols are : " + col.size()); 
		 for (int i = 0; i < col.size(); i++) {
		        	
		        	    String a = Integer.toString(i);
		        	    String Statexpath ="//span[@id='workbenchForm:workbenchTabs:grid:R:countryAndStateCode']";
		        	    String XpathForRowR = Statexpath.replace("R", a);
		        	    String Effecxpath ="//span[@id='workbenchForm:workbenchTabs:grid:A:effectiveDate']";
		        	    String XpathForEffec = Effecxpath.replace("A", a);
		        	    String Appxpath ="//span[@id='workbenchForm:workbenchTabs:grid:J:approval']";
		        	    String XpathForApp = Appxpath.replace("J", a);
		        	    

						WebElement StateWeb = driver.findElement(By.xpath(XpathForRowR));
						String statetxt = StateWeb.getText();
						if(statetxt.contains(var_PolicyState))
						{
							WebElement EffWeb = driver.findElement(By.xpath(XpathForEffec));
							var_Efftxt = EffWeb.getText();
							WebElement AppWeb = driver.findElement(By.xpath(XpathForApp));
							var_Apptext = AppWeb.getText();
							
						}
						if(statetxt.contains("All Countries and States")||statetxt.contains("United States"))
						{
							WebElement EffWeb = driver.findElement(By.xpath(XpathForEffec));
							var_Efftxtforall = EffWeb.getText();
							WebElement AppWeb = driver.findElement(By.xpath(XpathForApp));
							var_Apptextforall = AppWeb.getText();
						}
						
						
		        }
		        
		        if(var_Apptextforall.contains("Yes"))
		        {
		        	 java.text.SimpleDateFormat format = new java.text.SimpleDateFormat("MM/dd/yyyy");
		             
		             try {
		     			java.util.Date effectivedateforplanAll = format.parse(var_Efftxtforall);
		     			java.util.Date policyeffectivedate = format.parse(var_PolicyEffectiveDate);
		              if(policyeffectivedate.after(effectivedateforplanAll)) {
		            	  
		            	  var_planshouldbevisible="planshouldbevisible";
		              }
		     							
		   //       Assert.assertTrue((StrtDate.before(Sysdate)||StrtDate.compareTo(Sysdate)==0) && StrtDate.after(EStrtDate),"Startdate is earlist than earlistWithperiodStartDate or greater Than currentdate");
				
		     		} 
		             catch (java.text.ParseException e) {
		     			// TODO Auto-generated catch block
		     			e.printStackTrace();
		     		}
		        }
		        if(!var_Efftxt.contains("null") && var_Apptext.contains("No") )
		        {
		        	java.text.SimpleDateFormat format = new java.text.SimpleDateFormat("MM/dd/yyyy");
		        	try {
		     			java.util.Date effectivedateforplanstate = format.parse(var_Efftxt);
		     			java.util.Date policyeffectivedate = format.parse(var_PolicyEffectiveDate);
		              if(policyeffectivedate.after(effectivedateforplanstate)&& policyeffectivedate.before(effectivedateforplanstate)) {
		            	  
		            	  var_planshouldbevisible="planshouldbevisible";
		              }
		              else {
		            	  var_planshouldbevisible="planshouldnotbevisible";
		              }
		     							
		   //       Assert.assertTrue((StrtDate.before(Sysdate)||StrtDate.compareTo(Sysdate)==0) && StrtDate.after(EStrtDate),"Startdate is earlist than earlistWithperiodStartDate or greater Than currentdate");
				
		     		} 
		             catch (java.text.ParseException e) {
		     			// TODO Auto-generated catch block
		     			e.printStackTrace();

		        }
		        }
		        
		        System.out.println("var_planshouldbevisible::"+var_planshouldbevisible);
		 
		 ;} 
		 catch(Exception e) { PrintVal("Renderer Script Exception : "+e.toString()); } 
		 PrintVal("Executed Step = CALLRENDERERSCRIPT,VALIDATEPLANFORISSUESTATE"); 
        TAP.$("ELE_BUTTON_GIASHOME","//span[text()='GIAS']","TGTYPESCREENREG");

        TAP.$("ELE_TXTBOX_SEARCHTASKS","//input[@id='workbenchForm:workbenchTabs:taskSearchContainer:taskSearchSearch']","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_SEARCHTASKS","//input[@id='workbenchForm:workbenchTabs:taskSearchContainer:taskSearchSearch']","PLAN SETUP","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_TASKSEARCH","//a[@id='workbenchForm:workbenchTabs:taskSearchContainer:tb_buttonSearch']//span[@class='fa fa-search fa-lg']","TGTYPESCREENREG");

        WAIT.$(1,"TGTYPESCREENREG");

        TAP.$("ELE_LINK_PLANSETUP","//span[contains(text(),'Plan Setup')]","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_PLAN","//input[@id='workbenchForm:workbenchTabs:grid:planCode_Col:filter']","IL2AUT","FALSE","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

        TAP.$("ELE_LABEL_BASESUPPLEMENTFIRSTRECORD","//span[@id='workbenchForm:workbenchTabs:grid:0:planCode']","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

		takeFullScreenShot();
                 LOGGER.info("Executed Step = TAKEFULLSCREENSHOT,TGTYPESCREENREG");
        TAP.$("ELE_BUTTON_GIASHOME","//span[text()='GIAS']","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

        CALL.$("ValidateBenefitPlan","TGTYPESCREENREG");

        TAP.$("ELE_TXTBOX_SEARCHTASKS","//input[@id='workbenchForm:workbenchTabs:taskSearchContainer:taskSearchSearch']","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_SEARCHTASKS","//input[@id='workbenchForm:workbenchTabs:taskSearchContainer:taskSearchSearch']","POLICY INQUIRY","FALSE","TGTYPESCREENREG");

        WAIT.$(1,"TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_TASKSEARCH","//a[@id='workbenchForm:workbenchTabs:taskSearchContainer:tb_buttonSearch']//span[@class='fa fa-search fa-lg']","TGTYPESCREENREG");

        WAIT.$(1,"TGTYPESCREENREG");

        TAP.$("ELE_LINK_POLICYINQUIRY","//span[contains(text(),'Policy Inquiry')]","TGTYPESCREENREG");

        WAIT.$(1,"TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_POLICYNUMBER","//input[@id='workbenchForm:workbenchTabs:policyNumber']",var_PolicyNumber,"FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_CONTINUE","//span[contains(text(),'Continue')]","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SHOWLINKS","//span[@class='ui-button-icon-left ui-icon ui-c fa fa-ellipsis-v fa-2x']","TGTYPESCREENREG");

        TAP.$("ELE_LINK_BENEFIT","//span[contains(text(),'Benefits')]","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

        TAP.$("ELE_DRPDWN_BENEFITPLAN","//label[@id='workbenchForm:workbenchTabs:initialPlanCode_label']","TGTYPESCREENREG");

        LOGGER.info("Executed Step = START IF");
        if (check(var_planshouldbevisible,"=",var_planshouldbevisible,"TGTYPESCREENREG")) {
         LOGGER.info("Executed Step = IF,var_planshouldbevisible,=,var_planshouldbevisible,TGTYPESCREENREG");
		try { 
		      WebElement dropdown = driver.findElement(By.xpath("//ul[@id='workbenchForm:workbenchTabs:initialPlanCode_items']"));
		       
		    //    dropdown.click();
		       
		        List<WebElement> options = dropdown.findElements(By.tagName("li"));
		        
		        System.out.println("options ::"+options.size());
		        for (WebElement option : options)
		        {
		            if (option.getText().equals("IL2AUT - ISWL PLAN FOR AUTOMATION PURPOSE ONLY"))
		            {
		                option.click(); // click the desired option
		                
		                System.out.println("inside block:::::");
		                
		                takeFullScreenShot();
		                break;
		            }
		        }
		 
		 ;} 
		 catch(Exception e) { PrintVal("Renderer Script Exception : "+e.toString()); } 
		 PrintVal("Executed Step = CALLRENDERERSCRIPT,SELECTPLANIL2AUT"); 
        }
         else {
          LOGGER.info("Executed Step = ELSE,TGTYPESCREENREG");
		try { 
		 WebElement dropdown = driver.findElement(By.xpath("//ul[@id='workbenchForm:workbenchTabs:initialPlanCode_items']"));
		       
		    //    dropdown.click();
		       
		        List<WebElement> options = dropdown.findElements(By.tagName("li"));
		        
		        System.out.println("options ::"+options.size());
		        for (WebElement option : options)
		        {
		            if (option.getText().equals("IL2AUT - ISWL PLAN FOR AUTOMATION PURPOSE ONLY"))
		            {
		                Assert.assertTrue(false,"IL2AUT - ISWL PLAN FOR AUTOMATION PURPOSE ONLY should not be in the list.............");
		                break;
		            }
		        }
		 
		 ;} 
		 catch(Exception e) { PrintVal("Renderer Script Exception : "+e.toString()); } 
		 PrintVal("Executed Step = CALLRENDERERSCRIPT,PLANIL2AUTSHOULDNOTPRESENT"); 
        }
          LOGGER.info("Executed Step = ENDIF,TGTYPESCREENREG");
		var_Count = var_Count + ( 1 );
                 LOGGER.info("Executed Step = INCREASEVAR,var_Count,1,TGTYPESCREENREG");
    	}
          LOGGER.info("Executed Step = WEND,TGTYPESCREENREG");
    	resetAttemps();

    }


    @Test
    public void reasonselectionwithdraw() {

        // No need to declare / print method name here, handled by TestNg listener
        // declare("reasonselectionwithdraw");

		int var_Count = 0;
                 LOGGER.info("Executed Step = VAR,Integer,var_Count,0,TGTYPESCREENREG");
		com.jayway.jsonpath.ReadContext var_CSVData = getJsonData("http://10.1.104.229/json/20200814/anyvrX.json");
                 LOGGER.info("Executed Step = VAR,Json,var_CSVData,http://10.1.104.229/json/20200814/anyvrX.json,TGTYPESCREENREG");
        LOGGER.info("Executed Step = REPEAT IF");
    	while (check(var_Count,"<",1,"TGTYPESCREENREG")) {
          LOGGER.info("Executed Step = WHILE,var_Count,<,1,TGTYPESCREENREG");
		String var_PolicyNumber = "null";
                 LOGGER.info("Executed Step = VAR,String,var_PolicyNumber,null,TGTYPESCREENREG");
		String var_Action = "null";
                 LOGGER.info("Executed Step = VAR,String,var_Action,null,TGTYPESCREENREG");
		var_PolicyNumber = getJsonData(var_CSVData , "$.records["+var_Count+"].PolicyNumber");
                 LOGGER.info("Executed Step = STORE,var_PolicyNumber,var_CSVData,$.records[{var_Count}].PolicyNumber,TGTYPESCREENREG");
		var_Action = getJsonData(var_CSVData , "$.records["+var_Count+"].Action");
                 LOGGER.info("Executed Step = STORE,var_Action,var_CSVData,$.records[{var_Count}].Action,TGTYPESCREENREG");
        CALL.$("FTJQA2Login","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

		try { 
		 WebElement html = driver.findElement(By.tagName("html"));
		    	
		    	System.out.println("html size" + html.getSize());
		    	System.out.println("html" + html.getTagName());

		    	for( int i =0 ;i<4;i++) {
		        html.sendKeys(Keys.chord(Keys.CONTROL, Keys.SUBTRACT));
		    	}
		 
		 ;} 
		 catch(Exception e) { PrintVal("Renderer Script Exception : "+e.toString()); } 
		 PrintVal("Executed Step = CALLRENDERERSCRIPT,FTJ41ZOOMMAGNIFICATIONCHANGE"); 
        TYPE.$("ELE_TXTBOX_USERNAME","//input[@id='guestForm:username']","x5080","FALSE","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_PASSWORD","//input[@id='guestForm:password']","Welcome123$","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_LOGIN","//button[@id='guestForm:loginButton']","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        CALL.$("PRECHECKREASONREQUIRED","TGTYPESCREENREG");

        TAP.$("ELE_TXTBOX_SEARCHTASKS","//input[@id='workbenchForm:workbenchTabs:taskSearchContainer:taskSearchSearch']","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_SEARCHTASKS","//input[@id='workbenchForm:workbenchTabs:taskSearchContainer:taskSearchSearch']","New Policy Application","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_TASKSEARCH","//a[@id='workbenchForm:workbenchTabs:taskSearchContainer:tb_buttonSearch']//span[@class='fa fa-search fa-lg']","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_NEWPOLICYAPP","//span[contains(text(),'New Policy Application')]","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_FILTERPOLICYNUMBER1","//input[@id='workbenchForm:workbenchTabs:grid:policyNumber_Col:filter']",var_PolicyNumber,"FALSE","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

        ASSERT.$("ELE_LINK_FIRSTPOLICYNUMBER","//span[@id='workbenchForm:workbenchTabs:grid:0:policyNumber']","VISIBLE","TGTYPESCREENREG");

        TAP.$("ELE_LINK_FIRSTPOLICYNUMBER","//span[@id='workbenchForm:workbenchTabs:grid:0:policyNumber']","TGTYPESCREENREG");

        WAIT.$(1,"TGTYPESCREENREG");

        ASSERT.$("ELE_GETVAL_POLICYSTATUSPENDINGISSUE","//span[@id='workbenchForm:workbenchTabs:contractStatusNewBusText']","CONTAINS","Pending","TGTYPESCREENREG");

        CALL.$("VALIDATEREASONREQUIRED","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SHOWLINKS","//span[@class='ui-button-icon-left ui-icon ui-c fa fa-ellipsis-v fa-2x']","TGTYPESCREENREG");

        LOGGER.info("Executed Step = START IF");
        if (check(var_Action,"CONTAINS","Withdraw","TGTYPESCREENREG")) {
         LOGGER.info("Executed Step = IF,var_Action,CONTAINS,Withdraw,TGTYPESCREENREG");
        TAP.$("ELE_LINK_WITHDRAW","//span[contains(text(),'Withdraw')]","TGTYPESCREENREG");

        }
         else {
          LOGGER.info("Executed Step = ELSE,TGTYPESCREENREG");
        TAP.$("ELE_LINK_DECLINE","//span[contains(text(),'Decline')]","TGTYPESCREENREG");

        }
          LOGGER.info("Executed Step = ENDIF,TGTYPESCREENREG");
        WAIT.$(1,"TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_VALIDATE","//span[contains(text(),'Validate')]","TGTYPESCREENREG");

        WAIT.$(1,"TGTYPESCREENREG");

        ASSERT.$("ELE_GETVAL_WARNMSG","//div[@id='workbenchForm:workbenchTabs:messages']//div[@class='ui-messages-error ui-corner-all']//li","=","Reason is required for decline or withdraw.","TGTYPESCREENREG");

		takeFullScreenShot();
                 LOGGER.info("Executed Step = TAKEFULLSCREENSHOT,TGTYPESCREENREG");
        CALL.$("POSTVALIDATEREASONREQUIRED","TGTYPESCREENREG");

        TAP.$("ELE_TAB_APPLICATIONENTRY","//a[contains(text(),'Application Entry')]","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

        ASSERT.$("ELE_LINK_FIRSTPOLICYNUMBER","//span[@id='workbenchForm:workbenchTabs:grid:0:policyNumber']","VISIBLE","TGTYPESCREENREG");

        ASSERT.$("ELE_GETVAL_FIRSTSTATUSSEARCHEDRESULT","//span[@id='workbenchForm:workbenchTabs:grid:0:status']","CONTAINS","Pending","TGTYPESCREENREG");

		var_Count = var_Count + ( 1 );
                 LOGGER.info("Executed Step = INCREASEVAR,var_Count,1,TGTYPESCREENREG");
    	}
          LOGGER.info("Executed Step = WEND,TGTYPESCREENREG");
    	resetAttemps();

    }


    @Test
    public void reasonselectiondecline() {

        // No need to declare / print method name here, handled by TestNg listener
        // declare("reasonselectiondecline");

		int var_Count = 0;
                 LOGGER.info("Executed Step = VAR,Integer,var_Count,0,TGTYPESCREENREG");
		com.jayway.jsonpath.ReadContext var_CSVData = getJsonData("http://10.1.104.229/json/20200814/Hocpfe.json");
                 LOGGER.info("Executed Step = VAR,Json,var_CSVData,http://10.1.104.229/json/20200814/Hocpfe.json,TGTYPESCREENREG");
        LOGGER.info("Executed Step = REPEAT IF");
    	while (check(var_Count,"<",1,"TGTYPESCREENREG")) {
          LOGGER.info("Executed Step = WHILE,var_Count,<,1,TGTYPESCREENREG");
		String var_PolicyNumber = "null";
                 LOGGER.info("Executed Step = VAR,String,var_PolicyNumber,null,TGTYPESCREENREG");
		String var_Action = "null";
                 LOGGER.info("Executed Step = VAR,String,var_Action,null,TGTYPESCREENREG");
		var_PolicyNumber = getJsonData(var_CSVData , "$.records["+var_Count+"].PolicyNumber");
                 LOGGER.info("Executed Step = STORE,var_PolicyNumber,var_CSVData,$.records[{var_Count}].PolicyNumber,TGTYPESCREENREG");
		var_Action = getJsonData(var_CSVData , "$.records["+var_Count+"].Action");
                 LOGGER.info("Executed Step = STORE,var_Action,var_CSVData,$.records[{var_Count}].Action,TGTYPESCREENREG");
        CALL.$("FTJQA2Login","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

		try { 
		 WebElement html = driver.findElement(By.tagName("html"));
		    	
		    	System.out.println("html size" + html.getSize());
		    	System.out.println("html" + html.getTagName());

		    	for( int i =0 ;i<4;i++) {
		        html.sendKeys(Keys.chord(Keys.CONTROL, Keys.SUBTRACT));
		    	}
		 
		 ;} 
		 catch(Exception e) { PrintVal("Renderer Script Exception : "+e.toString()); } 
		 PrintVal("Executed Step = CALLRENDERERSCRIPT,FTJ41ZOOMMAGNIFICATIONCHANGE"); 
        TYPE.$("ELE_TXTBOX_USERNAME","//input[@id='guestForm:username']","x5080","FALSE","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_PASSWORD","//input[@id='guestForm:password']","Welcome123$","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_LOGIN","//button[@id='guestForm:loginButton']","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        CALL.$("PRECHECKREASONREQUIRED","TGTYPESCREENREG");

        TAP.$("ELE_TXTBOX_SEARCHTASKS","//input[@id='workbenchForm:workbenchTabs:taskSearchContainer:taskSearchSearch']","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_SEARCHTASKS","//input[@id='workbenchForm:workbenchTabs:taskSearchContainer:taskSearchSearch']","New Policy Application","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_TASKSEARCH","//a[@id='workbenchForm:workbenchTabs:taskSearchContainer:tb_buttonSearch']//span[@class='fa fa-search fa-lg']","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_NEWPOLICYAPP","//span[contains(text(),'New Policy Application')]","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_FILTERPOLICYNUMBER1","//input[@id='workbenchForm:workbenchTabs:grid:policyNumber_Col:filter']",var_PolicyNumber,"FALSE","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

        ASSERT.$("ELE_LINK_FIRSTPOLICYNUMBER","//span[@id='workbenchForm:workbenchTabs:grid:0:policyNumber']","VISIBLE","TGTYPESCREENREG");

        TAP.$("ELE_LINK_FIRSTPOLICYNUMBER","//span[@id='workbenchForm:workbenchTabs:grid:0:policyNumber']","TGTYPESCREENREG");

        WAIT.$(1,"TGTYPESCREENREG");

        ASSERT.$("ELE_GETVAL_POLICYSTATUSPENDINGISSUE","//span[@id='workbenchForm:workbenchTabs:contractStatusNewBusText']","CONTAINS","Pending","TGTYPESCREENREG");

        CALL.$("VALIDATEREASONREQUIRED","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SHOWLINKS","//span[@class='ui-button-icon-left ui-icon ui-c fa fa-ellipsis-v fa-2x']","TGTYPESCREENREG");

        LOGGER.info("Executed Step = START IF");
        if (check(var_Action,"CONTAINS","Withdraw","TGTYPESCREENREG")) {
         LOGGER.info("Executed Step = IF,var_Action,CONTAINS,Withdraw,TGTYPESCREENREG");
        TAP.$("ELE_LINK_WITHDRAW","//span[contains(text(),'Withdraw')]","TGTYPESCREENREG");

        }
         else {
          LOGGER.info("Executed Step = ELSE,TGTYPESCREENREG");
        TAP.$("ELE_LINK_DECLINE","//span[contains(text(),'Decline')]","TGTYPESCREENREG");

        }
          LOGGER.info("Executed Step = ENDIF,TGTYPESCREENREG");
        WAIT.$(1,"TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_VALIDATE","//span[contains(text(),'Validate')]","TGTYPESCREENREG");

        WAIT.$(1,"TGTYPESCREENREG");

        ASSERT.$("ELE_GETVAL_WARNMSG","//div[@id='workbenchForm:workbenchTabs:messages']//div[@class='ui-messages-error ui-corner-all']//li","=","Reason is required for decline or withdraw.","TGTYPESCREENREG");

		takeFullScreenShot();
                 LOGGER.info("Executed Step = TAKEFULLSCREENSHOT,TGTYPESCREENREG");
        CALL.$("POSTVALIDATEREASONREQUIRED","TGTYPESCREENREG");

        TAP.$("ELE_TAB_APPLICATIONENTRY","//a[contains(text(),'Application Entry')]","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

        ASSERT.$("ELE_LINK_FIRSTPOLICYNUMBER","//span[@id='workbenchForm:workbenchTabs:grid:0:policyNumber']","VISIBLE","TGTYPESCREENREG");

        ASSERT.$("ELE_GETVAL_FIRSTSTATUSSEARCHEDRESULT","//span[@id='workbenchForm:workbenchTabs:grid:0:status']","CONTAINS","Pending","TGTYPESCREENREG");

		var_Count = var_Count + ( 1 );
                 LOGGER.info("Executed Step = INCREASEVAR,var_Count,1,TGTYPESCREENREG");
    	}
          LOGGER.info("Executed Step = WEND,TGTYPESCREENREG");
    	resetAttemps();

    }


    @Test
    public void ftj41modf121individualmultiplerows() {

        // No need to declare / print method name here, handled by TestNg listener
        // declare("ftj41modf121individualmultiplerows");

		int var_Count = 0;
                 LOGGER.info("Executed Step = VAR,Integer,var_Count,0,TGTYPESCREENREG");
		com.jayway.jsonpath.ReadContext var_CSVData = getJsonData("http://10.1.104.229/json/20200814/aBSHVX.json");
                 LOGGER.info("Executed Step = VAR,Json,var_CSVData,http://10.1.104.229/json/20200814/aBSHVX.json,TGTYPESCREENREG");
        LOGGER.info("Executed Step = REPEAT IF");
    	while (check(var_Count,"<",1,"TGTYPESCREENREG")) {
          LOGGER.info("Executed Step = WHILE,var_Count,<,1,TGTYPESCREENREG");
		String var_CollectionNumber1;
                 LOGGER.info("Executed Step = VAR,String,var_CollectionNumber1,TGTYPESCREENREG");
		var_CollectionNumber1 = getJsonData(var_CSVData , "$.records["+var_Count+"].CollectionNumber1");
                 LOGGER.info("Executed Step = STORE,var_CollectionNumber1,var_CSVData,$.records[{var_Count}].CollectionNumber1,TGTYPESCREENREG");
		String var_CollectionNumber2;
                 LOGGER.info("Executed Step = VAR,String,var_CollectionNumber2,TGTYPESCREENREG");
		var_CollectionNumber2 = getJsonData(var_CSVData , "$.records["+var_Count+"].CollectionNumber2");
                 LOGGER.info("Executed Step = STORE,var_CollectionNumber2,var_CSVData,$.records[{var_Count}].CollectionNumber2,TGTYPESCREENREG");
		String var_CollectionNumber3;
                 LOGGER.info("Executed Step = VAR,String,var_CollectionNumber3,TGTYPESCREENREG");
		var_CollectionNumber3 = getJsonData(var_CSVData , "$.records["+var_Count+"].CollectionNumber3");
                 LOGGER.info("Executed Step = STORE,var_CollectionNumber3,var_CSVData,$.records[{var_Count}].CollectionNumber3,TGTYPESCREENREG");
		String var_AmntRec1;
                 LOGGER.info("Executed Step = VAR,String,var_AmntRec1,TGTYPESCREENREG");
		var_AmntRec1 = getJsonData(var_CSVData , "$.records["+var_Count+"].AmntRec1");
                 LOGGER.info("Executed Step = STORE,var_AmntRec1,var_CSVData,$.records[{var_Count}].AmntRec1,TGTYPESCREENREG");
		String var_AmntRec2;
                 LOGGER.info("Executed Step = VAR,String,var_AmntRec2,TGTYPESCREENREG");
		var_AmntRec2 = getJsonData(var_CSVData , "$.records["+var_Count+"].AmntRec2");
                 LOGGER.info("Executed Step = STORE,var_AmntRec2,var_CSVData,$.records[{var_Count}].AmntRec2,TGTYPESCREENREG");
		String var_AmntRec3;
                 LOGGER.info("Executed Step = VAR,String,var_AmntRec3,TGTYPESCREENREG");
		var_AmntRec3 = getJsonData(var_CSVData , "$.records["+var_Count+"].AmntRec3");
                 LOGGER.info("Executed Step = STORE,var_AmntRec3,var_CSVData,$.records[{var_Count}].AmntRec3,TGTYPESCREENREG");
		String var_WrongName;
                 LOGGER.info("Executed Step = VAR,String,var_WrongName,TGTYPESCREENREG");
		var_WrongName = getJsonData(var_CSVData , "$.records["+var_Count+"].WrongName");
                 LOGGER.info("Executed Step = STORE,var_WrongName,var_CSVData,$.records[{var_Count}].WrongName,TGTYPESCREENREG");
		String var_PayorLastName;
                 LOGGER.info("Executed Step = VAR,String,var_PayorLastName,TGTYPESCREENREG");
		var_PayorLastName = getJsonData(var_CSVData , "$.records["+var_Count+"].PayorLastName");
                 LOGGER.info("Executed Step = STORE,var_PayorLastName,var_CSVData,$.records[{var_Count}].PayorLastName,TGTYPESCREENREG");
		String var_InsuredLastName;
                 LOGGER.info("Executed Step = VAR,String,var_InsuredLastName,TGTYPESCREENREG");
		var_InsuredLastName = getJsonData(var_CSVData , "$.records["+var_Count+"].InsuredLastName");
                 LOGGER.info("Executed Step = STORE,var_InsuredLastName,var_CSVData,$.records[{var_Count}].InsuredLastName,TGTYPESCREENREG");
		String var_CollectionNumber4;
                 LOGGER.info("Executed Step = VAR,String,var_CollectionNumber4,TGTYPESCREENREG");
		var_CollectionNumber4 = getJsonData(var_CSVData , "$.records["+var_Count+"].CollectionNumber4");
                 LOGGER.info("Executed Step = STORE,var_CollectionNumber4,var_CSVData,$.records[{var_Count}].CollectionNumber4,TGTYPESCREENREG");
		String var_GroupName;
                 LOGGER.info("Executed Step = VAR,String,var_GroupName,TGTYPESCREENREG");
		var_GroupName = getJsonData(var_CSVData , "$.records["+var_Count+"].GroupName");
                 LOGGER.info("Executed Step = STORE,var_GroupName,var_CSVData,$.records[{var_Count}].GroupName,TGTYPESCREENREG");
		String var_AmntRec4;
                 LOGGER.info("Executed Step = VAR,String,var_AmntRec4,TGTYPESCREENREG");
		var_AmntRec4 = getJsonData(var_CSVData , "$.records["+var_Count+"].AmntRec4");
                 LOGGER.info("Executed Step = STORE,var_AmntRec4,var_CSVData,$.records[{var_Count}].AmntRec4,TGTYPESCREENREG");
		String var_CollectionNumber5;
                 LOGGER.info("Executed Step = VAR,String,var_CollectionNumber5,TGTYPESCREENREG");
		var_CollectionNumber5 = getJsonData(var_CSVData , "$.records["+var_Count+"].CollectionNumber5");
                 LOGGER.info("Executed Step = STORE,var_CollectionNumber5,var_CSVData,$.records[{var_Count}].CollectionNumber5,TGTYPESCREENREG");
		String var_UpperAndLower;
                 LOGGER.info("Executed Step = VAR,String,var_UpperAndLower,TGTYPESCREENREG");
		var_UpperAndLower = getJsonData(var_CSVData , "$.records["+var_Count+"].UpperAndLower");
                 LOGGER.info("Executed Step = STORE,var_UpperAndLower,var_CSVData,$.records[{var_Count}].UpperAndLower,TGTYPESCREENREG");
		String var_AmntRec5;
                 LOGGER.info("Executed Step = VAR,String,var_AmntRec5,TGTYPESCREENREG");
		var_AmntRec5 = getJsonData(var_CSVData , "$.records["+var_Count+"].AmntRec5");
                 LOGGER.info("Executed Step = STORE,var_AmntRec5,var_CSVData,$.records[{var_Count}].AmntRec5,TGTYPESCREENREG");
		String var_CollectionNumber6;
                 LOGGER.info("Executed Step = VAR,String,var_CollectionNumber6,TGTYPESCREENREG");
		var_CollectionNumber6 = getJsonData(var_CSVData , "$.records["+var_Count+"].CollectionNumber6");
                 LOGGER.info("Executed Step = STORE,var_CollectionNumber6,var_CSVData,$.records[{var_Count}].CollectionNumber6,TGTYPESCREENREG");
		String var_SpaceInName;
                 LOGGER.info("Executed Step = VAR,String,var_SpaceInName,TGTYPESCREENREG");
		var_SpaceInName = getJsonData(var_CSVData , "$.records["+var_Count+"].SpaceInName");
                 LOGGER.info("Executed Step = STORE,var_SpaceInName,var_CSVData,$.records[{var_Count}].SpaceInName,TGTYPESCREENREG");
		String var_AmntRec6;
                 LOGGER.info("Executed Step = VAR,String,var_AmntRec6,TGTYPESCREENREG");
		var_AmntRec6 = getJsonData(var_CSVData , "$.records["+var_Count+"].AmntRec6");
                 LOGGER.info("Executed Step = STORE,var_AmntRec6,var_CSVData,$.records[{var_Count}].AmntRec6,TGTYPESCREENREG");
		String var_CollectionNumber7;
                 LOGGER.info("Executed Step = VAR,String,var_CollectionNumber7,TGTYPESCREENREG");
		var_CollectionNumber7 = getJsonData(var_CSVData , "$.records["+var_Count+"].CollectionNumber7");
                 LOGGER.info("Executed Step = STORE,var_CollectionNumber7,var_CSVData,$.records[{var_Count}].CollectionNumber7,TGTYPESCREENREG");
		String var_Hyphen;
                 LOGGER.info("Executed Step = VAR,String,var_Hyphen,TGTYPESCREENREG");
		var_Hyphen = getJsonData(var_CSVData , "$.records["+var_Count+"].Hyphen");
                 LOGGER.info("Executed Step = STORE,var_Hyphen,var_CSVData,$.records[{var_Count}].Hyphen,TGTYPESCREENREG");
		String var_AmntRec7;
                 LOGGER.info("Executed Step = VAR,String,var_AmntRec7,TGTYPESCREENREG");
		var_AmntRec7 = getJsonData(var_CSVData , "$.records["+var_Count+"].AmntRec7");
                 LOGGER.info("Executed Step = STORE,var_AmntRec7,var_CSVData,$.records[{var_Count}].AmntRec7,TGTYPESCREENREG");
		String var_CollectionNumber8;
                 LOGGER.info("Executed Step = VAR,String,var_CollectionNumber8,TGTYPESCREENREG");
		var_CollectionNumber8 = getJsonData(var_CSVData , "$.records["+var_Count+"].CollectionNumber8");
                 LOGGER.info("Executed Step = STORE,var_CollectionNumber8,var_CSVData,$.records[{var_Count}].CollectionNumber8,TGTYPESCREENREG");
		String var_SecondOwner;
                 LOGGER.info("Executed Step = VAR,String,var_SecondOwner,TGTYPESCREENREG");
		var_SecondOwner = getJsonData(var_CSVData , "$.records["+var_Count+"].SecondOwner");
                 LOGGER.info("Executed Step = STORE,var_SecondOwner,var_CSVData,$.records[{var_Count}].SecondOwner,TGTYPESCREENREG");
		String var_AmntRec8;
                 LOGGER.info("Executed Step = VAR,String,var_AmntRec8,TGTYPESCREENREG");
		var_AmntRec8 = getJsonData(var_CSVData , "$.records["+var_Count+"].AmntRec8");
                 LOGGER.info("Executed Step = STORE,var_AmntRec8,var_CSVData,$.records[{var_Count}].AmntRec8,TGTYPESCREENREG");
		String var_CollectionNumber9;
                 LOGGER.info("Executed Step = VAR,String,var_CollectionNumber9,TGTYPESCREENREG");
		var_CollectionNumber9 = getJsonData(var_CSVData , "$.records["+var_Count+"].CollectionNumber9");
                 LOGGER.info("Executed Step = STORE,var_CollectionNumber9,var_CSVData,$.records[{var_Count}].CollectionNumber9,TGTYPESCREENREG");
		String var_AmntRec9;
                 LOGGER.info("Executed Step = VAR,String,var_AmntRec9,TGTYPESCREENREG");
		var_AmntRec9 = getJsonData(var_CSVData , "$.records["+var_Count+"].AmntRec9");
                 LOGGER.info("Executed Step = STORE,var_AmntRec9,var_CSVData,$.records[{var_Count}].AmntRec9,TGTYPESCREENREG");
		String var_OwnerDiffClients;
                 LOGGER.info("Executed Step = VAR,String,var_OwnerDiffClients,TGTYPESCREENREG");
		var_OwnerDiffClients = getJsonData(var_CSVData , "$.records["+var_Count+"].OwnerDiffClients");
                 LOGGER.info("Executed Step = STORE,var_OwnerDiffClients,var_CSVData,$.records[{var_Count}].OwnerDiffClients,TGTYPESCREENREG");
		String var_OwnerUpperLower;
                 LOGGER.info("Executed Step = VAR,String,var_OwnerUpperLower,TGTYPESCREENREG");
		var_OwnerUpperLower = getJsonData(var_CSVData , "$.records["+var_Count+"].OwnerUpperLower");
                 LOGGER.info("Executed Step = STORE,var_OwnerUpperLower,var_CSVData,$.records[{var_Count}].OwnerUpperLower,TGTYPESCREENREG");
		String var_OwnerSpace;
                 LOGGER.info("Executed Step = VAR,String,var_OwnerSpace,TGTYPESCREENREG");
		var_OwnerSpace = getJsonData(var_CSVData , "$.records["+var_Count+"].OwnerSpace");
                 LOGGER.info("Executed Step = STORE,var_OwnerSpace,var_CSVData,$.records[{var_Count}].OwnerSpace,TGTYPESCREENREG");
		String var_OwnerHyphen;
                 LOGGER.info("Executed Step = VAR,String,var_OwnerHyphen,TGTYPESCREENREG");
		var_OwnerHyphen = getJsonData(var_CSVData , "$.records["+var_Count+"].OwnerHyphen");
                 LOGGER.info("Executed Step = STORE,var_OwnerHyphen,var_CSVData,$.records[{var_Count}].OwnerHyphen,TGTYPESCREENREG");
		String var_OwnerOneOfTwo;
                 LOGGER.info("Executed Step = VAR,String,var_OwnerOneOfTwo,TGTYPESCREENREG");
		var_OwnerOneOfTwo = getJsonData(var_CSVData , "$.records["+var_Count+"].OwnerOneOfTwo");
                 LOGGER.info("Executed Step = STORE,var_OwnerOneOfTwo,var_CSVData,$.records[{var_Count}].OwnerOneOfTwo,TGTYPESCREENREG");
		String var_CompanyOwner;
                 LOGGER.info("Executed Step = VAR,String,var_CompanyOwner,TGTYPESCREENREG");
		var_CompanyOwner = getJsonData(var_CSVData , "$.records["+var_Count+"].CompanyOwner");
                 LOGGER.info("Executed Step = STORE,var_CompanyOwner,var_CSVData,$.records[{var_Count}].CompanyOwner,TGTYPESCREENREG");
		String var_CompanyOwnerHyphen;
                 LOGGER.info("Executed Step = VAR,String,var_CompanyOwnerHyphen,TGTYPESCREENREG");
		var_CompanyOwnerHyphen = getJsonData(var_CSVData , "$.records["+var_Count+"].CompanyOwnerHyphen");
                 LOGGER.info("Executed Step = STORE,var_CompanyOwnerHyphen,var_CSVData,$.records[{var_Count}].CompanyOwnerHyphen,TGTYPESCREENREG");
		String var_OwnerOneCharacter;
                 LOGGER.info("Executed Step = VAR,String,var_OwnerOneCharacter,TGTYPESCREENREG");
		var_OwnerOneCharacter = getJsonData(var_CSVData , "$.records["+var_Count+"].OwnerOneCharacter");
                 LOGGER.info("Executed Step = STORE,var_OwnerOneCharacter,var_CSVData,$.records[{var_Count}].OwnerOneCharacter,TGTYPESCREENREG");
		String var_OwnerTwoCharacter;
                 LOGGER.info("Executed Step = VAR,String,var_OwnerTwoCharacter,TGTYPESCREENREG");
		var_OwnerTwoCharacter = getJsonData(var_CSVData , "$.records["+var_Count+"].OwnerTwoCharacter");
                 LOGGER.info("Executed Step = STORE,var_OwnerTwoCharacter,var_CSVData,$.records[{var_Count}].OwnerTwoCharacter,TGTYPESCREENREG");
        CALL.$("FTJQA2Login","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

		try { 
		 WebElement html = driver.findElement(By.tagName("html"));
		    	
		    	System.out.println("html size" + html.getSize());
		    	System.out.println("html" + html.getTagName());

		    	for( int i =0 ;i<4;i++) {
		        html.sendKeys(Keys.chord(Keys.CONTROL, Keys.SUBTRACT));
		    	}
		 
		 ;} 
		 catch(Exception e) { PrintVal("Renderer Script Exception : "+e.toString()); } 
		 PrintVal("Executed Step = CALLRENDERERSCRIPT,FTJ41ZOOMMAGNIFICATIONCHANGE"); 
        TYPE.$("ELE_TXTBOX_USERNAME","//input[@id='guestForm:username']","x5080","FALSE","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_PASSWORD","//input[@id='guestForm:password']","Welcome123$","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_LOGIN","//button[@id='guestForm:loginButton']","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        CALL.$("BatchPaymentValidationFieldChecks","TGTYPESCREENREG");

        TAP.$("ELE_TXTBOX_SEARCHTASKS","//input[@id='workbenchForm:workbenchTabs:taskSearchContainer:taskSearchSearch']","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_SEARCHTASKS","//input[@id='workbenchForm:workbenchTabs:taskSearchContainer:taskSearchSearch']","BLC000","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_TASKSEARCH","//a[@id='workbenchForm:workbenchTabs:taskSearchContainer:tb_buttonSearch']//span[@class='fa fa-search fa-lg']","TGTYPESCREENREG");

        WAIT.$(1,"TGTYPESCREENREG");

        TAP.$("ELE_LINK_BILLINGANDPAYMENTPROPERTIES","//span[contains(text(),'Billing and Payment Properties')]","TGTYPESCREENREG");

        WAIT.$(1,"TGTYPESCREENREG");

        ASSERT.$("ELE_GETVAL_BATCHPAYMENTNAMEVALIDATION","//span[@id='workbenchForm:workbenchTabs:batchPaymentLastNameValidation']","VISIBLE","TGTYPESCREENFULL");

        ASSERT.$("ELE_GETVAL_NUMBEROFNAMEPOSITIONSTOVALIDATE","//span[@id='workbenchForm:workbenchTabs:noOfNamePositions']","VISIBLE","TGTYPESCREENREG");

        ASSERT.$("ELE_GETVAL_NAMEVALIDATIONCASESENSITIVE","//span[@id='workbenchForm:workbenchTabs:nameValidationCaseSensitive']","VISIBLE","TGTYPESCREENREG");

        TAP.$("ELE_LOGO_GIAS","//span[text()='GIAS']","TGTYPESCREENREG");

        WAIT.$(1,"TGTYPESCREENREG");

        CALL.$("BatchPolicyPaymentMultipleValidationsAtOnce","TGTYPESCREENREG");

        SWIPE.$("DOWN","TGTYPESCREENREG");

        SWIPE.$("DOWN","TGTYPESCREENREG");

        TAP.$("ELE_TXTBOX_SEARCHTASKS","//input[@id='workbenchForm:workbenchTabs:taskSearchContainer:taskSearchSearch']","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_SEARCHTASKS","//input[@id='workbenchForm:workbenchTabs:taskSearchContainer:taskSearchSearch']","Batch Policy Payment","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_TASKSEARCH","//a[@id='workbenchForm:workbenchTabs:taskSearchContainer:tb_buttonSearch']//span[@class='fa fa-search fa-lg']","TGTYPESCREENREG");

        WAIT.$(1,"TGTYPESCREENREG");

        TAP.$("ELE_LINK_BATCHPOLICYPAYMENT","//span[contains(text(),'Batch Policy Payment')]","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

        TAP.$("ELE_LINK_BATCHNUMBER_ROW1","//tbody[@id='workbenchForm:workbenchTabs:grid_data']//tr[1]","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

        ASSERT.$("ELE_GETVAL_NAMECOLUMN_BATCHPOLICYPAYMENT","//span[contains(text(),'Name')]","VISIBLE","TGTYPESCREENREG");

        CALL.$("CollectionNumberAndAmountRecievedRow1","TGTYPESCREENREG");

        TAP.$("ELE_TXTBOX_COLLECTIONNUMBER_TAP_ROW1","//div[@class='ui-datatable ui-widget dynamicTable']//tr[1]//td[1]//div[1]//div[1]","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_COLLECTIONNUMBER_TYPE_ROW1","//input[@name='workbenchForm:workbenchTabs:grid_blc400s2:0:in_collectionNumber_Col']",var_CollectionNumber1,"FALSE","TGTYPESCREENREG");

        TAP.$("ELE_TXTBOX_AMOUNTRECEIVED_TAP_ROW1","//tr[1]//td[3]//div[1]//div[1]","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_AMOUNTRECEIVED_TYPE_ROW1","//input[@name='workbenchForm:workbenchTabs:grid_blc400s2:0:in_amountReceived_Col_input']",var_AmntRec1,"FALSE","TGTYPESCREENREG");

        CALL.$("CollectionNumberAndAmountRecievedRow2","TGTYPESCREENREG");

        SWIPE.$("UP","TGTYPESCREENREG");

        TAP.$("ELE_TXTBOX_COLLECTIONNUMBER_TAP_ROW2","//div[@class='ui-datatable ui-widget dynamicTable']//tr[2]//td[1]//div[1]//div[1]","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_COLLECTIONNUMBER_TYPE_ROW2","//input[@name='workbenchForm:workbenchTabs:grid_blc400s2:1:in_collectionNumber_Col']",var_CollectionNumber2,"FALSE","TGTYPESCREENREG");

        TAP.$("ELE_TXTBOX_AMOUNTRECEIVED_TAP_ROW2","//tr[2]//td[3]//div[1]//div[1]","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_AMOUNTRECEIVED_TYPE_ROW2","//input[@name='workbenchForm:workbenchTabs:grid_blc400s2:1:in_amountReceived_Col_input']",var_AmntRec2,"FALSE","TGTYPESCREENREG");

        TAP.$("ELE_TXTBOX_NAMECOLUMN_TAP_ROW2","//tr[2]//td[2]//div[1]//div[1]","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_NAME_TYPE_ROW2","//input[@name='workbenchForm:workbenchTabs:grid_blc400s2:1:in_ownerName_Col']",var_WrongName,"FALSE","TGTYPESCREENREG");

        CALL.$("CollectionNumberAndAmountRecievedRow3","TGTYPESCREENREG");

        TAP.$("ELE_TXTBOX_COLLECTIONNUMBER_TAP_ROW3","//tr[3]//td[1]//div[1]//div[1]","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_COLLECTIONNUMBER_TYPE_ROW3","//input[@name='workbenchForm:workbenchTabs:grid_blc400s2:2:in_collectionNumber_Col']",var_CollectionNumber3,"FALSE","TGTYPESCREENREG");

        TAP.$("ELE_TXTBOX_AMOUNTRECEIVED_TAP_ROW3","//tr[3]//td[3]//div[1]//div[1]","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_AMOUNTRECEIVED_TYPE_ROW3","//input[@name='workbenchForm:workbenchTabs:grid_blc400s2:2:in_amountReceived_Col_input']",var_AmntRec3,"FALSE","TGTYPESCREENREG");

        TAP.$("ELE_TXTBOX_NAMECOLUMN_TAP_ROW3","//tr[3]//td[2]//div[1]//div[1]","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_NAME_TYPE_ROW3","//input[@name='workbenchForm:workbenchTabs:grid_blc400s2:2:in_ownerName_Col']",var_GroupName,"FALSE","TGTYPESCREENREG");

        CALL.$("CollectionNumberAndAmountRecievedRow4","TGTYPESCREENREG");

        TAP.$("ELE_TXTBOX_COLLECTIONNUMBER_TAP_ROW4","//tr[4]//td[1]//div[1]//div[1]","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_COLLECTIONNUMBER_TYPE_ROW4","//input[@name='workbenchForm:workbenchTabs:grid_blc400s2:3:in_collectionNumber_Col']",var_CollectionNumber4,"FALSE","TGTYPESCREENREG");

        TAP.$("ELE_TXTBOX_AMOUNTRECEIVED_TAP_ROW4","//tr[4]//td[3]//div[1]//div[1]","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_AMOUNTRECEIVED_TYPE_ROW4","//input[@name='workbenchForm:workbenchTabs:grid_blc400s2:3:in_amountReceived_Col_input']",var_AmntRec4,"FALSE","TGTYPESCREENREG");

        TAP.$("ELE_TXTBOX_NAMECOLUMN_TAP_ROW4","//tr[4]//td[2]//div[1]//div[1]","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_NAME_TYPE_ROW4","//input[@name='workbenchForm:workbenchTabs:grid_blc400s2:3:in_ownerName_Col']",var_PayorLastName,"FALSE","TGTYPESCREENREG");

        CALL.$("CollectionNumberAndAmountRecievedRow5","TGTYPESCREENREG");

        TAP.$("ELE_TXTBOX_COLLECTIONNUMBER_TAP_ROW5","//div[@class='ui-datatable ui-widget dynamicTable']//tr[5]//td[1]//div[1]//div[1]","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_COLLECTIONNUMBER_TYPE_ROW5","//input[@name='workbenchForm:workbenchTabs:grid_blc400s2:4:in_collectionNumber_Col']",var_CollectionNumber5,"FALSE","TGTYPESCREENREG");

        TAP.$("ELE_TXTBOX_AMOUNTRECEIVED_TAP_ROW5","//tr[5]//td[3]//div[1]//div[1]","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_AMOUNTRECEIVED_TYPE_ROW5","//input[@name='workbenchForm:workbenchTabs:grid_blc400s2:4:in_amountReceived_Col_input']",var_AmntRec5,"FALSE","TGTYPESCREENREG");

        TAP.$("ELE_TXTBOX_NAMECOLUMN_TAP_ROW5","//div[@class='ui-datatable ui-widget dynamicTable']//tr[5]//td[2]//div[1]//div[1]","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_NAME_TYPE_ROW5","//input[@name='workbenchForm:workbenchTabs:grid_blc400s2:4:in_ownerName_Col']",var_InsuredLastName,"FALSE","TGTYPESCREENREG");

        CALL.$("CollectionNumberAndAmountRecievedRow6","TGTYPESCREENREG");

        TAP.$("ELE_TXTBOX_COLLECTIONNUMBER_TAP_ROW6","//tr[6]//td[1]//div[1]//div[1]","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_COLLECTIONNUMBER_TYPE_ROW6","//input[@name='workbenchForm:workbenchTabs:grid_blc400s2:5:in_collectionNumber_Col']",var_CollectionNumber6,"FALSE","TGTYPESCREENREG");

        TAP.$("ELE_TXTBOX_AMOUNTRECEIVED_TAP_ROW6","//tr[6]//td[3]//div[1]//div[1]","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_AMOUNTRECEIVED_TYPE_ROW6","//input[@name='workbenchForm:workbenchTabs:grid_blc400s2:5:in_amountReceived_Col_input']",var_AmntRec6,"FALSE","TGTYPESCREENREG");

        TAP.$("ELE_TXTBOX_NAMECOLUMN_TAP_ROW6","//tr[6]//td[2]//div[1]//div[1]","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_NAME_TYPE_ROW6","//input[@name='workbenchForm:workbenchTabs:grid_blc400s2:5:in_ownerName_Col']",var_UpperAndLower,"FALSE","TGTYPESCREENREG");

        CALL.$("CollectionNumberAndAmountRecievedRow7","TGTYPESCREENREG");

        SWIPE.$("UP","TGTYPESCREENREG");

        TAP.$("ELE_TXTBOX_COLLECTIONNUMBER_TAP_ROW7","//tr[7]//td[1]//div[1]//div[1]","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_COLLECTIONNUMBER_TYPE_ROW7","//input[@name='workbenchForm:workbenchTabs:grid_blc400s2:6:in_collectionNumber_Col']",var_CollectionNumber7,"FALSE","TGTYPESCREENREG");

        TAP.$("ELE_TXTBOX_AMOUNTRECEIVED_TAP_ROW7","//tr[7]//td[3]//div[1]//div[1]","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_AMOUNTRECEIVED_TYPE_ROW7","//input[@name='workbenchForm:workbenchTabs:grid_blc400s2:6:in_amountReceived_Col_input']",var_AmntRec7,"FALSE","TGTYPESCREENREG");

        TAP.$("ELE_TXTBOX_NAMECOLUMN_TAP_ROW7","//tr[7]//td[2]//div[1]//div[1]","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_NAME_TYPE_ROW7","//input[@name='workbenchForm:workbenchTabs:grid_blc400s2:6:in_ownerName_Col']",var_SpaceInName,"FALSE","TGTYPESCREENREG");

        CALL.$("CollectionNumberAndAmountRecievedRow8","TGTYPESCREENREG");

        TAP.$("ELE_TXTBOX_COLLECTIONNUMBER_TAP_ROW8","//tr[8]//td[1]//div[1]//div[1]","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_COLLECTIONNUMBER_TYPE_ROW8","//input[@name='workbenchForm:workbenchTabs:grid_blc400s2:7:in_collectionNumber_Col']",var_CollectionNumber8,"FALSE","TGTYPESCREENREG");

        TAP.$("ELE_TXTBOX_AMOUNTRECEIVED_TAP_ROW8","//tr[8]//td[3]//div[1]//div[1]","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_AMOUNTRECEIVED_TYPE_ROW8","//input[@name='workbenchForm:workbenchTabs:grid_blc400s2:7:in_amountReceived_Col_input']",var_AmntRec8,"FALSE","TGTYPESCREENREG");

        TAP.$("ELE_TXTBOX_NAMECOLUMN_TAP_ROW8","//tr[8]//td[2]//div[1]//div[1]","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_NAME_TYPE_ROW8","//input[@name='workbenchForm:workbenchTabs:grid_blc400s2:7:in_ownerName_Col']",var_Hyphen,"FALSE","TGTYPESCREENREG");

        CALL.$("CollectionNumberAndAmountRecievedRow9","TGTYPESCREENREG");

        TAP.$("ELE_TXTBOX_COLLECTIONNUMBER_TAP_ROW9","//tr[9]//td[1]//div[1]//div[1]","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_COLLECTIONNUMBER_TYPE_ROW9","//input[@name='workbenchForm:workbenchTabs:grid_blc400s2:8:in_collectionNumber_Col']",var_CollectionNumber9,"FALSE","TGTYPESCREENREG");

        TAP.$("ELE_TXTBOX_AMOUNTRECEIVED_TAP_ROW9","//tr[9]//td[3]//div[1]//div[1]","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_AMOUNTRECEIVED_TYPE_ROW9","//input[@name='workbenchForm:workbenchTabs:grid_blc400s2:8:in_amountReceived_Col_input']",var_AmntRec9,"FALSE","TGTYPESCREENREG");

        TAP.$("ELE_TXTBOX_NAMECOLUMN_TAP_ROW9","//tr[9]//td[2]//div[1]//div[1]","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_NAME_TYPE_ROW9","//input[@name='workbenchForm:workbenchTabs:grid_blc400s2:8:in_ownerName_Col']",var_SecondOwner,"FALSE","TGTYPESCREENREG");

        TAP.$("ELE_TXTBOX_COLLECTIONNUMBER_TAP_ROW10","//tr[10]//td[1]//div[1]//div[1]","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_VALIDATE","//span[contains(text(),'Validate')]","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_DISPLAYMESSAGE_ROW1","//button[@id='workbenchForm:workbenchTabs:grid_blc400s2:0:msgbox_button']","TGTYPESCREENREG");

        WAIT.$(3,"TGTYPESCREENREG");

        ASSERT.$("ELE_GETMSG_DISPLAYMESSAGES","//div[@id='workbenchForm:workbenchTabs:messagesInDialog']//span[@class='ui-messages-error-summary']","CONTAINS","Name cannot be blank.","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_OK","//span[contains(text(),'OK')]","TGTYPESCREENREG");

        WAIT.$(1,"TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_DISPLAYMESSAGE_ROW2","//button[@id='workbenchForm:workbenchTabs:grid_blc400s2:1:msgbox_button']","TGTYPESCREENREG");

        WAIT.$(3,"TGTYPESCREENREG");

        ASSERT.$("ELE_GETMSG_DISPLAYMESSAGES","//div[@id='workbenchForm:workbenchTabs:messagesInDialog']//span[@class='ui-messages-error-summary']","CONTAINS","Name entered does not match owner name on policy.","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_OK","//span[contains(text(),'OK')]","TGTYPESCREENREG");

        WAIT.$(1,"TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_DISPLAYMESSAGE_ROW3","//button[@id='workbenchForm:workbenchTabs:grid_blc400s2:2:msgbox_button']","TGTYPESCREENREG");

        WAIT.$(3,"TGTYPESCREENREG");

        ASSERT.$("ELE_GETMSG_DISPLAYMESSAGES","//div[@id='workbenchForm:workbenchTabs:messagesInDialog']//span[@class='ui-messages-error-summary']","CONTAINS","Name entered does not match owner name on policy.","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_OK","//span[contains(text(),'OK')]","TGTYPESCREENREG");

        WAIT.$(1,"TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_DISPLAYMESSAGE_ROW4","//button[@id='workbenchForm:workbenchTabs:grid_blc400s2:3:msgbox_button']","TGTYPESCREENREG");

        WAIT.$(3,"TGTYPESCREENREG");

        ASSERT.$("ELE_GETMSG_DISPLAYMESSAGES","//div[@id='workbenchForm:workbenchTabs:messagesInDialog']//span[@class='ui-messages-error-summary']","CONTAINS","Name entered does not match owner name on policy.","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_OK","//span[contains(text(),'OK')]","TGTYPESCREENREG");

        WAIT.$(1,"TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_DISPLAYMESSAGE_ROW5","//button[@id='workbenchForm:workbenchTabs:grid_blc400s2:4:msgbox_button']","TGTYPESCREENREG");

        WAIT.$(3,"TGTYPESCREENREG");

        ASSERT.$("ELE_GETMSG_DISPLAYMESSAGES","//div[@id='workbenchForm:workbenchTabs:messagesInDialog']//span[@class='ui-messages-error-summary']","CONTAINS","Name entered does not match owner name on policy.","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_OK","//span[contains(text(),'OK')]","TGTYPESCREENREG");

        WAIT.$(1,"TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_DISPLAYMESSAGE_ROW6","//button[@id='workbenchForm:workbenchTabs:grid_blc400s2:5:msgbox_button']","TGTYPESCREENREG");

        WAIT.$(3,"TGTYPESCREENREG");

        ASSERT.$("ELE_GETMSG_DISPLAYMESSAGES","//div[@id='workbenchForm:workbenchTabs:messagesInDialog']//span[@class='ui-messages-error-summary']","CONTAINS","Name entered does not match owner name on policy.","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_OK","//span[contains(text(),'OK')]","TGTYPESCREENREG");

        WAIT.$(1,"TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_DISPLAYMESSAGE_ROW7","//button[@id='workbenchForm:workbenchTabs:grid_blc400s2:6:msgbox_button']","TGTYPESCREENREG");

        WAIT.$(3,"TGTYPESCREENREG");

        ASSERT.$("ELE_GETMSG_DISPLAYMESSAGES","//div[@id='workbenchForm:workbenchTabs:messagesInDialog']//span[@class='ui-messages-error-summary']","CONTAINS","Name entered does not match owner name on policy.","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_OK","//span[contains(text(),'OK')]","TGTYPESCREENREG");

        WAIT.$(1,"TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_DISPLAYMESSAGE_ROW8","//button[@id='workbenchForm:workbenchTabs:grid_blc400s2:7:msgbox_button']","TGTYPESCREENREG");

        WAIT.$(3,"TGTYPESCREENREG");

        ASSERT.$("ELE_GETMSG_DISPLAYMESSAGES","//div[@id='workbenchForm:workbenchTabs:messagesInDialog']//span[@class='ui-messages-error-summary']","CONTAINS","Name entered does not match owner name on policy.","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_OK","//span[contains(text(),'OK')]","TGTYPESCREENREG");

        WAIT.$(1,"TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_DISPLAYMESSAGE_ROW9","//button[@id='workbenchForm:workbenchTabs:grid_blc400s2:8:msgbox_button']","TGTYPESCREENREG");

        WAIT.$(3,"TGTYPESCREENREG");

        ASSERT.$("ELE_GETMSG_DISPLAYMESSAGES","//div[@id='workbenchForm:workbenchTabs:messagesInDialog']//span[@class='ui-messages-error-summary']","CONTAINS","Name entered does not match owner name on policy.","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_OK","//span[contains(text(),'OK')]","TGTYPESCREENREG");

        CALL.$("ValidateBatchPaymentUpdatedWithoutErrors","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

        SWIPE.$("DOWN","TGTYPESCREENREG");

        SWIPE.$("DOWN","TGTYPESCREENREG");

        SWIPE.$("DOWN","TGTYPESCREENREG");

        TAP.$("ELE_TXTBOX_NAMECOLUMN_TAP_ROW1","//div[@class='ui-datatable ui-widget dynamicTable']//tr[1]//td[2]//div[1]//div[1]","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_NAME_TYPE_ROW1","//input[@name='workbenchForm:workbenchTabs:grid_blc400s2:0:in_ownerName_Col']",var_OwnerDiffClients,"FALSE","TGTYPESCREENREG");

        TAP.$("ELE_TXTBOX_NAMECOLUMN_TAP_ROW2","//tr[2]//td[2]//div[1]//div[1]","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_NAME_TYPE_ROW2","//input[@name='workbenchForm:workbenchTabs:grid_blc400s2:1:in_ownerName_Col']",var_OwnerUpperLower,"FALSE","TGTYPESCREENREG");

        SWIPE.$("UP","TGTYPESCREENREG");

        SWIPE.$("UP","TGTYPESCREENREG");

        TAP.$("ELE_TXTBOX_NAMECOLUMN_TAP_ROW3","//tr[3]//td[2]//div[1]//div[1]","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_NAME_TYPE_ROW3","//input[@name='workbenchForm:workbenchTabs:grid_blc400s2:2:in_ownerName_Col']",var_OwnerSpace,"FALSE","TGTYPESCREENREG");

        TAP.$("ELE_TXTBOX_NAMECOLUMN_TAP_ROW4","//tr[4]//td[2]//div[1]//div[1]","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_NAME_TYPE_ROW4","//input[@name='workbenchForm:workbenchTabs:grid_blc400s2:3:in_ownerName_Col']",var_OwnerHyphen,"FALSE","TGTYPESCREENREG");

        TAP.$("ELE_TXTBOX_NAMECOLUMN_TAP_ROW5","//div[@class='ui-datatable ui-widget dynamicTable']//tr[5]//td[2]//div[1]//div[1]","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_NAME_TYPE_ROW5","//input[@name='workbenchForm:workbenchTabs:grid_blc400s2:4:in_ownerName_Col']",var_OwnerOneOfTwo,"FALSE","TGTYPESCREENREG");

        TAP.$("ELE_TXTBOX_NAMECOLUMN_TAP_ROW6","//tr[6]//td[2]//div[1]//div[1]","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_NAME_TYPE_ROW6","//input[@name='workbenchForm:workbenchTabs:grid_blc400s2:5:in_ownerName_Col']",var_CompanyOwner,"FALSE","TGTYPESCREENREG");

        TAP.$("ELE_TXTBOX_NAMECOLUMN_TAP_ROW7","//tr[7]//td[2]//div[1]//div[1]","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_NAME_TYPE_ROW7","//input[@name='workbenchForm:workbenchTabs:grid_blc400s2:6:in_ownerName_Col']",var_CompanyOwnerHyphen,"FALSE","TGTYPESCREENREG");

        TAP.$("ELE_TXTBOX_NAMECOLUMN_TAP_ROW8","//tr[8]//td[2]//div[1]//div[1]","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_NAME_TYPE_ROW8","//input[@name='workbenchForm:workbenchTabs:grid_blc400s2:7:in_ownerName_Col']",var_OwnerOneCharacter,"FALSE","TGTYPESCREENREG");

        TAP.$("ELE_TXTBOX_NAMECOLUMN_TAP_ROW9","//tr[9]//td[2]//div[1]//div[1]","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_NAME_TYPE_ROW9","//input[@name='workbenchForm:workbenchTabs:grid_blc400s2:8:in_ownerName_Col']",var_OwnerTwoCharacter,"FALSE","TGTYPESCREENREG");

        TAP.$("ELE_TXTBOX_COLLECTIONNUMBER_TAP_ROW10","//tr[10]//td[1]//div[1]//div[1]","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_VALIDATE","//span[contains(text(),'Validate')]","TGTYPESCREENREG");

        WAIT.$(1,"TGTYPESCREENREG");

        ASSERT.$("ELE_BUTTON_DISPLAYMESSAGE_ROW1","//button[@id='workbenchForm:workbenchTabs:grid_blc400s2:0:msgbox_button']","INVISIBLE","TGTYPESCREENREG");

        ASSERT.$("ELE_BUTTON_DISPLAYMESSAGE_ROW2","//button[@id='workbenchForm:workbenchTabs:grid_blc400s2:1:msgbox_button']","INVISIBLE","TGTYPESCREENREG");

        ASSERT.$("ELE_BUTTON_DISPLAYMESSAGE_ROW3","//button[@id='workbenchForm:workbenchTabs:grid_blc400s2:2:msgbox_button']","INVISIBLE","TGTYPESCREENREG");

        ASSERT.$("ELE_BUTTON_DISPLAYMESSAGE_ROW4","//button[@id='workbenchForm:workbenchTabs:grid_blc400s2:3:msgbox_button']","INVISIBLE","TGTYPESCREENREG");

        ASSERT.$("ELE_BUTTON_DISPLAYMESSAGE_ROW5","//button[@id='workbenchForm:workbenchTabs:grid_blc400s2:4:msgbox_button']","INVISIBLE","TGTYPESCREENREG");

        ASSERT.$("ELE_BUTTON_DISPLAYMESSAGE_ROW6","//button[@id='workbenchForm:workbenchTabs:grid_blc400s2:5:msgbox_button']","INVISIBLE","TGTYPESCREENREG");

        ASSERT.$("ELE_BUTTON_DISPLAYMESSAGE_ROW7","//button[@id='workbenchForm:workbenchTabs:grid_blc400s2:6:msgbox_button']","INVISIBLE","TGTYPESCREENREG");

        ASSERT.$("ELE_BUTTON_DISPLAYMESSAGE_ROW8","//button[@id='workbenchForm:workbenchTabs:grid_blc400s2:7:msgbox_button']","INVISIBLE","TGTYPESCREENREG");

        ASSERT.$("ELE_BUTTON_DISPLAYMESSAGE_ROW9","//button[@id='workbenchForm:workbenchTabs:grid_blc400s2:8:msgbox_button']","INVISIBLE","TGTYPESCREENREG");

		var_Count = var_Count + ( 1 );
                 LOGGER.info("Executed Step = INCREASEVAR,var_Count,1,TGTYPESCREENREG");
    	}
          LOGGER.info("Executed Step = WEND,TGTYPESCREENREG");
    	resetAttemps();

    }


    @Test
    public void ftj41modf028() {

        // No need to declare / print method name here, handled by TestNg listener
        // declare("ftj41modf028");

		int var_Count = 0;
                 LOGGER.info("Executed Step = VAR,Integer,var_Count,0,TGTYPESCREENREG");
		com.jayway.jsonpath.ReadContext var_CSVData = getJsonData("http://10.1.104.229/json/20200914/wVbqnF.json");
                 LOGGER.info("Executed Step = VAR,Json,var_CSVData,http://10.1.104.229/json/20200914/wVbqnF.json,TGTYPESCREENREG");
        LOGGER.info("Executed Step = REPEAT IF");
    	while (check(var_Count,"<",1,"TGTYPESCREENREG")) {
          LOGGER.info("Executed Step = WHILE,var_Count,<,1,TGTYPESCREENREG");
		String var_Plan;
                 LOGGER.info("Executed Step = VAR,String,var_Plan,TGTYPESCREENREG");
		var_Plan = getJsonData(var_CSVData , "$.records["+var_Count+"].Plan");
                 LOGGER.info("Executed Step = STORE,var_Plan,var_CSVData,$.records[{var_Count}].Plan,TGTYPESCREENREG");
        CALL.$("FTJQA2Login","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

		try { 
		 WebElement html = driver.findElement(By.tagName("html"));
		    	
		    	System.out.println("html size" + html.getSize());
		    	System.out.println("html" + html.getTagName());

		    	for( int i =0 ;i<4;i++) {
		        html.sendKeys(Keys.chord(Keys.CONTROL, Keys.SUBTRACT));
		    	}
		 
		 ;} 
		 catch(Exception e) { PrintVal("Renderer Script Exception : "+e.toString()); } 
		 PrintVal("Executed Step = CALLRENDERERSCRIPT,FTJ41ZOOMMAGNIFICATIONCHANGE"); 
        TYPE.$("ELE_TXTBOX_USERNAME","//input[@id='guestForm:username']","x5080","FALSE","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_PASSWORD","//input[@id='guestForm:password']","Welcome123$","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_LOGIN","//button[@id='guestForm:loginButton']","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        CALL.$("MODF028PlanDescriptionUpdatesValidations","TGTYPESCREENREG");

        TAP.$("ELE_TXTBOX_SEARCHTASKS","//input[@id='workbenchForm:workbenchTabs:taskSearchContainer:taskSearchSearch']","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_SEARCHTASKS","//input[@id='workbenchForm:workbenchTabs:taskSearchContainer:taskSearchSearch']","Plan Setup","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_TASKSEARCH","//a[@id='workbenchForm:workbenchTabs:taskSearchContainer:tb_buttonSearch']//span[@class='fa fa-search fa-lg']","TGTYPESCREENREG");

        WAIT.$(1,"TGTYPESCREENREG");

        TAP.$("ELE_LINK_PLANSETUP","//span[contains(text(),'Plan Setup')]","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

        TAP.$("ELE_TXTBOX_PLAN_PLANDESCRIPTION","//input[@name='workbenchForm:workbenchTabs:grid:planCode_Col:filter']","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_PLAN_PLANDESCRIPTION","//input[@name='workbenchForm:workbenchTabs:grid:planCode_Col:filter']",var_Plan,"FALSE","TGTYPESCREENREG");

        WAIT.$(3,"TGTYPESCREENREG");

        TAP.$("ELE_LINK_PLAN_ROW1","//div[@class='ui-outputpanel ui-widget']//td[1]","TGTYPESCREENREG");

        WAIT.$(3,"TGTYPESCREENREG");

        ASSERT.$("ELE_GETVAL_OWNERIDNUMBERREQUIRED","//label[contains(text(),'Owner Identification Number Required')]","VISIBLE","TGTYPESCREENFULL");

        TAP.$("ELE_BUTTON_EDIT","//span[text()='Edit']","TGTYPESCREENREG");

        WAIT.$(8,"TGTYPESCREENREG");

        TAP.$("ELE_DRPDWN_OWNERIDENTIFICATIONNUMBERREQUIRED","//tr[25]//td[2]//div[1]//div[3]//span[1]","TGTYPESCREENREG");

        TAP.$("ELE_DRPDWN_OWNERIDNUMBERREQUIRED_BLANK","*[id='workbenchForm:workbenchTabs:ownerIdReq_0']TGWEBCOMMACssSelectorTGWEBCOMMA0","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_VALIDATE","//span[contains(text(),'Validate')]","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

        ASSERT.$("ELE_GETMSG_ERROR","//div[@id='workbenchForm:workbenchTabs:messages']//span[@class='ui-messages-error-summary']","CONTAINS","Owner identification number required cannot be blank.","TGTYPESCREENFULL");

        TAP.$("ELE_DRPDWN_OWNERIDENTIFICATIONNUMBERREQUIRED","//tr[25]//td[2]//div[1]//div[3]//span[1]","TGTYPESCREENREG");

        TAP.$("ELE_DRPDWN_OWNERIDNUMBERREQUIRED_NO","*[id='workbenchForm:workbenchTabs:ownerIdReq_2']TGWEBCOMMACssSelectorTGWEBCOMMA0","TGTYPESCREENREG");

        SWIPE.$("DOWN","TGTYPESCREENREG");

        SWIPE.$("DOWN","TGTYPESCREENREG");

        SWIPE.$("DOWN","TGTYPESCREENREG");

        SWIPE.$("DOWN","TGTYPESCREENREG");

        SWIPE.$("DOWN","TGTYPESCREENREG");

        SWIPE.$("DOWN","TGTYPESCREENREG");

        SWIPE.$("DOWN","TGTYPESCREENREG");

        SWIPE.$("DOWN","TGTYPESCREENREG");

        SWIPE.$("DOWN","TGTYPESCREENREG");

        SWIPE.$("DOWN","TGTYPESCREENREG");

        TAP.$("ELE_DRPDWN_TAXQUALIFIEDPLAN","//td[3]//table[1]//tbody[1]//tr[1]//td[1]//fieldset[1]//div[1]//table[1]//tbody[1]//tr[3]//td[2]//div[1]//div[3]//span[1]","TGTYPESCREENREG");

        TAP.$("ELE_DRPDWN_TAXQUALIFIEDPLAN_YES","*[id='workbenchForm:workbenchTabs:taxQualifiedPlan_1']TGWEBCOMMACssSelectorTGWEBCOMMA0","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_VALIDATE","//span[contains(text(),'Validate')]","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

        ASSERT.$("ELE_GETMSG_ERROR","//div[@id='workbenchForm:workbenchTabs:messages']//span[@class='ui-messages-error-summary']","CONTAINS","Owner identification number required must be Yes for a tax-qualified plan.","TGTYPESCREENFULL");

        TAP.$("ELE_DRPDWN_OWNERIDENTIFICATIONNUMBERREQUIRED","//tr[25]//td[2]//div[1]//div[3]//span[1]","TGTYPESCREENREG");

        TAP.$("ELE_DRPDWN_OWNERIDNUMBERREQUIRED_YES","*[id='workbenchForm:workbenchTabs:ownerIdReq_1']TGWEBCOMMACssSelectorTGWEBCOMMA0","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_VALIDATE","//span[contains(text(),'Validate')]","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

        ASSERT.$("ELE_MSG_VALIDATIONSUCCESS","//div[@id='workbenchForm:workbenchTabs:messages']//span[@class='ui-messages-info-summary'][contains(text(),'Validation completed successfully with no errors.')]","VISIBLE","TGTYPESCREENFULL");

        TAP.$("ELE_LOGO_GIAS","//span[text()='GIAS']",1,0,"TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

        CALL.$("F028UpdateClientErrorValidation","TGTYPESCREENREG");

        TAP.$("ELE_TXTBOX_SEARCHTASKS","//input[@id='workbenchForm:workbenchTabs:taskSearchContainer:taskSearchSearch']","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_SEARCHTASKS","//input[@id='workbenchForm:workbenchTabs:taskSearchContainer:taskSearchSearch']","Client Names and","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_TASKSEARCH","//a[@id='workbenchForm:workbenchTabs:taskSearchContainer:tb_buttonSearch']//span[@class='fa fa-search fa-lg']","TGTYPESCREENREG");

        WAIT.$(1,"TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_CLIENTNAMESANDADDRESSES","//span[contains(text(),'Client Names and Addresses')]","TGTYPESCREENREG");

        WAIT.$(3,"TGTYPESCREENREG");

        TAP.$("ELE_TXTBOX_NAME_CLIENTNAMEANDADDRESSES","//input[@name='workbenchForm:workbenchTabs:grid1:individualName_Col:filter']","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_NAME_CLIENTNAMEANDADDRESSES","//input[@name='workbenchForm:workbenchTabs:grid1:individualName_Col:filter']","Snow","FALSE","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        TAP.$("ELE_LINK_NAME_ROW1_CLIENTNAMESANDADDRESSES","//tbody[@class='ui-datatable-data ui-widget-content']//tr[1]","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_EDIT","//span[text()='Edit']","TGTYPESCREENREG");

        WAIT.$(1,"TGTYPESCREENREG");

		try { 
		 for (int i = 0; i < 11; i++) {
		    driver.findElement(By.xpath("//input[@id='workbenchForm:workbenchTabs:identificationNumber']")).sendKeys(Keys.BACK_SPACE);
		}
		 
		 ;} 
		 catch(Exception e) { PrintVal("Renderer Script Exception : "+e.toString()); } 
		 PrintVal("Executed Step = CALLRENDERERSCRIPT,CLEARIDNUMBERTEXTFIELD"); 
        WAIT.$(1,"TGTYPESCREENFULL");

        TAP.$("ELE_BUTTON_VALIDATE","//span[contains(text(),'Validate')]","TGTYPESCREENREG");

        ASSERT.$("ELE_GETMSG_ERROR_OWNERIDNUMBERISREQUIRED","//div[@id='workbenchForm:workbenchTabs:messages']//span[@class='ui-messages-error-summary'][contains(text(),'Owner identification number is required.')]","VISIBLE","TGTYPESCREENFULL");

        TAP.$("ELE_LOGO_GIAS","//span[text()='GIAS']",1,0,"TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

        CALL.$("F028UpdateClientValidation","TGTYPESCREENREG");

        TAP.$("ELE_TXTBOX_SEARCHTASKS","//input[@id='workbenchForm:workbenchTabs:taskSearchContainer:taskSearchSearch']","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_SEARCHTASKS","//input[@id='workbenchForm:workbenchTabs:taskSearchContainer:taskSearchSearch']","Client Names and","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_TASKSEARCH","//a[@id='workbenchForm:workbenchTabs:taskSearchContainer:tb_buttonSearch']//span[@class='fa fa-search fa-lg']","TGTYPESCREENREG");

        WAIT.$(1,"TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_CLIENTNAMESANDADDRESSES","//span[contains(text(),'Client Names and Addresses')]","TGTYPESCREENREG");

        WAIT.$(3,"TGTYPESCREENREG");

        TAP.$("ELE_TXTBOX_NAME_CLIENTNAMEANDADDRESSES","//input[@name='workbenchForm:workbenchTabs:grid1:individualName_Col:filter']","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_NAME_CLIENTNAMEANDADDRESSES","//input[@name='workbenchForm:workbenchTabs:grid1:individualName_Col:filter']","F028","FALSE","TGTYPESCREENREG");

        WAIT.$(4,"TGTYPESCREENREG");

        TAP.$("ELE_LINK_NAME_ROW1_CLIENTNAMESANDADDRESSES","//tbody[@class='ui-datatable-data ui-widget-content']//tr[1]","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

        LOGGER.info("Executed Step = START IF");
        if (check("ELE_GETVAL_IDTYPE_UPDATECLIENTPAGE","//span[@id='workbenchForm:workbenchTabs:taxIdenUsag']","CONTAINS","Not Required","TGTYPESCREENREG")) {
         LOGGER.info("Executed Step = IF,ELE_GETVAL_IDTYPE_UPDATECLIENTPAGE,CONTAINS,Not Required,TGTYPESCREENREG");
        TAP.$("ELE_BUTTON_EDIT","//span[text()='Edit']","TGTYPESCREENREG");

        TAP.$("ELE_DRPDWN_IDTYPE_UPDATECLIENT","//div[@id='workbenchForm:workbenchTabs:taxIdenUsag']//div//span","TGTYPESCREENREG");

        TAP.$("ELE_DRPDWN_IDTYPE_SOCIALSECURITYNUMBER","//li[contains(text(),'Social Security Number')]","TGTYPESCREENREG");

        TAP.$("ELE_TXTBOX_IDNUMBER","//input[@id='workbenchForm:workbenchTabs:identificationNumber']","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_IDNUMBER","//input[@id='workbenchForm:workbenchTabs:identificationNumber']","123451232","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SAVE","//span[contains(text(),'Save')]","TGTYPESCREENREG");

        WAIT.$(3,"TGTYPESCREENREG");

        }
          LOGGER.info("Executed Step = ENDIF,TGTYPESCREENREG");
        TAP.$("ELE_BUTTON_EDIT","//span[text()='Edit']","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

        TAP.$("ELE_DRPDWN_IDTYPE_UPDATECLIENT","//div[@id='workbenchForm:workbenchTabs:taxIdenUsag']//div//span","TGTYPESCREENREG");

        TAP.$("ELE_DRPDWN_IDTYPE_NOTREQUIRED","//li[contains(text(),'Not Required')]","TGTYPESCREENREG");

		try { 
		 for (int i = 0; i < 11; i++) {
		    driver.findElement(By.xpath("//input[@id='workbenchForm:workbenchTabs:identificationNumber']")).sendKeys(Keys.BACK_SPACE);
		}
		 
		 ;} 
		 catch(Exception e) { PrintVal("Renderer Script Exception : "+e.toString()); } 
		 PrintVal("Executed Step = CALLRENDERERSCRIPT,CLEARIDNUMBERTEXTFIELD"); 
        WAIT.$(1,"TGTYPESCREENFULL");

        TAP.$("ELE_BUTTON_SAVE","//span[contains(text(),'Save')]","TGTYPESCREENREG");

        WAIT.$(3,"TGTYPESCREENREG");

        ASSERT.$("ELE_MSG_ITEMSUCCESSUPDATE","//div[@id='workbenchForm:workbenchTabs:messages']//span[@class='ui-messages-info-summary'][contains(text(),'Item successfully updated.')]","VISIBLE","TGTYPESCREENFULL");

        TAP.$("ELE_LOGO_GIAS","//span[text()='GIAS']","TGTYPESCREENREG");

        CALL.$("MODF028BenefitIsNotUpdateValidations","TGTYPESCREENREG");

        TAP.$("ELE_TXTBOX_SEARCHTASKS","//input[@id='workbenchForm:workbenchTabs:taskSearchContainer:taskSearchSearch']","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_SEARCHTASKS","//input[@id='workbenchForm:workbenchTabs:taskSearchContainer:taskSearchSearch']","pos700","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_TASKSEARCH","//a[@id='workbenchForm:workbenchTabs:taskSearchContainer:tb_buttonSearch']//span[@class='fa fa-search fa-lg']","TGTYPESCREENREG");

        WAIT.$(1,"TGTYPESCREENREG");

        TAP.$("ELE_LABEL_CHANGEPOLICYBENEFIT","//body/div[@id='Outer40']/div[@id='ContentPanel']/div[@id='Content40']/form[@id='workbenchForm']/div[@id='PageTiles']/div[@id='workbenchForm:workbenchTabs']/div[1]/div[1]/div[1]/div[2]/div[2]/div[1]/div[1]/div[1]/div[3]/div[1]/div[1]/button[1]/span[2]","TGTYPESCREENREG");

        TAP.$("ELE_TXTBOX_POLICYNUMBER","//input[@id='workbenchForm:workbenchTabs:policyNumber']","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_POLICYNUMBER","//input[@id='workbenchForm:workbenchTabs:policyNumber']","F02805","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_CONTINUE","//span[contains(text(),'Continue')]","TGTYPESCREENREG");

        WAIT.$("ELE_LINK_BASEPLAN_ROW1","//tr[@class='ui-widget-content ui-datatable-even ui-datatable-selectable']","VISIBLE","TGTYPESCREENREG");

        TAP.$("ELE_LINK_BASEPLAN_ROW1","//tr[@class='ui-widget-content ui-datatable-even ui-datatable-selectable']","TGTYPESCREENREG");

        WAIT.$(6,"TGTYPESCREENREG");

        TAP.$("ELE_DRPDWN_PLAN_UPDATEBENEFITPAGE","//div[@id='workbenchForm:workbenchTabs:planCode']//div//span","TGTYPESCREENREG");

        TAP.$("ELE_DRPDWN_PLAN_OLCPYC","//li[contains(text(),'OLCPYC - F028 TESTING')]","TGTYPESCREENREG");

        WAIT.$(13,"TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_VALIDATE","//span[contains(text(),'Validate')]","TGTYPESCREENREG");

        WAIT.$(3,"TGTYPESCREENREG");

        WAIT.$("ELE_GETMSG_OWNERIDNUMBERREQUIRED_ERROR","//span[@class='ui-message-error-detail']","VISIBLE","TGTYPESCREENREG");

        ASSERT.$("ELE_GETMSG_OWNERIDNUMBERREQUIRED_ERROR","//span[@class='ui-message-error-detail']","VISIBLE","Owner identification number is required.","TGTYPESCREENFULL");

        TAP.$("ELE_TAB_CLOSE3","//li[@class='ui-state-default ui-tabs-selected ui-state-active ui-corner-top']//span[@class='ui-icon ui-icon-close']","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

        TAP.$("ELE_LINK_ROW2","//tr[@class='ui-widget-content ui-datatable-odd ui-datatable-selectable']","TGTYPESCREENREG");

        WAIT.$(6,"TGTYPESCREENREG");

        TAP.$("ELE_DRPDWN_PLAN_UPDATEBENEFITPAGE","//div[@id='workbenchForm:workbenchTabs:planCode']//div//span","TGTYPESCREENREG");

        TAP.$("ELE_DRPDWN_PLAN_TRCYPD","//li[contains(text(),'TRCPYD - F028 TESTING')]","TGTYPESCREENREG");

        WAIT.$(13,"TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_VALIDATE","//span[contains(text(),'Validate')]","TGTYPESCREENREG");

        WAIT.$(4,"TGTYPESCREENREG");

        ASSERT.$("ELE_GETMSG_OWNERIDNUMBERREQUIRED_ERROR","//span[@class='ui-message-error-detail']","VISIBLE","TGTYPESCREENFULL");

        TAP.$("ELE_LOGO_GIAS","//span[text()='GIAS']","TGTYPESCREENREG");

        WAIT.$(1,"TGTYPESCREENREG");

        CALL.$("MODF028UpdateBenefitTransactionValidations","TGTYPESCREENREG");

        TAP.$("ELE_TXTBOX_SEARCHTASKS","//input[@id='workbenchForm:workbenchTabs:taskSearchContainer:taskSearchSearch']","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_SEARCHTASKS","//input[@id='workbenchForm:workbenchTabs:taskSearchContainer:taskSearchSearch']","Change a Policy Benefit","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_TASKSEARCH","//a[@id='workbenchForm:workbenchTabs:taskSearchContainer:tb_buttonSearch']//span[@class='fa fa-search fa-lg']","TGTYPESCREENREG");

        WAIT.$(1,"TGTYPESCREENREG");

        TAP.$("ELE_LABEL_CHANGEPOLICYBENEFIT","//body/div[@id='Outer40']/div[@id='ContentPanel']/div[@id='Content40']/form[@id='workbenchForm']/div[@id='PageTiles']/div[@id='workbenchForm:workbenchTabs']/div[1]/div[1]/div[1]/div[2]/div[2]/div[1]/div[1]/div[1]/div[3]/div[1]/div[1]/button[1]/span[2]","TGTYPESCREENREG");

        WAIT.$(1,"TGTYPESCREENREG");

        TAP.$("ELE_TXTBOX_POLICYNUMBER","//input[@id='workbenchForm:workbenchTabs:policyNumber']","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_POLICYNUMBER","//input[@id='workbenchForm:workbenchTabs:policyNumber']","F02810","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_CONTINUE","//span[contains(text(),'Continue')]","TGTYPESCREENREG");

        WAIT.$(1,"TGTYPESCREENREG");

        LOGGER.info("Executed Step = START IF");
        if (check("ELE_GETVAL_BASEPLAN_DTCPYG","//span[contains(text(),'DTCPYG - F028 TESTING')]","VISIBLE","TGTYPESCREENREG")) {
         LOGGER.info("Executed Step = IF,ELE_GETVAL_BASEPLAN_DTCPYG,VISIBLE,TGTYPESCREENREG");
        CALL.$("UpdateBaseBenefitPlanToOLCPYC","TGTYPESCREENREG");

        TAP.$("ELE_LINK_BASEPLAN_ROW1","//tr[@class='ui-widget-content ui-datatable-even ui-datatable-selectable']","TGTYPESCREENREG");

        WAIT.$(6,"TGTYPESCREENREG");

        TAP.$("ELE_DRPDWN_PLAN_UPDATEBENEFITPAGE","//div[@id='workbenchForm:workbenchTabs:planCode']//div//span","TGTYPESCREENREG");

        WAIT.$(1,"TGTYPESCREENREG");

        TAP.$("ELE_DRPDWN_PLAN_OLCPYC","//li[contains(text(),'OLCPYC - F028 TESTING')]","TGTYPESCREENREG");

        WAIT.$(15,"TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SAVE","//span[contains(text(),'Save')]","TGTYPESCREENREG");

        WAIT.$(10,"TGTYPESCREENREG");

        LOGGER.info("Executed Step = START IF");
        if (check("ELE_ERRORMSG_RISKCLASSRATESINEFFECTFIELDMUSTBEBLANK","//body/div[@id='Outer40']/div[@id='ContentPanel']/div[@id='Content40']/form[@id='workbenchForm']/div[@id='PageTiles']/div[@id='workbenchForm:workbenchTabs']/div[1]/div[4]/div[1]/div[4]/div[1]/ul[1]/li[1]/span[1]","VISIBLE","TGTYPESCREENREG")) {
         LOGGER.info("Executed Step = IF,ELE_ERRORMSG_RISKCLASSRATESINEFFECTFIELDMUSTBEBLANK,VISIBLE,TGTYPESCREENREG");
        TAP.$("ELE_BUTTON_SAVE","//span[contains(text(),'Save')]","TGTYPESCREENREG");

        WAIT.$(10,"TGTYPESCREENREG");

		try { 
		 driver.switchTo().frame(0);
		driver.findElement(By.xpath("//form[@id='confirmDialogForm']")).click();
		Thread.sleep(1000);
		driver.findElement(By.xpath("//button[@id='j_idt9']")).click();
		// Add Screenshot will only work for Android platform

		takeScreenshot();

		 
		 ;} 
		 catch(Exception e) { PrintVal("Renderer Script Exception : "+e.toString()); } 
		 PrintVal("Executed Step = CALLRENDERERSCRIPT,CLICKYESONCONFIRMACTION"); 
        }
          LOGGER.info("Executed Step = ENDIF,TGTYPESCREENREG");
        WAIT.$(8,"TGTYPESCREENREG");

        ASSERT.$("ELE_MSG_REQUESTCOMPLETESUCCESSFULLY","//span[text()='Request completed successfully.']","VISIBLE","TGTYPESCREENFULL");

        TAP.$("ELE_TAB_CLOSE3","//li[@class='ui-state-default ui-tabs-selected ui-state-active ui-corner-top']//span[@class='ui-icon ui-icon-close']","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

        }
          LOGGER.info("Executed Step = ENDIF,TGTYPESCREENREG");
        LOGGER.info("Executed Step = START IF");
        if (check("ELE_GETVAL_BASEPLAN_OLCPYC","//span[contains(text(),'OLCPYC - F028 TESTING')]","VISIBLE","TGTYPESCREENREG")) {
         LOGGER.info("Executed Step = IF,ELE_GETVAL_BASEPLAN_OLCPYC,VISIBLE,TGTYPESCREENREG");
        CALL.$("UpdateBaseBenefitPlanToDTCPYG","TGTYPESCREENREG");

        TAP.$("ELE_LINK_BASEPLAN_ROW1","//tr[@class='ui-widget-content ui-datatable-even ui-datatable-selectable']","TGTYPESCREENREG");

        WAIT.$(6,"TGTYPESCREENREG");

        TAP.$("ELE_DRPDWN_PLAN_UPDATEBENEFITPAGE","//div[@id='workbenchForm:workbenchTabs:planCode']//div//span","TGTYPESCREENREG");

        WAIT.$(1,"TGTYPESCREENREG");

        TAP.$("ELE_DRPDWN_PLAN_DTCPYG","//li[contains(text(),'DTCPYG - F028 TESTING')]","TGTYPESCREENREG");

        WAIT.$(15,"TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SAVE","//span[contains(text(),'Save')]","TGTYPESCREENREG");

        WAIT.$(6,"TGTYPESCREENREG");

        ASSERT.$("ELE_MSG_REQUESTCOMPLETESUCCESSFULLY","//span[text()='Request completed successfully.']","VISIBLE","TGTYPESCREENFULL");

        TAP.$("ELE_TAB_CLOSE3","//li[@class='ui-state-default ui-tabs-selected ui-state-active ui-corner-top']//span[@class='ui-icon ui-icon-close']","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

        }
          LOGGER.info("Executed Step = ENDIF,TGTYPESCREENREG");
        LOGGER.info("Executed Step = START IF");
        if (check("ELE_GETVAL_RIDERPLAN_OLCPYA","//span[contains(text(),'OLCPYA - F028 TESTING')]","VISIBLE","TGTYPESCREENREG")) {
         LOGGER.info("Executed Step = IF,ELE_GETVAL_RIDERPLAN_OLCPYA,VISIBLE,TGTYPESCREENREG");
        CALL.$("UpdateRiderPlanToTRCPYD","TGTYPESCREENREG");

        TAP.$("ELE_LINK_ROW2","//tr[@class='ui-widget-content ui-datatable-odd ui-datatable-selectable']","TGTYPESCREENREG");

        WAIT.$(6,"TGTYPESCREENREG");

        TAP.$("ELE_DRPDWN_PLAN_UPDATEBENEFITPAGE","//div[@id='workbenchForm:workbenchTabs:planCode']//div//span","TGTYPESCREENREG");

        WAIT.$(1,"TGTYPESCREENREG");

        TAP.$("ELE_DRPDWN_PLAN_TRCPYD","//li[contains(text(),'TRCPYD - F028 TESTING')]","TGTYPESCREENREG");

        WAIT.$(10,"TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SAVE","//span[contains(text(),'Save')]","TGTYPESCREENREG");

        WAIT.$(4,"TGTYPESCREENREG");

        ASSERT.$("ELE_MSG_REQUESTCOMPLETESUCCESSFULLY","//span[text()='Request completed successfully.']","VISIBLE","TGTYPESCREENFULL");

        TAP.$("ELE_TAB_CLOSE3","//li[@class='ui-state-default ui-tabs-selected ui-state-active ui-corner-top']//span[@class='ui-icon ui-icon-close']","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

        }
          LOGGER.info("Executed Step = ENDIF,TGTYPESCREENREG");
        LOGGER.info("Executed Step = START IF");
        if (check("ELE_GETVAL_RIDERPLAN_TRCPYD","//span[contains(text(),'TRCPYD - F028 TESTING')]","VISIBLE","TGTYPESCREENREG")) {
         LOGGER.info("Executed Step = IF,ELE_GETVAL_RIDERPLAN_TRCPYD,VISIBLE,TGTYPESCREENREG");
        CALL.$("UpdateRiderPlanToOLCPYA","TGTYPESCREENREG");

        TAP.$("ELE_LINK_ROW2","//tr[@class='ui-widget-content ui-datatable-odd ui-datatable-selectable']","TGTYPESCREENREG");

        WAIT.$(6,"TGTYPESCREENREG");

        TAP.$("ELE_DRPDWN_PLAN_UPDATEBENEFITPAGE","//div[@id='workbenchForm:workbenchTabs:planCode']//div//span","TGTYPESCREENREG");

        WAIT.$(1,"TGTYPESCREENREG");

        TAP.$("ELE_DRPDWN_PLAN_OLCPYA","//li[contains(text(),'OLCPYA - F028 TESTING')]","TGTYPESCREENREG");

        WAIT.$(15,"TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SAVE","//span[contains(text(),'Save')]","TGTYPESCREENREG");

        WAIT.$(7,"TGTYPESCREENREG");

        ASSERT.$("ELE_MSG_REQUESTCOMPLETESUCCESSFULLY","//span[text()='Request completed successfully.']","VISIBLE","TGTYPESCREENFULL");

        TAP.$("ELE_TAB3","a[href='#workbenchForm:workbenchTabs:tab_2']TGWEBCOMMACssSelectorTGWEBCOMMA0","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

        }
          LOGGER.info("Executed Step = ENDIF,TGTYPESCREENREG");
        TAP.$("ELE_LOGO_GIAS","//span[text()='GIAS']",1,0,"TGTYPESCREENREG");

        CALL.$("F028AddRiderBenefitErrorValidation","TGTYPESCREENREG");

        TAP.$("ELE_TXTBOX_SEARCHTASKS","//input[@id='workbenchForm:workbenchTabs:taskSearchContainer:taskSearchSearch']","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_SEARCHTASKS","//input[@id='workbenchForm:workbenchTabs:taskSearchContainer:taskSearchSearch']","POS500","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_TASKSEARCH","//a[@id='workbenchForm:workbenchTabs:taskSearchContainer:tb_buttonSearch']//span[@class='fa fa-search fa-lg']","TGTYPESCREENREG");

        WAIT.$(1,"TGTYPESCREENREG");

        TAP.$("ELE_LINK_ADD_POLICY_BENEFIT","//span[contains(text(),'Add a Policy Benefit')]","TGTYPESCREENREG");

        WAIT.$(1,"TGTYPESCREENREG");

        TAP.$("ELE_TXTBOX_POLICYNUMBER","//input[@id='workbenchForm:workbenchTabs:policyNumber']","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_POLICYNUMBER","//input[@id='workbenchForm:workbenchTabs:policyNumber']","F02812","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_CONTINUE","//span[contains(text(),'Continue')]","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_RADIO_BENEFIT","//tr[@class='ui-widget-content ui-panelgrid-even PanelRow']//tr[1]//td[1]//label[1]","TGTYPESCREENREG");

        WAIT.$(3,"TGTYPESCREENREG");

        TAP.$("ELE_DRPDWN_PLAN_ADDBENEFIT","//div[@id='workbenchForm:workbenchTabs:testPlanCode']//div//span","TGTYPESCREENREG");

        WAIT.$(1,"TGTYPESCREENREG");

        TAP.$("ELE_DRPDWN_PLAN_TRCYPD_F028","//li[@id='workbenchForm:workbenchTabs:testPlanCode_1092']","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_CONTINUE","//span[contains(text(),'Continue')]","TGTYPESCREENFULL");

        WAIT.$(5,"TGTYPESCREENREG");

        WAIT.$("ELE_GETMSG_ERROR_OWNERIDNUMBERISREQUIRED","//div[@id='workbenchForm:workbenchTabs:messages']//span[@class='ui-messages-error-summary'][contains(text(),'Owner identification number is required.')]","VISIBLE","TGTYPESCREENREG");

        TAP.$("ELE_TAB_CLOSE3","//li[@class='ui-state-default ui-tabs-selected ui-state-active ui-corner-top']//span[@class='ui-icon ui-icon-close']","TGTYPESCREENREG");

		var_Count = var_Count + ( 1 );
                 LOGGER.info("Executed Step = INCREASEVAR,var_Count,1,TGTYPESCREENREG");
    	}
          LOGGER.info("Executed Step = WEND,TGTYPESCREENREG");
    	resetAttemps();

    }


    @Test
    public void ftj41modf119() {

        // No need to declare / print method name here, handled by TestNg listener
        // declare("ftj41modf119");

		int var_Count = 0;
                 LOGGER.info("Executed Step = VAR,Integer,var_Count,0,TGTYPESCREENREG");
        LOGGER.info("Executed Step = REPEAT IF");
    	while (check(var_Count,"<",1,"TGTYPESCREENREG")) {
          LOGGER.info("Executed Step = WHILE,var_Count,<,1,TGTYPESCREENREG");
        CALL.$("FTJQA2Login","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

		try { 
		 WebElement html = driver.findElement(By.tagName("html"));
		    	
		    	System.out.println("html size" + html.getSize());
		    	System.out.println("html" + html.getTagName());

		    	for( int i =0 ;i<4;i++) {
		        html.sendKeys(Keys.chord(Keys.CONTROL, Keys.SUBTRACT));
		    	}
		 
		 ;} 
		 catch(Exception e) { PrintVal("Renderer Script Exception : "+e.toString()); } 
		 PrintVal("Executed Step = CALLRENDERERSCRIPT,FTJ41ZOOMMAGNIFICATIONCHANGE"); 
        TYPE.$("ELE_TXTBOX_USERNAME","//input[@id='guestForm:username']","x5080","FALSE","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_PASSWORD","//input[@id='guestForm:password']","Welcome123$","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_LOGIN","//button[@id='guestForm:loginButton']","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        CALL.$("ModF119","TGTYPESCREENREG");

        TAP.$("ELE_TXTBOX_SEARCHTASKS","//input[@id='workbenchForm:workbenchTabs:taskSearchContainer:taskSearchSearch']","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_SEARCHTASKS","//input[@id='workbenchForm:workbenchTabs:taskSearchContainer:taskSearchSearch']","Batch Policy Payment","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_TASKSEARCH","//a[@id='workbenchForm:workbenchTabs:taskSearchContainer:tb_buttonSearch']//span[@class='fa fa-search fa-lg']","TGTYPESCREENREG");

        WAIT.$(1,"TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_BATCHPOLICYPAYMENT","//span[contains(text(),'Batch Policy Payment')]","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

        ASSERT.$("ELE_GETVAL_BATCHDESCRIPTION","//span[contains(text(),'Description')]","VISIBLE","TGTYPESCREENREG");

        ASSERT.$("ELE_GETVAL_BATCHCURRENCY","//span[contains(text(),'Currency')]","VISIBLE","TGTYPESCREENREG");

        ASSERT.$("ELE_GETVAL_SASVEDPAYMENTCOLUMN","//span[contains(text(),'Saved Payment')]","VISIBLE","TGTYPESCREENFULL");

        CALL.$("ChangeCurrencyDrpDwnToDollarUS","TGTYPESCREENREG");

        TAP.$("ELE_DRPDWN_CURRENCY","//div[@class='ui-selectonemenu-trigger ui-state-default ui-corner-right']","TGTYPESCREENREG");

        TAP.$("ELE_DRPDWN_DOLLARUS","//li[@class='ui-selectonemenu-item ui-selectonemenu-list-item ui-corner-all'][contains(text(),'Dollar [US]')]","TGTYPESCREENREG");

        WAIT.$(4,"TGTYPESCREENREG");

        TAP.$("ELE_TXTBOX_BATCHNUMBER","//input[@name='workbenchForm:workbenchTabs:grid:suspenseControlNumb_Col:filter']","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_BATCHNUMBER","//input[@name='workbenchForm:workbenchTabs:grid:suspenseControlNumb_Col:filter']","0160 BATCH","FALSE","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        TAP.$("ELE_LINK_BATCHNUMBER_ROW1","//tbody[@id='workbenchForm:workbenchTabs:grid_data']//tr[1]","TGTYPESCREENREG");

        WAIT.$(1,"TGTYPESCREENREG");

        ASSERT.$("ELE_TXTBOX_COLLECTIONNUMBER_TAP_ROW1","//div[@class='ui-datatable ui-widget dynamicTable']//tr[1]//td[1]//div[1]//div[1]","VISIBLE","TGTYPESCREENREG");

        ASSERT.$("ELE_TXTBOX_NAMECOLUMN_TAP_ROW1","//div[@class='ui-datatable ui-widget dynamicTable']//tr[1]//td[2]//div[1]//div[1]","VISIBLE","TGTYPESCREENREG");

        ASSERT.$("ELE_TXTBOX_AMOUNTRECEIVED_TAP_ROW1","//tr[1]//td[3]//div[1]//div[1]","VISIBLE","TGTYPESCREENREG");

        ASSERT.$("ELE_BUTTON_SAVE","//span[contains(text(),'Save')]","VISIBLE","TGTYPESCREENFULL");

        TAP.$("ELE_BUTTON_SAVE","//span[contains(text(),'Save')]","TGTYPESCREENREG");

        ASSERT.$("ELE_CONFIRMACTION_YES","//div[@class='ui-dialog-content ui-widget-content ui-df-content']//iframe","INVISIBLE","TGTYPESCREENREG");

        WAIT.$(1,"TGTYPESCREENREG");

        TAP.$("ELE_LINK_BATCHNUMBER_ROW1","//tbody[@id='workbenchForm:workbenchTabs:grid_data']//tr[1]","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

        ASSERT.$("ELE_BUTTON_DELETE","//span[contains(text(),'Delete')]","VISIBLE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_DELETE","//span[contains(text(),'Delete')]","TGTYPESCREENFULL");

        ASSERT.$("ELE_BUTTON_CONFIRMDELETE","//span[contains(text(),'Confirm Delete')]","VISIBLE","TGTYPESCREENREG");

        TAP.$("ELE_LOGO_GIAS","//span[text()='GIAS']","TGTYPESCREENREG");

		var_Count = var_Count + ( 1 );
                 LOGGER.info("Executed Step = INCREASEVAR,var_Count,1,TGTYPESCREENREG");
    	}
          LOGGER.info("Executed Step = WEND,TGTYPESCREENREG");
    	resetAttemps();

    }


    @Test
    public void ftj41f121listbillvalidations() {

        // No need to declare / print method name here, handled by TestNg listener
        // declare("ftj41f121listbillvalidations");

		int var_Count = 0;
                 LOGGER.info("Executed Step = VAR,Integer,var_Count,0,TGTYPESCREENREG");
		com.jayway.jsonpath.ReadContext var_CSVData = getJsonData("http://10.1.104.229/json/20200814/GrquPL.json");
                 LOGGER.info("Executed Step = VAR,Json,var_CSVData,http://10.1.104.229/json/20200814/GrquPL.json,TGTYPESCREENREG");
        LOGGER.info("Executed Step = REPEAT IF");
    	while (check(var_Count,"<",1,"TGTYPESCREENREG")) {
          LOGGER.info("Executed Step = WHILE,var_Count,<,1,TGTYPESCREENREG");
		String var_CollectionNumber1;
                 LOGGER.info("Executed Step = VAR,String,var_CollectionNumber1,TGTYPESCREENREG");
		var_CollectionNumber1 = getJsonData(var_CSVData , "$.records["+var_Count+"].CollectionNumber1");
                 LOGGER.info("Executed Step = STORE,var_CollectionNumber1,var_CSVData,$.records[{var_Count}].CollectionNumber1,TGTYPESCREENREG");
		String var_WrongGroup;
                 LOGGER.info("Executed Step = VAR,String,var_WrongGroup,TGTYPESCREENREG");
		var_WrongGroup = getJsonData(var_CSVData , "$.records["+var_Count+"].WrongGroup");
                 LOGGER.info("Executed Step = STORE,var_WrongGroup,var_CSVData,$.records[{var_Count}].WrongGroup,TGTYPESCREENREG");
		String var_AmntRec1;
                 LOGGER.info("Executed Step = VAR,String,var_AmntRec1,TGTYPESCREENREG");
		var_AmntRec1 = getJsonData(var_CSVData , "$.records["+var_Count+"].AmntRec1");
                 LOGGER.info("Executed Step = STORE,var_AmntRec1,var_CSVData,$.records[{var_Count}].AmntRec1,TGTYPESCREENREG");
		String var_CollectionNumber2;
                 LOGGER.info("Executed Step = VAR,String,var_CollectionNumber2,TGTYPESCREENREG");
		var_CollectionNumber2 = getJsonData(var_CSVData , "$.records["+var_Count+"].CollectionNumber2");
                 LOGGER.info("Executed Step = STORE,var_CollectionNumber2,var_CSVData,$.records[{var_Count}].CollectionNumber2,TGTYPESCREENREG");
		String var_UpperAndLower;
                 LOGGER.info("Executed Step = VAR,String,var_UpperAndLower,TGTYPESCREENREG");
		var_UpperAndLower = getJsonData(var_CSVData , "$.records["+var_Count+"].UpperAndLower");
                 LOGGER.info("Executed Step = STORE,var_UpperAndLower,var_CSVData,$.records[{var_Count}].UpperAndLower,TGTYPESCREENREG");
		String var_AmntRec2;
                 LOGGER.info("Executed Step = VAR,String,var_AmntRec2,TGTYPESCREENREG");
		var_AmntRec2 = getJsonData(var_CSVData , "$.records["+var_Count+"].AmntRec2");
                 LOGGER.info("Executed Step = STORE,var_AmntRec2,var_CSVData,$.records[{var_Count}].AmntRec2,TGTYPESCREENREG");
		String var_CollectionNumber3;
                 LOGGER.info("Executed Step = VAR,String,var_CollectionNumber3,TGTYPESCREENREG");
		var_CollectionNumber3 = getJsonData(var_CSVData , "$.records["+var_Count+"].CollectionNumber3");
                 LOGGER.info("Executed Step = STORE,var_CollectionNumber3,var_CSVData,$.records[{var_Count}].CollectionNumber3,TGTYPESCREENREG");
		String var_SpaceInName;
                 LOGGER.info("Executed Step = VAR,String,var_SpaceInName,TGTYPESCREENREG");
		var_SpaceInName = getJsonData(var_CSVData , "$.records["+var_Count+"].SpaceInName");
                 LOGGER.info("Executed Step = STORE,var_SpaceInName,var_CSVData,$.records[{var_Count}].SpaceInName,TGTYPESCREENREG");
		String var_AmntRec3;
                 LOGGER.info("Executed Step = VAR,String,var_AmntRec3,TGTYPESCREENREG");
		var_AmntRec3 = getJsonData(var_CSVData , "$.records["+var_Count+"].AmntRec3");
                 LOGGER.info("Executed Step = STORE,var_AmntRec3,var_CSVData,$.records[{var_Count}].AmntRec3,TGTYPESCREENREG");
		String var_CollectionNumber4;
                 LOGGER.info("Executed Step = VAR,String,var_CollectionNumber4,TGTYPESCREENREG");
		var_CollectionNumber4 = getJsonData(var_CSVData , "$.records["+var_Count+"].CollectionNumber4");
                 LOGGER.info("Executed Step = STORE,var_CollectionNumber4,var_CSVData,$.records[{var_Count}].CollectionNumber4,TGTYPESCREENREG");
		String var_CompanySpaceInName;
                 LOGGER.info("Executed Step = VAR,String,var_CompanySpaceInName,TGTYPESCREENREG");
		var_CompanySpaceInName = getJsonData(var_CSVData , "$.records["+var_Count+"].CompanySpaceInName");
                 LOGGER.info("Executed Step = STORE,var_CompanySpaceInName,var_CSVData,$.records[{var_Count}].CompanySpaceInName,TGTYPESCREENREG");
		String var_AmntRec4;
                 LOGGER.info("Executed Step = VAR,String,var_AmntRec4,TGTYPESCREENREG");
		var_AmntRec4 = getJsonData(var_CSVData , "$.records["+var_Count+"].AmntRec4");
                 LOGGER.info("Executed Step = STORE,var_AmntRec4,var_CSVData,$.records[{var_Count}].AmntRec4,TGTYPESCREENREG");
		String var_CollectionNumber5;
                 LOGGER.info("Executed Step = VAR,String,var_CollectionNumber5,TGTYPESCREENREG");
		var_CollectionNumber5 = getJsonData(var_CSVData , "$.records["+var_Count+"].CollectionNumber5");
                 LOGGER.info("Executed Step = STORE,var_CollectionNumber5,var_CSVData,$.records[{var_Count}].CollectionNumber5,TGTYPESCREENREG");
		String var_Apostrophe;
                 LOGGER.info("Executed Step = VAR,String,var_Apostrophe,TGTYPESCREENREG");
		var_Apostrophe = getJsonData(var_CSVData , "$.records["+var_Count+"].Apostrophe");
                 LOGGER.info("Executed Step = STORE,var_Apostrophe,var_CSVData,$.records[{var_Count}].Apostrophe,TGTYPESCREENREG");
		String var_AmntRec5;
                 LOGGER.info("Executed Step = VAR,String,var_AmntRec5,TGTYPESCREENREG");
		var_AmntRec5 = getJsonData(var_CSVData , "$.records["+var_Count+"].AmntRec5");
                 LOGGER.info("Executed Step = STORE,var_AmntRec5,var_CSVData,$.records[{var_Count}].AmntRec5,TGTYPESCREENREG");
		String var_CollectionNumber6;
                 LOGGER.info("Executed Step = VAR,String,var_CollectionNumber6,TGTYPESCREENREG");
		var_CollectionNumber6 = getJsonData(var_CSVData , "$.records["+var_Count+"].CollectionNumber6");
                 LOGGER.info("Executed Step = STORE,var_CollectionNumber6,var_CSVData,$.records[{var_Count}].CollectionNumber6,TGTYPESCREENREG");
		String var_ThreeCharacter;
                 LOGGER.info("Executed Step = VAR,String,var_ThreeCharacter,TGTYPESCREENREG");
		var_ThreeCharacter = getJsonData(var_CSVData , "$.records["+var_Count+"].ThreeCharacter");
                 LOGGER.info("Executed Step = STORE,var_ThreeCharacter,var_CSVData,$.records[{var_Count}].ThreeCharacter,TGTYPESCREENREG");
		String var_AmntRec6;
                 LOGGER.info("Executed Step = VAR,String,var_AmntRec6,TGTYPESCREENREG");
		var_AmntRec6 = getJsonData(var_CSVData , "$.records["+var_Count+"].AmntRec6");
                 LOGGER.info("Executed Step = STORE,var_AmntRec6,var_CSVData,$.records[{var_Count}].AmntRec6,TGTYPESCREENREG");
		String var_OwnerDiffClients;
                 LOGGER.info("Executed Step = VAR,String,var_OwnerDiffClients,TGTYPESCREENREG");
		var_OwnerDiffClients = getJsonData(var_CSVData , "$.records["+var_Count+"].OwnerDiffClients");
                 LOGGER.info("Executed Step = STORE,var_OwnerDiffClients,var_CSVData,$.records[{var_Count}].OwnerDiffClients,TGTYPESCREENREG");
		String var_GroupName;
                 LOGGER.info("Executed Step = VAR,String,var_GroupName,TGTYPESCREENREG");
		var_GroupName = getJsonData(var_CSVData , "$.records["+var_Count+"].GroupName");
                 LOGGER.info("Executed Step = STORE,var_GroupName,var_CSVData,$.records[{var_Count}].GroupName,TGTYPESCREENREG");
		String var_IndividualSpace;
                 LOGGER.info("Executed Step = VAR,String,var_IndividualSpace,TGTYPESCREENREG");
		var_IndividualSpace = getJsonData(var_CSVData , "$.records["+var_Count+"].IndividualSpace");
                 LOGGER.info("Executed Step = STORE,var_IndividualSpace,var_CSVData,$.records[{var_Count}].IndividualSpace,TGTYPESCREENREG");
		String var_CompanySpace;
                 LOGGER.info("Executed Step = VAR,String,var_CompanySpace,TGTYPESCREENREG");
		var_CompanySpace = getJsonData(var_CSVData , "$.records["+var_Count+"].CompanySpace");
                 LOGGER.info("Executed Step = STORE,var_CompanySpace,var_CSVData,$.records[{var_Count}].CompanySpace,TGTYPESCREENREG");
		String var_CompanyApostrophe;
                 LOGGER.info("Executed Step = VAR,String,var_CompanyApostrophe,TGTYPESCREENREG");
		var_CompanyApostrophe = getJsonData(var_CSVData , "$.records["+var_Count+"].CompanyApostrophe");
                 LOGGER.info("Executed Step = STORE,var_CompanyApostrophe,var_CSVData,$.records[{var_Count}].CompanyApostrophe,TGTYPESCREENREG");
        CALL.$("FTJQA2Login","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

		try { 
		 WebElement html = driver.findElement(By.tagName("html"));
		    	
		    	System.out.println("html size" + html.getSize());
		    	System.out.println("html" + html.getTagName());

		    	for( int i =0 ;i<4;i++) {
		        html.sendKeys(Keys.chord(Keys.CONTROL, Keys.SUBTRACT));
		    	}
		 
		 ;} 
		 catch(Exception e) { PrintVal("Renderer Script Exception : "+e.toString()); } 
		 PrintVal("Executed Step = CALLRENDERERSCRIPT,FTJ41ZOOMMAGNIFICATIONCHANGE"); 
        TYPE.$("ELE_TXTBOX_USERNAME","//input[@id='guestForm:username']","x5080","FALSE","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_PASSWORD","//input[@id='guestForm:password']","Welcome123$","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_LOGIN","//button[@id='guestForm:loginButton']","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        CALL.$("BatchPaymentValidationFieldChecks","TGTYPESCREENREG");

        TAP.$("ELE_TXTBOX_SEARCHTASKS","//input[@id='workbenchForm:workbenchTabs:taskSearchContainer:taskSearchSearch']","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_SEARCHTASKS","//input[@id='workbenchForm:workbenchTabs:taskSearchContainer:taskSearchSearch']","BLC000","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_TASKSEARCH","//a[@id='workbenchForm:workbenchTabs:taskSearchContainer:tb_buttonSearch']//span[@class='fa fa-search fa-lg']","TGTYPESCREENREG");

        WAIT.$(1,"TGTYPESCREENREG");

        TAP.$("ELE_LINK_BILLINGANDPAYMENTPROPERTIES","//span[contains(text(),'Billing and Payment Properties')]","TGTYPESCREENREG");

        WAIT.$(1,"TGTYPESCREENREG");

        ASSERT.$("ELE_GETVAL_BATCHPAYMENTNAMEVALIDATION","//span[@id='workbenchForm:workbenchTabs:batchPaymentLastNameValidation']","VISIBLE","TGTYPESCREENFULL");

        ASSERT.$("ELE_GETVAL_NUMBEROFNAMEPOSITIONSTOVALIDATE","//span[@id='workbenchForm:workbenchTabs:noOfNamePositions']","VISIBLE","TGTYPESCREENREG");

        ASSERT.$("ELE_GETVAL_NAMEVALIDATIONCASESENSITIVE","//span[@id='workbenchForm:workbenchTabs:nameValidationCaseSensitive']","VISIBLE","TGTYPESCREENREG");

        TAP.$("ELE_LOGO_GIAS","//span[text()='GIAS']","TGTYPESCREENREG");

        WAIT.$(1,"TGTYPESCREENREG");

        CALL.$("BatchPolicyPaymentListBillValidationsTemp01","TGTYPESCREENREG");

        TAP.$("ELE_TXTBOX_SEARCHTASKS","//input[@id='workbenchForm:workbenchTabs:taskSearchContainer:taskSearchSearch']","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_SEARCHTASKS","//input[@id='workbenchForm:workbenchTabs:taskSearchContainer:taskSearchSearch']","Batch Policy Payment","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_TASKSEARCH","//a[@id='workbenchForm:workbenchTabs:taskSearchContainer:tb_buttonSearch']//span[@class='fa fa-search fa-lg']","TGTYPESCREENREG");

        WAIT.$(1,"TGTYPESCREENREG");

        TAP.$("ELE_LINK_BATCHPOLICYPAYMENT","//span[contains(text(),'Batch Policy Payment')]","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

        CALL.$("ChangeCurrencyDrpDwnToDollarUS","TGTYPESCREENREG");

        TAP.$("ELE_DRPDWN_CURRENCY","//div[@class='ui-selectonemenu-trigger ui-state-default ui-corner-right']","TGTYPESCREENREG");

        TAP.$("ELE_DRPDWN_DOLLARUS","//li[@class='ui-selectonemenu-item ui-selectonemenu-list-item ui-corner-all'][contains(text(),'Dollar [US]')]","TGTYPESCREENREG");

        WAIT.$(4,"TGTYPESCREENREG");

        TAP.$("ELE_TXTBOX_BATCHNUMBER","//input[@name='workbenchForm:workbenchTabs:grid:suspenseControlNumb_Col:filter']","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_BATCHNUMBER","//input[@name='workbenchForm:workbenchTabs:grid:suspenseControlNumb_Col:filter']","11222","FALSE","TGTYPESCREENREG");

        WAIT.$(1,"TGTYPESCREENREG");

        TAP.$("ELE_LINK_BATCHNUMBER_ROW1","//tbody[@id='workbenchForm:workbenchTabs:grid_data']//tr[1]","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

        ASSERT.$("ELE_GETVAL_NAMECOLUMN_BATCHPOLICYPAYMENT","//span[contains(text(),'Name')]","VISIBLE","TGTYPESCREENREG");

        CALL.$("CollectionNumberAndAmountRecievedRow1","TGTYPESCREENREG");

        TAP.$("ELE_TXTBOX_COLLECTIONNUMBER_TAP_ROW1","//div[@class='ui-datatable ui-widget dynamicTable']//tr[1]//td[1]//div[1]//div[1]","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_COLLECTIONNUMBER_TYPE_ROW1","//input[@name='workbenchForm:workbenchTabs:grid_blc400s2:0:in_collectionNumber_Col']",var_CollectionNumber1,"FALSE","TGTYPESCREENREG");

        TAP.$("ELE_TXTBOX_AMOUNTRECEIVED_TAP_ROW1","//tr[1]//td[3]//div[1]//div[1]","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_AMOUNTRECEIVED_TYPE_ROW1","//input[@name='workbenchForm:workbenchTabs:grid_blc400s2:0:in_amountReceived_Col_input']",var_AmntRec1,"FALSE","TGTYPESCREENREG");

        TAP.$("ELE_TXTBOX_NAMECOLUMN_TAP_ROW1","//div[@class='ui-datatable ui-widget dynamicTable']//tr[1]//td[2]//div[1]//div[1]","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_NAME_TYPE_ROW1","//input[@name='workbenchForm:workbenchTabs:grid_blc400s2:0:in_ownerName_Col']",var_WrongGroup,"FALSE","TGTYPESCREENREG");

        CALL.$("CollectionNumberAndAmountRecievedRow2","TGTYPESCREENREG");

        SWIPE.$("UP","TGTYPESCREENREG");

        TAP.$("ELE_TXTBOX_COLLECTIONNUMBER_TAP_ROW2","//div[@class='ui-datatable ui-widget dynamicTable']//tr[2]//td[1]//div[1]//div[1]","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_COLLECTIONNUMBER_TYPE_ROW2","//input[@name='workbenchForm:workbenchTabs:grid_blc400s2:1:in_collectionNumber_Col']",var_CollectionNumber2,"FALSE","TGTYPESCREENREG");

        TAP.$("ELE_TXTBOX_AMOUNTRECEIVED_TAP_ROW2","//tr[2]//td[3]//div[1]//div[1]","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_AMOUNTRECEIVED_TYPE_ROW2","//input[@name='workbenchForm:workbenchTabs:grid_blc400s2:1:in_amountReceived_Col_input']",var_AmntRec2,"FALSE","TGTYPESCREENREG");

        TAP.$("ELE_TXTBOX_NAMECOLUMN_TAP_ROW2","//tr[2]//td[2]//div[1]//div[1]","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_NAME_TYPE_ROW2","//input[@name='workbenchForm:workbenchTabs:grid_blc400s2:1:in_ownerName_Col']",var_UpperAndLower,"FALSE","TGTYPESCREENREG");

        CALL.$("CollectionNumberAndAmountRecievedRow3","TGTYPESCREENREG");

        TAP.$("ELE_TXTBOX_COLLECTIONNUMBER_TAP_ROW3","//tr[3]//td[1]//div[1]//div[1]","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_COLLECTIONNUMBER_TYPE_ROW3","//input[@name='workbenchForm:workbenchTabs:grid_blc400s2:2:in_collectionNumber_Col']",var_CollectionNumber3,"FALSE","TGTYPESCREENREG");

        TAP.$("ELE_TXTBOX_AMOUNTRECEIVED_TAP_ROW3","//tr[3]//td[3]//div[1]//div[1]","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_AMOUNTRECEIVED_TYPE_ROW3","//input[@name='workbenchForm:workbenchTabs:grid_blc400s2:2:in_amountReceived_Col_input']",var_AmntRec3,"FALSE","TGTYPESCREENREG");

        TAP.$("ELE_TXTBOX_NAMECOLUMN_TAP_ROW3","//tr[3]//td[2]//div[1]//div[1]","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_NAME_TYPE_ROW3","//input[@name='workbenchForm:workbenchTabs:grid_blc400s2:2:in_ownerName_Col']",var_SpaceInName,"FALSE","TGTYPESCREENREG");

        CALL.$("CollectionNumberAndAmountRecievedRow4","TGTYPESCREENREG");

        TAP.$("ELE_TXTBOX_COLLECTIONNUMBER_TAP_ROW4","//tr[4]//td[1]//div[1]//div[1]","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_COLLECTIONNUMBER_TYPE_ROW4","//input[@name='workbenchForm:workbenchTabs:grid_blc400s2:3:in_collectionNumber_Col']",var_CollectionNumber4,"FALSE","TGTYPESCREENREG");

        TAP.$("ELE_TXTBOX_AMOUNTRECEIVED_TAP_ROW4","//tr[4]//td[3]//div[1]//div[1]","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_AMOUNTRECEIVED_TYPE_ROW4","//input[@name='workbenchForm:workbenchTabs:grid_blc400s2:3:in_amountReceived_Col_input']",var_AmntRec4,"FALSE","TGTYPESCREENREG");

        TAP.$("ELE_TXTBOX_NAMECOLUMN_TAP_ROW4","//tr[4]//td[2]//div[1]//div[1]","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_NAME_TYPE_ROW4","//input[@name='workbenchForm:workbenchTabs:grid_blc400s2:3:in_ownerName_Col']",var_CompanySpaceInName,"FALSE","TGTYPESCREENREG");

        CALL.$("CollectionNumberAndAmountRecievedRow5","TGTYPESCREENREG");

        TAP.$("ELE_TXTBOX_COLLECTIONNUMBER_TAP_ROW5","//div[@class='ui-datatable ui-widget dynamicTable']//tr[5]//td[1]//div[1]//div[1]","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_COLLECTIONNUMBER_TYPE_ROW5","//input[@name='workbenchForm:workbenchTabs:grid_blc400s2:4:in_collectionNumber_Col']",var_CollectionNumber5,"FALSE","TGTYPESCREENREG");

        TAP.$("ELE_TXTBOX_AMOUNTRECEIVED_TAP_ROW5","//tr[5]//td[3]//div[1]//div[1]","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_AMOUNTRECEIVED_TYPE_ROW5","//input[@name='workbenchForm:workbenchTabs:grid_blc400s2:4:in_amountReceived_Col_input']",var_AmntRec5,"FALSE","TGTYPESCREENREG");

        TAP.$("ELE_TXTBOX_NAMECOLUMN_TAP_ROW5","//div[@class='ui-datatable ui-widget dynamicTable']//tr[5]//td[2]//div[1]//div[1]","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_NAME_TYPE_ROW5","//input[@name='workbenchForm:workbenchTabs:grid_blc400s2:4:in_ownerName_Col']",var_Apostrophe,"FALSE","TGTYPESCREENREG");

        TAP.$("ELE_TXTBOX_NAMECOLUMN_TAP_ROW6","//tr[6]//td[2]//div[1]//div[1]","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_VALIDATE","//span[contains(text(),'Validate')]","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_DISPLAYMESSAGE_ROW1","//button[@id='workbenchForm:workbenchTabs:grid_blc400s2:0:msgbox_button']","TGTYPESCREENREG");

        WAIT.$(3,"TGTYPESCREENREG");

        ASSERT.$("ELE_GETMSG_DISPLAYMESSAGES","//div[@id='workbenchForm:workbenchTabs:messagesInDialog']//span[@class='ui-messages-error-summary']","CONTAINS","Name entered does not match name on group.","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_OK","//span[contains(text(),'OK')]","TGTYPESCREENREG");

        WAIT.$(1,"TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_DISPLAYMESSAGE_ROW2","//button[@id='workbenchForm:workbenchTabs:grid_blc400s2:1:msgbox_button']","TGTYPESCREENREG");

        WAIT.$(3,"TGTYPESCREENREG");

        ASSERT.$("ELE_GETMSG_DISPLAYMESSAGES","//div[@id='workbenchForm:workbenchTabs:messagesInDialog']//span[@class='ui-messages-error-summary']","CONTAINS","Name entered does not match name on group.","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_OK","//span[contains(text(),'OK')]","TGTYPESCREENREG");

        WAIT.$(1,"TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_DISPLAYMESSAGE_ROW3","//button[@id='workbenchForm:workbenchTabs:grid_blc400s2:2:msgbox_button']","TGTYPESCREENREG");

        WAIT.$(3,"TGTYPESCREENREG");

        ASSERT.$("ELE_GETMSG_DISPLAYMESSAGES","//div[@id='workbenchForm:workbenchTabs:messagesInDialog']//span[@class='ui-messages-error-summary']","CONTAINS","Name entered does not match name on group.","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_OK","//span[contains(text(),'OK')]","TGTYPESCREENREG");

        WAIT.$(1,"TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_DISPLAYMESSAGE_ROW4","//button[@id='workbenchForm:workbenchTabs:grid_blc400s2:3:msgbox_button']","TGTYPESCREENREG");

        WAIT.$(3,"TGTYPESCREENREG");

        ASSERT.$("ELE_GETMSG_DISPLAYMESSAGES","//div[@id='workbenchForm:workbenchTabs:messagesInDialog']//span[@class='ui-messages-error-summary']","CONTAINS","Name entered does not match name on group.","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_OK","//span[contains(text(),'OK')]","TGTYPESCREENREG");

        WAIT.$(1,"TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_DISPLAYMESSAGE_ROW5","//button[@id='workbenchForm:workbenchTabs:grid_blc400s2:4:msgbox_button']","TGTYPESCREENREG");

        WAIT.$(3,"TGTYPESCREENREG");

        ASSERT.$("ELE_GETMSG_DISPLAYMESSAGES","//div[@id='workbenchForm:workbenchTabs:messagesInDialog']//span[@class='ui-messages-error-summary']","CONTAINS","Name entered does not match name on group.","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_OK","//span[contains(text(),'OK')]","TGTYPESCREENREG");

        WAIT.$(1,"TGTYPESCREENREG");

		var_Count = var_Count + ( 1 );
                 LOGGER.info("Executed Step = INCREASEVAR,var_Count,1,TGTYPESCREENREG");
    	}
          LOGGER.info("Executed Step = WEND,TGTYPESCREENREG");
    	resetAttemps();

    }


    @Test
    public void ftj41modf024() {

        // No need to declare / print method name here, handled by TestNg listener
        // declare("ftj41modf024");

		int var_Count = 0;
                 LOGGER.info("Executed Step = VAR,Integer,var_Count,0,TGTYPESCREENREG");
		com.jayway.jsonpath.ReadContext var_CSVData = getJsonData("http://10.1.104.229/json/20201009/k6Hzt1.json");
                 LOGGER.info("Executed Step = VAR,Json,var_CSVData,http://10.1.104.229/json/20201009/k6Hzt1.json,TGTYPESCREENREG");
        LOGGER.info("Executed Step = REPEAT IF");
    	while (check(var_Count,"<",1,"TGTYPESCREENREG")) {
          LOGGER.info("Executed Step = WHILE,var_Count,<,1,TGTYPESCREENREG");
		String var_Plan;
                 LOGGER.info("Executed Step = VAR,String,var_Plan,TGTYPESCREENREG");
		var_Plan = getJsonData(var_CSVData , "$.records["+var_Count+"].Plan");
                 LOGGER.info("Executed Step = STORE,var_Plan,var_CSVData,$.records[{var_Count}].Plan,TGTYPESCREENREG");
		String var_PlanToCopy;
                 LOGGER.info("Executed Step = VAR,String,var_PlanToCopy,TGTYPESCREENREG");
		var_PlanToCopy = getJsonData(var_CSVData , "$.records["+var_Count+"].PlanToCopy");
                 LOGGER.info("Executed Step = STORE,var_PlanToCopy,var_CSVData,$.records[{var_Count}].PlanToCopy,TGTYPESCREENREG");
		String var_CopyToPlan;
                 LOGGER.info("Executed Step = VAR,String,var_CopyToPlan,TGTYPESCREENREG");
		var_CopyToPlan = getJsonData(var_CSVData , "$.records["+var_Count+"].CopyToPlan");
                 LOGGER.info("Executed Step = STORE,var_CopyToPlan,var_CSVData,$.records[{var_Count}].CopyToPlan,TGTYPESCREENREG");
		String var_ShortDescription;
                 LOGGER.info("Executed Step = VAR,String,var_ShortDescription,TGTYPESCREENREG");
		var_ShortDescription = getJsonData(var_CSVData , "$.records["+var_Count+"].ShortDescription");
                 LOGGER.info("Executed Step = STORE,var_ShortDescription,var_CSVData,$.records[{var_Count}].ShortDescription,TGTYPESCREENREG");
		String var_IssueDate;
                 LOGGER.info("Executed Step = VAR,String,var_IssueDate,TGTYPESCREENREG");
		var_IssueDate = getJsonData(var_CSVData , "$.records["+var_Count+"].IssueDate");
                 LOGGER.info("Executed Step = STORE,var_IssueDate,var_CSVData,$.records[{var_Count}].IssueDate,TGTYPESCREENREG");
		String var_TargetPlan;
                 LOGGER.info("Executed Step = VAR,String,var_TargetPlan,TGTYPESCREENREG");
		var_TargetPlan = getJsonData(var_CSVData , "$.records["+var_Count+"].TargetPlan");
                 LOGGER.info("Executed Step = STORE,var_TargetPlan,var_CSVData,$.records[{var_Count}].TargetPlan,TGTYPESCREENREG");
        CALL.$("FTJQA2Login","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

		try { 
		 WebElement html = driver.findElement(By.tagName("html"));
		    	
		    	System.out.println("html size" + html.getSize());
		    	System.out.println("html" + html.getTagName());

		    	for( int i =0 ;i<4;i++) {
		        html.sendKeys(Keys.chord(Keys.CONTROL, Keys.SUBTRACT));
		    	}
		 
		 ;} 
		 catch(Exception e) { PrintVal("Renderer Script Exception : "+e.toString()); } 
		 PrintVal("Executed Step = CALLRENDERERSCRIPT,FTJ41ZOOMMAGNIFICATIONCHANGE"); 
        TYPE.$("ELE_TXTBOX_USERNAME","//input[@id='guestForm:username']","x5080","FALSE","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_PASSWORD","//input[@id='guestForm:password']","Welcome123$","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_LOGIN","//button[@id='guestForm:loginButton']","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        CALL.$("ModF024PlanDescriptionValidations","TGTYPESCREENREG");

        TAP.$("ELE_TXTBOX_SEARCHTASKS","//input[@id='workbenchForm:workbenchTabs:taskSearchContainer:taskSearchSearch']","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_SEARCHTASKS","//input[@id='workbenchForm:workbenchTabs:taskSearchContainer:taskSearchSearch']","Plan Setup","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_TASKSEARCH","//a[@id='workbenchForm:workbenchTabs:taskSearchContainer:tb_buttonSearch']//span[@class='fa fa-search fa-lg']","TGTYPESCREENREG");

        WAIT.$(1,"TGTYPESCREENREG");

        TAP.$("ELE_LINK_PLANSETUP","//span[contains(text(),'Plan Setup')]","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

        TAP.$("ELE_TXTBOX_PLAN","//input[@id='workbenchForm:workbenchTabs:grid:planCode_Col:filter']","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_PLAN","//input[@id='workbenchForm:workbenchTabs:grid:planCode_Col:filter']",var_Plan,"FALSE","TGTYPESCREENREG");

        WAIT.$(3,"TGTYPESCREENREG");

        TAP.$("ELE_LINK_PLAN_ROW1","//div[@class='ui-outputpanel ui-widget']//td[1]","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_EDIT","//span[text()='Edit']","TGTYPESCREENREG");

        WAIT.$(3,"TGTYPESCREENREG");

        TAP.$("ELE_TAB_PREMIUMS","//div[@class='ui-outputpanel ui-widget']//li[10]//a[1]//span[1]","TGTYPESCREENREG");

        WAIT.$(4,"TGTYPESCREENREG");

        TAP.$("ELE_DRPDWN_OVERRIDEPREMIUMINCOMESTATE","//tr[3]//td[1]//fieldset[1]//div[1]//table[1]//tbody[1]//tr[5]//td[2]//div[1]//div[3]//span[1]","TGTYPESCREENREG");

        TAP.$("ELE_DRPDWN_BENEFITTAXREPORTINGSTATE","//li[contains(text(),'Benefit Tax Reporting State')]","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_VALIDATE","//span[contains(text(),'Validate')]","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

        ASSERT.$("ELE_MSG_VALIDATIONSUCCESS","//div[@id='workbenchForm:workbenchTabs:messages']//span[@class='ui-messages-info-summary'][contains(text(),'Validation completed successfully with no errors.')]","VISIBLE","TGTYPESCREENREG");

        TAP.$("ELE_DRPDWN_OVERRIDEPREMIUMINCOMESTATE","//tr[3]//td[1]//fieldset[1]//div[1]//table[1]//tbody[1]//tr[5]//td[2]//div[1]//div[3]//span[1]","TGTYPESCREENREG");

        TAP.$("ELE_DRPDWN_EMPLOYERSITUSSTATE","//li[contains(text(),'Employer Situs State')]","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_VALIDATE","//span[contains(text(),'Validate')]","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

        ASSERT.$("ELE_MSG_VALIDATIONSUCCESS","//div[@id='workbenchForm:workbenchTabs:messages']//span[@class='ui-messages-info-summary'][contains(text(),'Validation completed successfully with no errors.')]","VISIBLE","TGTYPESCREENREG");

        TAP.$("ELE_DRPDWN_OVERRIDEPREMIUMINCOMESTATE","//tr[3]//td[1]//fieldset[1]//div[1]//table[1]//tbody[1]//tr[5]//td[2]//div[1]//div[3]//span[1]","TGTYPESCREENREG");

        TAP.$("ELE_DRPDWN_LISTBILLADDRESSSTATE","//li[contains(text(),'List Bill Address State')]","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_VALIDATE","//span[contains(text(),'Validate')]","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

        ASSERT.$("ELE_MSG_VALIDATIONSUCCESS","//div[@id='workbenchForm:workbenchTabs:messages']//span[@class='ui-messages-info-summary'][contains(text(),'Validation completed successfully with no errors.')]","VISIBLE","TGTYPESCREENREG");

        TAP.$("ELE_DRPDWN_OVERRIDEPREMIUMINCOMESTATE","//tr[3]//td[1]//fieldset[1]//div[1]//table[1]//tbody[1]//tr[5]//td[2]//div[1]//div[3]//span[1]","TGTYPESCREENREG");

        TAP.$("ELE_DRPDWN_NOSTATE","//li[contains(text(),'No State')]","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_VALIDATE","//span[contains(text(),'Validate')]","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

        ASSERT.$("ELE_MSG_VALIDATIONSUCCESS","//div[@id='workbenchForm:workbenchTabs:messages']//span[@class='ui-messages-info-summary'][contains(text(),'Validation completed successfully with no errors.')]","VISIBLE","TGTYPESCREENREG");

        TAP.$("ELE_DRPDWN_OVERRIDEPREMIUMINCOMESTATE","//tr[3]//td[1]//fieldset[1]//div[1]//table[1]//tbody[1]//tr[5]//td[2]//div[1]//div[3]//span[1]","TGTYPESCREENREG");

        TAP.$("ELE_DRPDWN_PAYORADDRESSSTATE","//li[contains(text(),'Payor Address State')]","TGTYPESCREENREG");

        ASSERT.$("ELE_MSG_VALIDATIONSUCCESS","//div[@id='workbenchForm:workbenchTabs:messages']//span[@class='ui-messages-info-summary'][contains(text(),'Validation completed successfully with no errors.')]","VISIBLE","TGTYPESCREENREG");

        SWIPE.$("DOWN","TGTYPESCREENREG");

        SWIPE.$("DOWN","TGTYPESCREENREG");

        TAP.$("ELE_TAB_BASICINSURANCEINFORMATION","//span[contains(text(),'Basic Insurance Information')]","TGTYPESCREENREG");

        WAIT.$(3,"TGTYPESCREENREG");

        WAIT.$("ELE_BUTTON_SAVE","//span[contains(text(),'Save')]","VISIBLE","TGTYPESCREENREG");

        WAIT.$(1,"TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SAVE","//span[contains(text(),'Save')]","TGTYPESCREENFULL");

        WAIT.$(2,"TGTYPESCREENREG");

        ASSERT.$("ELE_MSG_ITEMSUCCESSUPDATE","//div[@id='workbenchForm:workbenchTabs:messages']//span[@class='ui-messages-info-summary'][contains(text(),'Item successfully updated.')]","VISIBLE","TGTYPESCREENREG");

        TAP.$("ELE_TAB_PREMIUMS","//div[@class='ui-outputpanel ui-widget']//li[10]//a[1]//span[1]","TGTYPESCREENREG");

        WAIT.$(4,"TGTYPESCREENREG");

        ASSERT.$("ELE_GETVAL_OVERRIDEPREMIUMINCOMESTATE","//span[@id='workbenchForm:workbenchTabs:overridePremIncState']","CONTAINS","Payor Address State","TGTYPESCREENREG");

        TAP.$("ELE_TAB_CLOSE3","//li[@class='ui-state-default ui-tabs-selected ui-state-active ui-corner-top']//span[@class='ui-icon ui-icon-close']","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

        CALL.$("ModF024CopyPlanValidations","TGTYPESCREENREG");

        TAP.$("ELE_TXTBOX_PLAN_PLANDESCRIPTION","//input[@name='workbenchForm:workbenchTabs:grid:planCode_Col:filter']","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_PLAN_PLANDESCRIPTION","//input[@name='workbenchForm:workbenchTabs:grid:planCode_Col:filter']",var_PlanToCopy,"FALSE","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

        TAP.$("ELE_LINK_PLAN_ROW1","//div[@class='ui-outputpanel ui-widget']//td[1]","TGTYPESCREENREG");

        WAIT.$(4,"TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_COPY","//span[contains(text(),'Copy')]","TGTYPESCREENFULL");

        TAP.$("ELE_TXTBOX_PLANCODE_COPY","//input[@name='workbenchForm:workbenchTabs:planCode2']","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_PLANCODE_COPY","//input[@name='workbenchForm:workbenchTabs:planCode2']",var_CopyToPlan,"FALSE","TGTYPESCREENREG");

        TAP.$("ELE_TXTBOX_SHORTDESCRIPTION","//input[@name='workbenchForm:workbenchTabs:shortDescription2']","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_SHORTDESCRIPTION","//input[@name='workbenchForm:workbenchTabs:shortDescription2']",var_ShortDescription,"FALSE","TGTYPESCREENREG");

        TAP.$("ELE_TXTBOX_ISSUEDATE","//input[@name='workbenchForm:workbenchTabs:beginningIssueDate2_input']","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_ISSUEDATE","//input[@name='workbenchForm:workbenchTabs:beginningIssueDate2_input']",var_IssueDate,"FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SAVE","//span[contains(text(),'Save')]","TGTYPESCREENREG");

        WAIT.$(3,"TGTYPESCREENREG");

        ASSERT.$("ELE_GETVAL_PLAN","//span[@id='workbenchForm:workbenchTabs:planCode']","CONTAINS",var_CopyToPlan,"TGTYPESCREENFULL");

        TAP.$("ELE_LOGO_GIAS","//span[text()='GIAS']","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

        CALL.$("ModF024EmployerValidations","TGTYPESCREENREG");

        TAP.$("ELE_TXTBOX_SEARCHTASKS","//input[@id='workbenchForm:workbenchTabs:taskSearchContainer:taskSearchSearch']","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_SEARCHTASKS","//input[@id='workbenchForm:workbenchTabs:taskSearchContainer:taskSearchSearch']","Employer","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_TASKSEARCH","//a[@id='workbenchForm:workbenchTabs:taskSearchContainer:tb_buttonSearch']//span[@class='fa fa-search fa-lg']","TGTYPESCREENREG");

        WAIT.$(1,"TGTYPESCREENREG");

        TAP.$("ELE_LABEL_INDIVIDUALLIFEEMPLOYER","//span[contains(text(),'Individual Life Employers')]","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_ADDNEW","//button[@id='workbenchForm:workbenchTabs:tb_buttonAdd']","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

        ASSERT.$("ELE_DRPDWN_SITUSSTATEFORTAXREPORTING","//div[@id='workbenchForm:workbenchTabs:countryAndStateCode']//div//span","VISIBLE","TGTYPESCREENFULL");

        TAP.$("ELE_TAB_CLOSE3","//li[@class='ui-state-default ui-tabs-selected ui-state-active ui-corner-top']//span[@class='ui-icon ui-icon-close']","TGTYPESCREENREG");

        WAIT.$(3,"TGTYPESCREENREG");

        TAP.$("ELE_LINK_EMPLOYER_ROW1","//tbody[@id='workbenchForm:workbenchTabs:grid_data']//tr[1]","TGTYPESCREENREG");

        WAIT.$(3,"TGTYPESCREENREG");

        ASSERT.$("ELE_GETVAL_SITUSSTATEFORTAXREPORTING","//label[@id='workbenchForm:workbenchTabs:countryAndStateCode_lbl']","VISIBLE","TGTYPESCREENFULL");

        TAP.$("ELE_BUTTON_COPY","//span[contains(text(),'Copy')]","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

        ASSERT.$("ELE_DRPDWN_SITUSSTATEFORTAXREPORTING","//div[@id='workbenchForm:workbenchTabs:countryAndStateCode']//div//span","VISIBLE","TGTYPESCREENREG");

        TAP.$("ELE_LOGO_GIAS","//span[text()='GIAS']","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

        CALL.$("ModF024CheckPremiumIncomTaxDrpDwnIsNOTvisible","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_SEARCHTASKS","//input[@id='workbenchForm:workbenchTabs:taskSearchContainer:taskSearchSearch']","New Policy Application","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_TASKSEARCH","//a[@id='workbenchForm:workbenchTabs:taskSearchContainer:tb_buttonSearch']//span[@class='fa fa-search fa-lg']","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_NEWPOLICYAPP","//span[contains(text(),'New Policy Application')]","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_FILTERPOLICYNUMBER1","//input[@id='workbenchForm:workbenchTabs:grid:policyNumber_Col:filter']","0000007002","FALSE","TGTYPESCREENREG");

        WAIT.$(1,"TGTYPESCREENREG");

        TAP.$("ELE_LINK_POLICYNUMBER_ROW1","//tr[@class='ui-widget-content ui-datatable-even ui-datatable-selectable']","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

        ASSERT.$("ELE_GETVAL_OVERRIDEPREMIUMINCOMESTATE","//span[@id='workbenchForm:workbenchTabs:overridePremIncState']","INVISIBLE","TGTYPESCREENFULL");

        TAP.$("ELE_LOGO_GIAS","//span[text()='GIAS']","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

        CALL.$("F024BenefitInquiryValidations","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_SEARCHTASKS","//input[@id='workbenchForm:workbenchTabs:taskSearchContainer:taskSearchSearch']","Policy Inquiry","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_TASKSEARCH","//a[@id='workbenchForm:workbenchTabs:taskSearchContainer:tb_buttonSearch']//span[@class='fa fa-search fa-lg']","TGTYPESCREENREG");

        TAP.$("ELE_LINK_POLICYINQUIRY","//span[contains(text(),'Policy Inquiry')]","TGTYPESCREENREG");

        WAIT.$(1,"TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_POLICYNUMBER","//input[@id='workbenchForm:workbenchTabs:policyNumber']","F024001","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_CONTINUE","//span[contains(text(),'Continue')]","TGTYPESCREENREG");

        WAIT.$(10,"TGTYPESCREENREG");

        SWIPE.$("UP","TGTYPESCREENREG");

        SWIPE.$("UP","TGTYPESCREENREG");

        TAP.$("ELE_LINK_BASEPLAN_ROW1","//tr[@class='ui-widget-content ui-datatable-even ui-datatable-selectable']","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

        ASSERT.$("ELE_GETVAL_SITUSSTATEFORTAXREPORTING","//label[@id='workbenchForm:workbenchTabs:countryAndStateCode_lbl']","VISIBLE","TGTYPESCREENFULL");

        TAP.$("ELE_LOGO_GIAS","//span[text()='GIAS']","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_SEARCHTASKS","//input[@id='workbenchForm:workbenchTabs:taskSearchContainer:taskSearchSearch']","Policy Inquiry","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_TASKSEARCH","//a[@id='workbenchForm:workbenchTabs:taskSearchContainer:tb_buttonSearch']//span[@class='fa fa-search fa-lg']","TGTYPESCREENREG");

        TAP.$("ELE_LINK_POLICYINQUIRY","//span[contains(text(),'Policy Inquiry')]","TGTYPESCREENREG");

        WAIT.$(1,"TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_POLICYNUMBER","//input[@id='workbenchForm:workbenchTabs:policyNumber']","F024002","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_CONTINUE","//span[contains(text(),'Continue')]","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        SWIPE.$("UP","TGTYPESCREENREG");

        SWIPE.$("UP","TGTYPESCREENREG");

        TAP.$("ELE_LINK_BASEPLAN_ROW1","//tr[@class='ui-widget-content ui-datatable-even ui-datatable-selectable']","TGTYPESCREENREG");

        ASSERT.$("ELE_GETVAL_SITUSSTATEFORTAXREPORTING","//label[@id='workbenchForm:workbenchTabs:countryAndStateCode_lbl']","INVISIBLE","TGTYPESCREENFULL");

        TAP.$("ELE_LOGO_GIAS","//span[text()='GIAS']","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

        CALL.$("ModF024ImportPlanValidation","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_SEARCHTASKS","//input[@id='workbenchForm:workbenchTabs:taskSearchContainer:taskSearchSearch']","Import Plan","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_TASKSEARCH","//a[@id='workbenchForm:workbenchTabs:taskSearchContainer:tb_buttonSearch']//span[@class='fa fa-search fa-lg']","TGTYPESCREENREG");

        TAP.$("ELE_LABEL_IMPORTPLAN","//span[contains(text(),'Import Plan')]","TGTYPESCREENREG");

        WAIT.$(1,"TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_SOURCEURL","//input[@id='workbenchForm:workbenchTabs:webServiceURL']","http://cis-gsstcd3.btoins.ibm.com:17000/ws","FALSE","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_SOURCEPLANCODE","//input[@id='workbenchForm:workbenchTabs:sourcePlanCode']","F024","FALSE","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_USER","//input[@id='workbenchForm:workbenchTabs:userID']","x5080","FALSE","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_PASSWORD_IMPORTPLAN","//input[@id='workbenchForm:workbenchTabs:password']","Welcome123$","FALSE","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_TARGETPLANCODE","//input[@id='workbenchForm:workbenchTabs:targetPlanCode']",var_TargetPlan,"FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SUBMIT","//span[contains(text(),'Submit')]","TGTYPESCREENFULL");

		var_Count = var_Count + ( 1 );
                 LOGGER.info("Executed Step = INCREASEVAR,var_Count,1,TGTYPESCREENREG");
    	}
          LOGGER.info("Executed Step = WEND,TGTYPESCREENREG");
    	resetAttemps();

    }


    @Test
    public void ftjmodf119part2() {

        // No need to declare / print method name here, handled by TestNg listener
        // declare("ftjmodf119part2");

		int var_Count = 0;
                 LOGGER.info("Executed Step = VAR,Integer,var_Count,0,TGTYPESCREENREG");
        LOGGER.info("Executed Step = REPEAT IF");
    	while (check(var_Count,"<",1,"TGTYPESCREENREG")) {
          LOGGER.info("Executed Step = WHILE,var_Count,<,1,TGTYPESCREENREG");
        CALL.$("FTJQA2Login","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

		try { 
		 WebElement html = driver.findElement(By.tagName("html"));
		    	
		    	System.out.println("html size" + html.getSize());
		    	System.out.println("html" + html.getTagName());

		    	for( int i =0 ;i<4;i++) {
		        html.sendKeys(Keys.chord(Keys.CONTROL, Keys.SUBTRACT));
		    	}
		 
		 ;} 
		 catch(Exception e) { PrintVal("Renderer Script Exception : "+e.toString()); } 
		 PrintVal("Executed Step = CALLRENDERERSCRIPT,FTJ41ZOOMMAGNIFICATIONCHANGE"); 
        TYPE.$("ELE_TXTBOX_USERNAME","//input[@id='guestForm:username']","x5080","FALSE","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_PASSWORD","//input[@id='guestForm:password']","Welcome123$","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_LOGIN","//button[@id='guestForm:loginButton']","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        CALL.$("ModF119Part2SelectAndSaveTheBatch","TGTYPESCREENREG");

        TAP.$("ELE_TXTBOX_SEARCHTASKS","//input[@id='workbenchForm:workbenchTabs:taskSearchContainer:taskSearchSearch']","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_SEARCHTASKS","//input[@id='workbenchForm:workbenchTabs:taskSearchContainer:taskSearchSearch']","Batch Policy Payment","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_TASKSEARCH","//a[@id='workbenchForm:workbenchTabs:taskSearchContainer:tb_buttonSearch']//span[@class='fa fa-search fa-lg']","TGTYPESCREENREG");

        WAIT.$(1,"TGTYPESCREENREG");

        TAP.$("ELE_LINK_BATCHPOLICYPAYMENT","//span[contains(text(),'Batch Policy Payment')]","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENFULL");

        CALL.$("ChangeCurrencyDrpDwnToDollarUS","TGTYPESCREENREG");

        TAP.$("ELE_DRPDWN_CURRENCY","//div[@class='ui-selectonemenu-trigger ui-state-default ui-corner-right']","TGTYPESCREENREG");

        TAP.$("ELE_DRPDWN_DOLLARUS","//li[@class='ui-selectonemenu-item ui-selectonemenu-list-item ui-corner-all'][contains(text(),'Dollar [US]')]","TGTYPESCREENREG");

        WAIT.$(4,"TGTYPESCREENREG");

        TAP.$("ELE_TXTBOX_BATCHNUMBER","//input[@name='workbenchForm:workbenchTabs:grid:suspenseControlNumb_Col:filter']","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_BATCHNUMBER","//input[@name='workbenchForm:workbenchTabs:grid:suspenseControlNumb_Col:filter']","11222","FALSE","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

        TAP.$("ELE_LINK_BATCHNUMBER_ROW1","//tbody[@id='workbenchForm:workbenchTabs:grid_data']//tr[1]","TGTYPESCREENREG");

        WAIT.$(1,"TGTYPESCREENREG");

        CALL.$("ModF119FieldEntriesForBatch","TGTYPESCREENREG");

        TAP.$("ELE_TXTBOX_COLLECTIONNUMBER_TAP_ROW1","//div[@class='ui-datatable ui-widget dynamicTable']//tr[1]//td[1]//div[1]//div[1]","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_COLLECTIONNUMBER_TYPE_ROW1","//input[@name='workbenchForm:workbenchTabs:grid_blc400s2:0:in_collectionNumber_Col']","F119001 0","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_TXTBOX_NAMECOLUMN_TAP_ROW1","//div[@class='ui-datatable ui-widget dynamicTable']//tr[1]//td[2]//div[1]//div[1]","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_NAME_TYPE_ROW1","//input[@name='workbenchForm:workbenchTabs:grid_blc400s2:0:in_ownerName_Col']","Mayberry","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_TXTBOX_AMOUNTRECEIVED_TAP_ROW1","//tr[1]//td[3]//div[1]//div[1]","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_AMOUNTRECEIVED_TYPE_ROW1","//input[@name='workbenchForm:workbenchTabs:grid_blc400s2:0:in_amountReceived_Col_input']","108.82","FALSE","TGTYPESCREENREG");

        SWIPE.$("UP","TGTYPESCREENREG");

        TAP.$("ELE_TXTBOX_COLLECTIONNUMBER_TAP_ROW2","//div[@class='ui-datatable ui-widget dynamicTable']//tr[2]//td[1]//div[1]//div[1]","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_COLLECTIONNUMBER_TYPE_ROW2","//input[@name='workbenchForm:workbenchTabs:grid_blc400s2:1:in_collectionNumber_Col']","F119002 0","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_TXTBOX_NAMECOLUMN_TAP_ROW2","//tr[2]//td[2]//div[1]//div[1]","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_NAME_TYPE_ROW2","//input[@name='workbenchForm:workbenchTabs:grid_blc400s2:1:in_ownerName_Col']","Smith","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_TXTBOX_AMOUNTRECEIVED_TAP_ROW2","//tr[2]//td[3]//div[1]//div[1]","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_AMOUNTRECEIVED_TYPE_ROW2","//input[@name='workbenchForm:workbenchTabs:grid_blc400s2:1:in_amountReceived_Col_input']","1300.00","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_TXTBOX_COLLECTIONNUMBER_TAP_ROW3","//tr[3]//td[1]//div[1]//div[1]","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_COLLECTIONNUMBER_TYPE_ROW3","//input[@name='workbenchForm:workbenchTabs:grid_blc400s2:2:in_collectionNumber_Col']","F119003 0","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_TXTBOX_NAMECOLUMN_TAP_ROW3","//tr[3]//td[2]//div[1]//div[1]","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_NAME_TYPE_ROW3","//input[@name='workbenchForm:workbenchTabs:grid_blc400s2:2:in_ownerName_Col']","Jones","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_TXTBOX_AMOUNTRECEIVED_TAP_ROW3","//tr[3]//td[3]//div[1]//div[1]","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_AMOUNTRECEIVED_TYPE_ROW3","//input[@name='workbenchForm:workbenchTabs:grid_blc400s2:2:in_amountReceived_Col_input']","124.97","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_TXTBOX_COLLECTIONNUMBER_TAP_ROW4","//tr[4]//td[1]//div[1]//div[1]","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_COLLECTIONNUMBER_TYPE_ROW4","//input[@name='workbenchForm:workbenchTabs:grid_blc400s2:3:in_collectionNumber_Col']","F199004 0","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_TXTBOX_NAMECOLUMN_TAP_ROW4","//tr[4]//td[2]//div[1]//div[1]","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_NAME_TYPE_ROW4","//input[@name='workbenchForm:workbenchTabs:grid_blc400s2:3:in_ownerName_Col']","Jackson","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_TXTBOX_AMOUNTRECEIVED_TAP_ROW4","//tr[4]//td[3]//div[1]//div[1]","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_AMOUNTRECEIVED_TYPE_ROW4","//input[@name='workbenchForm:workbenchTabs:grid_blc400s2:3:in_amountReceived_Col_input']","6700.00","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_TXTBOX_COLLECTIONNUMBER_TAP_ROW5","//div[@class='ui-datatable ui-widget dynamicTable']//tr[5]//td[1]//div[1]//div[1]","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_COLLECTIONNUMBER_TYPE_ROW5","//input[@name='workbenchForm:workbenchTabs:grid_blc400s2:4:in_collectionNumber_Col']","F119005 0","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_TXTBOX_NAMECOLUMN_TAP_ROW5","//div[@class='ui-datatable ui-widget dynamicTable']//tr[5]//td[2]//div[1]//div[1]","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_NAME_TYPE_ROW5","//input[@name='workbenchForm:workbenchTabs:grid_blc400s2:4:in_ownerName_Col']","Sand","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_TXTBOX_AMOUNTRECEIVED_TAP_ROW5","//tr[5]//td[3]//div[1]//div[1]","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_AMOUNTRECEIVED_TYPE_ROW5","//input[@name='workbenchForm:workbenchTabs:grid_blc400s2:4:in_amountReceived_Col_input']","64.44","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_TXTBOX_COLLECTIONNUMBER_TAP_ROW6","//tr[6]//td[1]//div[1]//div[1]","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_VALIDATE","//span[contains(text(),'Validate')]","TGTYPESCREENREG");

        WAIT.$(4,"TGTYPESCREENREG");

		String var_BatchTotalEntered;
                 LOGGER.info("Executed Step = VAR,String,var_BatchTotalEntered,TGTYPESCREENREG");
		var_BatchTotalEntered = ActionWrapper.getElementValue("ELE_GETVAL_BATCHTOTALENTERED", "//span[@id='workbenchForm:workbenchTabs:totalEntered']");
                 LOGGER.info("Executed Step = STORE,var_BatchTotalEntered,ELE_GETVAL_BATCHTOTALENTERED,TGTYPESCREENREG");
        TAP.$("ELE_BUTTON_SAVE","//span[contains(text(),'Save')]","TGTYPESCREENFULL");

        WAIT.$(5,"TGTYPESCREENREG");

        CALL.$("ModF119SavedBatchAndErrorValidations","TGTYPESCREENREG");

        CALL.$("ChangeCurrencyDrpDwnToDollarUS","TGTYPESCREENREG");

        TAP.$("ELE_DRPDWN_CURRENCY","//div[@class='ui-selectonemenu-trigger ui-state-default ui-corner-right']","TGTYPESCREENREG");

        TAP.$("ELE_DRPDWN_DOLLARUS","//li[@class='ui-selectonemenu-item ui-selectonemenu-list-item ui-corner-all'][contains(text(),'Dollar [US]')]","TGTYPESCREENREG");

        WAIT.$(4,"TGTYPESCREENREG");

        TAP.$("ELE_TXTBOX_BATCHNUMBER","//input[@name='workbenchForm:workbenchTabs:grid:suspenseControlNumb_Col:filter']","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_BATCHNUMBER","//input[@name='workbenchForm:workbenchTabs:grid:suspenseControlNumb_Col:filter']","11222","FALSE","TGTYPESCREENREG");

        WAIT.$(1,"TGTYPESCREENREG");

        ASSERT.$("ELE_GETVAL_SAVEPAYMENT_YES","//span[@class='OutputGeneral'][contains(text(),'Yes')]","VISIBLE","TGTYPESCREENREG");

        TAP.$("ELE_LINK_BATCHNUMBER_ROW1","//tbody[@id='workbenchForm:workbenchTabs:grid_data']//tr[1]","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

        ASSERT.$("ELE_BUTTON_SAVE","//span[contains(text(),'Save')]","VISIBLE","TGTYPESCREENREG");

        ASSERT.$("ELE_BUTTON_DELETE","//span[contains(text(),'Delete')]","VISIBLE","TGTYPESCREENREG");

        ASSERT.$("ELE_GETVAL_BATCHTOTALENTERED","//span[@id='workbenchForm:workbenchTabs:totalEntered']","=",var_BatchTotalEntered,"TGTYPESCREENREG");

        TAP.$("ELE_TXTBOX_AMOUNTRECEIVED_TAP_ROW4","//tr[4]//td[3]//div[1]//div[1]","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_AMOUNTRECEIVED_TYPE_ROW4","//input[@name='workbenchForm:workbenchTabs:grid_blc400s2:3:in_amountReceived_Col_input']","0","FALSE","TGTYPESCREENREG");

        SWIPE.$("UP","TGTYPESCREENREG");

        TAP.$("ELE_TXTBOX_AMOUNTRECEIVED_TAP_ROW5","//tr[5]//td[3]//div[1]//div[1]","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_AMOUNTRECEIVED_TYPE_ROW5","//input[@name='workbenchForm:workbenchTabs:grid_blc400s2:4:in_amountReceived_Col_input']","55","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_TXTBOX_COLLECTIONNUMBER_TAP_ROW6","//tr[6]//td[1]//div[1]//div[1]","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_VALIDATE","//span[contains(text(),'Validate')]","TGTYPESCREENREG");

        WAIT.$(25,"TGTYPESCREENREG");

        SWIPE.$("UP","TGTYPESCREENREG");

        SWIPE.$("LEFT","TGTYPESCREENREG");

        ASSERT.$("ELE_GETMSG_ERROR","//div[@id='workbenchForm:workbenchTabs:messages']//span[@class='ui-messages-error-summary']","VISIBLE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_DISPLAYMESSAGE_ROW4","//button[@id='workbenchForm:workbenchTabs:grid_blc400s2:3:msgbox_button']","TGTYPESCREENREG");

        WAIT.$(3,"TGTYPESCREENREG");

        ASSERT.$("ELE_GETMSG_DISPLAYMESSAGES","//div[@id='workbenchForm:workbenchTabs:messagesInDialog']//span[@class='ui-messages-error-summary']","CONTAINS","Collection number or amount received is missing.","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_OK","//span[contains(text(),'OK')]","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_DISPLAYMESSAGE_ROW5","//button[@id='workbenchForm:workbenchTabs:grid_blc400s2:4:msgbox_button']","TGTYPESCREENREG");

        WAIT.$(3,"TGTYPESCREENREG");

        ASSERT.$("ELE_GETVAL_ERRORMSG_OUTSIDEOVERSHORTLIMIT","//div[@id='workbenchForm:workbenchTabs:messagesInDialog']//div//span[contains(text(),'Outside over')]","VISIBLE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_OK","//span[contains(text(),'OK')]","TGTYPESCREENREG");

		String var_BatchTotalEntered2;
                 LOGGER.info("Executed Step = VAR,String,var_BatchTotalEntered2,TGTYPESCREENREG");
		var_BatchTotalEntered2 = ActionWrapper.getElementValue("ELE_GETVAL_BATCHTOTALENTERED", "//span[@id='workbenchForm:workbenchTabs:totalEntered']");
                 LOGGER.info("Executed Step = STORE,var_BatchTotalEntered2,ELE_GETVAL_BATCHTOTALENTERED,TGTYPESCREENREG");
        TAP.$("ELE_BUTTON_SAVE","//span[contains(text(),'Save')]","TGTYPESCREENFULL");

        WAIT.$(5,"TGTYPESCREENREG");

        CALL.$("ModF119ContinuedChecksAndDeletionValidations","TGTYPESCREENREG");

        CALL.$("ChangeCurrencyDrpDwnToDollarUS","TGTYPESCREENREG");

        TAP.$("ELE_DRPDWN_CURRENCY","//div[@class='ui-selectonemenu-trigger ui-state-default ui-corner-right']","TGTYPESCREENREG");

        TAP.$("ELE_DRPDWN_DOLLARUS","//li[@class='ui-selectonemenu-item ui-selectonemenu-list-item ui-corner-all'][contains(text(),'Dollar [US]')]","TGTYPESCREENREG");

        WAIT.$(4,"TGTYPESCREENREG");

        TAP.$("ELE_TXTBOX_BATCHNUMBER","//input[@name='workbenchForm:workbenchTabs:grid:suspenseControlNumb_Col:filter']","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_BATCHNUMBER","//input[@name='workbenchForm:workbenchTabs:grid:suspenseControlNumb_Col:filter']","11222","FALSE","TGTYPESCREENREG");

        WAIT.$(1,"TGTYPESCREENREG");

        TAP.$("ELE_LINK_BATCHNUMBER_ROW1","//tbody[@id='workbenchForm:workbenchTabs:grid_data']//tr[1]","TGTYPESCREENREG");

        ASSERT.$("ELE_GETMSG_ERROR","//div[@id='workbenchForm:workbenchTabs:messages']//span[@class='ui-messages-error-summary']","INVISIBLE","TGTYPESCREENREG");

        ASSERT.$("ELE_BUTTON_DISPLAYMESSAGE_ROW4","//button[@id='workbenchForm:workbenchTabs:grid_blc400s2:3:msgbox_button']","INVISIBLE","TGTYPESCREENREG");

        ASSERT.$("ELE_BUTTON_DISPLAYMESSAGE_ROW5","//button[@id='workbenchForm:workbenchTabs:grid_blc400s2:4:msgbox_button']","INVISIBLE","TGTYPESCREENREG");

        ASSERT.$("ELE_GETVAL_BATCHTOTALENTERED","//span[@id='workbenchForm:workbenchTabs:totalEntered']","=",var_BatchTotalEntered2,"TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_DELETE","//span[contains(text(),'Delete')]","TGTYPESCREENREG");

        WAIT.$(1,"TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_CONFIRMDELETE","//span[contains(text(),'Confirm Delete')]","TGTYPESCREENREG");

        WAIT.$(3,"TGTYPESCREENREG");

        ASSERT.$("ELE_GETMSG_ITEMSUCCESSFULLYDELETED","//div[@id='workbenchForm:workbenchTabs:messages']//span[@class='ui-messages-info-summary'][contains(text(),'Item successfully deleted.')]","VISIBLE","TGTYPESCREENREG");

        ASSERT.$("ELE_GETVAL_SUSPENSEAMOUNTROWS","//div[@id='ContentPanel']//tr[1]//td[3]","<>","0.00","TGTYPESCREENREG");

        CALL.$("ChangeCurrencyDrpDwnToDollarUS","TGTYPESCREENREG");

        TAP.$("ELE_DRPDWN_CURRENCY","//div[@class='ui-selectonemenu-trigger ui-state-default ui-corner-right']","TGTYPESCREENREG");

        TAP.$("ELE_DRPDWN_DOLLARUS","//li[@class='ui-selectonemenu-item ui-selectonemenu-list-item ui-corner-all'][contains(text(),'Dollar [US]')]","TGTYPESCREENREG");

        WAIT.$(4,"TGTYPESCREENREG");

        TAP.$("ELE_TXTBOX_BATCHNUMBER","//input[@name='workbenchForm:workbenchTabs:grid:suspenseControlNumb_Col:filter']","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_BATCHNUMBER","//input[@name='workbenchForm:workbenchTabs:grid:suspenseControlNumb_Col:filter']","11222","FALSE","TGTYPESCREENREG");

        WAIT.$(3,"TGTYPESCREENREG");

        ASSERT.$("ELE_GETVAL_SAVEPAYMENT_YES","//span[@class='OutputGeneral'][contains(text(),'Yes')]","INVISIBLE","TGTYPESCREENREG");

		var_Count = var_Count + ( 1 );
                 LOGGER.info("Executed Step = INCREASEVAR,var_Count,1,TGTYPESCREENREG");
    	}
          LOGGER.info("Executed Step = WEND,TGTYPESCREENREG");
    	resetAttemps();

    }


    @Test
    public void ftjmodf170() {

        // No need to declare / print method name here, handled by TestNg listener
        // declare("ftjmodf170");

		int var_Count = 0;
                 LOGGER.info("Executed Step = VAR,Integer,var_Count,0,TGTYPESCREENREG");
		com.jayway.jsonpath.ReadContext var_CSVData = getJsonData("http://10.1.104.229/json/20200818/8Vr4pf.json");
                 LOGGER.info("Executed Step = VAR,Json,var_CSVData,http://10.1.104.229/json/20200818/8Vr4pf.json,TGTYPESCREENREG");
        LOGGER.info("Executed Step = REPEAT IF");
    	while (check(var_Count,"<",1,"TGTYPESCREENREG")) {
          LOGGER.info("Executed Step = WHILE,var_Count,<,1,TGTYPESCREENREG");
		String var_AgentNumber;
                 LOGGER.info("Executed Step = VAR,String,var_AgentNumber,TGTYPESCREENREG");
		var_AgentNumber = getJsonData(var_CSVData , "$.records["+var_Count+"].AgentNumber");
                 LOGGER.info("Executed Step = STORE,var_AgentNumber,var_CSVData,$.records[{var_Count}].AgentNumber,TGTYPESCREENREG");
        CALL.$("FTJQA2Login","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

		try { 
		 WebElement html = driver.findElement(By.tagName("html"));
		    	
		    	System.out.println("html size" + html.getSize());
		    	System.out.println("html" + html.getTagName());

		    	for( int i =0 ;i<4;i++) {
		        html.sendKeys(Keys.chord(Keys.CONTROL, Keys.SUBTRACT));
		    	}
		 
		 ;} 
		 catch(Exception e) { PrintVal("Renderer Script Exception : "+e.toString()); } 
		 PrintVal("Executed Step = CALLRENDERERSCRIPT,FTJ41ZOOMMAGNIFICATIONCHANGE"); 
        TYPE.$("ELE_TXTBOX_USERNAME","//input[@id='guestForm:username']","x5080","FALSE","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_PASSWORD","//input[@id='guestForm:password']","Welcome123$","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_LOGIN","//button[@id='guestForm:loginButton']","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        CALL.$("F170PlanValidations","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_SEARCHTASKS","//input[@id='workbenchForm:workbenchTabs:taskSearchContainer:taskSearchSearch']","Plan Setup","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_TASKSEARCH","//a[@id='workbenchForm:workbenchTabs:taskSearchContainer:tb_buttonSearch']//span[@class='fa fa-search fa-lg']","TGTYPESCREENREG");

        TAP.$("ELE_LINK_PLANSETUP","//span[contains(text(),'Plan Setup')]","TGTYPESCREENREG");

        WAIT.$(3,"TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_PLAN","//input[@id='workbenchForm:workbenchTabs:grid:planCode_Col:filter']","OLUSDA","FALSE","TGTYPESCREENREG");

        WAIT.$(3,"TGTYPESCREENREG");

        TAP.$("ELE_LINK_PLAN_ROW1","//div[@class='ui-outputpanel ui-widget']//td[1]","TGTYPESCREENREG");

        WAIT.$(3,"TGTYPESCREENREG");

        TAP.$("ELE_TAB_COMMISSIONS","//span[contains(text(),'Commissions')]","TGTYPESCREENREG");

        WAIT.$(1,"TGTYPESCREENFULL");

        SWIPE.$("UP","TGTYPESCREENREG");

        SWIPE.$("UP","TGTYPESCREENREG");

        ASSERT.$("ELE_GETVAL_MAXIMUMAGENTADVANCEAMNT_UNDERCALC","//label[@id='workbenchForm:workbenchTabs:maxAgtAdvance_lbl']","VISIBLE","TGTYPESCREENREG");

        LOGGER.info("Executed Step = START IF");
        if (check("ELE_GETVAL_MAXIMUMAGENTADVANCEAMNT","//span[@id='workbenchForm:workbenchTabs:maxAgtAdvance']","=",25,"TGTYPESCREENREG")) {
         LOGGER.info("Executed Step = IF,ELE_GETVAL_MAXIMUMAGENTADVANCEAMNT,=,25,TGTYPESCREENREG");
        CALL.$("F170MaximumAgentAdvanceAmountTo5","TGTYPESCREENREG");

        SWIPE.$("DOWN","TGTYPESCREENREG");

        SWIPE.$("DOWN","TGTYPESCREENREG");

        TAP.$("ELE_TAB_BASICINSURANCEINFORMATION","//span[contains(text(),'Basic Insurance Information')]","TGTYPESCREENREG");

        WAIT.$(1,"TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_EDIT","//span[text()='Edit']","TGTYPESCREENREG");

        WAIT.$(4,"TGTYPESCREENREG");

        TAP.$("ELE_TAB_COMMISSIONS","//span[contains(text(),'Commissions')]","TGTYPESCREENREG");

        WAIT.$(1,"TGTYPESCREENREG");

        TAP.$("ELE_TXTBOX_MAXIMUMAGENTADVANCEAMOUNT","//input[@id='workbenchForm:workbenchTabs:maxAgtAdvance_input']","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_MAXIMUMAGENTADVANCEAMOUNT","//input[@id='workbenchForm:workbenchTabs:maxAgtAdvance_input']","5","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_VALIDATE","//span[contains(text(),'Validate')]","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

        ASSERT.$("ELE_MSG_VALIDATIONSUCCESS","//div[@id='workbenchForm:workbenchTabs:messages']//span[@class='ui-messages-info-summary'][contains(text(),'Validation completed successfully with no errors.')]","VISIBLE","TGTYPESCREENREG");

        SWIPE.$("DOWN","TGTYPESCREENREG");

        SWIPE.$("DOWN","TGTYPESCREENREG");

        TAP.$("ELE_TAB_BASICINSURANCEINFORMATION","//span[contains(text(),'Basic Insurance Information')]","TGTYPESCREENREG");

        WAIT.$(1,"TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SAVE","//span[contains(text(),'Save')]","TGTYPESCREENREG");

        WAIT.$(7,"TGTYPESCREENREG");

        ASSERT.$("ELE_MSG_ITEMSUCCESSUPDATE","//div[@id='workbenchForm:workbenchTabs:messages']//span[@class='ui-messages-info-summary'][contains(text(),'Item successfully updated.')]","VISIBLE","TGTYPESCREENREG");

        TAP.$("ELE_TAB_COMMISSIONS","//span[contains(text(),'Commissions')]","TGTYPESCREENREG");

        ASSERT.$("ELE_GETVAL_MAXIMUMAGENTADVANCEAMNT","//span[@id='workbenchForm:workbenchTabs:maxAgtAdvance']","CONTAINS",5,"TGTYPESCREENFULL");

        TAP.$("ELE_LOGO_GIAS","//span[text()='GIAS']","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

        }
          LOGGER.info("Executed Step = ENDIF,TGTYPESCREENREG");
        LOGGER.info("Executed Step = START IF");
        if (check("ELE_GETVAL_MAXIMUMAGENTADVANCEAMNT","//span[@id='workbenchForm:workbenchTabs:maxAgtAdvance']","=",5,"TGTYPESCREENREG")) {
         LOGGER.info("Executed Step = IF,ELE_GETVAL_MAXIMUMAGENTADVANCEAMNT,=,5,TGTYPESCREENREG");
        CALL.$("F170MaximumAgentAdvanceAmountTo25","TGTYPESCREENREG");

        SWIPE.$("DOWN","TGTYPESCREENREG");

        SWIPE.$("DOWN","TGTYPESCREENREG");

        TAP.$("ELE_TAB_BASICINSURANCEINFORMATION","//span[contains(text(),'Basic Insurance Information')]","TGTYPESCREENREG");

        WAIT.$(1,"TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_EDIT","//span[text()='Edit']","TGTYPESCREENREG");

        WAIT.$(4,"TGTYPESCREENREG");

        TAP.$("ELE_TAB_COMMISSIONS","//span[contains(text(),'Commissions')]","TGTYPESCREENREG");

        WAIT.$(1,"TGTYPESCREENREG");

        TAP.$("ELE_TXTBOX_MAXIMUMAGENTADVANCEAMOUNT","//input[@id='workbenchForm:workbenchTabs:maxAgtAdvance_input']","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_MAXIMUMAGENTADVANCEAMOUNT","//input[@id='workbenchForm:workbenchTabs:maxAgtAdvance_input']","25","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_VALIDATE","//span[contains(text(),'Validate')]","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

        ASSERT.$("ELE_MSG_VALIDATIONSUCCESS","//div[@id='workbenchForm:workbenchTabs:messages']//span[@class='ui-messages-info-summary'][contains(text(),'Validation completed successfully with no errors.')]","VISIBLE","TGTYPESCREENREG");

        SWIPE.$("DOWN","TGTYPESCREENREG");

        SWIPE.$("DOWN","TGTYPESCREENREG");

        TAP.$("ELE_TAB_BASICINSURANCEINFORMATION","//span[contains(text(),'Basic Insurance Information')]","TGTYPESCREENREG");

        WAIT.$(3,"TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SAVE","//span[contains(text(),'Save')]","TGTYPESCREENREG");

        WAIT.$(25,"TGTYPESCREENREG");

        ASSERT.$("ELE_MSG_ITEMSUCCESSUPDATE","//div[@id='workbenchForm:workbenchTabs:messages']//span[@class='ui-messages-info-summary'][contains(text(),'Item successfully updated.')]","VISIBLE","TGTYPESCREENREG");

        TAP.$("ELE_TAB_COMMISSIONS","//span[contains(text(),'Commissions')]","TGTYPESCREENREG");

        ASSERT.$("ELE_GETVAL_MAXIMUMAGENTADVANCEAMNT","//span[@id='workbenchForm:workbenchTabs:maxAgtAdvance']","CONTAINS",25,"TGTYPESCREENFULL");

        TAP.$("ELE_LOGO_GIAS","//span[text()='GIAS']","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

        }
          LOGGER.info("Executed Step = ENDIF,TGTYPESCREENREG");
        CALL.$("F170AgentValidations","TGTYPESCREENREG");

        SWIPE.$("DOWN","TGTYPESCREENREG");

        SWIPE.$("DOWN","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_SEARCHTASKS","//input[@id='workbenchForm:workbenchTabs:taskSearchContainer:taskSearchSearch']","Agents","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_TASKSEARCH","//a[@id='workbenchForm:workbenchTabs:taskSearchContainer:tb_buttonSearch']//span[@class='fa fa-search fa-lg']","TGTYPESCREENREG");

        WAIT.$(1,"TGTYPESCREENREG");

        TAP.$("ELE_LINK_AGENTS","//span[text()='Agents']","TGTYPESCREENREG");

        WAIT.$(1,"TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_AGENTNUMBER","//input[@name='workbenchForm:workbenchTabs:grid1:agentNumber_Col:filter']",var_AgentNumber,"FALSE","TGTYPESCREENREG");

        WAIT.$(3,"TGTYPESCREENREG");

        TAP.$("ELE_LINK_AGENTNUMBER_ROW1","//tbody[@class='ui-datatable-data ui-widget-content']//tr[1]","TGTYPESCREENREG");

        WAIT.$(3,"TGTYPESCREENREG");

        ASSERT.$("ELE_GETVAL_MINIMUMCHECKAMOUNT","//label[@id='workbenchForm:workbenchTabs:agtMinChkAmt_lbl']","VISIBLE","TGTYPESCREENFULL");

        ASSERT.$("ELE_GETVAL_MINIMUMDIRECTDEPOSITAMOUNT","//label[@id='workbenchForm:workbenchTabs:agtMinDdAmt_lbl']","VISIBLE","TGTYPESCREENFULL");

        LOGGER.info("Executed Step = START IF");
        if (check("ELE_GETVAL_MINIMUMCHECKAMNT_AGENTPAGE","//span[@id='workbenchForm:workbenchTabs:agtMinChkAmt']","=","25.00","TGTYPESCREENREG")) {
         LOGGER.info("Executed Step = IF,ELE_GETVAL_MINIMUMCHECKAMNT_AGENTPAGE,=,25.00,TGTYPESCREENREG");
        TAP.$("ELE_BUTTON_EDIT","//span[text()='Edit']","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_MINIMUMCHECKAMOUNT","//input[@id='workbenchForm:workbenchTabs:agtMinChkAmt_input']","50.00","FALSE","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_MINIMUMDIRECTDEPOSITAMOUNT","//input[@id='workbenchForm:workbenchTabs:agtMinDdAmt_input']","10.00","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SAVE","//span[contains(text(),'Save')]","TGTYPESCREENREG");

        }
          LOGGER.info("Executed Step = ENDIF,TGTYPESCREENREG");
        LOGGER.info("Executed Step = START IF");
        if (check("ELE_GETVAL_MINIMUMCHECKAMNT_AGENTPAGE","//span[@id='workbenchForm:workbenchTabs:agtMinChkAmt']","=","50.00","TGTYPESCREENREG")) {
         LOGGER.info("Executed Step = IF,ELE_GETVAL_MINIMUMCHECKAMNT_AGENTPAGE,=,50.00,TGTYPESCREENREG");
        TAP.$("ELE_BUTTON_EDIT","//span[text()='Edit']","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_MINIMUMCHECKAMOUNT","//input[@id='workbenchForm:workbenchTabs:agtMinChkAmt_input']","25.00","FALSE","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_MINIMUMDIRECTDEPOSITAMOUNT","//input[@id='workbenchForm:workbenchTabs:agtMinDdAmt_input']","5.00","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SAVE","//span[contains(text(),'Save')]","TGTYPESCREENFULL");

        }
          LOGGER.info("Executed Step = ENDIF,TGTYPESCREENREG");
        WAIT.$(2,"TGTYPESCREENREG");

        ASSERT.$("ELE_MSG_ITEMSUCCESSUPDATE","//div[@id='workbenchForm:workbenchTabs:messages']//span[@class='ui-messages-info-summary'][contains(text(),'Item successfully updated.')]","VISIBLE","TGTYPESCREENFULL");

        TAP.$("ELE_LOGO_GIAS","//span[text()='GIAS']","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

        CALL.$("F170MiscellaneousAmountPropertiesAddModeChecks","TGTYPESCREENREG");

        SWIPE.$("DOWN","TGTYPESCREENREG");

        SWIPE.$("DOWN","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_SEARCHTASKS","//input[@id='workbenchForm:workbenchTabs:taskSearchContainer:taskSearchSearch']","LSP026","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_TASKSEARCH","//a[@id='workbenchForm:workbenchTabs:taskSearchContainer:tb_buttonSearch']//span[@class='fa fa-search fa-lg']","TGTYPESCREENREG");

        TAP.$("ELE_LABEL_MISCELLANEOUSAMOUNTPROPERTIES","//span[contains(text(),'Miscellaneous Amount Properties')]","TGTYPESCREENREG");

        WAIT.$(1,"TGTYPESCREENFULL");

        SWIPE.$("DOWN","TGTYPESCREENREG");

        TAP.$("ELE_DRPDWN_DESIGNATEDCURRENCY","//div[@id='workbenchForm:workbenchTabs:grid:currencyCode_filter']//div//span","TGTYPESCREENREG");

        TAP.$("ELE_DRPDWN_DOLLARUS","//li[@class='ui-selectonemenu-item ui-selectonemenu-list-item ui-corner-all'][contains(text(),'Dollar [US]')]","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

        ASSERT.$("ELE_GETVAL_MINIMUMAGENTCHECAMNT_EQUALS_25","//span[contains(text(),'25.00')]","VISIBLE","TGTYPESCREENREG");

        ASSERT.$("ELE_GETVAL_MINIMUMAGENTDIRECTDEPOSITAMNT_EQUALS_5","//span[@id='workbenchForm:workbenchTabs:grid:0:minCommEftCheckAmount']","VISIBLE","TGTYPESCREENREG");

        TAP.$("ELE_LINK_POLICYNUMBER_ROW1","//tr[@class='ui-widget-content ui-datatable-even ui-datatable-selectable']","TGTYPESCREENREG");

        ASSERT.$("ELE_GETVAL_MINIMUMAGENTCHECKAMNT_MISCPAGE","//span[@id='workbenchForm:workbenchTabs:minCommCheckAmt']","=","25.00","TGTYPESCREENFULL");

        ASSERT.$("ELE_GETVAL_MINIMUMAGENTDIRECTDEPOSITAMNT_MISCPAGE","//span[@id='workbenchForm:workbenchTabs:minCommEftCheckAmount']","=","5.00","TGTYPESCREENFULL");

        TAP.$("ELE_LOGO_GIAS","//span[text()='GIAS']","TGTYPESCREENREG");

        WAIT.$(1,"TGTYPESCREENREG");

        SWIPE.$("DOWN","TGTYPESCREENREG");

		var_Count = var_Count + ( 1 );
                 LOGGER.info("Executed Step = INCREASEVAR,var_Count,1,TGTYPESCREENREG");
    	}
          LOGGER.info("Executed Step = WEND,TGTYPESCREENREG");
    	resetAttemps();

    }


    @Test
    public void f237workingscript() {

        // No need to declare / print method name here, handled by TestNg listener
        // declare("f237workingscript");

		int var_Count = 0;
                 LOGGER.info("Executed Step = VAR,Integer,var_Count,0,TGTYPESCREENREG");
		com.jayway.jsonpath.ReadContext var_CSVData = getJsonData("http://10.1.104.229/json/20201009/DPsDMk.json");
                 LOGGER.info("Executed Step = VAR,Json,var_CSVData,http://10.1.104.229/json/20201009/DPsDMk.json,TGTYPESCREENREG");
        LOGGER.info("Executed Step = REPEAT IF");
    	while (check(var_Count,"<",1,"TGTYPESCREENREG")) {
          LOGGER.info("Executed Step = WHILE,var_Count,<,1,TGTYPESCREENREG");
		String var_FundWcoverageValue;
                 LOGGER.info("Executed Step = VAR,String,var_FundWcoverageValue,TGTYPESCREENREG");
		var_FundWcoverageValue = getJsonData(var_CSVData , "$.records["+var_Count+"].FundWcoverageValue");
                 LOGGER.info("Executed Step = STORE,var_FundWcoverageValue,var_CSVData,$.records[{var_Count}].FundWcoverageValue,TGTYPESCREENREG");
		String var_FundWOcoverageValue;
                 LOGGER.info("Executed Step = VAR,String,var_FundWOcoverageValue,TGTYPESCREENREG");
		var_FundWOcoverageValue = getJsonData(var_CSVData , "$.records["+var_Count+"].FundWOcoverageValue");
                 LOGGER.info("Executed Step = STORE,var_FundWOcoverageValue,var_CSVData,$.records[{var_Count}].FundWOcoverageValue,TGTYPESCREENREG");
		String var_Table;
                 LOGGER.info("Executed Step = VAR,String,var_Table,TGTYPESCREENREG");
		var_Table = getJsonData(var_CSVData , "$.records["+var_Count+"].Table");
                 LOGGER.info("Executed Step = STORE,var_Table,var_CSVData,$.records[{var_Count}].Table,TGTYPESCREENREG");
		String var_EndingDuration;
                 LOGGER.info("Executed Step = VAR,String,var_EndingDuration,TGTYPESCREENREG");
		var_EndingDuration = getJsonData(var_CSVData , "$.records["+var_Count+"].EndingDuration");
                 LOGGER.info("Executed Step = STORE,var_EndingDuration,var_CSVData,$.records[{var_Count}].EndingDuration,TGTYPESCREENREG");
		String var_EffectiveDate;
                 LOGGER.info("Executed Step = VAR,String,var_EffectiveDate,TGTYPESCREENREG");
		var_EffectiveDate = getJsonData(var_CSVData , "$.records["+var_Count+"].EffectiveDate");
                 LOGGER.info("Executed Step = STORE,var_EffectiveDate,var_CSVData,$.records[{var_Count}].EffectiveDate,TGTYPESCREENREG");
		String var_AnnualInterestRate;
                 LOGGER.info("Executed Step = VAR,String,var_AnnualInterestRate,TGTYPESCREENREG");
		var_AnnualInterestRate = getJsonData(var_CSVData , "$.records["+var_Count+"].AnnualInterestRate");
                 LOGGER.info("Executed Step = STORE,var_AnnualInterestRate,var_CSVData,$.records[{var_Count}].AnnualInterestRate,TGTYPESCREENREG");
        CALL.$("FTJQA2Login","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

		try { 
		 WebElement html = driver.findElement(By.tagName("html"));
		    	
		    	System.out.println("html size" + html.getSize());
		    	System.out.println("html" + html.getTagName());

		    	for( int i =0 ;i<4;i++) {
		        html.sendKeys(Keys.chord(Keys.CONTROL, Keys.SUBTRACT));
		    	}
		 
		 ;} 
		 catch(Exception e) { PrintVal("Renderer Script Exception : "+e.toString()); } 
		 PrintVal("Executed Step = CALLRENDERERSCRIPT,FTJ41ZOOMMAGNIFICATIONCHANGE"); 
        TYPE.$("ELE_TXTBOX_USERNAME","//input[@id='guestForm:username']","x5080","FALSE","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_PASSWORD","//input[@id='guestForm:password']","Welcome123$","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_LOGIN","//button[@id='guestForm:loginButton']","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        CALL.$("ModF237BB577FundScreenVal","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_SEARCHTASKS","//input[@id='workbenchForm:workbenchTabs:taskSearchContainer:taskSearchSearch']","Fund Setup","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_TASKSEARCH","//a[@id='workbenchForm:workbenchTabs:taskSearchContainer:tb_buttonSearch']//span[@class='fa fa-search fa-lg']","TGTYPESCREENREG");

        WAIT.$(1,"TGTYPESCREENREG");

        TAP.$("ELE_LABEL_FUNDSETUP","//span[contains(text(),'Fund Setup')]","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_FUND","//input[@id='workbenchForm:workbenchTabs:grid:fundCode_Col:filter']",var_FundWcoverageValue,"FALSE","TGTYPESCREENREG");

        WAIT.$(1,"TGTYPESCREENREG");

        TAP.$("ELE_LINK_ROW1","//tr[@class='ui-widget-content ui-datatable-even ui-datatable-selectable']","TGTYPESCREENREG");

        WAIT.$(3,"TGTYPESCREENFULL");

        CALL.$("FundBalanceSegmentationCheck","TGTYPESCREENREG");

        LOGGER.info("Executed Step = START IF");
        if (check("ELE_GETVAL_FUNDBALANCESEGMENTATION","//span[@id='workbenchForm:workbenchTabs:fundBalanceSegmentation']","=","Single","TGTYPESCREENREG")) {
         LOGGER.info("Executed Step = IF,ELE_GETVAL_FUNDBALANCESEGMENTATION,=,Single,TGTYPESCREENREG");
        ASSERT.$("ELE_GETVAL_COVERAGEVALUEINTERESTRATEADJUSTMENT","//label[@id='workbenchForm:workbenchTabs:interestCoverageValIntRtAdj_lbl']","VISIBLE","TGTYPESCREENFULL");

        }
          LOGGER.info("Executed Step = ENDIF,TGTYPESCREENREG");
        LOGGER.info("Executed Step = START IF");
        if (check("ELE_GETVAL_FUNDBALANCESEGMENTATION","//span[@id='workbenchForm:workbenchTabs:fundBalanceSegmentation']","<>","Single","TGTYPESCREENREG")) {
         LOGGER.info("Executed Step = IF,ELE_GETVAL_FUNDBALANCESEGMENTATION,<>,Single,TGTYPESCREENREG");
        ASSERT.$("ELE_GETVAL_COVERAGEVALUEINTERESTRATEADJUSTMENT","//label[@id='workbenchForm:workbenchTabs:interestCoverageValIntRtAdj_lbl']","INVISIBLE","TGTYPESCREENFULL");

        }
          LOGGER.info("Executed Step = ENDIF,TGTYPESCREENREG");
        TAP.$("ELE_TAB_CLOSE3","//li[@class='ui-state-default ui-tabs-selected ui-state-active ui-corner-top']//span[@class='ui-icon ui-icon-close']","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_FUND","//input[@id='workbenchForm:workbenchTabs:grid:fundCode_Col:filter']",var_FundWOcoverageValue,"FALSE","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

        TAP.$("ELE_LINK_ROW1","//tr[@class='ui-widget-content ui-datatable-even ui-datatable-selectable']","TGTYPESCREENREG");

        WAIT.$(3,"TGTYPESCREENFULL");

        CALL.$("FundBalanceSegmentationCheck","TGTYPESCREENREG");

        LOGGER.info("Executed Step = START IF");
        if (check("ELE_GETVAL_FUNDBALANCESEGMENTATION","//span[@id='workbenchForm:workbenchTabs:fundBalanceSegmentation']","=","Single","TGTYPESCREENREG")) {
         LOGGER.info("Executed Step = IF,ELE_GETVAL_FUNDBALANCESEGMENTATION,=,Single,TGTYPESCREENREG");
        ASSERT.$("ELE_GETVAL_COVERAGEVALUEINTERESTRATEADJUSTMENT","//label[@id='workbenchForm:workbenchTabs:interestCoverageValIntRtAdj_lbl']","VISIBLE","TGTYPESCREENFULL");

        }
          LOGGER.info("Executed Step = ENDIF,TGTYPESCREENREG");
        LOGGER.info("Executed Step = START IF");
        if (check("ELE_GETVAL_FUNDBALANCESEGMENTATION","//span[@id='workbenchForm:workbenchTabs:fundBalanceSegmentation']","<>","Single","TGTYPESCREENREG")) {
         LOGGER.info("Executed Step = IF,ELE_GETVAL_FUNDBALANCESEGMENTATION,<>,Single,TGTYPESCREENREG");
        ASSERT.$("ELE_GETVAL_COVERAGEVALUEINTERESTRATEADJUSTMENT","//label[@id='workbenchForm:workbenchTabs:interestCoverageValIntRtAdj_lbl']","INVISIBLE","TGTYPESCREENFULL");

        }
          LOGGER.info("Executed Step = ENDIF,TGTYPESCREENREG");
        TAP.$("ELE_TAB_CLOSE3","//li[@class='ui-state-default ui-tabs-selected ui-state-active ui-corner-top']//span[@class='ui-icon ui-icon-close']","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_ADD2","//button[@id='workbenchForm:workbenchTabs:tb_buttonAdd']//span[@class='ui-button-text ui-c'][contains(text(),'Add')]","TGTYPESCREENREG");

        WAIT.$(3,"TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_FUND_ADDSCREEN","//input[@id='workbenchForm:workbenchTabs:fundCode']","ABC","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_RADIOBUTTON_TYPE_INTEREST","//table[@id='workbenchForm:workbenchTabs:fundType']//tr[2]//td[1]//div[1]//div[2]//span[1]","TGTYPESCREENREG");

        TAP.$("ELE_RADIOBUTTON_SINGLE","//table[@id='workbenchForm:workbenchTabs:fundBalanceSegmentation']//tr[2]//td[1]//div[1]//div[2]//span[1]","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_CONTINUE","//span[contains(text(),'Continue')]","TGTYPESCREENFULL");

        WAIT.$(3,"TGTYPESCREENREG");

        ASSERT.$("ELE_BUTTON_COVERAGEVALUEINTERESTRATEADJUSTMENT_SELECTEDNO","//table[@id='workbenchForm:workbenchTabs:interestCoverageValIntRtAdj']//span[@class='ui-radiobutton-icon ui-icon ui-icon-bullet ui-c']","VISIBLE","TGTYPESCREENFULL");

        TAP.$("ELE_TAB_CLOSE3","//li[@class='ui-state-default ui-tabs-selected ui-state-active ui-corner-top']//span[@class='ui-icon ui-icon-close']","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_ADD2","//button[@id='workbenchForm:workbenchTabs:tb_buttonAdd']//span[@class='ui-button-text ui-c'][contains(text(),'Add')]","TGTYPESCREENREG");

        WAIT.$(1,"TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_FUND_ADDSCREEN","//input[@id='workbenchForm:workbenchTabs:fundCode']","ABC","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_RADIOBUTTON_TYPE_INTEREST","//table[@id='workbenchForm:workbenchTabs:fundType']//tr[2]//td[1]//div[1]//div[2]//span[1]","TGTYPESCREENREG");

        TAP.$("ELE_RADIOBUTTON_COMPONENT","//table[@id='workbenchForm:workbenchTabs:fundBalanceSegmentation']//tr[1]//td[1]//div[1]//div[2]//span[1]","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_CONTINUE","//span[contains(text(),'Continue')]","TGTYPESCREENFULL");

        WAIT.$(1,"TGTYPESCREENREG");

        ASSERT.$("ELE_BUTTON_COVERAGEVALUEINTERESTRATEADJUSTMENT_SELECTEDNO","//table[@id='workbenchForm:workbenchTabs:interestCoverageValIntRtAdj']//span[@class='ui-radiobutton-icon ui-icon ui-icon-bullet ui-c']","INVISIBLE","TGTYPESCREENFULL");

        TAP.$("ELE_LOGO_GIAS","//span[text()='GIAS']","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

        CALL.$("CoverageValuesTEst1","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_SEARCHTASKS","//input[@id='workbenchForm:workbenchTabs:taskSearchContainer:taskSearchSearch']","Coverage Values","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_TASKSEARCH","//a[@id='workbenchForm:workbenchTabs:taskSearchContainer:tb_buttonSearch']//span[@class='fa fa-search fa-lg']","TGTYPESCREENREG");

        TAP.$("ELE_LABEL_COVERAGEVALUES","//span[contains(text(),'Coverage Values')]","TGTYPESCREENREG");

        WAIT.$(1,"TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_TABLE_COVERAGEVALUES","//input[@id='workbenchForm:workbenchTabs:grid:coverageDescTabl_Col:filter']",var_Table,"FALSE","TGTYPESCREENREG");

        WAIT.$(4,"TGTYPESCREENREG");

        TAP.$("ELE_LINK_ROW2","//tr[@class='ui-widget-content ui-datatable-odd ui-datatable-selectable']","TGTYPESCREENREG");

        WAIT.$(3,"TGTYPESCREENREG");

        ASSERT.$("ELE_GETVAL_FUNDINTERESTRATEADJUSTMENTTABLE_COVERAGEVALUESPAGE","//label[@id='workbenchForm:workbenchTabs:fundIntRateAdjTabl_lbl']","VISIBLE","TGTYPESCREENFULL");

        LOGGER.info("Executed Step = START IF");
        if (check("ELE_GETVAL_FUNDINTERESTRATEADJUSTMENTTABLE","//span[@id='workbenchForm:workbenchTabs:fundIntRateAdjTabl']","=","ABBA","TGTYPESCREENFULL")) {
         LOGGER.info("Executed Step = IF,ELE_GETVAL_FUNDINTERESTRATEADJUSTMENTTABLE,=,ABBA,TGTYPESCREENFULL");
        TAP.$("ELE_BUTTON_EDIT","//span[text()='Edit']","TGTYPESCREENREG");

        TAP.$("ELE_TXTBOX_FUNDINTERESTRATEADJUSTMENTTABLE","//input[@id='workbenchForm:workbenchTabs:fundIntRateAdjTabl']","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_FUNDINTERESTRATEADJUSTMENTTABLE","//input[@id='workbenchForm:workbenchTabs:fundIntRateAdjTabl']","ABBB","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SAVE","//span[contains(text(),'Save')]","TGTYPESCREENREG");

        }
          LOGGER.info("Executed Step = ENDIF,TGTYPESCREENREG");
        LOGGER.info("Executed Step = START IF");
        if (check("ELE_GETVAL_FUNDINTERESTRATEADJUSTMENTTABLE","//span[@id='workbenchForm:workbenchTabs:fundIntRateAdjTabl']","=","ABBB","TGTYPESCREENFULL")) {
         LOGGER.info("Executed Step = IF,ELE_GETVAL_FUNDINTERESTRATEADJUSTMENTTABLE,=,ABBB,TGTYPESCREENFULL");
        TAP.$("ELE_BUTTON_EDIT","//span[text()='Edit']","TGTYPESCREENREG");

        TAP.$("ELE_TXTBOX_FUNDINTERESTRATEADJUSTMENTTABLE","//input[@id='workbenchForm:workbenchTabs:fundIntRateAdjTabl']","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_FUNDINTERESTRATEADJUSTMENTTABLE","//input[@id='workbenchForm:workbenchTabs:fundIntRateAdjTabl']","ABBA","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SAVE","//span[contains(text(),'Save')]","TGTYPESCREENREG");

        }
          LOGGER.info("Executed Step = ENDIF,TGTYPESCREENREG");
        WAIT.$(1,"TGTYPESCREENREG");

        ASSERT.$("ELE_MSG_ITEMSUCCESSUPDATE","//div[@id='workbenchForm:workbenchTabs:messages']//span[@class='ui-messages-info-summary'][contains(text(),'Item successfully updated.')]","VISIBLE","TGTYPESCREENREG");

		String var_FundInterestRateAdjustmentTable;
                 LOGGER.info("Executed Step = VAR,String,var_FundInterestRateAdjustmentTable,TGTYPESCREENREG");
		var_FundInterestRateAdjustmentTable = ActionWrapper.getElementValue("ELE_GETVAL_FUNDINTERESTRATEADJUSTMENTTABLE", "//span[@id='workbenchForm:workbenchTabs:fundIntRateAdjTabl']");
                 LOGGER.info("Executed Step = STORE,var_FundInterestRateAdjustmentTable,ELE_GETVAL_FUNDINTERESTRATEADJUSTMENTTABLE,TGTYPESCREENREG");
        TAP.$("ELE_LOGO_GIAS","//span[text()='GIAS']","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

        CALL.$("ModF237BB578InterestRatesPage","TGTYPESCREENREG");

        SWIPE.$("DOWN","TGTYPESCREENREG");

        SWIPE.$("DOWN","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_SEARCHTASKS","//input[@id='workbenchForm:workbenchTabs:taskSearchContainer:taskSearchSearch']","Fund Interest Rates","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_TASKSEARCH","//a[@id='workbenchForm:workbenchTabs:taskSearchContainer:tb_buttonSearch']//span[@class='fa fa-search fa-lg']","TGTYPESCREENREG");

        TAP.$("ELE_LABEL_FUNDINTERESTRATES","//span[contains(text(),'Fund Interest Rates')]","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_ADD2","//button[@id='workbenchForm:workbenchTabs:tb_buttonAdd']//span[@class='ui-button-text ui-c'][contains(text(),'Add')]","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_TABLE_INTERESTRATES","//input[@id='workbenchForm:workbenchTabs:tableCode']",var_FundInterestRateAdjustmentTable,"FALSE","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_ENDINGDURATION","//input[@id='workbenchForm:workbenchTabs:duration_input']",var_EndingDuration,"FALSE","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_EFFECTIVEDATE","//input[@id='workbenchForm:workbenchTabs:effectiveDate_input']",var_EffectiveDate,"FALSE","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_ANNUALINTERESTRATE_INTERESTRATES","//input[@id='workbenchForm:workbenchTabs:annualInterestRate_input']",var_AnnualInterestRate,"FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_VALIDATE","//span[contains(text(),'Validate')]","TGTYPESCREENFULL");

        WAIT.$(2,"TGTYPESCREENREG");

        ASSERT.$("ELE_MSG_VALIDATIONSUCCESS","//div[@id='workbenchForm:workbenchTabs:messages']//span[@class='ui-messages-info-summary'][contains(text(),'Validation completed successfully with no errors.')]","VISIBLE","TGTYPESCREENFULL");

        TYPE.$("ELE_TXTBOX_TABLE_INTERESTRATES","//input[@id='workbenchForm:workbenchTabs:tableCode']","ABBY","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_VALIDATE","//span[contains(text(),'Validate')]","TGTYPESCREENREG");

        WAIT.$(1,"TGTYPESCREENREG");

        ASSERT.$("ELE_MSG_ERRORMESG_BELOWFIELD","//span[@class='ui-message-error-detail']","VISIBLE","TGTYPESCREENFULL");

        ASSERT.$("ELE_MSG_ERRORMESG_BELOWFIELD","//span[@class='ui-message-error-detail']","CONTAINS","Table not found on fund description","TGTYPESCREENFULL");

		var_Count = var_Count + ( 1 );
                 LOGGER.info("Executed Step = INCREASEVAR,var_Count,1,TGTYPESCREENREG");
    	}
          LOGGER.info("Executed Step = WEND,TGTYPESCREENREG");
    	resetAttemps();

    }


    @Test
    public void ftj41modf216() {

        // No need to declare / print method name here, handled by TestNg listener
        // declare("ftj41modf216");

		int var_Count = 0;
                 LOGGER.info("Executed Step = VAR,Integer,var_Count,0,TGTYPESCREENREG");
		com.jayway.jsonpath.ReadContext var_CSVData = getJsonData("http://10.1.104.229/json/20201009/vinRK3.json");
                 LOGGER.info("Executed Step = VAR,Json,var_CSVData,http://10.1.104.229/json/20201009/vinRK3.json,TGTYPESCREENREG");
        LOGGER.info("Executed Step = REPEAT IF");
    	while (check(var_Count,"<",1,"TGTYPESCREENREG")) {
          LOGGER.info("Executed Step = WHILE,var_Count,<,1,TGTYPESCREENREG");
		String var_Plan;
                 LOGGER.info("Executed Step = VAR,String,var_Plan,TGTYPESCREENREG");
		var_Plan = getJsonData(var_CSVData , "$.records["+var_Count+"].Plan");
                 LOGGER.info("Executed Step = STORE,var_Plan,var_CSVData,$.records[{var_Count}].Plan,TGTYPESCREENREG");
		String var_TargetPlan;
                 LOGGER.info("Executed Step = VAR,String,var_TargetPlan,TGTYPESCREENREG");
		var_TargetPlan = getJsonData(var_CSVData , "$.records["+var_Count+"].TargetPlan");
                 LOGGER.info("Executed Step = STORE,var_TargetPlan,var_CSVData,$.records[{var_Count}].TargetPlan,TGTYPESCREENREG");
		String var_CapitalizedInterestAllocationMethod;
                 LOGGER.info("Executed Step = VAR,String,var_CapitalizedInterestAllocationMethod,TGTYPESCREENREG");
		var_CapitalizedInterestAllocationMethod = getJsonData(var_CSVData , "$.records["+var_Count+"].CapitalizedInterestAllocationMethod");
                 LOGGER.info("Executed Step = STORE,var_CapitalizedInterestAllocationMethod,var_CSVData,$.records[{var_Count}].CapitalizedInterestAllocationMethod,TGTYPESCREENREG");
        CALL.$("FTJQA2Login","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

		try { 
		 WebElement html = driver.findElement(By.tagName("html"));
		    	
		    	System.out.println("html size" + html.getSize());
		    	System.out.println("html" + html.getTagName());

		    	for( int i =0 ;i<4;i++) {
		        html.sendKeys(Keys.chord(Keys.CONTROL, Keys.SUBTRACT));
		    	}
		 
		 ;} 
		 catch(Exception e) { PrintVal("Renderer Script Exception : "+e.toString()); } 
		 PrintVal("Executed Step = CALLRENDERERSCRIPT,FTJ41ZOOMMAGNIFICATIONCHANGE"); 
        TYPE.$("ELE_TXTBOX_USERNAME","//input[@id='guestForm:username']","x5080","FALSE","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_PASSWORD","//input[@id='guestForm:password']","Welcome123$","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_LOGIN","//button[@id='guestForm:loginButton']","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        CALL.$("F216ImportPlan","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_SEARCHTASKS","//input[@id='workbenchForm:workbenchTabs:taskSearchContainer:taskSearchSearch']","Import Plan","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_TASKSEARCH","//a[@id='workbenchForm:workbenchTabs:taskSearchContainer:tb_buttonSearch']//span[@class='fa fa-search fa-lg']","TGTYPESCREENREG");

        TAP.$("ELE_LABEL_IMPORTPLAN","//span[contains(text(),'Import Plan')]","TGTYPESCREENREG");

        WAIT.$(1,"TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_SOURCEURL","//input[@id='workbenchForm:workbenchTabs:webServiceURL']","http://cis-gsstcd3.btoins.ibm.com:17100/ws","FALSE","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_SOURCEPLANCODE","//input[@id='workbenchForm:workbenchTabs:sourcePlanCode']",var_Plan,"FALSE","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_USER","//input[@id='workbenchForm:workbenchTabs:userID']","Bolger","FALSE","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_PASSWORD_IMPORTPLAN","//input[@id='workbenchForm:workbenchTabs:password']","Password!123","FALSE","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_TARGETPLANCODE","//input[@id='workbenchForm:workbenchTabs:targetPlanCode']",var_TargetPlan,"FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SUBMIT","//span[contains(text(),'Submit')]","TGTYPESCREENFULL");

        WAIT.$(55,"TGTYPESCREENREG");

        TAP.$("ELE_LOGO_GIAS","//span[text()='GIAS']","TGTYPESCREENREG");

        WAIT.$(1,"TGTYPESCREENREG");

        CALL.$("F216CapitalzedInterestAllocationMethodValidations","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_SEARCHTASKS","//input[@id='workbenchForm:workbenchTabs:taskSearchContainer:taskSearchSearch']","Plan setup","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_TASKSEARCH","//a[@id='workbenchForm:workbenchTabs:taskSearchContainer:tb_buttonSearch']//span[@class='fa fa-search fa-lg']","TGTYPESCREENREG");

        TAP.$("ELE_LINK_PLANSETUP","//span[contains(text(),'Plan Setup')]","TGTYPESCREENREG");

        WAIT.$(1,"TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_PLAN","//input[@id='workbenchForm:workbenchTabs:grid:planCode_Col:filter']",var_TargetPlan,"FALSE","TGTYPESCREENREG");

        WAIT.$(1,"TGTYPESCREENREG");

        TAP.$("ELE_LINK_ROW1","//tr[@class='ui-widget-content ui-datatable-even ui-datatable-selectable']","TGTYPESCREENREG");

        WAIT.$(3,"TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_EDIT","//span[text()='Edit']","TGTYPESCREENREG");

        WAIT.$(1,"TGTYPESCREENREG");

        LOGGER.info("Executed Step = START IF");
        if (check("ELE_DRPDWN_CARRIER","//div[@id='workbenchForm:workbenchTabs:carrierCode']//div//span","VISIBLE","TGTYPESCREENREG")) {
         LOGGER.info("Executed Step = IF,ELE_DRPDWN_CARRIER,VISIBLE,TGTYPESCREENREG");
        TAP.$("ELE_DRPDWN_CARRIER","//div[@id='workbenchForm:workbenchTabs:carrierCode']//div//span","TGTYPESCREENREG");

        TAP.$("ELE_DRPDWN_CARRIERCOMPANYABC1","//li[contains(text(),'Carrier Company ABC1')]","TGTYPESCREENREG");

        SWIPE.$("DOWN","TGTYPESCREENREG");

        SWIPE.$("DOWN","TGTYPESCREENREG");

        SWIPE.$("DOWN","TGTYPESCREENREG");

        SWIPE.$("DOWN","TGTYPESCREENREG");

        SWIPE.$("DOWN","TGTYPESCREENREG");

        }
          LOGGER.info("Executed Step = ENDIF,TGTYPESCREENREG");
        TAP.$("ELE_TAB_LOANS","//span[contains(text(),'Loans')]","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

        ASSERT.$("ELE_GETVAL_CAPITALIZEDINTERESTALLOCATIONMETHOD","//label[@id='workbenchForm:workbenchTabs:capIntAllocMethod_lbl']","VISIBLE","TGTYPESCREENFULL");

        SWIPE.$("DOWN","TGTYPESCREENREG");

        SWIPE.$("DOWN","TGTYPESCREENREG");

        TAP.$("ELE_DRPDWN_INTERESTCAPITALIZATIONMETHOD","//div[@id='workbenchForm:workbenchTabs:loanIntCapiMthd']//div//span","TGTYPESCREENREG");

        TAP.$("ELE_DRPDWN_INTERESTCAPITALIZATIONMETHOD_BLANK","//li[@id='workbenchForm:workbenchTabs:loanIntCapiMthd_0']","TGTYPESCREENREG");

        TAP.$("ELE_DRPDWN_CAPITALIZEDINTERESTALLOCATIONMETHOD","//div[@id='workbenchForm:workbenchTabs:capIntAllocMethod']//div//span","TGTYPESCREENREG");

        TAP.$("ELE_DRPDWN_CAPITALIZEDINTERESTALLOCATIONMETHOD_BLANK","//li[@id='workbenchForm:workbenchTabs:capIntAllocMethod_0']","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_VALIDATE","//span[contains(text(),'Validate')]","TGTYPESCREENFULL");

        ASSERT.$("ELE_MSG_ERRORMESG_BELOWFIELD","//span[@class='ui-message-error-detail']","CONTAINS","Interest capitalization method cannot be blank.","TGTYPESCREENREG");

        CALL.$("InterestCapitalizationMethodEqualsPayment","TGTYPESCREENREG");

        SWIPE.$("DOWN","TGTYPESCREENREG");

        SWIPE.$("DOWN","TGTYPESCREENREG");

        TAP.$("ELE_DRPDWN_INTERESTCAPITALIZATIONMETHOD","//div[@id='workbenchForm:workbenchTabs:loanIntCapiMthd']//div//span","TGTYPESCREENREG");

        TAP.$("ELE_DRPDWN_INTERESTCAPITALIZATIONMETHOD_PAYMENT","//li[@id='workbenchForm:workbenchTabs:loanIntCapiMthd_1']","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_VALIDATE","//span[contains(text(),'Validate')]","TGTYPESCREENFULL");

        WAIT.$(1,"TGTYPESCREENREG");

        ASSERT.$("ELE_MSG_ERRORMESG_BELOWFIELD","//span[@class='ui-message-error-detail']","CONTAINS","Capitalized Interest Allocation Method cannot be blank.","TGTYPESCREENREG");

        SWIPE.$("DOWN","TGTYPESCREENREG");

        SWIPE.$("DOWN","TGTYPESCREENREG");

        TAP.$("ELE_DRPDWN_CAPITALIZEDINTERESTALLOCATIONMETHOD","//div[@id='workbenchForm:workbenchTabs:capIntAllocMethod']//div//span","TGTYPESCREENREG");

        TAP.$("ELE_DRPDWN_CAPITALIZEDINTERESTALLOCATIONMETHOD_CURRENTPREMIUMALLOCATION","//li[@id='workbenchForm:workbenchTabs:capIntAllocMethod_1']","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_VALIDATE","//span[contains(text(),'Validate')]","TGTYPESCREENFULL");

        WAIT.$(1,"TGTYPESCREENREG");

        ASSERT.$("ELE_MSG_VALIDATIONSUCCESS","//div[@id='workbenchForm:workbenchTabs:messages']//span[@class='ui-messages-info-summary'][contains(text(),'Validation completed successfully with no errors.')]","VISIBLE","TGTYPESCREENREG");

        SWIPE.$("DOWN","TGTYPESCREENREG");

        SWIPE.$("DOWN","TGTYPESCREENREG");

        TAP.$("ELE_DRPDWN_CAPITALIZEDINTERESTALLOCATIONMETHOD","//div[@id='workbenchForm:workbenchTabs:capIntAllocMethod']//div//span","TGTYPESCREENREG");

        TAP.$("ELE_DRPDWN_CAPITALIZEDINTERESTALLOCATIONMETHODCURRENTPREMIUM_PRORATED","//li[@id='workbenchForm:workbenchTabs:capIntAllocMethod_2']","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_VALIDATE","//span[contains(text(),'Validate')]","TGTYPESCREENREG");

        WAIT.$(1,"TGTYPESCREENREG");

        ASSERT.$("ELE_MSG_VALIDATIONSUCCESS","//div[@id='workbenchForm:workbenchTabs:messages']//span[@class='ui-messages-info-summary'][contains(text(),'Validation completed successfully with no errors.')]","VISIBLE","TGTYPESCREENFULL");

        CALL.$("InterestCapitalizationMethodEqualsPolicyAnniversary","TGTYPESCREENREG");

        SWIPE.$("DOWN","TGTYPESCREENREG");

        SWIPE.$("DOWN","TGTYPESCREENREG");

        TAP.$("ELE_DRPDWN_INTERESTCAPITALIZATIONMETHOD","//div[@id='workbenchForm:workbenchTabs:loanIntCapiMthd']//div//span","TGTYPESCREENREG");

        TAP.$("ELE_DRPDWN_INTERESTCAPITALIZATIONMETHOD_POLICYANNIVERSARY","//li[@id='workbenchForm:workbenchTabs:loanIntCapiMthd_2']","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_VALIDATE","//span[contains(text(),'Validate')]","TGTYPESCREENFULL");

        WAIT.$(1,"TGTYPESCREENREG");

        ASSERT.$("ELE_MSG_ERRORMESG_BELOWFIELD","//span[@class='ui-message-error-detail']","CONTAINS","Capitalized Interest Allocation Method must be blank.","TGTYPESCREENFULL");

        SWIPE.$("DOWN","TGTYPESCREENREG");

        SWIPE.$("DOWN","TGTYPESCREENREG");

        TAP.$("ELE_DRPDWN_CAPITALIZEDINTERESTALLOCATIONMETHOD","//div[@id='workbenchForm:workbenchTabs:capIntAllocMethod']//div//span","TGTYPESCREENREG");

        TAP.$("ELE_DRPDWN_CAPITALIZEDINTERESTALLOCATIONMETHOD_CURRENTPREMIUMALLOCATION","//li[@id='workbenchForm:workbenchTabs:capIntAllocMethod_1']","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_VALIDATE","//span[contains(text(),'Validate')]","TGTYPESCREENFULL");

        WAIT.$(1,"TGTYPESCREENREG");

        ASSERT.$("ELE_MSG_ERRORMESG_BELOWFIELD","//span[@class='ui-message-error-detail']","CONTAINS","Capitalized Interest Allocation Method must be blank.","TGTYPESCREENFULL");

        SWIPE.$("DOWN","TGTYPESCREENREG");

        SWIPE.$("DOWN","TGTYPESCREENREG");

        TAP.$("ELE_DRPDWN_CAPITALIZEDINTERESTALLOCATIONMETHOD","//div[@id='workbenchForm:workbenchTabs:capIntAllocMethod']//div//span","TGTYPESCREENREG");

        TAP.$("ELE_DRPDWN_CAPITALIZEDINTERESTALLOCATIONMETHOD_BLANK","//li[@id='workbenchForm:workbenchTabs:capIntAllocMethod_0']","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_VALIDATE","//span[contains(text(),'Validate')]","TGTYPESCREENFULL");

        WAIT.$(1,"TGTYPESCREENREG");

        ASSERT.$("ELE_MSG_VALIDATIONSUCCESS","//div[@id='workbenchForm:workbenchTabs:messages']//span[@class='ui-messages-info-summary'][contains(text(),'Validation completed successfully with no errors.')]","VISIBLE","TGTYPESCREENFULL");

        CALL.$("SaveCapitalizedInterestAllocation","TGTYPESCREENREG");

        SWIPE.$("DOWN","TGTYPESCREENREG");

        SWIPE.$("DOWN","TGTYPESCREENREG");

        TAP.$("ELE_DRPDWN_INTERESTCAPITALIZATIONMETHOD","//div[@id='workbenchForm:workbenchTabs:loanIntCapiMthd']//div//span","TGTYPESCREENREG");

        TAP.$("ELE_DRPDWN_INTERESTCAPITALIZATIONMETHOD_PAYMENT","//li[@id='workbenchForm:workbenchTabs:loanIntCapiMthd_1']","TGTYPESCREENREG");

		String var_dataToPass;
                 LOGGER.info("Executed Step = VAR,String,var_dataToPass,TGTYPESCREENREG");
		var_dataToPass = getJsonData(var_CSVData , "$.records["+var_Count+"].CapitalizedInterestAllocationMethod");
                 LOGGER.info("Executed Step = STORE,var_dataToPass,var_CSVData,$.records[{var_Count}].CapitalizedInterestAllocationMethod,TGTYPESCREENREG");
        TAP.$("ELE_DRPDWN_CAPITALIZEDINTERESTALLOCATIONMETHOD","//div[@id='workbenchForm:workbenchTabs:capIntAllocMethod']//div//span","TGTYPESCREENREG");

        WAIT.$(1,"TGTYPESCREENREG");

		try { 
		 Thread.sleep(1000);
		WebElement selectValueOfDropDown= driver.findElement(By.xpath("//li[@data-label='"+ var_dataToPass +"']"));

		takeScreenshot();

				selectValueOfDropDown.click();
		// Add Screenshot will only work for Android platform

		takeScreenshot();

				 Thread.sleep(1000);
		 
		 ;} 
		 catch(Exception e) { PrintVal("Renderer Script Exception : "+e.toString()); } 
		 PrintVal("Executed Step = CALLRENDERERSCRIPT,SELECTVALUEFROMDROPDOWN"); 
        WAIT.$(2,"TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_VALIDATE","//span[contains(text(),'Validate')]","TGTYPESCREENREG");

        WAIT.$(1,"TGTYPESCREENREG");

        ASSERT.$("ELE_MSG_VALIDATIONSUCCESS","//div[@id='workbenchForm:workbenchTabs:messages']//span[@class='ui-messages-info-summary'][contains(text(),'Validation completed successfully with no errors.')]","VISIBLE","TGTYPESCREENREG");

        SWIPE.$("DOWN","TGTYPESCREENREG");

        SWIPE.$("DOWN","TGTYPESCREENREG");

        TAP.$("ELE_TAB_BASICINSURANCEINFORMATION","//span[contains(text(),'Basic Insurance Information')]","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SAVE","//span[contains(text(),'Save')]","TGTYPESCREENREG");

        WAIT.$(10,"TGTYPESCREENREG");

        LOGGER.info("Executed Step = START IF");
        if (check("ELE_CONFIRMACTION_YES","//div[@class='ui-dialog-content ui-widget-content ui-df-content']//iframe","VISIBLE","TGTYPESCREENREG")) {
         LOGGER.info("Executed Step = IF,ELE_CONFIRMACTION_YES,VISIBLE,TGTYPESCREENREG");
		try { 
		 driver.switchTo().frame(0);
		driver.findElement(By.xpath("//form[@id='confirmDialogForm']")).click();
		Thread.sleep(1000);
		driver.findElement(By.xpath("//button[@id='j_idt9']")).click();
		// Add Screenshot will only work for Android platform

		takeScreenshot();

		 
		 ;} 
		 catch(Exception e) { PrintVal("Renderer Script Exception : "+e.toString()); } 
		 PrintVal("Executed Step = CALLRENDERERSCRIPT,CLICKYESONCONFIRMACTION"); 
        WAIT.$(8,"TGTYPESCREENREG");

        }
          LOGGER.info("Executed Step = ENDIF,TGTYPESCREENREG");
        ASSERT.$("ELE_MSG_ITEMSUCCESSUPDATE","//div[@id='workbenchForm:workbenchTabs:messages']//span[@class='ui-messages-info-summary'][contains(text(),'Item successfully updated.')]","VISIBLE","TGTYPESCREENREG");

		var_Count = var_Count + ( 1 );
                 LOGGER.info("Executed Step = INCREASEVAR,var_Count,1,TGTYPESCREENREG");
    	}
          LOGGER.info("Executed Step = WEND,TGTYPESCREENREG");
    	resetAttemps();

    }


    @Test
    public void ftj41mod17glc6() {

        // No need to declare / print method name here, handled by TestNg listener
        // declare("ftj41mod17glc6");

		int var_Count = 0;
                 LOGGER.info("Executed Step = VAR,Integer,var_Count,0,TGTYPESCREENREG");
		com.jayway.jsonpath.ReadContext var_CSVData = getJsonData("http://10.1.104.229/json/20200818/FXAjkq.json");
                 LOGGER.info("Executed Step = VAR,Json,var_CSVData,http://10.1.104.229/json/20200818/FXAjkq.json,TGTYPESCREENREG");
        LOGGER.info("Executed Step = REPEAT IF");
    	while (check(var_Count,"<",1,"TGTYPESCREENREG")) {
          LOGGER.info("Executed Step = WHILE,var_Count,<,1,TGTYPESCREENREG");
		String var_LastName;
                 LOGGER.info("Executed Step = VAR,String,var_LastName,TGTYPESCREENREG");
		var_LastName = getJsonData(var_CSVData , "$.records["+var_Count+"].LastName");
                 LOGGER.info("Executed Step = STORE,var_LastName,var_CSVData,$.records[{var_Count}].LastName,TGTYPESCREENREG");
		String var_GroupValue;
                 LOGGER.info("Executed Step = VAR,String,var_GroupValue,TGTYPESCREENREG");
		var_GroupValue = getJsonData(var_CSVData , "$.records["+var_Count+"].GroupValue");
                 LOGGER.info("Executed Step = STORE,var_GroupValue,var_CSVData,$.records[{var_Count}].GroupValue,TGTYPESCREENREG");
        CALL.$("FTJQA2Login","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

		try { 
		 WebElement html = driver.findElement(By.tagName("html"));
		    	
		    	System.out.println("html size" + html.getSize());
		    	System.out.println("html" + html.getTagName());

		    	for( int i =0 ;i<4;i++) {
		        html.sendKeys(Keys.chord(Keys.CONTROL, Keys.SUBTRACT));
		    	}
		 
		 ;} 
		 catch(Exception e) { PrintVal("Renderer Script Exception : "+e.toString()); } 
		 PrintVal("Executed Step = CALLRENDERERSCRIPT,FTJ41ZOOMMAGNIFICATIONCHANGE"); 
        TYPE.$("ELE_TXTBOX_USERNAME","//input[@id='guestForm:username']","x5080","FALSE","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_PASSWORD","//input[@id='guestForm:password']","Welcome123$","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_LOGIN","//button[@id='guestForm:loginButton']","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        CALL.$("Mod17CLM500Providers","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_SEARCHTASKS","//input[@id='workbenchForm:workbenchTabs:taskSearchContainer:taskSearchSearch']","clm500","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_TASKSEARCH","//a[@id='workbenchForm:workbenchTabs:taskSearchContainer:tb_buttonSearch']//span[@class='fa fa-search fa-lg']","TGTYPESCREENREG");

        TAP.$("ELE_LABEL_PROVIDERSELECTIONCRITERIA","//span[contains(text(),'Provider Selection Criteria')]","TGTYPESCREENREG");

        WAIT.$(1,"TGTYPESCREENREG");

        TAP.$("ELE_DRPDWN_SEARCHCRITERIA","//div[@id='workbenchForm:workbenchTabs:searchCriteria']//div//span","TGTYPESCREENREG");

        TAP.$("ELE_DRPDWN_SEARCHCRITERIA_PROVIDERNAME","//li[@id='workbenchForm:workbenchTabs:searchCriteria_2']","TGTYPESCREENREG");

        WAIT.$(1,"TGTYPESCREENREG");

        TAP.$("ELE_DRPDWN_ENTITYTYPE","//div[@id='workbenchForm:workbenchTabs:entityType']//div//span","TGTYPESCREENREG");

        TAP.$("ELE_DRPDWN_ENTITYTYPE_INDIVIDUAL","//li[@id='workbenchForm:workbenchTabs:entityType_1']","TGTYPESCREENREG");

        WAIT.$(1,"TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_LASTNAME2","//input[@id='workbenchForm:workbenchTabs:lastName']",var_LastName,"FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_CONTINUE","//span[contains(text(),'Continue')]","TGTYPESCREENFULL");

        WAIT.$(1,"TGTYPESCREENREG");

        ASSERT.$("ELE_COLUMN_SUFFIX","//th[@id='workbenchForm:workbenchTabs:grid:providerIDSuffix_Col']","VISIBLE","TGTYPESCREENREG");

        ASSERT.$("ELE_COLUMN_SERVICEADDRESS","//th[@id='workbenchForm:workbenchTabs:grid:providerServiceAddress_Col']","VISIBLE","TGTYPESCREENREG");

        ASSERT.$("ELE_COLUMN_PAYEEADDRESS","//th[@id='workbenchForm:workbenchTabs:grid:providerPayeeAddress_Col']","VISIBLE","TGTYPESCREENREG");

        ASSERT.$("ELE_COLUMN_TAXADDRESS","//th[@id='workbenchForm:workbenchTabs:grid:providerTaxAddress_Col']","VISIBLE","TGTYPESCREENREG");

        TAP.$("ELE_LINK_ROW1","//tr[@class='ui-widget-content ui-datatable-even ui-datatable-selectable']","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

		String var_SuffixID;
                 LOGGER.info("Executed Step = VAR,String,var_SuffixID,TGTYPESCREENREG");
		var_SuffixID = ActionWrapper.getElementValue("ELE_GETVAL_SUFFIXID_DISPLAYPROVIDERPAGE", "//span[@id='workbenchForm:workbenchTabs:providerIDSuffix']");
                 LOGGER.info("Executed Step = STORE,var_SuffixID,ELE_GETVAL_SUFFIXID_DISPLAYPROVIDERPAGE,TGTYPESCREENREG");
		String var_PayeeAddress;
                 LOGGER.info("Executed Step = VAR,String,var_PayeeAddress,TGTYPESCREENREG");
		var_PayeeAddress = ActionWrapper.getElementValue("ELE_GETVAL_PAYEEADDRESS_DISPLAYPROVIDER", "//span[@id='workbenchForm:workbenchTabs:currentPayeeProviderAddress1']");
                 LOGGER.info("Executed Step = STORE,var_PayeeAddress,ELE_GETVAL_PAYEEADDRESS_DISPLAYPROVIDER,TGTYPESCREENREG");
		String var_TaxAddressPart1;
                 LOGGER.info("Executed Step = VAR,String,var_TaxAddressPart1,TGTYPESCREENREG");
		var_TaxAddressPart1 = ActionWrapper.getElementValue("ELE_GETVAL_TAXADDRESS1_DISPLAYPROVIDER", "//span[@id='workbenchForm:workbenchTabs:currentTaxProviderAddress1']");
                 LOGGER.info("Executed Step = STORE,var_TaxAddressPart1,ELE_GETVAL_TAXADDRESS1_DISPLAYPROVIDER,TGTYPESCREENREG");
		String var_TaxAddressPart2;
                 LOGGER.info("Executed Step = VAR,String,var_TaxAddressPart2,TGTYPESCREENREG");
		var_TaxAddressPart2 = ActionWrapper.getElementValue("ELE_GETVAL_TAXADDRESS2_DISPLAYPROVIDER", "//span[@id='workbenchForm:workbenchTabs:currentTaxProviderAddress2']");
                 LOGGER.info("Executed Step = STORE,var_TaxAddressPart2,ELE_GETVAL_TAXADDRESS2_DISPLAYPROVIDER,TGTYPESCREENREG");
		String var_TaxAddressPart3;
                 LOGGER.info("Executed Step = VAR,String,var_TaxAddressPart3,TGTYPESCREENREG");
		var_TaxAddressPart3 = ActionWrapper.getElementValue("ELE_GETVAL_TAXADDRESS3_DISPLAYPROVIDER", "//span[@id='workbenchForm:workbenchTabs:currentTaxProviderCityStateZip']");
                 LOGGER.info("Executed Step = STORE,var_TaxAddressPart3,ELE_GETVAL_TAXADDRESS3_DISPLAYPROVIDER,TGTYPESCREENREG");
        WAIT.$(1,"TGTYPESCREENFULL");

        TAP.$("ELE_TAB3","a[href='#workbenchForm:workbenchTabs:tab_2']TGWEBCOMMACssSelectorTGWEBCOMMA0","TGTYPESCREENREG");

        WAIT.$(1,"TGTYPESCREENREG");

        ASSERT.$("ELE_GETVAL_SUFFIXID_PROVIDERVALUE","//span[@id='workbenchForm:workbenchTabs:grid:0:providerIDSuffix']","CONTAINS",var_SuffixID,"TGTYPESCREENREG");

        ASSERT.$("ELE_GETVAL_PAYEEADDRESS_PROVIDERVALUE","//span[@id='workbenchForm:workbenchTabs:grid:0:providerPayeeAddress']","CONTAINS",var_PayeeAddress,"TGTYPESCREENREG");

        ASSERT.$("ELE_GETVAL_TAXADDRESS_PROVIDERVALUE","//span[@id='workbenchForm:workbenchTabs:grid:0:providerTaxAddress']","CONTAINS",var_TaxAddressPart1,"TGTYPESCREENREG");

        ASSERT.$("ELE_GETVAL_TAXADDRESS_PROVIDERVALUE","//span[@id='workbenchForm:workbenchTabs:grid:0:providerTaxAddress']","CONTAINS",var_TaxAddressPart2,"TGTYPESCREENFULL");

        TAP.$("ELE_LOGO_GIAS","//span[text()='GIAS']","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

        CALL.$("Mod17CLM495ProvidersByAddress","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_SEARCHTASKS","//input[@id='workbenchForm:workbenchTabs:taskSearchContainer:taskSearchSearch']","edi120","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_TASKSEARCH","//a[@id='workbenchForm:workbenchTabs:taskSearchContainer:tb_buttonSearch']//span[@class='fa fa-search fa-lg']","TGTYPESCREENREG");

        TAP.$("ELE_LABEL_CLAIMSRESOLUTIONSEARCH","//span[contains(text(),'Claim Resolution - Search')]","TGTYPESCREENREG");

        WAIT.$(1,"TGTYPESCREENREG");

        TAP.$("ELE_DRPDWN_OPERATOR","//div[@id='workbenchForm:workbenchTabs:searchTable:0:NumericOperator']//div//span","TGTYPESCREENREG");

        TAP.$("ELE_DRPDWN_OPERATOR_BLANK","//li[@id='workbenchForm:workbenchTabs:searchTable:0:NumericOperator_0']","TGTYPESCREENREG");

        TAP.$("ELE_DRPDWN_GROUP","//div[@id='workbenchForm:workbenchTabs:searchTable:5:NumericOperator']//div//span","TGTYPESCREENREG");

        TAP.$("ELE_DRPDWN_GROUP_EQUALS","//li[@id='workbenchForm:workbenchTabs:searchTable:5:NumericOperator_1']","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_GROUP_EDI120","//input[@name='workbenchForm:workbenchTabs:searchTable:5:fieldHybrid10']",var_GroupValue,"FALSE","TGTYPESCREENREG");

        WAIT.$(1,"TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SEARCHBUTTON","//div[@id='workbenchForm:workbenchTabs:tab_1']//span[@class='ui-button-text ui-c'][contains(text(),'Search')]","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

        TAP.$("ELE_LINK_ROW1","//tr[@class='ui-widget-content ui-datatable-even ui-datatable-selectable']","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SHOWLINKS","//span[@class='ui-button-icon-left ui-icon ui-c fa fa-ellipsis-v fa-2x']","TGTYPESCREENREG");

        TAP.$("ELE_LINK_CLAIMREPOSITORYPROVIDER","//span[contains(text(),'Claim Repository - Provider')]","TGTYPESCREENREG");

        WAIT.$("ELE_BUTTON_EDIT","//span[text()='Edit']","VISIBLE","TGTYPESCREENREG");

        WAIT.$(10,"TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_EDIT","//span[text()='Edit']","TGTYPESCREENREG");

        WAIT.$(4,"TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SHOWLINKS","//span[@class='ui-button-icon-left ui-icon ui-c fa fa-ellipsis-v fa-2x']","TGTYPESCREENREG");

        TAP.$("ELE_LINK_PROVIDERSELECT","//span[contains(text(),'Provider Select')]","TGTYPESCREENREG");

        WAIT.$(3,"TGTYPESCREENREG");

        SWIPE.$("DOWN","TGTYPESCREENREG");

        TAP.$("ELE_DRPDWN_SEARCHMETHOD","//div[@id='workbenchForm:workbenchTabs:searchMethod']//div//span","TGTYPESCREENREG");

        TAP.$("ELE_DRPDWN_SEARCHMETHOD_PROVIDERONLY","//li[@id='workbenchForm:workbenchTabs:searchMethod_4']","TGTYPESCREENREG");

        WAIT.$(1,"TGTYPESCREENREG");

        TAP.$("ELE_DRPDWN_PROVIDERTYPE","//div[@id='workbenchForm:workbenchTabs:providerType']//div//span","TGTYPESCREENREG");

        TAP.$("ELE_DRPDWN_PROVIDERTYPE_INDIVIDUAL","//li[@id='workbenchForm:workbenchTabs:providerType_1']","TGTYPESCREENREG");

        WAIT.$(1,"TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_LASTNAME_CLM495","//input[@id='workbenchForm:workbenchTabs:individualLastName']",var_LastName,"FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_CONTINUE","//span[contains(text(),'Continue')]","TGTYPESCREENREG");

        WAIT.$(4,"TGTYPESCREENREG");

        ASSERT.$("ELE_COLUMN_SUFFIX","//th[@id='workbenchForm:workbenchTabs:grid:providerIDSuffix_Col']","VISIBLE","TGTYPESCREENREG");

        ASSERT.$("ELE_COLUMN_TAXADDRESS_CLM495","//th[@id='workbenchForm:workbenchTabs:grid:formattedTaxAddress_Col']","VISIBLE","TGTYPESCREENREG");

        ASSERT.$("ELE_GETVAL_SUFFIXID_PROVIDERVALUE","//span[@id='workbenchForm:workbenchTabs:grid:0:providerIDSuffix']","CONTAINS",var_SuffixID,"TGTYPESCREENREG");

        ASSERT.$("ELE_GETVAL_TAXADDRESS_PROVIDERVALUECLM495","//span[@id='workbenchForm:workbenchTabs:grid:0:formattedTaxAddress']","CONTAINS",var_TaxAddressPart1,"TGTYPESCREENREG");

        ASSERT.$("ELE_GETVAL_TAXADDRESS_PROVIDERVALUECLM495","//span[@id='workbenchForm:workbenchTabs:grid:0:formattedTaxAddress']","CONTAINS",var_TaxAddressPart3,"TGTYPESCREENFULL");

		var_Count = var_Count + ( 1 );
                 LOGGER.info("Executed Step = INCREASEVAR,var_Count,1,TGTYPESCREENREG");
    	}
          LOGGER.info("Executed Step = WEND,TGTYPESCREENREG");
    	resetAttemps();

    }


    @Test
    public void ftj41mod227sprint09() {

        // No need to declare / print method name here, handled by TestNg listener
        // declare("ftj41mod227sprint09");

		int var_Count = 0;
                 LOGGER.info("Executed Step = VAR,Integer,var_Count,0,TGTYPESCREENREG");
		com.jayway.jsonpath.ReadContext var_CSVData = getJsonData("http://10.1.104.229/json/20200818/as6p79.json");
                 LOGGER.info("Executed Step = VAR,Json,var_CSVData,http://10.1.104.229/json/20200818/as6p79.json,TGTYPESCREENREG");
        LOGGER.info("Executed Step = REPEAT IF");
    	while (check(var_Count,"<",1,"TGTYPESCREENREG")) {
          LOGGER.info("Executed Step = WHILE,var_Count,<,1,TGTYPESCREENREG");
		String var_EndingDuration;
                 LOGGER.info("Executed Step = VAR,String,var_EndingDuration,TGTYPESCREENREG");
		var_EndingDuration = getJsonData(var_CSVData , "$.records["+var_Count+"].EndingDuration");
                 LOGGER.info("Executed Step = STORE,var_EndingDuration,var_CSVData,$.records[{var_Count}].EndingDuration,TGTYPESCREENREG");
		String var_Plan;
                 LOGGER.info("Executed Step = VAR,String,var_Plan,TGTYPESCREENREG");
		var_Plan = getJsonData(var_CSVData , "$.records["+var_Count+"].Plan");
                 LOGGER.info("Executed Step = STORE,var_Plan,var_CSVData,$.records[{var_Count}].Plan,TGTYPESCREENREG");
        CALL.$("FTJQA2Login","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

		try { 
		 WebElement html = driver.findElement(By.tagName("html"));
		    	
		    	System.out.println("html size" + html.getSize());
		    	System.out.println("html" + html.getTagName());

		    	for( int i =0 ;i<4;i++) {
		        html.sendKeys(Keys.chord(Keys.CONTROL, Keys.SUBTRACT));
		    	}
		 
		 ;} 
		 catch(Exception e) { PrintVal("Renderer Script Exception : "+e.toString()); } 
		 PrintVal("Executed Step = CALLRENDERERSCRIPT,FTJ41ZOOMMAGNIFICATIONCHANGE"); 
        TYPE.$("ELE_TXTBOX_USERNAME","//input[@id='guestForm:username']","x5080","FALSE","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_PASSWORD","//input[@id='guestForm:password']","Welcome123$","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_LOGIN","//button[@id='guestForm:loginButton']","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        CALL.$("F227VPUNewAccessMethodValidations","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_SEARCHTASKS","//input[@id='workbenchForm:workbenchTabs:taskSearchContainer:taskSearchSearch']","pdf280","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_TASKSEARCH","//a[@id='workbenchForm:workbenchTabs:taskSearchContainer:tb_buttonSearch']//span[@class='fa fa-search fa-lg']","TGTYPESCREENREG");

        TAP.$("ELE_LINK_VALUEPERUNIT","//span[contains(text(),'Value Per Unit')]","TGTYPESCREENREG");

        WAIT.$(1,"TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_ADD2","//button[@id='workbenchForm:workbenchTabs:tb_buttonAdd']//span[@class='ui-button-text ui-c'][contains(text(),'Add')]","TGTYPESCREENREG");

        WAIT.$(3,"TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_TABLE_VALUEPERUNIT","//input[@id='workbenchForm:workbenchTabs:unitValueTable']","ATTAGE","FALSE","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_ENDINGDURATION","//input[@id='workbenchForm:workbenchTabs:duration_input']",var_EndingDuration,"FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_VALIDATE","//span[contains(text(),'Validate')]","TGTYPESCREENREG");

        ASSERT.$("ELE_MSG_ERRORMESG_BELOWFIELD","//span[@class='ui-message-error-detail']","CONTAINS","Ending duration must be 999 for access method.","TGTYPESCREENFULL");

        TAP.$("ELE_LOGO_GIAS","//span[text()='GIAS']","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

        CALL.$("F227PlanDescriptionValidations","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_SEARCHTASKS","//input[@id='workbenchForm:workbenchTabs:taskSearchContainer:taskSearchSearch']","plan setup","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_TASKSEARCH","//a[@id='workbenchForm:workbenchTabs:taskSearchContainer:tb_buttonSearch']//span[@class='fa fa-search fa-lg']","TGTYPESCREENREG");

        TAP.$("ELE_LINK_PLANSETUP","//span[contains(text(),'Plan Setup')]","TGTYPESCREENREG");

        WAIT.$(1,"TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_PLAN","//input[@id='workbenchForm:workbenchTabs:grid:planCode_Col:filter']",var_Plan,"FALSE","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

        TAP.$("ELE_LINK_ROW1","//tr[@class='ui-widget-content ui-datatable-even ui-datatable-selectable']","TGTYPESCREENREG");

        WAIT.$(3,"TGTYPESCREENREG");

        ASSERT.$("ELE_GETVAL_CASHVALUESALLOWED","//span[@id='workbenchForm:workbenchTabs:cashValuesAllowed']","CONTAINS","No","TGTYPESCREENFULL");

        TAP.$("ELE_BUTTON_EDIT","//span[text()='Edit']","TGTYPESCREENREG");

        WAIT.$(3,"TGTYPESCREENREG");

        TAP.$("ELE_DRPDWN_UNITVALUEACCESSMETHOD","//div[@id='workbenchForm:workbenchTabs:unitValueAccessMthd']//div//span","TGTYPESCREENREG");

        ASSERT.$("ELE_DRPDWN_UNITVALUEACCESSMETHOD_ATTAINEDAGEINSUREDSBIRTHDAY","//li[@id='workbenchForm:workbenchTabs:unitValueAccessMthd_2']","VISIBLE","TGTYPESCREENREG");

        TAP.$("ELE_DRPDWN_INSURANCETYPE","//div[@id='workbenchForm:workbenchTabs:insuranceType']//div//span","TGTYPESCREENREG");

        TAP.$("ELE_DRPDWN_INSURANCETYPE_ORDINARYLIFE","//li[@id='workbenchForm:workbenchTabs:insuranceType_6']","TGTYPESCREENREG");

        ASSERT.$("ELE_GETVAL_MATUREEXPIRYDATECALCMETHOD_INSUREDSBIRTHDAY","//label[@id='workbenchForm:workbenchTabs:matuExpiAgeDateCalc_label']","VISIBLE","TGTYPESCREENREG");

        SWIPE.$("DOWN","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_VALIDATE","//span[contains(text(),'Validate')]","TGTYPESCREENFULL");

        WAIT.$(3,"TGTYPESCREENREG");

        ASSERT.$("ELE_MSG_VALIDATIONSUCCESS","//div[@id='workbenchForm:workbenchTabs:messages']//span[@class='ui-messages-info-summary'][contains(text(),'Validation completed successfully with no errors.')]","VISIBLE","TGTYPESCREENREG");

        TAP.$("ELE_DRPDWN_INSURANCETYPE","//div[@id='workbenchForm:workbenchTabs:insuranceType']//div//span","TGTYPESCREENREG");

        TAP.$("ELE_DRPDWN_INSURANCETYPE_ENDOWMENT","//li[@id='workbenchForm:workbenchTabs:insuranceType_1']","TGTYPESCREENREG");

        ASSERT.$("ELE_GETVAL_MATUREEXPIRYDATECALCMETHOD_INSUREDSBIRTHDAY","//label[@id='workbenchForm:workbenchTabs:matuExpiAgeDateCalc_label']","VISIBLE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_VALIDATE","//span[contains(text(),'Validate')]","TGTYPESCREENREG");

        WAIT.$(3,"TGTYPESCREENREG");

        ASSERT.$("ELE_MSG_VALIDATIONSUCCESS","//div[@id='workbenchForm:workbenchTabs:messages']//span[@class='ui-messages-info-summary'][contains(text(),'Validation completed successfully with no errors.')]","VISIBLE","TGTYPESCREENFULL");

        CALL.$("F227PlanDescriptionErrorValidations","TGTYPESCREENREG");

        TAP.$("ELE_DRPDWN_CASHVALUESALLOWED","//div[@id='workbenchForm:workbenchTabs:cashValuesAllowed']//div//span","TGTYPESCREENREG");

        TAP.$("ELE_DRPDWN_CASHVALUEALLOWED_YES","//li[@id='workbenchForm:workbenchTabs:cashValuesAllowed_1']","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_VALIDATE","//span[contains(text(),'Validate')]","TGTYPESCREENFULL");

        WAIT.$(3,"TGTYPESCREENREG");

        ASSERT.$("ELE_GETMSG_ERROR_UNITVALUEACCESSMETHODISNOTVALID","//span[@class='ui-message-error-detail'][contains(text(),'Unit value access method is not valid.')]","VISIBLE","TGTYPESCREENREG");

        TAP.$("ELE_DRPDWN_CASHVALUESALLOWED","//div[@id='workbenchForm:workbenchTabs:cashValuesAllowed']//div//span","TGTYPESCREENREG");

        TAP.$("ELE_DRPDWN_CASHVALUESALLOWED_NO","//li[@id='workbenchForm:workbenchTabs:cashValuesAllowed_1']","TGTYPESCREENREG");

        SWIPE.$("DOWN","TGTYPESCREENREG");

        SWIPE.$("DOWN","TGTYPESCREENREG");

        SWIPE.$("DOWN","TGTYPESCREENREG");

        TAP.$("ELE_DRPDWN_INSURANCETYPE","//div[@id='workbenchForm:workbenchTabs:insuranceType']//div//span","TGTYPESCREENREG");

        TAP.$("ELE_DRPDWN_INSURANCETYPE_FLIXEDPREMIUMANNUITY","//li[@id='workbenchForm:workbenchTabs:insuranceType_2']","TGTYPESCREENREG");

        ASSERT.$("ELE_GETVAL_MATUREEXPIRYDATECALCMETHOD_INSUREDSBIRTHDAY","//label[@id='workbenchForm:workbenchTabs:matuExpiAgeDateCalc_label']","VISIBLE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_VALIDATE","//span[contains(text(),'Validate')]","TGTYPESCREENFULL");

        WAIT.$(3,"TGTYPESCREENREG");

        ASSERT.$("ELE_GETMSG_ERROR_UNITVALUEACCESSMETHODISNOTVALID","//span[@class='ui-message-error-detail'][contains(text(),'Unit value access method is not valid.')]","VISIBLE","TGTYPESCREENREG");

        TAP.$("ELE_DRPDWN_INSURANCETYPE","//div[@id='workbenchForm:workbenchTabs:insuranceType']//div//span","TGTYPESCREENREG");

        TAP.$("ELE_DRPDWN_INSURANCETYPE_FLEXIBLEPREMIUMANNUITY","//li[@id='workbenchForm:workbenchTabs:insuranceType_3']","TGTYPESCREENREG");

        ASSERT.$("ELE_GETVAL_MATUREEXPIRYDATECALCMETHOD_INSUREDSBIRTHDAY","//label[@id='workbenchForm:workbenchTabs:matuExpiAgeDateCalc_label']","VISIBLE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_VALIDATE","//span[contains(text(),'Validate')]","TGTYPESCREENREG");

        WAIT.$(3,"TGTYPESCREENREG");

        ASSERT.$("ELE_GETMSG_ERROR_UNITVALUEACCESSMETHODISNOTVALID","//span[@class='ui-message-error-detail'][contains(text(),'Unit value access method is not valid.')]","VISIBLE","TGTYPESCREENREG");

        SWIPE.$("DOWN","TGTYPESCREENREG");

        SWIPE.$("DOWN","TGTYPESCREENREG");

        TAP.$("ELE_DRPDWN_INSURANCETYPE","//div[@id='workbenchForm:workbenchTabs:insuranceType']//div//span","TGTYPESCREENREG");

        TAP.$("ELE_DRPDWN_INSURANCETYPE_HEALTH","//li[@id='workbenchForm:workbenchTabs:insuranceType_4']","TGTYPESCREENREG");

        ASSERT.$("ELE_GETVAL_MATUREEXPIRYDATECALCMETHOD_INSUREDSBIRTHDAY","//label[@id='workbenchForm:workbenchTabs:matuExpiAgeDateCalc_label']","VISIBLE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_VALIDATE","//span[contains(text(),'Validate')]","TGTYPESCREENREG");

        WAIT.$(3,"TGTYPESCREENREG");

        ASSERT.$("ELE_GETMSG_ERROR_UNITVALUEACCESSMETHODISNOTVALID","//span[@class='ui-message-error-detail'][contains(text(),'Unit value access method is not valid.')]","VISIBLE","TGTYPESCREENREG");

        SWIPE.$("DOWN","TGTYPESCREENREG");

        SWIPE.$("DOWN","TGTYPESCREENREG");

        SWIPE.$("DOWN","TGTYPESCREENREG");

        SWIPE.$("DOWN","TGTYPESCREENREG");

        SWIPE.$("DOWN","TGTYPESCREENREG");

        SWIPE.$("DOWN","TGTYPESCREENREG");

        TAP.$("ELE_DRPDWN_INSURANCETYPE","//div[@id='workbenchForm:workbenchTabs:insuranceType']//div//span","TGTYPESCREENREG");

        TAP.$("ELE_DRPDWN_INSURANCETYPE_INTERESTSENSITIVEWHOLELIFE","//li[@id='workbenchForm:workbenchTabs:insuranceType_5']","TGTYPESCREENREG");

        ASSERT.$("ELE_GETVAL_MATUREEXPIRYDATECALCMETHOD_INSUREDSBIRTHDAY","//label[@id='workbenchForm:workbenchTabs:matuExpiAgeDateCalc_label']","VISIBLE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_VALIDATE","//span[contains(text(),'Validate')]","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        ASSERT.$("ELE_GETMSG_ERROR_UNITVALUEACCESSMETHODISNOTVALID","//span[@class='ui-message-error-detail'][contains(text(),'Unit value access method is not valid.')]","VISIBLE","TGTYPESCREENREG");

        SWIPE.$("DOWN","TGTYPESCREENREG");

        SWIPE.$("DOWN","TGTYPESCREENREG");

        SWIPE.$("DOWN","TGTYPESCREENREG");

        SWIPE.$("DOWN","TGTYPESCREENREG");

        TAP.$("ELE_DRPDWN_INSURANCETYPE","//div[@id='workbenchForm:workbenchTabs:insuranceType']//div//span","TGTYPESCREENREG");

        TAP.$("ELE_DRPDWN_INSURANCETYPE_SINGLEPREMIUMIMMEDIATEANNUITY","//li[@id='workbenchForm:workbenchTabs:insuranceType_7']","TGTYPESCREENREG");

        ASSERT.$("ELE_GETVAL_MATUREEXPIRYDATECALCMETHOD_INSUREDSBIRTHDAY","//label[@id='workbenchForm:workbenchTabs:matuExpiAgeDateCalc_label']","VISIBLE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_VALIDATE","//span[contains(text(),'Validate')]","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        ASSERT.$("ELE_GETMSG_ERROR_UNITVALUEACCESSMETHODISNOTVALID","//span[@class='ui-message-error-detail'][contains(text(),'Unit value access method is not valid.')]","VISIBLE","TGTYPESCREENREG");

        TAP.$("ELE_DRPDWN_INSURANCETYPE","//div[@id='workbenchForm:workbenchTabs:insuranceType']//div//span","TGTYPESCREENREG");

        TAP.$("ELE_DRPDWN_INSURANCETYPE_SUPPLEMNTALCONTRACT","//li[@id='workbenchForm:workbenchTabs:insuranceType_8']","TGTYPESCREENREG");

        ASSERT.$("ELE_GETVAL_MATUREEXPIRYDATECALCMETHOD_INSUREDSBIRTHDAY","//label[@id='workbenchForm:workbenchTabs:matuExpiAgeDateCalc_label']","VISIBLE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_VALIDATE","//span[contains(text(),'Validate')]","TGTYPESCREENREG");

        WAIT.$(6,"TGTYPESCREENREG");

        ASSERT.$("ELE_GETMSG_ERROR_UNITVALUEACCESSMETHODISNOTVALID","//span[@class='ui-message-error-detail'][contains(text(),'Unit value access method is not valid.')]","VISIBLE","TGTYPESCREENREG");

        TAP.$("ELE_DRPDWN_INSURANCETYPE","//div[@id='workbenchForm:workbenchTabs:insuranceType']//div//span","TGTYPESCREENREG");

        TAP.$("ELE_DRPDWN_INSURANCETYPE_UNIVERSALLIFE","//li[@id='workbenchForm:workbenchTabs:insuranceType_10']","TGTYPESCREENREG");

        ASSERT.$("ELE_GETVAL_MATUREEXPIRYDATECALCMETHOD_INSUREDSBIRTHDAY","//label[@id='workbenchForm:workbenchTabs:matuExpiAgeDateCalc_label']","VISIBLE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_VALIDATE","//span[contains(text(),'Validate')]","TGTYPESCREENREG");

        WAIT.$(3,"TGTYPESCREENREG");

        ASSERT.$("ELE_GETMSG_ERROR_UNITVALUEACCESSMETHODISNOTVALID","//span[@class='ui-message-error-detail'][contains(text(),'Unit value access method is not valid.')]","VISIBLE","TGTYPESCREENFULL");

        TAP.$("ELE_LOGO_GIAS","//span[text()='GIAS']","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

		var_Count = var_Count + ( 1 );
                 LOGGER.info("Executed Step = INCREASEVAR,var_Count,1,TGTYPESCREENREG");
    	}
          LOGGER.info("Executed Step = WEND,TGTYPESCREENREG");
    	resetAttemps();

    }


    @Test
    public void ftj41modf226sprint09() {

        // No need to declare / print method name here, handled by TestNg listener
        // declare("ftj41modf226sprint09");

		int var_Count = 0;
                 LOGGER.info("Executed Step = VAR,Integer,var_Count,0,TGTYPESCREENREG");
		com.jayway.jsonpath.ReadContext var_CSVData = getJsonData("http://10.1.104.229/json/20200818/sDGAZa.json");
                 LOGGER.info("Executed Step = VAR,Json,var_CSVData,http://10.1.104.229/json/20200818/sDGAZa.json,TGTYPESCREENREG");
        LOGGER.info("Executed Step = REPEAT IF");
    	while (check(var_Count,"<",1,"TGTYPESCREENREG")) {
          LOGGER.info("Executed Step = WHILE,var_Count,<,1,TGTYPESCREENREG");
		String var_Plan;
                 LOGGER.info("Executed Step = VAR,String,var_Plan,TGTYPESCREENREG");
		var_Plan = getJsonData(var_CSVData , "$.records["+var_Count+"].Plan");
                 LOGGER.info("Executed Step = STORE,var_Plan,var_CSVData,$.records[{var_Count}].Plan,TGTYPESCREENREG");
        CALL.$("FTJQA2Login","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

		try { 
		 WebElement html = driver.findElement(By.tagName("html"));
		    	
		    	System.out.println("html size" + html.getSize());
		    	System.out.println("html" + html.getTagName());

		    	for( int i =0 ;i<4;i++) {
		        html.sendKeys(Keys.chord(Keys.CONTROL, Keys.SUBTRACT));
		    	}
		 
		 ;} 
		 catch(Exception e) { PrintVal("Renderer Script Exception : "+e.toString()); } 
		 PrintVal("Executed Step = CALLRENDERERSCRIPT,FTJ41ZOOMMAGNIFICATIONCHANGE"); 
        TYPE.$("ELE_TXTBOX_USERNAME","//input[@id='guestForm:username']","x5080","FALSE","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_PASSWORD","//input[@id='guestForm:password']","Welcome123$","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_LOGIN","//button[@id='guestForm:loginButton']","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        CALL.$("F226PlanDescriptionPremiumPageBB655","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_SEARCHTASKS","//input[@id='workbenchForm:workbenchTabs:taskSearchContainer:taskSearchSearch']","plan setup","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_TASKSEARCH","//a[@id='workbenchForm:workbenchTabs:taskSearchContainer:tb_buttonSearch']//span[@class='fa fa-search fa-lg']","TGTYPESCREENREG");

        TAP.$("ELE_LINK_PLANSETUP","//span[contains(text(),'Plan Setup')]","TGTYPESCREENREG");

        WAIT.$(1,"TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_PLAN","//input[@id='workbenchForm:workbenchTabs:grid:planCode_Col:filter']",var_Plan,"FALSE","TGTYPESCREENREG");

        WAIT.$(1,"TGTYPESCREENREG");

        TAP.$("ELE_LINK_ROW1","//tr[@class='ui-widget-content ui-datatable-even ui-datatable-selectable']","TGTYPESCREENREG");

        WAIT.$(3,"TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_EDIT","//span[text()='Edit']","TGTYPESCREENREG");

        WAIT.$(3,"TGTYPESCREENREG");

        TAP.$("ELE_DRPDWN_INSURANCETYPE","//div[@id='workbenchForm:workbenchTabs:insuranceType']//div//span","TGTYPESCREENREG");

        TAP.$("ELE_DRPDWN_INSURANCETYPE_UNIVERSALLIFE","//li[@id='workbenchForm:workbenchTabs:insuranceType_10']","TGTYPESCREENREG");

        SWIPE.$("DOWN","TGTYPESCREENREG");

        SWIPE.$("DOWN","TGTYPESCREENREG");

        SWIPE.$("DOWN","TGTYPESCREENREG");

        TAP.$("ELE_LINK_PREMIUMS","//span[contains(text(),'Premiums')]","TGTYPESCREENREG");

        TAP.$("ELE_DRPDWN_TABLEACCESSMETHOD","//div[@id='workbenchForm:workbenchTabs:premiumRateAcceMthd']//div//span","TGTYPESCREENREG");

        TAP.$("ELE_DRPDWNTABLE_ACCESSMETHOD_ATTAINEDAGEINSUREDSBIRTHDAY","//li[@id='workbenchForm:workbenchTabs:premiumRateAcceMthd_2']","TGTYPESCREENREG");

        TAP.$("ELE_DRPDWN_PREMIUMRATELOOKUPDATE","//div[@id='workbenchForm:workbenchTabs:premiumRateLookDate']//div//span","TGTYPESCREENREG");

        TAP.$("ELE_DRPDWN_PREMIUMRATELOOKUPDATE_ATTAINEDAGEPTD","//li[contains(text(),'Attained Age, Paid To Date')]","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_VALIDATE","//span[contains(text(),'Validate')]","TGTYPESCREENFULL");

        WAIT.$(3,"TGTYPESCREENREG");

        ASSERT.$("ELE_MSG_ERRORMESG_BELOWFIELD","//span[@class='ui-message-error-detail']","CONTAINS","Table access method is not valid for insurance type.","TGTYPESCREENREG");

        SWIPE.$("DOWN","TGTYPESCREENREG");

        SWIPE.$("DOWN","TGTYPESCREENREG");

        SWIPE.$("DOWN","TGTYPESCREENREG");

        SWIPE.$("DOWN","TGTYPESCREENREG");

        SWIPE.$("DOWN","TGTYPESCREENREG");

        SWIPE.$("DOWN","TGTYPESCREENREG");

        SWIPE.$("DOWN","TGTYPESCREENREG");

        SWIPE.$("DOWN","TGTYPESCREENREG");

        TAP.$("ELE_TAB_BASICINSURANCEINFORMATION","//span[contains(text(),'Basic Insurance Information')]","TGTYPESCREENREG");

        TAP.$("ELE_DRPDWN_INSURANCETYPE","//div[@id='workbenchForm:workbenchTabs:insuranceType']//div//span","TGTYPESCREENREG");

        TAP.$("ELE_DRPDWN_INSURANCETYPE_FLEXIBLEPREMIUMANNUITY","//li[@id='workbenchForm:workbenchTabs:insuranceType_3']","TGTYPESCREENREG");

        SWIPE.$("DOWN","TGTYPESCREENREG");

        SWIPE.$("DOWN","TGTYPESCREENREG");

        SWIPE.$("DOWN","TGTYPESCREENREG");

        TAP.$("ELE_LINK_PREMIUMS","//span[contains(text(),'Premiums')]","TGTYPESCREENREG");

        WAIT.$(1,"TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_VALIDATE","//span[contains(text(),'Validate')]","TGTYPESCREENFULL");

        WAIT.$(3,"TGTYPESCREENREG");

        ASSERT.$("ELE_MSG_ERRORMESG_BELOWFIELD","//span[@class='ui-message-error-detail']","CONTAINS","Table access method is not valid for insurance type.","TGTYPESCREENREG");

        SWIPE.$("DOWN","TGTYPESCREENREG");

        SWIPE.$("DOWN","TGTYPESCREENREG");

        SWIPE.$("DOWN","TGTYPESCREENREG");

        SWIPE.$("DOWN","TGTYPESCREENREG");

        SWIPE.$("DOWN","TGTYPESCREENREG");

        SWIPE.$("DOWN","TGTYPESCREENREG");

        SWIPE.$("DOWN","TGTYPESCREENREG");

        SWIPE.$("DOWN","TGTYPESCREENREG");

        TAP.$("ELE_TAB_BASICINSURANCEINFORMATION","//span[contains(text(),'Basic Insurance Information')]","TGTYPESCREENREG");

        WAIT.$(1,"TGTYPESCREENREG");

        TAP.$("ELE_DRPDWN_INSURANCETYPE","//div[@id='workbenchForm:workbenchTabs:insuranceType']//div//span","TGTYPESCREENREG");

        TAP.$("ELE_DRPDWN_INSURANCETYPE_TERM","//li[@id='workbenchForm:workbenchTabs:insuranceType_9']","TGTYPESCREENREG");

        CALL.$("BB655ErrorAndSuccessValidationChecks","TGTYPESCREENREG");

        SWIPE.$("DOWN","TGTYPESCREENREG");

        SWIPE.$("DOWN","TGTYPESCREENREG");

        TAP.$("ELE_LINK_PREMIUMS","//span[contains(text(),'Premiums')]","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

        TAP.$("ELE_DRPDWN_LEVELPREMIUMS","//div[@id='workbenchForm:workbenchTabs:levelPremiumRates']//div//span","TGTYPESCREENREG");

        TAP.$("ELE_DRPDWN_LEVELPREMIUM_NONLEVELPREMIUMRATES","//li[contains(text(),'Nonlevel Premium Rates')]","TGTYPESCREENREG");

        TAP.$("ELE_DRPDWN_PREMIUMCALCULATIONFORMULA","//div[@id='workbenchForm:workbenchTabs:premiumCalcForm']//div//span","TGTYPESCREENREG");

        TAP.$("ELE_DRPDWN_PREMIUMCALCULATIONFORMULA_CALCULATEUSINGRATEPERUNIT","//li[@id='workbenchForm:workbenchTabs:premiumCalcForm_5']","TGTYPESCREENREG");

        TAP.$("ELE_DRPDWN_PREMIUMRATELOOKUPDATE","//div[@id='workbenchForm:workbenchTabs:premiumRateLookDate']//div//span","TGTYPESCREENREG");

        TAP.$("ELE_DRPDWN_PREMIUMRATELOOKUPDATE_ATTAINEDAGEPTD","//li[contains(text(),'Attained Age, Paid To Date')]","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_VALIDATE","//span[contains(text(),'Validate')]","TGTYPESCREENFULL");

        WAIT.$(6,"TGTYPESCREENREG");

        ASSERT.$("ELE_MSG_VALIDATIONSUCCESS","//div[@id='workbenchForm:workbenchTabs:messages']//span[@class='ui-messages-info-summary'][contains(text(),'Validation completed successfully with no errors.')]","VISIBLE","TGTYPESCREENREG");

        SWIPE.$("DOWN","TGTYPESCREENREG");

        SWIPE.$("DOWN","TGTYPESCREENREG");

        SWIPE.$("DOWN","TGTYPESCREENREG");

        SWIPE.$("DOWN","TGTYPESCREENREG");

        SWIPE.$("DOWN","TGTYPESCREENREG");

        SWIPE.$("DOWN","TGTYPESCREENREG");

        TAP.$("ELE_DRPDWN_LEVELPREMIUMS","//div[@id='workbenchForm:workbenchTabs:levelPremiumRates']//div//span","TGTYPESCREENREG");

        TAP.$("ELE_DRPDWN_LEVELPREMIUM_LEVELPREMIUMRATES","//li[contains(text(),'Level Premium Rates')]","TGTYPESCREENREG");

        TAP.$("ELE_DRPDWN_PREMIUMCALCULATIONFORMULA","//div[@id='workbenchForm:workbenchTabs:premiumCalcForm']//div//span","TGTYPESCREENREG");

        TAP.$("ELE_DRPDWN_PREMIUMCALCULATIONFORMULA_DONOTCALCULATEPREMIUM","//li[contains(text(),'Do not calculate premium')]","TGTYPESCREENREG");

        TAP.$("ELE_DRPDWN_PREMIUMRATELOOKUPDATE","//div[@id='workbenchForm:workbenchTabs:premiumRateLookDate']//div//span","TGTYPESCREENREG");

        TAP.$("ELE_DRPDWN_PREMIUMRATELOOKUPDATE_POLICYISSUEDATE","//li[contains(text(),'Policy Issue Date')]","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_VALIDATE","//span[contains(text(),'Validate')]","TGTYPESCREENREG");

        WAIT.$(3,"TGTYPESCREENREG");

        ASSERT.$("ELE_ERRORMSG_TABLEACCESSMETHODISNOTVALIDFORPREMIUMCALCULATIONFORMULA","//div[@id='workbenchForm:workbenchTabs:messages']//span[@class='ui-messages-error-summary'][contains(text(),'Table access method is not valid for premium calcu')]","VISIBLE","TGTYPESCREENREG");

        ASSERT.$("ELE_ERRORMSG_TABLEACCESSMETHODISONLYVALIDWHENLEVELPREMIUMISNONLEVELPREMIUMRATES","//div[@id='workbenchForm:workbenchTabs:messages']//span[@class='ui-messages-error-summary'][contains(text(),'Table access method is only valid when level premi')]","VISIBLE","TGTYPESCREENREG");

        ASSERT.$("ELE_ERRORMSG_PREMIUMRATELOOKUPDATEMUTBEATTAINEDAGEPTDFORTABLEACCESSMETHOD","//div[@id='workbenchForm:workbenchTabs:messages']//span[@class='ui-messages-error-summary'][contains(text(),'Premium rate lookup date must be Attained Age, Pai')]","VISIBLE","TGTYPESCREENFULL");

        SWIPE.$("DOWN","TGTYPESCREENREG");

        SWIPE.$("DOWN","TGTYPESCREENREG");

        SWIPE.$("DOWN","TGTYPESCREENREG");

        SWIPE.$("DOWN","TGTYPESCREENREG");

        SWIPE.$("DOWN","TGTYPESCREENREG");

        SWIPE.$("DOWN","TGTYPESCREENREG");

        SWIPE.$("DOWN","TGTYPESCREENREG");

        SWIPE.$("DOWN","TGTYPESCREENREG");

        SWIPE.$("DOWN","TGTYPESCREENREG");

        SWIPE.$("DOWN","TGTYPESCREENREG");

        TAP.$("ELE_TAB_BASICINSURANCEINFORMATION","//span[contains(text(),'Basic Insurance Information')]","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

        TAP.$("ELE_DRPDWN_INSURANCETYPE","//div[@id='workbenchForm:workbenchTabs:insuranceType']//div//span","TGTYPESCREENREG");

        TAP.$("ELE_DRPDWN_INSURANCETYPE_HEALTH","//li[@id='workbenchForm:workbenchTabs:insuranceType_4']","TGTYPESCREENREG");

        CALL.$("BB655ErrorAndSuccessValidationChecks","TGTYPESCREENREG");

        SWIPE.$("DOWN","TGTYPESCREENREG");

        SWIPE.$("DOWN","TGTYPESCREENREG");

        TAP.$("ELE_LINK_PREMIUMS","//span[contains(text(),'Premiums')]","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

        TAP.$("ELE_DRPDWN_LEVELPREMIUMS","//div[@id='workbenchForm:workbenchTabs:levelPremiumRates']//div//span","TGTYPESCREENREG");

        TAP.$("ELE_DRPDWN_LEVELPREMIUM_NONLEVELPREMIUMRATES","//li[contains(text(),'Nonlevel Premium Rates')]","TGTYPESCREENREG");

        TAP.$("ELE_DRPDWN_PREMIUMCALCULATIONFORMULA","//div[@id='workbenchForm:workbenchTabs:premiumCalcForm']//div//span","TGTYPESCREENREG");

        TAP.$("ELE_DRPDWN_PREMIUMCALCULATIONFORMULA_CALCULATEUSINGRATEPERUNIT","//li[@id='workbenchForm:workbenchTabs:premiumCalcForm_5']","TGTYPESCREENREG");

        TAP.$("ELE_DRPDWN_PREMIUMRATELOOKUPDATE","//div[@id='workbenchForm:workbenchTabs:premiumRateLookDate']//div//span","TGTYPESCREENREG");

        TAP.$("ELE_DRPDWN_PREMIUMRATELOOKUPDATE_ATTAINEDAGEPTD","//li[contains(text(),'Attained Age, Paid To Date')]","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_VALIDATE","//span[contains(text(),'Validate')]","TGTYPESCREENFULL");

        WAIT.$(6,"TGTYPESCREENREG");

        ASSERT.$("ELE_MSG_VALIDATIONSUCCESS","//div[@id='workbenchForm:workbenchTabs:messages']//span[@class='ui-messages-info-summary'][contains(text(),'Validation completed successfully with no errors.')]","VISIBLE","TGTYPESCREENREG");

        SWIPE.$("DOWN","TGTYPESCREENREG");

        SWIPE.$("DOWN","TGTYPESCREENREG");

        SWIPE.$("DOWN","TGTYPESCREENREG");

        SWIPE.$("DOWN","TGTYPESCREENREG");

        SWIPE.$("DOWN","TGTYPESCREENREG");

        SWIPE.$("DOWN","TGTYPESCREENREG");

        TAP.$("ELE_DRPDWN_LEVELPREMIUMS","//div[@id='workbenchForm:workbenchTabs:levelPremiumRates']//div//span","TGTYPESCREENREG");

        TAP.$("ELE_DRPDWN_LEVELPREMIUM_LEVELPREMIUMRATES","//li[contains(text(),'Level Premium Rates')]","TGTYPESCREENREG");

        TAP.$("ELE_DRPDWN_PREMIUMCALCULATIONFORMULA","//div[@id='workbenchForm:workbenchTabs:premiumCalcForm']//div//span","TGTYPESCREENREG");

        TAP.$("ELE_DRPDWN_PREMIUMCALCULATIONFORMULA_DONOTCALCULATEPREMIUM","//li[contains(text(),'Do not calculate premium')]","TGTYPESCREENREG");

        TAP.$("ELE_DRPDWN_PREMIUMRATELOOKUPDATE","//div[@id='workbenchForm:workbenchTabs:premiumRateLookDate']//div//span","TGTYPESCREENREG");

        TAP.$("ELE_DRPDWN_PREMIUMRATELOOKUPDATE_POLICYISSUEDATE","//li[contains(text(),'Policy Issue Date')]","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_VALIDATE","//span[contains(text(),'Validate')]","TGTYPESCREENREG");

        WAIT.$(3,"TGTYPESCREENREG");

        ASSERT.$("ELE_ERRORMSG_TABLEACCESSMETHODISNOTVALIDFORPREMIUMCALCULATIONFORMULA","//div[@id='workbenchForm:workbenchTabs:messages']//span[@class='ui-messages-error-summary'][contains(text(),'Table access method is not valid for premium calcu')]","VISIBLE","TGTYPESCREENREG");

        ASSERT.$("ELE_ERRORMSG_TABLEACCESSMETHODISONLYVALIDWHENLEVELPREMIUMISNONLEVELPREMIUMRATES","//div[@id='workbenchForm:workbenchTabs:messages']//span[@class='ui-messages-error-summary'][contains(text(),'Table access method is only valid when level premi')]","VISIBLE","TGTYPESCREENREG");

        ASSERT.$("ELE_ERRORMSG_PREMIUMRATELOOKUPDATEMUTBEATTAINEDAGEPTDFORTABLEACCESSMETHOD","//div[@id='workbenchForm:workbenchTabs:messages']//span[@class='ui-messages-error-summary'][contains(text(),'Premium rate lookup date must be Attained Age, Pai')]","VISIBLE","TGTYPESCREENFULL");

        SWIPE.$("DOWN","TGTYPESCREENREG");

        SWIPE.$("DOWN","TGTYPESCREENREG");

        SWIPE.$("DOWN","TGTYPESCREENREG");

        SWIPE.$("DOWN","TGTYPESCREENREG");

        SWIPE.$("DOWN","TGTYPESCREENREG");

        SWIPE.$("DOWN","TGTYPESCREENREG");

        SWIPE.$("DOWN","TGTYPESCREENREG");

        SWIPE.$("DOWN","TGTYPESCREENREG");

        SWIPE.$("DOWN","TGTYPESCREENREG");

        SWIPE.$("DOWN","TGTYPESCREENREG");

        TAP.$("ELE_TAB_BASICINSURANCEINFORMATION","//span[contains(text(),'Basic Insurance Information')]","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

        TAP.$("ELE_DRPDWN_INSURANCETYPE","//div[@id='workbenchForm:workbenchTabs:insuranceType']//div//span","TGTYPESCREENREG");

        TAP.$("ELE_DRPDWN_INSURANCETYPE_ORDINARYLIFE","//li[@id='workbenchForm:workbenchTabs:insuranceType_6']","TGTYPESCREENREG");

        CALL.$("BB655ErrorAndSuccessValidationChecks","TGTYPESCREENREG");

        SWIPE.$("DOWN","TGTYPESCREENREG");

        SWIPE.$("DOWN","TGTYPESCREENREG");

        TAP.$("ELE_LINK_PREMIUMS","//span[contains(text(),'Premiums')]","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

        TAP.$("ELE_DRPDWN_LEVELPREMIUMS","//div[@id='workbenchForm:workbenchTabs:levelPremiumRates']//div//span","TGTYPESCREENREG");

        TAP.$("ELE_DRPDWN_LEVELPREMIUM_NONLEVELPREMIUMRATES","//li[contains(text(),'Nonlevel Premium Rates')]","TGTYPESCREENREG");

        TAP.$("ELE_DRPDWN_PREMIUMCALCULATIONFORMULA","//div[@id='workbenchForm:workbenchTabs:premiumCalcForm']//div//span","TGTYPESCREENREG");

        TAP.$("ELE_DRPDWN_PREMIUMCALCULATIONFORMULA_CALCULATEUSINGRATEPERUNIT","//li[@id='workbenchForm:workbenchTabs:premiumCalcForm_5']","TGTYPESCREENREG");

        TAP.$("ELE_DRPDWN_PREMIUMRATELOOKUPDATE","//div[@id='workbenchForm:workbenchTabs:premiumRateLookDate']//div//span","TGTYPESCREENREG");

        TAP.$("ELE_DRPDWN_PREMIUMRATELOOKUPDATE_ATTAINEDAGEPTD","//li[contains(text(),'Attained Age, Paid To Date')]","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_VALIDATE","//span[contains(text(),'Validate')]","TGTYPESCREENFULL");

        WAIT.$(6,"TGTYPESCREENREG");

        ASSERT.$("ELE_MSG_VALIDATIONSUCCESS","//div[@id='workbenchForm:workbenchTabs:messages']//span[@class='ui-messages-info-summary'][contains(text(),'Validation completed successfully with no errors.')]","VISIBLE","TGTYPESCREENREG");

        SWIPE.$("DOWN","TGTYPESCREENREG");

        SWIPE.$("DOWN","TGTYPESCREENREG");

        SWIPE.$("DOWN","TGTYPESCREENREG");

        SWIPE.$("DOWN","TGTYPESCREENREG");

        SWIPE.$("DOWN","TGTYPESCREENREG");

        SWIPE.$("DOWN","TGTYPESCREENREG");

        TAP.$("ELE_DRPDWN_LEVELPREMIUMS","//div[@id='workbenchForm:workbenchTabs:levelPremiumRates']//div//span","TGTYPESCREENREG");

        TAP.$("ELE_DRPDWN_LEVELPREMIUM_LEVELPREMIUMRATES","//li[contains(text(),'Level Premium Rates')]","TGTYPESCREENREG");

        TAP.$("ELE_DRPDWN_PREMIUMCALCULATIONFORMULA","//div[@id='workbenchForm:workbenchTabs:premiumCalcForm']//div//span","TGTYPESCREENREG");

        TAP.$("ELE_DRPDWN_PREMIUMCALCULATIONFORMULA_DONOTCALCULATEPREMIUM","//li[contains(text(),'Do not calculate premium')]","TGTYPESCREENREG");

        TAP.$("ELE_DRPDWN_PREMIUMRATELOOKUPDATE","//div[@id='workbenchForm:workbenchTabs:premiumRateLookDate']//div//span","TGTYPESCREENREG");

        TAP.$("ELE_DRPDWN_PREMIUMRATELOOKUPDATE_POLICYISSUEDATE","//li[contains(text(),'Policy Issue Date')]","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_VALIDATE","//span[contains(text(),'Validate')]","TGTYPESCREENREG");

        WAIT.$(3,"TGTYPESCREENREG");

        ASSERT.$("ELE_ERRORMSG_TABLEACCESSMETHODISNOTVALIDFORPREMIUMCALCULATIONFORMULA","//div[@id='workbenchForm:workbenchTabs:messages']//span[@class='ui-messages-error-summary'][contains(text(),'Table access method is not valid for premium calcu')]","VISIBLE","TGTYPESCREENREG");

        ASSERT.$("ELE_ERRORMSG_TABLEACCESSMETHODISONLYVALIDWHENLEVELPREMIUMISNONLEVELPREMIUMRATES","//div[@id='workbenchForm:workbenchTabs:messages']//span[@class='ui-messages-error-summary'][contains(text(),'Table access method is only valid when level premi')]","VISIBLE","TGTYPESCREENREG");

        ASSERT.$("ELE_ERRORMSG_PREMIUMRATELOOKUPDATEMUTBEATTAINEDAGEPTDFORTABLEACCESSMETHOD","//div[@id='workbenchForm:workbenchTabs:messages']//span[@class='ui-messages-error-summary'][contains(text(),'Premium rate lookup date must be Attained Age, Pai')]","VISIBLE","TGTYPESCREENFULL");

        SWIPE.$("DOWN","TGTYPESCREENREG");

        SWIPE.$("DOWN","TGTYPESCREENREG");

        SWIPE.$("DOWN","TGTYPESCREENREG");

        SWIPE.$("DOWN","TGTYPESCREENREG");

        SWIPE.$("DOWN","TGTYPESCREENREG");

        SWIPE.$("DOWN","TGTYPESCREENREG");

        SWIPE.$("DOWN","TGTYPESCREENREG");

        SWIPE.$("DOWN","TGTYPESCREENREG");

        SWIPE.$("DOWN","TGTYPESCREENREG");

        SWIPE.$("DOWN","TGTYPESCREENREG");

        TAP.$("ELE_TAB_BASICINSURANCEINFORMATION","//span[contains(text(),'Basic Insurance Information')]","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

        TAP.$("ELE_DRPDWN_INSURANCETYPE","//div[@id='workbenchForm:workbenchTabs:insuranceType']//div//span","TGTYPESCREENREG");

        TAP.$("ELE_DRPDWN_INSURANCETYPE_INTERESTSENSITIVEWHOLELIFE","//li[@id='workbenchForm:workbenchTabs:insuranceType_5']","TGTYPESCREENREG");

        CALL.$("BB655ErrorAndSuccessValidationChecks","TGTYPESCREENREG");

        SWIPE.$("DOWN","TGTYPESCREENREG");

        SWIPE.$("DOWN","TGTYPESCREENREG");

        TAP.$("ELE_LINK_PREMIUMS","//span[contains(text(),'Premiums')]","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

        TAP.$("ELE_DRPDWN_LEVELPREMIUMS","//div[@id='workbenchForm:workbenchTabs:levelPremiumRates']//div//span","TGTYPESCREENREG");

        TAP.$("ELE_DRPDWN_LEVELPREMIUM_NONLEVELPREMIUMRATES","//li[contains(text(),'Nonlevel Premium Rates')]","TGTYPESCREENREG");

        TAP.$("ELE_DRPDWN_PREMIUMCALCULATIONFORMULA","//div[@id='workbenchForm:workbenchTabs:premiumCalcForm']//div//span","TGTYPESCREENREG");

        TAP.$("ELE_DRPDWN_PREMIUMCALCULATIONFORMULA_CALCULATEUSINGRATEPERUNIT","//li[@id='workbenchForm:workbenchTabs:premiumCalcForm_5']","TGTYPESCREENREG");

        TAP.$("ELE_DRPDWN_PREMIUMRATELOOKUPDATE","//div[@id='workbenchForm:workbenchTabs:premiumRateLookDate']//div//span","TGTYPESCREENREG");

        TAP.$("ELE_DRPDWN_PREMIUMRATELOOKUPDATE_ATTAINEDAGEPTD","//li[contains(text(),'Attained Age, Paid To Date')]","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_VALIDATE","//span[contains(text(),'Validate')]","TGTYPESCREENFULL");

        WAIT.$(6,"TGTYPESCREENREG");

        ASSERT.$("ELE_MSG_VALIDATIONSUCCESS","//div[@id='workbenchForm:workbenchTabs:messages']//span[@class='ui-messages-info-summary'][contains(text(),'Validation completed successfully with no errors.')]","VISIBLE","TGTYPESCREENREG");

        SWIPE.$("DOWN","TGTYPESCREENREG");

        SWIPE.$("DOWN","TGTYPESCREENREG");

        SWIPE.$("DOWN","TGTYPESCREENREG");

        SWIPE.$("DOWN","TGTYPESCREENREG");

        SWIPE.$("DOWN","TGTYPESCREENREG");

        SWIPE.$("DOWN","TGTYPESCREENREG");

        TAP.$("ELE_DRPDWN_LEVELPREMIUMS","//div[@id='workbenchForm:workbenchTabs:levelPremiumRates']//div//span","TGTYPESCREENREG");

        TAP.$("ELE_DRPDWN_LEVELPREMIUM_LEVELPREMIUMRATES","//li[contains(text(),'Level Premium Rates')]","TGTYPESCREENREG");

        TAP.$("ELE_DRPDWN_PREMIUMCALCULATIONFORMULA","//div[@id='workbenchForm:workbenchTabs:premiumCalcForm']//div//span","TGTYPESCREENREG");

        TAP.$("ELE_DRPDWN_PREMIUMCALCULATIONFORMULA_DONOTCALCULATEPREMIUM","//li[contains(text(),'Do not calculate premium')]","TGTYPESCREENREG");

        TAP.$("ELE_DRPDWN_PREMIUMRATELOOKUPDATE","//div[@id='workbenchForm:workbenchTabs:premiumRateLookDate']//div//span","TGTYPESCREENREG");

        TAP.$("ELE_DRPDWN_PREMIUMRATELOOKUPDATE_POLICYISSUEDATE","//li[contains(text(),'Policy Issue Date')]","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_VALIDATE","//span[contains(text(),'Validate')]","TGTYPESCREENREG");

        WAIT.$(3,"TGTYPESCREENREG");

        ASSERT.$("ELE_ERRORMSG_TABLEACCESSMETHODISNOTVALIDFORPREMIUMCALCULATIONFORMULA","//div[@id='workbenchForm:workbenchTabs:messages']//span[@class='ui-messages-error-summary'][contains(text(),'Table access method is not valid for premium calcu')]","VISIBLE","TGTYPESCREENREG");

        ASSERT.$("ELE_ERRORMSG_TABLEACCESSMETHODISONLYVALIDWHENLEVELPREMIUMISNONLEVELPREMIUMRATES","//div[@id='workbenchForm:workbenchTabs:messages']//span[@class='ui-messages-error-summary'][contains(text(),'Table access method is only valid when level premi')]","VISIBLE","TGTYPESCREENREG");

        ASSERT.$("ELE_ERRORMSG_PREMIUMRATELOOKUPDATEMUTBEATTAINEDAGEPTDFORTABLEACCESSMETHOD","//div[@id='workbenchForm:workbenchTabs:messages']//span[@class='ui-messages-error-summary'][contains(text(),'Premium rate lookup date must be Attained Age, Pai')]","VISIBLE","TGTYPESCREENFULL");

        SWIPE.$("DOWN","TGTYPESCREENREG");

        SWIPE.$("DOWN","TGTYPESCREENREG");

        SWIPE.$("DOWN","TGTYPESCREENREG");

        SWIPE.$("DOWN","TGTYPESCREENREG");

        SWIPE.$("DOWN","TGTYPESCREENREG");

        SWIPE.$("DOWN","TGTYPESCREENREG");

        SWIPE.$("DOWN","TGTYPESCREENREG");

        SWIPE.$("DOWN","TGTYPESCREENREG");

        SWIPE.$("DOWN","TGTYPESCREENREG");

        SWIPE.$("DOWN","TGTYPESCREENREG");

        TAP.$("ELE_TAB_BASICINSURANCEINFORMATION","//span[contains(text(),'Basic Insurance Information')]","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

        TAP.$("ELE_DRPDWN_INSURANCETYPE","//div[@id='workbenchForm:workbenchTabs:insuranceType']//div//span","TGTYPESCREENREG");

        TAP.$("ELE_DRPDWN_INSURANCETYPE_ENDOWMENT","//li[@id='workbenchForm:workbenchTabs:insuranceType_1']","TGTYPESCREENREG");

        CALL.$("BB655ErrorAndSuccessValidationChecks","TGTYPESCREENREG");

        SWIPE.$("DOWN","TGTYPESCREENREG");

        SWIPE.$("DOWN","TGTYPESCREENREG");

        TAP.$("ELE_LINK_PREMIUMS","//span[contains(text(),'Premiums')]","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

        TAP.$("ELE_DRPDWN_LEVELPREMIUMS","//div[@id='workbenchForm:workbenchTabs:levelPremiumRates']//div//span","TGTYPESCREENREG");

        TAP.$("ELE_DRPDWN_LEVELPREMIUM_NONLEVELPREMIUMRATES","//li[contains(text(),'Nonlevel Premium Rates')]","TGTYPESCREENREG");

        TAP.$("ELE_DRPDWN_PREMIUMCALCULATIONFORMULA","//div[@id='workbenchForm:workbenchTabs:premiumCalcForm']//div//span","TGTYPESCREENREG");

        TAP.$("ELE_DRPDWN_PREMIUMCALCULATIONFORMULA_CALCULATEUSINGRATEPERUNIT","//li[@id='workbenchForm:workbenchTabs:premiumCalcForm_5']","TGTYPESCREENREG");

        TAP.$("ELE_DRPDWN_PREMIUMRATELOOKUPDATE","//div[@id='workbenchForm:workbenchTabs:premiumRateLookDate']//div//span","TGTYPESCREENREG");

        TAP.$("ELE_DRPDWN_PREMIUMRATELOOKUPDATE_ATTAINEDAGEPTD","//li[contains(text(),'Attained Age, Paid To Date')]","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_VALIDATE","//span[contains(text(),'Validate')]","TGTYPESCREENFULL");

        WAIT.$(6,"TGTYPESCREENREG");

        ASSERT.$("ELE_MSG_VALIDATIONSUCCESS","//div[@id='workbenchForm:workbenchTabs:messages']//span[@class='ui-messages-info-summary'][contains(text(),'Validation completed successfully with no errors.')]","VISIBLE","TGTYPESCREENREG");

        SWIPE.$("DOWN","TGTYPESCREENREG");

        SWIPE.$("DOWN","TGTYPESCREENREG");

        SWIPE.$("DOWN","TGTYPESCREENREG");

        SWIPE.$("DOWN","TGTYPESCREENREG");

        SWIPE.$("DOWN","TGTYPESCREENREG");

        SWIPE.$("DOWN","TGTYPESCREENREG");

        TAP.$("ELE_DRPDWN_LEVELPREMIUMS","//div[@id='workbenchForm:workbenchTabs:levelPremiumRates']//div//span","TGTYPESCREENREG");

        TAP.$("ELE_DRPDWN_LEVELPREMIUM_LEVELPREMIUMRATES","//li[contains(text(),'Level Premium Rates')]","TGTYPESCREENREG");

        TAP.$("ELE_DRPDWN_PREMIUMCALCULATIONFORMULA","//div[@id='workbenchForm:workbenchTabs:premiumCalcForm']//div//span","TGTYPESCREENREG");

        TAP.$("ELE_DRPDWN_PREMIUMCALCULATIONFORMULA_DONOTCALCULATEPREMIUM","//li[contains(text(),'Do not calculate premium')]","TGTYPESCREENREG");

        TAP.$("ELE_DRPDWN_PREMIUMRATELOOKUPDATE","//div[@id='workbenchForm:workbenchTabs:premiumRateLookDate']//div//span","TGTYPESCREENREG");

        TAP.$("ELE_DRPDWN_PREMIUMRATELOOKUPDATE_POLICYISSUEDATE","//li[contains(text(),'Policy Issue Date')]","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_VALIDATE","//span[contains(text(),'Validate')]","TGTYPESCREENREG");

        WAIT.$(3,"TGTYPESCREENREG");

        ASSERT.$("ELE_ERRORMSG_TABLEACCESSMETHODISNOTVALIDFORPREMIUMCALCULATIONFORMULA","//div[@id='workbenchForm:workbenchTabs:messages']//span[@class='ui-messages-error-summary'][contains(text(),'Table access method is not valid for premium calcu')]","VISIBLE","TGTYPESCREENREG");

        ASSERT.$("ELE_ERRORMSG_TABLEACCESSMETHODISONLYVALIDWHENLEVELPREMIUMISNONLEVELPREMIUMRATES","//div[@id='workbenchForm:workbenchTabs:messages']//span[@class='ui-messages-error-summary'][contains(text(),'Table access method is only valid when level premi')]","VISIBLE","TGTYPESCREENREG");

        ASSERT.$("ELE_ERRORMSG_PREMIUMRATELOOKUPDATEMUTBEATTAINEDAGEPTDFORTABLEACCESSMETHOD","//div[@id='workbenchForm:workbenchTabs:messages']//span[@class='ui-messages-error-summary'][contains(text(),'Premium rate lookup date must be Attained Age, Pai')]","VISIBLE","TGTYPESCREENFULL");

        SWIPE.$("DOWN","TGTYPESCREENREG");

        SWIPE.$("DOWN","TGTYPESCREENREG");

        SWIPE.$("DOWN","TGTYPESCREENREG");

        SWIPE.$("DOWN","TGTYPESCREENREG");

        SWIPE.$("DOWN","TGTYPESCREENREG");

        SWIPE.$("DOWN","TGTYPESCREENREG");

        SWIPE.$("DOWN","TGTYPESCREENREG");

        SWIPE.$("DOWN","TGTYPESCREENREG");

        SWIPE.$("DOWN","TGTYPESCREENREG");

        SWIPE.$("DOWN","TGTYPESCREENREG");

        TAP.$("ELE_TAB_BASICINSURANCEINFORMATION","//span[contains(text(),'Basic Insurance Information')]","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

        TAP.$("ELE_DRPDWN_INSURANCETYPE","//div[@id='workbenchForm:workbenchTabs:insuranceType']//div//span","TGTYPESCREENREG");

        TAP.$("ELE_DRPDWN_INSURANCETYPE_FLIXEDPREMIUMANNUITY","//li[@id='workbenchForm:workbenchTabs:insuranceType_2']","TGTYPESCREENREG");

        CALL.$("BB655ErrorAndSuccessValidationChecks","TGTYPESCREENREG");

        SWIPE.$("DOWN","TGTYPESCREENREG");

        SWIPE.$("DOWN","TGTYPESCREENREG");

        TAP.$("ELE_LINK_PREMIUMS","//span[contains(text(),'Premiums')]","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

        TAP.$("ELE_DRPDWN_LEVELPREMIUMS","//div[@id='workbenchForm:workbenchTabs:levelPremiumRates']//div//span","TGTYPESCREENREG");

        TAP.$("ELE_DRPDWN_LEVELPREMIUM_NONLEVELPREMIUMRATES","//li[contains(text(),'Nonlevel Premium Rates')]","TGTYPESCREENREG");

        TAP.$("ELE_DRPDWN_PREMIUMCALCULATIONFORMULA","//div[@id='workbenchForm:workbenchTabs:premiumCalcForm']//div//span","TGTYPESCREENREG");

        TAP.$("ELE_DRPDWN_PREMIUMCALCULATIONFORMULA_CALCULATEUSINGRATEPERUNIT","//li[@id='workbenchForm:workbenchTabs:premiumCalcForm_5']","TGTYPESCREENREG");

        TAP.$("ELE_DRPDWN_PREMIUMRATELOOKUPDATE","//div[@id='workbenchForm:workbenchTabs:premiumRateLookDate']//div//span","TGTYPESCREENREG");

        TAP.$("ELE_DRPDWN_PREMIUMRATELOOKUPDATE_ATTAINEDAGEPTD","//li[contains(text(),'Attained Age, Paid To Date')]","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_VALIDATE","//span[contains(text(),'Validate')]","TGTYPESCREENFULL");

        WAIT.$(6,"TGTYPESCREENREG");

        ASSERT.$("ELE_MSG_VALIDATIONSUCCESS","//div[@id='workbenchForm:workbenchTabs:messages']//span[@class='ui-messages-info-summary'][contains(text(),'Validation completed successfully with no errors.')]","VISIBLE","TGTYPESCREENREG");

        SWIPE.$("DOWN","TGTYPESCREENREG");

        SWIPE.$("DOWN","TGTYPESCREENREG");

        SWIPE.$("DOWN","TGTYPESCREENREG");

        SWIPE.$("DOWN","TGTYPESCREENREG");

        SWIPE.$("DOWN","TGTYPESCREENREG");

        SWIPE.$("DOWN","TGTYPESCREENREG");

        TAP.$("ELE_DRPDWN_LEVELPREMIUMS","//div[@id='workbenchForm:workbenchTabs:levelPremiumRates']//div//span","TGTYPESCREENREG");

        TAP.$("ELE_DRPDWN_LEVELPREMIUM_LEVELPREMIUMRATES","//li[contains(text(),'Level Premium Rates')]","TGTYPESCREENREG");

        TAP.$("ELE_DRPDWN_PREMIUMCALCULATIONFORMULA","//div[@id='workbenchForm:workbenchTabs:premiumCalcForm']//div//span","TGTYPESCREENREG");

        TAP.$("ELE_DRPDWN_PREMIUMCALCULATIONFORMULA_DONOTCALCULATEPREMIUM","//li[contains(text(),'Do not calculate premium')]","TGTYPESCREENREG");

        TAP.$("ELE_DRPDWN_PREMIUMRATELOOKUPDATE","//div[@id='workbenchForm:workbenchTabs:premiumRateLookDate']//div//span","TGTYPESCREENREG");

        TAP.$("ELE_DRPDWN_PREMIUMRATELOOKUPDATE_POLICYISSUEDATE","//li[contains(text(),'Policy Issue Date')]","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_VALIDATE","//span[contains(text(),'Validate')]","TGTYPESCREENREG");

        WAIT.$(3,"TGTYPESCREENREG");

        ASSERT.$("ELE_ERRORMSG_TABLEACCESSMETHODISNOTVALIDFORPREMIUMCALCULATIONFORMULA","//div[@id='workbenchForm:workbenchTabs:messages']//span[@class='ui-messages-error-summary'][contains(text(),'Table access method is not valid for premium calcu')]","VISIBLE","TGTYPESCREENREG");

        ASSERT.$("ELE_ERRORMSG_TABLEACCESSMETHODISONLYVALIDWHENLEVELPREMIUMISNONLEVELPREMIUMRATES","//div[@id='workbenchForm:workbenchTabs:messages']//span[@class='ui-messages-error-summary'][contains(text(),'Table access method is only valid when level premi')]","VISIBLE","TGTYPESCREENREG");

        ASSERT.$("ELE_ERRORMSG_PREMIUMRATELOOKUPDATEMUTBEATTAINEDAGEPTDFORTABLEACCESSMETHOD","//div[@id='workbenchForm:workbenchTabs:messages']//span[@class='ui-messages-error-summary'][contains(text(),'Premium rate lookup date must be Attained Age, Pai')]","VISIBLE","TGTYPESCREENFULL");

        SWIPE.$("DOWN","TGTYPESCREENREG");

        SWIPE.$("DOWN","TGTYPESCREENREG");

        SWIPE.$("DOWN","TGTYPESCREENREG");

        SWIPE.$("DOWN","TGTYPESCREENREG");

        SWIPE.$("DOWN","TGTYPESCREENREG");

        SWIPE.$("DOWN","TGTYPESCREENREG");

        SWIPE.$("DOWN","TGTYPESCREENREG");

        SWIPE.$("DOWN","TGTYPESCREENREG");

        SWIPE.$("DOWN","TGTYPESCREENREG");

        SWIPE.$("DOWN","TGTYPESCREENREG");

        TAP.$("ELE_TAB_BASICINSURANCEINFORMATION","//span[contains(text(),'Basic Insurance Information')]","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

        WAIT.$(1,"TGTYPESCREENREG");

		var_Count = var_Count + ( 1 );
                 LOGGER.info("Executed Step = INCREASEVAR,var_Count,1,TGTYPESCREENREG");
    	}
          LOGGER.info("Executed Step = WEND,TGTYPESCREENREG");
    	resetAttemps();

    }


    @Test
    public void ftj41f226part2temp() {

        // No need to declare / print method name here, handled by TestNg listener
        // declare("ftj41f226part2temp");

		int var_Count = 0;
                 LOGGER.info("Executed Step = VAR,Integer,var_Count,0,TGTYPESCREENREG");
		com.jayway.jsonpath.ReadContext var_CSVData = getJsonData("http://10.1.104.229/json/20200818/4aVJBt.json");
                 LOGGER.info("Executed Step = VAR,Json,var_CSVData,http://10.1.104.229/json/20200818/4aVJBt.json,TGTYPESCREENREG");
        LOGGER.info("Executed Step = REPEAT IF");
    	while (check(var_Count,"<",1,"TGTYPESCREENREG")) {
          LOGGER.info("Executed Step = WHILE,var_Count,<,1,TGTYPESCREENREG");
		String var_Policy1;
                 LOGGER.info("Executed Step = VAR,String,var_Policy1,TGTYPESCREENREG");
		var_Policy1 = getJsonData(var_CSVData , "$.records["+var_Count+"].Policy1");
                 LOGGER.info("Executed Step = STORE,var_Policy1,var_CSVData,$.records[{var_Count}].Policy1,TGTYPESCREENREG");
		String var_SessionDate1;
                 LOGGER.info("Executed Step = VAR,String,var_SessionDate1,TGTYPESCREENREG");
		var_SessionDate1 = getJsonData(var_CSVData , "$.records["+var_Count+"].SessionDate1");
                 LOGGER.info("Executed Step = STORE,var_SessionDate1,var_CSVData,$.records[{var_Count}].SessionDate1,TGTYPESCREENREG");
        CALL.$("FTJQA2Login","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

		try { 
		 WebElement html = driver.findElement(By.tagName("html"));
		    	
		    	System.out.println("html size" + html.getSize());
		    	System.out.println("html" + html.getTagName());

		    	for( int i =0 ;i<4;i++) {
		        html.sendKeys(Keys.chord(Keys.CONTROL, Keys.SUBTRACT));
		    	}
		 
		 ;} 
		 catch(Exception e) { PrintVal("Renderer Script Exception : "+e.toString()); } 
		 PrintVal("Executed Step = CALLRENDERERSCRIPT,FTJ41ZOOMMAGNIFICATIONCHANGE"); 
        TYPE.$("ELE_TXTBOX_USERNAME","//input[@id='guestForm:username']","x5080","FALSE","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_PASSWORD","//input[@id='guestForm:password']","Welcome123$","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_LOGIN","//button[@id='guestForm:loginButton']","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        CALL.$("AdvancingPTDThroughPolicyPayment","TGTYPESCREENREG");

        TAP.$("ELE_CLICK_CHANGESESSIONDATE","//a[@id='headerForm:headerDateChange']//span[@class='fa fa-pencil-square-o']","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_NEWSESSIONDATE","//input[@id='sessionDateForm:sessionDateInput_input']",var_SessionDate1,"FALSE","TGTYPESCREENREG");

        TAP.$("ELE_CHANGEDATE_BUTTON","//span[contains(text(),'Change Date')]","TGTYPESCREENREG");

        WAIT.$(1,"TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_SEARCHTASKS","//input[@id='workbenchForm:workbenchTabs:taskSearchContainer:taskSearchSearch']","Policy Inquiry","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_TASKSEARCH","//a[@id='workbenchForm:workbenchTabs:taskSearchContainer:tb_buttonSearch']//span[@class='fa fa-search fa-lg']","TGTYPESCREENREG");

        TAP.$("ELE_LINK_POLICYINQUIRY","//span[contains(text(),'Policy Inquiry')]","TGTYPESCREENREG");

        WAIT.$(1,"TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_POLICYNUMBER","//input[@id='workbenchForm:workbenchTabs:policyNumber']",var_Policy1,"FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_CONTINUE","//span[contains(text(),'Continue')]","TGTYPESCREENREG");

        WAIT.$(10,"TGTYPESCREENREG");

        WAIT.$("ELE_LINK_BASEPLAN_ROW1","//tr[@class='ui-widget-content ui-datatable-even ui-datatable-selectable']","VISIBLE","TGTYPESCREENREG");

        TAP.$("ELE_LINK_BASEPLAN_ROW1","//tr[@class='ui-widget-content ui-datatable-even ui-datatable-selectable']","TGTYPESCREENFULL");

        WAIT.$(5,"TGTYPESCREENREG");

		String var_StartingAnnualBase;
                 LOGGER.info("Executed Step = VAR,String,var_StartingAnnualBase,TGTYPESCREENREG");
		var_StartingAnnualBase = ActionWrapper.getElementValue("ELE_LABEL_TOTALANNUAL", "//span[@id='workbenchForm:workbenchTabs:annualPremium']");
                 LOGGER.info("Executed Step = STORE,var_StartingAnnualBase,ELE_LABEL_TOTALANNUAL,TGTYPESCREENREG");
		String var_StartingModalPremiumBase;
                 LOGGER.info("Executed Step = VAR,String,var_StartingModalPremiumBase,TGTYPESCREENREG");
		var_StartingModalPremiumBase = ActionWrapper.getElementValue("ELE_GETVALUE_MODALPREMIUM", "//table[@id='workbenchForm:workbenchTabs:premiumInformation_tabPanel']//span[@id='workbenchForm:workbenchTabs:modalPremium']");
                 LOGGER.info("Executed Step = STORE,var_StartingModalPremiumBase,ELE_GETVALUE_MODALPREMIUM,TGTYPESCREENREG");
        TAP.$("ELE_TABCLOSE5","//li[@class='ui-state-default ui-tabs-selected ui-state-active ui-corner-top']//span[@class='ui-icon ui-icon-close']","TGTYPESCREENREG");

        WAIT.$(6,"TGTYPESCREENREG");

        TAP.$("ELE_LINK_ROW2","//tr[@class='ui-widget-content ui-datatable-odd ui-datatable-selectable']","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

		String var_StartingAnnualRider1;
                 LOGGER.info("Executed Step = VAR,String,var_StartingAnnualRider1,TGTYPESCREENREG");
		var_StartingAnnualRider1 = ActionWrapper.getElementValue("ELE_LABEL_TOTALANNUAL", "//span[@id='workbenchForm:workbenchTabs:annualPremium']");
                 LOGGER.info("Executed Step = STORE,var_StartingAnnualRider1,ELE_LABEL_TOTALANNUAL,TGTYPESCREENREG");
		String var_StartingModalPremiumRider1;
                 LOGGER.info("Executed Step = VAR,String,var_StartingModalPremiumRider1,TGTYPESCREENREG");
		var_StartingModalPremiumRider1 = ActionWrapper.getElementValue("ELE_GETVALUE_MODALPREMIUM", "//table[@id='workbenchForm:workbenchTabs:premiumInformation_tabPanel']//span[@id='workbenchForm:workbenchTabs:modalPremium']");
                 LOGGER.info("Executed Step = STORE,var_StartingModalPremiumRider1,ELE_GETVALUE_MODALPREMIUM,TGTYPESCREENREG");
        TAP.$("ELE_TABCLOSE5","//li[@class='ui-state-default ui-tabs-selected ui-state-active ui-corner-top']//span[@class='ui-icon ui-icon-close']","TGTYPESCREENREG");

        WAIT.$(6,"TGTYPESCREENREG");

        TAP.$("ELE_LINK_ROW3","//div[@id='workbenchForm:workbenchTabs:benefitTable']//tr[3]","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

		String var_StartingAnnualRider2;
                 LOGGER.info("Executed Step = VAR,String,var_StartingAnnualRider2,TGTYPESCREENREG");
		var_StartingAnnualRider2 = ActionWrapper.getElementValue("ELE_LABEL_TOTALANNUAL", "//span[@id='workbenchForm:workbenchTabs:annualPremium']");
                 LOGGER.info("Executed Step = STORE,var_StartingAnnualRider2,ELE_LABEL_TOTALANNUAL,TGTYPESCREENREG");
		String var_StartingModalPremiumRider2;
                 LOGGER.info("Executed Step = VAR,String,var_StartingModalPremiumRider2,TGTYPESCREENREG");
		var_StartingModalPremiumRider2 = ActionWrapper.getElementValue("ELE_GETVALUE_MODALPREMIUM", "//table[@id='workbenchForm:workbenchTabs:premiumInformation_tabPanel']//span[@id='workbenchForm:workbenchTabs:modalPremium']");
                 LOGGER.info("Executed Step = STORE,var_StartingModalPremiumRider2,ELE_GETVALUE_MODALPREMIUM,TGTYPESCREENREG");
        TAP.$("ELE_LOGO_GIAS","//span[text()='GIAS']","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

        SWIPE.$("DOWN","TGTYPESCREENREG");

        SWIPE.$("DOWN","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_SEARCHTASKS","//input[@id='workbenchForm:workbenchTabs:taskSearchContainer:taskSearchSearch']","BLC420","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_TASKSEARCH","//a[@id='workbenchForm:workbenchTabs:taskSearchContainer:tb_buttonSearch']//span[@class='fa fa-search fa-lg']","TGTYPESCREENREG");

        TAP.$("ELE_LABEL_POLICYPAYMENT","//body/div[@id='Outer40']/div[@id='ContentPanel']/div[@id='Content40']/form[@id='workbenchForm']/div[@id='PageTiles']/div[@id='workbenchForm:workbenchTabs']/div[1]/div[1]/div[1]/div[2]/div[2]/div[1]/div[1]/div[1]/div[3]/div[1]/div[1]/button[1]/span[2]","TGTYPESCREENREG");

        WAIT.$(3,"TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_POLICYNUMBER","//input[@id='workbenchForm:workbenchTabs:policyNumber']",var_Policy1,"FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_CONTINUE","//span[contains(text(),'Continue')]","TGTYPESCREENREG");

        WAIT.$(4,"TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SUBMIT","//span[contains(text(),'Submit')]","TGTYPESCREENREG");

        WAIT.$(8,"TGTYPESCREENREG");

        ASSERT.$("ELE_GETVAL_SUCCESSMESSAGE","//div[@id='workbenchForm:workbenchTabs:messages']//span[@class='ui-messages-info-summary'][contains(text(),'Request completed successfully.')]","VISIBLE","TGTYPESCREENREG");

        TAP.$("ELE_LOGO_GIAS","//span[text()='GIAS']","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

        SWIPE.$("DOWN","TGTYPESCREENREG");

        SWIPE.$("DOWN","TGTYPESCREENREG");

        CALL.$("F226PolicyPaymentReversal","TGTYPESCREENREG");

        SWIPE.$("DOWN","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_SEARCHTASKS","//input[@id='workbenchForm:workbenchTabs:taskSearchContainer:taskSearchSearch']","blc550","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_TASKSEARCH","//a[@id='workbenchForm:workbenchTabs:taskSearchContainer:tb_buttonSearch']//span[@class='fa fa-search fa-lg']","TGTYPESCREENREG");

        TAP.$("ELE_LABEL_POLICYPAYMENTREVERSAL","//span[contains(text(),'Policy Payment Reversal')]","TGTYPESCREENREG");

        WAIT.$(1,"TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_POLICYNUMBER","//input[@id='workbenchForm:workbenchTabs:policyNumber']",var_Policy1,"FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_CONTINUE","//span[contains(text(),'Continue')]","TGTYPESCREENREG");

        WAIT.$(3,"TGTYPESCREENREG");

        TAP.$("ELE_LINK_PAYMENTREVERSAL_ROW1","//tbody[@id='workbenchForm:workbenchTabs:grid_data']//tr[1]","TGTYPESCREENREG");

        WAIT.$(12,"TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SUBMIT","//span[contains(text(),'Submit')]","TGTYPESCREENFULL");

        WAIT.$(2,"TGTYPESCREENREG");

        ASSERT.$("ELE_GETVAL_SUCCESSMESSAGE","//div[@id='workbenchForm:workbenchTabs:messages']//span[@class='ui-messages-info-summary'][contains(text(),'Request completed successfully.')]","VISIBLE","TGTYPESCREENREG");

        TAP.$("ELE_LOGO_GIAS","//span[text()='GIAS']","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

        SWIPE.$("DOWN","TGTYPESCREENREG");

        SWIPE.$("DOWN","TGTYPESCREENREG");

        CALL.$("ValidatePolicyReversedAnnualandModalPremiums","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_SEARCHTASKS","//input[@id='workbenchForm:workbenchTabs:taskSearchContainer:taskSearchSearch']","Policy Inquiry","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_TASKSEARCH","//a[@id='workbenchForm:workbenchTabs:taskSearchContainer:tb_buttonSearch']//span[@class='fa fa-search fa-lg']","TGTYPESCREENREG");

        TAP.$("ELE_LINK_POLICYINQUIRY","//span[contains(text(),'Policy Inquiry')]","TGTYPESCREENREG");

        WAIT.$(1,"TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_POLICYNUMBER","//input[@id='workbenchForm:workbenchTabs:policyNumber']",var_Policy1,"FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_CONTINUE","//span[contains(text(),'Continue')]","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        TAP.$("ELE_LINK_BASEPLAN_ROW1","//tr[@class='ui-widget-content ui-datatable-even ui-datatable-selectable']","TGTYPESCREENFULL");

        WAIT.$(5,"TGTYPESCREENREG");

        ASSERT.$("ELE_LABEL_TOTALANNUAL","//span[@id='workbenchForm:workbenchTabs:annualPremium']","=",var_StartingAnnualBase,"TGTYPESCREENREG");

        ASSERT.$("ELE_GETVALUE_MODALPREMIUM","//table[@id='workbenchForm:workbenchTabs:premiumInformation_tabPanel']//span[@id='workbenchForm:workbenchTabs:modalPremium']","=",var_StartingModalPremiumBase,"TGTYPESCREENFULL");

        TAP.$("ELE_TABCLOSE5","//li[@class='ui-state-default ui-tabs-selected ui-state-active ui-corner-top']//span[@class='ui-icon ui-icon-close']","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        TAP.$("ELE_LINK_ROW2","//tr[@class='ui-widget-content ui-datatable-odd ui-datatable-selectable']","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        ASSERT.$("ELE_LABEL_TOTALANNUAL","//span[@id='workbenchForm:workbenchTabs:annualPremium']","=",var_StartingAnnualRider1,"TGTYPESCREENREG");

        ASSERT.$("ELE_GETVALUE_MODALPREMIUM","//table[@id='workbenchForm:workbenchTabs:premiumInformation_tabPanel']//span[@id='workbenchForm:workbenchTabs:modalPremium']","=",var_StartingModalPremiumRider1,"TGTYPESCREENFULL");

        TAP.$("ELE_TABCLOSE5","//li[@class='ui-state-default ui-tabs-selected ui-state-active ui-corner-top']//span[@class='ui-icon ui-icon-close']","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        TAP.$("ELE_LINK_ROW3","//div[@id='workbenchForm:workbenchTabs:benefitTable']//tr[3]","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        ASSERT.$("ELE_LABEL_TOTALANNUAL","//span[@id='workbenchForm:workbenchTabs:annualPremium']","=",var_StartingAnnualRider2,"TGTYPESCREENREG");

        ASSERT.$("ELE_GETVALUE_MODALPREMIUM","//table[@id='workbenchForm:workbenchTabs:premiumInformation_tabPanel']//span[@id='workbenchForm:workbenchTabs:modalPremium']","=",var_StartingModalPremiumRider2,"TGTYPESCREENFULL");

        TAP.$("ELE_LOGO_GIAS","//span[text()='GIAS']","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

        SWIPE.$("DOWN","TGTYPESCREENREG");

        SWIPE.$("DOWN","TGTYPESCREENREG");

		var_Count = var_Count + ( 1 );
                 LOGGER.info("Executed Step = INCREASEVAR,var_Count,1,TGTYPESCREENREG");
    	}
          LOGGER.info("Executed Step = WEND,TGTYPESCREENREG");
    	resetAttemps();

    }


    @Test
    public void ftj41f227payupandexpirydatevalidations() {

        // No need to declare / print method name here, handled by TestNg listener
        // declare("ftj41f227payupandexpirydatevalidations");

		com.jayway.jsonpath.ReadContext var_CSVData = getJsonData("http://10.1.104.229/json/20201009/hoMBQn.json");
                 LOGGER.info("Executed Step = VAR,Json,var_CSVData,http://10.1.104.229/json/20201009/hoMBQn.json,TGTYPESCREENREG");
		String var_PolicyNumber = "null";
                 LOGGER.info("Executed Step = VAR,String,var_PolicyNumber,null,TGTYPESCREENREG");
		String var_SessionDate = "null";
                 LOGGER.info("Executed Step = VAR,String,var_SessionDate,null,TGTYPESCREENREG");
		String var_SessionDate1 = "null";
                 LOGGER.info("Executed Step = VAR,String,var_SessionDate1,null,TGTYPESCREENREG");
		int var_Count = 0;
                 LOGGER.info("Executed Step = VAR,Integer,var_Count,0,TGTYPESCREENREG");
		String var_dataToPass = "null";
                 LOGGER.info("Executed Step = VAR,String,var_dataToPass,null,TGTYPESCREENREG");
		String var_PolicyStatus = "null";
                 LOGGER.info("Executed Step = VAR,String,var_PolicyStatus,null,TGTYPESCREENREG");
		String var_TaxQualifiedDescription = "null";
                 LOGGER.info("Executed Step = VAR,String,var_TaxQualifiedDescription,null,TGTYPESCREENREG");
		String var_CashWithApplication = "null";
                 LOGGER.info("Executed Step = VAR,String,var_CashWithApplication,null,TGTYPESCREENREG");
		String var_TaxWithholding = "null";
                 LOGGER.info("Executed Step = VAR,String,var_TaxWithholding,null,TGTYPESCREENREG");
		String var_SSNNumber = "null";
                 LOGGER.info("Executed Step = VAR,String,var_SSNNumber,null,TGTYPESCREENREG");
		String var_DateOfBirth = "null";
                 LOGGER.info("Executed Step = VAR,String,var_DateOfBirth,null,TGTYPESCREENREG");
		String var_PaidUpDate = "null";
                 LOGGER.info("Executed Step = VAR,String,var_PaidUpDate,null,TGTYPESCREENREG");
		String var_ExpirationDate = "null";
                 LOGGER.info("Executed Step = VAR,String,var_ExpirationDate,null,TGTYPESCREENREG");
        CALL.$("FTJQA2Login","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

		try { 
		 WebElement html = driver.findElement(By.tagName("html"));
		    	
		    	System.out.println("html size" + html.getSize());
		    	System.out.println("html" + html.getTagName());

		    	for( int i =0 ;i<4;i++) {
		        html.sendKeys(Keys.chord(Keys.CONTROL, Keys.SUBTRACT));
		    	}
		 
		 ;} 
		 catch(Exception e) { PrintVal("Renderer Script Exception : "+e.toString()); } 
		 PrintVal("Executed Step = CALLRENDERERSCRIPT,FTJ41ZOOMMAGNIFICATIONCHANGE"); 
        TYPE.$("ELE_TXTBOX_USERNAME","//input[@id='guestForm:username']","x5080","FALSE","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_PASSWORD","//input[@id='guestForm:password']","Welcome123$","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_LOGIN","//button[@id='guestForm:loginButton']","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        LOGGER.info("Executed Step = REPEAT IF");
    	while (check(var_Count,"<",1,"TGTYPESCREENREG")) {
          LOGGER.info("Executed Step = WHILE,var_Count,<,1,TGTYPESCREENREG");
		var_PolicyStatus = getJsonData(var_CSVData , "$.records["+var_Count+"].PolicyStatus");
                 LOGGER.info("Executed Step = STORE,var_PolicyStatus,var_CSVData,$.records[{var_Count}].PolicyStatus,TGTYPESCREENREG");
		var_SessionDate = getJsonData(var_CSVData , "$.records["+var_Count+"].SessionDate");
                 LOGGER.info("Executed Step = STORE,var_SessionDate,var_CSVData,$.records[{var_Count}].SessionDate,TGTYPESCREENREG");
		var_TaxQualifiedDescription = getJsonData(var_CSVData , "$.records["+var_Count+"].TaxQualifiedDescription");
                 LOGGER.info("Executed Step = STORE,var_TaxQualifiedDescription,var_CSVData,$.records[{var_Count}].TaxQualifiedDescription,TGTYPESCREENREG");
		var_PolicyNumber = getJsonData(var_CSVData , "$.records["+var_Count+"].PolicyNumber");
                 LOGGER.info("Executed Step = STORE,var_PolicyNumber,var_CSVData,$.records[{var_Count}].PolicyNumber,TGTYPESCREENREG");
		var_CashWithApplication = getJsonData(var_CSVData , "$.records["+var_Count+"].CashWithApplication");
                 LOGGER.info("Executed Step = STORE,var_CashWithApplication,var_CSVData,$.records[{var_Count}].CashWithApplication,TGTYPESCREENREG");
		var_TaxWithholding = getJsonData(var_CSVData , "$.records["+var_Count+"].TaxWithholding");
                 LOGGER.info("Executed Step = STORE,var_TaxWithholding,var_CSVData,$.records[{var_Count}].TaxWithholding,TGTYPESCREENREG");
		var_SSNNumber = getJsonData(var_CSVData , "$.records["+var_Count+"].SocialSecurityNumber");
                 LOGGER.info("Executed Step = STORE,var_SSNNumber,var_CSVData,$.records[{var_Count}].SocialSecurityNumber,TGTYPESCREENREG");
		var_DateOfBirth = getJsonData(var_CSVData , "$.records["+var_Count+"].ClientDOB");
                 LOGGER.info("Executed Step = STORE,var_DateOfBirth,var_CSVData,$.records[{var_Count}].ClientDOB,TGTYPESCREENREG");
		var_PaidUpDate = getJsonData(var_CSVData , "$.records["+var_Count+"].PaidUpDate");
                 LOGGER.info("Executed Step = STORE,var_PaidUpDate,var_CSVData,$.records[{var_Count}].PaidUpDate,TGTYPESCREENREG");
		var_ExpirationDate = getJsonData(var_CSVData , "$.records["+var_Count+"].ExpirationDate");
                 LOGGER.info("Executed Step = STORE,var_ExpirationDate,var_CSVData,$.records[{var_Count}].ExpirationDate,TGTYPESCREENREG");
        CALL.$("CheckIfPolicyExists","TGTYPESCREENREG");

        TAP.$("ELE_TAB_MYDASHBOARD","//a[contains(text(),'My Dashboard')]","TGTYPESCREENREG");

        WAIT.$(1,"TGTYPESCREENREG");

        TAP.$("ELE_TXTBOX_SEARCHTASKS","//input[@id='workbenchForm:workbenchTabs:taskSearchContainer:taskSearchSearch']","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_SEARCHTASKS","//input[@id='workbenchForm:workbenchTabs:taskSearchContainer:taskSearchSearch']","Policy Inquiry","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_TASKSEARCH","//a[@id='workbenchForm:workbenchTabs:taskSearchContainer:tb_buttonSearch']//span[@class='fa fa-search fa-lg']","TGTYPESCREENREG");

        TAP.$("ELE_LINK_POLICYINQUIRY","//span[contains(text(),'Policy Inquiry')]","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_POLICYNUMBER","//input[@id='workbenchForm:workbenchTabs:policyNumber']",var_PolicyNumber,"FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_CONTINUE","//span[contains(text(),'Continue')]","TGTYPESCREENREG");

        LOGGER.info("Executed Step = START IF");
        if (check("ELE_GETMSG_ERROR","//div[@id='workbenchForm:workbenchTabs:messages']//span[@class='ui-messages-error-summary']","INVISIBLE","TGTYPESCREENREG")) {
         LOGGER.info("Executed Step = IF,ELE_GETMSG_ERROR,INVISIBLE,TGTYPESCREENREG");
		PrintVal("Log : POLICY FOUND");
                 LOGGER.info("Executed Step = PRINTLOG,POLICY FOUND,TGTYPESCREENREG");
		try { 
		 driver.quit();
		 
		 ;} 
		 catch(Exception e) { PrintVal("Renderer Script Exception : "+e.toString()); } 
		 PrintVal("Executed Step = CALLRENDERERSCRIPT,EXITTHEPROGRAM"); 
        }
         else {
          LOGGER.info("Executed Step = ELSE,TGTYPESCREENREG");
        CALL.$("CheckIfClientExists","TGTYPESCREENREG");

        TAP.$("ELE_TAB_MYDASHBOARD","//a[contains(text(),'My Dashboard')]","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_SEARCHTASKS","//input[@id='workbenchForm:workbenchTabs:taskSearchContainer:taskSearchSearch']","Client Search","FALSE","TGTYPESCREENREG");

        WAIT.$(1,"TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_TASKSEARCH","//a[@id='workbenchForm:workbenchTabs:taskSearchContainer:tb_buttonSearch']//span[@class='fa fa-search fa-lg']","TGTYPESCREENREG");

        TAP.$("ELE_LINK_CLIENTSEARCH","//body/div[@id='Outer40']/div[@id='ContentPanel']/div[@id='Content40']/form[@id='workbenchForm']/div[@id='PageTiles']/div[@id='workbenchForm:workbenchTabs']/div[1]/div[1]/div[1]/div[2]/div[2]/div[1]/div[1]/div[1]/div[3]/div[1]/div[1]/button[1]/span[2]","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_IDNUMCLIENTSEARCH","//input[@id='workbenchForm:workbenchTabs:clientGrid:clientId_Col:filter']",var_SSNNumber,"FALSE","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_DOBCLIENTSEARCHGRID","//th[@id='workbenchForm:workbenchTabs:clientGrid:dateOfBirthDate_Col']//input[@class='ui-inputfield ui-widget ui-state-default ui-corner-all hasDatepicker']",var_DateOfBirth,"FALSE","TGTYPESCREENREG");

        WAIT.$(3,"TGTYPESCREENREG");

        LOGGER.info("Executed Step = START IF");
        if (check("ELE_GETVAL_IFCLIENTEXISTSFIRSTGRD","//tr[@class='ui-widget-content ui-datatable-even ui-datatable-selectable']","VISIBLE","TGTYPESCREENREG")) {
         LOGGER.info("Executed Step = IF,ELE_GETVAL_IFCLIENTEXISTSFIRSTGRD,VISIBLE,TGTYPESCREENREG");
		PrintVal("Log : CLIENT ALREEADY EXISTS. PlEASE ADD UNIQUE CLIENT");
                 LOGGER.info("Executed Step = PRINTLOG,CLIENT ALREEADY EXISTS. PlEASE ADD UNIQUE CLIENT,TGTYPESCREENREG");
		try { 
		 driver.quit();
		 
		 ;} 
		 catch(Exception e) { PrintVal("Renderer Script Exception : "+e.toString()); } 
		 PrintVal("Executed Step = CALLRENDERERSCRIPT,EXITTHEPROGRAM"); 
        }
         else {
          LOGGER.info("Executed Step = ELSE,TGTYPESCREENREG");
        ASSERT.$("ELE_GETVAL_NOTCLIENTFOUNDGRID","//td[contains(text(),'No records found.')]","CONTAINS","No records found","TGTYPESCREENREG");

        }
          LOGGER.info("Executed Step = ENDIF,TGTYPESCREENREG");
        TAP.$("ELE_TAB_CLOSE1","span.ui-icon.ui-icon-close:nth-child(2)TGWEBCOMMACssSelectorTGWEBCOMMA1",1,0,"TGTYPESCREENREG");

        WAIT.$(1,"TGTYPESCREENREG");

        TAP.$("ELE_TAB_CLOSE0","span.ui-icon.ui-icon-close:nth-child(2)TGWEBCOMMACssSelectorTGWEBCOMMA0",1,0,"TGTYPESCREENREG");

        CALL.$("FTJ41ChangeSessionDate","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SESSIONDATE","//a[@id='headerForm:sessionDatelink']","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_NEWSESSIONDATE","//input[@id='sessionDateForm:sessionDateInput_input']",var_SessionDate,"FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_CHANGEDATE","//span[text()='Change Date']","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

        CALL.$("AddPolicyContract","TGTYPESCREENREG");

        TAP.$("ELE_TAB_MYDASHBOARD","//a[contains(text(),'My Dashboard')]","TGTYPESCREENREG");

        WAIT.$(1,"TGTYPESCREENREG");

        TAP.$("ELE_TXTBOX_SEARCHTASKS","//input[@id='workbenchForm:workbenchTabs:taskSearchContainer:taskSearchSearch']","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_SEARCHTASKS","//input[@id='workbenchForm:workbenchTabs:taskSearchContainer:taskSearchSearch']","New Policy Application","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_TASKSEARCH","//a[@id='workbenchForm:workbenchTabs:taskSearchContainer:tb_buttonSearch']//span[@class='fa fa-search fa-lg']","TGTYPESCREENREG");

        TAP.$("ELE_LINK_APPLICATIONENTRYUPDATE","//span[contains(text(),'New Policy Application')]","TGTYPESCREENREG");

        WAIT.$(3,"TGTYPESCREENREG");

        TAP.$("ELE_TAB_APPLICATIONENTRYUPDATE","//a[contains(text(),'Application Entry/Update')]","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_ADD","//span[contains(text(),'Add')]","TGTYPESCREENREG");

        WAIT.$("ELE_TXTBOX_POLICYNUMBER","//input[@id='workbenchForm:workbenchTabs:policyNumber']","VISIBLE","TGTYPESCREENREG");

		var_dataToPass = getJsonData(var_CSVData , "$.records["+var_Count+"].PaymentFrequency");
                 LOGGER.info("Executed Step = STORE,var_dataToPass,var_CSVData,$.records[{var_Count}].PaymentFrequency,TGTYPESCREENREG");
        TYPE.$("ELE_TXTBOX_POLICYNUMBER","//input[@id='workbenchForm:workbenchTabs:policyNumber']",var_PolicyNumber,"FALSE","TGTYPESCREENREG");

        TAP.$("ELE_DROPDWN_PAYFREQ","//label[@id='workbenchForm:workbenchTabs:paymentMode_label']","TGTYPESCREENREG");

		try { 
		 Thread.sleep(1000);
		WebElement selectValueOfDropDown= driver.findElement(By.xpath("//li[@data-label='"+ var_dataToPass +"']"));

		takeScreenshot();

				selectValueOfDropDown.click();
		// Add Screenshot will only work for Android platform

		takeScreenshot();

				 Thread.sleep(1000);
		 
		 ;} 
		 catch(Exception e) { PrintVal("Renderer Script Exception : "+e.toString()); } 
		 PrintVal("Executed Step = CALLRENDERERSCRIPT,SELECTVALUEFROMDROPDOWN"); 
		var_dataToPass = getJsonData(var_CSVData , "$.records["+var_Count+"].PaymentMethod");
                 LOGGER.info("Executed Step = STORE,var_dataToPass,var_CSVData,$.records[{var_Count}].PaymentMethod,TGTYPESCREENREG");
        TAP.$("ELE_DRPDWN_PAYMETHOD","//label[@id='workbenchForm:workbenchTabs:paymentCode_label']","TGTYPESCREENREG");

		try { 
		 Thread.sleep(1000);
		WebElement selectValueOfDropDown= driver.findElement(By.xpath("//li[@data-label='"+ var_dataToPass +"']"));

		takeScreenshot();

				selectValueOfDropDown.click();
		// Add Screenshot will only work for Android platform

		takeScreenshot();

				 Thread.sleep(1000);
		 
		 ;} 
		 catch(Exception e) { PrintVal("Renderer Script Exception : "+e.toString()); } 
		 PrintVal("Executed Step = CALLRENDERERSCRIPT,SELECTVALUEFROMDROPDOWN"); 
		var_dataToPass = getJsonData(var_CSVData , "$.records["+var_Count+"].IssueState");
                 LOGGER.info("Executed Step = STORE,var_dataToPass,var_CSVData,$.records[{var_Count}].IssueState,TGTYPESCREENREG");
        TAP.$("ELE_DRPDWN_ISSUESTATECOUNTRY","//label[@id='workbenchForm:workbenchTabs:countryAndStateCode_label']","TGTYPESCREENREG");

		try { 
		 Thread.sleep(1000);
		WebElement selectValueOfDropDown= driver.findElement(By.xpath("//li[@data-label='"+ var_dataToPass +"']"));

		takeScreenshot();

				selectValueOfDropDown.click();
		// Add Screenshot will only work for Android platform

		takeScreenshot();

				 Thread.sleep(1000);
		 
		 ;} 
		 catch(Exception e) { PrintVal("Renderer Script Exception : "+e.toString()); } 
		 PrintVal("Executed Step = CALLRENDERERSCRIPT,SELECTVALUEFROMDROPDOWN"); 
		var_dataToPass = var_TaxQualifiedDescription;
                 LOGGER.info("Executed Step = STORE,var_dataToPass,var_TaxQualifiedDescription,TGTYPESCREENREG");
        TAP.$("ELE_DRPDWN_TAXQUALIFIEDDESP","//label[@id='workbenchForm:workbenchTabs:taxQualifiedCode_label']","TGTYPESCREENREG");

		try { 
		 Thread.sleep(1000);
		WebElement selectValueOfDropDown= driver.findElement(By.xpath("//li[@data-label='"+ var_dataToPass +"']"));

		takeScreenshot();

				selectValueOfDropDown.click();
		// Add Screenshot will only work for Android platform

		takeScreenshot();

				 Thread.sleep(1000);
		 
		 ;} 
		 catch(Exception e) { PrintVal("Renderer Script Exception : "+e.toString()); } 
		 PrintVal("Executed Step = CALLRENDERERSCRIPT,SELECTVALUEFROMDROPDOWN"); 
		var_dataToPass = var_TaxWithholding;
                 LOGGER.info("Executed Step = STORE,var_dataToPass,var_TaxWithholding,TGTYPESCREENREG");
        TAP.$("ELE_DRPDWN_TAXWITHOLDING","//label[@id='workbenchForm:workbenchTabs:taxWithholdingCode_label']","TGTYPESCREENREG");

		try { 
		 Thread.sleep(1000);
		WebElement selectValueOfDropDown= driver.findElement(By.xpath("//li[@data-label='"+ var_dataToPass +"']"));

		takeScreenshot();

				selectValueOfDropDown.click();
		// Add Screenshot will only work for Android platform

		takeScreenshot();

				 Thread.sleep(1000);
		 
		 ;} 
		 catch(Exception e) { PrintVal("Renderer Script Exception : "+e.toString()); } 
		 PrintVal("Executed Step = CALLRENDERERSCRIPT,SELECTVALUEFROMDROPDOWN"); 
        SWIPE.$("UP","TGTYPESCREENREG");

        TAP.$("ELE_TXTBOX_CASHWITHAPP","//input[@id='workbenchForm:workbenchTabs:cashWithApplication_input']","TGTYPESCREENREG");

		try { 
		 WebElement CashApplicationTextField = driver.findElement(By.xpath("//input[@id='workbenchForm:workbenchTabs:cashWithApplication_input']"));
		CashApplicationTextField.sendKeys(Keys.BACK_SPACE);
				CashApplicationTextField.sendKeys(Keys.BACK_SPACE);
				CashApplicationTextField.sendKeys(Keys.BACK_SPACE);	
		// Add Screenshot will only work for Android platform

		takeScreenshot();
			
				CashApplicationTextField.sendKeys(""+var_CashWithApplication+"");// Add Screenshot will only work for Android platform

		takeScreenshot();

		Thread.sleep(1000);
		 
		 ;} 
		 catch(Exception e) { PrintVal("Renderer Script Exception : "+e.toString()); } 
		 PrintVal("Executed Step = CALLRENDERERSCRIPT,CLEARCASHWITHAPPLICATIONTXTFIELD"); 
        SWIPE.$("UP","TGTYPESCREENREG");

        SWIPE.$("UP","TGTYPESCREENREG");

        TAP.$("ELE_GRID_AGENTNUMBER","//span[@class='ui-button-icon-left ui-icon ui-c fa fa-plus']","TGTYPESCREENREG");

        WAIT.$("ELE_TXTBOX_AGENTNUMBER","//input[@name='workbenchForm:workbenchTabs:grid1:agentNumber_Col:filter']","VISIBLE","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_AGENTNUM","//input[@id='workbenchForm:workbenchTabs:agentNumber']","A13000000","FALSE","TGTYPESCREENREG");

        WAIT.$("ELE_BUTTON_VALIDATE","//span[contains(text(),'Validate')]","VISIBLE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_VALIDATE","//span[contains(text(),'Validate')]","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

        TAP.$("ELE_TXTBOX_SPLITPERCENTAGE","//input[@id='workbenchForm:workbenchTabs:agentSplitPercentage_input']","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_SPLITPERCENTAGE","//input[@id='workbenchForm:workbenchTabs:agentSplitPercentage_input']","100","FALSE","TGTYPESCREENREG");

		takeFullScreenShot();
                 LOGGER.info("Executed Step = TAKEFULLSCREENSHOT,TGTYPESCREENREG");
        TAP.$("ELE_BUTTON_VALIDATE","//span[contains(text(),'Validate')]","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        WAIT.$("ELE_BUTTON_SAVE","//span[contains(text(),'Save')]","VISIBLE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SAVE","//span[contains(text(),'Save')]","TGTYPESCREENREG");

        WAIT.$(4,"TGTYPESCREENREG");

		takeFullScreenShot();
                 LOGGER.info("Executed Step = TAKEFULLSCREENSHOT,TGTYPESCREENREG");
        TAP.$("ELE_BUTTON_SHOWLINK","//span[@class='ui-button-icon-left ui-icon ui-c fa fa-ellipsis-v fa-2x']","TGTYPESCREENREG");

        WAIT.$(1,"TGTYPESCREENREG");

        TAP.$("ELE_LINK_SELECTCLIENT","//span[contains(text(),'Select Client')]","TGTYPESCREENREG");

        WAIT.$(1,"TGTYPESCREENREG");

        CALL.$("AddNewClient","TGTYPESCREENREG");

		String var_FirstName = "null";
                 LOGGER.info("Executed Step = VAR,String,var_FirstName,null,TGTYPESCREENREG");
		String var_LastName = "null";
                 LOGGER.info("Executed Step = VAR,String,var_LastName,null,TGTYPESCREENREG");
		String var_SocialSecurityNumber = "null";
                 LOGGER.info("Executed Step = VAR,String,var_SocialSecurityNumber,null,TGTYPESCREENREG");
		String var_stateToPass = "null";
                 LOGGER.info("Executed Step = VAR,String,var_stateToPass,null,TGTYPESCREENREG");
		var_LastName = getJsonData(var_CSVData , "$.records["+var_Count+"].LastName");
                 LOGGER.info("Executed Step = STORE,var_LastName,var_CSVData,$.records[{var_Count}].LastName,TGTYPESCREENREG");
		var_FirstName = getJsonData(var_CSVData , "$.records["+var_Count+"].FirstName");
                 LOGGER.info("Executed Step = STORE,var_FirstName,var_CSVData,$.records[{var_Count}].FirstName,TGTYPESCREENREG");
		var_SocialSecurityNumber = getJsonData(var_CSVData , "$.records["+var_Count+"].SocialSecurityNumber");
                 LOGGER.info("Executed Step = STORE,var_SocialSecurityNumber,var_CSVData,$.records[{var_Count}].SocialSecurityNumber,TGTYPESCREENREG");
        TAP.$("ELE_BUTTON_ADDCLIENT","//button[@id='workbenchForm:workbenchTabs:button_add']","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_FIRSTNAME","//input[@id='workbenchForm:workbenchTabs:nameFirst']",var_FirstName,"FALSE","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_LASTNAME","//input[@id='workbenchForm:workbenchTabs:nameLast']",var_LastName,"FALSE","TGTYPESCREENREG");

        SCROLL.$("ELE_TXTBOX_DOB","//input[@id='workbenchForm:workbenchTabs:dateOfBirth_input']","DOWN","TGTYPESCREENREG");

		String var_ClientDOB = "null";
                 LOGGER.info("Executed Step = VAR,String,var_ClientDOB,null,TGTYPESCREENREG");
		var_ClientDOB = getJsonData(var_CSVData , "$.records["+var_Count+"].ClientDOB");
                 LOGGER.info("Executed Step = STORE,var_ClientDOB,var_CSVData,$.records[{var_Count}].ClientDOB,TGTYPESCREENREG");
        TAP.$("ELE_TXTBOX_DOB","//input[@id='workbenchForm:workbenchTabs:dateOfBirth_input']","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_DOB","//input[@id='workbenchForm:workbenchTabs:dateOfBirth_input']",var_ClientDOB,"FALSE","TGTYPESCREENREG");

        TAP.$("ELE_RDOBTN_FEMALE","//tr[@class='ui-widget-content']//tr[1]//td[1]//div[1]//div[2]//span[1]","TGTYPESCREENREG");

        SWIPE.$("DOWN","TGTYPESCREENREG");

        SWIPE.$("DOWN","TGTYPESCREENREG");

		var_dataToPass = "Social Security Number";
                 LOGGER.info("Executed Step = STORE,var_dataToPass,Social Security Number,TGTYPESCREENREG");
        TAP.$("ELE_DRPDWN_IDTYPE","//label[@id='workbenchForm:workbenchTabs:taxIdenUsag_label']","TGTYPESCREENREG");

		try { 
		 Thread.sleep(1000);
		WebElement selectValueOfDropDown= driver.findElement(By.xpath("//li[@data-label='"+ var_dataToPass +"']"));

		takeScreenshot();

				selectValueOfDropDown.click();
		// Add Screenshot will only work for Android platform

		takeScreenshot();

				 Thread.sleep(1000);
		 
		 ;} 
		 catch(Exception e) { PrintVal("Renderer Script Exception : "+e.toString()); } 
		 PrintVal("Executed Step = CALLRENDERERSCRIPT,SELECTVALUEFROMDROPDOWN"); 
        TYPE.$("ELE_TXTBOX_IDNUMBER","//input[@id='workbenchForm:workbenchTabs:identificationNumber']",var_SocialSecurityNumber,"FALSE","TGTYPESCREENREG");

		takeFullScreenShot();
                 LOGGER.info("Executed Step = TAKEFULLSCREENSHOT,TGTYPESCREENREG");
        TAP.$("ELE_BUTTON_SAVE","//span[contains(text(),'Save')]","TGTYPESCREENREG");

        WAIT.$(20,"TGTYPESCREENREG");

		String var_ClientCity = "null";
                 LOGGER.info("Executed Step = VAR,String,var_ClientCity,null,TGTYPESCREENREG");
		String var_ClientZipCode = "null";
                 LOGGER.info("Executed Step = VAR,String,var_ClientZipCode,null,TGTYPESCREENREG");
		String var_ClientAddressOne = "null";
                 LOGGER.info("Executed Step = VAR,String,var_ClientAddressOne,null,TGTYPESCREENREG");
		var_ClientCity = getJsonData(var_CSVData , "$.records["+var_Count+"].ClientCity");
                 LOGGER.info("Executed Step = STORE,var_ClientCity,var_CSVData,$.records[{var_Count}].ClientCity,TGTYPESCREENREG");
		var_ClientZipCode = getJsonData(var_CSVData , "$.records["+var_Count+"].ClientZipCode");
                 LOGGER.info("Executed Step = STORE,var_ClientZipCode,var_CSVData,$.records[{var_Count}].ClientZipCode,TGTYPESCREENREG");
		var_ClientAddressOne = getJsonData(var_CSVData , "$.records["+var_Count+"].ClientAddress");
                 LOGGER.info("Executed Step = STORE,var_ClientAddressOne,var_CSVData,$.records[{var_Count}].ClientAddress,TGTYPESCREENREG");
        TYPE.$("ELE_TXTBOX_ADDRESSLINE1","//input[@id='workbenchForm:workbenchTabs:addressLineOne']",var_ClientAddressOne,"FALSE","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_CITY","//input[@id='workbenchForm:workbenchTabs:addressCity']",var_ClientCity,"FALSE","TGTYPESCREENREG");

        SCROLL.$("ELE_TXTBOX_ZIPCODE","//input[@id='workbenchForm:workbenchTabs:zipCode']","DOWN","TGTYPESCREENREG");

		var_stateToPass = getJsonData(var_CSVData , "$.records["+var_Count+"].IssueState");
                 LOGGER.info("Executed Step = STORE,var_stateToPass,var_CSVData,$.records[{var_Count}].IssueState,TGTYPESCREENREG");
        TAP.$("ELE_DRPDWN_ADDRESSSTATE","//label[@id='workbenchForm:workbenchTabs:countryAndStateCode_label']","TGTYPESCREENREG");

		try { 
		 Thread.sleep(1000);
				// WebElement selectValueOfDropDown= driver.findElement(By.xpath("//li[@id='workbenchForm:workbenchTabs:'"+ var_stateToPass +"']"));
		// WebElement selectValueOfDropDown= driver.findElement(By.xpath("//ul[@id='workbenchForm:workbenchTabs:birthCountryAndStateCode_items']//li[@data-label='"+ var_stateToPass +"']")); BIRTH STATE DROPDWN


		 WebElement selectValueOfDropDown= driver.findElement(By.xpath("//ul[@id='workbenchForm:workbenchTabs:countryAndStateCode_items']//li[@data-label='"+ var_stateToPass +"']")); //Address state Dropdown


						// Add Screenshot will only work for Android platform

		takeScreenshot();

						selectValueOfDropDown.click();	
		// Add Screenshot will only work for Android platform

		takeScreenshot();


						 Thread.sleep(1000);
		 
		 ;} 
		 catch(Exception e) { PrintVal("Renderer Script Exception : "+e.toString()); } 
		 PrintVal("Executed Step = CALLRENDERERSCRIPT,SELECTVALUEFROMDRPDOWNWITHPRESELECTEDVALUES"); 
        TYPE.$("ELE_TXTBOX_ZIPCODE","//input[@id='workbenchForm:workbenchTabs:zipCode']",var_ClientZipCode,"FALSE","TGTYPESCREENREG");

		takeFullScreenShot();
                 LOGGER.info("Executed Step = TAKEFULLSCREENSHOT,TGTYPESCREENREG");
        TAP.$("ELE_BUTTON_SAVE","//span[contains(text(),'Save')]","TGTYPESCREENREG");

        WAIT.$(15,"TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_CONTINUE","//span[contains(text(),'Continue')]","TGTYPESCREENREG");

        WAIT.$(1,"TGTYPESCREENREG");

		takeFullScreenShot();
                 LOGGER.info("Executed Step = TAKEFULLSCREENSHOT,TGTYPESCREENREG");
        TAP.$("ELE_BUTTON_SAVE","//span[contains(text(),'Save')]","TGTYPESCREENREG");

        WAIT.$(15,"TGTYPESCREENREG");

        CALL.$("AddBenefitPlanAndssuePolicy","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SHOWLINK","//span[@class='ui-button-icon-left ui-icon ui-c fa fa-ellipsis-v fa-2x']","TGTYPESCREENREG");

        WAIT.$(1,"TGTYPESCREENREG");

        TAP.$("ELE_LINK_BENEFIT","//span[contains(text(),'Benefits')]","TGTYPESCREENREG");

        WAIT.$("ELE_DRPDWN_BENEFITPLAN","//label[@id='workbenchForm:workbenchTabs:initialPlanCode_label']","VISIBLE","TGTYPESCREENREG");

		var_dataToPass = getJsonData(var_CSVData , "$.records["+var_Count+"].BenefitPlan");
                 LOGGER.info("Executed Step = STORE,var_dataToPass,var_CSVData,$.records[{var_Count}].BenefitPlan,TGTYPESCREENREG");
		String var_UnitOfInsurance = "null";
                 LOGGER.info("Executed Step = VAR,String,var_UnitOfInsurance,null,TGTYPESCREENREG");
		var_UnitOfInsurance = getJsonData(var_CSVData , "$.records["+var_Count+"].Units");
                 LOGGER.info("Executed Step = STORE,var_UnitOfInsurance,var_CSVData,$.records[{var_Count}].Units,TGTYPESCREENREG");
		String var_RequestedPremiumAmount = "null";
                 LOGGER.info("Executed Step = VAR,String,var_RequestedPremiumAmount,null,TGTYPESCREENREG");
		var_RequestedPremiumAmount = getJsonData(var_CSVData , "$.records["+var_Count+"].requestedAnnualPremium");
                 LOGGER.info("Executed Step = STORE,var_RequestedPremiumAmount,var_CSVData,$.records[{var_Count}].requestedAnnualPremium,TGTYPESCREENREG");
        TAP.$("ELE_DRPDWN_BENEFITPLAN","//label[@id='workbenchForm:workbenchTabs:initialPlanCode_label']","TGTYPESCREENREG");

		try { 
		 Thread.sleep(1000);
		WebElement selectValueOfDropDown= driver.findElement(By.xpath("//li[@data-label='"+ var_dataToPass +"']"));

		takeScreenshot();

				selectValueOfDropDown.click();
		// Add Screenshot will only work for Android platform

		takeScreenshot();

				 Thread.sleep(1000);
		 
		 ;} 
		 catch(Exception e) { PrintVal("Renderer Script Exception : "+e.toString()); } 
		 PrintVal("Executed Step = CALLRENDERERSCRIPT,SELECTVALUEFROMDROPDOWN"); 
        TAP.$("ELE_BUTTON_CONTINUE","//span[contains(text(),'Continue')]","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        TAP.$("ELE_TAB_ADDNEWPOLICYBENEFIT","//a[contains(text(),'Add')]","TGTYPESCREENREG");

		try { 
		 if(var_dataToPass.contains("OAUSD") || var_dataToPass.contains("IL2USD") || var_dataToPass.contains("TRAUSD") || var_dataToPass.contains("HLTUSD"))
				{
					//JavascriptExecutor jse = (JavascriptExecutor) driver; 
				 WebElement ClearUnitOfInsurance= driver.findElement(By.xpath("//input[@id='workbenchForm:workbenchTabs:unitsOfInsurance_input']"));
				 
				/// jse.executeScript("arguments[0].scrollIntoView(true);", ClearUnitOfInsurance);
				 //Thread.sleep(500);
							 ClearUnitOfInsurance.click();
							 takeScreenshot(); 
						     ClearUnitOfInsurance.sendKeys(Keys.BACK_SPACE);
						     ClearUnitOfInsurance.sendKeys(Keys.BACK_SPACE);
						     ClearUnitOfInsurance.sendKeys(Keys.BACK_SPACE);	
				             ClearUnitOfInsurance.sendKeys(Keys.BACK_SPACE);				
						     ClearUnitOfInsurance.sendKeys(""+var_UnitOfInsurance+"");
							 	 driver.findElement(By.xpath("//input[@id='workbenchForm:workbenchTabs:substandardRatiCode1']")).click();
								// Add Screenshot will only work for Android platform

		takeScreenshot();

								 Thread.sleep(1000);	
								 
								 if(var_dataToPass.contains("IL2USD"))
								 {
									 
									 driver.findElement(By.xpath("//table[@id='workbenchForm:workbenchTabs:deathBenefitOption']//tbody[1]/tr[1]//div[1]//div[2]//span[1]")).click();
									 Thread.sleep(1000);
									// Add Screenshot will only work for Android platform

		takeScreenshot();

								 }
				}
				
				 if(var_dataToPass.contains("UL1USD") | var_dataToPass.contains("UL2USD"))
				{
					
				WebElement requestedAnnualPremium= driver.findElement(By.xpath("//input[@id='workbenchForm:workbenchTabs:annualPremium_input']"));
							 					 
							 requestedAnnualPremium.click();
							 requestedAnnualPremium.sendKeys(Keys.BACK_SPACE);
							 requestedAnnualPremium.sendKeys(Keys.BACK_SPACE);
							 requestedAnnualPremium.sendKeys(Keys.BACK_SPACE);
							 requestedAnnualPremium.sendKeys(var_RequestedPremiumAmount);
							 Thread.sleep(1000);
							 // Add Screenshot will only work for Android platform

		takeScreenshot();

							//JavascriptExecutor jse = (JavascriptExecutor) driver;  
							//jse.executeScript("window.scrollBy(0,250)", "");
							WebElement ClearUnitOfInsurance= driver.findElement(By.xpath("//input[@id='workbenchForm:workbenchTabs:unitsOfInsurance_input']"));

						// jse.executeScript("arguments[0].scrollIntoView(true);", ClearUnitOfInsurance);
						// Thread.sleep(1000);
						 ClearUnitOfInsurance.click();
							 ClearUnitOfInsurance.sendKeys(Keys.BACK_SPACE);
						     ClearUnitOfInsurance.sendKeys(Keys.BACK_SPACE);
						     ClearUnitOfInsurance.sendKeys(Keys.BACK_SPACE);	
				             ClearUnitOfInsurance.sendKeys(Keys.BACK_SPACE);				
						     ClearUnitOfInsurance.sendKeys(""+var_UnitOfInsurance+"");
						//	 driver.findElement(By.xpath("//input[@id='workbenchForm:workbenchTabs:costBasis_input']")).click();
							
							 Thread.sleep(500);
							// Add Screenshot will only work for Android platform

		takeScreenshot();

							Thread.sleep(1000);

							WebElement faceAmount = driver.findElement(By.xpath("//table[@id='workbenchForm:workbenchTabs:deathBenefitOption']//tbody[1]/tr[1]//div[1]//div[2]//span[1]"));
									// jse.executeScript("arguments[0].scrollIntoView(true);", faceAmount);
									 
									 faceAmount.click();
									 Thread.sleep(1000);
									// Add Screenshot will only work for Android platform

		takeScreenshot();


						}
				
				 if(var_dataToPass.contains("FX3USD"))
				{
					JavascriptExecutor jse = (JavascriptExecutor) driver;  
							jse.executeScript("window.scrollBy(0,-250)", "");
							WebElement requestedAnnualPremium= driver.findElement(By.xpath("//input[@id='workbenchForm:workbenchTabs:annualPremium_input']"));
							 requestedAnnualPremium.click();
							 requestedAnnualPremium.sendKeys(Keys.BACK_SPACE);
							 requestedAnnualPremium.sendKeys(Keys.BACK_SPACE);
							 requestedAnnualPremium.sendKeys(Keys.BACK_SPACE);
							 requestedAnnualPremium.sendKeys(var_RequestedPremiumAmount);
							 driver.findElement(By.xpath("//input[@id='workbenchForm:workbenchTabs:preTEFRABasis_input']']")).click();
							 Thread.sleep(1000);
						// Add Screenshot will only work for Android platform

		takeScreenshot();

							 

				}
						
		 
		 ;} 
		 catch(Exception e) { PrintVal("Renderer Script Exception : "+e.toString()); } 
		 PrintVal("Executed Step = CALLRENDERERSCRIPT,SELECTBENEFITPLAN"); 
        SWIPE.$("DOWN","TGTYPESCREENREG");

		takeFullScreenShot();
                 LOGGER.info("Executed Step = TAKEFULLSCREENSHOT,TGTYPESCREENREG");
        TYPE.$("ELE_TXTBOX_UNITOFINSURANCE","//input[@id='workbenchForm:workbenchTabs:unitsOfInsurance_input']","100","FALSE","TGTYPESCREENREG");

        WAIT.$(1,"TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SAVE","//span[contains(text(),'Save')]","TGTYPESCREENREG");

        WAIT.$(20,"TGTYPESCREENREG");

        WAIT.$("ELE_GETMSG_SUCCESS","//div[@id='workbenchForm:workbenchTabs:messages']//span[@class='ui-messages-info-summary']","VISIBLE","TGTYPESCREENREG");

        ASSERT.$("ELE_GETMSG_SUCCESS","//div[@id='workbenchForm:workbenchTabs:messages']//span[@class='ui-messages-info-summary']","CONTAINS","successfully","TGTYPESCREENREG");

        WAIT.$(3,"TGTYPESCREENFULL");

        TAP.$("ELE_TAB_CLOSE2","span.ui-icon.ui-icon-close:nth-child(2)TGWEBCOMMACssSelectorTGWEBCOMMA2","TGTYPESCREENREG");

        WAIT.$(3,"TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SHOWLINK","//span[@class='ui-button-icon-left ui-icon ui-c fa fa-ellipsis-v fa-2x']","TGTYPESCREENREG");

        TAP.$("ELE_LINK_PREMIUMS","//span[contains(text(),'Premiums')]","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

        WAIT.$(1,"TGTYPESCREENREG");

        TAP.$("ELE_TAB_CLOSE2","span.ui-icon.ui-icon-close:nth-child(2)TGWEBCOMMACssSelectorTGWEBCOMMA2","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

		String var_EffectiveDate = "null";
                 LOGGER.info("Executed Step = VAR,String,var_EffectiveDate,null,TGTYPESCREENREG");
		var_EffectiveDate = getJsonData(var_CSVData , "$.records["+var_Count+"].EffectiveDate");
                 LOGGER.info("Executed Step = STORE,var_EffectiveDate,var_CSVData,$.records[{var_Count}].EffectiveDate,TGTYPESCREENREG");
        LOGGER.info("Executed Step = START IF");
        if (check(var_PolicyStatus,"=","Pending","TGTYPESCREENREG")) {
         LOGGER.info("Executed Step = IF,var_PolicyStatus,=,Pending,TGTYPESCREENREG");
        CALL.$("PolicyWithStatusPending","TGTYPESCREENREG");

        TAP.$("ELE_TAB_DISPLAYNEWPOLICYCONTRACT","//a[contains(text(),'Display')]","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

        ASSERT.$("ELE_GETVAL_PENDINGSTATUS","//span[contains(text(),'Pending')]","=","Pending","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

        TAP.$("ELE_TAB_CLOSE1","span.ui-icon.ui-icon-close:nth-child(2)TGWEBCOMMACssSelectorTGWEBCOMMA1","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

        TAP.$("ELE_TAB_CLOSE0","span.ui-icon.ui-icon-close:nth-child(2)TGWEBCOMMACssSelectorTGWEBCOMMA0","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

        }
          LOGGER.info("Executed Step = ENDIF,TGTYPESCREENREG");
        LOGGER.info("Executed Step = START IF");
        if (check(var_PolicyStatus,"=","Issue Not Paid","TGTYPESCREENREG")) {
         LOGGER.info("Executed Step = IF,var_PolicyStatus,=,Issue Not Paid,TGTYPESCREENREG");
        CALL.$("PolicyWithStatusIssueNotPaid","TGTYPESCREENREG");

        WAIT.$(1,"TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SHOWLINK","//span[@class='ui-button-icon-left ui-icon ui-c fa fa-ellipsis-v fa-2x']","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

        TAP.$("ELE_LINK_ISSUE","//span[@class='ui-menuitem-text'][contains(text(),'Issue')]","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

        TAP.$("ELE_TXTBOX_EFFECTIVEDATE","//input[@id='workbenchForm:workbenchTabs:effectiveDate_input']","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_EFFECTIVEDATE","//input[@id='workbenchForm:workbenchTabs:effectiveDate_input']",var_EffectiveDate,"TRUE","TGTYPESCREENREG");

		takeFullScreenShot();
                 LOGGER.info("Executed Step = TAKEFULLSCREENSHOT,TGTYPESCREENREG");
        TAP.$("ELE_BUTTON_SUBMIT","//span[contains(text(),'Submit')]","TGTYPESCREENREG");

        WAIT.$(10,"TGTYPESCREENREG");

        ASSERT.$("ELE_GETMSG_SUCCESS","//div[@id='workbenchForm:workbenchTabs:messages']//span[@class='ui-messages-info-summary']","CONTAINS","successfully","TGTYPESCREENREG");

        TAP.$("ELE_TAB_CLOSE2","span.ui-icon.ui-icon-close:nth-child(2)TGWEBCOMMACssSelectorTGWEBCOMMA2","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

        TAP.$("ELE_TAB_CLOSE1","span.ui-icon.ui-icon-close:nth-child(2)TGWEBCOMMACssSelectorTGWEBCOMMA1","TGTYPESCREENREG");

        WAIT.$(1,"TGTYPESCREENREG");

        TAP.$("ELE_TAB_CLOSE0","span.ui-icon.ui-icon-close:nth-child(2)TGWEBCOMMACssSelectorTGWEBCOMMA0","TGTYPESCREENREG");

        WAIT.$(1,"TGTYPESCREENREG");

        }
          LOGGER.info("Executed Step = ENDIF,TGTYPESCREENREG");
        LOGGER.info("Executed Step = START IF");
        if (check(var_PolicyStatus,"=","Settle","TGTYPESCREENREG")) {
         LOGGER.info("Executed Step = IF,var_PolicyStatus,=,Settle,TGTYPESCREENREG");
        WAIT.$(1,"TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SHOWLINK","//span[@class='ui-button-icon-left ui-icon ui-c fa fa-ellipsis-v fa-2x']","TGTYPESCREENREG");

        TAP.$("ELE_LINK_ISSUE","//span[@class='ui-menuitem-text'][contains(text(),'Issue')]","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

        TAP.$("ELE_TXTBOX_EFFECTIVEDATE","//input[@id='workbenchForm:workbenchTabs:effectiveDate_input']","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_EFFECTIVEDATE","//input[@id='workbenchForm:workbenchTabs:effectiveDate_input']",var_EffectiveDate,"TRUE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SUBMIT","//span[contains(text(),'Submit')]","TGTYPESCREENFULL");

        WAIT.$(15,"TGTYPESCREENREG");

        WAIT.$("ELE_GETMSG_SUCCESS","//div[@id='workbenchForm:workbenchTabs:messages']//span[@class='ui-messages-info-summary']","VISIBLE","TGTYPESCREENREG");

        ASSERT.$("ELE_GETMSG_SUCCESS","//div[@id='workbenchForm:workbenchTabs:messages']//span[@class='ui-messages-info-summary']","CONTAINS","successfully","TGTYPESCREENREG");

        TAP.$("ELE_TAB_CLOSE2","span.ui-icon.ui-icon-close:nth-child(2)TGWEBCOMMACssSelectorTGWEBCOMMA2","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        CALL.$("PolicyWithStatusSettle","TGTYPESCREENREG");

		String var_RolloverType = "null";
                 LOGGER.info("Executed Step = VAR,String,var_RolloverType,null,TGTYPESCREENREG");
		String var_RolloverPremium = "null";
                 LOGGER.info("Executed Step = VAR,String,var_RolloverPremium,null,TGTYPESCREENREG");
		String var_PostTefraGainContri = "null";
                 LOGGER.info("Executed Step = VAR,String,var_PostTefraGainContri,null,TGTYPESCREENREG");
		String var_PostTefraBasisContri = "null";
                 LOGGER.info("Executed Step = VAR,String,var_PostTefraBasisContri,null,TGTYPESCREENREG");
		var_RolloverType = getJsonData(var_CSVData , "$.records["+var_Count+"].RolloverType");
                 LOGGER.info("Executed Step = STORE,var_RolloverType,var_CSVData,$.records[{var_Count}].RolloverType,TGTYPESCREENREG");
		var_RolloverPremium = getJsonData(var_CSVData , "$.records["+var_Count+"].RolloverPremium");
                 LOGGER.info("Executed Step = STORE,var_RolloverPremium,var_CSVData,$.records[{var_Count}].RolloverPremium,TGTYPESCREENREG");
		var_PostTefraBasisContri = getJsonData(var_CSVData , "$.records["+var_Count+"].PostTEFRABasisContribution");
                 LOGGER.info("Executed Step = STORE,var_PostTefraBasisContri,var_CSVData,$.records[{var_Count}].PostTEFRABasisContribution,TGTYPESCREENREG");
		var_PostTefraGainContri = getJsonData(var_CSVData , "$.records["+var_Count+"].PostTEFRAGainContribution");
                 LOGGER.info("Executed Step = STORE,var_PostTefraGainContri,var_CSVData,$.records[{var_Count}].PostTEFRAGainContribution,TGTYPESCREENREG");
        TAP.$("ELE_TAB_DISPLAYNEWPOLICYCONTRACT","//a[contains(text(),'Display')]","TGTYPESCREENREG");

        WAIT.$(1,"TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SHOWLINK","//span[@class='ui-button-icon-left ui-icon ui-c fa fa-ellipsis-v fa-2x']","TGTYPESCREENREG");

        WAIT.$(1,"TGTYPESCREENREG");

        TAP.$("ELE_LINK_SETTLE","//span[contains(text(),'Settle')]","TGTYPESCREENREG");

        WAIT.$(3,"TGTYPESCREENREG");

        TAP.$("ELE_TAB_UPDATESTATUSCHANGE","//a[contains(text(),'Update Status Change')]","TGTYPESCREENREG");

		try { 
		 Thread.sleep(500);
		if(!var_RolloverType.contains("No Rollover")) {
						if(var_dataToPass.contains("FX")) 
							{
								System.out.println("FXXXX");
								JavascriptExecutor jse = (JavascriptExecutor) driver;  
								jse.executeScript("window.scrollBy(0,-500)", "");
								
								Thread.sleep(1000);
								driver.findElement(By.xpath("//label[@id='workbenchForm:workbenchTabs:rolloverType_label']")).click();
								Thread.sleep(1000);
								WebElement selectValueOfDropDown= driver.findElement(By.xpath("//li[@data-label='"+ var_RolloverType +"']"));				 		
								selectValueOfDropDown.click();
					
								 Thread.sleep(1000);
								 
		driver.findElement(By.xpath("//input[@id='workbenchForm:workbenchTabs:rolloverPremium_input']")).click();
		 driver.findElement(By.xpath("//input[@id='workbenchForm:workbenchTabs:rolloverPremium_input']")).sendKeys(Keys.BACK_SPACE);						   driver.findElement(By.xpath("//input[@id='workbenchForm:workbenchTabs:rolloverPremium_input']")).sendKeys(Keys.BACK_SPACE);						    driver.findElement(By.xpath("//input[@id='workbenchForm:workbenchTabs:rolloverPremium_input']")).sendKeys(Keys.BACK_SPACE);							driver.findElement(By.xpath("//input[@id='workbenchForm:workbenchTabs:rolloverPremium_input']")).sendKeys(var_RolloverPremium);
								 // Add Screenshot will only work for Android platform

				takeScreenshot();

								 Thread.sleep(1000);
								 if(!driver.findElements(By.xpath("//input[@id='workbenchForm:workbenchTabs:rolloverPostTefraBasisContribution_input']")).isEmpty())
								 {
		driver.findElement(By.xpath("//input[@id='workbenchForm:workbenchTabs:rolloverPostTefraBasisContribution_input']")).click();						 driver.findElement(By.xpath("//input[@id='workbenchForm:workbenchTabs:rolloverPostTefraBasisContribution_input']")).sendKeys(Keys.BACK_SPACE);						 driver.findElement(By.xpath("//input[@id='workbenchForm:workbenchTabs:rolloverPostTefraBasisContribution_input']")).sendKeys(Keys.BACK_SPACE);						driver.findElement(By.xpath("//input[@id='workbenchForm:workbenchTabs:rolloverPostTefraBasisContribution_input']")).sendKeys(Keys.BACK_SPACE);						driver.findElement(By.xpath("//input[@id='workbenchForm:workbenchTabs:rolloverPostTefraBasisContribution_input']")).sendKeys(var_PostTefraBasisContri);
								
								 Thread.sleep(1000);

		driver.findElement(By.xpath("//input[@id='workbenchForm:workbenchTabs:rolloverPostTefraGainContribution_input']")).click();						  driver.findElement(By.xpath("//input[@id='workbenchForm:workbenchTabs:rolloverPostTefraGainContribution_input']")).sendKeys(Keys.BACK_SPACE);
								   driver.findElement(By.xpath("//input[@id='workbenchForm:workbenchTabs:rolloverPostTefraGainContribution_input']")).sendKeys(Keys.BACK_SPACE);
								    driver.findElement(By.xpath("//input[@id='workbenchForm:workbenchTabs:rolloverPostTefraGainContribution_input']")).sendKeys(Keys.BACK_SPACE);
									 driver.findElement(By.xpath("//input[@id='workbenchForm:workbenchTabs:rolloverPostTefraGainContribution_input']")).sendKeys(var_PostTefraGainContri);
								// Add Screenshot will only work for Android platform

			// Add Screenshot will only work for Android platform

		takeScreenshot();


								 Thread.sleep(1000);
								 
								 int var_RolloverPremiumActual;
								 
								 var_RolloverPremiumActual = Integer.parseInt(var_PostTefraBasisContri) + Integer.parseInt(var_PostTefraGainContri);
								 Assert.assertTrue(Integer.toString(var_RolloverPremiumActual).equals(var_RolloverPremium));
								 }
								 
							
							}
								 
									 
				if(var_dataToPass.contains("OA") ||  var_dataToPass.contains("UL") || var_dataToPass.contains("IL") || var_dataToPass.contains("OL"))

							{
								
								System.out.println("BENEFIT PLAN IS "+var_dataToPass);
								JavascriptExecutor jse = (JavascriptExecutor) driver;  
									jse.executeScript("window.scrollBy(0,-500)", "");
								
								driver.findElement(By.xpath("//label[@id='workbenchForm:workbenchTabs:rolloverType_label']")).click();
								Thread.sleep(1000);
								WebElement selectValueOfDropDown= driver.findElement(By.xpath("//li[@data-label='"+ var_RolloverType +"']"));
						 		
								selectValueOfDropDown.click();
						// Add Screenshot will only work for Android platform

				
								 Thread.sleep(1000);
								 
		driver.findElement(By.xpath("//input[@id='workbenchForm:workbenchTabs:rolloverPremium_input']")).click();
		driver.findElement(By.xpath("//input[@id='workbenchForm:workbenchTabs:rolloverPremium_input']")).sendKeys(Keys.BACK_SPACE);						   driver.findElement(By.xpath("//input[@id='workbenchForm:workbenchTabs:rolloverPremium_input']")).sendKeys(Keys.BACK_SPACE);						    driver.findElement(By.xpath("//input[@id='workbenchForm:workbenchTabs:rolloverPremium_input']")).sendKeys(Keys.BACK_SPACE);							driver.findElement(By.xpath("//input[@id='workbenchForm:workbenchTabs:rolloverPremium_input']")).sendKeys(var_RolloverPremium);
								// Add Screenshot will only work for Android platform

				// Add Screenshot will only work for Android platform

		takeScreenshot();


		Thread.sleep(1000);
		driver.findElement(By.xpath("//input[@id='workbenchForm:workbenchTabs:rolloverBasisContribution_input']")).click();						 driver.findElement(By.xpath("//input[@id='workbenchForm:workbenchTabs:rolloverBasisContribution_input']")).sendKeys(Keys.BACK_SPACE);						 driver.findElement(By.xpath("//input[@id='workbenchForm:workbenchTabs:rolloverBasisContribution_input']")).sendKeys(Keys.BACK_SPACE);						driver.findElement(By.xpath("//input[@id='workbenchForm:workbenchTabs:rolloverBasisContribution_input']")).sendKeys(Keys.BACK_SPACE);						driver.findElement(By.xpath("//input[@id='workbenchForm:workbenchTabs:rolloverBasisContribution_input']")).sendKeys(var_PostTefraBasisContri);


		driver.findElement(By.xpath("//input[@id='workbenchForm:workbenchTabs:rolloverGainContribution_input']")).click();						  driver.findElement(By.xpath("//input[@id='workbenchForm:workbenchTabs:rolloverGainContribution_input']")).sendKeys(Keys.BACK_SPACE);						   driver.findElement(By.xpath("//input[@id='workbenchForm:workbenchTabs:rolloverGainContribution_input']")).sendKeys(Keys.BACK_SPACE);						    driver.findElement(By.xpath("//input[@id='workbenchForm:workbenchTabs:rolloverGainContribution_input']")).sendKeys(Keys.BACK_SPACE);							 driver.findElement(By.xpath("//input[@id='workbenchForm:workbenchTabs:rolloverGainContribution_input']")).sendKeys(var_PostTefraGainContri);
		Thread.sleep(1000);
							
								
		 int var_RolloverPremiumActual;
		 var_RolloverPremiumActual = Integer.parseInt(var_PostTefraBasisContri) + Integer.parseInt(var_PostTefraGainContri);
		 Assert.assertTrue(Integer.toString(var_RolloverPremiumActual).equals(var_RolloverPremium));

									
							}
				}		
				 
		 
		 ;} 
		 catch(Exception e) { PrintVal("Renderer Script Exception : "+e.toString()); } 
		 PrintVal("Executed Step = CALLRENDERERSCRIPT,ROLLOVERCONDITION"); 
		takeFullScreenShot();
                 LOGGER.info("Executed Step = TAKEFULLSCREENSHOT,TGTYPESCREENREG");
        TAP.$("ELE_BUTTON_VALIDATE","//span[contains(text(),'Validate')]","TGTYPESCREENREG");

        WAIT.$(4,"TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SUBMIT","//span[contains(text(),'Submit')]","TGTYPESCREENREG");

        WAIT.$(12,"TGTYPESCREENREG");

        WAIT.$("ELE_GETMSG_SUCCESS","//div[@id='workbenchForm:workbenchTabs:messages']//span[@class='ui-messages-info-summary']","VISIBLE","TGTYPESCREENREG");

        ASSERT.$("ELE_GETMSG_SUCCESS","//div[@id='workbenchForm:workbenchTabs:messages']//span[@class='ui-messages-info-summary']","CONTAINS","successfully","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

        TAP.$("ELE_TAB_CLOSE2","span.ui-icon.ui-icon-close:nth-child(2)TGWEBCOMMACssSelectorTGWEBCOMMA2","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        TAP.$("ELE_TAB_CLOSE1","span.ui-icon.ui-icon-close:nth-child(2)TGWEBCOMMACssSelectorTGWEBCOMMA1","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        TAP.$("ELE_TAB_CLOSE0","span.ui-icon.ui-icon-close:nth-child(2)TGWEBCOMMACssSelectorTGWEBCOMMA0","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        }
          LOGGER.info("Executed Step = ENDIF,TGTYPESCREENREG");
        WAIT.$(2,"TGTYPESCREENREG");

        CALL.$("PolicyInquiryPayUpAndExpiryDateValidations","TGTYPESCREENREG");

        TAP.$("ELE_TXTBOX_SEARCHTASKS","//input[@id='workbenchForm:workbenchTabs:taskSearchContainer:taskSearchSearch']","TGTYPESCREENREG");

		try { 
		 driver.findElement(By.xpath("//input[@id='workbenchForm:workbenchTabs:taskSearchContainer:taskSearchSearch']")).clear();
		 
		 ;} 
		 catch(Exception e) { PrintVal("Renderer Script Exception : "+e.toString()); } 
		 PrintVal("Executed Step = CALLRENDERERSCRIPT,CLEARTASKSEARCHFIELD"); 
        WAIT.$(1,"TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_SEARCHTASKS","//input[@id='workbenchForm:workbenchTabs:taskSearchContainer:taskSearchSearch']","Policy Inquiry","FALSE","TGTYPESCREENREG");

        WAIT.$(1,"TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_TASKSEARCH","//a[@id='workbenchForm:workbenchTabs:taskSearchContainer:tb_buttonSearch']//span[@class='fa fa-search fa-lg']","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

        TAP.$("ELE_LINK_POLICYINQUIRY","//span[contains(text(),'Policy Inquiry')]","TGTYPESCREENREG");

        TAP.$("ELE_TAB_ENTERPOLICYNUMBER","//a[contains(text(),'Enter Policy Number')]","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_POLICYNUMBER","//input[@id='workbenchForm:workbenchTabs:policyNumber']",var_PolicyNumber,"FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_CONTINUE","//span[contains(text(),'Continue')]","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        LOGGER.info("Executed Step = START IF");
        if (check(var_PolicyStatus,"=","Settle","TGTYPESCREENREG")) {
         LOGGER.info("Executed Step = IF,var_PolicyStatus,=,Settle,TGTYPESCREENREG");
		writeToCSV("Policy Number " , ActionWrapper.getWebElementValueForVariable("span[id='workbenchForm:workbenchTabs:policyNumber']", "CssSelector", 0, "span[id='workbenchForm:workbenchTabs:policyNumber']TGWEBCOMMACssSelectorTGWEBCOMMA0"));
                 LOGGER.info("Executed Step = WRITETOCSV,ELE_GETVAL_POLICYNUMBER,Policy Number,TGTYPESCREENREG");
		writeToCSV ("Policy Status " , ActionWrapper.getElementValue("ELE_GETVAL_POLICYSTATUS", "//span[@id='workbenchForm:workbenchTabs:statusText']"));
                 LOGGER.info("Executed Step = WRITETOCSV,ELE_GETVAL_POLICYSTATUS,Policy Status,TGTYPESCREENREG");
		writeToCSV ("Client Name " , ActionWrapper.getElementValue("ELE_GETVAL_CLIENTNAME", "//span[@id='workbenchForm:workbenchTabs:ownerName']"));
                 LOGGER.info("Executed Step = WRITETOCSV,ELE_GETVAL_CLIENTNAME,Client Name,TGTYPESCREENREG");
		writeToCSV ("Issue State " , ActionWrapper.getElementValue("ELE_GETVAL_ISSUESTATE", "//span[@id='workbenchForm:workbenchTabs:countryAndStateCode']"));
                 LOGGER.info("Executed Step = WRITETOCSV,ELE_GETVAL_ISSUESTATE,Issue State,TGTYPESCREENREG");
		writeToCSV ("Cash With Application " , ActionWrapper.getElementValue("ELE_GETVAL_CASHWITHAPP", "//span[@id='workbenchForm:workbenchTabs:cashWithApplication']"));
                 LOGGER.info("Executed Step = WRITETOCSV,ELE_GETVAL_CASHWITHAPP,Cash With Application,TGTYPESCREENREG");
        SCROLL.$("ELE_GETVAL_BENEFITPLAN","//span[@id='workbenchForm:workbenchTabs:benefitTable:0:planCodeText']","DOWN","TGTYPESCREENREG");

		writeToCSV ("Suspense Balance " , ActionWrapper.getElementValue("ELE_GETVAL_SUSPENSEBALANCE", "//span[@id='workbenchForm:workbenchTabs:suspenseBalance']"));
                 LOGGER.info("Executed Step = WRITETOCSV,ELE_GETVAL_SUSPENSEBALANCE,Suspense Balance,TGTYPESCREENREG");
		writeToCSV ("Benefit Plan " , ActionWrapper.getElementValue("ELE_GETVAL_BENEFITPLAN", "//span[@id='workbenchForm:workbenchTabs:benefitTable:0:planCodeText']"));
                 LOGGER.info("Executed Step = WRITETOCSV,ELE_GETVAL_BENEFITPLAN,Benefit Plan,TGTYPESCREENREG");
		takeFullScreenShot();
                 LOGGER.info("Executed Step = TAKEFULLSCREENSHOT,TGTYPESCREENREG");
        }
         else {
          LOGGER.info("Executed Step = ELSE,TGTYPESCREENREG");
		writeToCSV("Policy Number " , ActionWrapper.getWebElementValueForVariable("span[id='workbenchForm:workbenchTabs:policyNumber']", "CssSelector", 0, "span[id='workbenchForm:workbenchTabs:policyNumber']TGWEBCOMMACssSelectorTGWEBCOMMA0"));
                 LOGGER.info("Executed Step = WRITETOCSV,ELE_GETVAL_POLICYNUMBER,Policy Number,TGTYPESCREENREG");
		writeToCSV ("Policy Status " , ActionWrapper.getElementValue("ELE_GETVAL_POLICYSTATUSPENDINGISSUE", "//span[@id='workbenchForm:workbenchTabs:contractStatusNewBusText']"));
                 LOGGER.info("Executed Step = WRITETOCSV,ELE_GETVAL_POLICYSTATUSPENDINGISSUE,Policy Status,TGTYPESCREENREG");
		writeToCSV ("Issue State " , ActionWrapper.getElementValue("ELE_GETVAL_ISSUESTATE", "//span[@id='workbenchForm:workbenchTabs:countryAndStateCode']"));
                 LOGGER.info("Executed Step = WRITETOCSV,ELE_GETVAL_ISSUESTATE,Issue State,TGTYPESCREENREG");
		writeToCSV ("Cash With Application " , ActionWrapper.getElementValue("ELE_GETVAL_CASHWITHAPP", "//span[@id='workbenchForm:workbenchTabs:cashWithApplication']"));
                 LOGGER.info("Executed Step = WRITETOCSV,ELE_GETVAL_CASHWITHAPP,Cash With Application,TGTYPESCREENREG");
		takeFullScreenShot();
                 LOGGER.info("Executed Step = TAKEFULLSCREENSHOT,TGTYPESCREENREG");
        WAIT.$(3,"TGTYPESCREENREG");

        TAP.$("ELE_TAB_CLOSE1","span.ui-icon.ui-icon-close:nth-child(2)TGWEBCOMMACssSelectorTGWEBCOMMA1","TGTYPESCREENREG");

        WAIT.$(1,"TGTYPESCREENREG");

        TAP.$("ELE_TAB_CLOSE0","span.ui-icon.ui-icon-close:nth-child(2)TGWEBCOMMACssSelectorTGWEBCOMMA0","TGTYPESCREENREG");

        WAIT.$(1,"TGTYPESCREENREG");

        }
          LOGGER.info("Executed Step = ENDIF,TGTYPESCREENREG");
        TAP.$("ELE_LINK_ROW1","//tr[@class='ui-widget-content ui-datatable-even ui-datatable-selectable']","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

		String var_PaidUpDateGIAS;
                 LOGGER.info("Executed Step = VAR,String,var_PaidUpDateGIAS,TGTYPESCREENREG");
		var_PaidUpDateGIAS = ActionWrapper.getElementValue("ELE_GETVAL_PAIDUPDATE", "//span[@id='workbenchForm:workbenchTabs:payUpDate']");
                 LOGGER.info("Executed Step = STORE,var_PaidUpDateGIAS,ELE_GETVAL_PAIDUPDATE,TGTYPESCREENREG");
		String var_ExpirationDateGIAS;
                 LOGGER.info("Executed Step = VAR,String,var_ExpirationDateGIAS,TGTYPESCREENREG");
		var_ExpirationDateGIAS = ActionWrapper.getElementValue("ELE_GETVAL_EXPIRATIONDATE", "//span[@id='workbenchForm:workbenchTabs:expiryDate']");
                 LOGGER.info("Executed Step = STORE,var_ExpirationDateGIAS,ELE_GETVAL_EXPIRATIONDATE,TGTYPESCREENREG");
        ASSERT.$(var_PaidUpDateGIAS,"=",var_PaidUpDate,"TGTYPESCREENREG");

        ASSERT.$(var_ExpirationDateGIAS,"=",var_ExpirationDate,"TGTYPESCREENREG");

		takeFullScreenShot();
                 LOGGER.info("Executed Step = TAKEFULLSCREENSHOT,TGTYPESCREENREG");
        }
          LOGGER.info("Executed Step = ENDIF,TGTYPESCREENREG");
		var_Count = var_Count + ( 1 );
                 LOGGER.info("Executed Step = INCREASEVAR,var_Count,1,TGTYPESCREENREG");
    	}
          LOGGER.info("Executed Step = WEND,TGTYPESCREENREG");
    	resetAttemps();

    }


    @Test
    public void ftj41modf237sprint11() {

        // No need to declare / print method name here, handled by TestNg listener
        // declare("ftj41modf237sprint11");

		int var_Count = 0;
                 LOGGER.info("Executed Step = VAR,Integer,var_Count,0,TGTYPESCREENREG");
		com.jayway.jsonpath.ReadContext var_CSVData = getJsonData("http://10.1.104.229/json/20201009/qZ5JZg.json");
                 LOGGER.info("Executed Step = VAR,Json,var_CSVData,http://10.1.104.229/json/20201009/qZ5JZg.json,TGTYPESCREENREG");
        LOGGER.info("Executed Step = REPEAT IF");
    	while (check(var_Count,"<",1,"TGTYPESCREENREG")) {
          LOGGER.info("Executed Step = WHILE,var_Count,<,1,TGTYPESCREENREG");
		String var_Policy1;
                 LOGGER.info("Executed Step = VAR,String,var_Policy1,TGTYPESCREENREG");
		var_Policy1 = getJsonData(var_CSVData , "$.records["+var_Count+"].Policy1");
                 LOGGER.info("Executed Step = STORE,var_Policy1,var_CSVData,$.records[{var_Count}].Policy1,TGTYPESCREENREG");
		String var_EffectiveDate;
                 LOGGER.info("Executed Step = VAR,String,var_EffectiveDate,TGTYPESCREENREG");
		var_EffectiveDate = getJsonData(var_CSVData , "$.records["+var_Count+"].EffectiveDate");
                 LOGGER.info("Executed Step = STORE,var_EffectiveDate,var_CSVData,$.records[{var_Count}].EffectiveDate,TGTYPESCREENREG");
        CALL.$("FTJQA2Login","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

		try { 
		 WebElement html = driver.findElement(By.tagName("html"));
		    	
		    	System.out.println("html size" + html.getSize());
		    	System.out.println("html" + html.getTagName());

		    	for( int i =0 ;i<4;i++) {
		        html.sendKeys(Keys.chord(Keys.CONTROL, Keys.SUBTRACT));
		    	}
		 
		 ;} 
		 catch(Exception e) { PrintVal("Renderer Script Exception : "+e.toString()); } 
		 PrintVal("Executed Step = CALLRENDERERSCRIPT,FTJ41ZOOMMAGNIFICATIONCHANGE"); 
        TYPE.$("ELE_TXTBOX_USERNAME","//input[@id='guestForm:username']","x5080","FALSE","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_PASSWORD","//input[@id='guestForm:password']","Welcome123$","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_LOGIN","//button[@id='guestForm:loginButton']","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        CALL.$("ModF237UpdateBenefitCoverageBB933","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_SEARCHTASKS","//input[@id='workbenchForm:workbenchTabs:taskSearchContainer:taskSearchSearch']","Policy Inquiry","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_TASKSEARCH","//a[@id='workbenchForm:workbenchTabs:taskSearchContainer:tb_buttonSearch']//span[@class='fa fa-search fa-lg']","TGTYPESCREENREG");

        WAIT.$(1,"TGTYPESCREENREG");

        TAP.$("ELE_LINK_POLICYINQUIRY","//span[contains(text(),'Policy Inquiry')]","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_POLICYNUMBER","//input[@id='workbenchForm:workbenchTabs:policyNumber']",var_Policy1,"FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_CONTINUE","//span[contains(text(),'Continue')]","TGTYPESCREENREG");

        WAIT.$(10,"TGTYPESCREENREG");

        TAP.$("ELE_LINK_ROW1","//tr[@class='ui-widget-content ui-datatable-even ui-datatable-selectable']","TGTYPESCREENFULL");

        WAIT.$(3,"TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SHOWLINK","//span[@class='ui-button-icon-left ui-icon ui-c fa fa-ellipsis-v fa-2x']","TGTYPESCREENREG");

        TAP.$("ELE_LABEL_COVERAGES","//span[contains(text(),'Coverages')]","TGTYPESCREENREG");

        WAIT.$(3,"TGTYPESCREENREG");

        TAP.$("ELE_LINK_ROW1","//tr[@class='ui-widget-content ui-datatable-even ui-datatable-selectable']","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_EDIT","//span[text()='Edit']","TGTYPESCREENREG");

        TAP.$("ELE_DRPDWN_TERMINALILLNESSNURSINGHOME_COVERAGES","//body//tr[@class='ui-widget-content']//tr[@class='ui-widget-content']//tr[1]//td[2]//div[1]//div[3]//span[1]","TGTYPESCREENREG");

        TAP.$("ELE_DRPDWN_TERMINALILLNESS_DECLINE","//li[@id='workbenchForm:workbenchTabs:coverageValue1_1']","TGTYPESCREENREG");

        TAP.$("ELE_DRPDWN_IRC72Q_COVERAGES","//tr[3]//td[2]//div[1]//div[3]//span[1]","TGTYPESCREENREG");

        TAP.$("ELE_DRPDWN_IRC72_DECLINE","//li[@id='workbenchForm:workbenchTabs:coverageValue2_1']","TGTYPESCREENREG");

        TAP.$("ELE_DRPDWN_DEATHBENEFIT_COVERAGES","//tr[5]//td[2]//div[1]//div[3]//span[1]","TGTYPESCREENREG");

        TAP.$("ELE_DRPDWN_DEATHBENEFIT_DECLINE","//li[@id='workbenchForm:workbenchTabs:coverageValue3_1']","TGTYPESCREENREG");

        TAP.$("ELE_DRPDWN_RMD_COVERAGES","//tr[11]//td[2]//div[1]//div[3]//span[1]","TGTYPESCREENREG");

        TAP.$("ELE_DRPDWN_RMD_DECLINE","//li[@id='workbenchForm:workbenchTabs:coverageValue6_1']","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_VALIDATE","//span[contains(text(),'Validate')]","TGTYPESCREENFULL");

        WAIT.$(4,"TGTYPESCREENREG");

        ASSERT.$("ELE_ERRORMSG_COVERAGECANNOTBECHANGDUSEFIXIT","//div[@id='workbenchForm:workbenchTabs:messages']//span[@class='ui-messages-error-summary'][contains(text(),'Coverage cannot be changed. Use Fix It.')]","VISIBLE","TGTYPESCREENFULL");

        CALL.$("F237ValidateErrorOnAddCoverage","TGTYPESCREENREG");

        TAP.$("ELE_TABCLOSE5","//li[@class='ui-state-default ui-tabs-selected ui-state-active ui-corner-top']//span[@class='ui-icon ui-icon-close']","TGTYPESCREENREG");

        WAIT.$(4,"TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_ADD","//span[contains(text(),'Add')]","TGTYPESCREENREG");

        WAIT.$(3,"TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_EFFECTIVEDATE","//input[@id='workbenchForm:workbenchTabs:effectiveDate_input']",var_EffectiveDate,"FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_CONTINUE","//span[contains(text(),'Continue')]","TGTYPESCREENREG");

        WAIT.$(1,"TGTYPESCREENREG");

        ASSERT.$("ELE_MSG_ERRORMESG_BELOWFIELD","//span[@class='ui-message-error-detail']","CONTAINS","Coverage effective day must equal benefit effective day.","TGTYPESCREENFULL");

        TAP.$("ELE_LOGO_GIAS","//span[text()='GIAS']","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

		var_Count = var_Count + ( 1 );
                 LOGGER.info("Executed Step = INCREASEVAR,var_Count,1,TGTYPESCREENREG");
    	}
          LOGGER.info("Executed Step = WEND,TGTYPESCREENREG");
    	resetAttemps();

    }


    @Test
    public void ftj41tbd2() {

        // No need to declare / print method name here, handled by TestNg listener
        // declare("ftj41tbd2");

		int var_Count = 0;
                 LOGGER.info("Executed Step = VAR,Integer,var_Count,0,TGTYPESCREENREG");
		com.jayway.jsonpath.ReadContext var_CSVData = getJsonData("http://10.1.104.229/json/20200819/gxmuMD.json");
                 LOGGER.info("Executed Step = VAR,Json,var_CSVData,http://10.1.104.229/json/20200819/gxmuMD.json,TGTYPESCREENREG");
        LOGGER.info("Executed Step = REPEAT IF");
    	while (check(var_Count,"<",1,"TGTYPESCREENREG")) {
          LOGGER.info("Executed Step = WHILE,var_Count,<,1,TGTYPESCREENREG");
		String var_SecurityGroup;
                 LOGGER.info("Executed Step = VAR,String,var_SecurityGroup,TGTYPESCREENREG");
		var_SecurityGroup = getJsonData(var_CSVData , "$.records.["+var_Count+"].SecurityGroup");
                 LOGGER.info("Executed Step = STORE,var_SecurityGroup,var_CSVData,$.records.[{var_Count}].SecurityGroup,TGTYPESCREENREG");
        CALL.$("FTJQA2Login","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

		try { 
		 WebElement html = driver.findElement(By.tagName("html"));
		    	
		    	System.out.println("html size" + html.getSize());
		    	System.out.println("html" + html.getTagName());

		    	for( int i =0 ;i<4;i++) {
		        html.sendKeys(Keys.chord(Keys.CONTROL, Keys.SUBTRACT));
		    	}
		 
		 ;} 
		 catch(Exception e) { PrintVal("Renderer Script Exception : "+e.toString()); } 
		 PrintVal("Executed Step = CALLRENDERERSCRIPT,FTJ41ZOOMMAGNIFICATIONCHANGE"); 
        TYPE.$("ELE_TXTBOX_USERNAME","//input[@id='guestForm:username']","x5080","FALSE","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_PASSWORD","//input[@id='guestForm:password']","Welcome123$","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_LOGIN","//button[@id='guestForm:loginButton']","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        CALL.$("DisplaySecuirtyGroupNewFieldValidationsBB940","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_SEARCHTASKS","//input[@id='workbenchForm:workbenchTabs:taskSearchContainer:taskSearchSearch']","Security Group","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_TASKSEARCH","//a[@id='workbenchForm:workbenchTabs:taskSearchContainer:tb_buttonSearch']//span[@class='fa fa-search fa-lg']","TGTYPESCREENREG");

        WAIT.$(1,"TGTYPESCREENREG");

        TAP.$("ELE_LINK_SECURITYGROUPS","//span[contains(text(),'Security Groups')]","TGTYPESCREENREG");

        WAIT.$("ELE_TXTBOX_SECURITYGROUP","//input[@id='workbenchForm:workbenchTabs:grid:name_Col:filter']","VISIBLE","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_SECURITYGROUP","//input[@id='workbenchForm:workbenchTabs:grid:name_Col:filter']",var_SecurityGroup,"FALSE","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

		String var_SecurityGroupName;
                 LOGGER.info("Executed Step = VAR,String,var_SecurityGroupName,TGTYPESCREENREG");
		var_SecurityGroupName = ActionWrapper.getElementValue("ELE_GETVAL_NAME_ROW1", "//span[@id='workbenchForm:workbenchTabs:grid:0:name']");
                 LOGGER.info("Executed Step = STORE,var_SecurityGroupName,ELE_GETVAL_NAME_ROW1,TGTYPESCREENREG");
		String var_SecurityGroupDescription;
                 LOGGER.info("Executed Step = VAR,String,var_SecurityGroupDescription,TGTYPESCREENREG");
		var_SecurityGroupDescription = ActionWrapper.getElementValue("ELE_GETVAL_DESCRIPTION_ROW1", "//span[@id='workbenchForm:workbenchTabs:grid:0:description']");
                 LOGGER.info("Executed Step = STORE,var_SecurityGroupDescription,ELE_GETVAL_DESCRIPTION_ROW1,TGTYPESCREENREG");
        TAP.$("ELE_LINK_ROW1","//tr[@class='ui-widget-content ui-datatable-even ui-datatable-selectable']","TGTYPESCREENREG");

        WAIT.$("ELE_GETVAL_SECURITYGROUP","//span[@id='workbenchForm:workbenchTabs:name']","VISIBLE","TGTYPESCREENREG");

        ASSERT.$("ELE_GETVAL_SECURITYGROUP","//span[@id='workbenchForm:workbenchTabs:name']","=",var_SecurityGroupName,"TGTYPESCREENREG");

        ASSERT.$("ELE_GETVAL_DESCRIPTION","//span[@id='workbenchForm:workbenchTabs:description']","=",var_SecurityGroupDescription,"TGTYPESCREENREG");

        ASSERT.$("ELE_GETVAL_EXECUTABLE","//span[@id='workbenchForm:workbenchTabs:executable']","=","Yes","TGTYPESCREENREG");

        ASSERT.$("ELE_GETVAL_DISPLAYSUPPORTED","//span[@id='workbenchForm:workbenchTabs:displaySupported']","=","Yes","TGTYPESCREENREG");

        ASSERT.$("ELE_GETVAL_UPDATESUPPORTED","//span[@id='workbenchForm:workbenchTabs:updateSupported']","=","Yes","TGTYPESCREENREG");

        ASSERT.$("ELE_GETVAL_ADDSUPPORTED","//span[@id='workbenchForm:workbenchTabs:addSupported']","=","Yes","TGTYPESCREENREG");

        ASSERT.$("ELE_GETVAL_DELETESUPPORTED","//span[@id='workbenchForm:workbenchTabs:deleteSupported']","=","Yes","TGTYPESCREENREG");

        TAP.$("ELE_LOGO_GIAS","//span[text()='GIAS']","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

        CALL.$("PaymentMethodAndFrequencyValidationsWhenIssuingPolicyBB965","TGTYPESCREENREG");

        SWIPE.$("DOWN","TGTYPESCREENREG");

        SWIPE.$("DOWN","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_SEARCHTASKS","//input[@id='workbenchForm:workbenchTabs:taskSearchContainer:taskSearchSearch']","New Policy Application","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_TASKSEARCH","//a[@id='workbenchForm:workbenchTabs:taskSearchContainer:tb_buttonSearch']//span[@class='fa fa-search fa-lg']","TGTYPESCREENREG");

        WAIT.$(1,"TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_NEWPOLICYAPP","//span[contains(text(),'New Policy Application')]","TGTYPESCREENREG");

        WAIT.$("ELE_TXTBOX_FILTERPOLICYNUMBER1","//input[@id='workbenchForm:workbenchTabs:grid:policyNumber_Col:filter']","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_FILTERPOLICYNUMBER1","//input[@id='workbenchForm:workbenchTabs:grid:policyNumber_Col:filter']","12230","FALSE","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        TAP.$("ELE_LINK_ROW1","//tr[@class='ui-widget-content ui-datatable-even ui-datatable-selectable']","TGTYPESCREENREG");

        WAIT.$("ELE_GETVAL_PAYMENTCODE","//span[@id='workbenchForm:workbenchTabs:paymentCode']","VISIBLE","TGTYPESCREENREG");

        LOGGER.info("Executed Step = START IF");
        if (check("ELE_GETVAL_PAYMENTCODE","//span[@id='workbenchForm:workbenchTabs:paymentCode']","CONTAINS","Coupon Book","TGTYPESCREENREG")) {
         LOGGER.info("Executed Step = IF,ELE_GETVAL_PAYMENTCODE,CONTAINS,Coupon Book,TGTYPESCREENREG");
        CALL.$("UpdateToConsolidatedBookingAndPerformErrorValidationsBB965","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_EDIT","//span[text()='Edit']","TGTYPESCREENREG");

        WAIT.$("ELE_DROPDWN_PAYMENTMETHOD","//div[@id='workbenchForm:workbenchTabs:paymentCode']//span[@class='ui-icon ui-icon-triangle-1-s ui-c']","VISIBLE","TGTYPESCREENREG");

        TAP.$("ELE_DROPDWN_PAYMENTMETHOD","//div[@id='workbenchForm:workbenchTabs:paymentCode']//span[@class='ui-icon ui-icon-triangle-1-s ui-c']","TGTYPESCREENREG");

        TAP.$("ELE_DRPDWN_CONSOLIDATEDBILLING_NBS200","//li[@id='workbenchForm:workbenchTabs:paymentCode_0']","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SAVE","//span[contains(text(),'Save')]","TGTYPESCREENREG");

        WAIT.$("ELE_WARNINGMSG_PAYMENTMETHODFREQUENCYNOTVALIDFORBASEPLAN","//span[contains(text(),'Payment method and frequency not valid for the bas')]","TGTYPESCREENREG");

		try { 
		 driver.switchTo().frame(0);
		driver.findElement(By.xpath("//form[@id='confirmDialogForm']")).click();
		Thread.sleep(1000);
		driver.findElement(By.xpath("//button[@id='j_idt9']")).click();
		// Add Screenshot will only work for Android platform

		takeScreenshot();

		 
		 ;} 
		 catch(Exception e) { PrintVal("Renderer Script Exception : "+e.toString()); } 
		 PrintVal("Executed Step = CALLRENDERERSCRIPT,CLICKYESONCONFIRMACTION"); 
        WAIT.$("ELE_MSG_ITEMSUCCESSUPDATE","//div[@id='workbenchForm:workbenchTabs:messages']//span[@class='ui-messages-info-summary'][contains(text(),'Item successfully updated.')]","VISIBLE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SHOWLINK","//span[@class='ui-button-icon-left ui-icon ui-c fa fa-ellipsis-v fa-2x']","TGTYPESCREENREG");

        WAIT.$(1,"TGTYPESCREENREG");

        TAP.$("ELE_LINK_ISSUE","//span[@class='ui-menuitem-text'][contains(text(),'Issue')]","TGTYPESCREENREG");

        WAIT.$("ELE_BUTTON_VALIDATE","//span[contains(text(),'Validate')]","VISIBLE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_VALIDATE","//span[contains(text(),'Validate')]","TGTYPESCREENREG");

        WAIT.$("ELE_ERRORMSG_PREMIUMRATELOOKUPDATEMUTBEATTAINEDAGEPTDFORTABLEACCESSMETHOD","//div[@id='workbenchForm:workbenchTabs:messages']//span[@class='ui-messages-error-summary'][contains(text(),'Premium rate lookup date must be Attained Age, Pai')]","VISIBLE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SUBMIT","//span[contains(text(),'Submit')]","TGTYPESCREENREG");

        WAIT.$("ELE_ERRORMSG_PREMIUMRATELOOKUPDATEMUTBEATTAINEDAGEPTDFORTABLEACCESSMETHOD","//div[@id='workbenchForm:workbenchTabs:messages']//span[@class='ui-messages-error-summary'][contains(text(),'Premium rate lookup date must be Attained Age, Pai')]","VISIBLE","TGTYPESCREENREG");

        TAP.$("ELE_LOGO_GIAS","//span[text()='GIAS']","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

        }
          LOGGER.info("Executed Step = ENDIF,TGTYPESCREENREG");
        LOGGER.info("Executed Step = START IF");
        if (check("ELE_GETVAL_PAYMENTCODE","//span[@id='workbenchForm:workbenchTabs:paymentCode']","CONTAINS","Consolidated Billing","TGTYPESCREENREG")) {
         LOGGER.info("Executed Step = IF,ELE_GETVAL_PAYMENTCODE,CONTAINS,Consolidated Billing,TGTYPESCREENREG");
        CALL.$("UpdateToCouponBookAndPerformErrorValidationsBB965","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_EDIT","//span[text()='Edit']","TGTYPESCREENREG");

        WAIT.$("ELE_DROPDWN_PAYMENTMETHOD","//div[@id='workbenchForm:workbenchTabs:paymentCode']//span[@class='ui-icon ui-icon-triangle-1-s ui-c']","VISIBLE","TGTYPESCREENREG");

        TAP.$("ELE_DROPDWN_PAYMENTMETHOD","//div[@id='workbenchForm:workbenchTabs:paymentCode']//span[@class='ui-icon ui-icon-triangle-1-s ui-c']","TGTYPESCREENREG");

        TAP.$("ELE_DRPDWN_COUPONBOOK_NBS200","//li[@id='workbenchForm:workbenchTabs:paymentCode_1']","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SAVE","//span[contains(text(),'Save')]","TGTYPESCREENREG");

        WAIT.$("ELE_WARNINGMSG_PAYMENTMETHODFREQUENCYNOTVALIDFORBASEPLAN","//span[contains(text(),'Payment method and frequency not valid for the bas')]","VISIBLE","TGTYPESCREENREG");

		try { 
		 driver.switchTo().frame(0);
		driver.findElement(By.xpath("//form[@id='confirmDialogForm']")).click();
		Thread.sleep(1000);
		driver.findElement(By.xpath("//button[@id='j_idt9']")).click();
		// Add Screenshot will only work for Android platform

		takeScreenshot();

		 
		 ;} 
		 catch(Exception e) { PrintVal("Renderer Script Exception : "+e.toString()); } 
		 PrintVal("Executed Step = CALLRENDERERSCRIPT,CLICKYESONCONFIRMACTION"); 
        WAIT.$("ELE_MSG_ITEMSUCCESSUPDATE","//div[@id='workbenchForm:workbenchTabs:messages']//span[@class='ui-messages-info-summary'][contains(text(),'Item successfully updated.')]","VISIBLE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SHOWLINK","//span[@class='ui-button-icon-left ui-icon ui-c fa fa-ellipsis-v fa-2x']","TGTYPESCREENREG");

        WAIT.$(1,"TGTYPESCREENREG");

        TAP.$("ELE_LINK_ISSUE","//span[@class='ui-menuitem-text'][contains(text(),'Issue')]","TGTYPESCREENREG");

        WAIT.$("ELE_BUTTON_VALIDATE","//span[contains(text(),'Validate')]","VISIBLE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_VALIDATE","//span[contains(text(),'Validate')]","TGTYPESCREENREG");

        WAIT.$("ELE_ERRORMSG_PREMIUMRATELOOKUPDATEMUTBEATTAINEDAGEPTDFORTABLEACCESSMETHOD","//div[@id='workbenchForm:workbenchTabs:messages']//span[@class='ui-messages-error-summary'][contains(text(),'Premium rate lookup date must be Attained Age, Pai')]","VISIBLE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SUBMIT","//span[contains(text(),'Submit')]","TGTYPESCREENREG");

        WAIT.$("ELE_ERRORMSG_PREMIUMRATELOOKUPDATEMUTBEATTAINEDAGEPTDFORTABLEACCESSMETHOD","//div[@id='workbenchForm:workbenchTabs:messages']//span[@class='ui-messages-error-summary'][contains(text(),'Premium rate lookup date must be Attained Age, Pai')]","VISIBLE","TGTYPESCREENREG");

        TAP.$("ELE_LOGO_GIAS","//span[text()='GIAS']","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

        }
          LOGGER.info("Executed Step = ENDIF,TGTYPESCREENREG");
        CALL.$("EditPaymentMethodFrequencyAgainstPlanNBS100BB964","TGTYPESCREENREG");

        SWIPE.$("DOWN","TGTYPESCREENREG");

        SWIPE.$("DOWN","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_SEARCHTASKS","//input[@id='workbenchForm:workbenchTabs:taskSearchContainer:taskSearchSearch']","New Policy Application","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_TASKSEARCH","//a[@id='workbenchForm:workbenchTabs:taskSearchContainer:tb_buttonSearch']//span[@class='fa fa-search fa-lg']","TGTYPESCREENREG");

        WAIT.$(1,"TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_NEWPOLICYAPP","//span[contains(text(),'New Policy Application')]","TGTYPESCREENREG");

        WAIT.$("ELE_TXTBOX_FILTERPOLICYNUMBER1","//input[@id='workbenchForm:workbenchTabs:grid:policyNumber_Col:filter']","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_FILTERPOLICYNUMBER1","//input[@id='workbenchForm:workbenchTabs:grid:policyNumber_Col:filter']","111","FALSE","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        TAP.$("ELE_LINK_ROW1","//tr[@class='ui-widget-content ui-datatable-even ui-datatable-selectable']","TGTYPESCREENREG");

        WAIT.$("ELE_BUTTON_SHOWLINK","//span[@class='ui-button-icon-left ui-icon ui-c fa fa-ellipsis-v fa-2x']","VISIBLE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SHOWLINK","//span[@class='ui-button-icon-left ui-icon ui-c fa fa-ellipsis-v fa-2x']","TGTYPESCREENREG");

        WAIT.$(1,"TGTYPESCREENREG");

        TAP.$("ELE_LINK_BENEFIT","//span[contains(text(),'Benefits')]","TGTYPESCREENREG");

        WAIT.$("ELE_DRPDWN_BENEFITPLAN","//label[@id='workbenchForm:workbenchTabs:initialPlanCode_label']","VISIBLE","TGTYPESCREENREG");

        TAP.$("ELE_DRPDWN_BENEFITPLAN","//label[@id='workbenchForm:workbenchTabs:initialPlanCode_label']","TGTYPESCREENREG");

        WAIT.$(1,"TGTYPESCREENREG");

        TAP.$("ELE_DRPDWN_PLAN_VF1USDVARFLEXANNUITY","//li[contains(text(),'VF1USD - Var Flex Annuity; No FrLk;Bail-n')]","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_CONTINUE","//span[contains(text(),'Continue')]","TGTYPESCREENREG");

        WAIT.$("ELE_MSG_PAYMENTMETHODANDFREQUENCYNOTVALIDFORTHEBASEPLAN","//div[@class='mainTabGroup']//span[@class='ui-messages-warn-summary'][contains(text(),'Payment method and frequency not valid for the bas')]","VISIBLE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SAVE","//span[contains(text(),'Save')]","TGTYPESCREENREG");

        WAIT.$(3,"TGTYPESCREENREG");

        ASSERT.$("ELE_MSG_PAYMENTMETHODANDFREQUENCYNOTVALIDFORTHEBASEPLAN","//div[@class='mainTabGroup']//span[@class='ui-messages-warn-summary'][contains(text(),'Payment method and frequency not valid for the bas')]","VISIBLE","TGTYPESCREENREG");

        TAP.$("ELE_LOGO_GIAS","//span[text()='GIAS']","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

        CALL.$("NewFIeldValidationsPremiumTabPlanDescriptionBB846TBD2","TGTYPESCREENREG");

        SWIPE.$("DOWN","TGTYPESCREENREG");

        SWIPE.$("DOWN","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_SEARCHTASKS","//input[@id='workbenchForm:workbenchTabs:taskSearchContainer:taskSearchSearch']","pdf100","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_TASKSEARCH","//a[@id='workbenchForm:workbenchTabs:taskSearchContainer:tb_buttonSearch']//span[@class='fa fa-search fa-lg']","TGTYPESCREENREG");

        TAP.$("ELE_LINK_PLANSETUP","//span[contains(text(),'Plan Setup')]","TGTYPESCREENREG");

        WAIT.$("ELE_TXTBOX_PLAN","//input[@id='workbenchForm:workbenchTabs:grid:planCode_Col:filter']","VISIBLE","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_PLAN","//input[@id='workbenchForm:workbenchTabs:grid:planCode_Col:filter']","TEST","FALSE","TGTYPESCREENREG");

        WAIT.$(4,"TGTYPESCREENREG");

        TAP.$("ELE_LINK_ROW1","//tr[@class='ui-widget-content ui-datatable-even ui-datatable-selectable']","TGTYPESCREENREG");

        WAIT.$("ELE_TAB_PREMIUMS","//div[@class='ui-outputpanel ui-widget']//li[10]//a[1]//span[1]","VISIBLE","TGTYPESCREENREG");

        TAP.$("ELE_TAB_PREMIUMS","//div[@class='ui-outputpanel ui-widget']//li[10]//a[1]//span[1]","TGTYPESCREENREG");

        WAIT.$(3,"TGTYPESCREENREG");

        ASSERT.$("ELE_LABEL_VALIDPAYMENTMETHODFREQUENCYTABLE","//label[@id='workbenchForm:workbenchTabs:methodFrequencyTable_lbl']","VISIBLE","TGTYPESCREENREG");

        ASSERT.$("ELE_LABEL_TABLEACCESSMETHOD","//label[@id='workbenchForm:workbenchTabs:methodFrequencyAccessMethod_lbl']","VISIBLE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_EDIT","//span[text()='Edit']","TGTYPESCREENREG");

        TAP.$("ELE_DRPDWN_TABLEACCESSMETHOD_PLANPREMIUMPAGE","//div[@id='workbenchForm:workbenchTabs:methodFrequencyAccessMethod']//div//span","TGTYPESCREENREG");

        TAP.$("ELE_DRPDWN_TABLEACCESSMETHOD_POLICYEFFECTIVEDATE","//li[@id='workbenchForm:workbenchTabs:methodFrequencyAccessMethod_1']","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_VALIDATE","//span[contains(text(),'Validate')]","TGTYPESCREENREG");

        WAIT.$("ELE_MSG_ERRORMESG_BELOWFIELD","//span[@class='ui-message-error-detail']","VISIBLE","TGTYPESCREENREG");

        ASSERT.$("ELE_MSG_ERRORMESG_BELOWFIELD","//span[@class='ui-message-error-detail']","CONTAINS","Table access method must be blank.","TGTYPESCREENREG");

        TAP.$("ELE_DRPDWN_TABLEACCESSMETHOD_PLANPREMIUMPAGE","//div[@id='workbenchForm:workbenchTabs:methodFrequencyAccessMethod']//div//span","TGTYPESCREENREG");

        TAP.$("ELE_DRPDWN_TABLEACCESSMETHOD_PROCESSDATE","//li[@id='workbenchForm:workbenchTabs:methodFrequencyAccessMethod_2']","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_VALIDATE","//span[contains(text(),'Validate')]","TGTYPESCREENREG");

        WAIT.$("ELE_MSG_ERRORMESG_BELOWFIELD","//span[@class='ui-message-error-detail']","TGTYPESCREENREG");

        ASSERT.$("ELE_MSG_ERRORMESG_BELOWFIELD","//span[@class='ui-message-error-detail']","CONTAINS","Table access method must be blank.","TGTYPESCREENREG");

        TAP.$("ELE_DRPDWN_TABLEACCESSMETHOD_PLANPREMIUMPAGE","//div[@id='workbenchForm:workbenchTabs:methodFrequencyAccessMethod']//div//span","TGTYPESCREENREG");

        TAP.$("ELE_DRPDWN_TABLEACCESSMETHOD_BLANK","//li[@id='workbenchForm:workbenchTabs:methodFrequencyAccessMethod_0']","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_TABLE_FREQUENCY","//input[@id='workbenchForm:workbenchTabs:methodFrequencyTable']","ATest","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_VALIDATE","//span[contains(text(),'Validate')]","TGTYPESCREENREG");

        WAIT.$("ELE_MSG_ERRORMESG_BELOWFIELD","//span[@class='ui-message-error-detail']","VISIBLE","TGTYPESCREENREG");

        ASSERT.$("ELE_MSG_ERRORMESG_BELOWFIELD","//span[@class='ui-message-error-detail']","CONTAINS","Table access method required.","TGTYPESCREENREG");

        TAP.$("ELE_DRPDWN_TABLEACCESSMETHOD_PLANPREMIUMPAGE","//div[@id='workbenchForm:workbenchTabs:methodFrequencyAccessMethod']//div//span","TGTYPESCREENREG");

        TAP.$("ELE_DRPDWN_TABLEACCESSMETHOD_PROCESSDATE","//li[@id='workbenchForm:workbenchTabs:methodFrequencyAccessMethod_2']","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_VALIDATE","//span[contains(text(),'Validate')]","TGTYPESCREENREG");

        WAIT.$("ELE_MSG_VALIDATIONSUCCESS","//div[@id='workbenchForm:workbenchTabs:messages']//span[@class='ui-messages-info-summary'][contains(text(),'Validation completed successfully with no errors.')]","VISIBLE","TGTYPESCREENREG");

        TAP.$("ELE_LOGO_GIAS","//span[text()='GIAS']","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

        CALL.$("CopyValidationsonPaymentMethodPageBB844","TGTYPESCREENREG");

        SWIPE.$("DOWN","TGTYPESCREENREG");

        SWIPE.$("DOWN","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_SEARCHTASKS","//input[@id='workbenchForm:workbenchTabs:taskSearchContainer:taskSearchSearch']","BLC01","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_TASKSEARCH","//a[@id='workbenchForm:workbenchTabs:taskSearchContainer:tb_buttonSearch']//span[@class='fa fa-search fa-lg']","TGTYPESCREENREG");

        WAIT.$(1,"TGTYPESCREENREG");

        TAP.$("ELE_LABEL_VALIDPAYMENTMETHODFREQUENCY","//span[contains(text(),'Valid Payment Method')]","TGTYPESCREENREG");

        WAIT.$("ELE_TXTBOX_TABLE_VALIDPYMENTMETHODFREQUENCY","//input[@id='workbenchForm:workbenchTabs:grid:methodFrequencyTable_Col:filter']","TGTYPESCREENREG");

        TAP.$("ELE_LINK_ROW1","//tr[@class='ui-widget-content ui-datatable-even ui-datatable-selectable']","TGTYPESCREENREG");

        WAIT.$("ELE_BUTTON_COPY","//span[contains(text(),'Copy')]","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_COPY","//span[contains(text(),'Copy')]","TGTYPESCREENREG");

        ASSERT.$("ELE_DRPDWN_LABEL_PAYMENTMETHOD","//label[@id='workbenchForm:workbenchTabs:paymentMethod_label']","CONTAINS","Consolidated Billing","TGTYPESCREENREG");

        ASSERT.$("ELE_DRPDWN_LABEL_PAYMENTFREQUENCY","//label[@id='workbenchForm:workbenchTabs:paymentFrequency_label']","CONTAINS","Annual","TGTYPESCREENREG");

        TAP.$("ELE_DRPDWN_PAYMENTMETHOD_FREQUENCY","//div[@id='workbenchForm:workbenchTabs:paymentMethod']//div//span","TGTYPESCREENREG");

        ASSERT.$("ELE_DRPDWN_CONSILDTEDBILLING","//li[@id='workbenchForm:workbenchTabs:paymentMethod_0']","VISIBLE","TGTYPESCREENREG");

        ASSERT.$("ELE_DRPDWN_COUPONBOOK","//li[@id='workbenchForm:workbenchTabs:paymentMethod_1']","VISIBLE","TGTYPESCREENREG");

        ASSERT.$("ELE_DRPDWN_CREDITCARDDEDUCTION","//li[@id='workbenchForm:workbenchTabs:paymentMethod_2']","VISIBLE","TGTYPESCREENREG");

        ASSERT.$("ELE_DRPDWN_DIRECTPREMIUMNOTICE","//li[@id='workbenchForm:workbenchTabs:paymentMethod_3']","VISIBLE","TGTYPESCREENREG");

        ASSERT.$("ELE_DRPDWN_ELECTRONICFUNDSTRANSFER","//li[@id='workbenchForm:workbenchTabs:paymentMethod_4']","VISIBLE","TGTYPESCREENREG");

        ASSERT.$("ELE_DRPDWN_FAMILYBILL","//li[@id='workbenchForm:workbenchTabs:paymentMethod_5']","VISIBLE","TGTYPESCREENREG");

        ASSERT.$("ELE_DRPDWN_GOVERNMENTALLOTMENTS","//li[@id='workbenchForm:workbenchTabs:paymentMethod_6']","VISIBLE","TGTYPESCREENREG");

        ASSERT.$("ELE_DRPDWN_LISTBILL","//li[@id='workbenchForm:workbenchTabs:paymentMethod_7']","VISIBLE","TGTYPESCREENREG");

        ASSERT.$("ELE_DRPDWN_NONOTICE","//li[@id='workbenchForm:workbenchTabs:paymentMethod_8']","VISIBLE","TGTYPESCREENREG");

        ASSERT.$("ELE_DRPDWN_PAYORDEBT","//li[@id='workbenchForm:workbenchTabs:paymentMethod_9']","VISIBLE","TGTYPESCREENREG");

        ASSERT.$("ELE_DRPDWN_PAYORDISABILITY","//li[@id='workbenchForm:workbenchTabs:paymentMethod_10']","VISIBLE","TGTYPESCREENREG");

        ASSERT.$("ELE_DRPDWN_PAYORDEDUCTION","//li[@id='workbenchForm:workbenchTabs:paymentMethod_11']","VISIBLE","TGTYPESCREENREG");

        ASSERT.$("ELE_DRPDWN_PREMIUMDEPOSITFUND","//li[@id='workbenchForm:workbenchTabs:paymentMethod_12']","VISIBLE","TGTYPESCREENREG");

        ASSERT.$("ELE_DRPDWN_SALARYSAVINGS","//li[@id='workbenchForm:workbenchTabs:paymentMethod_13']","VISIBLE","TGTYPESCREENREG");

        ASSERT.$("ELE_DRPDWN_SIGHTDRAFT","//li[@id='workbenchForm:workbenchTabs:paymentMethod_14']","VISIBLE","TGTYPESCREENREG");

        ASSERT.$("ELE_DRPDWN_WAIVEROFPREMIUM","//li[@id='workbenchForm:workbenchTabs:paymentMethod_15']","VISIBLE","TGTYPESCREENREG");

        TAP.$("ELE_DRPDWN_CONSILDTEDBILLING","//li[@id='workbenchForm:workbenchTabs:paymentMethod_0']","TGTYPESCREENREG");

        TAP.$("ELE_DRPDWN_PAYMENTFREQUENCY_FREQUENCY","//div[@id='workbenchForm:workbenchTabs:paymentFrequency']//div//span","TGTYPESCREENREG");

        ASSERT.$("ELE_DRPDWN_ANNUAL","//li[@id='workbenchForm:workbenchTabs:paymentFrequency_0']","VISIBLE","TGTYPESCREENREG");

        ASSERT.$("ELE_DRPDWN_BIWEEKLY","//li[@id='workbenchForm:workbenchTabs:paymentFrequency_1']","VISIBLE","TGTYPESCREENREG");

        ASSERT.$("ELE_DRPDWN_MONTHLY","//li[@id='workbenchForm:workbenchTabs:paymentFrequency_2']","VISIBLE","TGTYPESCREENREG");

        ASSERT.$("ELE_DRPDWN_QUARTERLY","//li[@id='workbenchForm:workbenchTabs:paymentFrequency_3']","VISIBLE","TGTYPESCREENREG");

        ASSERT.$("ELE_DRPDWN_SEMIANNUAL","//li[@id='workbenchForm:workbenchTabs:paymentFrequency_4']","VISIBLE","TGTYPESCREENREG");

        ASSERT.$("ELE_DRPDWN_SEMIMONTHLY","//li[@id='workbenchForm:workbenchTabs:paymentFrequency_5']","VISIBLE","TGTYPESCREENREG");

        ASSERT.$("ELE_DRPDWN_THIRTEENTHLY","//li[@id='workbenchForm:workbenchTabs:paymentFrequency_6']","VISIBLE","TGTYPESCREENREG");

        ASSERT.$("ELE_DRPDWN_WEEKLY","//li[@id='workbenchForm:workbenchTabs:paymentFrequency_7']","VISIBLE","TGTYPESCREENREG");

        TAP.$("ELE_DRPDWN_ANNUAL","//li[@id='workbenchForm:workbenchTabs:paymentFrequency_0']","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SAVE","//span[contains(text(),'Save')]","TGTYPESCREENREG");

        ASSERT.$("ELE_ERRORMSG_ATTEMPTEDTOADDRESOURCEALREADYONFILE","//div[@id='workbenchForm:workbenchTabs:messages']//span[@class='ui-messages-error-summary'][contains(text(),'Attempted to add an item that already exists in fi')]","VISIBLE","TGTYPESCREENREG");

        TAP.$("ELE_TAB_CLOSE","//li[@class='ui-state-default ui-tabs-selected ui-state-active ui-corner-top']//span[@class='ui-icon ui-icon-close']","TGTYPESCREENREG");

        WAIT.$("ELE_BUTTON_ADD2","//button[@id='workbenchForm:workbenchTabs:tb_buttonAdd']//span[@class='ui-button-text ui-c'][contains(text(),'Add')]","VISIBLE","TGTYPESCREENREG");

        CALL.$("AddPageAndFieldValidationsTBD2BB844","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_ADD2","//button[@id='workbenchForm:workbenchTabs:tb_buttonAdd']//span[@class='ui-button-text ui-c'][contains(text(),'Add')]","TGTYPESCREENREG");

        WAIT.$("ELE_TXTBOX_TABLE_FREQUENCY","//input[@id='workbenchForm:workbenchTabs:methodFrequencyTable']","VISIBLE","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

        ASSERT.$("ELE_DRPDWN_LABEL_PAYMENTMETHOD","//label[@id='workbenchForm:workbenchTabs:paymentMethod_label']","CONTAINS","Consolidated Billing","TGTYPESCREENREG");

        ASSERT.$("ELE_DRPDWN_LABEL_PAYMENTFREQUENCY","//label[@id='workbenchForm:workbenchTabs:paymentFrequency_label']","CONTAINS","Annual","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_VALIDATE","//span[contains(text(),'Validate')]","TGTYPESCREENREG");

        WAIT.$("ELE_ERRORMSG_TABLECANNOTBEBLANK","//span[@class='ui-message-error-detail'][contains(text(),'Table cannot be blank.')]","VISIBLE","TGTYPESCREENREG");

        ASSERT.$("ELE_ERRORMSG_TABLECANNOTBEBLANK","//span[@class='ui-message-error-detail'][contains(text(),'Table cannot be blank.')]","VISIBLE","TGTYPESCREENREG");

        ASSERT.$("ELE_ERRORMSG_EFFECTIVEDATECANNOTBEBLANK","//span[@class='ui-message-error-detail'][contains(text(),'Effective date cannot be blank.')]","VISIBLE","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_TABLE_FREQUENCY","//input[@id='workbenchForm:workbenchTabs:methodFrequencyTable']","JBTST1","FALSE","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_EFFECTIVEDATE","//input[@id='workbenchForm:workbenchTabs:effectiveDate_input']","1/1","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_VALIDATE","//span[contains(text(),'Validate')]","TGTYPESCREENREG");

        ASSERT.$("ELE_MSG_ERRORMESG_BELOWFIELD","//span[@class='ui-message-error-detail']","CONTAINS","is not a valid date.","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_EFFECTIVEDATE","//input[@id='workbenchForm:workbenchTabs:effectiveDate_input']","07/30/2020","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_VALIDATE","//span[contains(text(),'Validate')]","TGTYPESCREENREG");

        ASSERT.$("ELE_ERRORMSG_ATTEMPTEDTOADDRESOURCEALREADYONFILE","//div[@id='workbenchForm:workbenchTabs:messages']//span[@class='ui-messages-error-summary'][contains(text(),'Attempted to add an item that already exists in fi')]","VISIBLE","TGTYPESCREENREG");

        TAP.$("ELE_DRPDWN_PAYMENTMETHOD_FREQUENCY","//div[@id='workbenchForm:workbenchTabs:paymentMethod']//div//span","TGTYPESCREENREG");

        ASSERT.$("ELE_DRPDWN_CONSILDTEDBILLING","//li[@id='workbenchForm:workbenchTabs:paymentMethod_0']","VISIBLE","TGTYPESCREENREG");

        ASSERT.$("ELE_DRPDWN_COUPONBOOK","//li[@id='workbenchForm:workbenchTabs:paymentMethod_1']","VISIBLE","TGTYPESCREENREG");

        ASSERT.$("ELE_DRPDWN_CREDITCARDDEDUCTION","//li[@id='workbenchForm:workbenchTabs:paymentMethod_2']","VISIBLE","TGTYPESCREENREG");

        ASSERT.$("ELE_DRPDWN_DIRECTPREMIUMNOTICE","//li[@id='workbenchForm:workbenchTabs:paymentMethod_3']","VISIBLE","TGTYPESCREENREG");

        ASSERT.$("ELE_DRPDWN_ELECTRONICFUNDSTRANSFER","//li[@id='workbenchForm:workbenchTabs:paymentMethod_4']","VISIBLE","TGTYPESCREENREG");

        ASSERT.$("ELE_DRPDWN_FAMILYBILL","//li[@id='workbenchForm:workbenchTabs:paymentMethod_5']","VISIBLE","TGTYPESCREENREG");

        ASSERT.$("ELE_DRPDWN_GOVERNMENTALLOTMENTS","//li[@id='workbenchForm:workbenchTabs:paymentMethod_6']","VISIBLE","TGTYPESCREENREG");

        ASSERT.$("ELE_DRPDWN_LISTBILL","//li[@id='workbenchForm:workbenchTabs:paymentMethod_7']","VISIBLE","TGTYPESCREENREG");

        ASSERT.$("ELE_DRPDWN_NONOTICE","//li[@id='workbenchForm:workbenchTabs:paymentMethod_8']","VISIBLE","TGTYPESCREENREG");

        ASSERT.$("ELE_DRPDWN_PAYORDEBT","//li[@id='workbenchForm:workbenchTabs:paymentMethod_9']","VISIBLE","TGTYPESCREENREG");

        ASSERT.$("ELE_DRPDWN_PAYORDISABILITY","//li[@id='workbenchForm:workbenchTabs:paymentMethod_10']","VISIBLE","TGTYPESCREENREG");

        ASSERT.$("ELE_DRPDWN_PAYORDEDUCTION","//li[@id='workbenchForm:workbenchTabs:paymentMethod_11']","VISIBLE","TGTYPESCREENREG");

        ASSERT.$("ELE_DRPDWN_PREMIUMDEPOSITFUND","//li[@id='workbenchForm:workbenchTabs:paymentMethod_12']","VISIBLE","TGTYPESCREENREG");

        ASSERT.$("ELE_DRPDWN_SALARYSAVINGS","//li[@id='workbenchForm:workbenchTabs:paymentMethod_13']","VISIBLE","TGTYPESCREENREG");

        ASSERT.$("ELE_DRPDWN_SIGHTDRAFT","//li[@id='workbenchForm:workbenchTabs:paymentMethod_14']","VISIBLE","TGTYPESCREENREG");

        ASSERT.$("ELE_DRPDWN_WAIVEROFPREMIUM","//li[@id='workbenchForm:workbenchTabs:paymentMethod_15']","VISIBLE","TGTYPESCREENREG");

        TAP.$("ELE_DRPDWN_CONSILDTEDBILLING","//li[@id='workbenchForm:workbenchTabs:paymentMethod_0']","TGTYPESCREENREG");

        TAP.$("ELE_DRPDWN_PAYMENTFREQUENCY_FREQUENCY","//div[@id='workbenchForm:workbenchTabs:paymentFrequency']//div//span","TGTYPESCREENREG");

        ASSERT.$("ELE_DRPDWN_ANNUAL","//li[@id='workbenchForm:workbenchTabs:paymentFrequency_0']","VISIBLE","TGTYPESCREENREG");

        ASSERT.$("ELE_DRPDWN_BIWEEKLY","//li[@id='workbenchForm:workbenchTabs:paymentFrequency_1']","VISIBLE","TGTYPESCREENREG");

        ASSERT.$("ELE_DRPDWN_MONTHLY","//li[@id='workbenchForm:workbenchTabs:paymentFrequency_2']","VISIBLE","TGTYPESCREENREG");

        ASSERT.$("ELE_DRPDWN_QUARTERLY","//li[@id='workbenchForm:workbenchTabs:paymentFrequency_3']","VISIBLE","TGTYPESCREENREG");

        ASSERT.$("ELE_DRPDWN_SEMIANNUAL","//li[@id='workbenchForm:workbenchTabs:paymentFrequency_4']","VISIBLE","TGTYPESCREENREG");

        ASSERT.$("ELE_DRPDWN_SEMIMONTHLY","//li[@id='workbenchForm:workbenchTabs:paymentFrequency_5']","VISIBLE","TGTYPESCREENREG");

        ASSERT.$("ELE_DRPDWN_THIRTEENTHLY","//li[@id='workbenchForm:workbenchTabs:paymentFrequency_6']","VISIBLE","TGTYPESCREENREG");

        ASSERT.$("ELE_DRPDWN_WEEKLY","//li[@id='workbenchForm:workbenchTabs:paymentFrequency_7']","VISIBLE","TGTYPESCREENREG");

        TAP.$("ELE_DRPDWN_ANNUAL","//li[@id='workbenchForm:workbenchTabs:paymentFrequency_0']","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_TABLE_FREQUENCY","//input[@id='workbenchForm:workbenchTabs:methodFrequencyTable']","JBTST2","FALSE","TGTYPESCREENREG");

        WAIT.$("ELE_MSG_VALIDATIONSUCCESS","//div[@id='workbenchForm:workbenchTabs:messages']//span[@class='ui-messages-info-summary'][contains(text(),'Validation completed successfully with no errors.')]","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SAVE","//span[contains(text(),'Save')]","TGTYPESCREENREG");

        WAIT.$("ELE_LABEL_ITEMSUCCESSFULLYADDED","//div[@id='workbenchForm:workbenchTabs:messages']//span[@class='ui-messages-info-summary'][contains(text(),'Item successfully added.')]","VISIBLE","TGTYPESCREENREG");

        TAP.$("ELE_TAB_CLOSE","//li[@class='ui-state-default ui-tabs-selected ui-state-active ui-corner-top']//span[@class='ui-icon ui-icon-close']","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        CALL.$("DeleteValidPaymentMethodFrequencyTableBB844TEMP","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_TABLE_VALIDPYMENTMETHODFREQUENCY","//input[@id='workbenchForm:workbenchTabs:grid:methodFrequencyTable_Col:filter']","JBTST2","FALSE","TGTYPESCREENREG");

        WAIT.$(3,"TGTYPESCREENREG");

        TAP.$("ELE_LINK_ROW1","//tr[@class='ui-widget-content ui-datatable-even ui-datatable-selectable']","TGTYPESCREENREG");

        WAIT.$("ELE_BUTTON_DELETE","//span[contains(text(),'Delete')]","VISIBLE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_DELETE","//span[contains(text(),'Delete')]","TGTYPESCREENREG");

		var_Count = var_Count + ( 1 );
                 LOGGER.info("Executed Step = INCREASEVAR,var_Count,1,TGTYPESCREENREG");
    	}
          LOGGER.info("Executed Step = WEND,TGTYPESCREENREG");
    	resetAttemps();

    }


    @Test
    public void ftj41modf095allscenarios() {

        // No need to declare / print method name here, handled by TestNg listener
        // declare("ftj41modf095allscenarios");

		int var_Count = 0;
                 LOGGER.info("Executed Step = VAR,Integer,var_Count,0,TGTYPESCREENREG");
		com.jayway.jsonpath.ReadContext var_CSVData = getJsonData("http://10.1.104.229/json/20200831/zP695q.json");
                 LOGGER.info("Executed Step = VAR,Json,var_CSVData,http://10.1.104.229/json/20200831/zP695q.json,TGTYPESCREENREG");
        LOGGER.info("Executed Step = REPEAT IF");
    	while (check(var_Count,"<",1,"TGTYPESCREENREG")) {
          LOGGER.info("Executed Step = WHILE,var_Count,<,1,TGTYPESCREENREG");
		String var_GroupNumber;
                 LOGGER.info("Executed Step = VAR,String,var_GroupNumber,TGTYPESCREENREG");
		var_GroupNumber = getJsonData(var_CSVData , "$.records.["+var_Count+"].GroupNumber");
                 LOGGER.info("Executed Step = STORE,var_GroupNumber,var_CSVData,$.records.[{var_Count}].GroupNumber,TGTYPESCREENREG");
		String var_ProductIdentifier;
                 LOGGER.info("Executed Step = VAR,String,var_ProductIdentifier,TGTYPESCREENREG");
		var_ProductIdentifier = getJsonData(var_CSVData , "$.records.["+var_Count+"].ProductIdentifier");
                 LOGGER.info("Executed Step = STORE,var_ProductIdentifier,var_CSVData,$.records.[{var_Count}].ProductIdentifier,TGTYPESCREENREG");
		String var_Policy1;
                 LOGGER.info("Executed Step = VAR,String,var_Policy1,TGTYPESCREENREG");
		var_Policy1 = getJsonData(var_CSVData , "$.records.["+var_Count+"].Policy1");
                 LOGGER.info("Executed Step = STORE,var_Policy1,var_CSVData,$.records.[{var_Count}].Policy1,TGTYPESCREENREG");
		String var_SituationCodeBB1046;
                 LOGGER.info("Executed Step = VAR,String,var_SituationCodeBB1046,TGTYPESCREENREG");
		var_SituationCodeBB1046 = getJsonData(var_CSVData , "$.records.["+var_Count+"].SituationCodeBB1046");
                 LOGGER.info("Executed Step = STORE,var_SituationCodeBB1046,var_CSVData,$.records.[{var_Count}].SituationCodeBB1046,TGTYPESCREENREG");
		String var_Policy2;
                 LOGGER.info("Executed Step = VAR,String,var_Policy2,TGTYPESCREENREG");
		var_Policy2 = getJsonData(var_CSVData , "$.records.["+var_Count+"].Policy2");
                 LOGGER.info("Executed Step = STORE,var_Policy2,var_CSVData,$.records.[{var_Count}].Policy2,TGTYPESCREENREG");
		String var_SituationCodeBB1045 = "$.records.[{Count}].SituationCodeBB1045";
                 LOGGER.info("Executed Step = VAR,String,var_SituationCodeBB1045,$.records.[{Count}].SituationCodeBB1045,TGTYPESCREENREG");
		var_SituationCodeBB1045 = getJsonData(var_CSVData , "$.records.["+var_Count+"].SituationCodeBB1045");
                 LOGGER.info("Executed Step = STORE,var_SituationCodeBB1045,var_CSVData,$.records.[{var_Count}].SituationCodeBB1045,TGTYPESCREENREG");
        CALL.$("FTJQA2Login","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

		try { 
		 WebElement html = driver.findElement(By.tagName("html"));
		    	
		    	System.out.println("html size" + html.getSize());
		    	System.out.println("html" + html.getTagName());

		    	for( int i =0 ;i<4;i++) {
		        html.sendKeys(Keys.chord(Keys.CONTROL, Keys.SUBTRACT));
		    	}
		 
		 ;} 
		 catch(Exception e) { PrintVal("Renderer Script Exception : "+e.toString()); } 
		 PrintVal("Executed Step = CALLRENDERERSCRIPT,FTJ41ZOOMMAGNIFICATIONCHANGE"); 
        TYPE.$("ELE_TXTBOX_USERNAME","//input[@id='guestForm:username']","x5080","FALSE","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_PASSWORD","//input[@id='guestForm:password']","Welcome123$","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_LOGIN","//button[@id='guestForm:loginButton']","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        CALL.$("F095SituationCodeDrpDwnListValidationsBB1255","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_SEARCHTASKS","//input[@id='workbenchForm:workbenchTabs:taskSearchContainer:taskSearchSearch']","bps060","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_TASKSEARCH","//a[@id='workbenchForm:workbenchTabs:taskSearchContainer:tb_buttonSearch']//span[@class='fa fa-search fa-lg']","TGTYPESCREENREG");

        TAP.$("ELE_LABEL_GROUPPRODUCTS","//span[contains(text(),'Group Products')]","TGTYPESCREENREG");

        WAIT.$("ELE_TXTBOX_GROUPNUMBER_GROUPPRODUCTSPAGE","//input[@id='workbenchForm:workbenchTabs:grid:groupNumber_filter']","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_GROUPNUMBER_GROUPPRODUCTSPAGE","//input[@id='workbenchForm:workbenchTabs:grid:groupNumber_filter']",var_GroupNumber,"FALSE","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_PRODUCTIDENTIFIER","//input[@name='workbenchForm:workbenchTabs:grid:productIdFind_Col:filter']",var_ProductIdentifier,"FALSE","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        TAP.$("ELE_LINK_ROW1","//tr[@class='ui-widget-content ui-datatable-even ui-datatable-selectable']","TGTYPESCREENREG");

        WAIT.$("ELE_BUTTON_EDIT","//span[text()='Edit']","VISIBLE","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_EDIT","//span[text()='Edit']","TGTYPESCREENREG");

        WAIT.$("ELE_RADIOBUTTON_BYPASSCOMMISSION_YES","//table[@id='workbenchForm:workbenchTabs:bypassCommission']//div[@class='ui-radiobutton-box ui-widget ui-corner-all ui-state-default']","VISIBLE","TGTYPESCREENREG");

        SWIPE.$("UP","TGTYPESCREENREG");

        SWIPE.$("UP","TGTYPESCREENREG");

        SWIPE.$("UP","TGTYPESCREENREG");

        SWIPE.$("UP","TGTYPESCREENREG");

        SWIPE.$("UP","TGTYPESCREENREG");

        SWIPE.$("UP","TGTYPESCREENREG");

        SWIPE.$("UP","TGTYPESCREENREG");

        SWIPE.$("UP","TGTYPESCREENREG");

        SWIPE.$("UP","TGTYPESCREENREG");

        SWIPE.$("UP","TGTYPESCREENREG");

        TAP.$("ELE_RADIOBUTTON_BYPASSCOMMISSION_NO","//table[@id='workbenchForm:workbenchTabs:bypassCommission']//span[@class='ui-radiobutton-icon ui-icon ui-icon-blank ui-c']","TGTYPESCREENREG");

        WAIT.$("ELE_DRPDWN_SITUATIONCODE_UPDATEGROUPPAGE","//div[@id='workbenchForm:workbenchTabs:situationCode']//div//span","VISIBLE","TGTYPESCREENREG");

        WAIT.$(1,"TGTYPESCREENREG");

        TAP.$("ELE_DRPDWN_CONTRACTTYPE","//div[@id='workbenchForm:workbenchTabs:contractType']//span[@class='ui-icon ui-icon-triangle-1-s ui-c']","TGTYPESCREENREG");

        TAP.$("ELE_DRPDWN_CONTRACTTYPE_SCTESTING","//li[contains(text(),'SC Testing')]","TGTYPESCREENREG");

        WAIT.$(1,"TGTYPESCREENREG");

        TAP.$("ELE_DRPDWN_SITUATIONCODE_UPDATEGROUPPAGE","//div[@id='workbenchForm:workbenchTabs:situationCode']//div//span","TGTYPESCREENREG");

        TAP.$("ELE_DRPDWN_SITUATIONCODE_TEST01","//li[contains(text(),'TEST01')]","TGTYPESCREENREG");

        WAIT.$(3,"TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SAVE","//span[contains(text(),'Save')]","TGTYPESCREENREG");

        WAIT.$("ELE_MSG_ITEMSUCCESSUPDATE","//div[@id='workbenchForm:workbenchTabs:messages']//span[@class='ui-messages-info-summary'][contains(text(),'Item successfully updated.')]","VISIBLE","TGTYPESCREENREG");

        WAIT.$(1,"TGTYPESCREENREG");

        ASSERT.$("ELE_GETVAL_CONTRACTTYPE","//span[@id='workbenchForm:workbenchTabs:contractType']","CONTAINS","SC Testing","TGTYPESCREENREG");

        ASSERT.$("ELE_GETVAL_SITUATIONCODE","//span[@id='workbenchForm:workbenchTabs:situationCode']","CONTAINS","TEST01","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_EDIT","//span[text()='Edit']","TGTYPESCREENREG");

        WAIT.$("ELE_RADIOBUTTON_BYPASSCOMMISSION_NO","//table[@id='workbenchForm:workbenchTabs:bypassCommission']//span[@class='ui-radiobutton-icon ui-icon ui-icon-blank ui-c']","VISIBLE","TGTYPESCREENREG");

        TAP.$("ELE_RADIOBUTTON_BYPASSCOMMISSION_YES","//table[@id='workbenchForm:workbenchTabs:bypassCommission']//div[@class='ui-radiobutton-box ui-widget ui-corner-all ui-state-default']","TGTYPESCREENREG");

        WAIT.$(12,"TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SAVE","//span[contains(text(),'Save')]","TGTYPESCREENREG");

        WAIT.$("ELE_MSG_ITEMSUCCESSUPDATE","//div[@id='workbenchForm:workbenchTabs:messages']//span[@class='ui-messages-info-summary'][contains(text(),'Item successfully updated.')]","VISIBLE","TGTYPESCREENFULL");

        ASSERT.$("ELE_GETVAL_CONTRACTTYPE","//span[@id='workbenchForm:workbenchTabs:contractType']","INVISIBLE","TGTYPESCREENREG");

        ASSERT.$("ELE_GETVAL_SITUATIONCODE","//span[@id='workbenchForm:workbenchTabs:situationCode']","INVISIBLE","TGTYPESCREENREG");

        TAP.$("ELE_LOGO_GIAS","//span[text()='GIAS']","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

        CALL.$("F095SituationCodeDrpDwnValidtionsAgentContractPageBB1042","TGTYPESCREENREG");

        SWIPE.$("DOWN","TGTYPESCREENREG");

        SWIPE.$("DOWN","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_SEARCHTASKS","//input[@id='workbenchForm:workbenchTabs:taskSearchContainer:taskSearchSearch']","Agents","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_TASKSEARCH","//a[@id='workbenchForm:workbenchTabs:taskSearchContainer:tb_buttonSearch']//span[@class='fa fa-search fa-lg']","TGTYPESCREENREG");

        WAIT.$(1,"TGTYPESCREENREG");

        TAP.$("ELE_LABEL_AGENTS","//span[contains(text(),'Agents')]","TGTYPESCREENREG");

        WAIT.$("ELE_TXTBOX_AGENTNUMBER","//input[@name='workbenchForm:workbenchTabs:grid1:agentNumber_Col:filter']","VISIBLE","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_AGENTNUMBER","//input[@name='workbenchForm:workbenchTabs:grid1:agentNumber_Col:filter']","A13000000","FALSE","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        TAP.$("ELE_LINK_ROW1","//tr[@class='ui-widget-content ui-datatable-even ui-datatable-selectable']","TGTYPESCREENREG");

        WAIT.$(14,"TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SHOWLINK","//span[@class='ui-button-icon-left ui-icon ui-c fa fa-ellipsis-v fa-2x']","TGTYPESCREENREG");

        WAIT.$(1,"TGTYPESCREENREG");

        TAP.$("ELE_LINK_AGENTCONTRACTS","//span[contains(text(),'Agent Contracts')]","TGTYPESCREENREG");

        WAIT.$("ELE_LINK_ROW1","//tr[@class='ui-widget-content ui-datatable-even ui-datatable-selectable']","VISIBLE","TGTYPESCREENREG");

        ASSERT.$("ELE_GETVAL_SITUATIONCODEDESCRIPTION_BLANK","//tr[@class='ui-widget-content ui-datatable-even ui-datatable-selectable']//span[@class='OutputGeneral'][contains(text(),'<blank>')]","VISIBLE","TGTYPESCREENREG");

        TAP.$("ELE_LINK_ROW1","//tr[@class='ui-widget-content ui-datatable-even ui-datatable-selectable']","TGTYPESCREENREG");

        WAIT.$("ELE_BUTTON_EDIT","//span[text()='Edit']","VISIBLE","TGTYPESCREENREG");

        WAIT.$(1,"TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_EDIT","//span[text()='Edit']","TGTYPESCREENREG");

        WAIT.$("ELE_BUTTON_VALIDATE","//span[contains(text(),'Validate')]","VISIBLE","TGTYPESCREENREG");

        WAIT.$(1,"TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_VALIDATE","//span[contains(text(),'Validate')]","TGTYPESCREENREG");

        WAIT.$("ELE_ERRORMSG_SITUATIONCODEREQUIRED","//div[@id='workbenchForm:workbenchTabs:messages']//span[@class='ui-messages-error-summary'][contains(text(),'Situation code is required.')]","VISIBLE","TGTYPESCREENREG");

        TAP.$("ELE_TAB_CLOSE","//li[@class='ui-state-default ui-tabs-selected ui-state-active ui-corner-top']//span[@class='ui-icon ui-icon-close']","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        WAIT.$("ELE_BUTTON_ADD","//span[contains(text(),'Add')]","VISIBLE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_ADD","//span[contains(text(),'Add')]","TGTYPESCREENREG");

        WAIT.$("ELE_DRPDWN_SITUATIONCODE","//div[@id='workbenchForm:workbenchTabs:agentSituationCode']//div//span","VISIBLE","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_EFFECTIVEDATE","//input[@id='workbenchForm:workbenchTabs:effectiveDate_input']","8/8/2020","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_DRPDWN_SITUATIONCODE","//div[@id='workbenchForm:workbenchTabs:agentSituationCode']//div//span","TGTYPESCREENREG");

        ASSERT.$("ELE_DRPDWN_SITUATIONCODE_BLANK","//li[@id='workbenchForm:workbenchTabs:agentSituationCode_0']","VISIBLE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_CONTINUE","//span[contains(text(),'Continue')]","TGTYPESCREENREG");

        WAIT.$("ELE_BUTTON_SAVE","//span[contains(text(),'Save')]","VISIBLE","TGTYPESCREENREG");

        WAIT.$(1,"TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_VALIDATE","//span[contains(text(),'Validate')]","TGTYPESCREENREG");

        WAIT.$("ELE_ERRORMSG_SITUATIONCODEREQUIRED","//div[@id='workbenchForm:workbenchTabs:messages']//span[@class='ui-messages-error-summary'][contains(text(),'Situation code is required.')]","VISIBLE","TGTYPESCREENREG");

        TAP.$("ELE_LOGO_GIAS","//span[text()='GIAS']","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

        CALL.$("F095DisplayDescriptionForSituationCodeNTL450PageBB1045","TGTYPESCREENREG");

        SWIPE.$("DOWN","TGTYPESCREENREG");

        SWIPE.$("DOWN","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_SEARCHTASKS","//input[@id='workbenchForm:workbenchTabs:taskSearchContainer:taskSearchSearch']","Policy Inquiry","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_TASKSEARCH","//a[@id='workbenchForm:workbenchTabs:taskSearchContainer:tb_buttonSearch']//span[@class='fa fa-search fa-lg']","TGTYPESCREENREG");

        WAIT.$(1,"TGTYPESCREENREG");

        TAP.$("ELE_LINK_POLICYINQUIRY","//span[contains(text(),'Policy Inquiry')]","TGTYPESCREENREG");

        WAIT.$("ELE_TXTBOX_POLICYNUMBER","//input[@id='workbenchForm:workbenchTabs:policyNumber']","VISIBLE","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_POLICYNUMBER","//input[@id='workbenchForm:workbenchTabs:policyNumber']",var_Policy2,"FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_CONTINUE","//span[contains(text(),'Continue')]","TGTYPESCREENREG");

        WAIT.$("ELE_BUTTON_SHOWLINK","//span[@class='ui-button-icon-left ui-icon ui-c fa fa-ellipsis-v fa-2x']","VISIBLE","TGTYPESCREENREG");

        WAIT.$(1,"TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SHOWLINK","//span[@class='ui-button-icon-left ui-icon ui-c fa fa-ellipsis-v fa-2x']","TGTYPESCREENREG");

        TAP.$("ELE_LINK_TRANSACTIONHISTORY","//span[text()='Transaction History']","TGTYPESCREENREG");

        WAIT.$("ELE_LINK_ROW1","//tr[@class='ui-widget-content ui-datatable-even ui-datatable-selectable']","VISIBLE","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        TAP.$("ELE_LINK_ROW1","//tr[@class='ui-widget-content ui-datatable-even ui-datatable-selectable']","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SHOWLINK","//span[@class='ui-button-icon-left ui-icon ui-c fa fa-ellipsis-v fa-2x']","TGTYPESCREENREG");

        TAP.$("ELE_LINK_PREMIUMCOMMISSIONHISTORY","//span[contains(text(),'Premium Commission History')]","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        TAP.$("ELE_LINK_ROW1","//tr[@class='ui-widget-content ui-datatable-even ui-datatable-selectable']","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        ASSERT.$("ELE_GETVALUE_SITUATIONCODE_AGENTPAGE","//span[@id='workbenchForm:workbenchTabs:agentSituationCode']","VISIBLE","TGTYPESCREENREG");

        ASSERT.$("ELE_GETVALUE_SITUATIONCODE_AGENTPAGE","//span[@id='workbenchForm:workbenchTabs:agentSituationCode']","CONTAINS",var_SituationCodeBB1045,"TGTYPESCREENREG");

        TAP.$("ELE_LOGO_GIAS","//span[text()='GIAS']","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

        CALL.$("F095ValidateNoDuplicatesOnAgentSituationCodesBB1253","TGTYPESCREENREG");

        SWIPE.$("DOWN","TGTYPESCREENREG");

        SWIPE.$("DOWN","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_SEARCHTASKS","//input[@id='workbenchForm:workbenchTabs:taskSearchContainer:taskSearchSearch']","com002","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_TASKSEARCH","//a[@id='workbenchForm:workbenchTabs:taskSearchContainer:tb_buttonSearch']//span[@class='fa fa-search fa-lg']","TGTYPESCREENREG");

        WAIT.$(1,"TGTYPESCREENREG");

        TAP.$("ELE_LABEL_AGENTSITUATIONCODE","//span[contains(text(),'Agent Situation Code')]","TGTYPESCREENREG");

        WAIT.$("ELE_DRPDWN_DISTRIBUTIONCHANNEL","//div[@id='workbenchForm:workbenchTabs:grid:distributionChannel_filter']//div//span","VISIBLE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_ADD2","//button[@id='workbenchForm:workbenchTabs:tb_buttonAdd']//span[@class='ui-button-text ui-c'][contains(text(),'Add')]","TGTYPESCREENREG");

        TYPE.$("ELE_TEXTBOX_SITUATIONCODE","//input[@id='workbenchForm:workbenchTabs:agentSituationCode']","1","FALSE","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_DESCRIPTION_AGENTSC","//input[@id='workbenchForm:workbenchTabs:agentSituationDesc']","AutoTesting","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_VALIDATE","//span[contains(text(),'Validate')]","TGTYPESCREENREG");

        WAIT.$("ELE_MSG_ERRORMESG_BELOWFIELD","//span[@class='ui-message-error-detail']","VISIBLE","TGTYPESCREENREG");

        ASSERT.$("ELE_MSG_ERRORMESG_BELOWFIELD","//span[@class='ui-message-error-detail']","CONTAINS","Situation code must be unique.","TGTYPESCREENREG");

        TYPE.$("ELE_TEXTBOX_SITUATIONCODE","//input[@id='workbenchForm:workbenchTabs:agentSituationCode']","AutoTest","FALSE","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_DESCRIPTION_AGENTSC","//input[@id='workbenchForm:workbenchTabs:agentSituationDesc']","ATest","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_VALIDATE","//span[contains(text(),'Validate')]","TGTYPESCREENREG");

        WAIT.$("ELE_MSG_ERRORMESG_BELOWFIELD","//span[@class='ui-message-error-detail']","VISIBLE","TGTYPESCREENREG");

        ASSERT.$("ELE_MSG_ERRORMESG_BELOWFIELD","//span[@class='ui-message-error-detail']","CONTAINS","Description must be unique.","TGTYPESCREENREG");

        TAP.$("ELE_TAB_CLOSE","//li[@class='ui-state-default ui-tabs-selected ui-state-active ui-corner-top']//span[@class='ui-icon ui-icon-close']","TGTYPESCREENREG");

        WAIT.$("ELE_DRPDWN_DISTCHANNEL_AGENTSC","//div[@id='workbenchForm:workbenchTabs:distributionChannel']//div//span","VISIBLE","TGTYPESCREENREG");

        TYPE.$("ELE_LABEL_SITUATIONCODE_AGENTSC","//input[@id='workbenchForm:workbenchTabs:grid:agentSituationCode_Col:filter']","01","FALSE","TGTYPESCREENREG");

        TYPE.$("ELE_LABEL_DESCRIPTION_AGENTSC","//input[@id='workbenchForm:workbenchTabs:grid:agentSituationDesc_Col:filter']","ATest","FALSE","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

        TAP.$("ELE_LINK_ROW1","//tr[@class='ui-widget-content ui-datatable-even ui-datatable-selectable']","TGTYPESCREENREG");

        WAIT.$("ELE_BUTTON_EDIT","//span[text()='Edit']","VISIBLE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_COPY","//span[contains(text(),'Copy')]","TGTYPESCREENREG");

        WAIT.$("ELE_BUTTON_VALIDATE","//span[contains(text(),'Validate')]","VISIBLE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_VALIDATE","//span[contains(text(),'Validate')]","TGTYPESCREENREG");

        WAIT.$("ELE_MSG_ERRORMESG_BELOWFIELD","//span[@class='ui-message-error-detail']","VISIBLE","TGTYPESCREENREG");

        ASSERT.$("ELE_MSG_ERRORMESG_BELOWFIELD","//span[@class='ui-message-error-detail']","CONTAINS","Situation code must be unique.","TGTYPESCREENREG");

        ASSERT.$("ELE_ERRORMSG_DESCRIPTIONMUSTBEUNIQUE","//span[@class='ui-message-error-detail'][contains(text(),'Description must be unique.')]","VISIBLE","TGTYPESCREENREG");

        TAP.$("ELE_DRPDWN_DISTCHANNEL_AGENTSC","//div[@id='workbenchForm:workbenchTabs:distributionChannel']//div//span","TGTYPESCREENREG");

        TAP.$("ELE_DRPDWN_DISTCHANNEL_ALL","//li[@id='workbenchForm:workbenchTabs:distributionChannel_0']","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

        ASSERT.$("ELE_MSG_ERRORMESG_BELOWFIELD","//span[@class='ui-message-error-detail']","CONTAINS","Situation code must be unique.","TGTYPESCREENREG");

        ASSERT.$("ELE_ERRORMSG_DESCRIPTIONMUSTBEUNIQUE","//span[@class='ui-message-error-detail'][contains(text(),'Description must be unique.')]","VISIBLE","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_DESCRIPTION_AGENTSC","//input[@id='workbenchForm:workbenchTabs:agentSituationDesc']","AutoTest","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_VALIDATE","//span[contains(text(),'Validate')]","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

        ASSERT.$("ELE_MSG_ERRORMESG_BELOWFIELD","//span[@class='ui-message-error-detail']","CONTAINS","Situation code must be unique.","TGTYPESCREENREG");

        TAP.$("ELE_LOGO_GIAS","//span[text()='GIAS']","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

        CALL.$("F095MakeSCDropDownListAccountProductsPageBB1256","TGTYPESCREENREG");

        SWIPE.$("DOWN","TGTYPESCREENREG");

        SWIPE.$("DOWN","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_SEARCHTASKS","//input[@id='workbenchForm:workbenchTabs:taskSearchContainer:taskSearchSearch']","bps080","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_TASKSEARCH","//a[@id='workbenchForm:workbenchTabs:taskSearchContainer:tb_buttonSearch']//span[@class='fa fa-search fa-lg']","TGTYPESCREENREG");

        WAIT.$(1,"TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_ACCOUNTPRODUCTS","//body[1]/div[1]/div[2]/div[1]/form[1]/div[1]/div[2]/div[1]/div[1]/div[1]/div[2]/div[2]/div[1]/div[1]/div[1]/div[3]/div[1]/div[1]/button[1]/span[2]","TGTYPESCREENREG");

        WAIT.$("ELE_TEXTBOX_GROUPNUMBER_ACCOUNTPRODUCTSPAGE","//input[@id='workbenchForm:workbenchTabs:grid:groupNumber_filter']","VISIBLE","TGTYPESCREENREG");

        WAIT.$(8,"TGTYPESCREENREG");

        TYPE.$("ELE_TEXTBOX_GROUPNUMBER_ACCOUNTPRODUCTSPAGE","//input[@id='workbenchForm:workbenchTabs:grid:groupNumber_filter']","107","FALSE","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_ACCOUNTNUMBER_ACCOUNTPRODUCTPAGE","//input[@id='workbenchForm:workbenchTabs:grid:accountNumberFind_filter']","1","FALSE","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_PRODUCT","//input[@id='workbenchForm:workbenchTabs:grid:productId_Col:filter']","AD&D","FALSE","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        TAP.$("ELE_LINK_ROW1","//tr[@class='ui-widget-content ui-datatable-even ui-datatable-selectable']","TGTYPESCREENREG");

        WAIT.$(3,"TGTYPESCREENREG");

        ASSERT.$("ELE_GETVAL_SITUATIONCODE","//span[@id='workbenchForm:workbenchTabs:situationCode']","VISIBLE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_EDIT","//span[text()='Edit']","TGTYPESCREENREG");

        WAIT.$("ELE_DRPDWN_SITUATIONCODE_ACCOUNTPRODUCTPAGE","//tr[@class='ui-widget-content']//tr[1]//td[1]//fieldset[1]//div[1]//table[1]//tbody[1]//tr[3]//td[2]//div[1]//div[3]//span[1]","VISIBLE","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        SWIPE.$("LEFT","TGTYPESCREENREG");

        SWIPE.$("LEFT","TGTYPESCREENREG");

        SWIPE.$("DOWN","TGTYPESCREENREG");

        SWIPE.$("DOWN","TGTYPESCREENREG");

        SWIPE.$("DOWN","TGTYPESCREENREG");

        TAP.$("ELE_DRPDWN_SITUATIONCODE_UPDATEGROUPPAGE","//div[@id='workbenchForm:workbenchTabs:situationCode']//div//span","TGTYPESCREENREG");

        TAP.$("ELE_TAB_CLOSE","//li[@class='ui-state-default ui-tabs-selected ui-state-active ui-corner-top']//span[@class='ui-icon ui-icon-close']","TGTYPESCREENREG");

        WAIT.$("ELE_TEXTBOX_GROUPNUMBER","//input[@id='workbenchForm:workbenchTabs:groupNumber']","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_ADD2","//button[@id='workbenchForm:workbenchTabs:tb_buttonAdd']//span[@class='ui-button-text ui-c'][contains(text(),'Add')]","TGTYPESCREENREG");

        WAIT.$("ELE_TEXTBOX_GROUPNUMBER","//input[@id='workbenchForm:workbenchTabs:groupNumber']","TGTYPESCREENREG");

        TYPE.$("ELE_TEXTBOX_GROUPNUMBER","//input[@id='workbenchForm:workbenchTabs:groupNumber']","25","FALSE","TGTYPESCREENREG");

        WAIT.$(1,"TGTYPESCREENREG");

        TAP.$("ELE_TXTBOX_EFFECTIVEDATE","//input[@id='workbenchForm:workbenchTabs:effectiveDate_input']","TGTYPESCREENREG");

        WAIT.$(8,"TGTYPESCREENREG");

        TAP.$("ELE_DRPDWN_ACCOUNT","//div[@id='workbenchForm:workbenchTabs:accountNumber']//div//span","TGTYPESCREENREG");

        TAP.$("ELE_DRPDWN_1BLUECROSSBUESHIELDOFJAMAICA","//li[contains(text(),'1 - Blue Cross Blue Shield Of Jamaica')]","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        WAIT.$("ELE_DRPDWN_PRODUCT","//fieldset[@class='ui-fieldset ui-widget ui-widget-content ui-corner-all ui-hidden-container dl_panel_fieldset dl_panel_first_panel']//tr[7]//td[2]//div[1]//div[3]//span[1]","VISIBLE","TGTYPESCREENREG");

        TAP.$("ELE_DRPDWN_PRODUCT","//fieldset[@class='ui-fieldset ui-widget ui-widget-content ui-corner-all ui-hidden-container dl_panel_fieldset dl_panel_first_panel']//tr[7]//td[2]//div[1]//div[3]//span[1]","TGTYPESCREENREG");

        WAIT.$(1,"TGTYPESCREENREG");

        TAP.$("ELE_DRPDWN_MEDMEDICAL","//li[@id='workbenchForm:workbenchTabs:productId_4']","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_EFFECTIVEDATE","//input[@id='workbenchForm:workbenchTabs:effectiveDate_input']","8/1/2020","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_DRPDWN_ACCOUNT","//div[@id='workbenchForm:workbenchTabs:accountNumber']//div//span","TGTYPESCREENREG");

        WAIT.$("ELE_LABEL_SITUATIONCODEDROPDOWNFIELD","//label[@id='workbenchForm:workbenchTabs:situationCode_label']","VISIBLE","TGTYPESCREENREG");

        TAP.$("ELE_DRPDWN_SITUATIONCODE_UPDATEGROUPPAGE","//div[@id='workbenchForm:workbenchTabs:situationCode']//div//span","TGTYPESCREENREG");

        TAP.$("ELE_LOGO_GIAS","//span[text()='GIAS']","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

        CALL.$("FTJModF095NewPageAgentSituationCodeBB1040","TGTYPESCREENREG");

        SWIPE.$("DOWN","TGTYPESCREENREG");

        SWIPE.$("DOWN","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_SEARCHTASKS","//input[@id='workbenchForm:workbenchTabs:taskSearchContainer:taskSearchSearch']","Agent situation code","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_TASKSEARCH","//a[@id='workbenchForm:workbenchTabs:taskSearchContainer:tb_buttonSearch']//span[@class='fa fa-search fa-lg']","TGTYPESCREENREG");

        WAIT.$(1,"TGTYPESCREENREG");

        TAP.$("ELE_LABEL_AGENTSITUATIONCODE","//span[contains(text(),'Agent Situation Code')]","TGTYPESCREENREG");

        WAIT.$("ELE_LABEL_DISTBUTIONCHANNEL_AGENTSC","//label[@id='workbenchForm:workbenchTabs:grid:distributionChannel_filter_label']","VISIBLE","TGTYPESCREENREG");

        ASSERT.$("ELE_LABEL_SITUATIONCODE_AGENTSC","//input[@id='workbenchForm:workbenchTabs:grid:agentSituationCode_Col:filter']","VISIBLE","TGTYPESCREENREG");

        ASSERT.$("ELE_LABEL_DESCRIPTION_AGENTSC","//input[@id='workbenchForm:workbenchTabs:grid:agentSituationDesc_Col:filter']","VISIBLE","TGTYPESCREENREG");

        ASSERT.$("ELE_LABEL_DISTBUTIONCHANNEL_AGENTSC","//label[@id='workbenchForm:workbenchTabs:grid:distributionChannel_filter_label']","VISIBLE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_ADD2","//button[@id='workbenchForm:workbenchTabs:tb_buttonAdd']//span[@class='ui-button-text ui-c'][contains(text(),'Add')]","TGTYPESCREENREG");

        CALL.$("F095AddPageNewFieldChecksBB1040","TGTYPESCREENREG");

        ASSERT.$("ELE_DRPDWN_DISTCHANNEL_AGENTSC","//div[@id='workbenchForm:workbenchTabs:distributionChannel']//div//span","VISIBLE","TGTYPESCREENREG");

        ASSERT.$("ELE_TEXTBOX_SITUATIONCODE","//input[@id='workbenchForm:workbenchTabs:agentSituationCode']","VISIBLE","TGTYPESCREENREG");

        ASSERT.$("ELE_TXTBOX_DESCRIPTION_AGENTSC","//input[@id='workbenchForm:workbenchTabs:agentSituationDesc']","VISIBLE","TGTYPESCREENREG");

        TAP.$("ELE_DRPDWN_DISTCHANNEL_AGENTSC","//div[@id='workbenchForm:workbenchTabs:distributionChannel']//div//span","TGTYPESCREENREG");

        ASSERT.$("ELE_DRPDWN_DISTCHANNEL_ALL","//li[@id='workbenchForm:workbenchTabs:distributionChannel_0']","VISIBLE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_VALIDATE","//span[contains(text(),'Validate')]","TGTYPESCREENREG");

        WAIT.$(3,"TGTYPESCREENREG");

        ASSERT.$("ELE_ERRORMSG_SCCANNOTBEBLANK","//span[@class='ui-message-error-detail'][contains(text(),'Situation code cannot be blank.')]","VISIBLE","TGTYPESCREENREG");

        ASSERT.$("ELE_ERRORMSG_SCDESCRIPTIONCANNOTBEBLANK","//span[@class='ui-message-error-detail'][contains(text(),'Situation code description cannot be blank.')]","VISIBLE","TGTYPESCREENREG");

        TYPE.$("ELE_TEXTBOX_SITUATIONCODE","//input[@id='workbenchForm:workbenchTabs:agentSituationCode']","001","FALSE","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_DESCRIPTION_AGENTSC","//input[@id='workbenchForm:workbenchTabs:agentSituationDesc']","AutoTest","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_VALIDATE","//span[contains(text(),'Validate')]","TGTYPESCREENREG");

        WAIT.$("ELE_MSG_VALIDATIONSUCCESS","//div[@id='workbenchForm:workbenchTabs:messages']//span[@class='ui-messages-info-summary'][contains(text(),'Validation completed successfully with no errors.')]","VISIBLE","TGTYPESCREENREG");

        WAIT.$(1,"TGTYPESCREENREG");

        TAP.$("ELE_TAB_CLOSE","//li[@class='ui-state-default ui-tabs-selected ui-state-active ui-corner-top']//span[@class='ui-icon ui-icon-close']","TGTYPESCREENREG");

        WAIT.$(4,"TGTYPESCREENREG");

        TAP.$("ELE_LINK_ROW1","//tr[@class='ui-widget-content ui-datatable-even ui-datatable-selectable']","TGTYPESCREENREG");

        WAIT.$("ELE_BUTTON_COPY","//span[contains(text(),'Copy')]","VISIBLE","TGTYPESCREENREG");

        WAIT.$(1,"TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_COPY","//span[contains(text(),'Copy')]","TGTYPESCREENREG");

        CALL.$("F095CopyFieldErrorChecksBB1040","TGTYPESCREENREG");

        ASSERT.$("ELE_DRPDWN_DISTCHANNEL_AGENTSC","//div[@id='workbenchForm:workbenchTabs:distributionChannel']//div//span","VISIBLE","TGTYPESCREENREG");

        ASSERT.$("ELE_TEXTBOX_SITUATIONCODE","//input[@id='workbenchForm:workbenchTabs:agentSituationCode']","VISIBLE","TGTYPESCREENREG");

        ASSERT.$("ELE_TXTBOX_DESCRIPTION_AGENTSC","//input[@id='workbenchForm:workbenchTabs:agentSituationDesc']","VISIBLE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_VALIDATE","//span[contains(text(),'Validate')]","TGTYPESCREENREG");

        WAIT.$(3,"TGTYPESCREENREG");

        ASSERT.$("ELE_MSG_ERRORMESG_BELOWFIELD","//span[@class='ui-message-error-detail']","VISIBLE","TGTYPESCREENREG");

        ASSERT.$("ELE_MSG_ERRORMESG_BELOWFIELD","//span[@class='ui-message-error-detail']","CONTAINS","Situation code must be unique.","TGTYPESCREENREG");

        ASSERT.$("ELE_ERRORMSG_DESCRIPTIONMUSTBEUNIQUE","//span[@class='ui-message-error-detail'][contains(text(),'Description must be unique.')]","VISIBLE","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_DESCRIPTION_AGENTSC","//input[@id='workbenchForm:workbenchTabs:agentSituationDesc']","Unique Desc","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_VALIDATE","//span[contains(text(),'Validate')]","TGTYPESCREENREG");

        WAIT.$("ELE_MSG_ERRORMESG_BELOWFIELD","//span[@class='ui-message-error-detail']","TGTYPESCREENREG");

        ASSERT.$("ELE_MSG_ERRORMESG_BELOWFIELD","//span[@class='ui-message-error-detail']","CONTAINS","Situation code must be unique.","TGTYPESCREENREG");

        TYPE.$("ELE_TEXTBOX_SITUATIONCODE","//input[@id='workbenchForm:workbenchTabs:agentSituationCode']","UniqueSC","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_VALIDATE","//span[contains(text(),'Validate')]","TGTYPESCREENREG");

        WAIT.$("ELE_MSG_VALIDATIONSUCCESS","//div[@id='workbenchForm:workbenchTabs:messages']//span[@class='ui-messages-info-summary'][contains(text(),'Validation completed successfully with no errors.')]","VISIBLE","TGTYPESCREENREG");

        TAP.$("ELE_TAB_CLOSE","//li[@class='ui-state-default ui-tabs-selected ui-state-active ui-corner-top']//span[@class='ui-icon ui-icon-close']","TGTYPESCREENREG");

        WAIT.$(4,"TGTYPESCREENREG");

        TAP.$("ELE_LOGO_GIAS","//span[text()='GIAS']","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

        CALL.$("F095DisplayAgentTransactionHistroySCdescriptionBB1050","TGTYPESCREENREG");

        SWIPE.$("DOWN","TGTYPESCREENREG");

        SWIPE.$("DOWN","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_SEARCHTASKS","//input[@id='workbenchForm:workbenchTabs:taskSearchContainer:taskSearchSearch']","Agents","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_TASKSEARCH","//a[@id='workbenchForm:workbenchTabs:taskSearchContainer:tb_buttonSearch']//span[@class='fa fa-search fa-lg']","TGTYPESCREENREG");

        WAIT.$(1,"TGTYPESCREENREG");

        TAP.$("ELE_LABEL_AGENTS","//span[contains(text(),'Agents')]","TGTYPESCREENREG");

        WAIT.$("ELE_TXTBOX_AGENTNUMBER","//input[@name='workbenchForm:workbenchTabs:grid1:agentNumber_Col:filter']","VISIBLE","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_AGENTNUMBER","//input[@name='workbenchForm:workbenchTabs:grid1:agentNumber_Col:filter']","ATEST007","FALSE","TGTYPESCREENREG");

        WAIT.$(3,"TGTYPESCREENREG");

        TAP.$("ELE_LINK_ROW1","//tr[@class='ui-widget-content ui-datatable-even ui-datatable-selectable']","TGTYPESCREENREG");

        WAIT.$("ELE_BUTTON_EDIT","//span[text()='Edit']","VISIBLE","TGTYPESCREENREG");

		String var_SituationCodeBefore;
                 LOGGER.info("Executed Step = VAR,String,var_SituationCodeBefore,TGTYPESCREENREG");
		var_SituationCodeBefore = ActionWrapper.getElementValue("ELE_GETVALUE_SITUATIONCODE_AGENTPAGE", "//span[@id='workbenchForm:workbenchTabs:agentSituationCode']");
                 LOGGER.info("Executed Step = STORE,var_SituationCodeBefore,ELE_GETVALUE_SITUATIONCODE_AGENTPAGE,TGTYPESCREENREG");
        LOGGER.info("Executed Step = START IF");
        if (check(var_SituationCodeBefore,"=","TEST01","TGTYPESCREENREG")) {
         LOGGER.info("Executed Step = IF,var_SituationCodeBefore,=,TEST01,TGTYPESCREENREG");
        CALL.$("AgentSituationCodeUpdateTo01BB1050","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_EDIT","//span[text()='Edit']","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        TAP.$("ELE_DRPDWN_SITUATIONCODE","//div[@id='workbenchForm:workbenchTabs:agentSituationCode']//div//span","TGTYPESCREENREG");

        WAIT.$(1,"TGTYPESCREENREG");

        TAP.$("ELE_DRPDWN_SITUATIONCODE_01","//li[@class='ui-selectonemenu-item ui-selectonemenu-list-item ui-corner-all'][contains(text(),'01')]","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SAVE","//span[contains(text(),'Save')]","TGTYPESCREENREG");

        WAIT.$(12,"TGTYPESCREENREG");

        LOGGER.info("Executed Step = START IF");
        if (check("ELE_CONFIRMACTION_YES","//div[@class='ui-dialog-content ui-widget-content ui-df-content']//iframe","VISIBLE","TGTYPESCREENREG")) {
         LOGGER.info("Executed Step = IF,ELE_CONFIRMACTION_YES,VISIBLE,TGTYPESCREENREG");
		try { 
		 driver.switchTo().frame(0);
		driver.findElement(By.xpath("//form[@id='confirmDialogForm']")).click();
		Thread.sleep(1000);
		driver.findElement(By.xpath("//button[@id='j_idt9']")).click();
		// Add Screenshot will only work for Android platform

		takeScreenshot();

		 
		 ;} 
		 catch(Exception e) { PrintVal("Renderer Script Exception : "+e.toString()); } 
		 PrintVal("Executed Step = CALLRENDERERSCRIPT,CLICKYESONCONFIRMACTION"); 
        WAIT.$(6,"TGTYPESCREENREG");

        }
          LOGGER.info("Executed Step = ENDIF,TGTYPESCREENREG");
        ASSERT.$("ELE_MSG_ITEMSUCCESSUPDATE","//div[@id='workbenchForm:workbenchTabs:messages']//span[@class='ui-messages-info-summary'][contains(text(),'Item successfully updated.')]","VISIBLE","TGTYPESCREENREG");

		String var_SituationCodeAfter;
                 LOGGER.info("Executed Step = VAR,String,var_SituationCodeAfter,TGTYPESCREENREG");
		var_SituationCodeAfter = ActionWrapper.getElementValue("ELE_GETVALUE_SITUATIONCODE_AGENTPAGE", "//span[@id='workbenchForm:workbenchTabs:agentSituationCode']");
                 LOGGER.info("Executed Step = STORE,var_SituationCodeAfter,ELE_GETVALUE_SITUATIONCODE_AGENTPAGE,TGTYPESCREENREG");
        TAP.$("ELE_BUTTON_SHOWLINK","//span[@class='ui-button-icon-left ui-icon ui-c fa fa-ellipsis-v fa-2x']","TGTYPESCREENREG");

        TAP.$("ELE_LINK_TRANSACTIONHISTORY","//span[text()='Transaction History']","TGTYPESCREENREG");

        WAIT.$(3,"TGTYPESCREENREG");

        TAP.$("ELE_LINK_ROW1","//tr[@class='ui-widget-content ui-datatable-even ui-datatable-selectable']","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        ASSERT.$("ELE_GETVAL_SITUATIONCODEBEFORE","//span[@id='workbenchForm:workbenchTabs:agentSituationCode']","=",var_SituationCodeBefore,"TGTYPESCREENREG");

        ASSERT.$("ELE_GETVAL_SITUATIONCODE_AFTER","//span[@id='workbenchForm:workbenchTabs:agentSituationCode_2']","=",var_SituationCodeAfter,"TGTYPESCREENREG");

        TAP.$("ELE_LOGO_GIAS","//span[text()='GIAS']","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

        }
          LOGGER.info("Executed Step = ENDIF,TGTYPESCREENREG");
        LOGGER.info("Executed Step = START IF");
        if (check(var_SituationCodeBefore,"=",01,"TGTYPESCREENREG")) {
         LOGGER.info("Executed Step = IF,var_SituationCodeBefore,=,01,TGTYPESCREENREG");
        CALL.$("AgentSituationCodeUpdateToTEST01BB1050","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_EDIT","//span[text()='Edit']","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        TAP.$("ELE_DRPDWN_SITUATIONCODE","//div[@id='workbenchForm:workbenchTabs:agentSituationCode']//div//span","TGTYPESCREENREG");

        TAP.$("ELE_DRPDWN_SITUATIONCODE_TEST01","//li[contains(text(),'TEST01')]","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SAVE","//span[contains(text(),'Save')]","TGTYPESCREENREG");

        WAIT.$(12,"TGTYPESCREENREG");

        LOGGER.info("Executed Step = START IF");
        if (check("ELE_CONFIRMACTION_YES","//div[@class='ui-dialog-content ui-widget-content ui-df-content']//iframe","VISIBLE","TGTYPESCREENREG")) {
         LOGGER.info("Executed Step = IF,ELE_CONFIRMACTION_YES,VISIBLE,TGTYPESCREENREG");
		try { 
		 driver.switchTo().frame(0);
		driver.findElement(By.xpath("//form[@id='confirmDialogForm']")).click();
		Thread.sleep(1000);
		driver.findElement(By.xpath("//button[@id='j_idt9']")).click();
		// Add Screenshot will only work for Android platform

		takeScreenshot();

		 
		 ;} 
		 catch(Exception e) { PrintVal("Renderer Script Exception : "+e.toString()); } 
		 PrintVal("Executed Step = CALLRENDERERSCRIPT,CLICKYESONCONFIRMACTION"); 
        WAIT.$(6,"TGTYPESCREENREG");

        }
          LOGGER.info("Executed Step = ENDIF,TGTYPESCREENREG");
        ASSERT.$("ELE_MSG_ITEMSUCCESSUPDATE","//div[@id='workbenchForm:workbenchTabs:messages']//span[@class='ui-messages-info-summary'][contains(text(),'Item successfully updated.')]","VISIBLE","TGTYPESCREENREG");

		String var_SituationCodeAfter;
                 LOGGER.info("Executed Step = VAR,String,var_SituationCodeAfter,TGTYPESCREENREG");
		var_SituationCodeAfter = ActionWrapper.getElementValue("ELE_GETVALUE_SITUATIONCODE_AGENTPAGE", "//span[@id='workbenchForm:workbenchTabs:agentSituationCode']");
                 LOGGER.info("Executed Step = STORE,var_SituationCodeAfter,ELE_GETVALUE_SITUATIONCODE_AGENTPAGE,TGTYPESCREENREG");
        TAP.$("ELE_BUTTON_SHOWLINK","//span[@class='ui-button-icon-left ui-icon ui-c fa fa-ellipsis-v fa-2x']","TGTYPESCREENREG");

        TAP.$("ELE_LINK_TRANSACTIONHISTORY","//span[text()='Transaction History']","TGTYPESCREENREG");

        WAIT.$(3,"TGTYPESCREENREG");

        TAP.$("ELE_LINK_ROW1","//tr[@class='ui-widget-content ui-datatable-even ui-datatable-selectable']","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        ASSERT.$("ELE_GETVAL_SITUATIONCODEBEFORE","//span[@id='workbenchForm:workbenchTabs:agentSituationCode']","=",var_SituationCodeBefore,"TGTYPESCREENREG");

        ASSERT.$("ELE_GETVAL_SITUATIONCODE_AFTER","//span[@id='workbenchForm:workbenchTabs:agentSituationCode_2']","=",var_SituationCodeAfter,"TGTYPESCREENREG");

        TAP.$("ELE_LOGO_GIAS","//span[text()='GIAS']","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

        }
          LOGGER.info("Executed Step = ENDIF,TGTYPESCREENREG");
		var_Count = var_Count + ( 1 );
                 LOGGER.info("Executed Step = INCREASEVAR,var_Count,1,TGTYPESCREENREG");
    	}
          LOGGER.info("Executed Step = WEND,TGTYPESCREENREG");
    	resetAttemps();

    }


    @Test
    public void ftj41modf228() {

        // No need to declare / print method name here, handled by TestNg listener
        // declare("ftj41modf228");

		int var_Count = 0;
                 LOGGER.info("Executed Step = VAR,Integer,var_Count,0,TGTYPESCREENREG");
		com.jayway.jsonpath.ReadContext var_CSVData = getJsonData("http://10.1.104.229/json/20201009/GaSO4s.json");
                 LOGGER.info("Executed Step = VAR,Json,var_CSVData,http://10.1.104.229/json/20201009/GaSO4s.json,TGTYPESCREENREG");
        LOGGER.info("Executed Step = REPEAT IF");
    	while (check(var_Count,"<",1,"TGTYPESCREENREG")) {
          LOGGER.info("Executed Step = WHILE,var_Count,<,1,TGTYPESCREENREG");
		String var_SourcePolicy;
                 LOGGER.info("Executed Step = VAR,String,var_SourcePolicy,TGTYPESCREENREG");
		var_SourcePolicy = getJsonData(var_CSVData , "$.records.["+var_Count+"].SourcePolicy");
                 LOGGER.info("Executed Step = STORE,var_SourcePolicy,var_CSVData,$.records.[{var_Count}].SourcePolicy,TGTYPESCREENREG");
		String var_TargetPolicy;
                 LOGGER.info("Executed Step = VAR,String,var_TargetPolicy,TGTYPESCREENREG");
		var_TargetPolicy = getJsonData(var_CSVData , "$.records.["+var_Count+"].TargetPolicy");
                 LOGGER.info("Executed Step = STORE,var_TargetPolicy,var_CSVData,$.records.[{var_Count}].TargetPolicy,TGTYPESCREENREG");
		String var_AgentNumber;
                 LOGGER.info("Executed Step = VAR,String,var_AgentNumber,TGTYPESCREENREG");
		var_AgentNumber = getJsonData(var_CSVData , "$.records.["+var_Count+"].AgentNumber");
                 LOGGER.info("Executed Step = STORE,var_AgentNumber,var_CSVData,$.records.[{var_Count}].AgentNumber,TGTYPESCREENREG");
        CALL.$("FTJQA2Login","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

		try { 
		 WebElement html = driver.findElement(By.tagName("html"));
		    	
		    	System.out.println("html size" + html.getSize());
		    	System.out.println("html" + html.getTagName());

		    	for( int i =0 ;i<4;i++) {
		        html.sendKeys(Keys.chord(Keys.CONTROL, Keys.SUBTRACT));
		    	}
		 
		 ;} 
		 catch(Exception e) { PrintVal("Renderer Script Exception : "+e.toString()); } 
		 PrintVal("Executed Step = CALLRENDERERSCRIPT,FTJ41ZOOMMAGNIFICATIONCHANGE"); 
        TYPE.$("ELE_TXTBOX_USERNAME","//input[@id='guestForm:username']","x5080","FALSE","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_PASSWORD","//input[@id='guestForm:password']","Welcome123$","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_LOGIN","//button[@id='guestForm:loginButton']","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        CALL.$("CopyPolicyBB1274","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_SEARCHTASKS","//input[@id='workbenchForm:workbenchTabs:taskSearchContainer:taskSearchSearch']","Copy Policy","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_TASKSEARCH","//a[@id='workbenchForm:workbenchTabs:taskSearchContainer:tb_buttonSearch']//span[@class='fa fa-search fa-lg']","TGTYPESCREENREG");

        WAIT.$(1,"TGTYPESCREENREG");

        TAP.$("ELE_LABEL_COPYPOLICY","//span[contains(text(),'Copy Policy')]","TGTYPESCREENREG");

        WAIT.$("ELE_BUTTON_CONTINUE","//span[contains(text(),'Continue')]","VISIBLE","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_POLICYNUMBER","//input[@id='workbenchForm:workbenchTabs:policyNumber']",var_SourcePolicy,"FALSE","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_TARGETPOLICYNUMBER","//input[@id='workbenchForm:workbenchTabs:targetPolicyNumber']",var_TargetPolicy,"FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_CONTINUE","//span[contains(text(),'Continue')]","TGTYPESCREENREG");

        WAIT.$("ELE_GETMSG_SUCCESS","//div[@id='workbenchForm:workbenchTabs:messages']//span[@class='ui-messages-info-summary']","VISIBLE","TGTYPESCREENREG");

        TAP.$("ELE_LOGO_GIAS","//span[text()='GIAS']","TGTYPESCREENREG");

        WAIT.$(3,"TGTYPESCREENREG");

        CALL.$("F228AllowStatusChangeDueToDeathBB1274","TGTYPESCREENREG");

        SWIPE.$("DOWN","TGTYPESCREENREG");

        SWIPE.$("DOWN","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_SEARCHTASKS","//input[@id='workbenchForm:workbenchTabs:taskSearchContainer:taskSearchSearch']","Policy Inquiry","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_TASKSEARCH","//a[@id='workbenchForm:workbenchTabs:taskSearchContainer:tb_buttonSearch']//span[@class='fa fa-search fa-lg']","TGTYPESCREENREG");

        WAIT.$(1,"TGTYPESCREENREG");

        TAP.$("ELE_LINK_POLICYINQUIRY","//span[contains(text(),'Policy Inquiry')]","TGTYPESCREENREG");

        WAIT.$("ELE_TXTBOX_POLICYNUMBER","//input[@id='workbenchForm:workbenchTabs:policyNumber']","VISIBLE","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_POLICYNUMBER","//input[@id='workbenchForm:workbenchTabs:policyNumber']",var_TargetPolicy,"FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_CONTINUE","//span[contains(text(),'Continue')]","TGTYPESCREENREG");

        WAIT.$("ELE_GETVAL_POLICYSTATUS","//span[@id='workbenchForm:workbenchTabs:statusText']","VISIBLE","TGTYPESCREENREG");

        ASSERT.$("ELE_GETVAL_POLICYSTATUS","//span[@id='workbenchForm:workbenchTabs:statusText']","CONTAINS","Premium Paying","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SHOWLINK","//span[@class='ui-button-icon-left ui-icon ui-c fa fa-ellipsis-v fa-2x']","TGTYPESCREENREG");

        WAIT.$(1,"TGTYPESCREENREG");

        TAP.$("ELE_LINK_BENEFIT","//span[contains(text(),'Benefits')]","TGTYPESCREENREG");

        WAIT.$(6,"TGTYPESCREENREG");

        ASSERT.$("ELE_LABEL_STATUSBENEFIT","//span[@id='workbenchForm:workbenchTabs:grid:0:benefitStatusText']","CONTAINS","Premium Paying","TGTYPESCREENREG");

        ASSERT.$("ELE_LABEL_BENEFITSTATUS_ROW2","//span[@id='workbenchForm:workbenchTabs:grid:1:benefitStatusText']","CONTAINS","Premium Paying","TGTYPESCREENREG");

        ASSERT.$("ELE_LABEL_BENEFITSTATUS_ROW3","//span[@id='workbenchForm:workbenchTabs:grid:2:benefitStatusText']","CONTAINS","Premium Paying","TGTYPESCREENREG");

        TAP.$("ELE_LOGO_GIAS","//span[text()='GIAS']","TGTYPESCREENREG");

        WAIT.$(4,"TGTYPESCREENREG");

        SWIPE.$("DOWN","TGTYPESCREENREG");

        SWIPE.$("DOWN","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_SEARCHTASKS","//input[@id='workbenchForm:workbenchTabs:taskSearchContainer:taskSearchSearch']","pos100","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_TASKSEARCH","//a[@id='workbenchForm:workbenchTabs:taskSearchContainer:tb_buttonSearch']//span[@class='fa fa-search fa-lg']","TGTYPESCREENREG");

        WAIT.$(1,"TGTYPESCREENREG");

        TAP.$("ELE_LABEL_CHANGEAPOLICYBENFITSTATUS","//body/div[@id='Outer40']/div[@id='ContentPanel']/div[@id='Content40']/form[@id='workbenchForm']/div[@id='PageTiles']/div[@id='workbenchForm:workbenchTabs']/div[1]/div[1]/div[1]/div[2]/div[2]/div[1]/div[1]/div[1]/div[3]/div[1]/div[1]/button[1]/span[2]","TGTYPESCREENREG");

        WAIT.$("ELE_TXTBOX_POLICYNUMBER","//input[@id='workbenchForm:workbenchTabs:policyNumber']","VISIBLE","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_POLICYNUMBER","//input[@id='workbenchForm:workbenchTabs:policyNumber']",var_TargetPolicy,"FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_CONTINUE","//span[contains(text(),'Continue')]","TGTYPESCREENREG");

        WAIT.$(8,"TGTYPESCREENREG");

        ASSERT.$("ELE_LABEL_STATUSBENEFIT","//span[@id='workbenchForm:workbenchTabs:grid:0:benefitStatusText']","CONTAINS","Premium Paying","TGTYPESCREENREG");

        ASSERT.$("ELE_LABEL_BENEFITSTATUS_ROW2","//span[@id='workbenchForm:workbenchTabs:grid:1:benefitStatusText']","CONTAINS","Premium Paying","TGTYPESCREENREG");

        ASSERT.$("ELE_LABEL_BENEFITSTATUS_ROW3","//span[@id='workbenchForm:workbenchTabs:grid:2:benefitStatusText']","CONTAINS","Premium Paying","TGTYPESCREENREG");

        TAP.$("ELE_LINK_ROW2","//tr[@class='ui-widget-content ui-datatable-odd ui-datatable-selectable']","TGTYPESCREENREG");

		String var_PTDtermDate;
                 LOGGER.info("Executed Step = VAR,String,var_PTDtermDate,TGTYPESCREENREG");
		var_PTDtermDate = ActionWrapper.getElementValue("ELE_GETVAL_PAIDTODATE", "//span[@id='workbenchForm:workbenchTabs:paidToDate']");
                 LOGGER.info("Executed Step = STORE,var_PTDtermDate,ELE_GETVAL_PAIDTODATE,TGTYPESCREENREG");
        CALL.$("BenefitStatusChangeandErrorValidationsBB1274","TGTYPESCREENREG");

        WAIT.$("ELE_BUTTON_SUBMIT","//span[contains(text(),'Submit')]","VISIBLE","TGTYPESCREENREG");

        TAP.$("ELE_DRPDWN_NEWBENEFITSTATUS_BENEFITSTATUSCHANGE","//div[@id='workbenchForm:workbenchTabs:newBenefitStatus']//div//span","TGTYPESCREENREG");

        TAP.$("ELE_DRPDWN_NEWBENEFITSTATUS_CANCELLEDDUETODEATH","//li[contains(text(),'Cancelled Due to Death')]","TGTYPESCREENREG");

        WAIT.$(3,"TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_VALIDATE","//span[contains(text(),'Validate')]","TGTYPESCREENREG");

        WAIT.$("ELE_ERRORMSG_TERMINATIONDATEISINVALID","//div[@id='workbenchForm:workbenchTabs:messages']//span[@class='ui-messages-error-summary'][contains(text(),'Termination date is invalid.')]","VISIBLE","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_TERMINATIONDATE","//input[@id='workbenchForm:workbenchTabs:terminationDate_input']","7/30/2020","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_VALIDATE","//span[contains(text(),'Validate')]","TGTYPESCREENREG");

        WAIT.$(3,"TGTYPESCREENREG");

        LOGGER.info("Executed Step = START IF");
        if (check("ELE_MSG_ERRORMESG_BELOWFIELD","//span[@class='ui-message-error-detail']","VISIBLE","TGTYPESCREENREG")) {
         LOGGER.info("Executed Step = IF,ELE_MSG_ERRORMESG_BELOWFIELD,VISIBLE,TGTYPESCREENREG");
        ASSERT.$("ELE_MSG_ERRORMESG_BELOWFIELD","//span[@class='ui-message-error-detail']","CONTAINS","Have minimum premium so termination date must be paid to date.","TGTYPESCREENREG");

        }
          LOGGER.info("Executed Step = ENDIF,TGTYPESCREENREG");
		var_PTDtermDate = ActionWrapper.getElementValue("ELE_GETVAL_PAIDTODATE", "//span[@id='workbenchForm:workbenchTabs:paidToDate']");
                 LOGGER.info("Executed Step = STORE,var_PTDtermDate,ELE_GETVAL_PAIDTODATE,TGTYPESCREENREG");
        TYPE.$("ELE_TXTBOX_TERMINATIONDATE","//input[@id='workbenchForm:workbenchTabs:terminationDate_input']",var_PTDtermDate,"FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SUBMIT","//span[contains(text(),'Submit')]","TGTYPESCREENREG");

        WAIT.$("ELE_MSG_REQUESTCOMPLETESUCCESSFULLY","//span[text()='Request completed successfully.']","VISIBLE","TGTYPESCREENREG");

        TAP.$("ELE_LOGO_GIAS","//span[text()='GIAS']","TGTYPESCREENREG");

        WAIT.$(4,"TGTYPESCREENREG");

        SWIPE.$("DOWN","TGTYPESCREENREG");

        SWIPE.$("DOWN","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_SEARCHTASKS","//input[@id='workbenchForm:workbenchTabs:taskSearchContainer:taskSearchSearch']","Policy Inquiry","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_TASKSEARCH","//a[@id='workbenchForm:workbenchTabs:taskSearchContainer:tb_buttonSearch']//span[@class='fa fa-search fa-lg']","TGTYPESCREENREG");

        WAIT.$(1,"TGTYPESCREENREG");

        TAP.$("ELE_LINK_POLICYINQUIRY","//span[contains(text(),'Policy Inquiry')]","TGTYPESCREENREG");

        WAIT.$("ELE_BUTTON_CONTINUE","//span[contains(text(),'Continue')]","VISIBLE","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_POLICYNUMBER","//input[@id='workbenchForm:workbenchTabs:policyNumber']",var_TargetPolicy,"FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_CONTINUE","//span[contains(text(),'Continue')]","TGTYPESCREENREG");

        WAIT.$("ELE_BUTTON_SHOWLINK","//span[@class='ui-button-icon-left ui-icon ui-c fa fa-ellipsis-v fa-2x']","VISIBLE","TGTYPESCREENREG");

        ASSERT.$("ELE_GETVAL_RIDER1","//span[contains(text(),'Rider 1')]","INVISIBLE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SHOWLINK","//span[@class='ui-button-icon-left ui-icon ui-c fa fa-ellipsis-v fa-2x']","TGTYPESCREENREG");

        WAIT.$(1,"TGTYPESCREENREG");

        TAP.$("ELE_LINK_BENEFIT","//span[contains(text(),'Benefits')]","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        ASSERT.$("ELE_LABEL_BENEFITSTATUS_ROW2","//span[@id='workbenchForm:workbenchTabs:grid:1:benefitStatusText']","CONTAINS","Cancelled Due to Death","TGTYPESCREENREG");

        TAP.$("ELE_TAB_MYDASHBOARD","//a[contains(text(),'My Dashboard')]","TGTYPESCREENREG");

        WAIT.$(3,"TGTYPESCREENREG");

        SWIPE.$("DOWN","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_SEARCHTASKS","//input[@id='workbenchForm:workbenchTabs:taskSearchContainer:taskSearchSearch']","pos100","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_TASKSEARCH","//a[@id='workbenchForm:workbenchTabs:taskSearchContainer:tb_buttonSearch']//span[@class='fa fa-search fa-lg']","TGTYPESCREENREG");

        WAIT.$(1,"TGTYPESCREENREG");

        TAP.$("ELE_LABEL_CHANGEAPOLICYBENFITSTATUS","//body/div[@id='Outer40']/div[@id='ContentPanel']/div[@id='Content40']/form[@id='workbenchForm']/div[@id='PageTiles']/div[@id='workbenchForm:workbenchTabs']/div[1]/div[1]/div[1]/div[2]/div[2]/div[1]/div[1]/div[1]/div[3]/div[1]/div[1]/button[1]/span[2]","TGTYPESCREENREG");

        WAIT.$("ELE_BUTTON_CONTINUE","//span[contains(text(),'Continue')]","VISIBLE","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_POLICYNUMBER","//input[@id='workbenchForm:workbenchTabs:policyNumber']",var_TargetPolicy,"FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_CONTINUE","//span[contains(text(),'Continue')]","TGTYPESCREENREG");

        WAIT.$(8,"TGTYPESCREENREG");

        ASSERT.$("ELE_LABEL_STATUSBENEFIT","//span[@id='workbenchForm:workbenchTabs:grid:0:benefitStatusText']","CONTAINS","Premium Paying","TGTYPESCREENREG");

        TAP.$("ELE_LINK_ROW1","//tr[@class='ui-widget-content ui-datatable-even ui-datatable-selectable']","TGTYPESCREENREG");

        CALL.$("BenefitStatusChangeandErrorValidationsBB1274","TGTYPESCREENREG");

        WAIT.$("ELE_BUTTON_SUBMIT","//span[contains(text(),'Submit')]","VISIBLE","TGTYPESCREENREG");

        TAP.$("ELE_DRPDWN_NEWBENEFITSTATUS_BENEFITSTATUSCHANGE","//div[@id='workbenchForm:workbenchTabs:newBenefitStatus']//div//span","TGTYPESCREENREG");

        TAP.$("ELE_DRPDWN_NEWBENEFITSTATUS_CANCELLEDDUETODEATH","//li[contains(text(),'Cancelled Due to Death')]","TGTYPESCREENREG");

        WAIT.$(3,"TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_VALIDATE","//span[contains(text(),'Validate')]","TGTYPESCREENREG");

        WAIT.$("ELE_ERRORMSG_TERMINATIONDATEISINVALID","//div[@id='workbenchForm:workbenchTabs:messages']//span[@class='ui-messages-error-summary'][contains(text(),'Termination date is invalid.')]","VISIBLE","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_TERMINATIONDATE","//input[@id='workbenchForm:workbenchTabs:terminationDate_input']","7/30/2020","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_VALIDATE","//span[contains(text(),'Validate')]","TGTYPESCREENREG");

        WAIT.$(3,"TGTYPESCREENREG");

        LOGGER.info("Executed Step = START IF");
        if (check("ELE_MSG_ERRORMESG_BELOWFIELD","//span[@class='ui-message-error-detail']","VISIBLE","TGTYPESCREENREG")) {
         LOGGER.info("Executed Step = IF,ELE_MSG_ERRORMESG_BELOWFIELD,VISIBLE,TGTYPESCREENREG");
        ASSERT.$("ELE_MSG_ERRORMESG_BELOWFIELD","//span[@class='ui-message-error-detail']","CONTAINS","Have minimum premium so termination date must be paid to date.","TGTYPESCREENREG");

        }
          LOGGER.info("Executed Step = ENDIF,TGTYPESCREENREG");
		var_PTDtermDate = ActionWrapper.getElementValue("ELE_GETVAL_PAIDTODATE", "//span[@id='workbenchForm:workbenchTabs:paidToDate']");
                 LOGGER.info("Executed Step = STORE,var_PTDtermDate,ELE_GETVAL_PAIDTODATE,TGTYPESCREENREG");
        TYPE.$("ELE_TXTBOX_TERMINATIONDATE","//input[@id='workbenchForm:workbenchTabs:terminationDate_input']",var_PTDtermDate,"FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SUBMIT","//span[contains(text(),'Submit')]","TGTYPESCREENREG");

        WAIT.$("ELE_MSG_REQUESTCOMPLETESUCCESSFULLY","//span[text()='Request completed successfully.']","VISIBLE","TGTYPESCREENREG");

        TAP.$("ELE_LOGO_GIAS","//span[text()='GIAS']","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

        SWIPE.$("DOWN","TGTYPESCREENREG");

        SWIPE.$("DOWN","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_SEARCHTASKS","//input[@id='workbenchForm:workbenchTabs:taskSearchContainer:taskSearchSearch']","pos100","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_TASKSEARCH","//a[@id='workbenchForm:workbenchTabs:taskSearchContainer:tb_buttonSearch']//span[@class='fa fa-search fa-lg']","TGTYPESCREENREG");

        WAIT.$(1,"TGTYPESCREENREG");

        TAP.$("ELE_LABEL_CHANGEAPOLICYBENFITSTATUS","//body/div[@id='Outer40']/div[@id='ContentPanel']/div[@id='Content40']/form[@id='workbenchForm']/div[@id='PageTiles']/div[@id='workbenchForm:workbenchTabs']/div[1]/div[1]/div[1]/div[2]/div[2]/div[1]/div[1]/div[1]/div[3]/div[1]/div[1]/button[1]/span[2]","TGTYPESCREENREG");

        WAIT.$("ELE_TXTBOX_POLICYNUMBER","//input[@id='workbenchForm:workbenchTabs:policyNumber']","VISIBLE","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_POLICYNUMBER","//input[@id='workbenchForm:workbenchTabs:policyNumber']",var_TargetPolicy,"FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_CONTINUE","//span[contains(text(),'Continue')]","TGTYPESCREENREG");

        WAIT.$(8,"TGTYPESCREENREG");

        ASSERT.$("ELE_LABEL_STATUSBENEFIT","//span[@id='workbenchForm:workbenchTabs:grid:0:benefitStatusText']","CONTAINS","Cancelled Due to Death","TGTYPESCREENREG");

        ASSERT.$("ELE_LABEL_BENEFITSTATUS_ROW2","//span[@id='workbenchForm:workbenchTabs:grid:1:benefitStatusText']","CONTAINS","Cancelled Due to Death","TGTYPESCREENREG");

        ASSERT.$("ELE_LABEL_BENEFITSTATUS_ROW3","//span[@id='workbenchForm:workbenchTabs:grid:2:benefitStatusText']","CONTAINS","Cancelled Due to Death","TGTYPESCREENREG");

        TAP.$("ELE_LOGO_GIAS","//span[text()='GIAS']","TGTYPESCREENREG");

        WAIT.$(4,"TGTYPESCREENREG");

        SWIPE.$("DOWN","TGTYPESCREENREG");

        SWIPE.$("DOWN","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_SEARCHTASKS","//input[@id='workbenchForm:workbenchTabs:taskSearchContainer:taskSearchSearch']","Policy Inquiry","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_TASKSEARCH","//a[@id='workbenchForm:workbenchTabs:taskSearchContainer:tb_buttonSearch']//span[@class='fa fa-search fa-lg']","TGTYPESCREENREG");

        WAIT.$(1,"TGTYPESCREENREG");

        TAP.$("ELE_LINK_POLICYINQUIRY","//span[contains(text(),'Policy Inquiry')]","TGTYPESCREENREG");

        WAIT.$("ELE_BUTTON_CONTINUE","//span[contains(text(),'Continue')]","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_POLICYNUMBER","//input[@id='workbenchForm:workbenchTabs:policyNumber']",var_TargetPolicy,"FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_CONTINUE","//span[contains(text(),'Continue')]","TGTYPESCREENREG");

        WAIT.$("ELE_BUTTON_SHOWLINKS","//span[@class='ui-button-icon-left ui-icon ui-c fa fa-ellipsis-v fa-2x']","VISIBLE","TGTYPESCREENREG");

        ASSERT.$("ELE_GETVAL_POLICYSTATUS","//span[@id='workbenchForm:workbenchTabs:statusText']","CONTAINS","Cancelled Due to Death","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SHOWLINK","//span[@class='ui-button-icon-left ui-icon ui-c fa fa-ellipsis-v fa-2x']","TGTYPESCREENREG");

        TAP.$("ELE_LINK_BENEFIT","//span[contains(text(),'Benefits')]","TGTYPESCREENREG");

        ASSERT.$("ELE_LABEL_STATUSBENEFIT","//span[@id='workbenchForm:workbenchTabs:grid:0:benefitStatusText']","CONTAINS","Cancelled Due to Death","TGTYPESCREENREG");

        ASSERT.$("ELE_LABEL_BENEFITSTATUS_ROW2","//span[@id='workbenchForm:workbenchTabs:grid:1:benefitStatusText']","CONTAINS","Cancelled Due to Death","TGTYPESCREENREG");

        ASSERT.$("ELE_LABEL_BENEFITSTATUS_ROW3","//span[@id='workbenchForm:workbenchTabs:grid:2:benefitStatusText']","CONTAINS","Cancelled Due to Death","TGTYPESCREENREG");

        TAP.$("ELE_LOGO_GIAS","//span[text()='GIAS']","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

        SWIPE.$("DOWN","TGTYPESCREENREG");

        CALL.$("CancelledDueToDeathValidationsCOM405BB1275","TGTYPESCREENREG");

        SWIPE.$("DOWN","TGTYPESCREENREG");

        SWIPE.$("DOWN","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_SEARCHTASKS","//input[@id='workbenchForm:workbenchTabs:taskSearchContainer:taskSearchSearch']","com405","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_TASKSEARCH","//a[@id='workbenchForm:workbenchTabs:taskSearchContainer:tb_buttonSearch']//span[@class='fa fa-search fa-lg']","TGTYPESCREENREG");

        WAIT.$(1,"TGTYPESCREENREG");

        TAP.$("ELE_LABEL_AGENTPOLICIESPOSTSETTLE","//span[contains(text(),'Agent Policies - Post Settle')]","TGTYPESCREENREG");

        WAIT.$("ELE_BUTTON_CONTINUE","//span[contains(text(),'Continue')]","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_AGENTNUM","//input[@id='workbenchForm:workbenchTabs:agentNumber']",var_AgentNumber,"FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_CONTINUE","//span[contains(text(),'Continue')]","TGTYPESCREENREG");

        WAIT.$("ELE_TXTBOX_FILTERPOLICYNUMBER1","//input[@id='workbenchForm:workbenchTabs:grid:policyNumber_Col:filter']","VISIBLE","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_FILTERPOLICYNUMBER1","//input[@id='workbenchForm:workbenchTabs:grid:policyNumber_Col:filter']",var_TargetPolicy,"FALSE","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

        ASSERT.$("ELE_GETVAL_FIRSTSTATUSSEARCHEDRESULT","//span[@id='workbenchForm:workbenchTabs:grid:0:status']","CONTAINS","Cancelled Due to Death","TGTYPESCREENREG");

        TAP.$("ELE_LOGO_GIAS","//span[text()='GIAS']","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

        CALL.$("CancelledDueToDeathValidationsPOS700BB1275","TGTYPESCREENREG");

        SWIPE.$("DOWN","TGTYPESCREENREG");

        SWIPE.$("DOWN","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_SEARCHTASKS","//input[@id='workbenchForm:workbenchTabs:taskSearchContainer:taskSearchSearch']","pos700","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_TASKSEARCH","//a[@id='workbenchForm:workbenchTabs:taskSearchContainer:tb_buttonSearch']//span[@class='fa fa-search fa-lg']","TGTYPESCREENREG");

        WAIT.$(1,"TGTYPESCREENREG");

        TAP.$("ELE_LABEL_CHANGEAPOLICYBENFITSTATUS","//body/div[@id='Outer40']/div[@id='ContentPanel']/div[@id='Content40']/form[@id='workbenchForm']/div[@id='PageTiles']/div[@id='workbenchForm:workbenchTabs']/div[1]/div[1]/div[1]/div[2]/div[2]/div[1]/div[1]/div[1]/div[3]/div[1]/div[1]/button[1]/span[2]","TGTYPESCREENREG");

        WAIT.$("ELE_BUTTON_CONTINUE","//span[contains(text(),'Continue')]","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_POLICYNUMBER","//input[@id='workbenchForm:workbenchTabs:policyNumber']",var_TargetPolicy,"FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_CONTINUE","//span[contains(text(),'Continue')]","TGTYPESCREENREG");

        WAIT.$("ELE_LINK_ROW1","//tr[@class='ui-widget-content ui-datatable-even ui-datatable-selectable']","VISIBLE","TGTYPESCREENREG");

        ASSERT.$("ELE_GETVAL_STATUS_CHANGEAPOLICYBENEFIT","//span[@id='workbenchForm:workbenchTabs:contractStatusAdminText']","CONTAINS","Cancelled Due to Death","TGTYPESCREENREG");

        ASSERT.$("ELE_LABEL_STATUSBENEFIT","//span[@id='workbenchForm:workbenchTabs:grid:0:benefitStatusText']","CONTAINS","Cancelled Due to Death","TGTYPESCREENREG");

        ASSERT.$("ELE_LABEL_BENEFITSTATUS_ROW2","//span[@id='workbenchForm:workbenchTabs:grid:1:benefitStatusText']","CONTAINS","Cancelled Due to Death","TGTYPESCREENREG");

        ASSERT.$("ELE_LABEL_BENEFITSTATUS_ROW3","//span[@id='workbenchForm:workbenchTabs:grid:2:benefitStatusText']","CONTAINS","Cancelled Due to Death","TGTYPESCREENREG");

        TAP.$("ELE_LOGO_GIAS","//span[text()='GIAS']","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

		var_Count = var_Count + ( 1 );
                 LOGGER.info("Executed Step = INCREASEVAR,var_Count,1,TGTYPESCREENREG");
    	}
          LOGGER.info("Executed Step = WEND,TGTYPESCREENREG");
    	resetAttemps();

    }


    @Test
    public void ftjf091() {

        // No need to declare / print method name here, handled by TestNg listener
        // declare("ftjf091");

		int var_Count = 0;
                 LOGGER.info("Executed Step = VAR,Integer,var_Count,0,TGTYPESCREENREG");
        LOGGER.info("Executed Step = REPEAT IF");
    	while (check(var_Count,"<",1,"TGTYPESCREENREG")) {
          LOGGER.info("Executed Step = WHILE,var_Count,<,1,TGTYPESCREENREG");
        CALL.$("FTJQA2Login","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

		try { 
		 WebElement html = driver.findElement(By.tagName("html"));
		    	
		    	System.out.println("html size" + html.getSize());
		    	System.out.println("html" + html.getTagName());

		    	for( int i =0 ;i<4;i++) {
		        html.sendKeys(Keys.chord(Keys.CONTROL, Keys.SUBTRACT));
		    	}
		 
		 ;} 
		 catch(Exception e) { PrintVal("Renderer Script Exception : "+e.toString()); } 
		 PrintVal("Executed Step = CALLRENDERERSCRIPT,FTJ41ZOOMMAGNIFICATIONCHANGE"); 
        TYPE.$("ELE_TXTBOX_USERNAME","//input[@id='guestForm:username']","x5080","FALSE","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_PASSWORD","//input[@id='guestForm:password']","Welcome123$","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_LOGIN","//button[@id='guestForm:loginButton']","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        CALL.$("F091CreateNewMaitenancePageValidationsBB1414","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_SEARCHTASKS","//input[@id='workbenchForm:workbenchTabs:taskSearchContainer:taskSearchSearch']","com850","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_TASKSEARCH","//a[@id='workbenchForm:workbenchTabs:taskSearchContainer:tb_buttonSearch']//span[@class='fa fa-search fa-lg']","TGTYPESCREENREG");

        WAIT.$(1,"TGTYPESCREENREG");

        TAP.$("ELE_LABEL_COMMISSINOSTATEMENTSERIES","//span[@class='ui-button-text ui-c'][contains(text(),'Commission Statement Series')]","TGTYPESCREENREG");

        WAIT.$("ELE_BUTTON_ADD2","//button[@id='workbenchForm:workbenchTabs:tb_buttonAdd']//span[@class='ui-button-text ui-c'][contains(text(),'Add')]","VISIBLE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_ADD2","//button[@id='workbenchForm:workbenchTabs:tb_buttonAdd']//span[@class='ui-button-text ui-c'][contains(text(),'Add')]","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

        ASSERT.$("ELE_TXTBOX_COMMISSIONSTATEMENTSERIES","//input[@id='workbenchForm:workbenchTabs:comStmtSeries']","VISIBLE","TGTYPESCREENREG");

        ASSERT.$("ELE_TXTBOX_DESCRIPTION_COMMISSIONSTATEMENTSSERIES","//input[@id='workbenchForm:workbenchTabs:comStmtSeriesDesc']","VISIBLE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_VALIDATE","//span[contains(text(),'Validate')]","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

        ASSERT.$("ELE_ERRORMSG_COMMISSIONSTATEMENTSERIESCANNOTBEBLANK","//div[@id='workbenchForm:workbenchTabs:messages']//span[@class='ui-messages-error-summary'][contains(text(),'Commission Statement Series cannot be blank.')]","VISIBLE","TGTYPESCREENREG");

        ASSERT.$("ELE_ERRORMSG_DESCRIPTIONCANNOTBEBLANK","//body/div[@id='Outer40']/div[@id='ContentPanel']/div[@id='Content40']/form[@id='workbenchForm']/div[@id='PageTiles']/div[@id='workbenchForm:workbenchTabs']/div[1]/div[3]/div[1]/div[4]/div[1]/ul[1]/li[2]/span[1]","VISIBLE","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_DESCRIPTION_COMMISSIONSTATEMENTSSERIES","//input[@id='workbenchForm:workbenchTabs:comStmtSeriesDesc']","Automated Test Desc Maint Desc","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_TXTBOX_COMMISSIONSTATEMENTSERIES","//input[@id='workbenchForm:workbenchTabs:comStmtSeries']","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_COMMISSIONSTATEMENTSERIES","//input[@id='workbenchForm:workbenchTabs:comStmtSeries']","AT1","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_VALIDATE","//span[contains(text(),'Validate')]","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

        ASSERT.$("ELE_ERRORMSG_ATTEMPTEDTOADDRESOURCEALREADYONFILE","//div[@id='workbenchForm:workbenchTabs:messages']//span[@class='ui-messages-error-summary'][contains(text(),'Attempted to add an item that already exists in fi')]","VISIBLE","TGTYPESCREENREG");

        TAP.$("ELE_TAB_CLOSE3","//li[@class='ui-state-default ui-tabs-selected ui-state-active ui-corner-top']//span[@class='ui-icon ui-icon-close']","TGTYPESCREENREG");

        WAIT.$(3,"TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_ADD2","//button[@id='workbenchForm:workbenchTabs:tb_buttonAdd']//span[@class='ui-button-text ui-c'][contains(text(),'Add')]","TGTYPESCREENREG");

        WAIT.$(1,"TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_DESCRIPTION_COMMISSIONSTATEMENTSSERIES","//input[@id='workbenchForm:workbenchTabs:comStmtSeriesDesc']","Auto Test Desc Field","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_TXTBOX_COMMISSIONSTATEMENTSERIES","//input[@id='workbenchForm:workbenchTabs:comStmtSeries']","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_COMMISSIONSTATEMENTSERIES","//input[@id='workbenchForm:workbenchTabs:comStmtSeries']","Tst","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SAVE","//span[contains(text(),'Save')]","TGTYPESCREENREG");

        WAIT.$("ELE_LABEL_ITEMSUCCESSFULLYADDED","//div[@id='workbenchForm:workbenchTabs:messages']//span[@class='ui-messages-info-summary'][contains(text(),'Item successfully added.')]","VISIBLE","TGTYPESCREENREG");

        TAP.$("ELE_TAB_CLOSE3","//li[@class='ui-state-default ui-tabs-selected ui-state-active ui-corner-top']//span[@class='ui-icon ui-icon-close']","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        TAP.$("ELE_TXTBOX_COMISSIONSTATEMENTSERIES","//input[@id='workbenchForm:workbenchTabs:grid:comStmtSeries_Col:filter']","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_COMISSIONSTATEMENTSERIES","//input[@id='workbenchForm:workbenchTabs:grid:comStmtSeries_Col:filter']","AT1","FALSE","TGTYPESCREENREG");

        WAIT.$(1,"TGTYPESCREENREG");

        TAP.$("ELE_LINK_ROW1","//tr[@class='ui-widget-content ui-datatable-even ui-datatable-selectable']","TGTYPESCREENREG");

        WAIT.$("ELE_BUTTON_EDIT","//span[text()='Edit']","VISIBLE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_COPY","//span[contains(text(),'Copy')]","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_VALIDATE","//span[contains(text(),'Validate')]","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

        ASSERT.$("ELE_ERRORMSG_DESCRIPTIONMUSTBEUNIQUE","//span[@class='ui-message-error-detail'][contains(text(),'Description must be unique.')]","VISIBLE","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_DESCRIPTION_COMMISSIONSTATEMENTSSERIES","//input[@id='workbenchForm:workbenchTabs:comStmtSeriesDesc']","Unique","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_VALIDATE","//span[contains(text(),'Validate')]","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

        ASSERT.$("ELE_ERRORMSG_ATTEMPTEDTOADDRESOURCEALREADYONFILE","//div[@id='workbenchForm:workbenchTabs:messages']//span[@class='ui-messages-error-summary'][contains(text(),'Attempted to add an item that already exists in fi')]","VISIBLE","TGTYPESCREENREG");

        WAIT.$(1,"TGTYPESCREENREG");

        TAP.$("ELE_TXTBOX_COMMISSIONSTATEMENTSERIES","//input[@id='workbenchForm:workbenchTabs:comStmtSeries']","TGTYPESCREENREG");

		try { 
		 driver.findElement(By.xpath("//input[@id='workbenchForm:workbenchTabs:comStmtSeries']")).clear();
		 
		 ;} 
		 catch(Exception e) { PrintVal("Renderer Script Exception : "+e.toString()); } 
		 PrintVal("Executed Step = CALLRENDERERSCRIPT,CLEARTXTBOXCOMMISSIONSTATEMENTSERIES"); 
		try { 
		 driver.findElement(By.xpath("//input[@id='workbenchForm:workbenchTabs:comStmtSeriesDesc']")).clear();
		 
		 ;} 
		 catch(Exception e) { PrintVal("Renderer Script Exception : "+e.toString()); } 
		 PrintVal("Executed Step = CALLRENDERERSCRIPT,CLEARTXTBOXDESCRIPTIONCOMMSSIONSSERIESPAGE"); 
        TAP.$("ELE_BUTTON_VALIDATE","//span[contains(text(),'Validate')]","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

        ASSERT.$("ELE_ERRORMSG_COMMISSIONSTATEMENTSERIESCANNOTBEBLANK","//div[@id='workbenchForm:workbenchTabs:messages']//span[@class='ui-messages-error-summary'][contains(text(),'Commission Statement Series cannot be blank.')]","VISIBLE","TGTYPESCREENREG");

        ASSERT.$("ELE_ERRORMSG_DESCRIPTIONCANNOTBEBLANK","//body/div[@id='Outer40']/div[@id='ContentPanel']/div[@id='Content40']/form[@id='workbenchForm']/div[@id='PageTiles']/div[@id='workbenchForm:workbenchTabs']/div[1]/div[3]/div[1]/div[4]/div[1]/ul[1]/li[2]/span[1]","VISIBLE","TGTYPESCREENREG");

        TAP.$("ELE_TAB_CLOSE3","//li[@class='ui-state-default ui-tabs-selected ui-state-active ui-corner-top']//span[@class='ui-icon ui-icon-close']","TGTYPESCREENREG");

        WAIT.$(4,"TGTYPESCREENREG");

        TAP.$("ELE_TXTBOX_COMISSIONSTATEMENTSERIES","//input[@id='workbenchForm:workbenchTabs:grid:comStmtSeries_Col:filter']","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_COMISSIONSTATEMENTSERIES","//input[@id='workbenchForm:workbenchTabs:grid:comStmtSeries_Col:filter']","Tst","FALSE","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

        TAP.$("ELE_LINK_ROW1","//tr[@class='ui-widget-content ui-datatable-even ui-datatable-selectable']","TGTYPESCREENREG");

        WAIT.$("ELE_BUTTON_EDIT","//span[text()='Edit']","VISIBLE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_DELETE","//span[contains(text(),'Delete')]","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

        TAP.$("ELE_CONFIRMDELETE_COM850PAGE","//body/div[@id='workbenchForm:workbenchTabs:tb_confirm_dialog']/div[3]/button[1]/span[2]","TGTYPESCREENREG");

        ASSERT.$("ELE_GETMSG_ITEMSUCCESSFULLYDELETED","//div[@id='workbenchForm:workbenchTabs:messages']//span[@class='ui-messages-info-summary'][contains(text(),'Item successfully deleted.')]","VISIBLE","TGTYPESCREENREG");

        TAP.$("ELE_LOGO_GIAS","//span[text()='GIAS']","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

		var_Count = var_Count + ( 1 );
                 LOGGER.info("Executed Step = INCREASEVAR,var_Count,1,TGTYPESCREENREG");
    	}
          LOGGER.info("Executed Step = WEND,TGTYPESCREENREG");
    	resetAttemps();

    }


    @Test
    public void ftj41modf235f246() {

        // No need to declare / print method name here, handled by TestNg listener
        // declare("ftj41modf235f246");

		int var_Count = 0;
                 LOGGER.info("Executed Step = VAR,Integer,var_Count,0,TGTYPESCREENREG");
		com.jayway.jsonpath.ReadContext var_CSVData = getJsonData("http://10.1.104.229/json/20200915/b9xEW6.json");
                 LOGGER.info("Executed Step = VAR,Json,var_CSVData,http://10.1.104.229/json/20200915/b9xEW6.json,TGTYPESCREENREG");
        LOGGER.info("Executed Step = REPEAT IF");
    	while (check(var_Count,"<",1,"TGTYPESCREENREG")) {
          LOGGER.info("Executed Step = WHILE,var_Count,<,1,TGTYPESCREENREG");
		String var_Series;
                 LOGGER.info("Executed Step = VAR,String,var_Series,TGTYPESCREENREG");
		var_Series = getJsonData(var_CSVData , "$.records.["+var_Count+"].Series");
                 LOGGER.info("Executed Step = STORE,var_Series,var_CSVData,$.records.[{var_Count}].Series,TGTYPESCREENREG");
		String var_PlanStagingNotAllowed;
                 LOGGER.info("Executed Step = VAR,String,var_PlanStagingNotAllowed,TGTYPESCREENREG");
		var_PlanStagingNotAllowed = getJsonData(var_CSVData , "$.records.["+var_Count+"].PlanStagingNotAllowed");
                 LOGGER.info("Executed Step = STORE,var_PlanStagingNotAllowed,var_CSVData,$.records.[{var_Count}].PlanStagingNotAllowed,TGTYPESCREENREG");
		String var_PlanAllCountriesStatesNotValid;
                 LOGGER.info("Executed Step = VAR,String,var_PlanAllCountriesStatesNotValid,TGTYPESCREENREG");
		var_PlanAllCountriesStatesNotValid = getJsonData(var_CSVData , "$.records.["+var_Count+"].PlanAllCountriesStatesNotValid");
                 LOGGER.info("Executed Step = STORE,var_PlanAllCountriesStatesNotValid,var_CSVData,$.records.[{var_Count}].PlanAllCountriesStatesNotValid,TGTYPESCREENREG");
		String var_PolicyNumberError;
                 LOGGER.info("Executed Step = VAR,String,var_PolicyNumberError,TGTYPESCREENREG");
		var_PolicyNumberError = getJsonData(var_CSVData , "$.records.["+var_Count+"].PolicyNumberError");
                 LOGGER.info("Executed Step = STORE,var_PolicyNumberError,var_CSVData,$.records.[{var_Count}].PolicyNumberError,TGTYPESCREENREG");
		String var_FromPolicyNumber;
                 LOGGER.info("Executed Step = VAR,String,var_FromPolicyNumber,TGTYPESCREENREG");
		var_FromPolicyNumber = getJsonData(var_CSVData , "$.records.["+var_Count+"].FromPolicyNumber");
                 LOGGER.info("Executed Step = STORE,var_FromPolicyNumber,var_CSVData,$.records.[{var_Count}].FromPolicyNumber,TGTYPESCREENREG");
		String var_ToPolicyNumber;
                 LOGGER.info("Executed Step = VAR,String,var_ToPolicyNumber,TGTYPESCREENREG");
		var_ToPolicyNumber = getJsonData(var_CSVData , "$.records.["+var_Count+"].ToPolicyNumber");
                 LOGGER.info("Executed Step = STORE,var_ToPolicyNumber,var_CSVData,$.records.[{var_Count}].ToPolicyNumber,TGTYPESCREENREG");
        CALL.$("FTJQA2Login","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

		try { 
		 WebElement html = driver.findElement(By.tagName("html"));
		    	
		    	System.out.println("html size" + html.getSize());
		    	System.out.println("html" + html.getTagName());

		    	for( int i =0 ;i<4;i++) {
		        html.sendKeys(Keys.chord(Keys.CONTROL, Keys.SUBTRACT));
		    	}
		 
		 ;} 
		 catch(Exception e) { PrintVal("Renderer Script Exception : "+e.toString()); } 
		 PrintVal("Executed Step = CALLRENDERERSCRIPT,FTJ41ZOOMMAGNIFICATIONCHANGE"); 
        TYPE.$("ELE_TXTBOX_USERNAME","//input[@id='guestForm:username']","x5080","FALSE","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_PASSWORD","//input[@id='guestForm:password']","Welcome123$","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_LOGIN","//button[@id='guestForm:loginButton']","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        CALL.$("F235RemovePOS740FromDashBoard","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_SEARCHTASKS","//input[@id='workbenchForm:workbenchTabs:taskSearchContainer:taskSearchSearch']","pos740","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_TASKSEARCH","//a[@id='workbenchForm:workbenchTabs:taskSearchContainer:tb_buttonSearch']//span[@class='fa fa-search fa-lg']","TGTYPESCREENREG");

        WAIT.$(1,"TGTYPESCREENREG");

        ASSERT.$("ELE_LABEL_STAGINGPLANSELECTION_POS740","//span[contains(text(),'Staging Plan Selection')]","INVISIBLE","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_SEARCHTASKS","//input[@id='workbenchForm:workbenchTabs:taskSearchContainer:taskSearchSearch']","Staging Plan Selection","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_TASKSEARCH","//a[@id='workbenchForm:workbenchTabs:taskSearchContainer:tb_buttonSearch']//span[@class='fa fa-search fa-lg']","TGTYPESCREENREG");

        ASSERT.$("ELE_LABEL_STAGINGPLANSELECTION_POS740","//span[contains(text(),'Staging Plan Selection')]","INVISIBLE","TGTYPESCREENREG");

        CALL.$("F235ChangesToMaintenancePagesAndErrorValidations","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_SEARCHTASKS","//input[@id='workbenchForm:workbenchTabs:taskSearchContainer:taskSearchSearch']","pdf179","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_TASKSEARCH","//a[@id='workbenchForm:workbenchTabs:taskSearchContainer:tb_buttonSearch']//span[@class='fa fa-search fa-lg']","TGTYPESCREENREG");

        WAIT.$(1,"TGTYPESCREENREG");

        TAP.$("ELE_LABEL_STAGINIGSERIESMAITENANCE","//span[contains(text(),'Staging Series Maintenance')]","TGTYPESCREENREG");

        WAIT.$("ELE_TXTBOX_SERIES_STAGINGSERIESMAINTENANCE","//input[@id='workbenchForm:workbenchTabs:grid:healthRateIncrSeri_Col:filter']","VISIBLE","TGTYPESCREENREG");

        ASSERT.$("ELE_GETVAL_COLUMN_POLICYNUMBER","//th[@id='workbenchForm:workbenchTabs:grid:policyNumber_Col']","VISIBLE","TGTYPESCREENREG");

        ASSERT.$("ELE_GETVAL_COLUMN_FROMPOLICYNUMBER","//th[@id='workbenchForm:workbenchTabs:grid:fromPolicyNumber_Col']","VISIBLE","TGTYPESCREENREG");

        ASSERT.$("ELE_GETVAL_COLUMN_TOPOLICYNUMBER","//th[@id='workbenchForm:workbenchTabs:grid:toPolicyNumber_Col']","VISIBLE","TGTYPESCREENREG");

        TAP.$("ELE_DRPDWN_STATEANDCOUNTRY","//div[@id='workbenchForm:workbenchTabs:grid:stateCode_filter']//span[@class='ui-icon ui-icon-triangle-1-s ui-c']","TGTYPESCREENREG");

        TAP.$("ELE_DRPDWN_ALLCOUNTRIESANDSTATES","//li[contains(text(),'All Countries and States')]","TGTYPESCREENREG");

        WAIT.$(1,"TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_ADD2","//button[@id='workbenchForm:workbenchTabs:tb_buttonAdd']//span[@class='ui-button-text ui-c'][contains(text(),'Add')]","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

        WAIT.$("ELE_TXTBOX_PLANCODE","//input[@id='workbenchForm:workbenchTabs:planCode']","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_SERIES","//input[@id='workbenchForm:workbenchTabs:healthRateIncrSeri']",var_Series,"FALSE","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_PLANCODE","//input[@id='workbenchForm:workbenchTabs:planCode']",var_PlanStagingNotAllowed,"FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_VALIDATE","//span[contains(text(),'Validate')]","TGTYPESCREENREG");

        WAIT.$(3,"TGTYPESCREENREG");

        ASSERT.$("ELE_ERRORMSG_STAGINGNOTALLOWEDFORPREMIUMCALCFORMULA","//div[@id='workbenchForm:workbenchTabs:messages']//span[@class='ui-messages-error-summary'][contains(text(),'Staging not allowed for premium calculation formul')]","VISIBLE","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_PLANCODE","//input[@id='workbenchForm:workbenchTabs:planCode']",var_PlanAllCountriesStatesNotValid,"FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_VALIDATE","//span[contains(text(),'Validate')]","TGTYPESCREENREG");

        WAIT.$(3,"TGTYPESCREENREG");

        ASSERT.$("ELE_ERRORMSG_ALLCOUNTRIESANDSTATESNOTVALIDFORHEALTHPRODUCTS","//div[@id='workbenchForm:workbenchTabs:messages']//span[@class='ui-messages-error-summary'][contains(text(),'All countries and states not valid for health prod')]","VISIBLE","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_POLICYNUMBER","//input[@id='workbenchForm:workbenchTabs:policyNumber']",var_PolicyNumberError,"FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_VALIDATE","//span[contains(text(),'Validate')]","TGTYPESCREENREG");

        WAIT.$(3,"TGTYPESCREENREG");

        ASSERT.$("ELE_ERRORMSG_ONLYONEVALUEMAYBEENTEREDINPLANPOLICYORFROMPOLICY","//div[@id='workbenchForm:workbenchTabs:messages']//span[@class='ui-messages-error-summary'][contains(text(),'Only one value may be entered in plan, policy numb')]","VISIBLE","TGTYPESCREENREG");

		try { 
		 driver.findElement(By.xpath("//input[@id='workbenchForm:workbenchTabs:planCode']")).clear();
		 
		 ;} 
		 catch(Exception e) { PrintVal("Renderer Script Exception : "+e.toString()); } 
		 PrintVal("Executed Step = CALLRENDERERSCRIPT,CLEARTXTBOXPLAN"); 
		try { 
		 driver.findElement(By.xpath("//input[@id='workbenchForm:workbenchTabs:policyNumber']")).clear();
		 
		 ;} 
		 catch(Exception e) { PrintVal("Renderer Script Exception : "+e.toString()); } 
		 PrintVal("Executed Step = CALLRENDERERSCRIPT,CLEARTXTBOXPOLICYNUMBER"); 
        TYPE.$("ELE_TXTBOX_FROMPOLICY","//input[@id='workbenchForm:workbenchTabs:fromPolicyNumber']",var_FromPolicyNumber,"FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_VALIDATE","//span[contains(text(),'Validate')]","TGTYPESCREENREG");

        WAIT.$(3,"TGTYPESCREENREG");

        ASSERT.$("ELE_ERRORMSG_FROMPOLICYONLYALLOWEDWITHNUMERICPOLICYNUMBERMASKING","//div[@id='workbenchForm:workbenchTabs:messages']//span[@class='ui-messages-error-summary'][contains(text(),'From policy number is only allowed with numeric po')]","VISIBLE","TGTYPESCREENREG");

        ASSERT.$("ELE_ERRORMSG_TOPOLICYNUMBERISREQUIREDWHENFROMPOLICYISENTERED","//div[@id='workbenchForm:workbenchTabs:messages']//span[@class='ui-messages-error-summary'][contains(text(),'To policy number is required when from policy numb')]","VISIBLE","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_TOPOLICYNUMBER","//input[@id='workbenchForm:workbenchTabs:toPolicyNumber']",var_ToPolicyNumber,"FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_VALIDATE","//span[contains(text(),'Validate')]","TGTYPESCREENREG");

        WAIT.$(3,"TGTYPESCREENREG");

        ASSERT.$("ELE_ERRORMSG_FROMPOLICYONLYALLOWEDWITHNUMERICPOLICYNUMBERMASKING","//div[@id='workbenchForm:workbenchTabs:messages']//span[@class='ui-messages-error-summary'][contains(text(),'From policy number is only allowed with numeric po')]","VISIBLE","TGTYPESCREENREG");

        ASSERT.$("ELE_ERRORMSG_TOPOLICYNUMBERONLYALLOWEDWITHNUMERICPOLICYNUMBERMASKING","//div[@id='workbenchForm:workbenchTabs:messages']//span[@class='ui-messages-error-summary'][contains(text(),'To policy number is only allowed with numeric poli')]","VISIBLE","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_PLANCODE","//input[@id='workbenchForm:workbenchTabs:planCode']",var_PlanStagingNotAllowed,"FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_VALIDATE","//span[contains(text(),'Validate')]","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

        ASSERT.$("ELE_ERRORMSG_ONLYONEVALUEMAYBEENTEREDINPLANPOLICYORFROMPOLICY","//div[@id='workbenchForm:workbenchTabs:messages']//span[@class='ui-messages-error-summary'][contains(text(),'Only one value may be entered in plan, policy numb')]","VISIBLE","TGTYPESCREENREG");

        TAP.$("ELE_TAB_CLOSE","//li[@class='ui-state-default ui-tabs-selected ui-state-active ui-corner-top']//span[@class='ui-icon ui-icon-close']","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        TAP.$("ELE_LINK_ROW1","//tr[@class='ui-widget-content ui-datatable-even ui-datatable-selectable']","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

        WAIT.$("ELE_BUTTON_SHOWLINK","//span[@class='ui-button-icon-left ui-icon ui-c fa fa-ellipsis-v fa-2x']","VISIBLE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SHOWLINK","//span[@class='ui-button-icon-left ui-icon ui-c fa fa-ellipsis-v fa-2x']","TGTYPESCREENREG");

        ASSERT.$("ELE_LINK_SUBMITSTAGINGJOB_POS740PAGE","//span[contains(text(),'Submit Staging Job')]","VISIBLE","TGTYPESCREENREG");

		var_Count = var_Count + ( 1 );
                 LOGGER.info("Executed Step = INCREASEVAR,var_Count,1,TGTYPESCREENREG");
    	}
          LOGGER.info("Executed Step = WEND,TGTYPESCREENREG");
    	resetAttemps();

    }


    @Test
    public void ftj41glc4mod13() {

        // No need to declare / print method name here, handled by TestNg listener
        // declare("ftj41glc4mod13");

		int var_Count = 0;
                 LOGGER.info("Executed Step = VAR,Integer,var_Count,0,TGTYPESCREENREG");
		com.jayway.jsonpath.ReadContext var_CSVData = getJsonData("http://10.1.104.229/json/20200918/uRaRiw.json");
                 LOGGER.info("Executed Step = VAR,Json,var_CSVData,http://10.1.104.229/json/20200918/uRaRiw.json,TGTYPESCREENREG");
        LOGGER.info("Executed Step = REPEAT IF");
    	while (check(var_Count,"<",1,"TGTYPESCREENREG")) {
          LOGGER.info("Executed Step = WHILE,var_Count,<,1,TGTYPESCREENREG");
		String var_GroupA;
                 LOGGER.info("Executed Step = VAR,String,var_GroupA,TGTYPESCREENREG");
		var_GroupA = getJsonData(var_CSVData , "$.records.["+var_Count+"].GroupA");
                 LOGGER.info("Executed Step = STORE,var_GroupA,var_CSVData,$.records.[{var_Count}].GroupA,TGTYPESCREENREG");
		String var_GroupN;
                 LOGGER.info("Executed Step = VAR,String,var_GroupN,TGTYPESCREENREG");
		var_GroupN = getJsonData(var_CSVData , "$.records.["+var_Count+"].GroupN");
                 LOGGER.info("Executed Step = STORE,var_GroupN,var_CSVData,$.records.[{var_Count}].GroupN,TGTYPESCREENREG");
        CALL.$("FTJQA2Login","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

		try { 
		 WebElement html = driver.findElement(By.tagName("html"));
		    	
		    	System.out.println("html size" + html.getSize());
		    	System.out.println("html" + html.getTagName());

		    	for( int i =0 ;i<4;i++) {
		        html.sendKeys(Keys.chord(Keys.CONTROL, Keys.SUBTRACT));
		    	}
		 
		 ;} 
		 catch(Exception e) { PrintVal("Renderer Script Exception : "+e.toString()); } 
		 PrintVal("Executed Step = CALLRENDERERSCRIPT,FTJ41ZOOMMAGNIFICATIONCHANGE"); 
        TYPE.$("ELE_TXTBOX_USERNAME","//input[@id='guestForm:username']","x5080","FALSE","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_PASSWORD","//input[@id='guestForm:password']","Welcome123$","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_LOGIN","//button[@id='guestForm:loginButton']","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        CALL.$("GLC4Mod13AutomaticallyAssignMemberIDDrpDwnValidations","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_SEARCHTASKS","//input[@id='workbenchForm:workbenchTabs:taskSearchContainer:taskSearchSearch']","bps010","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_TASKSEARCH","//a[@id='workbenchForm:workbenchTabs:taskSearchContainer:tb_buttonSearch']//span[@class='fa fa-search fa-lg']","TGTYPESCREENREG");

        WAIT.$(1,"TGTYPESCREENREG");

        TAP.$("ELE_LABEL_GROUPS","//span[contains(text(),'Groups')]","TGTYPESCREENREG");

        WAIT.$("ELE_BUTTON_ADD2","//button[@id='workbenchForm:workbenchTabs:tb_buttonAdd']//span[@class='ui-button-text ui-c'][contains(text(),'Add')]","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_GROUPNUMBER_GROUPS","//input[@id='workbenchForm:workbenchTabs:grid:groupNumber_filter']",var_GroupA,"FALSE","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        TAP.$("ELE_LINK_GROUPNUMBER_GROUPS","//body[1]/div[1]/div[2]/div[1]/form[1]/div[1]/div[2]/div[1]/div[2]/div[1]/div[4]/div[2]/table[1]/tbody[1]/tr[1]/td[1]","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

        WAIT.$("ELE_BUTTON_COPY","//span[contains(text(),'Copy')]","VISIBLE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_EDIT","//span[text()='Edit']","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

        WAIT.$("ELE_BUTTON_SAVE","//span[contains(text(),'Save')]","VISIBLE","TGTYPESCREENREG");

        ASSERT.$("ELE_LABEL_AUTOMATICALLYASSIGNMEMBERIDENTIFIER","//label[contains(text(),'Automatically Assign Member Identifier')]","VISIBLE","TGTYPESCREENREG");

        TAP.$("ELE_DRPDWN_AUTOMATICALLYASSIGNMEMBERIDENTIFIER","//div[@id='workbenchForm:workbenchTabs:calculateSSNIndicator']//div[@class='ui-selectonemenu-trigger ui-state-default ui-corner-right']","TGTYPESCREENREG");

        ASSERT.$("ELE_DRPDWN_NOMEMBERIDISNEVERAUTOMATICALLYASSIGNED","//li[contains(text(),'No, Member ID is never automatically assigned')]","VISIBLE","TGTYPESCREENREG");

        ASSERT.$("ELE_DRPDWN_YESANDREASSIGNVALUEIFRECEIVEDELECTRONICALLY","//li[contains(text(),'Yes, and reassign value if received electronically')]","VISIBLE","TGTYPESCREENREG");

        ASSERT.$("ELE_DRPDWN_YESANDREJECTIFVALUERECEIVEDELECTRONICALLY","//li[contains(text(),'Yes, and reject if value received electronically')]","VISIBLE","TGTYPESCREENREG");

        ASSERT.$("ELE_DRPDWN_YESBUTUSEVALUEIFRECEIVEDELECTRONICALLY","//li[contains(text(),'Yes, but use value if received electronically')]","VISIBLE","TGTYPESCREENREG");

        TAP.$("ELE_DRPDWN_NOMEMBERIDISNEVERAUTOMATICALLYASSIGNED","//li[contains(text(),'No, Member ID is never automatically assigned')]","TGTYPESCREENREG");

        TAP.$("ELE_LOGO_GIAS","//span[text()='GIAS']","TGTYPESCREENREG");

        WAIT.$(3,"TGTYPESCREENREG");

        CALL.$("GLC4Mod13ContinuedChecksGroupN","TGTYPESCREENREG");

        SWIPE.$("DOWN","TGTYPESCREENREG");

        SWIPE.$("DOWN","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_SEARCHTASKS","//input[@id='workbenchForm:workbenchTabs:taskSearchContainer:taskSearchSearch']","bps010","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_TASKSEARCH","//a[@id='workbenchForm:workbenchTabs:taskSearchContainer:tb_buttonSearch']//span[@class='fa fa-search fa-lg']","TGTYPESCREENREG");

        WAIT.$(1,"TGTYPESCREENREG");

        TAP.$("ELE_LABEL_GROUPS","//span[contains(text(),'Groups')]","TGTYPESCREENREG");

        WAIT.$("ELE_BUTTON_ADD2","//button[@id='workbenchForm:workbenchTabs:tb_buttonAdd']//span[@class='ui-button-text ui-c'][contains(text(),'Add')]","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_GROUPNUMBER_GROUPS","//input[@id='workbenchForm:workbenchTabs:grid:groupNumber_filter']",var_GroupN,"FALSE","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

        TAP.$("ELE_LINK_ROW1","//tr[@class='ui-widget-content ui-datatable-even ui-datatable-selectable']","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

        WAIT.$("ELE_BUTTON_COPY","//span[contains(text(),'Copy')]","VISIBLE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_EDIT","//span[text()='Edit']","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

        WAIT.$("ELE_BUTTON_SAVE","//span[contains(text(),'Save')]","VISIBLE","TGTYPESCREENREG");

        ASSERT.$("ELE_LABEL_AUTOMATICALLYASSIGNMEMBERIDENTIFIER","//label[contains(text(),'Automatically Assign Member Identifier')]","VISIBLE","TGTYPESCREENREG");

        TAP.$("ELE_DRPDWN_AUTOMATICALLYASSIGNMEMBERIDENTIFIER","//div[@id='workbenchForm:workbenchTabs:calculateSSNIndicator']//div[@class='ui-selectonemenu-trigger ui-state-default ui-corner-right']","TGTYPESCREENREG");

        ASSERT.$("ELE_DRPDWN_NOMEMBERIDISNEVERAUTOMATICALLYASSIGNED","//li[contains(text(),'No, Member ID is never automatically assigned')]","VISIBLE","TGTYPESCREENREG");

        ASSERT.$("ELE_DRPDWN_YESANDREASSIGNVALUEIFRECEIVEDELECTRONICALLY","//li[contains(text(),'Yes, and reassign value if received electronically')]","VISIBLE","TGTYPESCREENREG");

        ASSERT.$("ELE_DRPDWN_YESANDREJECTIFVALUERECEIVEDELECTRONICALLY","//li[contains(text(),'Yes, and reject if value received electronically')]","VISIBLE","TGTYPESCREENREG");

        ASSERT.$("ELE_DRPDWN_YESBUTUSEVALUEIFRECEIVEDELECTRONICALLY","//li[contains(text(),'Yes, but use value if received electronically')]","VISIBLE","TGTYPESCREENREG");

        TAP.$("ELE_DRPDWN_NOMEMBERIDISNEVERAUTOMATICALLYASSIGNED","//li[contains(text(),'No, Member ID is never automatically assigned')]","TGTYPESCREENREG");

        TAP.$("ELE_LOGO_GIAS","//span[text()='GIAS']","TGTYPESCREENREG");

        WAIT.$(3,"TGTYPESCREENREG");

        CALL.$("GLC4Mod13ValidateMemberIDAutoAssign","TGTYPESCREENREG");

        SWIPE.$("DOWN","TGTYPESCREENREG");

        SWIPE.$("DOWN","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_SEARCHTASKS","//input[@id='workbenchForm:workbenchTabs:taskSearchContainer:taskSearchSearch']","mbr010","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_TASKSEARCH","//a[@id='workbenchForm:workbenchTabs:taskSearchContainer:tb_buttonSearch']//span[@class='fa fa-search fa-lg']","TGTYPESCREENREG");

        WAIT.$(1,"TGTYPESCREENREG");

        TAP.$("ELE_LABEL_MEMBERSANDDEPENDENTS","//span[contains(text(),'Members and Dependents')]","TGTYPESCREENREG");

        WAIT.$("ELE_BUTTON_CONTINUE","//span[contains(text(),'Continue')]","VISIBLE","TGTYPESCREENREG");

        TYPE.$("ELE_TEXTBOX_GROUPNUMBER","//input[@id='workbenchForm:workbenchTabs:groupNumber']",var_GroupA,"FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_CONTINUE","//span[contains(text(),'Continue')]","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_ADD","//span[contains(text(),'Add')]","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

        WAIT.$("ELE_BUTTON_SAVE","//span[contains(text(),'Save')]","VISIBLE","TGTYPESCREENREG");

        ASSERT.$("ELE_GETVAL_MEMBERIDENTIFIER_ADDMEMBER","//span[@id='workbenchForm:workbenchTabs:participantIDNumber']","VISIBLE","TGTYPESCREENREG");

        ASSERT.$("ELE_LABEL_AUTOMATICALLYASSIGNMEMBERIDENTIFIER","//label[contains(text(),'Automatically Assign Member Identifier')]","INVISIBLE","TGTYPESCREENREG");

		var_Count = var_Count + ( 1 );
                 LOGGER.info("Executed Step = INCREASEVAR,var_Count,1,TGTYPESCREENREG");
    	}
          LOGGER.info("Executed Step = WEND,TGTYPESCREENREG");
    	resetAttemps();

    }


    @Test
    public void ftj41modf235f246sprint15() {

        // No need to declare / print method name here, handled by TestNg listener
        // declare("ftj41modf235f246sprint15");

		int var_Count = 0;
                 LOGGER.info("Executed Step = VAR,Integer,var_Count,0,TGTYPESCREENREG");
		com.jayway.jsonpath.ReadContext var_CSVData = getJsonData("http://10.1.104.229/json/20201001/yztk7d.json");
                 LOGGER.info("Executed Step = VAR,Json,var_CSVData,http://10.1.104.229/json/20201001/yztk7d.json,TGTYPESCREENREG");
        LOGGER.info("Executed Step = REPEAT IF");
    	while (check(var_Count,"<",1,"TGTYPESCREENREG")) {
          LOGGER.info("Executed Step = WHILE,var_Count,<,1,TGTYPESCREENREG");
		String var_PlanCodeError;
                 LOGGER.info("Executed Step = VAR,String,var_PlanCodeError,TGTYPESCREENREG");
		var_PlanCodeError = getJsonData(var_CSVData , "$.records.["+var_Count+"].PlanCodeError");
                 LOGGER.info("Executed Step = STORE,var_PlanCodeError,var_CSVData,$.records.[{var_Count}].PlanCodeError,TGTYPESCREENREG");
		String var_PlanCode;
                 LOGGER.info("Executed Step = VAR,String,var_PlanCode,TGTYPESCREENREG");
		var_PlanCode = getJsonData(var_CSVData , "$.records.["+var_Count+"].PlanCode");
                 LOGGER.info("Executed Step = STORE,var_PlanCode,var_CSVData,$.records.[{var_Count}].PlanCode,TGTYPESCREENREG");
		String var_Policy;
                 LOGGER.info("Executed Step = VAR,String,var_Policy,TGTYPESCREENREG");
		var_Policy = getJsonData(var_CSVData , "$.records.["+var_Count+"].Policy");
                 LOGGER.info("Executed Step = STORE,var_Policy,var_CSVData,$.records.[{var_Count}].Policy,TGTYPESCREENREG");
		String var_TransactionDate;
                 LOGGER.info("Executed Step = VAR,String,var_TransactionDate,TGTYPESCREENREG");
		var_TransactionDate = getJsonData(var_CSVData , "$.records.["+var_Count+"].TransactionDate");
                 LOGGER.info("Executed Step = STORE,var_TransactionDate,var_CSVData,$.records.[{var_Count}].TransactionDate,TGTYPESCREENREG");
        CALL.$("FTJQA2Login","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

		try { 
		 WebElement html = driver.findElement(By.tagName("html"));
		    	
		    	System.out.println("html size" + html.getSize());
		    	System.out.println("html" + html.getTagName());

		    	for( int i =0 ;i<4;i++) {
		        html.sendKeys(Keys.chord(Keys.CONTROL, Keys.SUBTRACT));
		    	}
		 
		 ;} 
		 catch(Exception e) { PrintVal("Renderer Script Exception : "+e.toString()); } 
		 PrintVal("Executed Step = CALLRENDERERSCRIPT,FTJ41ZOOMMAGNIFICATIONCHANGE"); 
        TYPE.$("ELE_TXTBOX_USERNAME","//input[@id='guestForm:username']","x5080","FALSE","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_PASSWORD","//input[@id='guestForm:password']","Welcome123$","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_LOGIN","//button[@id='guestForm:loginButton']","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        CALL.$("F235F246NewRecalculationOnDemandPageFieldValidations","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_SEARCHTASKS","//input[@id='workbenchForm:workbenchTabs:taskSearchContainer:taskSearchSearch']","Premium Recalculation On","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_TASKSEARCH","//a[@id='workbenchForm:workbenchTabs:taskSearchContainer:tb_buttonSearch']//span[@class='fa fa-search fa-lg']","TGTYPESCREENREG");

        WAIT.$(1,"TGTYPESCREENREG");

        TAP.$("ELE_LABEL_PREMIUMRECALCULATIONONDEMAND","//span[contains(text(),'Premium Recalculation On Demand')]","TGTYPESCREENREG");

        WAIT.$(3,"TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_VALIDATE","//span[contains(text(),'Validate')]","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

        ASSERT.$("ELE_ERRORMSG_EITHERPLANCODEORSELECTIDMUSTBEENTERED","//div[@id='workbenchForm:workbenchTabs:messages']//span[@class='ui-messages-error-summary'][contains(text(),'Either plan code or select ID must be entered.')]","VISIBLE","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_PLANCODE","//input[@id='workbenchForm:workbenchTabs:planCode']",var_PlanCodeError,"FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_VALIDATE","//span[contains(text(),'Validate')]","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

        ASSERT.$("ELE_ERRORMSG_PLANCODEISNOTVALID","//div[@id='workbenchForm:workbenchTabs:messages']//span[@class='ui-messages-error-summary'][contains(text(),'Plan code is not valid.')]","VISIBLE","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_RECALCULATEPREMIUMID","//input[@id='workbenchForm:workbenchTabs:selectId']","1","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_VALIDATE","//span[contains(text(),'Validate')]","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

        ASSERT.$("ELE_ERRORMSG_RECALCULATEPREMIUMIDMUSTBEBLANKWHENPLANCODEISENTERED","//div[@id='workbenchForm:workbenchTabs:messages']//span[@class='ui-messages-error-summary'][contains(text(),'Recalculate premium ID must be blank when plan cod')]","VISIBLE","TGTYPESCREENREG");

		try { 
		 driver.findElement(By.xpath("//input[@id='workbenchForm:workbenchTabs:selectId']")).clear();
		 
		 ;} 
		 catch(Exception e) { PrintVal("Renderer Script Exception : "+e.toString()); } 
		 PrintVal("Executed Step = CALLRENDERERSCRIPT,CLEARTXTBOXRECALCULATEPREMIUMID"); 
        TYPE.$("ELE_TXTBOX_PLANCODE","//input[@id='workbenchForm:workbenchTabs:planCode']",var_PlanCode,"FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_VALIDATE","//span[contains(text(),'Validate')]","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

        ASSERT.$("ELE_MSG_VALIDATIONSUCCESS","//div[@id='workbenchForm:workbenchTabs:messages']//span[@class='ui-messages-info-summary'][contains(text(),'Validation completed successfully with no errors.')]","VISIBLE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SHOWLINK","//span[@class='ui-button-icon-left ui-icon ui-c fa fa-ellipsis-v fa-2x']","TGTYPESCREENREG");

        ASSERT.$("ELE_LINK_SCHEDULER","//span[contains(text(),'Scheduler')]","VISIBLE","TGTYPESCREENREG");

        ASSERT.$("ELE_LINK_SUBMITTEDJOBLOGS","//span[contains(text(),'Submitted Job Logs')]","VISIBLE","TGTYPESCREENREG");

        TAP.$("ELE_LOGO_GIAS","//span[text()='GIAS']","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

        CALL.$("F235F246DisplayStagingRecordsInTranactionHistInquiryBB1688","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_SEARCHTASKS","//input[@id='workbenchForm:workbenchTabs:taskSearchContainer:taskSearchSearch']","pos200","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_TASKSEARCH","//a[@id='workbenchForm:workbenchTabs:taskSearchContainer:tb_buttonSearch']//span[@class='fa fa-search fa-lg']","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

        TAP.$("ELE_LINK_POLICYINQUIRY","//span[contains(text(),'Policy Inquiry')]","TGTYPESCREENREG");

        WAIT.$("ELE_TXTBOX_POLICYNUMBER","//input[@id='workbenchForm:workbenchTabs:policyNumber']","VISIBLE","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_POLICYNUMBER","//input[@id='workbenchForm:workbenchTabs:policyNumber']",var_Policy,"FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_CONTINUE","//span[contains(text(),'Continue')]","TGTYPESCREENREG");

        WAIT.$("ELE_BUTTON_SHOWLINK","//span[@class='ui-button-icon-left ui-icon ui-c fa fa-ellipsis-v fa-2x']","VISIBLE","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SHOWLINK","//span[@class='ui-button-icon-left ui-icon ui-c fa fa-ellipsis-v fa-2x']","TGTYPESCREENREG");

        TAP.$("ELE_LINK_TRANSACTIONHISTORY","//span[text()='Transaction History']","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_TRANSACTIONDATE","//input[@id='workbenchForm:workbenchTabs:grid:transactionDate_filter_input']",var_TransactionDate,"FALSE","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

        TAP.$("ELE_DRPDWN_BENEFIT_TRANSACTIONHISTPAGE","//thead/tr[1]/th[4]/div[1]/div[1]/div[3]/span[1]","TGTYPESCREENREG");

        TAP.$("ELE_DRPDWN_BENEFIT_BASE","//li[@id='workbenchForm:workbenchTabs:grid:baseRiderCode_filter_2']","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

        ASSERT.$("ELE_GETVAL_TRANSACTIONTYPE","//span[@id='workbenchForm:workbenchTabs:grid:0:typeText']","CONTAINS","Staging History","TGTYPESCREENREG");

        TAP.$("ELE_LOGO_GIAS","//span[text()='GIAS']","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

		var_Count = var_Count + ( 1 );
                 LOGGER.info("Executed Step = INCREASEVAR,var_Count,1,TGTYPESCREENREG");
    	}
          LOGGER.info("Executed Step = WEND,TGTYPESCREENREG");
    	resetAttemps();

    }


    @Test
    public void ftjmodf091sprint15() {

        // No need to declare / print method name here, handled by TestNg listener
        // declare("ftjmodf091sprint15");

		int var_Count = 0;
                 LOGGER.info("Executed Step = VAR,Integer,var_Count,0,TGTYPESCREENREG");
		com.jayway.jsonpath.ReadContext var_CSVData = getJsonData("http://10.1.104.229/json/20200923/g21LyV.json");
                 LOGGER.info("Executed Step = VAR,Json,var_CSVData,http://10.1.104.229/json/20200923/g21LyV.json,TGTYPESCREENREG");
        LOGGER.info("Executed Step = REPEAT IF");
    	while (check(var_Count,"<",1,"TGTYPESCREENREG")) {
          LOGGER.info("Executed Step = WHILE,var_Count,<,1,TGTYPESCREENREG");
		String var_Plan;
                 LOGGER.info("Executed Step = VAR,String,var_Plan,TGTYPESCREENREG");
		var_Plan = getJsonData(var_CSVData , "$.records.["+var_Count+"].Plan");
                 LOGGER.info("Executed Step = STORE,var_Plan,var_CSVData,$.records.[{var_Count}].Plan,TGTYPESCREENREG");
		String var_Agent;
                 LOGGER.info("Executed Step = VAR,String,var_Agent,TGTYPESCREENREG");
		var_Agent = getJsonData(var_CSVData , "$.records.["+var_Count+"].Agent");
                 LOGGER.info("Executed Step = STORE,var_Agent,var_CSVData,$.records.[{var_Count}].Agent,TGTYPESCREENREG");
		String var_StatementSeriesSubmittedValues;
                 LOGGER.info("Executed Step = VAR,String,var_StatementSeriesSubmittedValues,TGTYPESCREENREG");
		var_StatementSeriesSubmittedValues = getJsonData(var_CSVData , "$.records.["+var_Count+"].StatementSeriesSubmittedValues");
                 LOGGER.info("Executed Step = STORE,var_StatementSeriesSubmittedValues,var_CSVData,$.records.[{var_Count}].StatementSeriesSubmittedValues,TGTYPESCREENREG");
        CALL.$("FTJQA2Login","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

		try { 
		 WebElement html = driver.findElement(By.tagName("html"));
		    	
		    	System.out.println("html size" + html.getSize());
		    	System.out.println("html" + html.getTagName());

		    	for( int i =0 ;i<4;i++) {
		        html.sendKeys(Keys.chord(Keys.CONTROL, Keys.SUBTRACT));
		    	}
		 
		 ;} 
		 catch(Exception e) { PrintVal("Renderer Script Exception : "+e.toString()); } 
		 PrintVal("Executed Step = CALLRENDERERSCRIPT,FTJ41ZOOMMAGNIFICATIONCHANGE"); 
        TYPE.$("ELE_TXTBOX_USERNAME","//input[@id='guestForm:username']","x5080","FALSE","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_PASSWORD","//input[@id='guestForm:password']","Welcome123$","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_LOGIN","//button[@id='guestForm:loginButton']","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        CALL.$("F091AddCommissionStatementDrpDwnToPDF100BB1425","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_SEARCHTASKS","//input[@id='workbenchForm:workbenchTabs:taskSearchContainer:taskSearchSearch']","pdf100","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_TASKSEARCH","//a[@id='workbenchForm:workbenchTabs:taskSearchContainer:tb_buttonSearch']//span[@class='fa fa-search fa-lg']","TGTYPESCREENREG");

        WAIT.$(1,"TGTYPESCREENREG");

        TAP.$("ELE_LINK_PLANSETUP","//span[contains(text(),'Plan Setup')]","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_PLAN","//input[@id='workbenchForm:workbenchTabs:grid:planCode_Col:filter']",var_Plan,"FALSE","TGTYPESCREENREG");

        WAIT.$(3,"TGTYPESCREENREG");

        TAP.$("ELE_LINK_ROW1","//tr[@class='ui-widget-content ui-datatable-even ui-datatable-selectable']","TGTYPESCREENREG");

        WAIT.$("ELE_BUTTON_EDIT","//span[text()='Edit']","VISIBLE","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

        TAP.$("ELE_TAB_COMMISSIONS","//span[contains(text(),'Commissions')]","TGTYPESCREENREG");

        WAIT.$(3,"TGTYPESCREENREG");

        ASSERT.$("ELE_LABEL_COMMISSIONSTATEMENTSERIES","//label[@id='workbenchForm:workbenchTabs:comStmtSeries_lbl']","VISIBLE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_EDIT","//span[text()='Edit']","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

        TAP.$("ELE_DRPDWN_COMMISSIONSTATEMENTSERIES","//div[@id='workbenchForm:workbenchTabs:comStmtSeries']//div//span","TGTYPESCREENREG");

        ASSERT.$("ELE_DRPDWN_COMMISSIONSTATEMENTSERIES_BLANK","//li[@id='workbenchForm:workbenchTabs:comStmtSeries_0']","VISIBLE","TGTYPESCREENREG");

        TAP.$("ELE_DRPDWN_COMMISSIONSTATEMENTSERIES_BLANK","//li[@id='workbenchForm:workbenchTabs:comStmtSeries_0']","TGTYPESCREENREG");

        TAP.$("ELE_LOGO_GIAS","//span[text()='GIAS']","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

        CALL.$("F091Com504MakeStatementSeriesADropDownBB1426","TGTYPESCREENREG");

        WAIT.$(1,"TGTYPESCREENREG");

        SWIPE.$("DOWN","TGTYPESCREENREG");

        SWIPE.$("DOWN","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_SEARCHTASKS","//input[@id='workbenchForm:workbenchTabs:taskSearchContainer:taskSearchSearch']","com500","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_TASKSEARCH","//a[@id='workbenchForm:workbenchTabs:taskSearchContainer:tb_buttonSearch']//span[@class='fa fa-search fa-lg']","TGTYPESCREENREG");

        WAIT.$(1,"TGTYPESCREENREG");

        TAP.$("ELE_LABEL_AGENTS","//span[contains(text(),'Agents')]","TGTYPESCREENREG");

        WAIT.$("ELE_TXTBOX_AGENTNUMBER","//input[@name='workbenchForm:workbenchTabs:grid1:agentNumber_Col:filter']","VISIBLE","TGTYPESCREENREG");

        WAIT.$(1,"TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_AGENTNUMBER","//input[@name='workbenchForm:workbenchTabs:grid1:agentNumber_Col:filter']",var_Agent,"FALSE","TGTYPESCREENREG");

        WAIT.$(3,"TGTYPESCREENREG");

        TAP.$("ELE_LINK_ROW1","//tr[@class='ui-widget-content ui-datatable-even ui-datatable-selectable']","TGTYPESCREENREG");

        WAIT.$("ELE_BUTTON_COPY","//span[contains(text(),'Copy')]","VISIBLE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_EDIT","//span[text()='Edit']","TGTYPESCREENREG");

        ASSERT.$("ELE_DRPDWN_STATEMENTSERIES","//div[@id='workbenchForm:workbenchTabs:commissionStatSeri']//div//span","VISIBLE","TGTYPESCREENREG");

        TAP.$("ELE_DRPDWN_STATEMENTSERIES","//div[@id='workbenchForm:workbenchTabs:commissionStatSeri']//div//span","TGTYPESCREENREG");

        ASSERT.$("ELE_DRPDWN_STATEMENTSERIES_BLANK","//li[@id='workbenchForm:workbenchTabs:commissionStatSeri_0']","VISIBLE","TGTYPESCREENREG");

        ASSERT.$("ELE_DRPDWN_STATEMENTSERIES_USEPLANSTATEMENTSERIES","//li[@id='workbenchForm:workbenchTabs:commissionStatSeri_1']","VISIBLE","TGTYPESCREENREG");

        TAP.$("ELE_DRPDWN_STATEMENTSERIES_USEPLANSTATEMENTSERIES","//li[@id='workbenchForm:workbenchTabs:commissionStatSeri_1']","TGTYPESCREENREG");

        TAP.$("ELE_LOGO_GIAS","//span[text()='GIAS']","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

        CALL.$("F091AddCommStatementSeriesDrpDwnToCOM520BB1427","TGTYPESCREENREG");

        SWIPE.$("DOWN","TGTYPESCREENREG");

        SWIPE.$("DOWN","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_SEARCHTASKS","//input[@id='workbenchForm:workbenchTabs:taskSearchContainer:taskSearchSearch']","com520","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_TASKSEARCH","//a[@id='workbenchForm:workbenchTabs:taskSearchContainer:tb_buttonSearch']//span[@class='fa fa-search fa-lg']","TGTYPESCREENREG");

        WAIT.$(1,"TGTYPESCREENREG");

        TAP.$("ELE_LABEL_PENDINGDAILYAGENTADJUSTMENTS","//span[contains(text(),'Pending Daily Agent Adjustments')]","TGTYPESCREENREG");

        WAIT.$("ELE_BUTTON_CONTINUE","//span[contains(text(),'Continue')]","VISIBLE","TGTYPESCREENREG");

        WAIT.$(1,"TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_AGENTNUM","//input[@id='workbenchForm:workbenchTabs:agentNumber']",var_Agent,"FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_CONTINUE","//span[contains(text(),'Continue')]","TGTYPESCREENREG");

        WAIT.$(3,"TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_ADD","//span[contains(text(),'Add')]","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        ASSERT.$("ELE_LABEL_COMMISSIONSTATEMENTSERIES","//label[@id='workbenchForm:workbenchTabs:comStmtSeries_lbl']","VISIBLE","TGTYPESCREENREG");

        TAP.$("ELE_DRPDWN_COMMISSIONSTATEMENTSERIES","//div[@id='workbenchForm:workbenchTabs:comStmtSeries']//div//span","TGTYPESCREENREG");

        ASSERT.$("ELE_DRPDWN_COMMISSIONSTATEMENTSERIES_COM520_BLANK","//li[@id='workbenchForm:workbenchTabs:comStmtSeries_0']","VISIBLE","TGTYPESCREENREG");

        TAP.$("ELE_DRPDWN_COMMISSIONSTATEMENTSERIES_COM520_BLANK","//li[@id='workbenchForm:workbenchTabs:comStmtSeries_0']","TGTYPESCREENREG");

        TAP.$("ELE_LOGO_GIAS","//span[text()='GIAS']","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

        CALL.$("F091COM600ChangesToComStatementSeriesBB1429","TGTYPESCREENREG");

        SWIPE.$("DOWN","TGTYPESCREENREG");

        SWIPE.$("DOWN","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_SEARCHTASKS","//input[@id='workbenchForm:workbenchTabs:taskSearchContainer:taskSearchSearch']","opr600","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_TASKSEARCH","//a[@id='workbenchForm:workbenchTabs:taskSearchContainer:tb_buttonSearch']//span[@class='fa fa-search fa-lg']","TGTYPESCREENREG");

        WAIT.$(1,"TGTYPESCREENREG");

        TAP.$("ELE_LABEL_CREATECOMMISSIONSTATEMENTS","//span[contains(text(),'Create Commission Statements Offline')]","TGTYPESCREENREG");

        WAIT.$(4,"TGTYPESCREENREG");

        ASSERT.$("ELE_LABEL_STATEMENTSERIES","//label[@id='workbenchForm:workbenchTabs:statementSeries_lbl']","VISIBLE","TGTYPESCREENREG");

        TAP.$("ELE_CHECKBOX_STATEMENTSERIES_BLANK","//li[1]//div[1]//div[1]//span[1]","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_VALIDATE","//span[contains(text(),'Validate')]","TGTYPESCREENREG");

        WAIT.$(3,"TGTYPESCREENREG");

        ASSERT.$("ELE_ERRORMSG_STATEMENTSERIESSELECTIONISREQUIRED","//div[@id='workbenchForm:workbenchTabs:messages']//span[@class='ui-messages-error-summary'][contains(text(),'Statement Series selection is required.')]","VISIBLE","TGTYPESCREENREG");

        ASSERT.$("ELE_MSG_ERRORMESG_BELOWFIELD","//span[@class='ui-message-error-detail']","CONTAINS","Statement Series selection is required.","TGTYPESCREENREG");

        TAP.$("ELE_CHECKBOX_STATEMENTSERIES_BLANK","//li[1]//div[1]//div[1]//span[1]","TGTYPESCREENREG");

        TAP.$("ELE_CHECKBOX_STATEMENTSERIES_000","//html//body//div//div//div//form//div//div//div//div//div//table//tbody//tr//td//table//tbody//tr//td//fieldset//div//table//tbody//tr//td//div//div//ul//li[contains(text(),'000')]//div//div//span","TGTYPESCREENREG");

        TAP.$("ELE_CHECKBOX_STATEMENTSERIES_A01","//html//body//div//div//div//form//div//div//div//div//div//table//tbody//tr//td//table//tbody//tr//td//fieldset//div//table//tbody//tr//td//div//div//ul//li[contains(text(),'A01')]//div//div//span","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_VALIDATE","//span[contains(text(),'Validate')]","TGTYPESCREENREG");

        WAIT.$(3,"TGTYPESCREENREG");

        ASSERT.$("ELE_MSG_VALIDATIONSUCCESS","//div[@id='workbenchForm:workbenchTabs:messages']//span[@class='ui-messages-info-summary'][contains(text(),'Validation completed successfully with no errors.')]","VISIBLE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SUBMIT","//span[contains(text(),'Submit')]","TGTYPESCREENREG");

        WAIT.$(3,"TGTYPESCREENREG");

        ASSERT.$("ELE_MSG_SCHEDULEDJOBWASSUCCESSFULLYSUBMITTEDANDASSIGNEDJOBID","//div[@id='workbenchForm:workbenchTabs:messages']//span[@class='ui-messages-info-summary'][contains(text(),'Scheduled job was successfully submitted and assig')]","VISIBLE","TGTYPESCREENREG");

        TAP.$("ELE_LOGO_GIAS","//span[text()='GIAS']","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

        CALL.$("F091NewFIeldStatementSeiriesAddedToOPR610BB1430","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_SEARCHTASKS","//input[@id='workbenchForm:workbenchTabs:taskSearchContainer:taskSearchSearch']","opr610","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_TASKSEARCH","//a[@id='workbenchForm:workbenchTabs:taskSearchContainer:tb_buttonSearch']//span[@class='fa fa-search fa-lg']","TGTYPESCREENREG");

        WAIT.$(1,"TGTYPESCREENREG");

        TAP.$("ELE_LABEL_CREATETRIALCOMMISSIONSTATEMENTSOFFLINE","//span[contains(text(),'Create Trial Commission Statements Offline')]","TGTYPESCREENREG");

        WAIT.$(4,"TGTYPESCREENREG");

        ASSERT.$("ELE_LABEL_STATEMENTSERIES","//label[@id='workbenchForm:workbenchTabs:statementSeries_lbl']","VISIBLE","TGTYPESCREENREG");

        TAP.$("ELE_CHECKBOX_STATEMENTSERIES_BLANK","//li[1]//div[1]//div[1]//span[1]","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_VALIDATE","//span[contains(text(),'Validate')]","TGTYPESCREENREG");

        WAIT.$(3,"TGTYPESCREENREG");

        ASSERT.$("ELE_ERRORMSG_STATEMENTSERIESSELECTIONISREQUIRED","//div[@id='workbenchForm:workbenchTabs:messages']//span[@class='ui-messages-error-summary'][contains(text(),'Statement Series selection is required.')]","VISIBLE","TGTYPESCREENREG");

        ASSERT.$("ELE_MSG_ERRORMESG_BELOWFIELD","//span[@class='ui-message-error-detail']","CONTAINS","Statement Series selection is required.","TGTYPESCREENREG");

        TAP.$("ELE_CHECKBOX_STATEMENTSERIES_BLANK","//li[1]//div[1]//div[1]//span[1]","TGTYPESCREENREG");

        TAP.$("ELE_CHECKBOX_STATEMENTSERIES_000","//html//body//div//div//div//form//div//div//div//div//div//table//tbody//tr//td//table//tbody//tr//td//fieldset//div//table//tbody//tr//td//div//div//ul//li[contains(text(),'000')]//div//div//span","TGTYPESCREENREG");

        TAP.$("ELE_CHECKBOX_STATEMENTSERIES_A01","//html//body//div//div//div//form//div//div//div//div//div//table//tbody//tr//td//table//tbody//tr//td//fieldset//div//table//tbody//tr//td//div//div//ul//li[contains(text(),'A01')]//div//div//span","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_VALIDATE","//span[contains(text(),'Validate')]","TGTYPESCREENREG");

        WAIT.$(3,"TGTYPESCREENREG");

        ASSERT.$("ELE_MSG_VALIDATIONSUCCESS","//div[@id='workbenchForm:workbenchTabs:messages']//span[@class='ui-messages-info-summary'][contains(text(),'Validation completed successfully with no errors.')]","VISIBLE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SUBMIT","//span[contains(text(),'Submit')]","TGTYPESCREENREG");

        WAIT.$(3,"TGTYPESCREENREG");

        ASSERT.$("ELE_MSG_SCHEDULEDJOBWASSUCCESSFULLYSUBMITTEDANDASSIGNEDJOBID","//div[@id='workbenchForm:workbenchTabs:messages']//span[@class='ui-messages-info-summary'][contains(text(),'Scheduled job was successfully submitted and assig')]","VISIBLE","TGTYPESCREENREG");

        TAP.$("ELE_LOGO_GIAS","//span[text()='GIAS']","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

        CALL.$("F091AddComStatementSeriesFieldsToOPR300BB1431","TGTYPESCREENREG");

        SWIPE.$("DOWN","TGTYPESCREENREG");

        SWIPE.$("DOWN","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_SEARCHTASKS","//input[@id='workbenchForm:workbenchTabs:taskSearchContainer:taskSearchSearch']","opr300","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_TASKSEARCH","//a[@id='workbenchForm:workbenchTabs:taskSearchContainer:tb_buttonSearch']//span[@class='fa fa-search fa-lg']","TGTYPESCREENREG");

        WAIT.$(1,"TGTYPESCREENREG");

        TAP.$("ELE_LABEL_MONTHLYOFFLINEPROCESSING","//span[contains(text(),'Monthly Offline Processing')]","TGTYPESCREENREG");

        WAIT.$(4,"TGTYPESCREENREG");

        ASSERT.$("ELE_LABEL_STATEMENTSERIES","//label[@id='workbenchForm:workbenchTabs:statementSeries_lbl']","VISIBLE","TGTYPESCREENREG");

        TAP.$("ELE_CHECKBOX_STATEMENTSERIES_BLANK","//li[1]//div[1]//div[1]//span[1]","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_VALIDATE","//span[contains(text(),'Validate')]","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        ASSERT.$("ELE_ERRORMSG_VALUEISREQUIRED","//div[@id='workbenchForm:workbenchTabs:messages']//span[@class='ui-messages-error-summary'][contains(text(),'Value is required.')]","VISIBLE","TGTYPESCREENREG");

        ASSERT.$("ELE_MSG_ERRORMESG_BELOWFIELD","//span[@class='ui-message-error-detail']","CONTAINS","Value is required.","TGTYPESCREENREG");

        TAP.$("ELE_CHECKBOX_STATEMENTSERIES_BLANK","//li[1]//div[1]//div[1]//span[1]","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_VALIDATE","//span[contains(text(),'Validate')]","TGTYPESCREENREG");

        WAIT.$(3,"TGTYPESCREENREG");

        ASSERT.$("ELE_ERRORMSG_VALUEISREQUIRED","//div[@id='workbenchForm:workbenchTabs:messages']//span[@class='ui-messages-error-summary'][contains(text(),'Value is required.')]","VISIBLE","TGTYPESCREENREG");

        TAP.$("ELE_CHECKBOX_STATEMENTSERIES_000","//html//body//div//div//div//form//div//div//div//div//div//table//tbody//tr//td//table//tbody//tr//td//fieldset//div//table//tbody//tr//td//div//div//ul//li[contains(text(),'000')]//div//div//span","TGTYPESCREENREG");

        TAP.$("ELE_CHECKBOX_STATEMENTSERIES_A01","//html//body//div//div//div//form//div//div//div//div//div//table//tbody//tr//td//table//tbody//tr//td//fieldset//div//table//tbody//tr//td//div//div//ul//li[contains(text(),'A01')]//div//div//span","TGTYPESCREENREG");

        TAP.$("ELE_RADIOBUTTON_CREATEELECTRONICFUNDSTRANSFERDIRECTDEPOSITFILE_NO","//fieldset[@class='ui-fieldset ui-widget ui-widget-content ui-corner-all ui-hidden-container dl_panel_fieldset dl_panel_first_panel']//tr[2]//td[1]//div[1]//div[2]//span[1]","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_VALIDATE","//span[contains(text(),'Validate')]","TGTYPESCREENREG");

        WAIT.$(3,"TGTYPESCREENREG");

        ASSERT.$("ELE_MSG_VALIDATIONSUCCESS","//div[@id='workbenchForm:workbenchTabs:messages']//span[@class='ui-messages-info-summary'][contains(text(),'Validation completed successfully with no errors.')]","VISIBLE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SUBMIT","//span[contains(text(),'Submit')]","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        ASSERT.$("ELE_MSG_SCHEDULEDJOBWASSUCCESSFULLYSUBMITTEDANDASSIGNEDJOBID","//div[@id='workbenchForm:workbenchTabs:messages']//span[@class='ui-messages-info-summary'][contains(text(),'Scheduled job was successfully submitted and assig')]","VISIBLE","TGTYPESCREENREG");

        ASSERT.$("ELE_GETVAL_STATEMENTSERIESVALUES","//span[@id='workbenchForm:workbenchTabs:statementSeriesDisplay']","CONTAINS",var_StatementSeriesSubmittedValues,"TGTYPESCREENREG");

        TAP.$("ELE_LOGO_GIAS","//span[text()='GIAS']","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

		var_Count = var_Count + ( 1 );
                 LOGGER.info("Executed Step = INCREASEVAR,var_Count,1,TGTYPESCREENREG");
    	}
          LOGGER.info("Executed Step = WEND,TGTYPESCREENREG");
    	resetAttemps();

    }


    @Test
    public void ftj41tbd2sprint16() {

        // No need to declare / print method name here, handled by TestNg listener
        // declare("ftj41tbd2sprint16");

		int var_Count = 0;
                 LOGGER.info("Executed Step = VAR,Integer,var_Count,0,TGTYPESCREENREG");
		com.jayway.jsonpath.ReadContext var_CSVData = getJsonData("http://10.1.104.229/json/20201006/5SRUPz.json");
                 LOGGER.info("Executed Step = VAR,Json,var_CSVData,http://10.1.104.229/json/20201006/5SRUPz.json,TGTYPESCREENREG");
        LOGGER.info("Executed Step = REPEAT IF");
    	while (check(var_Count,"<",1,"TGTYPESCREENREG")) {
          LOGGER.info("Executed Step = WHILE,var_Count,<,1,TGTYPESCREENREG");
		String var_AccountControlNumber;
                 LOGGER.info("Executed Step = VAR,String,var_AccountControlNumber,TGTYPESCREENREG");
		var_AccountControlNumber = getJsonData(var_CSVData , "$.records.["+var_Count+"].AccountControlNumber");
                 LOGGER.info("Executed Step = STORE,var_AccountControlNumber,var_CSVData,$.records.[{var_Count}].AccountControlNumber,TGTYPESCREENREG");
        CALL.$("FTJQA2Login","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

		try { 
		 WebElement html = driver.findElement(By.tagName("html"));
		    	
		    	System.out.println("html size" + html.getSize());
		    	System.out.println("html" + html.getTagName());

		    	for( int i =0 ;i<4;i++) {
		        html.sendKeys(Keys.chord(Keys.CONTROL, Keys.SUBTRACT));
		    	}
		 
		 ;} 
		 catch(Exception e) { PrintVal("Renderer Script Exception : "+e.toString()); } 
		 PrintVal("Executed Step = CALLRENDERERSCRIPT,FTJ41ZOOMMAGNIFICATIONCHANGE"); 
        TYPE.$("ELE_TXTBOX_USERNAME","//input[@id='guestForm:username']","x5080","FALSE","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_PASSWORD","//input[@id='guestForm:password']","Welcome123$","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_LOGIN","//button[@id='guestForm:loginButton']","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        CALL.$("TBD2Sprint16Add3FieldsToAutomaticPaymentPropertiesBB1552","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_SEARCHTASKS","//input[@id='workbenchForm:workbenchTabs:taskSearchContainer:taskSearchSearch']","app000","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_TASKSEARCH","//a[@id='workbenchForm:workbenchTabs:taskSearchContainer:tb_buttonSearch']//span[@class='fa fa-search fa-lg']","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        TAP.$("ELE_LABEL_AUTOMATICPAYMENTPROPERTIES","//span[contains(text(),'Automatic Payment Properties')]","TGTYPESCREENREG");

        WAIT.$(3,"TGTYPESCREENREG");

        ASSERT.$("ELE_GETVAL_ELECTRONICPAYMENTVENDORCODE","//label[@id='workbenchForm:workbenchTabs:electronicPaymentVendorCode_lbl']","VISIBLE","TGTYPESCREENREG");

        ASSERT.$("ELE_GETVAL_VENDORDESCRIPTION","//label[@id='workbenchForm:workbenchTabs:vendorDescription_lbl']","VISIBLE","TGTYPESCREENREG");

        ASSERT.$("ELE_GETVAL_MERCHANTID","//label[@id='workbenchForm:workbenchTabs:merchantID_lbl']","VISIBLE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_EDIT","//span[text()='Edit']","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_ELECTRONICPAYMENTVENDORCODE","//input[@id='workbenchForm:workbenchTabs:electronicPaymentVendorCode']","Test","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_VALIDATE","//span[contains(text(),'Validate')]","TGTYPESCREENREG");

        SWIPE.$("DOWN","TGTYPESCREENREG");

        SWIPE.$("DOWN","TGTYPESCREENREG");

        SWIPE.$("DOWN","TGTYPESCREENREG");

        ASSERT.$("ELE_ERRORMSG_MERCHANTIDCANNOTBEBLANK","//body/div[@id='Outer40']/div[@id='ContentPanel']/div[@id='Content40']/form[@id='workbenchForm']/div[@id='PageTiles']/div[@id='workbenchForm:workbenchTabs']/div[1]/div[2]/div[1]/div[4]/div[1]/ul[1]/li[2]/span[1]","VISIBLE","TGTYPESCREENREG");

        ASSERT.$("ELE_MSG_ERRORMESG_BELOWFIELD","//span[@class='ui-message-error-detail']","CONTAINS","Description cannot be blank.","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_VENDORDESCRIPTION","//input[@id='workbenchForm:workbenchTabs:vendorDescription']","123Test","FALSE","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_MERCHANTID","//input[@id='workbenchForm:workbenchTabs:merchantID']","Test321","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_VALIDATE","//span[contains(text(),'Validate')]","TGTYPESCREENREG");

        WAIT.$(3,"TGTYPESCREENREG");

        ASSERT.$("ELE_MSG_VALIDATIONSUCCESS","//div[@id='workbenchForm:workbenchTabs:messages']//span[@class='ui-messages-info-summary'][contains(text(),'Validation completed successfully with no errors.')]","VISIBLE","TGTYPESCREENREG");

        TAP.$("ELE_LOGO_GIAS","//span[text()='GIAS']","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

        CALL.$("TBD2Sprint16TwoNewFieldValidationsBB1553","TGTYPESCREENREG");

        SWIPE.$("DOWN","TGTYPESCREENREG");

        SWIPE.$("DOWN","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_SEARCHTASKS","//input[@id='workbenchForm:workbenchTabs:taskSearchContainer:taskSearchSearch']","app800","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_TASKSEARCH","//a[@id='workbenchForm:workbenchTabs:taskSearchContainer:tb_buttonSearch']//span[@class='fa fa-search fa-lg']","TGTYPESCREENREG");

        WAIT.$(1,"TGTYPESCREENREG");

        TAP.$("ELE_LABEL_EFTANDCREDITCARDSETUP","//span[contains(text(),'EFT and Credit Card Setup')]","TGTYPESCREENREG");

        WAIT.$("ELE_TXTBOX_ACCOUNTCONTROLNUMBER_ACCOUNTCONTROL","//input[@id='workbenchForm:workbenchTabs:grid:accountControlNumber_Col:filter']","VISIBLE","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_ACCOUNTCONTROLNUMBER_ACCOUNTCONTROL","//input[@id='workbenchForm:workbenchTabs:grid:accountControlNumber_Col:filter']",var_AccountControlNumber,"FALSE","TGTYPESCREENREG");

        WAIT.$(3,"TGTYPESCREENREG");

        TAP.$("ELE_LINK_ROW1","//tr[@class='ui-widget-content ui-datatable-even ui-datatable-selectable']","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        ASSERT.$("ELE_LABEL_CUSTOMERPROFILEID","//label[@id='workbenchForm:workbenchTabs:customerProfileId_lbl']","VISIBLE","TGTYPESCREENREG");

        ASSERT.$("ELE_LABEL_CREDITCARDTOKEN","//label[@id='workbenchForm:workbenchTabs:creditCardToken_lbl']","VISIBLE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_EDIT","//span[text()='Edit']","TGTYPESCREENREG");

        WAIT.$(3,"TGTYPESCREENREG");

        ASSERT.$("ELE_LABEL_CUSTOMERPROFILEID","//label[@id='workbenchForm:workbenchTabs:customerProfileId_lbl']","VISIBLE","TGTYPESCREENREG");

        ASSERT.$("ELE_LABEL_CREDITCARDTOKEN","//label[@id='workbenchForm:workbenchTabs:creditCardToken_lbl']","VISIBLE","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_CUSTOMERPROFILEID","//input[@id='workbenchForm:workbenchTabs:customerProfileId']","Test123","FALSE","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_CREDITCARDTOKEN","//input[@id='workbenchForm:workbenchTabs:creditCardToken']","Test123","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_VALIDATE","//span[contains(text(),'Validate')]","TGTYPESCREENREG");

        WAIT.$(3,"TGTYPESCREENREG");

        ASSERT.$("ELE_MSG_VALIDATIONSUCCESS","//div[@id='workbenchForm:workbenchTabs:messages']//span[@class='ui-messages-info-summary'][contains(text(),'Validation completed successfully with no errors.')]","VISIBLE","TGTYPESCREENREG");

        TAP.$("ELE_TAB_CLOSE3","//li[@class='ui-state-default ui-tabs-selected ui-state-active ui-corner-top']//span[@class='ui-icon ui-icon-close']","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_ADD2","//button[@id='workbenchForm:workbenchTabs:tb_buttonAdd']//span[@class='ui-button-text ui-c'][contains(text(),'Add')]","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        ASSERT.$("ELE_LABEL_CUSTOMERPROFILEID","//label[@id='workbenchForm:workbenchTabs:customerProfileId_lbl']","VISIBLE","TGTYPESCREENREG");

        ASSERT.$("ELE_LABEL_CREDITCARDTOKEN","//label[@id='workbenchForm:workbenchTabs:creditCardToken_lbl']","VISIBLE","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_CUSTOMERPROFILEID","//input[@id='workbenchForm:workbenchTabs:customerProfileId']","Test123","FALSE","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_CREDITCARDTOKEN","//input[@id='workbenchForm:workbenchTabs:creditCardToken']","123tesT","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_LOGO_GIAS","//span[text()='GIAS']","TGTYPESCREENREG");

        WAIT.$(3,"TGTYPESCREENREG");

		var_Count = var_Count + ( 1 );
                 LOGGER.info("Executed Step = INCREASEVAR,var_Count,1,TGTYPESCREENREG");
    	}
          LOGGER.info("Executed Step = WEND,TGTYPESCREENREG");
    	resetAttemps();

    }


    @Test
    public void glc1mod1() {

        // No need to declare / print method name here, handled by TestNg listener
        // declare("glc1mod1");

		int var_Count = 0;
                 LOGGER.info("Executed Step = VAR,Integer,var_Count,0,TGTYPESCREENREG");
		com.jayway.jsonpath.ReadContext var_CSVData = getJsonData("http://10.1.104.229/json/20201006/VVBeMv.json");
                 LOGGER.info("Executed Step = VAR,Json,var_CSVData,http://10.1.104.229/json/20201006/VVBeMv.json,TGTYPESCREENREG");
        LOGGER.info("Executed Step = REPEAT IF");
    	while (check(var_Count,"<",1,"TGTYPESCREENREG")) {
          LOGGER.info("Executed Step = WHILE,var_Count,<,1,TGTYPESCREENREG");
		String var_Group;
                 LOGGER.info("Executed Step = VAR,String,var_Group,TGTYPESCREENREG");
		var_Group = getJsonData(var_CSVData , "$.records.["+var_Count+"].Group");
                 LOGGER.info("Executed Step = STORE,var_Group,var_CSVData,$.records.[{var_Count}].Group,TGTYPESCREENREG");
        CALL.$("FTJQA2Login","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

		try { 
		 WebElement html = driver.findElement(By.tagName("html"));
		    	
		    	System.out.println("html size" + html.getSize());
		    	System.out.println("html" + html.getTagName());

		    	for( int i =0 ;i<4;i++) {
		        html.sendKeys(Keys.chord(Keys.CONTROL, Keys.SUBTRACT));
		    	}
		 
		 ;} 
		 catch(Exception e) { PrintVal("Renderer Script Exception : "+e.toString()); } 
		 PrintVal("Executed Step = CALLRENDERERSCRIPT,FTJ41ZOOMMAGNIFICATIONCHANGE"); 
        TYPE.$("ELE_TXTBOX_USERNAME","//input[@id='guestForm:username']","x5080","FALSE","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_PASSWORD","//input[@id='guestForm:password']","Welcome123$","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_LOGIN","//button[@id='guestForm:loginButton']","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        CALL.$("GLC1Mod1AddFieldColumnPolicyNumberToMBR040","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_SEARCHTASKS","//input[@id='workbenchForm:workbenchTabs:taskSearchContainer:taskSearchSearch']","mbr040","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_TASKSEARCH","//a[@id='workbenchForm:workbenchTabs:taskSearchContainer:tb_buttonSearch']//span[@class='fa fa-search fa-lg']","TGTYPESCREENREG");

        WAIT.$(1,"TGTYPESCREENREG");

        TAP.$("ELE_LABEL_MEMBERPRODUCT","//span[contains(text(),'Member Product')]","TGTYPESCREENREG");

        WAIT.$("ELE_BUTTON_CONTINUE","//span[contains(text(),'Continue')]","VISIBLE","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

        TYPE.$("ELE_TEXTBOX_GROUPNUMBER","//input[@id='workbenchForm:workbenchTabs:groupNumber']",var_Group,"FALSE","TGTYPESCREENREG");

        TAP.$("ELE_TXTBOX_MAINSEARCH","//input[@id='headerForm:searchMain_input']","TGTYPESCREENREG");

        WAIT.$(1,"TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_CONTINUE","//span[contains(text(),'Continue')]","TGTYPESCREENREG");

        WAIT.$("ELE_BUTTON_ADD","//span[contains(text(),'Add')]","VISIBLE","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

        ASSERT.$("ELE_GETVAL_POLICYNUMBERCOLUMN_MEMBERPRODUCTS","//span[contains(text(),'Policy Number')]","INVISIBLE","TGTYPESCREENREG");

        TAP.$("ELE_LABEL_MENUAVATAR","//span[@id='headerForm:avatarInitialsText']","TGTYPESCREENREG");

        TAP.$("ELE_LABEL_LAYOUTEDITOR","//span[contains(text(),'Layout Editor')]","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        ASSERT.$("ELE_GETVAL_POLICYNUMBERCOLUMN_LAYOUTEDITOR","//thead/tr[1]/th[12]/span[1]","INVISIBLE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_CREATEANEWLAYOUT","//span[contains(text(),'Create a new layout')]","TGTYPESCREENREG");

        WAIT.$(10,"TGTYPESCREENREG");

        TAP.$("ELE_TAB_EDITTABLE","//a[contains(text(),'Edit Table')]","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SHOWHIDEHIDDENCOLUMNS","//button[@id='workbenchForm:workbenchTabs:j_idt4897:column_toggler_button']","TGTYPESCREENREG");

        SCROLL.$("ELE_CHECKBOX_POLICYNUMBER_HIDDENCOLUMNLIST","//body/div[@id='workbenchForm:workbenchTabs:j_idt4897:pt_grid:column_toggler']/ul[1]/li[57]/div[1]/div[2]/span[1]","DOWN","TGTYPESCREENREG");

        TAP.$("ELE_CHECKBOX_POLICYNUMBER_HIDDENCOLUMNLIST","//body/div[@id='workbenchForm:workbenchTabs:j_idt4897:pt_grid:column_toggler']/ul[1]/li[57]/div[1]/div[2]/span[1]","TGTYPESCREENREG");

        TAP.$("ELE_TXTBOX_MAINSEARCH","//input[@id='headerForm:searchMain_input']","TGTYPESCREENREG");

        ASSERT.$("ELE_POLICYNUMBERCOLUMN_PAGELAYOUTEDITORTABLE","//thead/tr[1]/th[57]/span[1]","VISIBLE","TGTYPESCREENREG");

        TAP.$("ELE_DRPDWN_NEWLAYOUT_MBR040","//tbody/tr[1]/td[3]/div[1]/div[3]/span[1]","TGTYPESCREENREG");

        TAP.$("ELE_DRPDWN_NEWLAYOUT_PERSONALIZEDLAYOUTFORUSER_LAYOUTEDITORPAGE","//li[@id='workbenchForm:workbenchTabs:manageLayoutLevelTarget_1']","TGTYPESCREENREG");

        WAIT.$(8,"TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SAVE","//span[contains(text(),'Save')]","TGTYPESCREENREG");

        WAIT.$(3,"TGTYPESCREENREG");

        ASSERT.$("ELE_GETVAL_POLICYNUMBERCOLUMN_LAYOUTEDITOR","//thead/tr[1]/th[12]/span[1]","VISIBLE","TGTYPESCREENREG");

        TAP.$("ELE_TAB_MEMBERPRODUCTS","//a[contains(text(),' Products')]","TGTYPESCREENREG");

        WAIT.$(8,"TGTYPESCREENREG");

        ASSERT.$("ELE_GETVAL_POLICYNUMBERCOLUMN_MEMBERPRODUCTS","//span[contains(text(),'Policy Number')]","VISIBLE","TGTYPESCREENREG");

        TAP.$("ELE_TAB_LAYOUTEDITOR","//a[contains(text(),'Layout Editor')]","TGTYPESCREENREG");

        WAIT.$("ELE_BUTTON_EDIT","//span[text()='Edit']","VISIBLE","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_DELETE","//span[contains(text(),'Delete')]","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_CONFIRMDELETE_LAYOUTEDITOR","//body/div[@id='workbenchForm:workbenchTabs:tb_confirm_dialog']/div[3]/button[1]/span[2]","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        ASSERT.$("ELE_GETVAL_POLICYNUMBERCOLUMN_LAYOUTEDITOR","//thead/tr[1]/th[12]/span[1]","INVISIBLE","TGTYPESCREENREG");

        TAP.$("ELE_TAB_MEMBERPRODUCTS","//a[contains(text(),' Products')]","TGTYPESCREENREG");

        WAIT.$(3,"TGTYPESCREENREG");

        ASSERT.$("ELE_GETVAL_POLICYNUMBERCOLUMN_MEMBERPRODUCTS","//span[contains(text(),'Policy Number')]","INVISIBLE","TGTYPESCREENREG");

        TAP.$("ELE_LOGO_GIAS","//span[text()='GIAS']","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

		var_Count = var_Count + ( 1 );
                 LOGGER.info("Executed Step = INCREASEVAR,var_Count,1,TGTYPESCREENREG");
    	}
          LOGGER.info("Executed Step = WEND,TGTYPESCREENREG");
    	resetAttemps();

    }


    @Test
    public void f091sprint16() {

        // No need to declare / print method name here, handled by TestNg listener
        // declare("f091sprint16");

		int var_Count = 0;
                 LOGGER.info("Executed Step = VAR,Integer,var_Count,0,TGTYPESCREENREG");
		com.jayway.jsonpath.ReadContext var_CSVData = getJsonData("http://10.1.104.229/json/20201007/Dx63lv.json");
                 LOGGER.info("Executed Step = VAR,Json,var_CSVData,http://10.1.104.229/json/20201007/Dx63lv.json,TGTYPESCREENREG");
        LOGGER.info("Executed Step = REPEAT IF");
    	while (check(var_Count,"<",1,"TGTYPESCREENREG")) {
          LOGGER.info("Executed Step = WHILE,var_Count,<,1,TGTYPESCREENREG");
		String var_Agent;
                 LOGGER.info("Executed Step = VAR,String,var_Agent,TGTYPESCREENREG");
		var_Agent = getJsonData(var_CSVData , "$.records.["+var_Count+"].Agent");
                 LOGGER.info("Executed Step = STORE,var_Agent,var_CSVData,$.records.[{var_Count}].Agent,TGTYPESCREENREG");
        CALL.$("FTJQA2Login","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

		try { 
		 WebElement html = driver.findElement(By.tagName("html"));
		    	
		    	System.out.println("html size" + html.getSize());
		    	System.out.println("html" + html.getTagName());

		    	for( int i =0 ;i<4;i++) {
		        html.sendKeys(Keys.chord(Keys.CONTROL, Keys.SUBTRACT));
		    	}
		 
		 ;} 
		 catch(Exception e) { PrintVal("Renderer Script Exception : "+e.toString()); } 
		 PrintVal("Executed Step = CALLRENDERERSCRIPT,FTJ41ZOOMMAGNIFICATIONCHANGE"); 
        TYPE.$("ELE_TXTBOX_USERNAME","//input[@id='guestForm:username']","x5080","FALSE","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_PASSWORD","//input[@id='guestForm:password']","Welcome123$","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_LOGIN","//button[@id='guestForm:loginButton']","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        CALL.$("F091AddComStatementSeriesFieldOPR620BB1541","TGTYPESCREENREG");

        SWIPE.$("DOWN","TGTYPESCREENREG");

        SWIPE.$("DOWN","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_SEARCHTASKS","//input[@id='workbenchForm:workbenchTabs:taskSearchContainer:taskSearchSearch']","OPR620","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_TASKSEARCH","//a[@id='workbenchForm:workbenchTabs:taskSearchContainer:tb_buttonSearch']//span[@class='fa fa-search fa-lg']","TGTYPESCREENREG");

        WAIT.$(1,"TGTYPESCREENREG");

        TAP.$("ELE_LABEL_AGENTPRODUCTIONREPORTING","//span[contains(text(),'Agent Production Reporting')]","TGTYPESCREENREG");

        WAIT.$(4,"TGTYPESCREENREG");

        ASSERT.$("ELE_LABEL_STATEMENTSERIES","//label[@id='workbenchForm:workbenchTabs:statementSeries_lbl']","VISIBLE","TGTYPESCREENREG");

        TAP.$("ELE_CHECKBOX_STATEMENTSERIES_BLANK","//li[1]//div[1]//div[1]//span[1]","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_VALIDATE","//span[contains(text(),'Validate')]","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        ASSERT.$("ELE_MSG_ERRORMESG_BELOWFIELD","//span[@class='ui-message-error-detail']","CONTAINS","Statement Series selection is required.","TGTYPESCREENREG");

        TAP.$("ELE_CHECKBOX_STATEMENTSERIES_BLANK","//li[1]//div[1]//div[1]//span[1]","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_VALIDATE","//span[contains(text(),'Validate')]","TGTYPESCREENREG");

        WAIT.$(3,"TGTYPESCREENREG");

        ASSERT.$("ELE_MSG_VALIDATIONSUCCESS","//div[@id='workbenchForm:workbenchTabs:messages']//span[@class='ui-messages-info-summary'][contains(text(),'Validation completed successfully with no errors.')]","VISIBLE","TGTYPESCREENREG");

        TAP.$("ELE_CHECKBOX_STATEMENTSERIES_000","//html//body//div//div//div//form//div//div//div//div//div//table//tbody//tr//td//table//tbody//tr//td//fieldset//div//table//tbody//tr//td//div//div//ul//li[contains(text(),'000')]//div//div//span","TGTYPESCREENREG");

        TAP.$("ELE_CHECKBOX_STATEMENTSERIES_A01","//html//body//div//div//div//form//div//div//div//div//div//table//tbody//tr//td//table//tbody//tr//td//fieldset//div//table//tbody//tr//td//div//div//ul//li[contains(text(),'A01')]//div//div//span","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_VALIDATE","//span[contains(text(),'Validate')]","TGTYPESCREENREG");

        WAIT.$(3,"TGTYPESCREENREG");

        ASSERT.$("ELE_MSG_VALIDATIONSUCCESS","//div[@id='workbenchForm:workbenchTabs:messages']//span[@class='ui-messages-info-summary'][contains(text(),'Validation completed successfully with no errors.')]","VISIBLE","TGTYPESCREENREG");

        TAP.$("ELE_LOGO_GIAS","//span[text()='GIAS']","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

        CALL.$("F091Com525UpdateCommissionStatmentSeriesFieldBB1428","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_SEARCHTASKS","//input[@id='workbenchForm:workbenchTabs:taskSearchContainer:taskSearchSearch']","com500","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_TASKSEARCH","//a[@id='workbenchForm:workbenchTabs:taskSearchContainer:tb_buttonSearch']//span[@class='fa fa-search fa-lg']","TGTYPESCREENREG");

        WAIT.$(1,"TGTYPESCREENREG");

        TAP.$("ELE_LABEL_AGENTS","//span[contains(text(),'Agents')]","TGTYPESCREENREG");

        WAIT.$(3,"TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_AGENTNUMBER","//input[@name='workbenchForm:workbenchTabs:grid1:agentNumber_Col:filter']",var_Agent,"FALSE","TGTYPESCREENREG");

        WAIT.$(3,"TGTYPESCREENREG");

        TAP.$("ELE_LINK_ROW1","//tr[@class='ui-widget-content ui-datatable-even ui-datatable-selectable']","TGTYPESCREENREG");

        WAIT.$("ELE_BUTTON_EDIT","//span[text()='Edit']","VISIBLE","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

		String var_AgentsStatementSeries;
                 LOGGER.info("Executed Step = VAR,String,var_AgentsStatementSeries,TGTYPESCREENREG");
		var_AgentsStatementSeries = ActionWrapper.getElementValue("ELE_GETVAL_COMMISSIONSTATMENTSERIESAGENTTABLE", "//span[@id='workbenchForm:workbenchTabs:commissionStatSeri']");
                 LOGGER.info("Executed Step = STORE,var_AgentsStatementSeries,ELE_GETVAL_COMMISSIONSTATMENTSERIESAGENTTABLE,TGTYPESCREENREG");
        TAP.$("ELE_LOGO_GIAS","//span[text()='GIAS']","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_SEARCHTASKS","//input[@id='workbenchForm:workbenchTabs:taskSearchContainer:taskSearchSearch']","com525","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_TASKSEARCH","//a[@id='workbenchForm:workbenchTabs:taskSearchContainer:tb_buttonSearch']//span[@class='fa fa-search fa-lg']","TGTYPESCREENREG");

        WAIT.$(1,"TGTYPESCREENREG");

        TAP.$("ELE_LABEL_AGENTRECURRINGADJUSTMENTS","//span[contains(text(),'Agent Recurring Adjustments')]","TGTYPESCREENREG");

        WAIT.$("ELE_BUTTON_CONTINUE","//span[contains(text(),'Continue')]","VISIBLE","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_AGENTNUM","//input[@id='workbenchForm:workbenchTabs:agentNumber']",var_Agent,"FALSE","TGTYPESCREENREG");

        WAIT.$(3,"TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_CONTINUE","//span[contains(text(),'Continue')]","TGTYPESCREENREG");

        WAIT.$(3,"TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_ADD","//span[contains(text(),'Add')]","TGTYPESCREENREG");

        WAIT.$(3,"TGTYPESCREENREG");

        ASSERT.$("ELE_LABEL_COMMISSIONSTATEMENTSERIES","//label[@id='workbenchForm:workbenchTabs:comStmtSeries_lbl']","VISIBLE","TGTYPESCREENREG");

        ASSERT.$("ELE_LABEL_DRPDWN_COMMISSIONSTATMENTSERIES","//label[@id='workbenchForm:workbenchTabs:comStmtSeries_label']","CONTAINS",var_AgentsStatementSeries,"TGTYPESCREENREG");

        TAP.$("ELE_LOGO_GIAS","//span[text()='GIAS']","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

		var_Count = var_Count + ( 1 );
                 LOGGER.info("Executed Step = INCREASEVAR,var_Count,1,TGTYPESCREENREG");
    	}
          LOGGER.info("Executed Step = WEND,TGTYPESCREENREG");
    	resetAttemps();

    }


    @Test
    public void f190sprint16() {

        // No need to declare / print method name here, handled by TestNg listener
        // declare("f190sprint16");

		int var_Count = 0;
                 LOGGER.info("Executed Step = VAR,Integer,var_Count,0,TGTYPESCREENREG");
		com.jayway.jsonpath.ReadContext var_CSVData = getJsonData("http://10.1.104.229/json/20201009/pBpLoD.json");
                 LOGGER.info("Executed Step = VAR,Json,var_CSVData,http://10.1.104.229/json/20201009/pBpLoD.json,TGTYPESCREENREG");
        LOGGER.info("Executed Step = REPEAT IF");
    	while (check(var_Count,"<",1,"TGTYPESCREENREG")) {
          LOGGER.info("Executed Step = WHILE,var_Count,<,1,TGTYPESCREENREG");
		String var_Agent;
                 LOGGER.info("Executed Step = VAR,String,var_Agent,TGTYPESCREENREG");
		var_Agent = getJsonData(var_CSVData , "$.records.["+var_Count+"].Agent");
                 LOGGER.info("Executed Step = STORE,var_Agent,var_CSVData,$.records.[{var_Count}].Agent,TGTYPESCREENREG");
        CALL.$("FTJQA2Login","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

		try { 
		 WebElement html = driver.findElement(By.tagName("html"));
		    	
		    	System.out.println("html size" + html.getSize());
		    	System.out.println("html" + html.getTagName());

		    	for( int i =0 ;i<4;i++) {
		        html.sendKeys(Keys.chord(Keys.CONTROL, Keys.SUBTRACT));
		    	}
		 
		 ;} 
		 catch(Exception e) { PrintVal("Renderer Script Exception : "+e.toString()); } 
		 PrintVal("Executed Step = CALLRENDERERSCRIPT,FTJ41ZOOMMAGNIFICATIONCHANGE"); 
        TYPE.$("ELE_TXTBOX_USERNAME","//input[@id='guestForm:username']","x5080","FALSE","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_PASSWORD","//input[@id='guestForm:password']","Welcome123$","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_LOGIN","//button[@id='guestForm:loginButton']","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        CALL.$("F190AddCarrierColumnToAgentBalanceTableCOM501","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_SEARCHTASKS","//input[@id='workbenchForm:workbenchTabs:taskSearchContainer:taskSearchSearch']","com500","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_TASKSEARCH","//a[@id='workbenchForm:workbenchTabs:taskSearchContainer:tb_buttonSearch']//span[@class='fa fa-search fa-lg']","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

        TAP.$("ELE_LABEL_AGENTS","//span[contains(text(),'Agents')]","TGTYPESCREENREG");

        WAIT.$("ELE_BUTTON_ADD","//span[contains(text(),'Add')]","VISIBLE","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_AGENTNUMBER","//input[@name='workbenchForm:workbenchTabs:grid1:agentNumber_Col:filter']",var_Agent,"FALSE","TGTYPESCREENREG");

        WAIT.$(3,"TGTYPESCREENREG");

        TAP.$("ELE_LINK_ROW1","//tr[@class='ui-widget-content ui-datatable-even ui-datatable-selectable']","TGTYPESCREENREG");

        WAIT.$("ELE_BUTTON_EDIT","//span[text()='Edit']","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SHOWLINK","//span[@class='ui-button-icon-left ui-icon ui-c fa fa-ellipsis-v fa-2x']","TGTYPESCREENREG");

        TAP.$("ELE_LINK_BALANCES","//span[contains(text(),'Balances')]","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        ASSERT.$("ELE_GETVAL_CARRIERCOLUMN","//span[contains(text(),'Carrier')]","VISIBLE","TGTYPESCREENREG");

        LOGGER.info("Executed Step = START IF");
        if (check("ELE_GETVAL_BLANKCURRENCY_ROW1","//body[1]/div[1]/div[2]/div[1]/form[1]/div[1]/div[2]/div[1]/div[5]/div[1]/div[5]/div[1]/table[1]/tbody[1]/tr[1]/td[2]","VISIBLE","TGTYPESCREENREG")) {
         LOGGER.info("Executed Step = IF,ELE_GETVAL_BLANKCURRENCY_ROW1,VISIBLE,TGTYPESCREENREG");
        ASSERT.$("ELE_GETVAL_BLANKCURRENCY_ROW1","//body[1]/div[1]/div[2]/div[1]/form[1]/div[1]/div[2]/div[1]/div[5]/div[1]/div[5]/div[1]/table[1]/tbody[1]/tr[1]/td[2]","VISIBLE","TGTYPESCREENREG");

        }
          LOGGER.info("Executed Step = ENDIF,TGTYPESCREENREG");
        LOGGER.info("Executed Step = START IF");
        if (check("ELE_GETVAL_CURRENCY_ROW2","//span[@id='workbenchForm:workbenchTabs:grid:1:currencyCode']","VISIBLE","TGTYPESCREENREG")) {
         LOGGER.info("Executed Step = IF,ELE_GETVAL_CURRENCY_ROW2,VISIBLE,TGTYPESCREENREG");
        ASSERT.$("ELE_GETVAL_CURRENCY_ROW2","//span[@id='workbenchForm:workbenchTabs:grid:1:currencyCode']","CONTAINS","[FR]","TGTYPESCREENREG");

        }
          LOGGER.info("Executed Step = ENDIF,TGTYPESCREENREG");
        LOGGER.info("Executed Step = START IF");
        if (check("ELE_GETVAL_CURRENCY_ROW3","//span[@id='workbenchForm:workbenchTabs:grid:2:currencyCode']","VISIBLE","TGTYPESCREENREG")) {
         LOGGER.info("Executed Step = IF,ELE_GETVAL_CURRENCY_ROW3,VISIBLE,TGTYPESCREENREG");
        ASSERT.$("ELE_GETVAL_CURRENCY_ROW3","//span[@id='workbenchForm:workbenchTabs:grid:2:currencyCode']","CONTAINS","[US]","TGTYPESCREENREG");

        }
          LOGGER.info("Executed Step = ENDIF,TGTYPESCREENREG");
        TAP.$("ELE_LOGO_GIAS","//span[text()='GIAS']","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

		var_Count = var_Count + ( 1 );
                 LOGGER.info("Executed Step = INCREASEVAR,var_Count,1,TGTYPESCREENREG");
    	}
          LOGGER.info("Executed Step = WEND,TGTYPESCREENREG");
    	resetAttemps();

    }


}
