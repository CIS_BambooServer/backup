package appium.tgo;

import com.tg.octopus.base.TGOWrapper;
import com.tg.octopus.libs.testng.listener.TestListener;
import static com.tg.octopus.base.TGOWrapper.Action.*;
import static com.tg.octopus.base.TGOWrapper.LOGGER;
import com.tg.octopus.wrapper.ActionWrapper;

import com.jayway.jsonpath.JsonPath;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.SessionNotCreatedException;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.Augmenter;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.remote.UnreachableBrowserException;

import org.testng.Assert;
import org.testng.ITestContext;
import org.testng.ITestResult;
import org.testng.Reporter;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;
import java.net.HttpURLConnection;
import java.net.URL;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import org.json.JSONArray;
import org.json.JSONObject;
import helper.WebTestUtility;
import java.sql.*;
import com.testautomationguru.utility.PDFUtil;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.multipdf.Splitter;

import java.util.*;
import org.openqa.selenium.*;
import java.io.*;
import java.net.*;

import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;


@Listeners(TestListener.class)
public class MM_4_0_Base_Master_v.1.1.0 extends TGOWrapper {



    static {
       needsresetoncebeforetest ="false";
       needsresetbeforeeverymethod ="true";
       captureverystepsnap ="true";
       appendlastsnap ="false";
       ishybridapp ="true";

    }


    @Test
    public void giasbase40reg1to3n8to14n19createnewpolicy() {

        // No need to declare / print method name here, handled by TestNg listener
        // declare("giasbase40reg1to3n8to14n19createnewpolicy");

		com.jayway.jsonpath.ReadContext var_CSVData = getJsonData("C:/GIAS_Automation/4.0 MassMutual/InputData/MassMutual40_IP_Reg1to3n8to14n19_CreatePolicy.csv");
                 LOGGER.info("Executed Step = VAR,Json,var_CSVData,C:/GIAS_Automation/4.0 MassMutual/InputData/MassMutual40_IP_Reg1to3n8to14n19_CreatePolicy.csv,TGTYPESCREENREG");
		String var_PolicyNumber = "null";
                 LOGGER.info("Executed Step = VAR,String,var_PolicyNumber,null,TGTYPESCREENREG");
		String var_SessionDate = "null";
                 LOGGER.info("Executed Step = VAR,String,var_SessionDate,null,TGTYPESCREENREG");
		String var_SessionDate1 = "null";
                 LOGGER.info("Executed Step = VAR,String,var_SessionDate1,null,TGTYPESCREENREG");
		int var_Count = 0;
                 LOGGER.info("Executed Step = VAR,Integer,var_Count,0,TGTYPESCREENREG");
		String var_dataToPass = "null";
                 LOGGER.info("Executed Step = VAR,String,var_dataToPass,null,TGTYPESCREENREG");
		String var_PolicyStatus = "null";
                 LOGGER.info("Executed Step = VAR,String,var_PolicyStatus,null,TGTYPESCREENREG");
		String var_TaxQualifiedDescription = "null";
                 LOGGER.info("Executed Step = VAR,String,var_TaxQualifiedDescription,null,TGTYPESCREENREG");
		String var_CashWithApplication = "null";
                 LOGGER.info("Executed Step = VAR,String,var_CashWithApplication,null,TGTYPESCREENREG");
		String var_TaxWithholding = "null";
                 LOGGER.info("Executed Step = VAR,String,var_TaxWithholding,null,TGTYPESCREENREG");
		String var_SSNNumber = "null";
                 LOGGER.info("Executed Step = VAR,String,var_SSNNumber,null,TGTYPESCREENREG");
		String var_DateOfBirth = "null";
                 LOGGER.info("Executed Step = VAR,String,var_DateOfBirth,null,TGTYPESCREENREG");
        CALL.$("LoginToGIAS4","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENNO");

        TYPE.$("ELE_TXTBOX_USERNAME","//input[@id='guestForm:username']","Mano","FALSE","TGTYPESCREENNO");

        TYPE.$("ELE_TXTBOX_PASSWORD","//input[@id='guestForm:password']","Cnx12345!","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_LOGIN","//button[@id='guestForm:loginButton']","TGTYPESCREENNO");

        LOGGER.info("Executed Step = REPEAT IF");
    	while (check(var_Count,"<",1,"TGTYPESCREENREG")) {
          LOGGER.info("Executed Step = WHILE,var_Count,<,1,TGTYPESCREENREG");
		var_PolicyStatus = getJsonData(var_CSVData , "$.records["+var_Count+"].PolicyStatus");
                 LOGGER.info("Executed Step = STORE,var_PolicyStatus,var_CSVData,$.records[{var_Count}].PolicyStatus,TGTYPESCREENREG");
		var_SessionDate = getJsonData(var_CSVData , "$.records["+var_Count+"].SessionDate");
                 LOGGER.info("Executed Step = STORE,var_SessionDate,var_CSVData,$.records[{var_Count}].SessionDate,TGTYPESCREENREG");
		var_TaxQualifiedDescription = getJsonData(var_CSVData , "$.records["+var_Count+"].TaxQualifiedDescription");
                 LOGGER.info("Executed Step = STORE,var_TaxQualifiedDescription,var_CSVData,$.records[{var_Count}].TaxQualifiedDescription,TGTYPESCREENREG");
		var_PolicyNumber = getJsonData(var_CSVData , "$.records["+var_Count+"].PolicyNumber");
                 LOGGER.info("Executed Step = STORE,var_PolicyNumber,var_CSVData,$.records[{var_Count}].PolicyNumber,TGTYPESCREENREG");
		var_CashWithApplication = getJsonData(var_CSVData , "$.records["+var_Count+"].CashWithApplication");
                 LOGGER.info("Executed Step = STORE,var_CashWithApplication,var_CSVData,$.records[{var_Count}].CashWithApplication,TGTYPESCREENREG");
		var_TaxWithholding = getJsonData(var_CSVData , "$.records["+var_Count+"].TaxWithholding");
                 LOGGER.info("Executed Step = STORE,var_TaxWithholding,var_CSVData,$.records[{var_Count}].TaxWithholding,TGTYPESCREENREG");
		var_SSNNumber = getJsonData(var_CSVData , "$.records["+var_Count+"].SocialSecurityNumber");
                 LOGGER.info("Executed Step = STORE,var_SSNNumber,var_CSVData,$.records[{var_Count}].SocialSecurityNumber,TGTYPESCREENREG");
		var_DateOfBirth = getJsonData(var_CSVData , "$.records["+var_Count+"].ClientDOB");
                 LOGGER.info("Executed Step = STORE,var_DateOfBirth,var_CSVData,$.records[{var_Count}].ClientDOB,TGTYPESCREENREG");
        CALL.$("CheckIfPolicyExists","TGTYPESCREENREG");

        TAP.$("ELE_TAB_MYDASHBOARD","//a[contains(text(),'My Dashboard')]","TGTYPESCREENREG");

        WAIT.$(1,"TGTYPESCREENREG");

        TAP.$("ELE_TXTBOX_SEARCHTASKS","//input[@id='workbenchForm:workbenchTabs:taskSearchContainer:taskSearchSearch']","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_SEARCHTASKS","//input[@id='workbenchForm:workbenchTabs:taskSearchContainer:taskSearchSearch']","Policy Inquiry","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_TASKSEARCH","//a[@id='workbenchForm:workbenchTabs:taskSearchContainer:tb_buttonSearch']//span[@class='fa fa-search fa-lg']","TGTYPESCREENREG");

        TAP.$("ELE_LINK_POLICYINQUIRY","//span[contains(text(),'Policy Inquiry')]","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_POLICYNUMBER","//input[@id='workbenchForm:workbenchTabs:policyNumber']",var_PolicyNumber,"FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_CONTINUE","//span[contains(text(),'Continue')]","TGTYPESCREENREG");

        LOGGER.info("Executed Step = START IF");
        if (check("ELE_GETMSG_ERROR","//div[@id='workbenchForm:workbenchTabs:messages']//span[@class='ui-messages-error-summary']","INVISIBLE","TGTYPESCREENREG")) {
         LOGGER.info("Executed Step = IF,ELE_GETMSG_ERROR,INVISIBLE,TGTYPESCREENREG");
		PrintVal("Log : POLICY FOUND");
                 LOGGER.info("Executed Step = PRINTLOG,POLICY FOUND,TGTYPESCREENREG");
		try { 
		 driver.quit();
		 
		 ;} 
		 catch(Exception e) { PrintVal("Renderer Script Exception : "+e.toString()); } 
		 PrintVal("Executed Step = CALLRENDERERSCRIPT,EXITTHEPROGRAM"); 
        }
         else {
          LOGGER.info("Executed Step = ELSE,TGTYPESCREENREG");
        CALL.$("CheckIfClientExists","TGTYPESCREENREG");

		String var_SSNLast4Digits;
                 LOGGER.info("Executed Step = VAR,String,var_SSNLast4Digits,TGTYPESCREENREG");
		var_SSNLast4Digits = getJsonData(var_CSVData , "$.records["+var_Count+"].SSNLast4Digits");
                 LOGGER.info("Executed Step = STORE,var_SSNLast4Digits,var_CSVData,$.records[{var_Count}].SSNLast4Digits,TGTYPESCREENREG");
		String var_LastName2;
                 LOGGER.info("Executed Step = VAR,String,var_LastName2,TGTYPESCREENREG");
		var_LastName2 = getJsonData(var_CSVData , "$.records["+var_Count+"].LastName");
                 LOGGER.info("Executed Step = STORE,var_LastName2,var_CSVData,$.records[{var_Count}].LastName,TGTYPESCREENREG");
		String var_ClientDOB2;
                 LOGGER.info("Executed Step = VAR,String,var_ClientDOB2,TGTYPESCREENREG");
		var_ClientDOB2 = getJsonData(var_CSVData , "$.records["+var_Count+"].ClientDOB");
                 LOGGER.info("Executed Step = STORE,var_ClientDOB2,var_CSVData,$.records[{var_Count}].ClientDOB,TGTYPESCREENREG");
        TAP.$("ELE_TAB_MYDASHBOARD","//a[contains(text(),'My Dashboard')]","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_SEARCHTASKS","//input[@id='workbenchForm:workbenchTabs:taskSearchContainer:taskSearchSearch']","Client Search","FALSE","TGTYPESCREENREG");

        WAIT.$(1,"TGTYPESCREENNO");

        TAP.$("ELE_BUTTON_TASKSEARCH","//a[@id='workbenchForm:workbenchTabs:taskSearchContainer:tb_buttonSearch']//span[@class='fa fa-search fa-lg']","TGTYPESCREENREG");

        TAP.$("ELE_LINK_CLIENTSEARCH","//span[contains(text(),'Client Search')]","TGTYPESCREENREG");

        WAIT.$(3,"TGTYPESCREENNO");

        TYPE.$("ELE_TEXTINPUT_SEARCHCLIENTNAME","//input[@id='workbenchForm:workbenchTabs:clientGrid:individualName_Col:filter']",var_LastName2,"FALSE","TGTYPESCREENREG");

        WAIT.$(3,"TGTYPESCREENNO");

        TYPE.$("ELE_TXTBOX_IDNUMCLIENTSEARCH","//input[@id='workbenchForm:workbenchTabs:clientGrid:clientId_Col:filter']",var_SSNLast4Digits,"FALSE","TGTYPESCREENREG");

        WAIT.$(3,"TGTYPESCREENNO");

        TYPE.$("ELE_TXTBOX_DOBCLIENTSEARCHGRID","//th[@id='workbenchForm:workbenchTabs:clientGrid:dateOfBirthDate_Col']//input[@class='ui-inputfield ui-widget ui-state-default ui-corner-all hasDatepicker']",var_ClientDOB2,"FALSE","TGTYPESCREENREG");

        WAIT.$(3,"TGTYPESCREENNO");

        LOGGER.info("Executed Step = START IF");
        if (check("ELE_GETVAL_IFCLIENTEXISTSFIRSTGRD","//tr[@class='ui-widget-content ui-datatable-even ui-datatable-selectable']","VISIBLE","TGTYPESCREENREG")) {
         LOGGER.info("Executed Step = IF,ELE_GETVAL_IFCLIENTEXISTSFIRSTGRD,VISIBLE,TGTYPESCREENREG");
		try { 
		 Assert.assertTrue(false,"Client is already existing. Please enter unique client data ........");
		 
		 ;} 
		 catch(Exception e) { PrintVal("Renderer Script Exception : "+e.toString()); } 
		 PrintVal("Executed Step = CALLRENDERERSCRIPT,CHECKIFCLIENTEXISTS"); 
        }
         else {
          LOGGER.info("Executed Step = ELSE,TGTYPESCREENREG");
        ASSERT.$("ELE_GETVAL_NOTCLIENTFOUNDGRID","//td[contains(text(),'No records found.')]","CONTAINS","No records found","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_GIASHOME","//span[text()='GIAS']","TGTYPESCREENREG");

        }
          LOGGER.info("Executed Step = ENDIF,TGTYPESCREENREG");
        TAP.$("ELE_TAB_CLOSE1","span.ui-icon.ui-icon-close:nth-child(2)TGWEBCOMMACssSelectorTGWEBCOMMA1",1,0,"TGTYPESCREENREG");

        WAIT.$(1,"TGTYPESCREENREG");

        TAP.$("ELE_TAB_CLOSE0","span.ui-icon.ui-icon-close:nth-child(2)TGWEBCOMMACssSelectorTGWEBCOMMA0",1,0,"TGTYPESCREENREG");

        CALL.$("ChangeSessionDate","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SESSIONDATE","//a[@id='headerForm:sessionDatelink']","TGTYPESCREENNO");

        WAIT.$(2,"TGTYPESCREENNO");

        TYPE.$("ELE_TXTBOX_NEWSESSIONDATE","//input[@id='sessionDateForm:sessionDateInput_input']",var_SessionDate,"FALSE","TGTYPESCREENNO");

		takeFullScreenShot();
                 LOGGER.info("Executed Step = TAKEFULLSCREENSHOT,TGTYPESCREENREG");
        TAP.$("ELE_BUTTON_CHANGEDATE","//span[text()='Change Date']","TGTYPESCREENNO");

        WAIT.$(2,"TGTYPESCREENNO");

        CALL.$("AddPolicyContract","TGTYPESCREENREG");

		String var_ListBillNumber;
                 LOGGER.info("Executed Step = VAR,String,var_ListBillNumber,TGTYPESCREENNO");
		var_ListBillNumber = getJsonData(var_CSVData , "$.records["+var_Count+"].ListBillNumber");
                 LOGGER.info("Executed Step = STORE,var_ListBillNumber,var_CSVData,$.records[{var_Count}].ListBillNumber,TGTYPESCREENNO");
		String var_AgentNumber;
                 LOGGER.info("Executed Step = VAR,String,var_AgentNumber,TGTYPESCREENNO");
		var_AgentNumber = getJsonData(var_CSVData , "$.records["+var_Count+"].AgentNumber");
                 LOGGER.info("Executed Step = STORE,var_AgentNumber,var_CSVData,$.records[{var_Count}].AgentNumber,TGTYPESCREENNO");
        TAP.$("ELE_TXTBOX_SEARCHTASKS","//input[@id='workbenchForm:workbenchTabs:taskSearchContainer:taskSearchSearch']","TGTYPESCREENNO");

        TYPE.$("ELE_TXTBOX_SEARCHTASKS","//input[@id='workbenchForm:workbenchTabs:taskSearchContainer:taskSearchSearch']","New Policy Application","FALSE","TGTYPESCREENNO");

        TAP.$("ELE_BUTTON_TASKSEARCH","//a[@id='workbenchForm:workbenchTabs:taskSearchContainer:tb_buttonSearch']//span[@class='fa fa-search fa-lg']","TGTYPESCREENNO");

        WAIT.$(2,"TGTYPESCREENNO");

        TAP.$("ELE_LINK_NEWPOLICYAPPLICATION","//span[contains(text(),'New Policy Application')]","TGTYPESCREENNO");

        WAIT.$(3,"TGTYPESCREENNO");

        TAP.$("ELE_TAB_APPLICATIONENTRYUPDATE","//a[contains(text(),'Application Entry/Update')]","TGTYPESCREENNO");

        TAP.$("ELE_BUTTON_ADD","//button[@id='workbenchForm:workbenchTabs:tb_buttonAdd']//span[@class='ui-button-text ui-c'][contains(text(),'Add')]","TGTYPESCREENNO");

		var_dataToPass = getJsonData(var_CSVData , "$.records["+var_Count+"].PaymentFrequency");
                 LOGGER.info("Executed Step = STORE,var_dataToPass,var_CSVData,$.records[{var_Count}].PaymentFrequency,TGTYPESCREENNO");
        TAP.$("ELE_DROPDWN_PAYFREQ","//label[@id='workbenchForm:workbenchTabs:paymentMode_label']","TGTYPESCREENNO");

        WAIT.$(2,"TGTYPESCREENNO");

		try { 
		 Thread.sleep(1000);
		WebElement selectValueOfDropDown= driver.findElement(By.xpath("//li[@data-label='"+ var_dataToPass +"']"));

				selectValueOfDropDown.click();
		// Add Screenshot will only work for Android platform

				 Thread.sleep(1000);
		 
		 ;} 
		 catch(Exception e) { PrintVal("Renderer Script Exception : "+e.toString()); } 
		 PrintVal("Executed Step = CALLRENDERERSCRIPT,SELECTVALUEFROMDROPDOWN"); 
		var_dataToPass = getJsonData(var_CSVData , "$.records["+var_Count+"].PaymentMethod");
                 LOGGER.info("Executed Step = STORE,var_dataToPass,var_CSVData,$.records[{var_Count}].PaymentMethod,TGTYPESCREENNO");
        TAP.$("ELE_DRPDWN_PAYMETHOD","//label[@id='workbenchForm:workbenchTabs:paymentCode_label']","TGTYPESCREENNO");

        WAIT.$(2,"TGTYPESCREENNO");

		try { 
		 Thread.sleep(1000);
		WebElement selectValueOfDropDown= driver.findElement(By.xpath("//li[@data-label='"+ var_dataToPass +"']"));

				selectValueOfDropDown.click();
		// Add Screenshot will only work for Android platform

				 Thread.sleep(1000);
		 
		 ;} 
		 catch(Exception e) { PrintVal("Renderer Script Exception : "+e.toString()); } 
		 PrintVal("Executed Step = CALLRENDERERSCRIPT,SELECTVALUEFROMDROPDOWN"); 
		var_dataToPass = getJsonData(var_CSVData , "$.records["+var_Count+"].IssueState");
                 LOGGER.info("Executed Step = STORE,var_dataToPass,var_CSVData,$.records[{var_Count}].IssueState,TGTYPESCREENNO");
        TAP.$("ELE_DRPDWN_ISSUESTATECOUNTRY","//label[@id='workbenchForm:workbenchTabs:countryAndStateCode_label']","TGTYPESCREENNO");

        WAIT.$(2,"TGTYPESCREENNO");

		try { 
		 Thread.sleep(1000);
		WebElement selectValueOfDropDown= driver.findElement(By.xpath("//li[@data-label='"+ var_dataToPass +"']"));

				selectValueOfDropDown.click();
		// Add Screenshot will only work for Android platform

				 Thread.sleep(1000);
		 
		 ;} 
		 catch(Exception e) { PrintVal("Renderer Script Exception : "+e.toString()); } 
		 PrintVal("Executed Step = CALLRENDERERSCRIPT,SELECTVALUEFROMDROPDOWN"); 
		var_dataToPass = var_TaxQualifiedDescription;
                 LOGGER.info("Executed Step = STORE,var_dataToPass,var_TaxQualifiedDescription,TGTYPESCREENNO");
        TAP.$("ELE_DRPDWN_TAXQUALIFIEDDESP","//label[@id='workbenchForm:workbenchTabs:taxQualifiedCode_label']","TGTYPESCREENNO");

        WAIT.$(2,"TGTYPESCREENNO");

		try { 
		 Thread.sleep(1000);
		WebElement selectValueOfDropDown= driver.findElement(By.xpath("//li[@data-label='"+ var_dataToPass +"']"));

				selectValueOfDropDown.click();
		// Add Screenshot will only work for Android platform

				 Thread.sleep(1000);
		 
		 ;} 
		 catch(Exception e) { PrintVal("Renderer Script Exception : "+e.toString()); } 
		 PrintVal("Executed Step = CALLRENDERERSCRIPT,SELECTVALUEFROMDROPDOWN"); 
		var_dataToPass = var_TaxWithholding;
                 LOGGER.info("Executed Step = STORE,var_dataToPass,var_TaxWithholding,TGTYPESCREENNO");
        TAP.$("ELE_DRPDWN_TAXWITHOLDING","//label[@id='workbenchForm:workbenchTabs:taxWithholdingCode_label']","TGTYPESCREENNO");

        WAIT.$(2,"TGTYPESCREENNO");

		try { 
		 Thread.sleep(1000);
		WebElement selectValueOfDropDown= driver.findElement(By.xpath("//li[@data-label='"+ var_dataToPass +"']"));

				selectValueOfDropDown.click();
		// Add Screenshot will only work for Android platform

				 Thread.sleep(1000);
		 
		 ;} 
		 catch(Exception e) { PrintVal("Renderer Script Exception : "+e.toString()); } 
		 PrintVal("Executed Step = CALLRENDERERSCRIPT,SELECTVALUEFROMDROPDOWN"); 
        WAIT.$(1,"TGTYPESCREENNO");

        TYPE.$("ELE_TXTBOX_LISTBILLNUMBER","//input[@id='workbenchForm:workbenchTabs:groupNumber']",var_ListBillNumber,"FALSE","TGTYPESCREENNO");

        SWIPE.$("UP","TGTYPESCREENNO");

        WAIT.$(1,"TGTYPESCREENNO");

        TAP.$("ELE_TXTBOX_EFFECTIVEDATE","//input[@id='workbenchForm:workbenchTabs:effectiveDate_input']","TGTYPESCREENNO");

        TYPE.$("ELE_TXTBOX_EFFECTIVEDATE","//input[@id='workbenchForm:workbenchTabs:effectiveDate_input']",var_EffectiveDate,"FALSE","TGTYPESCREENNO");

        WAIT.$(1,"TGTYPESCREENNO");

        TAP.$("ELE_TXTBOX_CASHWITHAPP","//input[@id='workbenchForm:workbenchTabs:cashWithApplication_input']","TGTYPESCREENNO");

        WAIT.$(2,"TGTYPESCREENNO");

		try { 
		 WebElement CashApplicationTextField = driver.findElement(By.xpath("//input[@id='workbenchForm:workbenchTabs:cashWithApplication_input']"));
		CashApplicationTextField.sendKeys(Keys.BACK_SPACE);
				CashApplicationTextField.sendKeys(Keys.BACK_SPACE);
				CashApplicationTextField.sendKeys(Keys.BACK_SPACE);	
		// Add Screenshot will only work for Android platform

				CashApplicationTextField.sendKeys(""+var_CashWithApplication+"");// Add Screenshot will only work for Android platform

		Thread.sleep(1000);
		 
		 ;} 
		 catch(Exception e) { PrintVal("Renderer Script Exception : "+e.toString()); } 
		 PrintVal("Executed Step = CALLRENDERERSCRIPT,CLEARCASHWITHAPPLICATIONTXTFIELD"); 
        SWIPE.$("UP","TGTYPESCREENNO");

        SWIPE.$("UP","TGTYPESCREENNO");

        TAP.$("ELE_TAP_AGENTNUMBERBOX","//div[@id='workbenchForm:workbenchTabs:grid']//tr[1]//td[1]","TGTYPESCREENNO");

        WAIT.$(1,"TGTYPESCREENNO");

        TYPE.$("ELE_TXTBOX_AGENTNUM","//input[@id='workbenchForm:workbenchTabs:grid:0:in_agentNumber_Col']",var_AgentNumber,"FALSE","TGTYPESCREENNO");

        WAIT.$(1,"TGTYPESCREENNO");

        TAP.$("ELE_TAP_AGENTNUMBER","//th[@id='workbenchForm:workbenchTabs:grid:agentNumber_Col_Col']//span","TGTYPESCREENNO");

		takeFullScreenShot();
                 LOGGER.info("Executed Step = TAKEFULLSCREENSHOT,TGTYPESCREENREG");
        CALL.$("AddClient","TGTYPESCREENREG");

		String var_FirstName = "null";
                 LOGGER.info("Executed Step = VAR,String,var_FirstName,null,TGTYPESCREENNO");
		String var_LastName = "null";
                 LOGGER.info("Executed Step = VAR,String,var_LastName,null,TGTYPESCREENNO");
		String var_SocialSecurityNumber = "null";
                 LOGGER.info("Executed Step = VAR,String,var_SocialSecurityNumber,null,TGTYPESCREENNO");
		String var_stateToPass = "null";
                 LOGGER.info("Executed Step = VAR,String,var_stateToPass,null,TGTYPESCREENNO");
		var_LastName = getJsonData(var_CSVData , "$.records["+var_Count+"].LastName");
                 LOGGER.info("Executed Step = STORE,var_LastName,var_CSVData,$.records[{var_Count}].LastName,TGTYPESCREENNO");
		var_FirstName = getJsonData(var_CSVData , "$.records["+var_Count+"].FirstName");
                 LOGGER.info("Executed Step = STORE,var_FirstName,var_CSVData,$.records[{var_Count}].FirstName,TGTYPESCREENNO");
		var_SocialSecurityNumber = getJsonData(var_CSVData , "$.records["+var_Count+"].SocialSecurityNumber");
                 LOGGER.info("Executed Step = STORE,var_SocialSecurityNumber,var_CSVData,$.records[{var_Count}].SocialSecurityNumber,TGTYPESCREENNO");
		String var_ClientNameSearch;
                 LOGGER.info("Executed Step = VAR,String,var_ClientNameSearch,TGTYPESCREENNO");
		var_ClientNameSearch = getJsonData(var_CSVData , "$.records["+var_Count+"].ClientNameSearch");
                 LOGGER.info("Executed Step = STORE,var_ClientNameSearch,var_CSVData,$.records[{var_Count}].ClientNameSearch,TGTYPESCREENNO");
		String var_ClientID;
                 LOGGER.info("Executed Step = VAR,String,var_ClientID,TGTYPESCREENNO");
		var_ClientID = getJsonData(var_CSVData , "$.records["+var_Count+"].ClientID");
                 LOGGER.info("Executed Step = STORE,var_ClientID,var_CSVData,$.records[{var_Count}].ClientID,TGTYPESCREENNO");
		String var_ClientBirthDate;
                 LOGGER.info("Executed Step = VAR,String,var_ClientBirthDate,TGTYPESCREENNO");
		var_ClientBirthDate = getJsonData(var_CSVData , "$.records["+var_Count+"].ClientDOB");
                 LOGGER.info("Executed Step = STORE,var_ClientBirthDate,var_CSVData,$.records[{var_Count}].ClientDOB,TGTYPESCREENNO");
		String var_Gender;
                 LOGGER.info("Executed Step = VAR,String,var_Gender,TGTYPESCREENREG");
		var_Gender = getJsonData(var_CSVData , "$.records["+var_Count+"].Gender");
                 LOGGER.info("Executed Step = STORE,var_Gender,var_CSVData,$.records[{var_Count}].Gender,TGTYPESCREENREG");
        TAP.$("ELE_BUTTON_SHOWLINK","//button[@id='workbenchForm:workbenchTabs:tb_buttonLink']","TGTYPESCREENREG");

        WAIT.$(1,"TGTYPESCREENNO");

        TAP.$("ELE_LINK_SELECTCLIENT","//span[contains(text(),'Select Client')]","TGTYPESCREENNO");

        WAIT.$(2,"TGTYPESCREENNO");

        TYPE.$("ELE_TXTBOX_SEARCH","//input[@id='workbenchForm:workbenchTabs:searchName']",var_ClientNameSearch,"FALSE","TGTYPESCREENNO");

        TAP.$("ELE_BUTTON_SEARCH","//span[contains(text(),'Search')]","TGTYPESCREENNO");

        WAIT.$(3,"TGTYPESCREENNO");

        TYPE.$("ELE_TEXTINPUT_SEARCHCLIENTNAME","//input[@id='workbenchForm:workbenchTabs:clientGrid:individualName_Col:filter']",var_LastName,"FALSE","TGTYPESCREENNO");

        WAIT.$(2,"TGTYPESCREENNO");

        TYPE.$("ELE_TXTBOX_IDNUMCLIENTSEARCH","//input[@id='workbenchForm:workbenchTabs:clientGrid:clientId_Col:filter']",var_ClientID,"FALSE","TGTYPESCREENNO");

        WAIT.$(3,"TGTYPESCREENNO");

        LOGGER.info("Executed Step = START IF");
        if (check("ELE_GETVAL_CLIENTNAMEGRIDLINE1","//span[@id='workbenchForm:workbenchTabs:clientGrid:0:individualName']","VISIBLE","TGTYPESCREENREG")) {
         LOGGER.info("Executed Step = IF,ELE_GETVAL_CLIENTNAMEGRIDLINE1,VISIBLE,TGTYPESCREENREG");
        TAP.$("ELE_LINK_CLIENTSEARCHEDNAMEGRID","//tbody[@id='workbenchForm:workbenchTabs:clientGrid_data']//tr","TGTYPESCREENNO");

        WAIT.$(3,"TGTYPESCREENNO");

        TAP.$("ELE_BUTTON_SELECTCLIENT","//button[@id='workbenchForm:workbenchTabs:button_select']","TGTYPESCREENNO");

        TAP.$("ELE_BUTTON_CONTINUE","//span[contains(text(),'Continue')]","TGTYPESCREENNO");

        WAIT.$(3,"TGTYPESCREENNO");

        }
         else {
          LOGGER.info("Executed Step = ELSE,TGTYPESCREENREG");
        ASSERT.$("ELE_GETVAL_NOTCLIENTFOUNDGRID","//td[contains(text(),'No records found.')]","CONTAINS","No records found","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_CANCELSEARCH","//button[@id='workbenchForm:workbenchTabs:button_cancel']","TGTYPESCREENNO");

        WAIT.$(2,"TGTYPESCREENNO");

        TAP.$("ELE_BUTTON_ADDCLIENT","//button[@id='workbenchForm:workbenchTabs:button_add']","TGTYPESCREENNO");

        TYPE.$("ELE_TXTBOX_FIRSTNAME","//input[@id='workbenchForm:workbenchTabs:nameFirst']",var_FirstName,"FALSE","TGTYPESCREENNO");

        TYPE.$("ELE_TXTBOX_LASTNAME","//input[@id='workbenchForm:workbenchTabs:nameLast']",var_LastName,"FALSE","TGTYPESCREENNO");

        SCROLL.$("ELE_TXTBOX_DOB","//input[@id='workbenchForm:workbenchTabs:dateOfBirth_input']","DOWN","TGTYPESCREENNO");

		String var_ClientDOB = "null";
                 LOGGER.info("Executed Step = VAR,String,var_ClientDOB,null,TGTYPESCREENNO");
		var_ClientDOB = getJsonData(var_CSVData , "$.records["+var_Count+"].ClientDOB");
                 LOGGER.info("Executed Step = STORE,var_ClientDOB,var_CSVData,$.records[{var_Count}].ClientDOB,TGTYPESCREENNO");
        TAP.$("ELE_TXTBOX_DOB","//input[@id='workbenchForm:workbenchTabs:dateOfBirth_input']","TGTYPESCREENNO");

        TYPE.$("ELE_TXTBOX_DOB","//input[@id='workbenchForm:workbenchTabs:dateOfBirth_input']",var_ClientDOB,"FALSE","TGTYPESCREENNO");

        LOGGER.info("Executed Step = START IF");
        if (check(var_Gender,"CONTAINS","Male","TGTYPESCREENNO")) {
         LOGGER.info("Executed Step = IF,var_Gender,CONTAINS,Male,TGTYPESCREENNO");
        TAP.$("ELE_RDOBTN_MALE","//body//tr[@class='ui-widget-content']//tr[@class='ui-widget-content']//tr[2]//td[1]//div[1]//div[2]//span[1]","TGTYPESCREENNO");

        }
         else {
          LOGGER.info("Executed Step = ELSE,TGTYPESCREENNO");
        TAP.$("ELE_RDOBTN_FEMALE","//tr[@class='ui-widget-content']//tr[1]//td[1]//div[1]//div[2]//span[1]","TGTYPESCREENNO");

        }
          LOGGER.info("Executed Step = ENDIF,TGTYPESCREENNO");
        SWIPE.$("DOWN","TGTYPESCREENNO");

        SWIPE.$("DOWN","TGTYPESCREENNO");

		var_dataToPass = "Social Security Number";
                 LOGGER.info("Executed Step = STORE,var_dataToPass,Social Security Number,TGTYPESCREENNO");
        TAP.$("ELE_DRPDWN_IDTYPE","//label[@id='workbenchForm:workbenchTabs:taxIdenUsag_label']","TGTYPESCREENNO");

        WAIT.$(2,"TGTYPESCREENNO");

		try { 
		 Thread.sleep(1000);
		WebElement selectValueOfDropDown= driver.findElement(By.xpath("//li[@data-label='"+ var_dataToPass +"']"));

				selectValueOfDropDown.click();
		// Add Screenshot will only work for Android platform

				 Thread.sleep(1000);
		 
		 ;} 
		 catch(Exception e) { PrintVal("Renderer Script Exception : "+e.toString()); } 
		 PrintVal("Executed Step = CALLRENDERERSCRIPT,SELECTVALUEFROMDROPDOWN"); 
        TYPE.$("ELE_TXTBOX_IDNUMBER","//input[@id='workbenchForm:workbenchTabs:identificationNumber']",var_SocialSecurityNumber,"FALSE","TGTYPESCREENNO");

		takeFullScreenShot();
                 LOGGER.info("Executed Step = TAKEFULLSCREENSHOT,TGTYPESCREENREG");
        TAP.$("ELE_BUTTON_VALIDATE","//span[contains(text(),'Validate')]","TGTYPESCREENNO");

        WAIT.$(2,"TGTYPESCREENNO");

        ASSERT.$("ELE_GETMSG_SUCCESS","//div[@id='workbenchForm:workbenchTabs:messages']//span[@class='ui-messages-info-summary']","CONTAINS","successfully","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SAVE","//span[contains(text(),'Save')]","TGTYPESCREENNO");

        WAIT.$(3,"TGTYPESCREENNO");

        ASSERT.$("ELE_GETMSG_SUCCESS","//div[@id='workbenchForm:workbenchTabs:messages']//span[@class='ui-messages-info-summary']","CONTAINS","successfully","TGTYPESCREENREG");

		String var_ClientCity = "null";
                 LOGGER.info("Executed Step = VAR,String,var_ClientCity,null,TGTYPESCREENNO");
		String var_ClientZipCode = "null";
                 LOGGER.info("Executed Step = VAR,String,var_ClientZipCode,null,TGTYPESCREENNO");
		String var_ClientAddressOne = "null";
                 LOGGER.info("Executed Step = VAR,String,var_ClientAddressOne,null,TGTYPESCREENNO");
		var_ClientCity = getJsonData(var_CSVData , "$.records["+var_Count+"].ClientCity");
                 LOGGER.info("Executed Step = STORE,var_ClientCity,var_CSVData,$.records[{var_Count}].ClientCity,TGTYPESCREENNO");
		var_ClientZipCode = getJsonData(var_CSVData , "$.records["+var_Count+"].ClientZipCode");
                 LOGGER.info("Executed Step = STORE,var_ClientZipCode,var_CSVData,$.records[{var_Count}].ClientZipCode,TGTYPESCREENNO");
		var_ClientAddressOne = getJsonData(var_CSVData , "$.records["+var_Count+"].ClientAddress");
                 LOGGER.info("Executed Step = STORE,var_ClientAddressOne,var_CSVData,$.records[{var_Count}].ClientAddress,TGTYPESCREENNO");
        TYPE.$("ELE_TXTBOX_ADDRESSLINE1","//input[@id='workbenchForm:workbenchTabs:addressLineOne']",var_ClientAddressOne,"FALSE","TGTYPESCREENNO");

        TYPE.$("ELE_TXTBOX_CITY","//input[@id='workbenchForm:workbenchTabs:addressCity']",var_ClientCity,"FALSE","TGTYPESCREENNO");

        SCROLL.$("ELE_TXTBOX_ZIPCODE","//input[@id='workbenchForm:workbenchTabs:zipCode']","DOWN","TGTYPESCREENNO");

		var_stateToPass = getJsonData(var_CSVData , "$.records["+var_Count+"].IssueState");
                 LOGGER.info("Executed Step = STORE,var_stateToPass,var_CSVData,$.records[{var_Count}].IssueState,TGTYPESCREENNO");
        TAP.$("ELE_DRPDWN_ADDRESSSTATE","//label[@id='workbenchForm:workbenchTabs:countryAndStateCode_label']","TGTYPESCREENNO");

        WAIT.$(2,"TGTYPESCREENNO");

		try { 
		 Thread.sleep(1000);
				// WebElement selectValueOfDropDown= driver.findElement(By.xpath("//li[@id='workbenchForm:workbenchTabs:'"+ var_stateToPass +"']"));
		// WebElement selectValueOfDropDown= driver.findElement(By.xpath("//ul[@id='workbenchForm:workbenchTabs:birthCountryAndStateCode_items']//li[@data-label='"+ var_stateToPass +"']")); BIRTH STATE DROPDWN


		 WebElement selectValueOfDropDown= driver.findElement(By.xpath("//ul[@id='workbenchForm:workbenchTabs:countryAndStateCode_items']//li[@data-label='"+ var_stateToPass +"']")); //Address state Dropdown


						// Add Screenshot will only work for Android platform

						selectValueOfDropDown.click();	
		// Add Screenshot will only work for Android platform


						 Thread.sleep(1000);
		 
		 ;} 
		 catch(Exception e) { PrintVal("Renderer Script Exception : "+e.toString()); } 
		 PrintVal("Executed Step = CALLRENDERERSCRIPT,SELECTVALUEFROMDRPDOWNWITHPRESELECTEDVALUES"); 
        TYPE.$("ELE_TXTBOX_ZIPCODE","//input[@id='workbenchForm:workbenchTabs:zipCode']",var_ClientZipCode,"FALSE","TGTYPESCREENNO");

		takeFullScreenShot();
                 LOGGER.info("Executed Step = TAKEFULLSCREENSHOT,TGTYPESCREENREG");
        TAP.$("ELE_BUTTON_VALIDATE","//span[contains(text(),'Validate')]","TGTYPESCREENNO");

        ASSERT.$("ELE_GETMSG_SUCCESS","//div[@id='workbenchForm:workbenchTabs:messages']//span[@class='ui-messages-info-summary']","CONTAINS","successfully","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SAVE","//span[contains(text(),'Save')]","TGTYPESCREENNO");

        WAIT.$(2,"TGTYPESCREENNO");

        ASSERT.$("ELE_GETMSG_SUCCESS","//div[@id='workbenchForm:workbenchTabs:messages']//span[@class='ui-messages-info-summary']","CONTAINS","successfully","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_CONTINUE","//span[contains(text(),'Continue')]","TGTYPESCREENNO");

        WAIT.$(3,"TGTYPESCREENNO");

        }
          LOGGER.info("Executed Step = ENDIF,TGTYPESCREENNO");
        CALL.$("AddClient","TGTYPESCREENREG");

		String var_FirstName = "null";
                 LOGGER.info("Executed Step = VAR,String,var_FirstName,null,TGTYPESCREENNO");
		String var_LastName = "null";
                 LOGGER.info("Executed Step = VAR,String,var_LastName,null,TGTYPESCREENNO");
		String var_SocialSecurityNumber = "null";
                 LOGGER.info("Executed Step = VAR,String,var_SocialSecurityNumber,null,TGTYPESCREENNO");
		String var_stateToPass = "null";
                 LOGGER.info("Executed Step = VAR,String,var_stateToPass,null,TGTYPESCREENNO");
		var_LastName = getJsonData(var_CSVData , "$.records["+var_Count+"].LastName");
                 LOGGER.info("Executed Step = STORE,var_LastName,var_CSVData,$.records[{var_Count}].LastName,TGTYPESCREENNO");
		var_FirstName = getJsonData(var_CSVData , "$.records["+var_Count+"].FirstName");
                 LOGGER.info("Executed Step = STORE,var_FirstName,var_CSVData,$.records[{var_Count}].FirstName,TGTYPESCREENNO");
		var_SocialSecurityNumber = getJsonData(var_CSVData , "$.records["+var_Count+"].SocialSecurityNumber");
                 LOGGER.info("Executed Step = STORE,var_SocialSecurityNumber,var_CSVData,$.records[{var_Count}].SocialSecurityNumber,TGTYPESCREENNO");
		String var_ClientNameSearch;
                 LOGGER.info("Executed Step = VAR,String,var_ClientNameSearch,TGTYPESCREENNO");
		var_ClientNameSearch = getJsonData(var_CSVData , "$.records["+var_Count+"].ClientNameSearch");
                 LOGGER.info("Executed Step = STORE,var_ClientNameSearch,var_CSVData,$.records[{var_Count}].ClientNameSearch,TGTYPESCREENNO");
		String var_ClientID;
                 LOGGER.info("Executed Step = VAR,String,var_ClientID,TGTYPESCREENNO");
		var_ClientID = getJsonData(var_CSVData , "$.records["+var_Count+"].ClientID");
                 LOGGER.info("Executed Step = STORE,var_ClientID,var_CSVData,$.records[{var_Count}].ClientID,TGTYPESCREENNO");
		String var_ClientBirthDate;
                 LOGGER.info("Executed Step = VAR,String,var_ClientBirthDate,TGTYPESCREENNO");
		var_ClientBirthDate = getJsonData(var_CSVData , "$.records["+var_Count+"].ClientDOB");
                 LOGGER.info("Executed Step = STORE,var_ClientBirthDate,var_CSVData,$.records[{var_Count}].ClientDOB,TGTYPESCREENNO");
		String var_Gender;
                 LOGGER.info("Executed Step = VAR,String,var_Gender,TGTYPESCREENREG");
		var_Gender = getJsonData(var_CSVData , "$.records["+var_Count+"].Gender");
                 LOGGER.info("Executed Step = STORE,var_Gender,var_CSVData,$.records[{var_Count}].Gender,TGTYPESCREENREG");
        TAP.$("ELE_BUTTON_SHOWLINK","//button[@id='workbenchForm:workbenchTabs:tb_buttonLink']","TGTYPESCREENREG");

        WAIT.$(1,"TGTYPESCREENNO");

        TAP.$("ELE_LINK_SELECTCLIENT","//span[contains(text(),'Select Client')]","TGTYPESCREENNO");

        WAIT.$(2,"TGTYPESCREENNO");

        TYPE.$("ELE_TXTBOX_SEARCH","//input[@id='workbenchForm:workbenchTabs:searchName']",var_ClientNameSearch,"FALSE","TGTYPESCREENNO");

        TAP.$("ELE_BUTTON_SEARCH","//span[contains(text(),'Search')]","TGTYPESCREENNO");

        WAIT.$(3,"TGTYPESCREENNO");

        TYPE.$("ELE_TEXTINPUT_SEARCHCLIENTNAME","//input[@id='workbenchForm:workbenchTabs:clientGrid:individualName_Col:filter']",var_LastName,"FALSE","TGTYPESCREENNO");

        WAIT.$(2,"TGTYPESCREENNO");

        TYPE.$("ELE_TXTBOX_IDNUMCLIENTSEARCH","//input[@id='workbenchForm:workbenchTabs:clientGrid:clientId_Col:filter']",var_ClientID,"FALSE","TGTYPESCREENNO");

        WAIT.$(3,"TGTYPESCREENNO");

        LOGGER.info("Executed Step = START IF");
        if (check("ELE_GETVAL_CLIENTNAMEGRIDLINE1","//span[@id='workbenchForm:workbenchTabs:clientGrid:0:individualName']","VISIBLE","TGTYPESCREENREG")) {
         LOGGER.info("Executed Step = IF,ELE_GETVAL_CLIENTNAMEGRIDLINE1,VISIBLE,TGTYPESCREENREG");
        TAP.$("ELE_LINK_CLIENTSEARCHEDNAMEGRID","//tbody[@id='workbenchForm:workbenchTabs:clientGrid_data']//tr","TGTYPESCREENNO");

        WAIT.$(3,"TGTYPESCREENNO");

        TAP.$("ELE_BUTTON_SELECTCLIENT","//button[@id='workbenchForm:workbenchTabs:button_select']","TGTYPESCREENNO");

        TAP.$("ELE_BUTTON_CONTINUE","//span[contains(text(),'Continue')]","TGTYPESCREENNO");

        WAIT.$(3,"TGTYPESCREENNO");

        }
         else {
          LOGGER.info("Executed Step = ELSE,TGTYPESCREENREG");
        ASSERT.$("ELE_GETVAL_NOTCLIENTFOUNDGRID","//td[contains(text(),'No records found.')]","CONTAINS","No records found","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_CANCELSEARCH","//button[@id='workbenchForm:workbenchTabs:button_cancel']","TGTYPESCREENNO");

        WAIT.$(2,"TGTYPESCREENNO");

        TAP.$("ELE_BUTTON_ADDCLIENT","//button[@id='workbenchForm:workbenchTabs:button_add']","TGTYPESCREENNO");

        TYPE.$("ELE_TXTBOX_FIRSTNAME","//input[@id='workbenchForm:workbenchTabs:nameFirst']",var_FirstName,"FALSE","TGTYPESCREENNO");

        TYPE.$("ELE_TXTBOX_LASTNAME","//input[@id='workbenchForm:workbenchTabs:nameLast']",var_LastName,"FALSE","TGTYPESCREENNO");

        SCROLL.$("ELE_TXTBOX_DOB","//input[@id='workbenchForm:workbenchTabs:dateOfBirth_input']","DOWN","TGTYPESCREENNO");

		String var_ClientDOB = "null";
                 LOGGER.info("Executed Step = VAR,String,var_ClientDOB,null,TGTYPESCREENNO");
		var_ClientDOB = getJsonData(var_CSVData , "$.records["+var_Count+"].ClientDOB");
                 LOGGER.info("Executed Step = STORE,var_ClientDOB,var_CSVData,$.records[{var_Count}].ClientDOB,TGTYPESCREENNO");
        TAP.$("ELE_TXTBOX_DOB","//input[@id='workbenchForm:workbenchTabs:dateOfBirth_input']","TGTYPESCREENNO");

        TYPE.$("ELE_TXTBOX_DOB","//input[@id='workbenchForm:workbenchTabs:dateOfBirth_input']",var_ClientDOB,"FALSE","TGTYPESCREENNO");

        LOGGER.info("Executed Step = START IF");
        if (check(var_Gender,"CONTAINS","Male","TGTYPESCREENNO")) {
         LOGGER.info("Executed Step = IF,var_Gender,CONTAINS,Male,TGTYPESCREENNO");
        TAP.$("ELE_RDOBTN_MALE","//body//tr[@class='ui-widget-content']//tr[@class='ui-widget-content']//tr[2]//td[1]//div[1]//div[2]//span[1]","TGTYPESCREENNO");

        }
         else {
          LOGGER.info("Executed Step = ELSE,TGTYPESCREENNO");
        TAP.$("ELE_RDOBTN_FEMALE","//tr[@class='ui-widget-content']//tr[1]//td[1]//div[1]//div[2]//span[1]","TGTYPESCREENNO");

        }
          LOGGER.info("Executed Step = ENDIF,TGTYPESCREENNO");
        SWIPE.$("DOWN","TGTYPESCREENNO");

        SWIPE.$("DOWN","TGTYPESCREENNO");

		var_dataToPass = "Social Security Number";
                 LOGGER.info("Executed Step = STORE,var_dataToPass,Social Security Number,TGTYPESCREENNO");
        TAP.$("ELE_DRPDWN_IDTYPE","//label[@id='workbenchForm:workbenchTabs:taxIdenUsag_label']","TGTYPESCREENNO");

        WAIT.$(2,"TGTYPESCREENNO");

		try { 
		 Thread.sleep(1000);
		WebElement selectValueOfDropDown= driver.findElement(By.xpath("//li[@data-label='"+ var_dataToPass +"']"));

				selectValueOfDropDown.click();
		// Add Screenshot will only work for Android platform

				 Thread.sleep(1000);
		 
		 ;} 
		 catch(Exception e) { PrintVal("Renderer Script Exception : "+e.toString()); } 
		 PrintVal("Executed Step = CALLRENDERERSCRIPT,SELECTVALUEFROMDROPDOWN"); 
        TYPE.$("ELE_TXTBOX_IDNUMBER","//input[@id='workbenchForm:workbenchTabs:identificationNumber']",var_SocialSecurityNumber,"FALSE","TGTYPESCREENNO");

		takeFullScreenShot();
                 LOGGER.info("Executed Step = TAKEFULLSCREENSHOT,TGTYPESCREENREG");
        TAP.$("ELE_BUTTON_VALIDATE","//span[contains(text(),'Validate')]","TGTYPESCREENNO");

        WAIT.$(2,"TGTYPESCREENNO");

        ASSERT.$("ELE_GETMSG_SUCCESS","//div[@id='workbenchForm:workbenchTabs:messages']//span[@class='ui-messages-info-summary']","CONTAINS","successfully","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SAVE","//span[contains(text(),'Save')]","TGTYPESCREENNO");

        WAIT.$(3,"TGTYPESCREENNO");

        ASSERT.$("ELE_GETMSG_SUCCESS","//div[@id='workbenchForm:workbenchTabs:messages']//span[@class='ui-messages-info-summary']","CONTAINS","successfully","TGTYPESCREENREG");

		String var_ClientCity = "null";
                 LOGGER.info("Executed Step = VAR,String,var_ClientCity,null,TGTYPESCREENNO");
		String var_ClientZipCode = "null";
                 LOGGER.info("Executed Step = VAR,String,var_ClientZipCode,null,TGTYPESCREENNO");
		String var_ClientAddressOne = "null";
                 LOGGER.info("Executed Step = VAR,String,var_ClientAddressOne,null,TGTYPESCREENNO");
		var_ClientCity = getJsonData(var_CSVData , "$.records["+var_Count+"].ClientCity");
                 LOGGER.info("Executed Step = STORE,var_ClientCity,var_CSVData,$.records[{var_Count}].ClientCity,TGTYPESCREENNO");
		var_ClientZipCode = getJsonData(var_CSVData , "$.records["+var_Count+"].ClientZipCode");
                 LOGGER.info("Executed Step = STORE,var_ClientZipCode,var_CSVData,$.records[{var_Count}].ClientZipCode,TGTYPESCREENNO");
		var_ClientAddressOne = getJsonData(var_CSVData , "$.records["+var_Count+"].ClientAddress");
                 LOGGER.info("Executed Step = STORE,var_ClientAddressOne,var_CSVData,$.records[{var_Count}].ClientAddress,TGTYPESCREENNO");
        TYPE.$("ELE_TXTBOX_ADDRESSLINE1","//input[@id='workbenchForm:workbenchTabs:addressLineOne']",var_ClientAddressOne,"FALSE","TGTYPESCREENNO");

        TYPE.$("ELE_TXTBOX_CITY","//input[@id='workbenchForm:workbenchTabs:addressCity']",var_ClientCity,"FALSE","TGTYPESCREENNO");

        SCROLL.$("ELE_TXTBOX_ZIPCODE","//input[@id='workbenchForm:workbenchTabs:zipCode']","DOWN","TGTYPESCREENNO");

		var_stateToPass = getJsonData(var_CSVData , "$.records["+var_Count+"].IssueState");
                 LOGGER.info("Executed Step = STORE,var_stateToPass,var_CSVData,$.records[{var_Count}].IssueState,TGTYPESCREENNO");
        TAP.$("ELE_DRPDWN_ADDRESSSTATE","//label[@id='workbenchForm:workbenchTabs:countryAndStateCode_label']","TGTYPESCREENNO");

        WAIT.$(2,"TGTYPESCREENNO");

		try { 
		 Thread.sleep(1000);
				// WebElement selectValueOfDropDown= driver.findElement(By.xpath("//li[@id='workbenchForm:workbenchTabs:'"+ var_stateToPass +"']"));
		// WebElement selectValueOfDropDown= driver.findElement(By.xpath("//ul[@id='workbenchForm:workbenchTabs:birthCountryAndStateCode_items']//li[@data-label='"+ var_stateToPass +"']")); BIRTH STATE DROPDWN


		 WebElement selectValueOfDropDown= driver.findElement(By.xpath("//ul[@id='workbenchForm:workbenchTabs:countryAndStateCode_items']//li[@data-label='"+ var_stateToPass +"']")); //Address state Dropdown


						// Add Screenshot will only work for Android platform

						selectValueOfDropDown.click();	
		// Add Screenshot will only work for Android platform


						 Thread.sleep(1000);
		 
		 ;} 
		 catch(Exception e) { PrintVal("Renderer Script Exception : "+e.toString()); } 
		 PrintVal("Executed Step = CALLRENDERERSCRIPT,SELECTVALUEFROMDRPDOWNWITHPRESELECTEDVALUES"); 
        TYPE.$("ELE_TXTBOX_ZIPCODE","//input[@id='workbenchForm:workbenchTabs:zipCode']",var_ClientZipCode,"FALSE","TGTYPESCREENNO");

		takeFullScreenShot();
                 LOGGER.info("Executed Step = TAKEFULLSCREENSHOT,TGTYPESCREENREG");
        TAP.$("ELE_BUTTON_VALIDATE","//span[contains(text(),'Validate')]","TGTYPESCREENNO");

        ASSERT.$("ELE_GETMSG_SUCCESS","//div[@id='workbenchForm:workbenchTabs:messages']//span[@class='ui-messages-info-summary']","CONTAINS","successfully","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SAVE","//span[contains(text(),'Save')]","TGTYPESCREENNO");

        WAIT.$(2,"TGTYPESCREENNO");

        ASSERT.$("ELE_GETMSG_SUCCESS","//div[@id='workbenchForm:workbenchTabs:messages']//span[@class='ui-messages-info-summary']","CONTAINS","successfully","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_CONTINUE","//span[contains(text(),'Continue')]","TGTYPESCREENNO");

        WAIT.$(3,"TGTYPESCREENNO");

        }
          LOGGER.info("Executed Step = ENDIF,TGTYPESCREENNO");
        CALL.$("AddBenefitPlanAndssuePolicy","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SHOWLINK","//button[@id='workbenchForm:workbenchTabs:tb_buttonLink']","TGTYPESCREENREG");

        WAIT.$(1,"TGTYPESCREENREG");

        TAP.$("ELE_LINK_BENEFIT","//span[contains(text(),'Benefits')]","TGTYPESCREENREG");

        WAIT.$(4,"TGTYPESCREENREG");

		var_dataToPass = getJsonData(var_CSVData , "$.records["+var_Count+"].BenefitPlan");
                 LOGGER.info("Executed Step = STORE,var_dataToPass,var_CSVData,$.records[{var_Count}].BenefitPlan,TGTYPESCREENREG");
		String var_UnitOfInsurance = "null";
                 LOGGER.info("Executed Step = VAR,String,var_UnitOfInsurance,null,TGTYPESCREENREG");
		var_UnitOfInsurance = getJsonData(var_CSVData , "$.records["+var_Count+"].Units");
                 LOGGER.info("Executed Step = STORE,var_UnitOfInsurance,var_CSVData,$.records[{var_Count}].Units,TGTYPESCREENREG");
		String var_RequestedPremiumAmount = "null";
                 LOGGER.info("Executed Step = VAR,String,var_RequestedPremiumAmount,null,TGTYPESCREENREG");
		var_RequestedPremiumAmount = getJsonData(var_CSVData , "$.records["+var_Count+"].requestedAnnualPremium");
                 LOGGER.info("Executed Step = STORE,var_RequestedPremiumAmount,var_CSVData,$.records[{var_Count}].requestedAnnualPremium,TGTYPESCREENREG");
        TAP.$("ELE_DRPDWN_BENEFITPLAN","//label[@id='workbenchForm:workbenchTabs:initialPlanCode_label']","TGTYPESCREENREG");

		try { 
		 Thread.sleep(1000);
		WebElement selectValueOfDropDown= driver.findElement(By.xpath("//li[@data-label='"+ var_dataToPass +"']"));

				selectValueOfDropDown.click();
		// Add Screenshot will only work for Android platform

				 Thread.sleep(1000);
		 
		 ;} 
		 catch(Exception e) { PrintVal("Renderer Script Exception : "+e.toString()); } 
		 PrintVal("Executed Step = CALLRENDERERSCRIPT,SELECTVALUEFROMDROPDOWN"); 
        TAP.$("ELE_BUTTON_CONTINUE","//span[contains(text(),'Continue')]","TGTYPESCREENREG");

        WAIT.$(1,"TGTYPESCREENREG");

        TAP.$("ELE_TAB_ADDNEWPOLICYBENEFIT","//a[contains(text(),'Add')]","TGTYPESCREENREG");

		try { 
		 if(var_dataToPass.contains("OAUSD") || var_dataToPass.contains("IL2USD") || var_dataToPass.contains("TRAUSD") || var_dataToPass.contains("HLTUSD"))
				{
					//JavascriptExecutor jse = (JavascriptExecutor) driver; 
				 WebElement ClearUnitOfInsurance= driver.findElement(By.xpath("//input[@id='workbenchForm:workbenchTabs:unitsOfInsurance_input']"));
				 
				/// jse.executeScript("arguments[0].scrollIntoView(true);", ClearUnitOfInsurance);
				 //Thread.sleep(500);
							 ClearUnitOfInsurance.click();
							 takeScreenshot(); 
						     ClearUnitOfInsurance.sendKeys(Keys.BACK_SPACE);
						     ClearUnitOfInsurance.sendKeys(Keys.BACK_SPACE);
						     ClearUnitOfInsurance.sendKeys(Keys.BACK_SPACE);	
				             ClearUnitOfInsurance.sendKeys(Keys.BACK_SPACE);				
						     ClearUnitOfInsurance.sendKeys(""+var_UnitOfInsurance+"");
							 	 driver.findElement(By.xpath("//input[@id='workbenchForm:workbenchTabs:substandardRatiCode1']")).click();
								// Add Screenshot will only work for Android platform

		takeScreenshot();

								 Thread.sleep(1000);	
								 
								 if(var_dataToPass.contains("IL2USD"))
								 {
									 
									 driver.findElement(By.xpath("//table[@id='workbenchForm:workbenchTabs:deathBenefitOption']//tbody[1]/tr[1]//div[1]//div[2]//span[1]")).click();
									 Thread.sleep(1000);
									// Add Screenshot will only work for Android platform

		takeScreenshot();

								 }
				}
				
				 if(var_dataToPass.contains("UL1USD") | var_dataToPass.contains("UL2USD"))
				{
					
				WebElement requestedAnnualPremium= driver.findElement(By.xpath("//input[@id='workbenchForm:workbenchTabs:annualPremium_input']"));
							 					 
							 requestedAnnualPremium.click();
							 requestedAnnualPremium.sendKeys(Keys.BACK_SPACE);
							 requestedAnnualPremium.sendKeys(Keys.BACK_SPACE);
							 requestedAnnualPremium.sendKeys(Keys.BACK_SPACE);
							 requestedAnnualPremium.sendKeys(var_RequestedPremiumAmount);
							 Thread.sleep(1000);
							 // Add Screenshot will only work for Android platform

		takeScreenshot();

							//JavascriptExecutor jse = (JavascriptExecutor) driver;  
							//jse.executeScript("window.scrollBy(0,250)", "");
							WebElement ClearUnitOfInsurance= driver.findElement(By.xpath("//input[@id='workbenchForm:workbenchTabs:unitsOfInsurance_input']"));

						// jse.executeScript("arguments[0].scrollIntoView(true);", ClearUnitOfInsurance);
						// Thread.sleep(1000);
						 ClearUnitOfInsurance.click();
							 ClearUnitOfInsurance.sendKeys(Keys.BACK_SPACE);
						     ClearUnitOfInsurance.sendKeys(Keys.BACK_SPACE);
						     ClearUnitOfInsurance.sendKeys(Keys.BACK_SPACE);	
				             ClearUnitOfInsurance.sendKeys(Keys.BACK_SPACE);				
						     ClearUnitOfInsurance.sendKeys(""+var_UnitOfInsurance+"");
						//	 driver.findElement(By.xpath("//input[@id='workbenchForm:workbenchTabs:costBasis_input']")).click();
							
							 Thread.sleep(500);
							// Add Screenshot will only work for Android platform

		takeScreenshot();

							Thread.sleep(1000);

							WebElement faceAmount = driver.findElement(By.xpath("//table[@id='workbenchForm:workbenchTabs:deathBenefitOption']//tbody[1]/tr[1]//div[1]//div[2]//span[1]"));
									// jse.executeScript("arguments[0].scrollIntoView(true);", faceAmount);
									 
									 faceAmount.click();
									 Thread.sleep(1000);
									// Add Screenshot will only work for Android platform

		takeScreenshot();


						}
				
				 if(var_dataToPass.contains("FX3USD"))
				{
					JavascriptExecutor jse = (JavascriptExecutor) driver;  
							jse.executeScript("window.scrollBy(0,-250)", "");
							WebElement requestedAnnualPremium= driver.findElement(By.xpath("//input[@id='workbenchForm:workbenchTabs:annualPremium_input']"));
							 requestedAnnualPremium.click();
							 requestedAnnualPremium.sendKeys(Keys.BACK_SPACE);
							 requestedAnnualPremium.sendKeys(Keys.BACK_SPACE);
							 requestedAnnualPremium.sendKeys(Keys.BACK_SPACE);
							 requestedAnnualPremium.sendKeys(var_RequestedPremiumAmount);
							 driver.findElement(By.xpath("//input[@id='workbenchForm:workbenchTabs:preTEFRABasis_input']']")).click();
							 Thread.sleep(1000);
						// Add Screenshot will only work for Android platform

		takeScreenshot();

							 

				}
						
		 
		 ;} 
		 catch(Exception e) { PrintVal("Renderer Script Exception : "+e.toString()); } 
		 PrintVal("Executed Step = CALLRENDERERSCRIPT,SELECTBENEFITPLAN"); 
        SWIPE.$("DOWN","TGTYPESCREENREG");

		takeFullScreenShot();
                 LOGGER.info("Executed Step = TAKEFULLSCREENSHOT,TGTYPESCREENREG");
        TAP.$("ELE_BUTTON_VALIDATE","//span[contains(text(),'Validate')]","TGTYPESCREENREG");

        WAIT.$(1,"TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SAVE","//span[contains(text(),'Save')]","TGTYPESCREENREG");

        WAIT.$(4,"TGTYPESCREENREG");

        ASSERT.$("ELE_GETMSG_SUCCESS","//div[@id='workbenchForm:workbenchTabs:messages']//span[@class='ui-messages-info-summary']","CONTAINS","successfully","TGTYPESCREENREG");

        WAIT.$(3,"TGTYPESCREENREG");

		takeFullScreenShot();
                 LOGGER.info("Executed Step = TAKEFULLSCREENSHOT,TGTYPESCREENREG");
        TAP.$("ELE_TAB_CLOSE2","span.ui-icon.ui-icon-close:nth-child(2)TGWEBCOMMACssSelectorTGWEBCOMMA2","TGTYPESCREENREG");

        TAP.$("ELE_TAB_DISPLAYNEWPOLICYCONTRACT","//a[contains(text(),'Display')]","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SHOWLINK","//button[@id='workbenchForm:workbenchTabs:tb_buttonLink']","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

        TAP.$("ELE_LINK_PREMIUMS","//span[contains(text(),'Premiums')]","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

		takeFullScreenShot();
                 LOGGER.info("Executed Step = TAKEFULLSCREENSHOT,TGTYPESCREENREG");
        TAP.$("ELE_TAB_CLOSE2","span.ui-icon.ui-icon-close:nth-child(2)TGWEBCOMMACssSelectorTGWEBCOMMA2","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

		String var_EffectiveDate = "null";
                 LOGGER.info("Executed Step = VAR,String,var_EffectiveDate,null,TGTYPESCREENREG");
		var_EffectiveDate = getJsonData(var_CSVData , "$.records["+var_Count+"].EffectiveDate");
                 LOGGER.info("Executed Step = STORE,var_EffectiveDate,var_CSVData,$.records[{var_Count}].EffectiveDate,TGTYPESCREENREG");
        LOGGER.info("Executed Step = START IF");
        if (check(var_PolicyStatus,"=","Pending","TGTYPESCREENREG")) {
         LOGGER.info("Executed Step = IF,var_PolicyStatus,=,Pending,TGTYPESCREENREG");
        CALL.$("PolicyWithStatusPending","TGTYPESCREENREG");

        TAP.$("ELE_TAB_DISPLAYNEWPOLICYCONTRACT","//a[contains(text(),'Display')]","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

        ASSERT.$("ELE_GETVAL_PENDINGSTATUS","//span[contains(text(),'Pending')]","=","Pending","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

        TAP.$("ELE_TAB_CLOSE1","span.ui-icon.ui-icon-close:nth-child(2)TGWEBCOMMACssSelectorTGWEBCOMMA1","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

        TAP.$("ELE_TAB_CLOSE0","span.ui-icon.ui-icon-close:nth-child(2)TGWEBCOMMACssSelectorTGWEBCOMMA0","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

        }
          LOGGER.info("Executed Step = ENDIF,TGTYPESCREENREG");
        LOGGER.info("Executed Step = START IF");
        if (check(var_PolicyStatus,"=","Issue Not Paid","TGTYPESCREENREG")) {
         LOGGER.info("Executed Step = IF,var_PolicyStatus,=,Issue Not Paid,TGTYPESCREENREG");
        CALL.$("PolicyWithStatusIssueNotPaid","TGTYPESCREENREG");

        WAIT.$(1,"TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SHOWLINK","//button[@id='workbenchForm:workbenchTabs:tb_buttonLink']","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

        TAP.$("ELE_LINK_ISSUE","//span[@class='ui-menuitem-text'][contains(text(),'Issue')]","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

        TAP.$("ELE_TXTBOX_EFFECTIVEDATE","//input[@id='workbenchForm:workbenchTabs:effectiveDate_input']","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_EFFECTIVEDATE","//input[@id='workbenchForm:workbenchTabs:effectiveDate_input']",var_EffectiveDate,"TRUE","TGTYPESCREENREG");

		takeFullScreenShot();
                 LOGGER.info("Executed Step = TAKEFULLSCREENSHOT,TGTYPESCREENREG");
        TAP.$("ELE_BUTTON_SUBMIT","//span[contains(text(),'Submit')]","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        ASSERT.$("ELE_GETMSG_SUCCESS","//div[@id='workbenchForm:workbenchTabs:messages']//span[@class='ui-messages-info-summary']","CONTAINS","successfully","TGTYPESCREENREG");

        TAP.$("ELE_TAB_CLOSE2","span.ui-icon.ui-icon-close:nth-child(2)TGWEBCOMMACssSelectorTGWEBCOMMA2","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

        TAP.$("ELE_TAB_CLOSE1","span.ui-icon.ui-icon-close:nth-child(2)TGWEBCOMMACssSelectorTGWEBCOMMA1","TGTYPESCREENREG");

        WAIT.$(1,"TGTYPESCREENREG");

        TAP.$("ELE_TAB_CLOSE0","span.ui-icon.ui-icon-close:nth-child(2)TGWEBCOMMACssSelectorTGWEBCOMMA0","TGTYPESCREENREG");

        WAIT.$(1,"TGTYPESCREENREG");

        }
          LOGGER.info("Executed Step = ENDIF,TGTYPESCREENREG");
        LOGGER.info("Executed Step = START IF");
        if (check(var_PolicyStatus,"=","Settle","TGTYPESCREENREG")) {
         LOGGER.info("Executed Step = IF,var_PolicyStatus,=,Settle,TGTYPESCREENREG");
        WAIT.$(1,"TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SHOWLINK","//button[@id='workbenchForm:workbenchTabs:tb_buttonLink']","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

        TAP.$("ELE_LINK_ISSUE","//span[@class='ui-menuitem-text'][contains(text(),'Issue')]","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

        TAP.$("ELE_TXTBOX_EFFECTIVEDATE","//input[@id='workbenchForm:workbenchTabs:effectiveDate_input']","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_EFFECTIVEDATE","//input[@id='workbenchForm:workbenchTabs:effectiveDate_input']",var_EffectiveDate,"TRUE","TGTYPESCREENREG");

		takeFullScreenShot();
                 LOGGER.info("Executed Step = TAKEFULLSCREENSHOT,TGTYPESCREENREG");
        TAP.$("ELE_BUTTON_SUBMIT","//span[contains(text(),'Submit')]","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        ASSERT.$("ELE_GETMSG_SUCCESS","//div[@id='workbenchForm:workbenchTabs:messages']//span[@class='ui-messages-info-summary']","CONTAINS","successfully","TGTYPESCREENREG");

        TAP.$("ELE_TAB_CLOSE2","span.ui-icon.ui-icon-close:nth-child(2)TGWEBCOMMACssSelectorTGWEBCOMMA2","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

        CALL.$("PolicyWithStatusSettle","TGTYPESCREENREG");

		String var_RolloverType = "null";
                 LOGGER.info("Executed Step = VAR,String,var_RolloverType,null,TGTYPESCREENREG");
		String var_RolloverPremium = "null";
                 LOGGER.info("Executed Step = VAR,String,var_RolloverPremium,null,TGTYPESCREENREG");
		String var_PostTefraGainContri = "null";
                 LOGGER.info("Executed Step = VAR,String,var_PostTefraGainContri,null,TGTYPESCREENREG");
		String var_PostTefraBasisContri = "null";
                 LOGGER.info("Executed Step = VAR,String,var_PostTefraBasisContri,null,TGTYPESCREENREG");
		var_RolloverType = getJsonData(var_CSVData , "$.records["+var_Count+"].RolloverType");
                 LOGGER.info("Executed Step = STORE,var_RolloverType,var_CSVData,$.records[{var_Count}].RolloverType,TGTYPESCREENREG");
		var_RolloverPremium = getJsonData(var_CSVData , "$.records["+var_Count+"].RolloverPremium");
                 LOGGER.info("Executed Step = STORE,var_RolloverPremium,var_CSVData,$.records[{var_Count}].RolloverPremium,TGTYPESCREENREG");
		var_PostTefraBasisContri = getJsonData(var_CSVData , "$.records["+var_Count+"].PostTEFRABasisContribution");
                 LOGGER.info("Executed Step = STORE,var_PostTefraBasisContri,var_CSVData,$.records[{var_Count}].PostTEFRABasisContribution,TGTYPESCREENREG");
		var_PostTefraGainContri = getJsonData(var_CSVData , "$.records["+var_Count+"].PostTEFRAGainContribution");
                 LOGGER.info("Executed Step = STORE,var_PostTefraGainContri,var_CSVData,$.records[{var_Count}].PostTEFRAGainContribution,TGTYPESCREENREG");
        TAP.$("ELE_TAB_DISPLAYNEWPOLICYCONTRACT","//a[contains(text(),'Display')]","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SHOWLINK","//button[@id='workbenchForm:workbenchTabs:tb_buttonLink']","TGTYPESCREENREG");

        TAP.$("ELE_LINK_SETTLE","//span[contains(text(),'Settle')]","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

        TAP.$("ELE_TAB_UPDATESTATUSCHANGE","//a[contains(text(),'Update Status Change')]","TGTYPESCREENREG");

		try { 
		 Thread.sleep(500);
		if(!var_RolloverType.contains("No Rollover")) {
						if(var_dataToPass.contains("FX")) 
							{
								System.out.println("FXXXX");
								JavascriptExecutor jse = (JavascriptExecutor) driver;  
								jse.executeScript("window.scrollBy(0,-500)", "");
								
								Thread.sleep(1000);
								driver.findElement(By.xpath("//label[@id='workbenchForm:workbenchTabs:rolloverType_label']")).click();
								Thread.sleep(1000);
								WebElement selectValueOfDropDown= driver.findElement(By.xpath("//li[@data-label='"+ var_RolloverType +"']"));				 		
								selectValueOfDropDown.click();
					
								 Thread.sleep(1000);
								 
		driver.findElement(By.xpath("//input[@id='workbenchForm:workbenchTabs:rolloverPremium_input']")).click();
		 driver.findElement(By.xpath("//input[@id='workbenchForm:workbenchTabs:rolloverPremium_input']")).sendKeys(Keys.BACK_SPACE);						   driver.findElement(By.xpath("//input[@id='workbenchForm:workbenchTabs:rolloverPremium_input']")).sendKeys(Keys.BACK_SPACE);						    driver.findElement(By.xpath("//input[@id='workbenchForm:workbenchTabs:rolloverPremium_input']")).sendKeys(Keys.BACK_SPACE);							driver.findElement(By.xpath("//input[@id='workbenchForm:workbenchTabs:rolloverPremium_input']")).sendKeys(var_RolloverPremium);
								 // Add Screenshot will only work for Android platform

				takeScreenshot();

								 Thread.sleep(1000);
								 if(!driver.findElements(By.xpath("//input[@id='workbenchForm:workbenchTabs:rolloverPostTefraBasisContribution_input']")).isEmpty())
								 {
		driver.findElement(By.xpath("//input[@id='workbenchForm:workbenchTabs:rolloverPostTefraBasisContribution_input']")).click();						 driver.findElement(By.xpath("//input[@id='workbenchForm:workbenchTabs:rolloverPostTefraBasisContribution_input']")).sendKeys(Keys.BACK_SPACE);						 driver.findElement(By.xpath("//input[@id='workbenchForm:workbenchTabs:rolloverPostTefraBasisContribution_input']")).sendKeys(Keys.BACK_SPACE);						driver.findElement(By.xpath("//input[@id='workbenchForm:workbenchTabs:rolloverPostTefraBasisContribution_input']")).sendKeys(Keys.BACK_SPACE);						driver.findElement(By.xpath("//input[@id='workbenchForm:workbenchTabs:rolloverPostTefraBasisContribution_input']")).sendKeys(var_PostTefraBasisContri);
								
								 Thread.sleep(1000);

		driver.findElement(By.xpath("//input[@id='workbenchForm:workbenchTabs:rolloverPostTefraGainContribution_input']")).click();						  driver.findElement(By.xpath("//input[@id='workbenchForm:workbenchTabs:rolloverPostTefraGainContribution_input']")).sendKeys(Keys.BACK_SPACE);
								   driver.findElement(By.xpath("//input[@id='workbenchForm:workbenchTabs:rolloverPostTefraGainContribution_input']")).sendKeys(Keys.BACK_SPACE);
								    driver.findElement(By.xpath("//input[@id='workbenchForm:workbenchTabs:rolloverPostTefraGainContribution_input']")).sendKeys(Keys.BACK_SPACE);
									 driver.findElement(By.xpath("//input[@id='workbenchForm:workbenchTabs:rolloverPostTefraGainContribution_input']")).sendKeys(var_PostTefraGainContri);
								// Add Screenshot will only work for Android platform

			// Add Screenshot will only work for Android platform

		takeScreenshot();


								 Thread.sleep(1000);
								 
								 int var_RolloverPremiumActual;
								 
								 var_RolloverPremiumActual = Integer.parseInt(var_PostTefraBasisContri) + Integer.parseInt(var_PostTefraGainContri);
								 Assert.assertTrue(Integer.toString(var_RolloverPremiumActual).equals(var_RolloverPremium));
								 }
								 
							
							}
								 
									 
				if(var_dataToPass.contains("OA") ||  var_dataToPass.contains("UL") || var_dataToPass.contains("IL") || var_dataToPass.contains("OL"))

							{
								
								System.out.println("BENEFIT PLAN IS "+var_dataToPass);
								JavascriptExecutor jse = (JavascriptExecutor) driver;  
									jse.executeScript("window.scrollBy(0,-500)", "");
								
								driver.findElement(By.xpath("//label[@id='workbenchForm:workbenchTabs:rolloverType_label']")).click();
								Thread.sleep(1000);
								WebElement selectValueOfDropDown= driver.findElement(By.xpath("//li[@data-label='"+ var_RolloverType +"']"));
						 		
								selectValueOfDropDown.click();
						// Add Screenshot will only work for Android platform

				
								 Thread.sleep(1000);
								 
		driver.findElement(By.xpath("//input[@id='workbenchForm:workbenchTabs:rolloverPremium_input']")).click();
		driver.findElement(By.xpath("//input[@id='workbenchForm:workbenchTabs:rolloverPremium_input']")).sendKeys(Keys.BACK_SPACE);						   driver.findElement(By.xpath("//input[@id='workbenchForm:workbenchTabs:rolloverPremium_input']")).sendKeys(Keys.BACK_SPACE);						    driver.findElement(By.xpath("//input[@id='workbenchForm:workbenchTabs:rolloverPremium_input']")).sendKeys(Keys.BACK_SPACE);							driver.findElement(By.xpath("//input[@id='workbenchForm:workbenchTabs:rolloverPremium_input']")).sendKeys(var_RolloverPremium);
								// Add Screenshot will only work for Android platform

				// Add Screenshot will only work for Android platform

		takeScreenshot();


		Thread.sleep(1000);
		driver.findElement(By.xpath("//input[@id='workbenchForm:workbenchTabs:rolloverBasisContribution_input']")).click();						 driver.findElement(By.xpath("//input[@id='workbenchForm:workbenchTabs:rolloverBasisContribution_input']")).sendKeys(Keys.BACK_SPACE);						 driver.findElement(By.xpath("//input[@id='workbenchForm:workbenchTabs:rolloverBasisContribution_input']")).sendKeys(Keys.BACK_SPACE);						driver.findElement(By.xpath("//input[@id='workbenchForm:workbenchTabs:rolloverBasisContribution_input']")).sendKeys(Keys.BACK_SPACE);						driver.findElement(By.xpath("//input[@id='workbenchForm:workbenchTabs:rolloverBasisContribution_input']")).sendKeys(var_PostTefraBasisContri);


		driver.findElement(By.xpath("//input[@id='workbenchForm:workbenchTabs:rolloverGainContribution_input']")).click();						  driver.findElement(By.xpath("//input[@id='workbenchForm:workbenchTabs:rolloverGainContribution_input']")).sendKeys(Keys.BACK_SPACE);						   driver.findElement(By.xpath("//input[@id='workbenchForm:workbenchTabs:rolloverGainContribution_input']")).sendKeys(Keys.BACK_SPACE);						    driver.findElement(By.xpath("//input[@id='workbenchForm:workbenchTabs:rolloverGainContribution_input']")).sendKeys(Keys.BACK_SPACE);							 driver.findElement(By.xpath("//input[@id='workbenchForm:workbenchTabs:rolloverGainContribution_input']")).sendKeys(var_PostTefraGainContri);
		Thread.sleep(1000);
							
								
		 int var_RolloverPremiumActual;
		 var_RolloverPremiumActual = Integer.parseInt(var_PostTefraBasisContri) + Integer.parseInt(var_PostTefraGainContri);
		 Assert.assertTrue(Integer.toString(var_RolloverPremiumActual).equals(var_RolloverPremium));

									
							}
				}		
				 
		 
		 ;} 
		 catch(Exception e) { PrintVal("Renderer Script Exception : "+e.toString()); } 
		 PrintVal("Executed Step = CALLRENDERERSCRIPT,ROLLOVERCONDITION"); 
		takeFullScreenShot();
                 LOGGER.info("Executed Step = TAKEFULLSCREENSHOT,TGTYPESCREENREG");
        TAP.$("ELE_BUTTON_VALIDATE","//span[contains(text(),'Validate')]","TGTYPESCREENREG");

        WAIT.$(4,"TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SUBMIT","//span[contains(text(),'Submit')]","TGTYPESCREENREG");

        WAIT.$(9,"TGTYPESCREENREG");

        ASSERT.$("ELE_GETMSG_SUCCESS","//div[@id='workbenchForm:workbenchTabs:messages']//span[@class='ui-messages-info-summary']","CONTAINS","successfully","TGTYPESCREENREG");

        WAIT.$(1,"TGTYPESCREENREG");

        TAP.$("ELE_TAB_CLOSE2","span.ui-icon.ui-icon-close:nth-child(2)TGWEBCOMMACssSelectorTGWEBCOMMA2","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

        TAP.$("ELE_TAB_CLOSE1","span.ui-icon.ui-icon-close:nth-child(2)TGWEBCOMMACssSelectorTGWEBCOMMA1","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

        TAP.$("ELE_TAB_CLOSE0","span.ui-icon.ui-icon-close:nth-child(2)TGWEBCOMMACssSelectorTGWEBCOMMA0","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

        }
          LOGGER.info("Executed Step = ENDIF,TGTYPESCREENREG");
        CALL.$("PolicyInquiryAfterPolicyCreation","TGTYPESCREENREG");

        TAP.$("ELE_TAB_MYDASHBOARD","//a[contains(text(),'My Dashboard')]","TGTYPESCREENREG");

        WAIT.$(1,"TGTYPESCREENREG");

        TAP.$("ELE_TXTBOX_SEARCHTASKS","//input[@id='workbenchForm:workbenchTabs:taskSearchContainer:taskSearchSearch']","TGTYPESCREENREG");

		try { 
		 driver.findElement(By.xpath("//input[@id='workbenchForm:workbenchTabs:taskSearchContainer:taskSearchSearch']")).clear();
		 
		 ;} 
		 catch(Exception e) { PrintVal("Renderer Script Exception : "+e.toString()); } 
		 PrintVal("Executed Step = CALLRENDERERSCRIPT,CLEARTASKSEARCHFIELD"); 
        WAIT.$(1,"TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_SEARCHTASKS","//input[@id='workbenchForm:workbenchTabs:taskSearchContainer:taskSearchSearch']","Policy Inquiry","FALSE","TGTYPESCREENREG");

        WAIT.$(1,"TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_TASKSEARCH","//a[@id='workbenchForm:workbenchTabs:taskSearchContainer:tb_buttonSearch']//span[@class='fa fa-search fa-lg']","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

        TAP.$("ELE_LINK_POLICYINQUIRY","//span[contains(text(),'Policy Inquiry')]","TGTYPESCREENREG");

        TAP.$("ELE_TAB_ENTERPOLICYNUMBER","//a[contains(text(),'Enter Policy Number')]","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_POLICYNUMBER","//input[@id='workbenchForm:workbenchTabs:policyNumber']",var_PolicyNumber,"FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_CONTINUE","//span[contains(text(),'Continue')]","TGTYPESCREENREG");

        WAIT.$(3,"TGTYPESCREENREG");

        LOGGER.info("Executed Step = START IF");
        if (check(var_PolicyStatus,"=","Settle","TGTYPESCREENREG")) {
         LOGGER.info("Executed Step = IF,var_PolicyStatus,=,Settle,TGTYPESCREENREG");
		writeToCSV("Policy Number " , ActionWrapper.getWebElementValueForVariable("span[id='workbenchForm:workbenchTabs:policyNumber']", "CssSelector", 0, "span[id='workbenchForm:workbenchTabs:policyNumber']TGWEBCOMMACssSelectorTGWEBCOMMA0"));
                 LOGGER.info("Executed Step = WRITETOCSV,ELE_GETVAL_POLICYNUMBER,Policy Number,TGTYPESCREENREG");
		writeToCSV ("Policy Status " , ActionWrapper.getElementValue("ELE_GETVAL_POLICYSTATUS", "//span[@id='workbenchForm:workbenchTabs:statusText']"));
                 LOGGER.info("Executed Step = WRITETOCSV,ELE_GETVAL_POLICYSTATUS,Policy Status,TGTYPESCREENREG");
		writeToCSV ("Client Name " , ActionWrapper.getElementValue("ELE_GETVAL_CLIENTNAME", "//span[@id='workbenchForm:workbenchTabs:ownerName']"));
                 LOGGER.info("Executed Step = WRITETOCSV,ELE_GETVAL_CLIENTNAME,Client Name,TGTYPESCREENREG");
        SCROLL.$("ELE_GETVAL_BENEFITPLAN","//span[@id='workbenchForm:workbenchTabs:benefitTable:0:planCodeText']","DOWN","TGTYPESCREENREG");

		writeToCSV ("Suspense Balance " , ActionWrapper.getElementValue("ELE_GETVAL_SUSPENSEBALANCE", "//span[@id='workbenchForm:workbenchTabs:suspenseBalance']"));
                 LOGGER.info("Executed Step = WRITETOCSV,ELE_GETVAL_SUSPENSEBALANCE,Suspense Balance,TGTYPESCREENREG");
		writeToCSV ("Benefit Plan " , ActionWrapper.getElementValue("ELE_GETVAL_BENEFITPLAN", "//span[@id='workbenchForm:workbenchTabs:benefitTable:0:planCodeText']"));
                 LOGGER.info("Executed Step = WRITETOCSV,ELE_GETVAL_BENEFITPLAN,Benefit Plan,TGTYPESCREENREG");
		takeFullScreenShot();
                 LOGGER.info("Executed Step = TAKEFULLSCREENSHOT,TGTYPESCREENREG");
        }
         else {
          LOGGER.info("Executed Step = ELSE,TGTYPESCREENREG");
		writeToCSV("Policy Number " , ActionWrapper.getWebElementValueForVariable("span[id='workbenchForm:workbenchTabs:policyNumber']", "CssSelector", 0, "span[id='workbenchForm:workbenchTabs:policyNumber']TGWEBCOMMACssSelectorTGWEBCOMMA0"));
                 LOGGER.info("Executed Step = WRITETOCSV,ELE_GETVAL_POLICYNUMBER,Policy Number,TGTYPESCREENREG");
		writeToCSV ("Policy Status " , ActionWrapper.getElementValue("ELE_GETVAL_POLICYSTATUSPENDINGISSUE", "//span[@id='workbenchForm:workbenchTabs:contractStatusNewBusText']"));
                 LOGGER.info("Executed Step = WRITETOCSV,ELE_GETVAL_POLICYSTATUSPENDINGISSUE,Policy Status,TGTYPESCREENREG");
		takeFullScreenShot();
                 LOGGER.info("Executed Step = TAKEFULLSCREENSHOT,TGTYPESCREENREG");
        WAIT.$(3,"TGTYPESCREENREG");

        TAP.$("ELE_TAB_CLOSE1","span.ui-icon.ui-icon-close:nth-child(2)TGWEBCOMMACssSelectorTGWEBCOMMA1","TGTYPESCREENREG");

        WAIT.$(1,"TGTYPESCREENREG");

        TAP.$("ELE_TAB_CLOSE0","span.ui-icon.ui-icon-close:nth-child(2)TGWEBCOMMACssSelectorTGWEBCOMMA0","TGTYPESCREENREG");

        WAIT.$(1,"TGTYPESCREENREG");

        }
          LOGGER.info("Executed Step = ENDIF,TGTYPESCREENREG");
        }
          LOGGER.info("Executed Step = ENDIF,TGTYPESCREENREG");
		var_Count = var_Count + ( 1 );
                 LOGGER.info("Executed Step = INCREASEVAR,var_Count,1,TGTYPESCREENREG");
    	}
          LOGGER.info("Executed Step = WEND,TGTYPESCREENREG");
    	resetAttemps();

    }


    @Test
    public void createapolicyshort() {

        // No need to declare / print method name here, handled by TestNg listener
        // declare("createapolicyshort");

		com.jayway.jsonpath.ReadContext var_CSVData = getJsonData("C:/GIAS_Automation/4.0 MassMutual/InputData/MassMutual40_IP_Reg1to3n8to14n19_CreatePolicy.csv");
                 LOGGER.info("Executed Step = VAR,Json,var_CSVData,C:/GIAS_Automation/4.0 MassMutual/InputData/MassMutual40_IP_Reg1to3n8to14n19_CreatePolicy.csv,TGTYPESCREENREG");
		String var_SessionDate = "null";
                 LOGGER.info("Executed Step = VAR,String,var_SessionDate,null,TGTYPESCREENREG");
		String var_SessionDate1 = "null";
                 LOGGER.info("Executed Step = VAR,String,var_SessionDate1,null,TGTYPESCREENREG");
		int var_Count = 0;
                 LOGGER.info("Executed Step = VAR,Integer,var_Count,0,TGTYPESCREENREG");
		String var_dataToPass = "null";
                 LOGGER.info("Executed Step = VAR,String,var_dataToPass,null,TGTYPESCREENREG");
		String var_TaxQualifiedDescription = "null";
                 LOGGER.info("Executed Step = VAR,String,var_TaxQualifiedDescription,null,TGTYPESCREENREG");
		String var_CashWithApplication = "null";
                 LOGGER.info("Executed Step = VAR,String,var_CashWithApplication,null,TGTYPESCREENREG");
		String var_TaxWithholding = "null";
                 LOGGER.info("Executed Step = VAR,String,var_TaxWithholding,null,TGTYPESCREENREG");
		String var_SSNNumber = "null";
                 LOGGER.info("Executed Step = VAR,String,var_SSNNumber,null,TGTYPESCREENREG");
		String var_DateOfBirth = "null";
                 LOGGER.info("Executed Step = VAR,String,var_DateOfBirth,null,TGTYPESCREENREG");
		String var_EffectiveDate;
                 LOGGER.info("Executed Step = VAR,String,var_EffectiveDate,TGTYPESCREENREG");
		var_EffectiveDate = getJsonData(var_CSVData , "$.records["+var_Count+"].EffectiveDate");
                 LOGGER.info("Executed Step = STORE,var_EffectiveDate,var_CSVData,$.records[{var_Count}].EffectiveDate,TGTYPESCREENREG");
        CALL.$("LoginToGIAS4","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENNO");

        TYPE.$("ELE_TXTBOX_USERNAME","//input[@id='guestForm:username']","Mano","FALSE","TGTYPESCREENNO");

        TYPE.$("ELE_TXTBOX_PASSWORD","//input[@id='guestForm:password']","Cnx12345!","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_LOGIN","//button[@id='guestForm:loginButton']","TGTYPESCREENNO");

        LOGGER.info("Executed Step = REPEAT IF");
    	while (check(var_Count,"<",1,"TGTYPESCREENREG")) {
          LOGGER.info("Executed Step = WHILE,var_Count,<,1,TGTYPESCREENREG");
		var_SessionDate = getJsonData(var_CSVData , "$.records["+var_Count+"].SessionDate");
                 LOGGER.info("Executed Step = STORE,var_SessionDate,var_CSVData,$.records[{var_Count}].SessionDate,TGTYPESCREENREG");
		var_TaxQualifiedDescription = getJsonData(var_CSVData , "$.records["+var_Count+"].TaxQualifiedDescription");
                 LOGGER.info("Executed Step = STORE,var_TaxQualifiedDescription,var_CSVData,$.records[{var_Count}].TaxQualifiedDescription,TGTYPESCREENREG");
		var_CashWithApplication = getJsonData(var_CSVData , "$.records["+var_Count+"].CashWithApplication");
                 LOGGER.info("Executed Step = STORE,var_CashWithApplication,var_CSVData,$.records[{var_Count}].CashWithApplication,TGTYPESCREENREG");
		var_TaxWithholding = getJsonData(var_CSVData , "$.records["+var_Count+"].TaxWithholding");
                 LOGGER.info("Executed Step = STORE,var_TaxWithholding,var_CSVData,$.records[{var_Count}].TaxWithholding,TGTYPESCREENREG");
		var_SSNNumber = getJsonData(var_CSVData , "$.records["+var_Count+"].SocialSecurityNumber");
                 LOGGER.info("Executed Step = STORE,var_SSNNumber,var_CSVData,$.records[{var_Count}].SocialSecurityNumber,TGTYPESCREENREG");
		var_DateOfBirth = getJsonData(var_CSVData , "$.records["+var_Count+"].ClientDOB");
                 LOGGER.info("Executed Step = STORE,var_DateOfBirth,var_CSVData,$.records[{var_Count}].ClientDOB,TGTYPESCREENREG");
        CALL.$("ChangeSessionDate","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SESSIONDATE","//a[@id='headerForm:sessionDatelink']","TGTYPESCREENNO");

        WAIT.$(2,"TGTYPESCREENNO");

        TYPE.$("ELE_TXTBOX_NEWSESSIONDATE","//input[@id='sessionDateForm:sessionDateInput_input']",var_SessionDate,"FALSE","TGTYPESCREENNO");

		takeFullScreenShot();
                 LOGGER.info("Executed Step = TAKEFULLSCREENSHOT,TGTYPESCREENREG");
        TAP.$("ELE_BUTTON_CHANGEDATE","//span[text()='Change Date']","TGTYPESCREENNO");

        WAIT.$(2,"TGTYPESCREENNO");

        CALL.$("AddPolicyContract","TGTYPESCREENREG");

		String var_ListBillNumber;
                 LOGGER.info("Executed Step = VAR,String,var_ListBillNumber,TGTYPESCREENNO");
		var_ListBillNumber = getJsonData(var_CSVData , "$.records["+var_Count+"].ListBillNumber");
                 LOGGER.info("Executed Step = STORE,var_ListBillNumber,var_CSVData,$.records[{var_Count}].ListBillNumber,TGTYPESCREENNO");
		String var_AgentNumber;
                 LOGGER.info("Executed Step = VAR,String,var_AgentNumber,TGTYPESCREENNO");
		var_AgentNumber = getJsonData(var_CSVData , "$.records["+var_Count+"].AgentNumber");
                 LOGGER.info("Executed Step = STORE,var_AgentNumber,var_CSVData,$.records[{var_Count}].AgentNumber,TGTYPESCREENNO");
        TAP.$("ELE_TXTBOX_SEARCHTASKS","//input[@id='workbenchForm:workbenchTabs:taskSearchContainer:taskSearchSearch']","TGTYPESCREENNO");

        TYPE.$("ELE_TXTBOX_SEARCHTASKS","//input[@id='workbenchForm:workbenchTabs:taskSearchContainer:taskSearchSearch']","New Policy Application","FALSE","TGTYPESCREENNO");

        TAP.$("ELE_BUTTON_TASKSEARCH","//a[@id='workbenchForm:workbenchTabs:taskSearchContainer:tb_buttonSearch']//span[@class='fa fa-search fa-lg']","TGTYPESCREENNO");

        WAIT.$(2,"TGTYPESCREENNO");

        TAP.$("ELE_LINK_NEWPOLICYAPPLICATION","//span[contains(text(),'New Policy Application')]","TGTYPESCREENNO");

        WAIT.$(3,"TGTYPESCREENNO");

        TAP.$("ELE_TAB_APPLICATIONENTRYUPDATE","//a[contains(text(),'Application Entry/Update')]","TGTYPESCREENNO");

        TAP.$("ELE_BUTTON_ADD","//button[@id='workbenchForm:workbenchTabs:tb_buttonAdd']//span[@class='ui-button-text ui-c'][contains(text(),'Add')]","TGTYPESCREENNO");

		var_dataToPass = getJsonData(var_CSVData , "$.records["+var_Count+"].PaymentFrequency");
                 LOGGER.info("Executed Step = STORE,var_dataToPass,var_CSVData,$.records[{var_Count}].PaymentFrequency,TGTYPESCREENNO");
        TAP.$("ELE_DROPDWN_PAYFREQ","//label[@id='workbenchForm:workbenchTabs:paymentMode_label']","TGTYPESCREENNO");

        WAIT.$(2,"TGTYPESCREENNO");

		try { 
		 Thread.sleep(1000);
		WebElement selectValueOfDropDown= driver.findElement(By.xpath("//li[@data-label='"+ var_dataToPass +"']"));

				selectValueOfDropDown.click();
		// Add Screenshot will only work for Android platform

				 Thread.sleep(1000);
		 
		 ;} 
		 catch(Exception e) { PrintVal("Renderer Script Exception : "+e.toString()); } 
		 PrintVal("Executed Step = CALLRENDERERSCRIPT,SELECTVALUEFROMDROPDOWN"); 
		var_dataToPass = getJsonData(var_CSVData , "$.records["+var_Count+"].PaymentMethod");
                 LOGGER.info("Executed Step = STORE,var_dataToPass,var_CSVData,$.records[{var_Count}].PaymentMethod,TGTYPESCREENNO");
        TAP.$("ELE_DRPDWN_PAYMETHOD","//label[@id='workbenchForm:workbenchTabs:paymentCode_label']","TGTYPESCREENNO");

        WAIT.$(2,"TGTYPESCREENNO");

		try { 
		 Thread.sleep(1000);
		WebElement selectValueOfDropDown= driver.findElement(By.xpath("//li[@data-label='"+ var_dataToPass +"']"));

				selectValueOfDropDown.click();
		// Add Screenshot will only work for Android platform

				 Thread.sleep(1000);
		 
		 ;} 
		 catch(Exception e) { PrintVal("Renderer Script Exception : "+e.toString()); } 
		 PrintVal("Executed Step = CALLRENDERERSCRIPT,SELECTVALUEFROMDROPDOWN"); 
		var_dataToPass = getJsonData(var_CSVData , "$.records["+var_Count+"].IssueState");
                 LOGGER.info("Executed Step = STORE,var_dataToPass,var_CSVData,$.records[{var_Count}].IssueState,TGTYPESCREENNO");
        TAP.$("ELE_DRPDWN_ISSUESTATECOUNTRY","//label[@id='workbenchForm:workbenchTabs:countryAndStateCode_label']","TGTYPESCREENNO");

        WAIT.$(2,"TGTYPESCREENNO");

		try { 
		 Thread.sleep(1000);
		WebElement selectValueOfDropDown= driver.findElement(By.xpath("//li[@data-label='"+ var_dataToPass +"']"));

				selectValueOfDropDown.click();
		// Add Screenshot will only work for Android platform

				 Thread.sleep(1000);
		 
		 ;} 
		 catch(Exception e) { PrintVal("Renderer Script Exception : "+e.toString()); } 
		 PrintVal("Executed Step = CALLRENDERERSCRIPT,SELECTVALUEFROMDROPDOWN"); 
		var_dataToPass = var_TaxQualifiedDescription;
                 LOGGER.info("Executed Step = STORE,var_dataToPass,var_TaxQualifiedDescription,TGTYPESCREENNO");
        TAP.$("ELE_DRPDWN_TAXQUALIFIEDDESP","//label[@id='workbenchForm:workbenchTabs:taxQualifiedCode_label']","TGTYPESCREENNO");

        WAIT.$(2,"TGTYPESCREENNO");

		try { 
		 Thread.sleep(1000);
		WebElement selectValueOfDropDown= driver.findElement(By.xpath("//li[@data-label='"+ var_dataToPass +"']"));

				selectValueOfDropDown.click();
		// Add Screenshot will only work for Android platform

				 Thread.sleep(1000);
		 
		 ;} 
		 catch(Exception e) { PrintVal("Renderer Script Exception : "+e.toString()); } 
		 PrintVal("Executed Step = CALLRENDERERSCRIPT,SELECTVALUEFROMDROPDOWN"); 
		var_dataToPass = var_TaxWithholding;
                 LOGGER.info("Executed Step = STORE,var_dataToPass,var_TaxWithholding,TGTYPESCREENNO");
        TAP.$("ELE_DRPDWN_TAXWITHOLDING","//label[@id='workbenchForm:workbenchTabs:taxWithholdingCode_label']","TGTYPESCREENNO");

        WAIT.$(2,"TGTYPESCREENNO");

		try { 
		 Thread.sleep(1000);
		WebElement selectValueOfDropDown= driver.findElement(By.xpath("//li[@data-label='"+ var_dataToPass +"']"));

				selectValueOfDropDown.click();
		// Add Screenshot will only work for Android platform

				 Thread.sleep(1000);
		 
		 ;} 
		 catch(Exception e) { PrintVal("Renderer Script Exception : "+e.toString()); } 
		 PrintVal("Executed Step = CALLRENDERERSCRIPT,SELECTVALUEFROMDROPDOWN"); 
        WAIT.$(1,"TGTYPESCREENNO");

        TYPE.$("ELE_TXTBOX_LISTBILLNUMBER","//input[@id='workbenchForm:workbenchTabs:groupNumber']",var_ListBillNumber,"FALSE","TGTYPESCREENNO");

        SWIPE.$("UP","TGTYPESCREENNO");

        WAIT.$(1,"TGTYPESCREENNO");

        TAP.$("ELE_TXTBOX_EFFECTIVEDATE","//input[@id='workbenchForm:workbenchTabs:effectiveDate_input']","TGTYPESCREENNO");

        TYPE.$("ELE_TXTBOX_EFFECTIVEDATE","//input[@id='workbenchForm:workbenchTabs:effectiveDate_input']",var_EffectiveDate,"FALSE","TGTYPESCREENNO");

        WAIT.$(1,"TGTYPESCREENNO");

        TAP.$("ELE_TXTBOX_CASHWITHAPP","//input[@id='workbenchForm:workbenchTabs:cashWithApplication_input']","TGTYPESCREENNO");

        WAIT.$(2,"TGTYPESCREENNO");

		try { 
		 WebElement CashApplicationTextField = driver.findElement(By.xpath("//input[@id='workbenchForm:workbenchTabs:cashWithApplication_input']"));
		CashApplicationTextField.sendKeys(Keys.BACK_SPACE);
				CashApplicationTextField.sendKeys(Keys.BACK_SPACE);
				CashApplicationTextField.sendKeys(Keys.BACK_SPACE);	
		// Add Screenshot will only work for Android platform

				CashApplicationTextField.sendKeys(""+var_CashWithApplication+"");// Add Screenshot will only work for Android platform

		Thread.sleep(1000);
		 
		 ;} 
		 catch(Exception e) { PrintVal("Renderer Script Exception : "+e.toString()); } 
		 PrintVal("Executed Step = CALLRENDERERSCRIPT,CLEARCASHWITHAPPLICATIONTXTFIELD"); 
        SWIPE.$("UP","TGTYPESCREENNO");

        SWIPE.$("UP","TGTYPESCREENNO");

        TAP.$("ELE_TAP_AGENTNUMBERBOX","//div[@id='workbenchForm:workbenchTabs:grid']//tr[1]//td[1]","TGTYPESCREENNO");

        WAIT.$(1,"TGTYPESCREENNO");

        TYPE.$("ELE_TXTBOX_AGENTNUM","//input[@id='workbenchForm:workbenchTabs:grid:0:in_agentNumber_Col']",var_AgentNumber,"FALSE","TGTYPESCREENNO");

        WAIT.$(1,"TGTYPESCREENNO");

        TAP.$("ELE_TAP_AGENTNUMBER","//th[@id='workbenchForm:workbenchTabs:grid:agentNumber_Col_Col']//span","TGTYPESCREENNO");

		takeFullScreenShot();
                 LOGGER.info("Executed Step = TAKEFULLSCREENSHOT,TGTYPESCREENREG");
        CALL.$("AddClient","TGTYPESCREENREG");

		String var_FirstName = "null";
                 LOGGER.info("Executed Step = VAR,String,var_FirstName,null,TGTYPESCREENNO");
		String var_LastName = "null";
                 LOGGER.info("Executed Step = VAR,String,var_LastName,null,TGTYPESCREENNO");
		String var_SocialSecurityNumber = "null";
                 LOGGER.info("Executed Step = VAR,String,var_SocialSecurityNumber,null,TGTYPESCREENNO");
		String var_stateToPass = "null";
                 LOGGER.info("Executed Step = VAR,String,var_stateToPass,null,TGTYPESCREENNO");
		var_LastName = getJsonData(var_CSVData , "$.records["+var_Count+"].LastName");
                 LOGGER.info("Executed Step = STORE,var_LastName,var_CSVData,$.records[{var_Count}].LastName,TGTYPESCREENNO");
		var_FirstName = getJsonData(var_CSVData , "$.records["+var_Count+"].FirstName");
                 LOGGER.info("Executed Step = STORE,var_FirstName,var_CSVData,$.records[{var_Count}].FirstName,TGTYPESCREENNO");
		var_SocialSecurityNumber = getJsonData(var_CSVData , "$.records["+var_Count+"].SocialSecurityNumber");
                 LOGGER.info("Executed Step = STORE,var_SocialSecurityNumber,var_CSVData,$.records[{var_Count}].SocialSecurityNumber,TGTYPESCREENNO");
		String var_ClientNameSearch;
                 LOGGER.info("Executed Step = VAR,String,var_ClientNameSearch,TGTYPESCREENNO");
		var_ClientNameSearch = getJsonData(var_CSVData , "$.records["+var_Count+"].ClientNameSearch");
                 LOGGER.info("Executed Step = STORE,var_ClientNameSearch,var_CSVData,$.records[{var_Count}].ClientNameSearch,TGTYPESCREENNO");
		String var_ClientID;
                 LOGGER.info("Executed Step = VAR,String,var_ClientID,TGTYPESCREENNO");
		var_ClientID = getJsonData(var_CSVData , "$.records["+var_Count+"].ClientID");
                 LOGGER.info("Executed Step = STORE,var_ClientID,var_CSVData,$.records[{var_Count}].ClientID,TGTYPESCREENNO");
		String var_ClientBirthDate;
                 LOGGER.info("Executed Step = VAR,String,var_ClientBirthDate,TGTYPESCREENNO");
		var_ClientBirthDate = getJsonData(var_CSVData , "$.records["+var_Count+"].ClientDOB");
                 LOGGER.info("Executed Step = STORE,var_ClientBirthDate,var_CSVData,$.records[{var_Count}].ClientDOB,TGTYPESCREENNO");
		String var_Gender;
                 LOGGER.info("Executed Step = VAR,String,var_Gender,TGTYPESCREENREG");
		var_Gender = getJsonData(var_CSVData , "$.records["+var_Count+"].Gender");
                 LOGGER.info("Executed Step = STORE,var_Gender,var_CSVData,$.records[{var_Count}].Gender,TGTYPESCREENREG");
        TAP.$("ELE_BUTTON_SHOWLINK","//button[@id='workbenchForm:workbenchTabs:tb_buttonLink']","TGTYPESCREENREG");

        WAIT.$(1,"TGTYPESCREENNO");

        TAP.$("ELE_LINK_SELECTCLIENT","//span[contains(text(),'Select Client')]","TGTYPESCREENNO");

        WAIT.$(2,"TGTYPESCREENNO");

        TYPE.$("ELE_TXTBOX_SEARCH","//input[@id='workbenchForm:workbenchTabs:searchName']",var_ClientNameSearch,"FALSE","TGTYPESCREENNO");

        TAP.$("ELE_BUTTON_SEARCH","//span[contains(text(),'Search')]","TGTYPESCREENNO");

        WAIT.$(3,"TGTYPESCREENNO");

        TYPE.$("ELE_TEXTINPUT_SEARCHCLIENTNAME","//input[@id='workbenchForm:workbenchTabs:clientGrid:individualName_Col:filter']",var_LastName,"FALSE","TGTYPESCREENNO");

        WAIT.$(2,"TGTYPESCREENNO");

        TYPE.$("ELE_TXTBOX_IDNUMCLIENTSEARCH","//input[@id='workbenchForm:workbenchTabs:clientGrid:clientId_Col:filter']",var_ClientID,"FALSE","TGTYPESCREENNO");

        WAIT.$(3,"TGTYPESCREENNO");

        LOGGER.info("Executed Step = START IF");
        if (check("ELE_GETVAL_CLIENTNAMEGRIDLINE1","//span[@id='workbenchForm:workbenchTabs:clientGrid:0:individualName']","VISIBLE","TGTYPESCREENREG")) {
         LOGGER.info("Executed Step = IF,ELE_GETVAL_CLIENTNAMEGRIDLINE1,VISIBLE,TGTYPESCREENREG");
        TAP.$("ELE_LINK_CLIENTSEARCHEDNAMEGRID","//tbody[@id='workbenchForm:workbenchTabs:clientGrid_data']//tr","TGTYPESCREENNO");

        WAIT.$(3,"TGTYPESCREENNO");

        TAP.$("ELE_BUTTON_SELECTCLIENT","//button[@id='workbenchForm:workbenchTabs:button_select']","TGTYPESCREENNO");

        TAP.$("ELE_BUTTON_CONTINUE","//span[contains(text(),'Continue')]","TGTYPESCREENNO");

        WAIT.$(3,"TGTYPESCREENNO");

        }
         else {
          LOGGER.info("Executed Step = ELSE,TGTYPESCREENREG");
        ASSERT.$("ELE_GETVAL_NOTCLIENTFOUNDGRID","//td[contains(text(),'No records found.')]","CONTAINS","No records found","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_CANCELSEARCH","//button[@id='workbenchForm:workbenchTabs:button_cancel']","TGTYPESCREENNO");

        WAIT.$(2,"TGTYPESCREENNO");

        TAP.$("ELE_BUTTON_ADDCLIENT","//button[@id='workbenchForm:workbenchTabs:button_add']","TGTYPESCREENNO");

        TYPE.$("ELE_TXTBOX_FIRSTNAME","//input[@id='workbenchForm:workbenchTabs:nameFirst']",var_FirstName,"FALSE","TGTYPESCREENNO");

        TYPE.$("ELE_TXTBOX_LASTNAME","//input[@id='workbenchForm:workbenchTabs:nameLast']",var_LastName,"FALSE","TGTYPESCREENNO");

        SCROLL.$("ELE_TXTBOX_DOB","//input[@id='workbenchForm:workbenchTabs:dateOfBirth_input']","DOWN","TGTYPESCREENNO");

		String var_ClientDOB = "null";
                 LOGGER.info("Executed Step = VAR,String,var_ClientDOB,null,TGTYPESCREENNO");
		var_ClientDOB = getJsonData(var_CSVData , "$.records["+var_Count+"].ClientDOB");
                 LOGGER.info("Executed Step = STORE,var_ClientDOB,var_CSVData,$.records[{var_Count}].ClientDOB,TGTYPESCREENNO");
        TAP.$("ELE_TXTBOX_DOB","//input[@id='workbenchForm:workbenchTabs:dateOfBirth_input']","TGTYPESCREENNO");

        TYPE.$("ELE_TXTBOX_DOB","//input[@id='workbenchForm:workbenchTabs:dateOfBirth_input']",var_ClientDOB,"FALSE","TGTYPESCREENNO");

        LOGGER.info("Executed Step = START IF");
        if (check(var_Gender,"CONTAINS","Male","TGTYPESCREENNO")) {
         LOGGER.info("Executed Step = IF,var_Gender,CONTAINS,Male,TGTYPESCREENNO");
        TAP.$("ELE_RDOBTN_MALE","//body//tr[@class='ui-widget-content']//tr[@class='ui-widget-content']//tr[2]//td[1]//div[1]//div[2]//span[1]","TGTYPESCREENNO");

        }
         else {
          LOGGER.info("Executed Step = ELSE,TGTYPESCREENNO");
        TAP.$("ELE_RDOBTN_FEMALE","//tr[@class='ui-widget-content']//tr[1]//td[1]//div[1]//div[2]//span[1]","TGTYPESCREENNO");

        }
          LOGGER.info("Executed Step = ENDIF,TGTYPESCREENNO");
        SWIPE.$("DOWN","TGTYPESCREENNO");

        SWIPE.$("DOWN","TGTYPESCREENNO");

		var_dataToPass = "Social Security Number";
                 LOGGER.info("Executed Step = STORE,var_dataToPass,Social Security Number,TGTYPESCREENNO");
        TAP.$("ELE_DRPDWN_IDTYPE","//label[@id='workbenchForm:workbenchTabs:taxIdenUsag_label']","TGTYPESCREENNO");

        WAIT.$(2,"TGTYPESCREENNO");

		try { 
		 Thread.sleep(1000);
		WebElement selectValueOfDropDown= driver.findElement(By.xpath("//li[@data-label='"+ var_dataToPass +"']"));

				selectValueOfDropDown.click();
		// Add Screenshot will only work for Android platform

				 Thread.sleep(1000);
		 
		 ;} 
		 catch(Exception e) { PrintVal("Renderer Script Exception : "+e.toString()); } 
		 PrintVal("Executed Step = CALLRENDERERSCRIPT,SELECTVALUEFROMDROPDOWN"); 
        TYPE.$("ELE_TXTBOX_IDNUMBER","//input[@id='workbenchForm:workbenchTabs:identificationNumber']",var_SocialSecurityNumber,"FALSE","TGTYPESCREENNO");

		takeFullScreenShot();
                 LOGGER.info("Executed Step = TAKEFULLSCREENSHOT,TGTYPESCREENREG");
        TAP.$("ELE_BUTTON_VALIDATE","//span[contains(text(),'Validate')]","TGTYPESCREENNO");

        WAIT.$(2,"TGTYPESCREENNO");

        ASSERT.$("ELE_GETMSG_SUCCESS","//div[@id='workbenchForm:workbenchTabs:messages']//span[@class='ui-messages-info-summary']","CONTAINS","successfully","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SAVE","//span[contains(text(),'Save')]","TGTYPESCREENNO");

        WAIT.$(3,"TGTYPESCREENNO");

        ASSERT.$("ELE_GETMSG_SUCCESS","//div[@id='workbenchForm:workbenchTabs:messages']//span[@class='ui-messages-info-summary']","CONTAINS","successfully","TGTYPESCREENREG");

		String var_ClientCity = "null";
                 LOGGER.info("Executed Step = VAR,String,var_ClientCity,null,TGTYPESCREENNO");
		String var_ClientZipCode = "null";
                 LOGGER.info("Executed Step = VAR,String,var_ClientZipCode,null,TGTYPESCREENNO");
		String var_ClientAddressOne = "null";
                 LOGGER.info("Executed Step = VAR,String,var_ClientAddressOne,null,TGTYPESCREENNO");
		var_ClientCity = getJsonData(var_CSVData , "$.records["+var_Count+"].ClientCity");
                 LOGGER.info("Executed Step = STORE,var_ClientCity,var_CSVData,$.records[{var_Count}].ClientCity,TGTYPESCREENNO");
		var_ClientZipCode = getJsonData(var_CSVData , "$.records["+var_Count+"].ClientZipCode");
                 LOGGER.info("Executed Step = STORE,var_ClientZipCode,var_CSVData,$.records[{var_Count}].ClientZipCode,TGTYPESCREENNO");
		var_ClientAddressOne = getJsonData(var_CSVData , "$.records["+var_Count+"].ClientAddress");
                 LOGGER.info("Executed Step = STORE,var_ClientAddressOne,var_CSVData,$.records[{var_Count}].ClientAddress,TGTYPESCREENNO");
        TYPE.$("ELE_TXTBOX_ADDRESSLINE1","//input[@id='workbenchForm:workbenchTabs:addressLineOne']",var_ClientAddressOne,"FALSE","TGTYPESCREENNO");

        TYPE.$("ELE_TXTBOX_CITY","//input[@id='workbenchForm:workbenchTabs:addressCity']",var_ClientCity,"FALSE","TGTYPESCREENNO");

        SCROLL.$("ELE_TXTBOX_ZIPCODE","//input[@id='workbenchForm:workbenchTabs:zipCode']","DOWN","TGTYPESCREENNO");

		var_stateToPass = getJsonData(var_CSVData , "$.records["+var_Count+"].IssueState");
                 LOGGER.info("Executed Step = STORE,var_stateToPass,var_CSVData,$.records[{var_Count}].IssueState,TGTYPESCREENNO");
        TAP.$("ELE_DRPDWN_ADDRESSSTATE","//label[@id='workbenchForm:workbenchTabs:countryAndStateCode_label']","TGTYPESCREENNO");

        WAIT.$(2,"TGTYPESCREENNO");

		try { 
		 Thread.sleep(1000);
				// WebElement selectValueOfDropDown= driver.findElement(By.xpath("//li[@id='workbenchForm:workbenchTabs:'"+ var_stateToPass +"']"));
		// WebElement selectValueOfDropDown= driver.findElement(By.xpath("//ul[@id='workbenchForm:workbenchTabs:birthCountryAndStateCode_items']//li[@data-label='"+ var_stateToPass +"']")); BIRTH STATE DROPDWN


		 WebElement selectValueOfDropDown= driver.findElement(By.xpath("//ul[@id='workbenchForm:workbenchTabs:countryAndStateCode_items']//li[@data-label='"+ var_stateToPass +"']")); //Address state Dropdown


						// Add Screenshot will only work for Android platform

						selectValueOfDropDown.click();	
		// Add Screenshot will only work for Android platform


						 Thread.sleep(1000);
		 
		 ;} 
		 catch(Exception e) { PrintVal("Renderer Script Exception : "+e.toString()); } 
		 PrintVal("Executed Step = CALLRENDERERSCRIPT,SELECTVALUEFROMDRPDOWNWITHPRESELECTEDVALUES"); 
        TYPE.$("ELE_TXTBOX_ZIPCODE","//input[@id='workbenchForm:workbenchTabs:zipCode']",var_ClientZipCode,"FALSE","TGTYPESCREENNO");

		takeFullScreenShot();
                 LOGGER.info("Executed Step = TAKEFULLSCREENSHOT,TGTYPESCREENREG");
        TAP.$("ELE_BUTTON_VALIDATE","//span[contains(text(),'Validate')]","TGTYPESCREENNO");

        ASSERT.$("ELE_GETMSG_SUCCESS","//div[@id='workbenchForm:workbenchTabs:messages']//span[@class='ui-messages-info-summary']","CONTAINS","successfully","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SAVE","//span[contains(text(),'Save')]","TGTYPESCREENNO");

        WAIT.$(2,"TGTYPESCREENNO");

        ASSERT.$("ELE_GETMSG_SUCCESS","//div[@id='workbenchForm:workbenchTabs:messages']//span[@class='ui-messages-info-summary']","CONTAINS","successfully","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_CONTINUE","//span[contains(text(),'Continue')]","TGTYPESCREENNO");

        WAIT.$(3,"TGTYPESCREENNO");

        }
          LOGGER.info("Executed Step = ENDIF,TGTYPESCREENNO");
        CALL.$("SubmitPolicyforCreation","TGTYPESCREENREG");

		takeFullScreenShot();
                 LOGGER.info("Executed Step = TAKEFULLSCREENSHOT,TGTYPESCREENREG");
        TAP.$("ELE_BUTTON_VALIDATE","//span[contains(text(),'Validate')]","TGTYPESCREENNO");

        ASSERT.$("ELE_GETMSG_SUCCESS","//div[@id='workbenchForm:workbenchTabs:messages']//span[@class='ui-messages-info-summary']","CONTAINS","successfully","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SAVE","//span[contains(text(),'Save')]","TGTYPESCREENNO");

        WAIT.$(3,"TGTYPESCREENNO");

        ASSERT.$("ELE_GETVAL_POLICYSTATUSPENDINGISSUE","//span[@id='workbenchForm:workbenchTabs:contractStatusNewBusText']","CONTAINS","Pending","TGTYPESCREENREG");

		writeToCSV("Policy Number " , ActionWrapper.getWebElementValueForVariable("span[id='workbenchForm:workbenchTabs:policyNumber']", "CssSelector", 0, "span[id='workbenchForm:workbenchTabs:policyNumber']TGWEBCOMMACssSelectorTGWEBCOMMA0"));
                 LOGGER.info("Executed Step = WRITETOCSV,ELE_GETVAL_POLICYNUMBER,Policy Number,TGTYPESCREENNO");
		takeFullScreenShot();
                 LOGGER.info("Executed Step = TAKEFULLSCREENSHOT,TGTYPESCREENREG");
        CALL.$("AddBenefitIssuePolicy","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SHOWLINKS","//span[@class='ui-button-icon-left ui-icon ui-c fa fa-ellipsis-v fa-2x']","TGTYPESCREENREG");

        TAP.$("ELE_LINK_BENEFIT","//span[contains(text(),'Benefits')]","TGTYPESCREENREG");

        TAP.$("ELE_DROPDWN_BENEFITSELECT","//div[@id='workbenchForm:workbenchTabs:initialPlanCode']//div[@class='ui-selectonemenu-trigger ui-state-default ui-corner-right']","TGTYPESCREENREG");

        TAP.$("ELE_DROPDWNSELECT_BENEFITW2013CWHOLELIFE1860","//li[@id='workbenchForm:workbenchTabs:initialPlanCode_4']","TGTYPESCREENREG");

        TYPE.$("ELE_DATE_BENEFITSTARTDATE","//input[@id='workbenchForm:workbenchTabs:start_date_input']",var_EffectiveDate,"FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_CONTINUE","//span[contains(text(),'Continue')]","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_UNITS","//input[@id='workbenchForm:workbenchTabs:unitsOfInsurance_input']","20","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_LINK_SUPLIMENTBENEFITUNITS","//span[@id='workbenchForm:workbenchTabs:section8:0:units']","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_SUPBENEFITENTERUNITS","//input[@id='workbenchForm:workbenchTabs:units_input']","10","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SAVE","//span[contains(text(),'Save')]","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_VALIDATE","//span[contains(text(),'Validate')]","TGTYPESCREENREG");

        ASSERT.$("ELE_MSG_VALIDATIONSUCCESS","//div[@id='workbenchForm:workbenchTabs:messages']//span[@class='ui-messages-info-summary'][contains(text(),'Validation completed successfully with no errors.')]","VISIBLE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SAVE","//span[contains(text(),'Save')]","TGTYPESCREENREG");

        TAP.$("ELE_TAB_DISPLAYNEWBUSINESSPOLICYCONTRACT","//li[@class='ui-state-default ui-corner-top ui-tabs-selected ui-state-active']//a[contains(text(),'Display New Business Policy Contract')]","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SHOWLINKS","//span[@class='ui-button-icon-left ui-icon ui-c fa fa-ellipsis-v fa-2x']","TGTYPESCREENREG");

        TAP.$("ELE_LINK_ISSUE","//span[@class='ui-menuitem-text'][contains(text(),'Issue')]","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_VALIDATE","//span[contains(text(),'Validate')]","TGTYPESCREENREG");

        ASSERT.$("ELE_MSG_VALIDATIONSUCCESS","//div[@id='workbenchForm:workbenchTabs:messages']//span[@class='ui-messages-info-summary'][contains(text(),'Validation completed successfully with no errors.')]","VISIBLE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SUBMIT","//span[contains(text(),'Submit')]","TGTYPESCREENREG");

        ASSERT.$("ELE_GETVAL_SUCCESSMESSAGE","//div[@id='workbenchForm:workbenchTabs:messages']//span[@class='ui-messages-info-summary'][contains(text(),'Request completed successfully.')]","VISIBLE","TGTYPESCREENREG");

		var_Count = var_Count + ( 1 );
                 LOGGER.info("Executed Step = INCREASEVAR,var_Count,1,TGTYPESCREENREG");
    	}
          LOGGER.info("Executed Step = WEND,TGTYPESCREENREG");
    	resetAttemps();

    }


    @Test
    public void mm40regscn01createapolicy() {

        // No need to declare / print method name here, handled by TestNg listener
        // declare("mm40regscn01createapolicy");

		com.jayway.jsonpath.ReadContext var_CSVData = getJsonData("http://10.1.104.229/json/20201201/9zz4qK.json");
                 LOGGER.info("Executed Step = VAR,Json,var_CSVData,http://10.1.104.229/json/20201201/9zz4qK.json,TGTYPESCREENREG");
		String var_SessionDate = "null";
                 LOGGER.info("Executed Step = VAR,String,var_SessionDate,null,TGTYPESCREENREG");
		String var_SessionDate1 = "null";
                 LOGGER.info("Executed Step = VAR,String,var_SessionDate1,null,TGTYPESCREENREG");
		int var_Count = 0;
                 LOGGER.info("Executed Step = VAR,Integer,var_Count,0,TGTYPESCREENREG");
		String var_dataToPass = "null";
                 LOGGER.info("Executed Step = VAR,String,var_dataToPass,null,TGTYPESCREENREG");
		String var_TaxQualifiedDescription = "null";
                 LOGGER.info("Executed Step = VAR,String,var_TaxQualifiedDescription,null,TGTYPESCREENREG");
		String var_CashWithApplication = "null";
                 LOGGER.info("Executed Step = VAR,String,var_CashWithApplication,null,TGTYPESCREENREG");
		String var_TaxWithholding = "null";
                 LOGGER.info("Executed Step = VAR,String,var_TaxWithholding,null,TGTYPESCREENREG");
		String var_SSNNumber = "null";
                 LOGGER.info("Executed Step = VAR,String,var_SSNNumber,null,TGTYPESCREENREG");
		String var_DateOfBirth = "null";
                 LOGGER.info("Executed Step = VAR,String,var_DateOfBirth,null,TGTYPESCREENREG");
		String var_DistributionChannelName;
                 LOGGER.info("Executed Step = VAR,String,var_DistributionChannelName,TGTYPESCREENREG");
		var_DistributionChannelName = getJsonData(var_CSVData , "$.records["+var_Count+"].DistributionChannelName");
                 LOGGER.info("Executed Step = STORE,var_DistributionChannelName,var_CSVData,$.records[{var_Count}].DistributionChannelName,TGTYPESCREENREG");
		String var_DistributionChannelDescription;
                 LOGGER.info("Executed Step = VAR,String,var_DistributionChannelDescription,TGTYPESCREENREG");
		var_DistributionChannelDescription = getJsonData(var_CSVData , "$.records["+var_Count+"].DistributionChannelDescription");
                 LOGGER.info("Executed Step = STORE,var_DistributionChannelDescription,var_CSVData,$.records[{var_Count}].DistributionChannelDescription,TGTYPESCREENREG");
		String var_TableName;
                 LOGGER.info("Executed Step = VAR,String,var_TableName,TGTYPESCREENREG");
		var_TableName = getJsonData(var_CSVData , "$.records["+var_Count+"].TableName");
                 LOGGER.info("Executed Step = STORE,var_TableName,var_CSVData,$.records[{var_Count}].TableName,TGTYPESCREENREG");
		String var_TableDescription;
                 LOGGER.info("Executed Step = VAR,String,var_TableDescription,TGTYPESCREENREG");
		var_TableDescription = getJsonData(var_CSVData , "$.records["+var_Count+"].TableDescription");
                 LOGGER.info("Executed Step = STORE,var_TableDescription,var_CSVData,$.records[{var_Count}].TableDescription,TGTYPESCREENREG");
		String var_AgencyDescription;
                 LOGGER.info("Executed Step = VAR,String,var_AgencyDescription,TGTYPESCREENREG");
		var_AgencyDescription = getJsonData(var_CSVData , "$.records["+var_Count+"].AgencyDescription");
                 LOGGER.info("Executed Step = STORE,var_AgencyDescription,var_CSVData,$.records[{var_Count}].AgencyDescription,TGTYPESCREENREG");
		String var_CommissionContractNumber;
                 LOGGER.info("Executed Step = VAR,String,var_CommissionContractNumber,TGTYPESCREENREG");
		var_CommissionContractNumber = getJsonData(var_CSVData , "$.records["+var_Count+"].CommissionContractNumber");
                 LOGGER.info("Executed Step = STORE,var_CommissionContractNumber,var_CSVData,$.records[{var_Count}].CommissionContractNumber,TGTYPESCREENREG");
		String var_CommissionContractDescription;
                 LOGGER.info("Executed Step = VAR,String,var_CommissionContractDescription,TGTYPESCREENREG");
		var_CommissionContractDescription = getJsonData(var_CSVData , "$.records["+var_Count+"].CommissionContractDescription");
                 LOGGER.info("Executed Step = STORE,var_CommissionContractDescription,var_CSVData,$.records[{var_Count}].CommissionContractDescription,TGTYPESCREENREG");
		String var_EffectiveDate;
                 LOGGER.info("Executed Step = VAR,String,var_EffectiveDate,TGTYPESCREENREG");
		var_EffectiveDate = getJsonData(var_CSVData , "$.records["+var_Count+"].EffectiveDate");
                 LOGGER.info("Executed Step = STORE,var_EffectiveDate,var_CSVData,$.records[{var_Count}].EffectiveDate,TGTYPESCREENREG");
		String var_LineDescription;
                 LOGGER.info("Executed Step = VAR,String,var_LineDescription,TGTYPESCREENREG");
		var_LineDescription = getJsonData(var_CSVData , "$.records["+var_Count+"].LineDescription");
                 LOGGER.info("Executed Step = STORE,var_LineDescription,var_CSVData,$.records[{var_Count}].LineDescription,TGTYPESCREENREG");
        CALL.$("LoginToGIAS4","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENNO");

        TYPE.$("ELE_TXTBOX_USERNAME","//input[@id='guestForm:username']","Mano","FALSE","TGTYPESCREENNO");

        TYPE.$("ELE_TXTBOX_PASSWORD","//input[@id='guestForm:password']","Cnx12345!","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_LOGIN","//button[@id='guestForm:loginButton']","TGTYPESCREENNO");

        LOGGER.info("Executed Step = REPEAT IF");
    	while (check(var_Count,"<",1,"TGTYPESCREENREG")) {
          LOGGER.info("Executed Step = WHILE,var_Count,<,1,TGTYPESCREENREG");
		var_SessionDate = getJsonData(var_CSVData , "$.records["+var_Count+"].SessionDate");
                 LOGGER.info("Executed Step = STORE,var_SessionDate,var_CSVData,$.records[{var_Count}].SessionDate,TGTYPESCREENREG");
		var_TaxQualifiedDescription = getJsonData(var_CSVData , "$.records["+var_Count+"].TaxQualifiedDescription");
                 LOGGER.info("Executed Step = STORE,var_TaxQualifiedDescription,var_CSVData,$.records[{var_Count}].TaxQualifiedDescription,TGTYPESCREENREG");
		var_CashWithApplication = getJsonData(var_CSVData , "$.records["+var_Count+"].CashWithApplication");
                 LOGGER.info("Executed Step = STORE,var_CashWithApplication,var_CSVData,$.records[{var_Count}].CashWithApplication,TGTYPESCREENREG");
		var_TaxWithholding = getJsonData(var_CSVData , "$.records["+var_Count+"].TaxWithholding");
                 LOGGER.info("Executed Step = STORE,var_TaxWithholding,var_CSVData,$.records[{var_Count}].TaxWithholding,TGTYPESCREENREG");
		var_SSNNumber = getJsonData(var_CSVData , "$.records["+var_Count+"].SocialSecurityNumber");
                 LOGGER.info("Executed Step = STORE,var_SSNNumber,var_CSVData,$.records[{var_Count}].SocialSecurityNumber,TGTYPESCREENREG");
		var_DateOfBirth = getJsonData(var_CSVData , "$.records["+var_Count+"].ClientDOB");
                 LOGGER.info("Executed Step = STORE,var_DateOfBirth,var_CSVData,$.records[{var_Count}].ClientDOB,TGTYPESCREENREG");
        CALL.$("ChangeSessionDate","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SESSIONDATE","//a[@id='headerForm:sessionDatelink']","TGTYPESCREENNO");

        WAIT.$(2,"TGTYPESCREENNO");

        TYPE.$("ELE_TXTBOX_NEWSESSIONDATE","//input[@id='sessionDateForm:sessionDateInput_input']",var_SessionDate,"FALSE","TGTYPESCREENNO");

		takeFullScreenShot();
                 LOGGER.info("Executed Step = TAKEFULLSCREENSHOT,TGTYPESCREENREG");
        TAP.$("ELE_BUTTON_CHANGEDATE","//span[text()='Change Date']","TGTYPESCREENNO");

        WAIT.$(2,"TGTYPESCREENNO");

        CALL.$("AddDistributionChannel","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_SEARCHTASKS","//input[@id='workbenchForm:workbenchTabs:taskSearchContainer:taskSearchSearch']","Distribution Channel","FALSE","TGTYPESCREENNO");

        TAP.$("ELE_BUTTON_TASKSEARCH","//a[@id='workbenchForm:workbenchTabs:taskSearchContainer:tb_buttonSearch']//span[@class='fa fa-search fa-lg']","TGTYPESCREENNO");

        WAIT.$(2,"TGTYPESCREENNO");

        TAP.$("ELE_LINK_AGENTDISTRIBUTIONCHANNEL","//span[text()='Agent Distribution Channel']","TGTYPESCREENNO");

        WAIT.$(3,"TGTYPESCREENNO");

        TYPE.$("ELE_TXTBOX_DISTRIBUTIONCHANNELGRID","//input[@id='workbenchForm:workbenchTabs:grid:distributionChanName_Col:filter']",var_DistributionChannelName,"FALSE","TGTYPESCREENNO");

        WAIT.$(2,"TGTYPESCREENNO");

        LOGGER.info("Executed Step = START IF");
        if (check("ELE_LINK_DISTCHANNELGRID","//span[@id='workbenchForm:workbenchTabs:grid:0:distributionChanName']","INVISIBLE","TGTYPESCREENREG")) {
         LOGGER.info("Executed Step = IF,ELE_LINK_DISTCHANNELGRID,INVISIBLE,TGTYPESCREENREG");
        TAP.$("ELE_BUTTON_ADD","//button[@id='workbenchForm:workbenchTabs:tb_buttonAdd']//span[@class='ui-button-text ui-c'][contains(text(),'Add')]","TGTYPESCREENNO");

        WAIT.$(3,"TGTYPESCREENNO");

        TAP.$("ELE_TXTBOX_DISTRIBUTIONCHANNELNAME","//input[@id='workbenchForm:workbenchTabs:distributionChanName']","TGTYPESCREENNO");

        WAIT.$(1,"TGTYPESCREENNO");

        TYPE.$("ELE_TXTBOX_DISTRIBUTIONCHANNELNAME","//input[@id='workbenchForm:workbenchTabs:distributionChanName']",var_DistributionChannelName,"FALSE","TGTYPESCREENNO");

        TAP.$("ELE_TXTBOX_DISTRIBUTIONCHANNELCOMMENT","//input[@id='workbenchForm:workbenchTabs:channelDescription']","TGTYPESCREENNO");

        WAIT.$(1,"TGTYPESCREENNO");

        TYPE.$("ELE_TXTBOX_DISTRIBUTIONCHANNELCOMMENT","//input[@id='workbenchForm:workbenchTabs:channelDescription']",var_DistributionChannelDescription,"FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_VALIDATE","//span[contains(text(),'Validate')]","TGTYPESCREENNO");

        WAIT.$(2,"TGTYPESCREENNO");

        ASSERT.$("ELE_GETMSG_SUCCESS","//div[@id='workbenchForm:workbenchTabs:messages']//span[@class='ui-messages-info-summary']","CONTAINS","successfully","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SAVE","//span[contains(text(),'Save')]","TGTYPESCREENNO");

        WAIT.$(2,"TGTYPESCREENNO");

        ASSERT.$("ELE_GETMSG_SUCCESS","//div[@id='workbenchForm:workbenchTabs:messages']//span[@class='ui-messages-info-summary']","CONTAINS","successfully","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_GIASHOME","//span[text()='GIAS']","TGTYPESCREENNO");

        }
         else {
          LOGGER.info("Executed Step = ELSE,TGTYPESCREENNO");
        TAP.$("ELE_BUTTON_GIASHOME","//span[text()='GIAS']","TGTYPESCREENNO");

        }
          LOGGER.info("Executed Step = ENDIF,TGTYPESCREENNO");
        CALL.$("AddAgentCodes","TGTYPESCREENREG");

		String var_AgencyRankCode;
                 LOGGER.info("Executed Step = VAR,String,var_AgencyRankCode,TGTYPESCREENNO");
		var_AgencyRankCode = getJsonData(var_CSVData , "$.records["+var_Count+"].AgencyRankCode");
                 LOGGER.info("Executed Step = STORE,var_AgencyRankCode,var_CSVData,$.records[{var_Count}].AgencyRankCode,TGTYPESCREENNO");
        TYPE.$("ELE_TXTBOX_SEARCHTASKS","//input[@id='workbenchForm:workbenchTabs:taskSearchContainer:taskSearchSearch']","Agent Rank Codes","FALSE","TGTYPESCREENNO");

        TAP.$("ELE_BUTTON_TASKSEARCH","//a[@id='workbenchForm:workbenchTabs:taskSearchContainer:tb_buttonSearch']//span[@class='fa fa-search fa-lg']","TGTYPESCREENNO");

        WAIT.$(3,"TGTYPESCREENNO");

        TAP.$("ELE_LINK_AGENTRANKCODES","//span[text()='Agent Rank Codes']","TGTYPESCREENNO");

        WAIT.$(2,"TGTYPESCREENNO");

		var_dataToPass = var_DistributionChannelDescription;
                 LOGGER.info("Executed Step = STORE,var_dataToPass,var_DistributionChannelDescription,TGTYPESCREENNO");
        TAP.$("ELE_DRPDWN_DISTCHANNELFROMGRID","//label[@id='workbenchForm:workbenchTabs:grid:distributionChannel_filter_label']","TGTYPESCREENNO");

        WAIT.$(2,"TGTYPESCREENNO");

		try { 
		 WebElement dropdown = driver.findElement(By.xpath("//ul[@id='workbenchForm:workbenchTabs:grid:distributionChannel_filter_items']")); 


		       //ul[@id='workbenchForm:workbenchTabs:distributionChannel_items']
		       // dropdown.click();
		       
		        List<WebElement> options = dropdown.findElements(By.tagName("li"));
				
				System.out.println("Size"+options.size());
		        for (WebElement option : options)
		        {
					System.out.print("Values of option"+option);
		            if (option.getText().equals(var_dataToPass))
		            {
		                option.click(); // click the desired option
		                break;
		            }
		        }
		 
		 ;} 
		 catch(Exception e) { PrintVal("Renderer Script Exception : "+e.toString()); } 
		 PrintVal("Executed Step = CALLRENDERERSCRIPT,FILTERDISTCHANNELFROMGRID"); 
        WAIT.$(3,"TGTYPESCREENNO");

        LOGGER.info("Executed Step = START IF");
        if (check("ELE_GETVAL_DISTCHFROMGRID","//span[@id='workbenchForm:workbenchTabs:grid:0:distributionChannel']","INVISIBLE","TGTYPESCREENREG")) {
         LOGGER.info("Executed Step = IF,ELE_GETVAL_DISTCHFROMGRID,INVISIBLE,TGTYPESCREENREG");
        TAP.$("ELE_BUTTON_ADD","//button[@id='workbenchForm:workbenchTabs:tb_buttonAdd']//span[@class='ui-button-text ui-c'][contains(text(),'Add')]","TGTYPESCREENNO");

		var_dataToPass = var_DistributionChannelDescription;
                 LOGGER.info("Executed Step = STORE,var_dataToPass,var_DistributionChannelDescription,TGTYPESCREENNO");
        TAP.$("ELE_DRPDWN_DISTRIBUTIONCHANNEL","//label[@id='workbenchForm:workbenchTabs:distributionChannel_label']","TGTYPESCREENNO");

        WAIT.$(2,"TGTYPESCREENNO");

		try { 
		 WebElement dropdown = driver.findElement(By.xpath("//ul[@id='workbenchForm:workbenchTabs:distributionChannel_items']")); 


		       //ul[@id='workbenchForm:workbenchTabs:distributionChannel_items']
		       // dropdown.click();
		       
		        List<WebElement> options = dropdown.findElements(By.tagName("li"));
				
				System.out.println("Size"+options.size());
		        for (WebElement option : options)
		        {
					System.out.print("Values of option"+option);
		            if (option.getText().equals(var_dataToPass))
		            {
		                option.click(); // click the desired option
		                break;
		            }
		        }
		 
		 ;} 
		 catch(Exception e) { PrintVal("Renderer Script Exception : "+e.toString()); } 
		 PrintVal("Executed Step = CALLRENDERERSCRIPT,SELECTDISTRIBUTIONCHANNELFROMDROPDOWN"); 
        WAIT.$(2,"TGTYPESCREENNO");

        TAP.$("ELE_TXTBOX_RANKLEVEL","//input[@id='workbenchForm:workbenchTabs:agencyRankLevel_input']","TGTYPESCREENNO");

        TYPE.$("ELE_TXTBOX_RANKLEVEL","//input[@id='workbenchForm:workbenchTabs:agencyRankLevel_input']","1","FALSE","TGTYPESCREENNO");

        WAIT.$(2,"TGTYPESCREENNO");

        TAP.$("ELE_TXTBOX_RANKCODE","//input[@id='workbenchForm:workbenchTabs:agencyRankCode']","TGTYPESCREENNO");

        TYPE.$("ELE_TXTBOX_RANKCODE","//input[@id='workbenchForm:workbenchTabs:agencyRankCode']",var_AgencyRankCode,"FALSE","TGTYPESCREENNO");

        WAIT.$(2,"TGTYPESCREENNO");

        TAP.$("ELE_TXTBOX_DESCRIPTION","//input[@id='workbenchForm:workbenchTabs:description']","TGTYPESCREENNO");

        TYPE.$("ELE_TXTBOX_DESCRIPTION","//input[@id='workbenchForm:workbenchTabs:description']",var_AgencyDescription,"FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_VALIDATE","//span[contains(text(),'Validate')]","TGTYPESCREENNO");

        ASSERT.$("ELE_GETMSG_SUCCESS","//div[@id='workbenchForm:workbenchTabs:messages']//span[@class='ui-messages-info-summary']","CONTAINS","successfully","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SAVE","//span[contains(text(),'Save')]","TGTYPESCREENNO");

        ASSERT.$("ELE_GETMSG_SUCCESS","//div[@id='workbenchForm:workbenchTabs:messages']//span[@class='ui-messages-info-summary']","CONTAINS","successfully","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_GIASHOME","//span[text()='GIAS']","TGTYPESCREENNO");

        }
         else {
          LOGGER.info("Executed Step = ELSE,TGTYPESCREENNO");
        TAP.$("ELE_BUTTON_GIASHOME","//span[text()='GIAS']","TGTYPESCREENNO");

        }
          LOGGER.info("Executed Step = ENDIF,TGTYPESCREENNO");
        CALL.$("AddCommissionScheduleRates","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_SEARCHTASKS","//input[@id='workbenchForm:workbenchTabs:taskSearchContainer:taskSearchSearch']","Commission","FALSE","TGTYPESCREENNO");

        TAP.$("ELE_BUTTON_TASKSEARCH","//a[@id='workbenchForm:workbenchTabs:taskSearchContainer:tb_buttonSearch']//span[@class='fa fa-search fa-lg']","TGTYPESCREENNO");

        WAIT.$(3,"TGTYPESCREENNO");

        TAP.$("ELE_LINK_COMMISSIONRATES","//span[text()='Commission Rates']","TGTYPESCREENNO");

        WAIT.$(3,"TGTYPESCREENNO");

        TYPE.$("ELE_TXTBOX_TABLENAMEGRID","//input[@id='workbenchForm:workbenchTabs:grid:tableName_Col:filter']",var_TableName,"FALSE","TGTYPESCREENNO");

        WAIT.$(2,"TGTYPESCREENNO");

        LOGGER.info("Executed Step = START IF");
        if (check("ELE_LINK_TABLENAMEGRID","//span[@id='workbenchForm:workbenchTabs:grid:0:tableName']","INVISIBLE","TGTYPESCREENNO")) {
         LOGGER.info("Executed Step = IF,ELE_LINK_TABLENAMEGRID,INVISIBLE,TGTYPESCREENNO");
        TAP.$("ELE_BUTTON_ADD","//button[@id='workbenchForm:workbenchTabs:tb_buttonAdd']//span[@class='ui-button-text ui-c'][contains(text(),'Add')]","TGTYPESCREENNO");

        WAIT.$(2,"TGTYPESCREENNO");

        TAP.$("ELE_TXTBOX_TABLENAME","//input[@id='workbenchForm:workbenchTabs:tableName']","TGTYPESCREENNO");

        WAIT.$(1,"TGTYPESCREENNO");

        TYPE.$("ELE_TXTBOX_TABLENAME","//input[@id='workbenchForm:workbenchTabs:tableName']",var_TableName,"FALSE","TGTYPESCREENNO");

        TAP.$("ELE_TXTBOX_DESCRIPTION","//input[@id='workbenchForm:workbenchTabs:description']","TGTYPESCREENNO");

        WAIT.$(1,"TGTYPESCREENNO");

        TYPE.$("ELE_TXTBOX_DESCRIPTION","//input[@id='workbenchForm:workbenchTabs:description']",var_TableDescription,"FALSE","TGTYPESCREENNO");

		var_dataToPass = var_DistributionChannelDescription;
                 LOGGER.info("Executed Step = STORE,var_dataToPass,var_DistributionChannelDescription,TGTYPESCREENNO");
        TAP.$("ELE_DRPDWN_DISTCHANNEL","//div[@id='workbenchForm:workbenchTabs:distributionChannel']","TGTYPESCREENNO");

        WAIT.$(2,"TGTYPESCREENNO");

		try { 
		 WebElement dropdown = driver.findElement(By.xpath("//ul[@id='workbenchForm:workbenchTabs:distributionChannel_items']")); 


		       //ul[@id='workbenchForm:workbenchTabs:distributionChannel_items']
		       // dropdown.click();
		       
		        List<WebElement> options = dropdown.findElements(By.tagName("li"));
				
				System.out.println("Size"+options.size());
		        for (WebElement option : options)
		        {
					System.out.print("Values of option"+option);
		            if (option.getText().equals(var_dataToPass))
		            {
		                option.click(); // click the desired option
		                break;
		            }
		        }
		 
		 ;} 
		 catch(Exception e) { PrintVal("Renderer Script Exception : "+e.toString()); } 
		 PrintVal("Executed Step = CALLRENDERERSCRIPT,SELECTDISTCHANNELCOMMISSIONSCHEDULERATES"); 
        TAP.$("ELE_BUTTON_CONTINUE","//span[contains(text(),'Continue')]","TGTYPESCREENREG");

        WAIT.$(4,"TGTYPESCREENNO");

		var_dataToPass = var_AgencyDescription;
                 LOGGER.info("Executed Step = STORE,var_dataToPass,var_AgencyDescription,TGTYPESCREENNO");
        TAP.$("ELE_TAP_SELECTAGENTRANKING","//div[@id='workbenchForm:workbenchTabs:rateTable']//tr[1]//td[1]","TGTYPESCREENNO");

        WAIT.$(2,"TGTYPESCREENNO");

		try { 
		 org.openqa.selenium.support.ui.Select dropdown = new org.openqa.selenium.support.ui.Select(driver.findElement(By.xpath("//select[@id='workbenchForm:workbenchTabs:rateTable:0:in_agencyRankCode_Col']")));
		        dropdown.selectByVisibleText(var_dataToPass);
				 
		 
		 ;} 
		 catch(Exception e) { PrintVal("Renderer Script Exception : "+e.toString()); } 
		 PrintVal("Executed Step = CALLRENDERERSCRIPT,SELECTAGENYRANKINGLEVEL"); 
        WAIT.$(1,"TGTYPESCREENNO");

        TAP.$("ELE_TXTBOX_COMMISSIONDURATION1","//input[@id='workbenchForm:workbenchTabs:rateTable:endingDuration1_input']","TGTYPESCREENNO");

        WAIT.$(1,"TGTYPESCREENNO");

        TYPE.$("ELE_TXTBOX_COMMISSIONDURATION1","//input[@id='workbenchForm:workbenchTabs:rateTable:endingDuration1_input']","1","FALSE","TGTYPESCREENNO");

        TAP.$("ELE_TXTBOX_COMMISSIONDURATION2","//input[@id='workbenchForm:workbenchTabs:rateTable:endingDuration2_input']","TGTYPESCREENNO");

        WAIT.$(1,"TGTYPESCREENNO");

        TYPE.$("ELE_TXTBOX_COMMISSIONDURATION2","//input[@id='workbenchForm:workbenchTabs:rateTable:endingDuration2_input']","5","FALSE","TGTYPESCREENNO");

        TAP.$("ELE_TXTBOX_COMMISSIONDURATION3","//input[@id='workbenchForm:workbenchTabs:rateTable:endingDuration3_input']","TGTYPESCREENNO");

        WAIT.$(1,"TGTYPESCREENNO");

        TYPE.$("ELE_TXTBOX_COMMISSIONDURATION3","//input[@id='workbenchForm:workbenchTabs:rateTable:endingDuration3_input']","120","FALSE","TGTYPESCREENNO");

        TAP.$("ELE_TXTBOX_COMMISSIONRATE1","//span[@id='workbenchForm:workbenchTabs:rateTable:0:out_commissionRate1']","TGTYPESCREENNO");

        WAIT.$(1,"TGTYPESCREENNO");

        TYPE.$("ELE_TXTBOX_COMMISSIONRATE1INPUT","//input[@id='workbenchForm:workbenchTabs:rateTable:0:in_commissionRate1_input']","20","FALSE","TGTYPESCREENNO");

        TAP.$("ELE_TXTBOX_COMMISSIONRATE2","//span[@id='workbenchForm:workbenchTabs:rateTable:0:out_commissionRate2']","TGTYPESCREENNO");

        WAIT.$(1,"TGTYPESCREENNO");

        TYPE.$("ELE_TXTBOX_COMMISSIONRATE2INPUT","//input[@id='workbenchForm:workbenchTabs:rateTable:0:in_commissionRate2_input']","15","FALSE","TGTYPESCREENNO");

        TAP.$("ELE_TXTBOX_COMMISSIONRATE3","//span[@id='workbenchForm:workbenchTabs:rateTable:0:out_commissionRate3']","TGTYPESCREENNO");

        WAIT.$(1,"TGTYPESCREENNO");

        TYPE.$("ELE_TXTBOX_COMMISSIONRATE3INPUT","//input[@id='workbenchForm:workbenchTabs:rateTable:0:in_commissionRate3_input']","10","FALSE","TGTYPESCREENNO");

        TAP.$("ELE_TXTBOX_COMMISSIONRATE2","//span[@id='workbenchForm:workbenchTabs:rateTable:0:out_commissionRate2']","TGTYPESCREENNO");

		takeFullScreenShot();
                 LOGGER.info("Executed Step = TAKEFULLSCREENSHOT,TGTYPESCREENREG");
        TAP.$("ELE_BUTTON_VALIDATE","//span[contains(text(),'Validate')]","TGTYPESCREENNO");

        WAIT.$(4,"TGTYPESCREENNO");

        ASSERT.$("ELE_GETMSG_SUCCESS","//div[@id='workbenchForm:workbenchTabs:messages']//span[@class='ui-messages-info-summary']","CONTAINS","successfully","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SAVE","//span[contains(text(),'Save')]","TGTYPESCREENNO");

        WAIT.$(4,"TGTYPESCREENNO");

        ASSERT.$("ELE_GETMSG_SUCCESS","//div[@id='workbenchForm:workbenchTabs:messages']//span[@class='ui-messages-info-summary']","CONTAINS","successfully","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_GIASHOME","//span[text()='GIAS']","TGTYPESCREENNO");

        }
         else {
          LOGGER.info("Executed Step = ELSE,TGTYPESCREENNO");
        TAP.$("ELE_BUTTON_GIASHOME","//span[text()='GIAS']","TGTYPESCREENNO");

        }
          LOGGER.info("Executed Step = ENDIF,TGTYPESCREENNO");
        CALL.$("AddCommissionContracts","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_SEARCHTASKS","//input[@id='workbenchForm:workbenchTabs:taskSearchContainer:taskSearchSearch']","Commission Contracts","FALSE","TGTYPESCREENNO");

        TAP.$("ELE_BUTTON_TASKSEARCH","//a[@id='workbenchForm:workbenchTabs:taskSearchContainer:tb_buttonSearch']//span[@class='fa fa-search fa-lg']","TGTYPESCREENNO");

        WAIT.$(2,"TGTYPESCREENNO");

        TAP.$("ELE_LINK_COMMISSIONCONTRACTS","//span[text()='Commission Contracts']","TGTYPESCREENNO");

        WAIT.$(3,"TGTYPESCREENNO");

        TYPE.$("ELE_TXTBOX_COMMISSIONCONTRACTNUMBERGRID","//input[@id='workbenchForm:workbenchTabs:grid:j_idt1590']",var_CommissionContractNumber,"FALSE","TGTYPESCREENNO");

        WAIT.$(2,"TGTYPESCREENNO");

        LOGGER.info("Executed Step = START IF");
        if (check("ELE_LINK_COMMISSIONCONTRACTGRIDLINE1","//span[@id='workbenchForm:workbenchTabs:grid:0:commissionContNumb']","INVISIBLE","TGTYPESCREENREG")) {
         LOGGER.info("Executed Step = IF,ELE_LINK_COMMISSIONCONTRACTGRIDLINE1,INVISIBLE,TGTYPESCREENREG");
        TAP.$("ELE_BUTTON_ADD","//button[@id='workbenchForm:workbenchTabs:tb_buttonAdd']//span[@class='ui-button-text ui-c'][contains(text(),'Add')]","TGTYPESCREENNO");

        WAIT.$(2,"TGTYPESCREENNO");

        TAP.$("ELE_TXTBOX_CONTRACTNUMBER","//input[@id='workbenchForm:workbenchTabs:commissionContNumb_input']","TGTYPESCREENNO");

        TYPE.$("ELE_TXTBOX_CONTRACTNUMBER","//input[@id='workbenchForm:workbenchTabs:commissionContNumb_input']",var_CommissionContractNumber,"FALSE","TGTYPESCREENNO");

        WAIT.$(2,"TGTYPESCREENNO");

        TAP.$("ELE_TXTBOX_DESCRIPTION2","//input[@id='workbenchForm:workbenchTabs:longDescription']","TGTYPESCREENNO");

        TYPE.$("ELE_TXTBOX_DESCRIPTION2","//input[@id='workbenchForm:workbenchTabs:longDescription']",var_CommissionContractDescription,"FALSE","TGTYPESCREENNO");

        WAIT.$(2,"TGTYPESCREENNO");

        TAP.$("ELE_DRPDWN_DISTRIBUTIONCHANNEL","//label[@id='workbenchForm:workbenchTabs:distributionChannel_label']","TGTYPESCREENNO");

        WAIT.$(2,"TGTYPESCREENNO");

		var_dataToPass = var_DistributionChannelDescription;
                 LOGGER.info("Executed Step = STORE,var_dataToPass,var_DistributionChannelDescription,TGTYPESCREENNO");
		try { 
		 WebElement dropdown = driver.findElement(By.xpath("//ul[@id='workbenchForm:workbenchTabs:distributionChannel_items']")); 


		       //ul[@id='workbenchForm:workbenchTabs:distributionChannel_items']
		       // dropdown.click();
		       
		        List<WebElement> options = dropdown.findElements(By.tagName("li"));
				
				System.out.println("Size"+options.size());
		        for (WebElement option : options)
		        {
					System.out.print("Values of option"+option);
		            if (option.getText().equals(var_dataToPass))
		            {
		                option.click(); // click the desired option
		                break;
		            }
		        }
		 
		 ;} 
		 catch(Exception e) { PrintVal("Renderer Script Exception : "+e.toString()); } 
		 PrintVal("Executed Step = CALLRENDERERSCRIPT,SELECTDISTRIBUTIONCHANNELFROMDROPDOWN"); 
        WAIT.$(3,"TGTYPESCREENNO");

        TAP.$("ELE_TXTBOX_EFFECTIVEDATE","//input[@id='workbenchForm:workbenchTabs:effectiveDate_input']","TGTYPESCREENNO");

        TYPE.$("ELE_TXTBOX_EFFECTIVEDATE","//input[@id='workbenchForm:workbenchTabs:effectiveDate_input']",var_EffectiveDate,"FALSE","TGTYPESCREENNO");

        WAIT.$(1,"TGTYPESCREENNO");

        TAP.$("ELE_DRPDWN_ADVANCEPAYBACKMETHOD","//label[@id='workbenchForm:workbenchTabs:advancePaybackMethod_label']","TGTYPESCREENNO");

        WAIT.$(2,"TGTYPESCREENNO");

		var_dataToPass = "100% of Commission Earned";
                 LOGGER.info("Executed Step = STORE,var_dataToPass,100% of Commission Earned,TGTYPESCREENNO");
		try { 
		 WebElement dropdown = driver.findElement(By.xpath("//ul[@id='workbenchForm:workbenchTabs:advancePaybackMethod_items']")); 


		       //ul[@id='workbenchForm:workbenchTabs:distributionChannel_items']
		       // dropdown.click();
		       
		        List<WebElement> options = dropdown.findElements(By.tagName("li"));
				
				System.out.println("Size"+options.size());
		        for (WebElement option : options)
		        {
					System.out.print("Values of option"+option);
		            if (option.getText().equals(var_dataToPass))
		            {
		                option.click(); // click the desired option
		                break;
		            }
		        }
		 
		 ;} 
		 catch(Exception e) { PrintVal("Renderer Script Exception : "+e.toString()); } 
		 PrintVal("Executed Step = CALLRENDERERSCRIPT,SELECTADVANCEPAYBACKMETHOD"); 
        WAIT.$(2,"TGTYPESCREENNO");

        TAP.$("ELE_DRPDWN_ADVANCETHROUGHAGENTRANK","//label[@id='workbenchForm:workbenchTabs:topLevelForAdvance_label']","TGTYPESCREENNO");

		var_dataToPass = var_AgencyDescription;
                 LOGGER.info("Executed Step = STORE,var_dataToPass,var_AgencyDescription,TGTYPESCREENNO");
        WAIT.$(2,"TGTYPESCREENNO");

		try { 
		 WebElement dropdown = driver.findElement(By.xpath("//ul[@id='workbenchForm:workbenchTabs:topLevelForAdvance_items']")); 


		       //ul[@id='workbenchForm:workbenchTabs:distributionChannel_items']
		       // dropdown.click();
		       
		        List<WebElement> options = dropdown.findElements(By.tagName("li"));
				
				System.out.println("Size"+options.size());
		        for (WebElement option : options)
		        {
					System.out.print("Values of option"+option);
		            if (option.getText().equals(var_dataToPass))
		            {
		                option.click(); // click the desired option
		                break;
		            }
		        }
		 
		 ;} 
		 catch(Exception e) { PrintVal("Renderer Script Exception : "+e.toString()); } 
		 PrintVal("Executed Step = CALLRENDERERSCRIPT,SELECTADVANCETHROUGHAGENTRANK"); 
        WAIT.$(2,"TGTYPESCREENNO");

        TAP.$("ELE_TXTBOX_DAYSPASTDUEBEFORECHARGEBACK","//input[@id='workbenchForm:workbenchTabs:daysLateBeforeChar_input']","TGTYPESCREENNO");

        TYPE.$("ELE_TXTBOX_DAYSPASTDUEBEFORECHARGEBACK","//input[@id='workbenchForm:workbenchTabs:daysLateBeforeChar_input']","3","FALSE","TGTYPESCREENNO");

        TAP.$("ELE_TAP_DAYSPAST","//label[@id='workbenchForm:workbenchTabs:daysLateBeforeChar_lbl']","TGTYPESCREENNO");

        WAIT.$(1,"TGTYPESCREENNO");

		takeFullScreenShot();
                 LOGGER.info("Executed Step = TAKEFULLSCREENSHOT,TGTYPESCREENFULL");
        TAP.$("ELE_BUTTON_VALIDATE","//span[contains(text(),'Validate')]","TGTYPESCREENNO");

        WAIT.$(3,"TGTYPESCREENNO");

        ASSERT.$("ELE_GETMSG_SUCCESS","//div[@id='workbenchForm:workbenchTabs:messages']//span[@class='ui-messages-info-summary']","CONTAINS","successfully","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SAVE","//span[contains(text(),'Save')]","TGTYPESCREENNO");

        WAIT.$(3,"TGTYPESCREENNO");

        ASSERT.$("ELE_GETMSG_SUCCESS","//div[@id='workbenchForm:workbenchTabs:messages']//span[@class='ui-messages-info-summary']","CONTAINS","successfully","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_GIASHOME","//span[text()='GIAS']","TGTYPESCREENNO");

        WAIT.$(3,"TGTYPESCREENNO");

        }
         else {
          LOGGER.info("Executed Step = ELSE,TGTYPESCREENNO");
        TAP.$("ELE_BUTTON_GIASHOME","//span[text()='GIAS']","TGTYPESCREENNO");

        }
          LOGGER.info("Executed Step = ENDIF,TGTYPESCREENNO");
        CALL.$("AddCommissionSchedule","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_SEARCHTASKS","//input[@id='workbenchForm:workbenchTabs:taskSearchContainer:taskSearchSearch']","Commission Contracts","FALSE","TGTYPESCREENNO");

        TAP.$("ELE_BUTTON_TASKSEARCH","//a[@id='workbenchForm:workbenchTabs:taskSearchContainer:tb_buttonSearch']//span[@class='fa fa-search fa-lg']","TGTYPESCREENNO");

        WAIT.$(2,"TGTYPESCREENNO");

        TAP.$("ELE_LINK_COMMISSIONCONTRACTS","//span[text()='Commission Contracts']","TGTYPESCREENNO");

        WAIT.$(3,"TGTYPESCREENNO");

        TAP.$("ELE_TXTBOX_COMMISSIONCONTRACTNUMBERGRID","//input[@id='workbenchForm:workbenchTabs:grid:j_idt1590']","TGTYPESCREENNO");

        WAIT.$(1,"TGTYPESCREENNO");

        TYPE.$("ELE_TXTBOX_COMMISSIONCONTRACTNUMBERGRID","//input[@id='workbenchForm:workbenchTabs:grid:j_idt1590']",var_CommissionContractNumber,"FALSE","TGTYPESCREENNO");

        WAIT.$(2,"TGTYPESCREENNO");

        TAP.$("ELE_LINK_COMMISSIONCONTRACTGRIDLINE1","//span[@id='workbenchForm:workbenchTabs:grid:0:commissionContNumb']","TGTYPESCREENNO");

        WAIT.$(3,"TGTYPESCREENNO");

        TAP.$("ELE_BUTTON_SHOWLINK","//button[@id='workbenchForm:workbenchTabs:tb_buttonLink']","TGTYPESCREENREG");

        TAP.$("ELE_LINK_COMMISSIONSCHEDULE","//div[@id='workbenchForm:workbenchTabs:link_menu']//ul//li//a","TGTYPESCREENNO");

        WAIT.$(3,"TGTYPESCREENNO");

        LOGGER.info("Executed Step = START IF");
        if (check("ELE_GETVAL_CONTRACTTYPE","//span[@id='workbenchForm:workbenchTabs:grid:0:commissionContType']","INVISIBLE","TGTYPESCREENREG")) {
         LOGGER.info("Executed Step = IF,ELE_GETVAL_CONTRACTTYPE,INVISIBLE,TGTYPESCREENREG");
        TAP.$("ELE_BUTTON_ADD","//button[@id='workbenchForm:workbenchTabs:tb_buttonAdd']//span[@class='ui-button-text ui-c'][contains(text(),'Add')]","TGTYPESCREENNO");

		var_dataToPass = "Dummy Contract";
                 LOGGER.info("Executed Step = STORE,var_dataToPass,Dummy Contract,TGTYPESCREENNO");
        TAP.$("ELE_DRPDWN_CONTRACTTYPE","//label[@id='workbenchForm:workbenchTabs:commissionContType_label']","TGTYPESCREENNO");

        WAIT.$(2,"TGTYPESCREENNO");

		try { 
		 WebElement dropdown = driver.findElement(By.xpath("//ul[@id='workbenchForm:workbenchTabs:commissionContType_items']")); 


		       //ul[@id='workbenchForm:workbenchTabs:distributionChannel_items']
		       // dropdown.click();
		       
		        List<WebElement> options = dropdown.findElements(By.tagName("li"));
				
				System.out.println("Size"+options.size());
		        for (WebElement option : options)
		        {
					System.out.print("Values of option"+option);
		            if (option.getText().equals(var_dataToPass))
		            {
		                option.click(); // click the desired option
		                break;
		            }
		        }
		 
		 ;} 
		 catch(Exception e) { PrintVal("Renderer Script Exception : "+e.toString()); } 
		 PrintVal("Executed Step = CALLRENDERERSCRIPT,SELECTCONTRACTTYPE"); 
        WAIT.$(2,"TGTYPESCREENNO");

        TAP.$("ELE_TXTBOX_EFFECTIVEDATE","//input[@id='workbenchForm:workbenchTabs:effectiveDate_input']","TGTYPESCREENNO");

        TYPE.$("ELE_TXTBOX_EFFECTIVEDATE","//input[@id='workbenchForm:workbenchTabs:effectiveDate_input']",var_EffectiveDate,"FALSE","TGTYPESCREENNO");

        WAIT.$(2,"TGTYPESCREENNO");

		var_dataToPass = var_TableDescription;
                 LOGGER.info("Executed Step = STORE,var_dataToPass,var_TableDescription,TGTYPESCREENNO");
        TAP.$("ELE_DRPDWN_COMMISSIONRATETABLENAME","//label[@id='workbenchForm:workbenchTabs:commissionRateTableName_label']","TGTYPESCREENNO");

        WAIT.$(2,"TGTYPESCREENNO");

		try { 
		 WebElement dropdown = driver.findElement(By.xpath("//ul[@id='workbenchForm:workbenchTabs:commissionRateTableName_items']")); 


		       //ul[@id='workbenchForm:workbenchTabs:distributionChannel_items']
		       // dropdown.click();
		       
		        List<WebElement> options = dropdown.findElements(By.tagName("li"));
				
				System.out.println("Size"+options.size());
		        for (WebElement option : options)
		        {
					System.out.print("Values of option"+option);
		            if (option.getText().equals(var_dataToPass))
		            {
		                option.click(); // click the desired option
		                break;
		            }
		        }
		 
		 ;} 
		 catch(Exception e) { PrintVal("Renderer Script Exception : "+e.toString()); } 
		 PrintVal("Executed Step = CALLRENDERERSCRIPT,SELECTCOMMISSIONTABLENAME"); 
        WAIT.$(2,"TGTYPESCREENNO");

        TAP.$("ELE_TXTBOX_LINEDESCRIPTION","//input[@id='workbenchForm:workbenchTabs:shortDescription']","TGTYPESCREENNO");

        TYPE.$("ELE_TXTBOX_LINEDESCRIPTION","//input[@id='workbenchForm:workbenchTabs:shortDescription']",var_LineDescription,"FALSE","TGTYPESCREENNO");

		takeFullScreenShot();
                 LOGGER.info("Executed Step = TAKEFULLSCREENSHOT,TGTYPESCREENREG");
        TAP.$("ELE_BUTTON_VALIDATE","//span[contains(text(),'Validate')]","TGTYPESCREENNO");

        WAIT.$(3,"TGTYPESCREENNO");

        ASSERT.$("ELE_GETMSG_SUCCESS","//div[@id='workbenchForm:workbenchTabs:messages']//span[@class='ui-messages-info-summary']","CONTAINS","successfully","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SAVE","//span[contains(text(),'Save')]","TGTYPESCREENNO");

        WAIT.$(3,"TGTYPESCREENNO");

        ASSERT.$("ELE_GETMSG_SUCCESS","//div[@id='workbenchForm:workbenchTabs:messages']//span[@class='ui-messages-info-summary']","CONTAINS","successfully","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_GIASHOME","//span[text()='GIAS']","TGTYPESCREENNO");

        WAIT.$(3,"TGTYPESCREENNO");

        }
         else {
          LOGGER.info("Executed Step = ELSE,TGTYPESCREENNO");
        TAP.$("ELE_BUTTON_GIASHOME","//span[text()='GIAS']","TGTYPESCREENNO");

        }
          LOGGER.info("Executed Step = ENDIF,TGTYPESCREENNO");
        CALL.$("AddPolicyContract","TGTYPESCREENREG");

		String var_ListBillNumber;
                 LOGGER.info("Executed Step = VAR,String,var_ListBillNumber,TGTYPESCREENNO");
		var_ListBillNumber = getJsonData(var_CSVData , "$.records["+var_Count+"].ListBillNumber");
                 LOGGER.info("Executed Step = STORE,var_ListBillNumber,var_CSVData,$.records[{var_Count}].ListBillNumber,TGTYPESCREENNO");
		String var_AgentNumber;
                 LOGGER.info("Executed Step = VAR,String,var_AgentNumber,TGTYPESCREENNO");
		var_AgentNumber = getJsonData(var_CSVData , "$.records["+var_Count+"].AgentNumber");
                 LOGGER.info("Executed Step = STORE,var_AgentNumber,var_CSVData,$.records[{var_Count}].AgentNumber,TGTYPESCREENNO");
        TAP.$("ELE_TXTBOX_SEARCHTASKS","//input[@id='workbenchForm:workbenchTabs:taskSearchContainer:taskSearchSearch']","TGTYPESCREENNO");

        TYPE.$("ELE_TXTBOX_SEARCHTASKS","//input[@id='workbenchForm:workbenchTabs:taskSearchContainer:taskSearchSearch']","New Policy Application","FALSE","TGTYPESCREENNO");

        TAP.$("ELE_BUTTON_TASKSEARCH","//a[@id='workbenchForm:workbenchTabs:taskSearchContainer:tb_buttonSearch']//span[@class='fa fa-search fa-lg']","TGTYPESCREENNO");

        WAIT.$(2,"TGTYPESCREENNO");

        TAP.$("ELE_LINK_NEWPOLICYAPPLICATION","//span[contains(text(),'New Policy Application')]","TGTYPESCREENNO");

        WAIT.$(3,"TGTYPESCREENNO");

        TAP.$("ELE_TAB_APPLICATIONENTRYUPDATE","//a[contains(text(),'Application Entry/Update')]","TGTYPESCREENNO");

        TAP.$("ELE_BUTTON_ADD","//button[@id='workbenchForm:workbenchTabs:tb_buttonAdd']//span[@class='ui-button-text ui-c'][contains(text(),'Add')]","TGTYPESCREENNO");

		var_dataToPass = getJsonData(var_CSVData , "$.records["+var_Count+"].PaymentFrequency");
                 LOGGER.info("Executed Step = STORE,var_dataToPass,var_CSVData,$.records[{var_Count}].PaymentFrequency,TGTYPESCREENNO");
        TAP.$("ELE_DROPDWN_PAYFREQ","//label[@id='workbenchForm:workbenchTabs:paymentMode_label']","TGTYPESCREENNO");

        WAIT.$(2,"TGTYPESCREENNO");

		try { 
		 Thread.sleep(1000);
		WebElement selectValueOfDropDown= driver.findElement(By.xpath("//li[@data-label='"+ var_dataToPass +"']"));

				selectValueOfDropDown.click();
		// Add Screenshot will only work for Android platform

				 Thread.sleep(1000);
		 
		 ;} 
		 catch(Exception e) { PrintVal("Renderer Script Exception : "+e.toString()); } 
		 PrintVal("Executed Step = CALLRENDERERSCRIPT,SELECTVALUEFROMDROPDOWN"); 
		var_dataToPass = getJsonData(var_CSVData , "$.records["+var_Count+"].PaymentMethod");
                 LOGGER.info("Executed Step = STORE,var_dataToPass,var_CSVData,$.records[{var_Count}].PaymentMethod,TGTYPESCREENNO");
        TAP.$("ELE_DRPDWN_PAYMETHOD","//label[@id='workbenchForm:workbenchTabs:paymentCode_label']","TGTYPESCREENNO");

        WAIT.$(2,"TGTYPESCREENNO");

		try { 
		 Thread.sleep(1000);
		WebElement selectValueOfDropDown= driver.findElement(By.xpath("//li[@data-label='"+ var_dataToPass +"']"));

				selectValueOfDropDown.click();
		// Add Screenshot will only work for Android platform

				 Thread.sleep(1000);
		 
		 ;} 
		 catch(Exception e) { PrintVal("Renderer Script Exception : "+e.toString()); } 
		 PrintVal("Executed Step = CALLRENDERERSCRIPT,SELECTVALUEFROMDROPDOWN"); 
		var_dataToPass = getJsonData(var_CSVData , "$.records["+var_Count+"].IssueState");
                 LOGGER.info("Executed Step = STORE,var_dataToPass,var_CSVData,$.records[{var_Count}].IssueState,TGTYPESCREENNO");
        TAP.$("ELE_DRPDWN_ISSUESTATECOUNTRY","//label[@id='workbenchForm:workbenchTabs:countryAndStateCode_label']","TGTYPESCREENNO");

        WAIT.$(2,"TGTYPESCREENNO");

		try { 
		 Thread.sleep(1000);
		WebElement selectValueOfDropDown= driver.findElement(By.xpath("//li[@data-label='"+ var_dataToPass +"']"));

				selectValueOfDropDown.click();
		// Add Screenshot will only work for Android platform

				 Thread.sleep(1000);
		 
		 ;} 
		 catch(Exception e) { PrintVal("Renderer Script Exception : "+e.toString()); } 
		 PrintVal("Executed Step = CALLRENDERERSCRIPT,SELECTVALUEFROMDROPDOWN"); 
		var_dataToPass = var_TaxQualifiedDescription;
                 LOGGER.info("Executed Step = STORE,var_dataToPass,var_TaxQualifiedDescription,TGTYPESCREENNO");
        TAP.$("ELE_DRPDWN_TAXQUALIFIEDDESP","//label[@id='workbenchForm:workbenchTabs:taxQualifiedCode_label']","TGTYPESCREENNO");

        WAIT.$(2,"TGTYPESCREENNO");

		try { 
		 Thread.sleep(1000);
		WebElement selectValueOfDropDown= driver.findElement(By.xpath("//li[@data-label='"+ var_dataToPass +"']"));

				selectValueOfDropDown.click();
		// Add Screenshot will only work for Android platform

				 Thread.sleep(1000);
		 
		 ;} 
		 catch(Exception e) { PrintVal("Renderer Script Exception : "+e.toString()); } 
		 PrintVal("Executed Step = CALLRENDERERSCRIPT,SELECTVALUEFROMDROPDOWN"); 
		var_dataToPass = var_TaxWithholding;
                 LOGGER.info("Executed Step = STORE,var_dataToPass,var_TaxWithholding,TGTYPESCREENNO");
        TAP.$("ELE_DRPDWN_TAXWITHOLDING","//label[@id='workbenchForm:workbenchTabs:taxWithholdingCode_label']","TGTYPESCREENNO");

        WAIT.$(2,"TGTYPESCREENNO");

		try { 
		 Thread.sleep(1000);
		WebElement selectValueOfDropDown= driver.findElement(By.xpath("//li[@data-label='"+ var_dataToPass +"']"));

				selectValueOfDropDown.click();
		// Add Screenshot will only work for Android platform

				 Thread.sleep(1000);
		 
		 ;} 
		 catch(Exception e) { PrintVal("Renderer Script Exception : "+e.toString()); } 
		 PrintVal("Executed Step = CALLRENDERERSCRIPT,SELECTVALUEFROMDROPDOWN"); 
        WAIT.$(1,"TGTYPESCREENNO");

        TYPE.$("ELE_TXTBOX_LISTBILLNUMBER","//input[@id='workbenchForm:workbenchTabs:groupNumber']",var_ListBillNumber,"FALSE","TGTYPESCREENNO");

        SWIPE.$("UP","TGTYPESCREENNO");

        WAIT.$(1,"TGTYPESCREENNO");

        TAP.$("ELE_TXTBOX_EFFECTIVEDATE","//input[@id='workbenchForm:workbenchTabs:effectiveDate_input']","TGTYPESCREENNO");

        TYPE.$("ELE_TXTBOX_EFFECTIVEDATE","//input[@id='workbenchForm:workbenchTabs:effectiveDate_input']",var_EffectiveDate,"FALSE","TGTYPESCREENNO");

        WAIT.$(1,"TGTYPESCREENNO");

        TAP.$("ELE_TXTBOX_CASHWITHAPP","//input[@id='workbenchForm:workbenchTabs:cashWithApplication_input']","TGTYPESCREENNO");

        WAIT.$(2,"TGTYPESCREENNO");

		try { 
		 WebElement CashApplicationTextField = driver.findElement(By.xpath("//input[@id='workbenchForm:workbenchTabs:cashWithApplication_input']"));
		CashApplicationTextField.sendKeys(Keys.BACK_SPACE);
				CashApplicationTextField.sendKeys(Keys.BACK_SPACE);
				CashApplicationTextField.sendKeys(Keys.BACK_SPACE);	
		// Add Screenshot will only work for Android platform

				CashApplicationTextField.sendKeys(""+var_CashWithApplication+"");// Add Screenshot will only work for Android platform

		Thread.sleep(1000);
		 
		 ;} 
		 catch(Exception e) { PrintVal("Renderer Script Exception : "+e.toString()); } 
		 PrintVal("Executed Step = CALLRENDERERSCRIPT,CLEARCASHWITHAPPLICATIONTXTFIELD"); 
        SWIPE.$("UP","TGTYPESCREENNO");

        SWIPE.$("UP","TGTYPESCREENNO");

        TAP.$("ELE_TAP_AGENTNUMBERBOX","//div[@id='workbenchForm:workbenchTabs:grid']//tr[1]//td[1]","TGTYPESCREENNO");

        WAIT.$(1,"TGTYPESCREENNO");

        TYPE.$("ELE_TXTBOX_AGENTNUM","//input[@id='workbenchForm:workbenchTabs:grid:0:in_agentNumber_Col']",var_AgentNumber,"FALSE","TGTYPESCREENNO");

        WAIT.$(1,"TGTYPESCREENNO");

        TAP.$("ELE_TAP_AGENTNUMBER","//th[@id='workbenchForm:workbenchTabs:grid:agentNumber_Col_Col']//span","TGTYPESCREENNO");

		takeFullScreenShot();
                 LOGGER.info("Executed Step = TAKEFULLSCREENSHOT,TGTYPESCREENREG");
        CALL.$("AddClient","TGTYPESCREENREG");

		String var_FirstName = "null";
                 LOGGER.info("Executed Step = VAR,String,var_FirstName,null,TGTYPESCREENNO");
		String var_LastName = "null";
                 LOGGER.info("Executed Step = VAR,String,var_LastName,null,TGTYPESCREENNO");
		String var_SocialSecurityNumber = "null";
                 LOGGER.info("Executed Step = VAR,String,var_SocialSecurityNumber,null,TGTYPESCREENNO");
		String var_stateToPass = "null";
                 LOGGER.info("Executed Step = VAR,String,var_stateToPass,null,TGTYPESCREENNO");
		var_LastName = getJsonData(var_CSVData , "$.records["+var_Count+"].LastName");
                 LOGGER.info("Executed Step = STORE,var_LastName,var_CSVData,$.records[{var_Count}].LastName,TGTYPESCREENNO");
		var_FirstName = getJsonData(var_CSVData , "$.records["+var_Count+"].FirstName");
                 LOGGER.info("Executed Step = STORE,var_FirstName,var_CSVData,$.records[{var_Count}].FirstName,TGTYPESCREENNO");
		var_SocialSecurityNumber = getJsonData(var_CSVData , "$.records["+var_Count+"].SocialSecurityNumber");
                 LOGGER.info("Executed Step = STORE,var_SocialSecurityNumber,var_CSVData,$.records[{var_Count}].SocialSecurityNumber,TGTYPESCREENNO");
		String var_ClientNameSearch;
                 LOGGER.info("Executed Step = VAR,String,var_ClientNameSearch,TGTYPESCREENNO");
		var_ClientNameSearch = getJsonData(var_CSVData , "$.records["+var_Count+"].ClientNameSearch");
                 LOGGER.info("Executed Step = STORE,var_ClientNameSearch,var_CSVData,$.records[{var_Count}].ClientNameSearch,TGTYPESCREENNO");
		String var_ClientID;
                 LOGGER.info("Executed Step = VAR,String,var_ClientID,TGTYPESCREENNO");
		var_ClientID = getJsonData(var_CSVData , "$.records["+var_Count+"].ClientID");
                 LOGGER.info("Executed Step = STORE,var_ClientID,var_CSVData,$.records[{var_Count}].ClientID,TGTYPESCREENNO");
		String var_ClientBirthDate;
                 LOGGER.info("Executed Step = VAR,String,var_ClientBirthDate,TGTYPESCREENNO");
		var_ClientBirthDate = getJsonData(var_CSVData , "$.records["+var_Count+"].ClientDOB");
                 LOGGER.info("Executed Step = STORE,var_ClientBirthDate,var_CSVData,$.records[{var_Count}].ClientDOB,TGTYPESCREENNO");
		String var_Gender;
                 LOGGER.info("Executed Step = VAR,String,var_Gender,TGTYPESCREENREG");
		var_Gender = getJsonData(var_CSVData , "$.records["+var_Count+"].Gender");
                 LOGGER.info("Executed Step = STORE,var_Gender,var_CSVData,$.records[{var_Count}].Gender,TGTYPESCREENREG");
        TAP.$("ELE_BUTTON_SHOWLINK","//button[@id='workbenchForm:workbenchTabs:tb_buttonLink']","TGTYPESCREENREG");

        WAIT.$(1,"TGTYPESCREENNO");

        TAP.$("ELE_LINK_SELECTCLIENT","//span[contains(text(),'Select Client')]","TGTYPESCREENNO");

        WAIT.$(2,"TGTYPESCREENNO");

        TYPE.$("ELE_TXTBOX_SEARCH","//input[@id='workbenchForm:workbenchTabs:searchName']",var_ClientNameSearch,"FALSE","TGTYPESCREENNO");

        TAP.$("ELE_BUTTON_SEARCH","//span[contains(text(),'Search')]","TGTYPESCREENNO");

        WAIT.$(3,"TGTYPESCREENNO");

        TYPE.$("ELE_TEXTINPUT_SEARCHCLIENTNAME","//input[@id='workbenchForm:workbenchTabs:clientGrid:individualName_Col:filter']",var_LastName,"FALSE","TGTYPESCREENNO");

        WAIT.$(2,"TGTYPESCREENNO");

        TYPE.$("ELE_TXTBOX_IDNUMCLIENTSEARCH","//input[@id='workbenchForm:workbenchTabs:clientGrid:clientId_Col:filter']",var_ClientID,"FALSE","TGTYPESCREENNO");

        WAIT.$(3,"TGTYPESCREENNO");

        LOGGER.info("Executed Step = START IF");
        if (check("ELE_GETVAL_CLIENTNAMEGRIDLINE1","//span[@id='workbenchForm:workbenchTabs:clientGrid:0:individualName']","VISIBLE","TGTYPESCREENREG")) {
         LOGGER.info("Executed Step = IF,ELE_GETVAL_CLIENTNAMEGRIDLINE1,VISIBLE,TGTYPESCREENREG");
        TAP.$("ELE_LINK_CLIENTSEARCHEDNAMEGRID","//tbody[@id='workbenchForm:workbenchTabs:clientGrid_data']//tr","TGTYPESCREENNO");

        WAIT.$(3,"TGTYPESCREENNO");

        TAP.$("ELE_BUTTON_SELECTCLIENT","//button[@id='workbenchForm:workbenchTabs:button_select']","TGTYPESCREENNO");

        TAP.$("ELE_BUTTON_CONTINUE","//span[contains(text(),'Continue')]","TGTYPESCREENNO");

        WAIT.$(3,"TGTYPESCREENNO");

        }
         else {
          LOGGER.info("Executed Step = ELSE,TGTYPESCREENREG");
        ASSERT.$("ELE_GETVAL_NOTCLIENTFOUNDGRID","//td[contains(text(),'No records found.')]","CONTAINS","No records found","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_CANCELSEARCH","//button[@id='workbenchForm:workbenchTabs:button_cancel']","TGTYPESCREENNO");

        WAIT.$(2,"TGTYPESCREENNO");

        TAP.$("ELE_BUTTON_ADDCLIENT","//button[@id='workbenchForm:workbenchTabs:button_add']","TGTYPESCREENNO");

        TYPE.$("ELE_TXTBOX_FIRSTNAME","//input[@id='workbenchForm:workbenchTabs:nameFirst']",var_FirstName,"FALSE","TGTYPESCREENNO");

        TYPE.$("ELE_TXTBOX_LASTNAME","//input[@id='workbenchForm:workbenchTabs:nameLast']",var_LastName,"FALSE","TGTYPESCREENNO");

        SCROLL.$("ELE_TXTBOX_DOB","//input[@id='workbenchForm:workbenchTabs:dateOfBirth_input']","DOWN","TGTYPESCREENNO");

		String var_ClientDOB = "null";
                 LOGGER.info("Executed Step = VAR,String,var_ClientDOB,null,TGTYPESCREENNO");
		var_ClientDOB = getJsonData(var_CSVData , "$.records["+var_Count+"].ClientDOB");
                 LOGGER.info("Executed Step = STORE,var_ClientDOB,var_CSVData,$.records[{var_Count}].ClientDOB,TGTYPESCREENNO");
        TAP.$("ELE_TXTBOX_DOB","//input[@id='workbenchForm:workbenchTabs:dateOfBirth_input']","TGTYPESCREENNO");

        TYPE.$("ELE_TXTBOX_DOB","//input[@id='workbenchForm:workbenchTabs:dateOfBirth_input']",var_ClientDOB,"FALSE","TGTYPESCREENNO");

        LOGGER.info("Executed Step = START IF");
        if (check(var_Gender,"CONTAINS","Male","TGTYPESCREENNO")) {
         LOGGER.info("Executed Step = IF,var_Gender,CONTAINS,Male,TGTYPESCREENNO");
        TAP.$("ELE_RDOBTN_MALE","//body//tr[@class='ui-widget-content']//tr[@class='ui-widget-content']//tr[2]//td[1]//div[1]//div[2]//span[1]","TGTYPESCREENNO");

        }
         else {
          LOGGER.info("Executed Step = ELSE,TGTYPESCREENNO");
        TAP.$("ELE_RDOBTN_FEMALE","//tr[@class='ui-widget-content']//tr[1]//td[1]//div[1]//div[2]//span[1]","TGTYPESCREENNO");

        }
          LOGGER.info("Executed Step = ENDIF,TGTYPESCREENNO");
        SWIPE.$("DOWN","TGTYPESCREENNO");

        SWIPE.$("DOWN","TGTYPESCREENNO");

		var_dataToPass = "Social Security Number";
                 LOGGER.info("Executed Step = STORE,var_dataToPass,Social Security Number,TGTYPESCREENNO");
        TAP.$("ELE_DRPDWN_IDTYPE","//label[@id='workbenchForm:workbenchTabs:taxIdenUsag_label']","TGTYPESCREENNO");

        WAIT.$(2,"TGTYPESCREENNO");

		try { 
		 Thread.sleep(1000);
		WebElement selectValueOfDropDown= driver.findElement(By.xpath("//li[@data-label='"+ var_dataToPass +"']"));

				selectValueOfDropDown.click();
		// Add Screenshot will only work for Android platform

				 Thread.sleep(1000);
		 
		 ;} 
		 catch(Exception e) { PrintVal("Renderer Script Exception : "+e.toString()); } 
		 PrintVal("Executed Step = CALLRENDERERSCRIPT,SELECTVALUEFROMDROPDOWN"); 
        TYPE.$("ELE_TXTBOX_IDNUMBER","//input[@id='workbenchForm:workbenchTabs:identificationNumber']",var_SocialSecurityNumber,"FALSE","TGTYPESCREENNO");

		takeFullScreenShot();
                 LOGGER.info("Executed Step = TAKEFULLSCREENSHOT,TGTYPESCREENREG");
        TAP.$("ELE_BUTTON_VALIDATE","//span[contains(text(),'Validate')]","TGTYPESCREENNO");

        WAIT.$(2,"TGTYPESCREENNO");

        ASSERT.$("ELE_GETMSG_SUCCESS","//div[@id='workbenchForm:workbenchTabs:messages']//span[@class='ui-messages-info-summary']","CONTAINS","successfully","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SAVE","//span[contains(text(),'Save')]","TGTYPESCREENNO");

        WAIT.$(3,"TGTYPESCREENNO");

        ASSERT.$("ELE_GETMSG_SUCCESS","//div[@id='workbenchForm:workbenchTabs:messages']//span[@class='ui-messages-info-summary']","CONTAINS","successfully","TGTYPESCREENREG");

		String var_ClientCity = "null";
                 LOGGER.info("Executed Step = VAR,String,var_ClientCity,null,TGTYPESCREENNO");
		String var_ClientZipCode = "null";
                 LOGGER.info("Executed Step = VAR,String,var_ClientZipCode,null,TGTYPESCREENNO");
		String var_ClientAddressOne = "null";
                 LOGGER.info("Executed Step = VAR,String,var_ClientAddressOne,null,TGTYPESCREENNO");
		var_ClientCity = getJsonData(var_CSVData , "$.records["+var_Count+"].ClientCity");
                 LOGGER.info("Executed Step = STORE,var_ClientCity,var_CSVData,$.records[{var_Count}].ClientCity,TGTYPESCREENNO");
		var_ClientZipCode = getJsonData(var_CSVData , "$.records["+var_Count+"].ClientZipCode");
                 LOGGER.info("Executed Step = STORE,var_ClientZipCode,var_CSVData,$.records[{var_Count}].ClientZipCode,TGTYPESCREENNO");
		var_ClientAddressOne = getJsonData(var_CSVData , "$.records["+var_Count+"].ClientAddress");
                 LOGGER.info("Executed Step = STORE,var_ClientAddressOne,var_CSVData,$.records[{var_Count}].ClientAddress,TGTYPESCREENNO");
        TYPE.$("ELE_TXTBOX_ADDRESSLINE1","//input[@id='workbenchForm:workbenchTabs:addressLineOne']",var_ClientAddressOne,"FALSE","TGTYPESCREENNO");

        TYPE.$("ELE_TXTBOX_CITY","//input[@id='workbenchForm:workbenchTabs:addressCity']",var_ClientCity,"FALSE","TGTYPESCREENNO");

        SCROLL.$("ELE_TXTBOX_ZIPCODE","//input[@id='workbenchForm:workbenchTabs:zipCode']","DOWN","TGTYPESCREENNO");

		var_stateToPass = getJsonData(var_CSVData , "$.records["+var_Count+"].IssueState");
                 LOGGER.info("Executed Step = STORE,var_stateToPass,var_CSVData,$.records[{var_Count}].IssueState,TGTYPESCREENNO");
        TAP.$("ELE_DRPDWN_ADDRESSSTATE","//label[@id='workbenchForm:workbenchTabs:countryAndStateCode_label']","TGTYPESCREENNO");

        WAIT.$(2,"TGTYPESCREENNO");

		try { 
		 Thread.sleep(1000);
				// WebElement selectValueOfDropDown= driver.findElement(By.xpath("//li[@id='workbenchForm:workbenchTabs:'"+ var_stateToPass +"']"));
		// WebElement selectValueOfDropDown= driver.findElement(By.xpath("//ul[@id='workbenchForm:workbenchTabs:birthCountryAndStateCode_items']//li[@data-label='"+ var_stateToPass +"']")); BIRTH STATE DROPDWN


		 WebElement selectValueOfDropDown= driver.findElement(By.xpath("//ul[@id='workbenchForm:workbenchTabs:countryAndStateCode_items']//li[@data-label='"+ var_stateToPass +"']")); //Address state Dropdown


						// Add Screenshot will only work for Android platform

						selectValueOfDropDown.click();	
		// Add Screenshot will only work for Android platform


						 Thread.sleep(1000);
		 
		 ;} 
		 catch(Exception e) { PrintVal("Renderer Script Exception : "+e.toString()); } 
		 PrintVal("Executed Step = CALLRENDERERSCRIPT,SELECTVALUEFROMDRPDOWNWITHPRESELECTEDVALUES"); 
        TYPE.$("ELE_TXTBOX_ZIPCODE","//input[@id='workbenchForm:workbenchTabs:zipCode']",var_ClientZipCode,"FALSE","TGTYPESCREENNO");

		takeFullScreenShot();
                 LOGGER.info("Executed Step = TAKEFULLSCREENSHOT,TGTYPESCREENREG");
        TAP.$("ELE_BUTTON_VALIDATE","//span[contains(text(),'Validate')]","TGTYPESCREENNO");

        ASSERT.$("ELE_GETMSG_SUCCESS","//div[@id='workbenchForm:workbenchTabs:messages']//span[@class='ui-messages-info-summary']","CONTAINS","successfully","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SAVE","//span[contains(text(),'Save')]","TGTYPESCREENNO");

        WAIT.$(2,"TGTYPESCREENNO");

        ASSERT.$("ELE_GETMSG_SUCCESS","//div[@id='workbenchForm:workbenchTabs:messages']//span[@class='ui-messages-info-summary']","CONTAINS","successfully","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_CONTINUE","//span[contains(text(),'Continue')]","TGTYPESCREENNO");

        WAIT.$(3,"TGTYPESCREENNO");

        }
          LOGGER.info("Executed Step = ENDIF,TGTYPESCREENNO");
        CALL.$("SubmitPolicyforCreation","TGTYPESCREENREG");

		takeFullScreenShot();
                 LOGGER.info("Executed Step = TAKEFULLSCREENSHOT,TGTYPESCREENREG");
        TAP.$("ELE_BUTTON_VALIDATE","//span[contains(text(),'Validate')]","TGTYPESCREENNO");

        ASSERT.$("ELE_GETMSG_SUCCESS","//div[@id='workbenchForm:workbenchTabs:messages']//span[@class='ui-messages-info-summary']","CONTAINS","successfully","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SAVE","//span[contains(text(),'Save')]","TGTYPESCREENNO");

        WAIT.$(3,"TGTYPESCREENNO");

        ASSERT.$("ELE_GETVAL_POLICYSTATUSPENDINGISSUE","//span[@id='workbenchForm:workbenchTabs:contractStatusNewBusText']","CONTAINS","Pending","TGTYPESCREENREG");

		writeToCSV("Policy Number " , ActionWrapper.getWebElementValueForVariable("span[id='workbenchForm:workbenchTabs:policyNumber']", "CssSelector", 0, "span[id='workbenchForm:workbenchTabs:policyNumber']TGWEBCOMMACssSelectorTGWEBCOMMA0"));
                 LOGGER.info("Executed Step = WRITETOCSV,ELE_GETVAL_POLICYNUMBER,Policy Number,TGTYPESCREENNO");
		takeFullScreenShot();
                 LOGGER.info("Executed Step = TAKEFULLSCREENSHOT,TGTYPESCREENREG");
		var_Count = var_Count + ( 1 );
                 LOGGER.info("Executed Step = INCREASEVAR,var_Count,1,TGTYPESCREENREG");
    	}
          LOGGER.info("Executed Step = WEND,TGTYPESCREENREG");
    	resetAttemps();

    }


    @Test
    public void mm40regscn02policypayment() {

        // No need to declare / print method name here, handled by TestNg listener
        // declare("mm40regscn02policypayment");

		int var_Count = 0;
                 LOGGER.info("Executed Step = VAR,Integer,var_Count,0,TGTYPESCREENREG");
		com.jayway.jsonpath.ReadContext var_CSVData = getJsonData("C:/GIAS_Automation/4.0 MassMutual/InputData/MassMutual40_IP_Reg02PolicyPayment.csv");
                 LOGGER.info("Executed Step = VAR,Json,var_CSVData,C:/GIAS_Automation/4.0 MassMutual/InputData/MassMutual40_IP_Reg02PolicyPayment.csv,TGTYPESCREENREG");
		String var_PolicyNumber = "null";
                 LOGGER.info("Executed Step = VAR,String,var_PolicyNumber,null,TGTYPESCREENREG");
		var_PolicyNumber = getJsonData(var_CSVData , "$.records["+var_Count+"].PolicyNumber");
                 LOGGER.info("Executed Step = STORE,var_PolicyNumber,var_CSVData,$.records[{var_Count}].PolicyNumber,TGTYPESCREENREG");
		String var_dataToPass = "null";
                 LOGGER.info("Executed Step = VAR,String,var_dataToPass,null,TGTYPESCREENREG");
        LOGGER.info("Executed Step = REPEAT IF");
    	while (check(var_Count,"<",1,"TGTYPESCREENREG")) {
          LOGGER.info("Executed Step = WHILE,var_Count,<,1,TGTYPESCREENREG");
        CALL.$("LoginToGIAS4","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENNO");

        TYPE.$("ELE_TXTBOX_USERNAME","//input[@id='guestForm:username']","Mano","FALSE","TGTYPESCREENNO");

        TYPE.$("ELE_TXTBOX_PASSWORD","//input[@id='guestForm:password']","Cnx12345!","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_LOGIN","//button[@id='guestForm:loginButton']","TGTYPESCREENNO");

        CALL.$("PolicyInquiryPrePayment","TGTYPESCREENREG");

		String var_PremiumAmt;
                 LOGGER.info("Executed Step = VAR,String,var_PremiumAmt,TGTYPESCREENREG");
		var_PremiumAmt = ActionWrapper.getElementValue("ELE_GETVAL_MODALPREMIUM", "//span[@id='workbenchForm:workbenchTabs:modalPremium']");
                 LOGGER.info("Executed Step = STORE,var_PremiumAmt,ELE_GETVAL_MODALPREMIUM,TGTYPESCREENREG");
        TYPE.$("ELE_TXTBOX_SEARCHTASKS","//input[@id='workbenchForm:workbenchTabs:taskSearchContainer:taskSearchSearch']","Policy Inquiry","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_TASKSEARCH","//a[@id='workbenchForm:workbenchTabs:taskSearchContainer:tb_buttonSearch']//span[@class='fa fa-search fa-lg']","TGTYPESCREENREG");

        TAP.$("ELE_LINK_POLICYINQUIRY","//span[contains(text(),'Policy Inquiry')]","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_POLICYNUMBER","//input[@id='workbenchForm:workbenchTabs:policyNumber']",var_PolicyNumber,"FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_CONTINUE","//span[contains(text(),'Continue')]","TGTYPESCREENREG");

		String var_SuspenseBalance;
                 LOGGER.info("Executed Step = VAR,String,var_SuspenseBalance,TGTYPESCREENREG");
		var_SuspenseBalance = ActionWrapper.getElementValue("ELE_GETVAL_SUSPENSEBALANCE", "//span[@id='workbenchForm:workbenchTabs:suspenseBalance']");
                 LOGGER.info("Executed Step = STORE,var_SuspenseBalance,ELE_GETVAL_SUSPENSEBALANCE,TGTYPESCREENREG");
		String var_ModalPremium;
                 LOGGER.info("Executed Step = VAR,String,var_ModalPremium,TGTYPESCREENREG");
		var_ModalPremium = ActionWrapper.getElementValue("ELE_GETVAL_MODALPREMIUM", "//span[@id='workbenchForm:workbenchTabs:modalPremium']");
                 LOGGER.info("Executed Step = STORE,var_ModalPremium,ELE_GETVAL_MODALPREMIUM,TGTYPESCREENREG");
        WAIT.$(1,"TGTYPESCREENREG");

		takeFullScreenShot();
                 LOGGER.info("Executed Step = TAKEFULLSCREENSHOT,TGTYPESCREENREG");
        ASSERT.$("ELE_GETVAL_POLICYSTATUS","//span[@id='workbenchForm:workbenchTabs:statusText']","=","Premium Paying","TGTYPESCREENREG");

		String var_Paidtodate;
                 LOGGER.info("Executed Step = VAR,String,var_Paidtodate,TGTYPESCREENREG");
		var_Paidtodate = ActionWrapper.getElementValue("ELE_GETVAL_PAIDTODATE", "//span[@id='workbenchForm:workbenchTabs:paidToDate']");
                 LOGGER.info("Executed Step = STORE,var_Paidtodate,ELE_GETVAL_PAIDTODATE,TGTYPESCREENREG");
		String var_SusBalancegreater = "false";
                 LOGGER.info("Executed Step = VAR,String,var_SusBalancegreater,false,TGTYPESCREENREG");
		try { 
		 var_SuspenseBalance = var_SuspenseBalance.trim().replaceAll(",","");
				      var_ModalPremium   = var_ModalPremium.trim().replaceAll(",","");  
		double suspensebalancedoublevalue = Double.parseDouble(var_SuspenseBalance);
		                double modelpremiumdoublevalue = Double.parseDouble(var_ModalPremium);
		     System.out.println("Suspencebalance::" +suspensebalancedoublevalue );
		        System.out.println("primium::::::"+modelpremiumdoublevalue );

		               
		                if(suspensebalancedoublevalue >modelpremiumdoublevalue )
		                {
		var_SusBalancegreater ="true";
		                  //  Assert.assertTrue(false, "Suspence balance must be lest than ModeLpremium ");
		                }
		else{
		var_SusBalancegreater ="false";

		}
		 
		 ;} 
		 catch(Exception e) { PrintVal("Renderer Script Exception : "+e.toString()); } 
		 PrintVal("Executed Step = CALLRENDERERSCRIPT,SUSPENSEBALANCECOMPARE"); 
        LOGGER.info("Executed Step = START IF");
        if (check(var_SusBalancegreater,"=","false","TGTYPESCREENREG")) {
         LOGGER.info("Executed Step = IF,var_SusBalancegreater,=,false,TGTYPESCREENREG");
        CALL.$("AddSuspense3","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_GIASHOME","//span[text()='GIAS']","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_SEARCHTASKS","//input[@id='workbenchForm:workbenchTabs:taskSearchContainer:taskSearchSearch']","Suspense","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_TASKSEARCH","//a[@id='workbenchForm:workbenchTabs:taskSearchContainer:tb_buttonSearch']//span[@class='fa fa-search fa-lg']","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SUSPENSE","//button[@id='workbenchForm:workbenchTabs:taskSearchContainer:1:j_idt81']//span[@class='ui-button-text ui-c'][contains(text(),'Suspense')]","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_ADDSUSPENSE","//button[@id='workbenchForm:workbenchTabs:tb_buttonAdd']//span[@class='ui-button-text ui-c'][contains(text(),'Add')]","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_SUSPENCECONTROLNUMBER","//input[@id='workbenchForm:workbenchTabs:suspenseControlNumb']",var_PolicyNumber,"FALSE","TGTYPESCREENREG");

        WAIT.$(3,"TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_AMTOFCHANGE","//input[@id='workbenchForm:workbenchTabs:amountOfChange_input']",var_ModalPremium,"FALSE","TGTYPESCREENREG");

        TAP.$("ELE_DRPDWN_SUSPENSETYPE","//label[@id='workbenchForm:workbenchTabs:suspenseType_label']","TGTYPESCREENREG");

		try { 
		 Thread.sleep(1000);
		WebElement selectValueOfDropDown= driver.findElement(By.xpath("//li[@id='workbenchForm:workbenchTabs:suspenseType_11']"));

		takeScreenshot();

				selectValueOfDropDown.click();
		// Add Screenshot will only work for Android platform

		takeScreenshot();

				 Thread.sleep(1000);
		 
		 ;} 
		 catch(Exception e) { PrintVal("Renderer Script Exception : "+e.toString()); } 
		 PrintVal("Executed Step = CALLRENDERERSCRIPT,SELECTPREMIUMSUSPENSE"); 
        TYPE.$("ELE_TXTBOX_SUSPENSECOMMENT","//input[@id='workbenchForm:workbenchTabs:suspenseComment']","Add Suspense","FALSE","TGTYPESCREENREG");

        WAIT.$(1,"TGTYPESCREENREG");

		takeFullScreenShot();
                 LOGGER.info("Executed Step = TAKEFULLSCREENSHOT,TGTYPESCREENREG");
        TAP.$("ELE_BUTTON_VALIDATE","//span[contains(text(),'Validate')]","TGTYPESCREENREG");

        ASSERT.$("ELE_GETMSG_SUCCESS","//div[@id='workbenchForm:workbenchTabs:messages']//span[@class='ui-messages-info-summary']","CONTAINS","successfully","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SAVE","//span[contains(text(),'Save')]","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

		try { 
		 driver.switchTo().frame(0);
		driver.findElement(By.xpath("//form[@id='confirmDialogForm']")).click();
		Thread.sleep(1000);
		driver.findElement(By.xpath("//button[@id='j_idt9']")).click();
		// Add Screenshot will only work for Android platform

		takeScreenshot();

		 
		 ;} 
		 catch(Exception e) { PrintVal("Renderer Script Exception : "+e.toString()); } 
		 PrintVal("Executed Step = CALLRENDERERSCRIPT,CLICKYESONCONFIRMACTION"); 
        WAIT.$(2,"TGTYPESCREENREG");

        ASSERT.$("ELE_LABEL_ITEMSUCCESSFULLYADDED","//div[@id='workbenchForm:workbenchTabs:messages']//span[@class='ui-messages-info-summary'][contains(text(),'Item successfully added.')]","VISIBLE","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_GIASHOME","//span[text()='GIAS']","TGTYPESCREENREG");

        }
          LOGGER.info("Executed Step = ENDIF,TGTYPESCREENREG");
        TAP.$("ELE_BUTTON_GIASHOME","//span[text()='GIAS']","TGTYPESCREENREG");

        CALL.$("PolicyPayment3","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_SEARCHTASKS","//input[@id='workbenchForm:workbenchTabs:taskSearchContainer:taskSearchSearch']","Policy Payment","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_TASKSEARCH","//a[@id='workbenchForm:workbenchTabs:taskSearchContainer:tb_buttonSearch']//span[@class='fa fa-search fa-lg']","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_POLICYPAYMENT","//span[text()='Policy Payment']","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_POLICYNUMBER","//input[@id='workbenchForm:workbenchTabs:policyNumber']",var_PolicyNumber,"FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_CONTINUE","//span[contains(text(),'Continue')]","TGTYPESCREENREG");

        WAIT.$(1,"TGTYPESCREENREG");

        TAP.$("ELE_TAB_POLICYPAYMENT","//a[contains(text(),'Payment')]","TGTYPESCREENREG");

        WAIT.$(1,"TGTYPESCREENREG");

		String var_NetPaymentField = "false";
                 LOGGER.info("Executed Step = VAR,String,var_NetPaymentField,false,TGTYPESCREENREG");
		try { 
		 Boolean ispresent = driver.findElements(By.xpath("//input[@id='workbenchForm:workbenchTabs:netPremium_input']")).size()>0;
		            System.out.println("Is Present "+ispresent);

		if(ispresent){
		var_NetPaymentField = "true";
		}
		else{
		var_NetPaymentField ="false";
		}
		 
		 ;} 
		 catch(Exception e) { PrintVal("Renderer Script Exception : "+e.toString()); } 
		 PrintVal("Executed Step = CALLRENDERERSCRIPT,STARTNETPAYMENTFIELD"); 
        LOGGER.info("Executed Step = START IF");
        if (check(var_NetPaymentField,"=","true","TGTYPESCREENREG")) {
         LOGGER.info("Executed Step = IF,var_NetPaymentField,=,true,TGTYPESCREENREG");
        TYPE.$("ELE_TXTBOX_NETPAYMENT","//input[@id='workbenchForm:workbenchTabs:netPremium_input']",var_PremiumAmt,"FALSE","TGTYPESCREENREG");

        }
          LOGGER.info("Executed Step = ENDIF,TGTYPESCREENREG");
        WAIT.$(2,"TGTYPESCREENREG");

		takeFullScreenShot();
                 LOGGER.info("Executed Step = TAKEFULLSCREENSHOT,TGTYPESCREENREG");
        TAP.$("ELE_BUTTON_VALIDATE","//span[contains(text(),'Validate')]","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

        ASSERT.$("ELE_GETMSG_SUCCESS","//div[@id='workbenchForm:workbenchTabs:messages']//span[@class='ui-messages-info-summary']","CONTAINS","successfully","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SUBMIT","//span[contains(text(),'Submit')]","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

        ASSERT.$("ELE_GETVAL_SUCCESSMESSAGE","//div[@id='workbenchForm:workbenchTabs:messages']//span[@class='ui-messages-info-summary'][contains(text(),'Request completed successfully.')]","=","Request completed successfully.","TGTYPESCREENREG");

        CALL.$("PolicyInquiryPostPolicyPayment3","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_GIASHOME","//span[text()='GIAS']","TGTYPESCREENREG");

        WAIT.$(1,"TGTYPESCREENREG");

        SWIPE.$("DOWN","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_SEARCHTASKS","//input[@id='workbenchForm:workbenchTabs:taskSearchContainer:taskSearchSearch']","Policy Inquiry","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_TASKSEARCH","//a[@id='workbenchForm:workbenchTabs:taskSearchContainer:tb_buttonSearch']//span[@class='fa fa-search fa-lg']","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

        TAP.$("ELE_LINK_POLICYINQUIRY","//span[contains(text(),'Policy Inquiry')]","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_POLICYNUMBER","//input[@id='workbenchForm:workbenchTabs:policyNumber']",var_PolicyNumber,"FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_CONTINUE","//span[contains(text(),'Continue')]","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

		takeFullScreenShot();
                 LOGGER.info("Executed Step = TAKEFULLSCREENSHOT,TGTYPESCREENREG");
		writeToCSV ("Modal Premium " , ActionWrapper.getElementValue("ELE_GETVAL_MODALPREMIUM", "//span[@id='workbenchForm:workbenchTabs:modalPremium']"));
                 LOGGER.info("Executed Step = WRITETOCSV,ELE_GETVAL_MODALPREMIUM,Modal Premium,TGTYPESCREENREG");
		writeToCSV ("Paid to Date " , ActionWrapper.getElementValue("ELE_GETVAL_PAIDTODATE", "//span[@id='workbenchForm:workbenchTabs:paidToDate']"));
                 LOGGER.info("Executed Step = WRITETOCSV,ELE_GETVAL_PAIDTODATE,Paid to Date,TGTYPESCREENREG");
        TAP.$("ELE_BUTTON_SHOWLINK","//button[@id='workbenchForm:workbenchTabs:tb_buttonLink']","TGTYPESCREENREG");

        TAP.$("ELE_LINK_TRANSACTIONHISTORY","//span[text()='Transaction History']","TGTYPESCREENREG");

		takeFullScreenShot();
                 LOGGER.info("Executed Step = TAKEFULLSCREENSHOT,TGTYPESCREENREG");
        TAP.$("ELE_BUTTON_TRANSACTIONDATEHISTORY","//span[@id='workbenchForm:workbenchTabs:grid:0:transactionDate']","TGTYPESCREENREG");

		takeFullScreenShot();
                 LOGGER.info("Executed Step = TAKEFULLSCREENSHOT,TGTYPESCREENREG");
		var_Count = var_Count + ( 1 );
                 LOGGER.info("Executed Step = INCREASEVAR,var_Count,1,TGTYPESCREENREG");
    	}
          LOGGER.info("Executed Step = WEND,TGTYPESCREENREG");
    	resetAttemps();

    }


    @Test
    public void mm40regscn03freelook() {

        // No need to declare / print method name here, handled by TestNg listener
        // declare("mm40regscn03freelook");

		int var_Count = 0;
                 LOGGER.info("Executed Step = VAR,Integer,var_Count,0,TGTYPESCREENREG");
		com.jayway.jsonpath.ReadContext var_CSVData = getJsonData("C:/GIAS_Automation/4.0 MassMutual/InputData/MassMutual40_IP_Reg03FreeLook.csv");
                 LOGGER.info("Executed Step = VAR,Json,var_CSVData,C:/GIAS_Automation/4.0 MassMutual/InputData/MassMutual40_IP_Reg03FreeLook.csv,TGTYPESCREENREG");
		String var_PolicyNumber;
                 LOGGER.info("Executed Step = VAR,String,var_PolicyNumber,TGTYPESCREENREG");
		var_PolicyNumber = getJsonData(var_CSVData , "$.records["+var_Count+"].PolicyNumber");
                 LOGGER.info("Executed Step = STORE,var_PolicyNumber,var_CSVData,$.records[{var_Count}].PolicyNumber,TGTYPESCREENREG");
		String var_dataToPass = "null";
                 LOGGER.info("Executed Step = VAR,String,var_dataToPass,null,TGTYPESCREENREG");
        LOGGER.info("Executed Step = REPEAT IF");
    	while (check(var_Count,"<",0,"TGTYPESCREENREG")) {
          LOGGER.info("Executed Step = WHILE,var_Count,<,0,TGTYPESCREENREG");
        CALL.$("VerifyPolicyStatusforFreeLookCancellation","TGTYPESCREENREG");

		String var_TodaysDate;
                 LOGGER.info("Executed Step = VAR,String,var_TodaysDate,TGTYPESCREENREG");
		var_TodaysDate = "$.records[{Count}].TodaysDate";
                 LOGGER.info("Executed Step = STORE,var_TodaysDate,$.records[{Count}].TodaysDate,TGTYPESCREENREG");
        TAP.$("ELE_BUTTON_SESSIONDATE","//a[@id='headerForm:sessionDatelink']","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_NEWSESSIONDATE","//input[@id='sessionDateForm:sessionDateInput_input']",var_TodaysDate,"FALSE","TGTYPESCREENREG");

		takeFullScreenShot();
                 LOGGER.info("Executed Step = TAKEFULLSCREENSHOT,TGTYPESCREENREG");
        TAP.$("ELE_BUTTON_CHANGEDATE","//span[text()='Change Date']","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_SEARCHTASKS","//input[@id='workbenchForm:workbenchTabs:taskSearchContainer:taskSearchSearch']","Policy Contract","FALSE","TGTYPESCREENREG");

        WAIT.$(1,"TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_TASKSEARCH","//a[@id='workbenchForm:workbenchTabs:taskSearchContainer:tb_buttonSearch']//span[@class='fa fa-search fa-lg']","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

        TAP.$("ELE_LINK_POLICY_CONTRACT","//span[text()='Policy Contract']","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_POLICYNUMBER","//input[@id='workbenchForm:workbenchTabs:policyNumber']",var_PolicyNumber,"FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_CONTINUE","//span[contains(text(),'Continue')]","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

        ASSERT.$("ELE_GETVAL_POLICYSTATUS","//span[@id='workbenchForm:workbenchTabs:statusText']","CONTAINS","Premium Paying","TGTYPESCREENREG");

		takeFullScreenShot();
                 LOGGER.info("Executed Step = TAKEFULLSCREENSHOT,TGTYPESCREENREG");
		String var_PaidtoDate2;
                 LOGGER.info("Executed Step = VAR,String,var_PaidtoDate2,TGTYPESCREENREG");
		var_PaidtoDate2 = ActionWrapper.getElementValue("ELE_GETVAL_PAIDTODATE", "//span[@id='workbenchForm:workbenchTabs:paidToDate']");
                 LOGGER.info("Executed Step = STORE,var_PaidtoDate2,ELE_GETVAL_PAIDTODATE,TGTYPESCREENREG");
		String var_SettledDate;
                 LOGGER.info("Executed Step = VAR,String,var_SettledDate,TGTYPESCREENREG");
		var_SettledDate = ActionWrapper.getElementValue("ELE_GETVAL_SETTLEDATE", "//span[@id='workbenchForm:workbenchTabs:settleDate']");
                 LOGGER.info("Executed Step = STORE,var_SettledDate,ELE_GETVAL_SETTLEDATE,TGTYPESCREENREG");
		int var_NumberOfDays = 0;
                 LOGGER.info("Executed Step = VAR,Integer,var_NumberOfDays,0,TGTYPESCREENREG");
		try { 
		     WebElement dateHeader = driver.findElement(By.cssSelector("a[id='headerForm:sessionDatelink']"));
		            String HeaderDateStr = dateHeader.getText().trim();
		            java.text.SimpleDateFormat simpleDateFormat = new java.text.SimpleDateFormat("EEEE, MMMM dd, yyyy");
		            java.util.Date headerDate = null;

		            try {
		                headerDate = simpleDateFormat.parse(HeaderDateStr);
		            } catch (Exception e) {
		                e.printStackTrace();
		            }


		            java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat("MM/dd/yyyy");
		            try {
		                java.util.Date date = formatter.parse(var_SettledDate.trim());
		                java.util.Calendar c = java.util.Calendar.getInstance();

		                org.joda.time.LocalDate date1 = new org.joda.time.LocalDate(date);
		                org.joda.time.LocalDate date2 = new org.joda.time.LocalDate(headerDate);
		                var_NumberOfDays = org.joda.time.Days.daysBetween(date1,date2).getDays();



		            } catch (Exception e) {
		                e.printStackTrace();
		                System.out.println(e.getMessage());
		            }
		 
		 ;} 
		 catch(Exception e) { PrintVal("Renderer Script Exception : "+e.toString()); } 
		 PrintVal("Executed Step = CALLRENDERERSCRIPT,CALCULATENOOFDAYSBETWEENSYSTEMANDSETTLEDATE"); 
        LOGGER.info("Executed Step = START IF");
        if (check(var_NumberOfDays,"<=",37,"TGTYPESCREENREG")) {
         LOGGER.info("Executed Step = IF,var_NumberOfDays,<=,37,TGTYPESCREENREG");
        ASSERT.$(var_NumberOfDays,"<=",37,"TGTYPESCREENREG");

        CALL.$("ProcessFreeLookUpCancellation","TGTYPESCREENREG");

        TAP.$("ELE_TAB_MYDASHBOARD","//a[contains(text(),'My Dashboard')]","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_SEARCHTASKS","//input[@id='workbenchForm:workbenchTabs:taskSearchContainer:taskSearchSearch']","Free Look","FALSE","TGTYPESCREENREG");

        WAIT.$(1,"TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SEARCH","//span[contains(text(),'Search')]","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

        TAP.$("ELE_LINK_FREELOOKCANCELLATION","//span[contains(text(),'Free Look Cancellation')]","TGTYPESCREENREG");

        WAIT.$(1,"TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_POLICYNUMBER","//input[@id='workbenchForm:workbenchTabs:policyNumber']",var_PolicyNumber,"FALSE","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_CANCELLATIONDATE","//input[@id='workbenchForm:workbenchTabs:promptDate_input']",var_SessionDate,"FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_CONTINUE","//span[contains(text(),'Continue')]","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

		String var_ExpiryDate = "null";
                 LOGGER.info("Executed Step = VAR,String,var_ExpiryDate,null,TGTYPESCREENREG");
		String var_AmountToOwnerAct = "null";
                 LOGGER.info("Executed Step = VAR,String,var_AmountToOwnerAct,null,TGTYPESCREENREG");
		String var_ReturnMethod = "null";
                 LOGGER.info("Executed Step = VAR,String,var_ReturnMethod,null,TGTYPESCREENREG");
		var_ReturnMethod = ActionWrapper.getElementValue("ELE_GETVAL_RETURNMETHOD", "//span[@id='workbenchForm:workbenchTabs:freeLookTypeText']");
                 LOGGER.info("Executed Step = STORE,var_ReturnMethod,ELE_GETVAL_RETURNMETHOD,TGTYPESCREENREG");
		String var_TotalCancelValue = "null";
                 LOGGER.info("Executed Step = VAR,String,var_TotalCancelValue,null,TGTYPESCREENREG");
		var_TotalCancelValue = ActionWrapper.getElementValue("ELE_GETVAL_TOTALCANCELVALUE", "//span[@id='workbenchForm:workbenchTabs:totalCancellationValue']");
                 LOGGER.info("Executed Step = STORE,var_TotalCancelValue,ELE_GETVAL_TOTALCANCELVALUE,TGTYPESCREENREG");
		String var_SuspenseAmtRelease = "null";
                 LOGGER.info("Executed Step = VAR,String,var_SuspenseAmtRelease,null,TGTYPESCREENREG");
		var_SuspenseAmtRelease = ActionWrapper.getElementValue("ELE_GETVAL_SUSPAMTRELEASE", "//span[@id='workbenchForm:workbenchTabs:suspenseAmount']");
                 LOGGER.info("Executed Step = STORE,var_SuspenseAmtRelease,ELE_GETVAL_SUSPAMTRELEASE,TGTYPESCREENREG");
		String var_AmountToOwnerExp = "null";
                 LOGGER.info("Executed Step = VAR,String,var_AmountToOwnerExp,null,TGTYPESCREENREG");
		var_AmountToOwnerExp = ActionWrapper.getElementValue("ELE_GETVAL_AMOUNTTOOWNER", "//span[@id='workbenchForm:workbenchTabs:amountToOwner']");
                 LOGGER.info("Executed Step = STORE,var_AmountToOwnerExp,ELE_GETVAL_AMOUNTTOOWNER,TGTYPESCREENREG");
		String var_FreelukExpiryDateExp = "null";
                 LOGGER.info("Executed Step = VAR,String,var_FreelukExpiryDateExp,null,TGTYPESCREENREG");
		var_FreelukExpiryDateExp = ActionWrapper.getElementValue("ELE_GETVAL_FREELOOKEXPIRYDATE", "//span[@id='workbenchForm:workbenchTabs:freeLookExpiDay']");
                 LOGGER.info("Executed Step = STORE,var_FreelukExpiryDateExp,ELE_GETVAL_FREELOOKEXPIRYDATE,TGTYPESCREENREG");
		String var_CancellationDate = "null";
                 LOGGER.info("Executed Step = VAR,String,var_CancellationDate,null,TGTYPESCREENREG");
		var_CancellationDate = ActionWrapper.getElementValue("ELE_GETVAL_CANCELLATIONDATE", "//label[@id='workbenchForm:workbenchTabs:cancellationDate_lbl']");
                 LOGGER.info("Executed Step = STORE,var_CancellationDate,ELE_GETVAL_CANCELLATIONDATE,TGTYPESCREENREG");
        ASSERT.$(var_ReturnMethod,"=","Refund Fund Balance","TGTYPESCREENREG");

		try { 
		 Double AmountToOwner=0.00;
		AmountToOwner = Double.parseDouble(var_SuspenseAmtRelease.trim().replace(",", "")) + Double.parseDouble(var_TotalCancelValue.trim().replace(",", ""));

		var_AmountToOwnerAct = ""+AmountToOwner ;
		var_AmountToOwnerExp = var_AmountToOwner.trim().replace(",","");

		var_ExpiryDate = java.time.LocalDate.parse(var_CancellationDate).plusDays(1).toString();
		 
		 ;} 
		 catch(Exception e) { PrintVal("Renderer Script Exception : "+e.toString()); } 
		 PrintVal("Executed Step = CALLRENDERERSCRIPT,CALCTHEAMTTOOWNERFORCANCELLATION"); 
        ASSERT.$(var_FreelukExpiryDateExp,"=",var_ExpiryDate,"TGTYPESCREENREG");

        ASSERT.$(var_AmountToOwnerExp,"=",var_AmountToOwnerAct,"TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SHOWLINKS","//span[@class='ui-button-icon-left ui-icon ui-c fa fa-ellipsis-v fa-2x']","TGTYPESCREENREG");

        WAIT.$(1,"TGTYPESCREENREG");

        TAP.$("ELE_LINK_DISTRIBUTIONINFORMATION","//text()[.='Distribution Information']/ancestor::a[1]","TGTYPESCREENREG");

        WAIT.$(1,"TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_CONTINUE","//span[contains(text(),'Continue')]","TGTYPESCREENREG");

        WAIT.$(1,"TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SUBMIT","//span[contains(text(),'Submit')]","TGTYPESCREENREG");

        WAIT.$(3,"TGTYPESCREENREG");

        ASSERT.$("ELE_GETMSG_SUCCESS","//div[@id='workbenchForm:workbenchTabs:messages']//span[@class='ui-messages-info-summary']","CONTAINS","successfully","TGTYPESCREENREG");

        WAIT.$(1,"TGTYPESCREENREG");

        TAP.$("ELE_TAB_CLOSE0","span.ui-icon.ui-icon-close:nth-child(2)TGWEBCOMMACssSelectorTGWEBCOMMA0","TGTYPESCREENREG");

        WAIT.$(1,"TGTYPESCREENREG");

        }
         else {
          LOGGER.info("Executed Step = ELSE,TGTYPESCREENREG");
		try { 
		 Assert.assertTrue(false,"Policy settle date is greater than 37 days. Please enter policy less than or equal to 37 days from settle date ........");
		 
		 ;} 
		 catch(Exception e) { PrintVal("Renderer Script Exception : "+e.toString()); } 
		 PrintVal("Executed Step = CALLRENDERERSCRIPT,CHECKIFPOLICYISINFREELOOKPERIOD"); 
        }
          LOGGER.info("Executed Step = ENDIF,TGTYPESCREENREG");
        CALL.$("ProcessFreeLookUpCancellation","TGTYPESCREENREG");

        TAP.$("ELE_TAB_MYDASHBOARD","//a[contains(text(),'My Dashboard')]","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_SEARCHTASKS","//input[@id='workbenchForm:workbenchTabs:taskSearchContainer:taskSearchSearch']","Free Look","FALSE","TGTYPESCREENREG");

        WAIT.$(1,"TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SEARCH","//span[contains(text(),'Search')]","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

        TAP.$("ELE_LINK_FREELOOKCANCELLATION","//span[contains(text(),'Free Look Cancellation')]","TGTYPESCREENREG");

        WAIT.$(1,"TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_POLICYNUMBER","//input[@id='workbenchForm:workbenchTabs:policyNumber']",var_PolicyNumber,"FALSE","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_CANCELLATIONDATE","//input[@id='workbenchForm:workbenchTabs:promptDate_input']",var_SessionDate,"FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_CONTINUE","//span[contains(text(),'Continue')]","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

		String var_ExpiryDate = "null";
                 LOGGER.info("Executed Step = VAR,String,var_ExpiryDate,null,TGTYPESCREENREG");
		String var_AmountToOwnerAct = "null";
                 LOGGER.info("Executed Step = VAR,String,var_AmountToOwnerAct,null,TGTYPESCREENREG");
		String var_ReturnMethod = "null";
                 LOGGER.info("Executed Step = VAR,String,var_ReturnMethod,null,TGTYPESCREENREG");
		var_ReturnMethod = ActionWrapper.getElementValue("ELE_GETVAL_RETURNMETHOD", "//span[@id='workbenchForm:workbenchTabs:freeLookTypeText']");
                 LOGGER.info("Executed Step = STORE,var_ReturnMethod,ELE_GETVAL_RETURNMETHOD,TGTYPESCREENREG");
		String var_TotalCancelValue = "null";
                 LOGGER.info("Executed Step = VAR,String,var_TotalCancelValue,null,TGTYPESCREENREG");
		var_TotalCancelValue = ActionWrapper.getElementValue("ELE_GETVAL_TOTALCANCELVALUE", "//span[@id='workbenchForm:workbenchTabs:totalCancellationValue']");
                 LOGGER.info("Executed Step = STORE,var_TotalCancelValue,ELE_GETVAL_TOTALCANCELVALUE,TGTYPESCREENREG");
		String var_SuspenseAmtRelease = "null";
                 LOGGER.info("Executed Step = VAR,String,var_SuspenseAmtRelease,null,TGTYPESCREENREG");
		var_SuspenseAmtRelease = ActionWrapper.getElementValue("ELE_GETVAL_SUSPAMTRELEASE", "//span[@id='workbenchForm:workbenchTabs:suspenseAmount']");
                 LOGGER.info("Executed Step = STORE,var_SuspenseAmtRelease,ELE_GETVAL_SUSPAMTRELEASE,TGTYPESCREENREG");
		String var_AmountToOwnerExp = "null";
                 LOGGER.info("Executed Step = VAR,String,var_AmountToOwnerExp,null,TGTYPESCREENREG");
		var_AmountToOwnerExp = ActionWrapper.getElementValue("ELE_GETVAL_AMOUNTTOOWNER", "//span[@id='workbenchForm:workbenchTabs:amountToOwner']");
                 LOGGER.info("Executed Step = STORE,var_AmountToOwnerExp,ELE_GETVAL_AMOUNTTOOWNER,TGTYPESCREENREG");
		String var_FreelukExpiryDateExp = "null";
                 LOGGER.info("Executed Step = VAR,String,var_FreelukExpiryDateExp,null,TGTYPESCREENREG");
		var_FreelukExpiryDateExp = ActionWrapper.getElementValue("ELE_GETVAL_FREELOOKEXPIRYDATE", "//span[@id='workbenchForm:workbenchTabs:freeLookExpiDay']");
                 LOGGER.info("Executed Step = STORE,var_FreelukExpiryDateExp,ELE_GETVAL_FREELOOKEXPIRYDATE,TGTYPESCREENREG");
		String var_CancellationDate = "null";
                 LOGGER.info("Executed Step = VAR,String,var_CancellationDate,null,TGTYPESCREENREG");
		var_CancellationDate = ActionWrapper.getElementValue("ELE_GETVAL_CANCELLATIONDATE", "//label[@id='workbenchForm:workbenchTabs:cancellationDate_lbl']");
                 LOGGER.info("Executed Step = STORE,var_CancellationDate,ELE_GETVAL_CANCELLATIONDATE,TGTYPESCREENREG");
        ASSERT.$(var_ReturnMethod,"=","Refund Fund Balance","TGTYPESCREENREG");

		try { 
		 Double AmountToOwner=0.00;
		AmountToOwner = Double.parseDouble(var_SuspenseAmtRelease.trim().replace(",", "")) + Double.parseDouble(var_TotalCancelValue.trim().replace(",", ""));

		var_AmountToOwnerAct = ""+AmountToOwner ;
		var_AmountToOwnerExp = var_AmountToOwner.trim().replace(",","");

		var_ExpiryDate = java.time.LocalDate.parse(var_CancellationDate).plusDays(1).toString();
		 
		 ;} 
		 catch(Exception e) { PrintVal("Renderer Script Exception : "+e.toString()); } 
		 PrintVal("Executed Step = CALLRENDERERSCRIPT,CALCTHEAMTTOOWNERFORCANCELLATION"); 
        ASSERT.$(var_FreelukExpiryDateExp,"=",var_ExpiryDate,"TGTYPESCREENREG");

        ASSERT.$(var_AmountToOwnerExp,"=",var_AmountToOwnerAct,"TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SHOWLINKS","//span[@class='ui-button-icon-left ui-icon ui-c fa fa-ellipsis-v fa-2x']","TGTYPESCREENREG");

        WAIT.$(1,"TGTYPESCREENREG");

        TAP.$("ELE_LINK_DISTRIBUTIONINFORMATION","//text()[.='Distribution Information']/ancestor::a[1]","TGTYPESCREENREG");

        WAIT.$(1,"TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_CONTINUE","//span[contains(text(),'Continue')]","TGTYPESCREENREG");

        WAIT.$(1,"TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SUBMIT","//span[contains(text(),'Submit')]","TGTYPESCREENREG");

        WAIT.$(3,"TGTYPESCREENREG");

        ASSERT.$("ELE_GETMSG_SUCCESS","//div[@id='workbenchForm:workbenchTabs:messages']//span[@class='ui-messages-info-summary']","CONTAINS","successfully","TGTYPESCREENREG");

        WAIT.$(1,"TGTYPESCREENREG");

        TAP.$("ELE_TAB_CLOSE0","span.ui-icon.ui-icon-close:nth-child(2)TGWEBCOMMACssSelectorTGWEBCOMMA0","TGTYPESCREENREG");

        WAIT.$(1,"TGTYPESCREENREG");

		var_Count = var_Count + ( 1 );
                 LOGGER.info("Executed Step = INCREASEVAR,var_Count,1,TGTYPESCREENREG");
    	}
          LOGGER.info("Executed Step = WEND,TGTYPESCREENREG");
    	resetAttemps();

    }


    @Test
    public void mm40regscn04paymentfrequencymodechange() {

        // No need to declare / print method name here, handled by TestNg listener
        // declare("mm40regscn04paymentfrequencymodechange");

		int var_Count = 0;
                 LOGGER.info("Executed Step = VAR,Integer,var_Count,0,TGTYPESCREENREG");
		com.jayway.jsonpath.ReadContext var_CSVData = getJsonData("C:/GIAS_Automation/4.0 MassMutual/InputData/MassMutual40_IP_Reg04PaymentChange.csv");
                 LOGGER.info("Executed Step = VAR,Json,var_CSVData,C:/GIAS_Automation/4.0 MassMutual/InputData/MassMutual40_IP_Reg04PaymentChange.csv,TGTYPESCREENREG");
		String var_PolicyNumber;
                 LOGGER.info("Executed Step = VAR,String,var_PolicyNumber,TGTYPESCREENREG");
		var_PolicyNumber = getJsonData(var_CSVData , "$.records["+var_Count+"].PolicyNumber");
                 LOGGER.info("Executed Step = STORE,var_PolicyNumber,var_CSVData,$.records[{var_Count}].PolicyNumber,TGTYPESCREENREG");
		String var_dataToPass = "null";
                 LOGGER.info("Executed Step = VAR,String,var_dataToPass,null,TGTYPESCREENREG");
        LOGGER.info("Executed Step = REPEAT IF");
    	while (check(var_Count,"<",1,"TGTYPESCREENREG")) {
          LOGGER.info("Executed Step = WHILE,var_Count,<,1,TGTYPESCREENREG");
        CALL.$("VerifyPolicyStatusforModeFrequencyChange","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_SEARCHTASKS","//input[@id='workbenchForm:workbenchTabs:taskSearchContainer:taskSearchSearch']","Policy Inquiry","FALSE","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

        TAP.$("ELE_LINK_POLICYINQUIRY","//span[contains(text(),'Policy Inquiry')]","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_POLICYNUMBER","//input[@id='workbenchForm:workbenchTabs:policyNumber']",var_PolicyNumber,"FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_CONTINUE","//span[contains(text(),'Continue')]","TGTYPESCREENREG");

        ASSERT.$("ELE_GETVAL_POLICYSTATUS","//span[@id='workbenchForm:workbenchTabs:statusText']","CONTAINS","Premium Paying","TGTYPESCREENREG");

		takeFullScreenShot();
                 LOGGER.info("Executed Step = TAKEFULLSCREENSHOT,TGTYPESCREENREG");
        LOGGER.info("Executed Step = START IF");
        if (check("ELE_GETVAL_PAYMENTMODE","//span[@id='workbenchForm:workbenchTabs:paymentMethodText']","CONTAINS","Electronic Funds","TGTYPESCREENREG")) {
         LOGGER.info("Executed Step = IF,ELE_GETVAL_PAYMENTMODE,CONTAINS,Electronic Funds,TGTYPESCREENREG");
        CALL.$("PolicyModeFrequencyChange","TGTYPESCREENREG");

		String var_PaymentFrequency;
                 LOGGER.info("Executed Step = VAR,String,var_PaymentFrequency,TGTYPESCREENREG");
		var_PaymentFrequency = getJsonData(var_CSVData , "$.records["+var_Count+"].PaymentFrequency");
                 LOGGER.info("Executed Step = STORE,var_PaymentFrequency,var_CSVData,$.records[{var_Count}].PaymentFrequency,TGTYPESCREENREG");
		String var_PaymentMode;
                 LOGGER.info("Executed Step = VAR,String,var_PaymentMode,TGTYPESCREENREG");
		var_PaymentMode = getJsonData(var_CSVData , "$.records["+var_Count+"].PaymentMode");
                 LOGGER.info("Executed Step = STORE,var_PaymentMode,var_CSVData,$.records[{var_Count}].PaymentMode,TGTYPESCREENREG");
		String var_ListBillNumber;
                 LOGGER.info("Executed Step = VAR,String,var_ListBillNumber,TGTYPESCREENREG");
		var_ListBillNumber = getJsonData(var_CSVData , "$.records["+var_Count+"].ListBillNumber");
                 LOGGER.info("Executed Step = STORE,var_ListBillNumber,var_CSVData,$.records[{var_Count}].ListBillNumber,TGTYPESCREENREG");
        TYPE.$("ELE_TXTBOX_SEARCHTASKS","//input[@id='workbenchForm:workbenchTabs:taskSearchContainer:taskSearchSearch']","Policy Contract","FALSE","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

        TAP.$("ELE_LINK_POLICY_CONTRACT","//span[text()='Policy Contract']","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_POLICYNUMBER","//input[@id='workbenchForm:workbenchTabs:policyNumber']",var_PolicyNumber,"FALSE","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_CONTINUE","//span[contains(text(),'Continue')]","TGTYPESCREENREG");

		takeFullScreenShot();
                 LOGGER.info("Executed Step = TAKEFULLSCREENSHOT,TGTYPESCREENREG");
        TAP.$("ELE_BUTTON_EDIT","//span[text()='Edit']","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

        TAP.$("ELE_LABEL_PAYMENTFREQUENCY","//label[@id='workbenchForm:workbenchTabs:billingMode_label']","TGTYPESCREENREG");

        WAIT.$(1,"TGTYPESCREENREG");

		var_dataToPass = var_PaymentFrequency;
                 LOGGER.info("Executed Step = STORE,var_dataToPass,var_PaymentFrequency,TGTYPESCREENREG");
        WAIT.$(1,"TGTYPESCREENREG");

		try { 
		 WebElement dropdown = driver.findElement(By.xpath("//ul[@id='workbenchForm:workbenchTabs:billingMode_items']")); 


		       //ul[@id='workbenchForm:workbenchTabs:distributionChannel_items']
		       // dropdown.click();
		       
		        List<WebElement> options = dropdown.findElements(By.tagName("li"));
				
				System.out.println("Size"+options.size());
		        for (WebElement option : options)
		        {
					System.out.print("Values of option"+option);
		            if (option.getText().equals(var_dataToPass))
		            {
		                option.click(); // click the desired option
		                break;
		            }
		        }
		 
		 ;} 
		 catch(Exception e) { PrintVal("Renderer Script Exception : "+e.toString()); } 
		 PrintVal("Executed Step = CALLRENDERERSCRIPT,SELECTPAYMENTFREQUENCY"); 
        WAIT.$(1,"TGTYPESCREENREG");

		var_dataToPass = var_PaymentMode;
                 LOGGER.info("Executed Step = STORE,var_dataToPass,var_PaymentMode,TGTYPESCREENREG");
        TAP.$("ELE_DRPDWN_PAYMENTMODE_EFT","*[id='workbenchForm:workbenchTabs:paymentCode_3']","TGTYPESCREENREG");

        WAIT.$(1,"TGTYPESCREENREG");

		try { 
		 WebElement dropdown = driver.findElement(By.xpath("//ul[@id='workbenchForm:workbenchTabs:paymentCode_items']")); 


		       //ul[@id='workbenchForm:workbenchTabs:distributionChannel_items']
		       // dropdown.click();
		       
		        List<WebElement> options = dropdown.findElements(By.tagName("li"));
				
				System.out.println("Size"+options.size());
		        for (WebElement option : options)
		        {
					System.out.print("Values of option"+option);
		            if (option.getText().equals(var_dataToPass))
		            {
		                option.click(); // click the desired option
		                break;
		            }
		        }
		 
		 ;} 
		 catch(Exception e) { PrintVal("Renderer Script Exception : "+e.toString()); } 
		 PrintVal("Executed Step = CALLRENDERERSCRIPT,SELECTPAYMENTMETHOD"); 
        WAIT.$(1,"TGTYPESCREENREG");

        LOGGER.info("Executed Step = START IF");
        if (check(var_PaymentMode,"CONTAINS","List Bill","TGTYPESCREENREG")) {
         LOGGER.info("Executed Step = IF,var_PaymentMode,CONTAINS,List Bill,TGTYPESCREENREG");
        TYPE.$("ELE_TXTBOX_LISTBILLNUMBER","//input[@id='workbenchForm:workbenchTabs:groupNumber']",var_ListBillNumber,"FALSE","TGTYPESCREENREG");

        }
         else {
          LOGGER.info("Executed Step = ELSE,TGTYPESCREENREG");
        TAP.$("ELE_TXTBOX_LISTBILLNUMBER","//input[@id='workbenchForm:workbenchTabs:groupNumber']","TGTYPESCREENREG");

		try { 
		 WebElement ClearUnitOfInsurance= driver.findElement(By.xpath("//input[@id='workbenchForm:workbenchTabs:groupNumber']"));
				     ClearUnitOfInsurance.sendKeys(Keys.BACK_SPACE);
				     ClearUnitOfInsurance.sendKeys(Keys.BACK_SPACE);
				     ClearUnitOfInsurance.sendKeys(Keys.BACK_SPACE);	
		                     ClearUnitOfInsurance.sendKeys(Keys.BACK_SPACE);	
		                     ClearUnitOfInsurance.sendKeys(Keys.BACK_SPACE);				
				     
		 
		 ;} 
		 catch(Exception e) { PrintVal("Renderer Script Exception : "+e.toString()); } 
		 PrintVal("Executed Step = CALLRENDERERSCRIPT,CLEARLISTBILLNUMBER"); 
        }
          LOGGER.info("Executed Step = ENDIF,TGTYPESCREENREG");
		takeFullScreenShot();
                 LOGGER.info("Executed Step = TAKEFULLSCREENSHOT,TGTYPESCREENREG");
        TAP.$("ELE_BUTTON_VALIDATE","//span[contains(text(),'Validate')]","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

        ASSERT.$("ELE_GETMSG_SUCCESS","//div[@id='workbenchForm:workbenchTabs:messages']//span[@class='ui-messages-info-summary']","CONTAINS","successfully","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SAVE","//span[contains(text(),'Save')]","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

        ASSERT.$("ELE_GETMSG_SUCCESS","//div[@id='workbenchForm:workbenchTabs:messages']//span[@class='ui-messages-info-summary']","CONTAINS","successfully","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_GIASHOME","//span[text()='GIAS']","TGTYPESCREENREG");

        CALL.$("ValidatePaymentFrequencyModeChange","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_SEARCHTASKS","//input[@id='workbenchForm:workbenchTabs:taskSearchContainer:taskSearchSearch']","Policy Inquiry","FALSE","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

        TAP.$("ELE_LINK_POLICYINQUIRY","//span[contains(text(),'Policy Inquiry')]","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_POLICYNUMBER","//input[@id='workbenchForm:workbenchTabs:policyNumber']",var_PolicyNumber,"FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_CONTINUE","//span[contains(text(),'Continue')]","TGTYPESCREENREG");

        ASSERT.$("ELE_GETVAL_PAYMENTMODE","//span[@id='workbenchForm:workbenchTabs:paymentMethodText']","CONTAINS",var_PaymentMode,"TGTYPESCREENREG");

        ASSERT.$("ELE_GETVAL_PAYMENTFREQUENCY2","//span[@id='workbenchForm:workbenchTabs:billingModeText']","CONTAINS",var_PaymentFrequency,"TGTYPESCREENREG");

		takeFullScreenShot();
                 LOGGER.info("Executed Step = TAKEFULLSCREENSHOT,TGTYPESCREENREG");
        }
         else {
          LOGGER.info("Executed Step = ELSE,TGTYPESCREENREG");
        CALL.$("PolicyModeFrequencyChange","TGTYPESCREENREG");

		String var_PaymentFrequency;
                 LOGGER.info("Executed Step = VAR,String,var_PaymentFrequency,TGTYPESCREENREG");
		var_PaymentFrequency = getJsonData(var_CSVData , "$.records["+var_Count+"].PaymentFrequency");
                 LOGGER.info("Executed Step = STORE,var_PaymentFrequency,var_CSVData,$.records[{var_Count}].PaymentFrequency,TGTYPESCREENREG");
		String var_PaymentMode;
                 LOGGER.info("Executed Step = VAR,String,var_PaymentMode,TGTYPESCREENREG");
		var_PaymentMode = getJsonData(var_CSVData , "$.records["+var_Count+"].PaymentMode");
                 LOGGER.info("Executed Step = STORE,var_PaymentMode,var_CSVData,$.records[{var_Count}].PaymentMode,TGTYPESCREENREG");
		String var_ListBillNumber;
                 LOGGER.info("Executed Step = VAR,String,var_ListBillNumber,TGTYPESCREENREG");
		var_ListBillNumber = getJsonData(var_CSVData , "$.records["+var_Count+"].ListBillNumber");
                 LOGGER.info("Executed Step = STORE,var_ListBillNumber,var_CSVData,$.records[{var_Count}].ListBillNumber,TGTYPESCREENREG");
        TYPE.$("ELE_TXTBOX_SEARCHTASKS","//input[@id='workbenchForm:workbenchTabs:taskSearchContainer:taskSearchSearch']","Policy Contract","FALSE","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

        TAP.$("ELE_LINK_POLICY_CONTRACT","//span[text()='Policy Contract']","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_POLICYNUMBER","//input[@id='workbenchForm:workbenchTabs:policyNumber']",var_PolicyNumber,"FALSE","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_CONTINUE","//span[contains(text(),'Continue')]","TGTYPESCREENREG");

		takeFullScreenShot();
                 LOGGER.info("Executed Step = TAKEFULLSCREENSHOT,TGTYPESCREENREG");
        TAP.$("ELE_BUTTON_EDIT","//span[text()='Edit']","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

        TAP.$("ELE_LABEL_PAYMENTFREQUENCY","//label[@id='workbenchForm:workbenchTabs:billingMode_label']","TGTYPESCREENREG");

        WAIT.$(1,"TGTYPESCREENREG");

		var_dataToPass = var_PaymentFrequency;
                 LOGGER.info("Executed Step = STORE,var_dataToPass,var_PaymentFrequency,TGTYPESCREENREG");
        WAIT.$(1,"TGTYPESCREENREG");

		try { 
		 WebElement dropdown = driver.findElement(By.xpath("//ul[@id='workbenchForm:workbenchTabs:billingMode_items']")); 


		       //ul[@id='workbenchForm:workbenchTabs:distributionChannel_items']
		       // dropdown.click();
		       
		        List<WebElement> options = dropdown.findElements(By.tagName("li"));
				
				System.out.println("Size"+options.size());
		        for (WebElement option : options)
		        {
					System.out.print("Values of option"+option);
		            if (option.getText().equals(var_dataToPass))
		            {
		                option.click(); // click the desired option
		                break;
		            }
		        }
		 
		 ;} 
		 catch(Exception e) { PrintVal("Renderer Script Exception : "+e.toString()); } 
		 PrintVal("Executed Step = CALLRENDERERSCRIPT,SELECTPAYMENTFREQUENCY"); 
        WAIT.$(1,"TGTYPESCREENREG");

		var_dataToPass = var_PaymentMode;
                 LOGGER.info("Executed Step = STORE,var_dataToPass,var_PaymentMode,TGTYPESCREENREG");
        TAP.$("ELE_DRPDWN_PAYMENTMODE_EFT","*[id='workbenchForm:workbenchTabs:paymentCode_3']","TGTYPESCREENREG");

        WAIT.$(1,"TGTYPESCREENREG");

		try { 
		 WebElement dropdown = driver.findElement(By.xpath("//ul[@id='workbenchForm:workbenchTabs:paymentCode_items']")); 


		       //ul[@id='workbenchForm:workbenchTabs:distributionChannel_items']
		       // dropdown.click();
		       
		        List<WebElement> options = dropdown.findElements(By.tagName("li"));
				
				System.out.println("Size"+options.size());
		        for (WebElement option : options)
		        {
					System.out.print("Values of option"+option);
		            if (option.getText().equals(var_dataToPass))
		            {
		                option.click(); // click the desired option
		                break;
		            }
		        }
		 
		 ;} 
		 catch(Exception e) { PrintVal("Renderer Script Exception : "+e.toString()); } 
		 PrintVal("Executed Step = CALLRENDERERSCRIPT,SELECTPAYMENTMETHOD"); 
        WAIT.$(1,"TGTYPESCREENREG");

        LOGGER.info("Executed Step = START IF");
        if (check(var_PaymentMode,"CONTAINS","List Bill","TGTYPESCREENREG")) {
         LOGGER.info("Executed Step = IF,var_PaymentMode,CONTAINS,List Bill,TGTYPESCREENREG");
        TYPE.$("ELE_TXTBOX_LISTBILLNUMBER","//input[@id='workbenchForm:workbenchTabs:groupNumber']",var_ListBillNumber,"FALSE","TGTYPESCREENREG");

        }
         else {
          LOGGER.info("Executed Step = ELSE,TGTYPESCREENREG");
        TAP.$("ELE_TXTBOX_LISTBILLNUMBER","//input[@id='workbenchForm:workbenchTabs:groupNumber']","TGTYPESCREENREG");

		try { 
		 WebElement ClearUnitOfInsurance= driver.findElement(By.xpath("//input[@id='workbenchForm:workbenchTabs:groupNumber']"));
				     ClearUnitOfInsurance.sendKeys(Keys.BACK_SPACE);
				     ClearUnitOfInsurance.sendKeys(Keys.BACK_SPACE);
				     ClearUnitOfInsurance.sendKeys(Keys.BACK_SPACE);	
		                     ClearUnitOfInsurance.sendKeys(Keys.BACK_SPACE);	
		                     ClearUnitOfInsurance.sendKeys(Keys.BACK_SPACE);				
				     
		 
		 ;} 
		 catch(Exception e) { PrintVal("Renderer Script Exception : "+e.toString()); } 
		 PrintVal("Executed Step = CALLRENDERERSCRIPT,CLEARLISTBILLNUMBER"); 
        }
          LOGGER.info("Executed Step = ENDIF,TGTYPESCREENREG");
		takeFullScreenShot();
                 LOGGER.info("Executed Step = TAKEFULLSCREENSHOT,TGTYPESCREENREG");
        TAP.$("ELE_BUTTON_VALIDATE","//span[contains(text(),'Validate')]","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

        ASSERT.$("ELE_GETMSG_SUCCESS","//div[@id='workbenchForm:workbenchTabs:messages']//span[@class='ui-messages-info-summary']","CONTAINS","successfully","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SAVE","//span[contains(text(),'Save')]","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

        ASSERT.$("ELE_GETMSG_SUCCESS","//div[@id='workbenchForm:workbenchTabs:messages']//span[@class='ui-messages-info-summary']","CONTAINS","successfully","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_GIASHOME","//span[text()='GIAS']","TGTYPESCREENREG");

        CALL.$("ValidatePaymentFrequencyModeChange","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_SEARCHTASKS","//input[@id='workbenchForm:workbenchTabs:taskSearchContainer:taskSearchSearch']","Policy Inquiry","FALSE","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

        TAP.$("ELE_LINK_POLICYINQUIRY","//span[contains(text(),'Policy Inquiry')]","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_POLICYNUMBER","//input[@id='workbenchForm:workbenchTabs:policyNumber']",var_PolicyNumber,"FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_CONTINUE","//span[contains(text(),'Continue')]","TGTYPESCREENREG");

        ASSERT.$("ELE_GETVAL_PAYMENTMODE","//span[@id='workbenchForm:workbenchTabs:paymentMethodText']","CONTAINS",var_PaymentMode,"TGTYPESCREENREG");

        ASSERT.$("ELE_GETVAL_PAYMENTFREQUENCY2","//span[@id='workbenchForm:workbenchTabs:billingModeText']","CONTAINS",var_PaymentFrequency,"TGTYPESCREENREG");

		takeFullScreenShot();
                 LOGGER.info("Executed Step = TAKEFULLSCREENSHOT,TGTYPESCREENREG");
        }
          LOGGER.info("Executed Step = ENDIF,TGTYPESCREENREG");
        CALL.$("PolicyModeFrequencyChange","TGTYPESCREENREG");

		String var_PaymentFrequency;
                 LOGGER.info("Executed Step = VAR,String,var_PaymentFrequency,TGTYPESCREENREG");
		var_PaymentFrequency = getJsonData(var_CSVData , "$.records["+var_Count+"].PaymentFrequency");
                 LOGGER.info("Executed Step = STORE,var_PaymentFrequency,var_CSVData,$.records[{var_Count}].PaymentFrequency,TGTYPESCREENREG");
		String var_PaymentMode;
                 LOGGER.info("Executed Step = VAR,String,var_PaymentMode,TGTYPESCREENREG");
		var_PaymentMode = getJsonData(var_CSVData , "$.records["+var_Count+"].PaymentMode");
                 LOGGER.info("Executed Step = STORE,var_PaymentMode,var_CSVData,$.records[{var_Count}].PaymentMode,TGTYPESCREENREG");
		String var_ListBillNumber;
                 LOGGER.info("Executed Step = VAR,String,var_ListBillNumber,TGTYPESCREENREG");
		var_ListBillNumber = getJsonData(var_CSVData , "$.records["+var_Count+"].ListBillNumber");
                 LOGGER.info("Executed Step = STORE,var_ListBillNumber,var_CSVData,$.records[{var_Count}].ListBillNumber,TGTYPESCREENREG");
        TYPE.$("ELE_TXTBOX_SEARCHTASKS","//input[@id='workbenchForm:workbenchTabs:taskSearchContainer:taskSearchSearch']","Policy Contract","FALSE","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

        TAP.$("ELE_LINK_POLICY_CONTRACT","//span[text()='Policy Contract']","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_POLICYNUMBER","//input[@id='workbenchForm:workbenchTabs:policyNumber']",var_PolicyNumber,"FALSE","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_CONTINUE","//span[contains(text(),'Continue')]","TGTYPESCREENREG");

		takeFullScreenShot();
                 LOGGER.info("Executed Step = TAKEFULLSCREENSHOT,TGTYPESCREENREG");
        TAP.$("ELE_BUTTON_EDIT","//span[text()='Edit']","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

        TAP.$("ELE_LABEL_PAYMENTFREQUENCY","//label[@id='workbenchForm:workbenchTabs:billingMode_label']","TGTYPESCREENREG");

        WAIT.$(1,"TGTYPESCREENREG");

		var_dataToPass = var_PaymentFrequency;
                 LOGGER.info("Executed Step = STORE,var_dataToPass,var_PaymentFrequency,TGTYPESCREENREG");
        WAIT.$(1,"TGTYPESCREENREG");

		try { 
		 WebElement dropdown = driver.findElement(By.xpath("//ul[@id='workbenchForm:workbenchTabs:billingMode_items']")); 


		       //ul[@id='workbenchForm:workbenchTabs:distributionChannel_items']
		       // dropdown.click();
		       
		        List<WebElement> options = dropdown.findElements(By.tagName("li"));
				
				System.out.println("Size"+options.size());
		        for (WebElement option : options)
		        {
					System.out.print("Values of option"+option);
		            if (option.getText().equals(var_dataToPass))
		            {
		                option.click(); // click the desired option
		                break;
		            }
		        }
		 
		 ;} 
		 catch(Exception e) { PrintVal("Renderer Script Exception : "+e.toString()); } 
		 PrintVal("Executed Step = CALLRENDERERSCRIPT,SELECTPAYMENTFREQUENCY"); 
        WAIT.$(1,"TGTYPESCREENREG");

		var_dataToPass = var_PaymentMode;
                 LOGGER.info("Executed Step = STORE,var_dataToPass,var_PaymentMode,TGTYPESCREENREG");
        TAP.$("ELE_DRPDWN_PAYMENTMODE_EFT","*[id='workbenchForm:workbenchTabs:paymentCode_3']","TGTYPESCREENREG");

        WAIT.$(1,"TGTYPESCREENREG");

		try { 
		 WebElement dropdown = driver.findElement(By.xpath("//ul[@id='workbenchForm:workbenchTabs:paymentCode_items']")); 


		       //ul[@id='workbenchForm:workbenchTabs:distributionChannel_items']
		       // dropdown.click();
		       
		        List<WebElement> options = dropdown.findElements(By.tagName("li"));
				
				System.out.println("Size"+options.size());
		        for (WebElement option : options)
		        {
					System.out.print("Values of option"+option);
		            if (option.getText().equals(var_dataToPass))
		            {
		                option.click(); // click the desired option
		                break;
		            }
		        }
		 
		 ;} 
		 catch(Exception e) { PrintVal("Renderer Script Exception : "+e.toString()); } 
		 PrintVal("Executed Step = CALLRENDERERSCRIPT,SELECTPAYMENTMETHOD"); 
        WAIT.$(1,"TGTYPESCREENREG");

        LOGGER.info("Executed Step = START IF");
        if (check(var_PaymentMode,"CONTAINS","List Bill","TGTYPESCREENREG")) {
         LOGGER.info("Executed Step = IF,var_PaymentMode,CONTAINS,List Bill,TGTYPESCREENREG");
        TYPE.$("ELE_TXTBOX_LISTBILLNUMBER","//input[@id='workbenchForm:workbenchTabs:groupNumber']",var_ListBillNumber,"FALSE","TGTYPESCREENREG");

        }
         else {
          LOGGER.info("Executed Step = ELSE,TGTYPESCREENREG");
        TAP.$("ELE_TXTBOX_LISTBILLNUMBER","//input[@id='workbenchForm:workbenchTabs:groupNumber']","TGTYPESCREENREG");

		try { 
		 WebElement ClearUnitOfInsurance= driver.findElement(By.xpath("//input[@id='workbenchForm:workbenchTabs:groupNumber']"));
				     ClearUnitOfInsurance.sendKeys(Keys.BACK_SPACE);
				     ClearUnitOfInsurance.sendKeys(Keys.BACK_SPACE);
				     ClearUnitOfInsurance.sendKeys(Keys.BACK_SPACE);	
		                     ClearUnitOfInsurance.sendKeys(Keys.BACK_SPACE);	
		                     ClearUnitOfInsurance.sendKeys(Keys.BACK_SPACE);				
				     
		 
		 ;} 
		 catch(Exception e) { PrintVal("Renderer Script Exception : "+e.toString()); } 
		 PrintVal("Executed Step = CALLRENDERERSCRIPT,CLEARLISTBILLNUMBER"); 
        }
          LOGGER.info("Executed Step = ENDIF,TGTYPESCREENREG");
		takeFullScreenShot();
                 LOGGER.info("Executed Step = TAKEFULLSCREENSHOT,TGTYPESCREENREG");
        TAP.$("ELE_BUTTON_VALIDATE","//span[contains(text(),'Validate')]","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

        ASSERT.$("ELE_GETMSG_SUCCESS","//div[@id='workbenchForm:workbenchTabs:messages']//span[@class='ui-messages-info-summary']","CONTAINS","successfully","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SAVE","//span[contains(text(),'Save')]","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

        ASSERT.$("ELE_GETMSG_SUCCESS","//div[@id='workbenchForm:workbenchTabs:messages']//span[@class='ui-messages-info-summary']","CONTAINS","successfully","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_GIASHOME","//span[text()='GIAS']","TGTYPESCREENREG");

		var_Count = var_Count + ( 1 );
                 LOGGER.info("Executed Step = INCREASEVAR,var_Count,1,TGTYPESCREENREG");
    	}
          LOGGER.info("Executed Step = WEND,TGTYPESCREENREG");
    	resetAttemps();

    }


    @Test
    public void mm40regscnapplyfortheloan() {

        // No need to declare / print method name here, handled by TestNg listener
        // declare("mm40regscnapplyfortheloan");

		int var_Count = 0;
                 LOGGER.info("Executed Step = VAR,Integer,var_Count,0,TGTYPESCREENREG");
		com.jayway.jsonpath.ReadContext var_CSVData = getJsonData("C:/GIAS_Automation/4.0 MassMutual/InputData/MassMutual40_IP_Reg04PaymentChange.csv");
                 LOGGER.info("Executed Step = VAR,Json,var_CSVData,C:/GIAS_Automation/4.0 MassMutual/InputData/MassMutual40_IP_Reg04PaymentChange.csv,TGTYPESCREENREG");
		String var_PolicyNumber;
                 LOGGER.info("Executed Step = VAR,String,var_PolicyNumber,TGTYPESCREENREG");
		var_PolicyNumber = getJsonData(var_CSVData , "$.records["+var_Count+"].PolicyNumber");
                 LOGGER.info("Executed Step = STORE,var_PolicyNumber,var_CSVData,$.records[{var_Count}].PolicyNumber,TGTYPESCREENREG");
		String var_dataToPass = "null";
                 LOGGER.info("Executed Step = VAR,String,var_dataToPass,null,TGTYPESCREENREG");
        LOGGER.info("Executed Step = REPEAT IF");
    	while (check(var_Count,"<",1,"TGTYPESCREENREG")) {
          LOGGER.info("Executed Step = WHILE,var_Count,<,1,TGTYPESCREENREG");
        CALL.$("VerifyPolicyStatusforApplytheLoan","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_TASKSEARCH","//a[@id='workbenchForm:workbenchTabs:taskSearchContainer:tb_buttonSearch']//span[@class='fa fa-search fa-lg']","TGTYPESCREENREG");

		int var_Count = 0;
                 LOGGER.info("Executed Step = VAR,Integer,var_Count,0,TGTYPESCREENREG");
		com.jayway.jsonpath.ReadContext var_CSVData = getJsonData("C:/GIAS_Automation/4.0 MassMutual/InputData/MassMutual40_IP_Reg10ApplytheLoan.csv");
                 LOGGER.info("Executed Step = VAR,Json,var_CSVData,C:/GIAS_Automation/4.0 MassMutual/InputData/MassMutual40_IP_Reg10ApplytheLoan.csv,TGTYPESCREENREG");
		String var_PolicyNumber;
                 LOGGER.info("Executed Step = VAR,String,var_PolicyNumber,TGTYPESCREENREG");
		var_PolicyNumber = getJsonData(var_CSVData , "$.records.["+var_Count+"].PolicyNumber");
                 LOGGER.info("Executed Step = STORE,var_PolicyNumber,var_CSVData,$.records.[{var_Count}].PolicyNumber,TGTYPESCREENREG");
		String var_RequestedLoanAmount;
                 LOGGER.info("Executed Step = VAR,String,var_RequestedLoanAmount,TGTYPESCREENREG");
		var_RequestedLoanAmount = getJsonData(var_CSVData , "$.records.["+var_Count+"].RequestedLoanAmount");
                 LOGGER.info("Executed Step = STORE,var_RequestedLoanAmount,var_CSVData,$.records.[{var_Count}].RequestedLoanAmount,TGTYPESCREENREG");
        TYPE.$("ELE_TXTBOX_SEARCHTASKS","//input[@id='workbenchForm:workbenchTabs:taskSearchContainer:taskSearchSearch']","Policy Inquiry","FALSE","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

        TAP.$("ELE_LINK_POLICYINQUIRY","//span[contains(text(),'Policy Inquiry')]","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_POLICYNUMBER","//input[@id='workbenchForm:workbenchTabs:policyNumber']",var_PolicyNumber,"FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_CONTINUE","//span[contains(text(),'Continue')]","TGTYPESCREENREG");

        ASSERT.$("ELE_GETVAL_POLICYSTATUS","//span[@id='workbenchForm:workbenchTabs:statusText']","CONTAINS","Premium Paying","TGTYPESCREENREG");

		takeFullScreenShot();
                 LOGGER.info("Executed Step = TAKEFULLSCREENSHOT,TGTYPESCREENREG");
		var_Count = var_Count + ( 1 );
                 LOGGER.info("Executed Step = INCREASEVAR,var_Count,1,TGTYPESCREENREG");
    	}
          LOGGER.info("Executed Step = WEND,TGTYPESCREENREG");
    	resetAttemps();

    }


    @Test
    public void mm40reg04premiumapplicationsuspensemaintenancedelete() {

        // No need to declare / print method name here, handled by TestNg listener
        // declare("mm40reg04premiumapplicationsuspensemaintenancedelete");

        CALL.$("LoginToGIAS4","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENNO");

        TYPE.$("ELE_TXTBOX_USERNAME","//input[@id='guestForm:username']","Mano","FALSE","TGTYPESCREENNO");

        TYPE.$("ELE_TXTBOX_PASSWORD","//input[@id='guestForm:password']","Cnx12345!","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_LOGIN","//button[@id='guestForm:loginButton']","TGTYPESCREENNO");

        CALL.$("PolicyPreInquiryPremiumApplication","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_TASKSEARCH","//a[@id='workbenchForm:workbenchTabs:taskSearchContainer:tb_buttonSearch']//span[@class='fa fa-search fa-lg']","TGTYPESCREENREG");

		int var_Count = 0;
                 LOGGER.info("Executed Step = VAR,Integer,var_Count,0,TGTYPESCREENREG");
		com.jayway.jsonpath.ReadContext var_CSVData = getJsonData("C:/GIAS_Automation/4.0 MassMutual/InputData/MassMutual40_IP_Reg04PremiumApplicationSuspenseMaintenance.csv");
                 LOGGER.info("Executed Step = VAR,Json,var_CSVData,C:/GIAS_Automation/4.0 MassMutual/InputData/MassMutual40_IP_Reg04PremiumApplicationSuspenseMaintenance.csv,TGTYPESCREENREG");
		String var_PolicyNumber;
                 LOGGER.info("Executed Step = VAR,String,var_PolicyNumber,TGTYPESCREENREG");
		var_PolicyNumber = getJsonData(var_CSVData , "$.records["+var_Count+"].PolicyNumber");
                 LOGGER.info("Executed Step = STORE,var_PolicyNumber,var_CSVData,$.records[{var_Count}].PolicyNumber,TGTYPESCREENREG");
		String var_AmountofChange;
                 LOGGER.info("Executed Step = VAR,String,var_AmountofChange,TGTYPESCREENREG");
		var_AmountofChange = getJsonData(var_CSVData , "$.records.["+var_Count+"].AmountofChange");
                 LOGGER.info("Executed Step = STORE,var_AmountofChange,var_CSVData,$.records.[{var_Count}].AmountofChange,TGTYPESCREENREG");
		String var_CommentsSuspence;
                 LOGGER.info("Executed Step = VAR,String,var_CommentsSuspence,TGTYPESCREENREG");
		var_CommentsSuspence = getJsonData(var_CSVData , "$.records.["+var_Count+"].CommentsSuspence");
                 LOGGER.info("Executed Step = STORE,var_CommentsSuspence,var_CSVData,$.records.[{var_Count}].CommentsSuspence,TGTYPESCREENREG");
        TYPE.$("ELE_TXTBOX_SEARCHTASKS","//input[@id='workbenchForm:workbenchTabs:taskSearchContainer:taskSearchSearch']","Policy Inquiry","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_TASKSEARCH","//a[@id='workbenchForm:workbenchTabs:taskSearchContainer:tb_buttonSearch']//span[@class='fa fa-search fa-lg']","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_POLICYNUMBER","//input[@id='workbenchForm:workbenchTabs:policyNumber']",var_PolicyNumber,"FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_CONTINUE","//span[contains(text(),'Continue')]","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        WAIT.$("ELE_GETVAL_POLICYSTATUS","//span[@id='workbenchForm:workbenchTabs:statusText']","VISIBLE","TGTYPESCREENREG");

		String var_PaidtoDate;
                 LOGGER.info("Executed Step = VAR,String,var_PaidtoDate,TGTYPESCREENREG");
		var_PaidtoDate = ActionWrapper.getElementValue("ELE_GETVAL_PAIDTODATE", "//span[@id='workbenchForm:workbenchTabs:paidToDate']");
                 LOGGER.info("Executed Step = STORE,var_PaidtoDate,ELE_GETVAL_PAIDTODATE,TGTYPESCREENREG");
		takeFullScreenShot();
                 LOGGER.info("Executed Step = TAKEFULLSCREENSHOT,TGTYPESCREENREG");
        TAP.$("ELE_LINK_GIASHOME","//button[@id='headerForm:j_idt11']//span[@class='ui-button-text ui-c'][contains(text(),'GIAS')]","TGTYPESCREENREG");

        CALL.$("AddSuspensePremiumApplicationSuspenseMaintenance","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_SEARCHTASKS","//input[@id='workbenchForm:workbenchTabs:taskSearchContainer:taskSearchSearch']","Suspense","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_TASKSEARCH","//a[@id='workbenchForm:workbenchTabs:taskSearchContainer:tb_buttonSearch']//span[@class='fa fa-search fa-lg']","TGTYPESCREENREG");

        TAP.$("ELE_LINK_SUSPENSEMAINTENCE","//span[contains(text(),'Suspense Maintenance')]","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_ADD","//button[@id='workbenchForm:workbenchTabs:tb_buttonAdd']//span[@class='ui-button-text ui-c'][contains(text(),'Add')]","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_SUSPENCECONTROLNUMBER","//input[@id='workbenchForm:workbenchTabs:suspenseControlNumb']",var_PolicyNumber,"FALSE","TGTYPESCREENREG");

        TAP.$("ELE_DROPDWN_TYPEADDSUSPENSE","//div[@id='workbenchForm:workbenchTabs:suspenseType']//div[@class='ui-selectonemenu-trigger ui-state-default ui-corner-right']","TGTYPESCREENREG");

        TAP.$("ELE_DROPDWN_SELECTPREMIUMSUSPENSEADDSUSPENSE","//li[@id='workbenchForm:workbenchTabs:suspenseType_11']","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_SUSPENCEAMOUNTCHANGE","//input[@id='workbenchForm:workbenchTabs:amountOfChange_input']",var_AmountofChange,"FALSE","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_SUSPENSECOMMENT","//input[@id='workbenchForm:workbenchTabs:suspenseComment']",var_CommentsSuspence,"FALSE","TGTYPESCREENREG");

        TAP.$("ELE_RADIOBUTTON_GENERALLEDGER","//div[@class='ui-radiobutton-box ui-widget ui-corner-all ui-state-default ui-state-active']","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_VALIDATE","//span[contains(text(),'Validate')]","TGTYPESCREENREG");

        LOGGER.info("Executed Step = START IF");
        if (check("ELE_MSG_SUSPENSEBALANCEALREADYEXISTSFORTHISSUSPENSE","//span[text()='Suspense balance already exists for this suspense type.']","VISIBLE","TGTYPESCREENREG")) {
         LOGGER.info("Executed Step = IF,ELE_MSG_SUSPENSEBALANCEALREADYEXISTSFORTHISSUSPENSE,VISIBLE,TGTYPESCREENREG");
        TAP.$("ELE_BUTTON_SAVE","//span[contains(text(),'Save')]","TGTYPESCREENREG");

		try { 
		 		takeFullScreenShot();		 		 
		driver.switchTo().frame(0);
		         driver.findElement(By.xpath("//span[text()='Yes']")).click(); 
		takeFullScreenShot();
		 
		 ;} 
		 catch(Exception e) { PrintVal("Renderer Script Exception : "+e.toString()); } 
		 PrintVal("Executed Step = CALLRENDERERSCRIPT,SELECTYESBUTTON"); 
        }
         else {
          LOGGER.info("Executed Step = ELSE,TGTYPESCREENREG");
        ASSERT.$("ELE_MSG_VALIDATIONSUCCESS","//div[@id='workbenchForm:workbenchTabs:messages']//span[@class='ui-messages-info-summary'][contains(text(),'Validation completed successfully with no errors.')]","VISIBLE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SAVE","//span[contains(text(),'Save')]","TGTYPESCREENREG");

        ASSERT.$("ELE_LABEL_ITEMSUCCESSFULLYADDED","//div[@id='workbenchForm:workbenchTabs:messages']//span[@class='ui-messages-info-summary'][contains(text(),'Item successfully added.')]","VISIBLE","TGTYPESCREENREG");

        }
          LOGGER.info("Executed Step = ENDIF,TGTYPESCREENREG");
        TAP.$("ELE_BUTTON_GIASHOME","//span[text()='GIAS']","TGTYPESCREENREG");

        CALL.$("PolicyPremPay","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_SEARCHTASKS","//input[@id='workbenchForm:workbenchTabs:taskSearchContainer:taskSearchSearch']","Policy Payment","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_TASKSEARCH","//a[@id='workbenchForm:workbenchTabs:taskSearchContainer:tb_buttonSearch']//span[@class='fa fa-search fa-lg']","TGTYPESCREENREG");

        TAP.$("ELE_LINKNEW_POLICYPAYMENT","//button[@id='workbenchForm:workbenchTabs:taskSearchContainer:2:j_idt84']//span[@class='ui-button-text ui-c'][contains(text(),'Policy Payment')]","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_POLICYNUMBER","//input[@id='workbenchForm:workbenchTabs:policyNumber']",var_PolicyNumber,"FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_CONTINUE","//span[contains(text(),'Continue')]","TGTYPESCREENREG");

		String var_ModalPremium;
                 LOGGER.info("Executed Step = VAR,String,var_ModalPremium,TGTYPESCREENREG");
		var_ModalPremium = ActionWrapper.getElementValue("ELE_GETVAL_MODALPREMIUM", "//span[@id='workbenchForm:workbenchTabs:modalPremium']");
                 LOGGER.info("Executed Step = STORE,var_ModalPremium,ELE_GETVAL_MODALPREMIUM,TGTYPESCREENREG");
		String var_NetPayment;
                 LOGGER.info("Executed Step = VAR,String,var_NetPayment,TGTYPESCREENREG");
		var_NetPayment = ActionWrapper.getElementValue("ELE_LABEL_NETPAYMENT", "//span[@id='workbenchForm:workbenchTabs:netPremium']");
                 LOGGER.info("Executed Step = STORE,var_NetPayment,ELE_LABEL_NETPAYMENT,TGTYPESCREENREG");
        ASSERT.$(var_NetPayment,"=",var_ModalPremium,"TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_DATETOAPPLYPOLICYPAYMENT","//input[@id='workbenchForm:workbenchTabs:applyDate_input']",var_PaidtoDate,"FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SUBMIT","//span[contains(text(),'Submit')]","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

		try { 
		 		takeFullScreenShot();		 		 
		driver.switchTo().frame(0);
		         driver.findElement(By.xpath("//span[text()='Yes']")).click(); 
		takeFullScreenShot();
		 
		 ;} 
		 catch(Exception e) { PrintVal("Renderer Script Exception : "+e.toString()); } 
		 PrintVal("Executed Step = CALLRENDERERSCRIPT,SELECTYESBUTTON"); 
        WAIT.$(5,"TGTYPESCREENREG");

		takeFullScreenShot();
                 LOGGER.info("Executed Step = TAKEFULLSCREENSHOT,TGTYPESCREENREG");
        TAP.$("ELE_BUTTON_GIASHOME","//span[text()='GIAS']","TGTYPESCREENREG");


    }


    @Test
    public void mm40reg07fullsurrender() {

        // No need to declare / print method name here, handled by TestNg listener
        // declare("mm40reg07fullsurrender");

        CALL.$("LoginToGIAS4","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENNO");

        TYPE.$("ELE_TXTBOX_USERNAME","//input[@id='guestForm:username']","Mano","FALSE","TGTYPESCREENNO");

        TYPE.$("ELE_TXTBOX_PASSWORD","//input[@id='guestForm:password']","Cnx12345!","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_LOGIN","//button[@id='guestForm:loginButton']","TGTYPESCREENNO");

        CALL.$("PolicyPreInquirySurrender","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_TASKSEARCH","//a[@id='workbenchForm:workbenchTabs:taskSearchContainer:tb_buttonSearch']//span[@class='fa fa-search fa-lg']","TGTYPESCREENREG");

		int var_Count = 0;
                 LOGGER.info("Executed Step = VAR,Integer,var_Count,0,TGTYPESCREENREG");
		com.jayway.jsonpath.ReadContext var_CSVData = getJsonData("http://10.1.104.229/json/20200916/DAk9V3.json");
                 LOGGER.info("Executed Step = VAR,Json,var_CSVData,http://10.1.104.229/json/20200916/DAk9V3.json,TGTYPESCREENREG");
		String var_SessionDate;
                 LOGGER.info("Executed Step = VAR,String,var_SessionDate,TGTYPESCREENREG");
		var_SessionDate = getJsonData(var_CSVData , "$.records["+var_Count+"].SessionDate");
                 LOGGER.info("Executed Step = STORE,var_SessionDate,var_CSVData,$.records[{var_Count}].SessionDate,TGTYPESCREENREG");
		String var_PolicyNumber;
                 LOGGER.info("Executed Step = VAR,String,var_PolicyNumber,TGTYPESCREENREG");
		var_PolicyNumber = getJsonData(var_CSVData , "$.records.["+var_Count+"].PolicyNumber");
                 LOGGER.info("Executed Step = STORE,var_PolicyNumber,var_CSVData,$.records.[{var_Count}].PolicyNumber,TGTYPESCREENREG");
		String var_SurrenderDate;
                 LOGGER.info("Executed Step = VAR,String,var_SurrenderDate,TGTYPESCREENREG");
		var_SurrenderDate = getJsonData(var_CSVData , "$.records["+var_Count+"].SurrenderDate");
                 LOGGER.info("Executed Step = STORE,var_SurrenderDate,var_CSVData,$.records[{var_Count}].SurrenderDate,TGTYPESCREENREG");
        TAP.$("ELE_BUTTON_SESSIONDATE","//a[@id='headerForm:sessionDatelink']","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_NEWSESSIONDATE","//input[@id='sessionDateForm:sessionDateInput_input']",var_SessionDate,"FALSE","TGTYPESCREENREG");

        TAP.$("ELE_CHANGEDATE_BUTTON","//span[contains(text(),'Change Date')]","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_SEARCHTASKS","//input[@id='workbenchForm:workbenchTabs:taskSearchContainer:taskSearchSearch']","Policy Inquiry","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_TASKSEARCH","//a[@id='workbenchForm:workbenchTabs:taskSearchContainer:tb_buttonSearch']//span[@class='fa fa-search fa-lg']","TGTYPESCREENREG");

        TAP.$("ELE_LINK_POLICYINQUIRY","//span[contains(text(),'Policy Inquiry')]","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_POLICYNUMBER","//input[@id='workbenchForm:workbenchTabs:policyNumber']",var_PolicyNumber,"FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_CONTINUE","//span[contains(text(),'Continue')]","TGTYPESCREENREG");

		takeFullScreenShot();
                 LOGGER.info("Executed Step = TAKEFULLSCREENSHOT,TGTYPESCREENREG");
        WAIT.$(5,"TGTYPESCREENREG");

        WAIT.$("ELE_GETVAL_POLICYSTATUS","//span[@id='workbenchForm:workbenchTabs:statusText']","VISIBLE","TGTYPESCREENREG");

        ASSERT.$("ELE_CHECK_POLICYSTATUSPREMIUMPAYING","//span[text()='Premium Paying']","=","Premium Paying","TGTYPESCREENREG");

        TAP.$("ELE_LOGO_GIAS1","//span[contains(text(),'GIAS')]","TGTYPESCREENREG");

        WAIT.$(3,"TGTYPESCREENREG");

        CALL.$("FullSurrender","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_GIASHOME","//span[text()='GIAS']","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_TASKSEARCH","//a[@id='workbenchForm:workbenchTabs:taskSearchContainer:tb_buttonSearch']//span[@class='fa fa-search fa-lg']","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_SEARCHTASKS","//input[@id='workbenchForm:workbenchTabs:taskSearchContainer:taskSearchSearch']","Surrender","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_TASKSEARCH","//a[@id='workbenchForm:workbenchTabs:taskSearchContainer:tb_buttonSearch']//span[@class='fa fa-search fa-lg']","TGTYPESCREENREG");

        TAP.$("ELE_LINK_SURRENDERPOLICY","//span[contains(text(),'Surrender Policy')]","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_POLICYNUMBERSURR","//input[@type='text'][@name='workbenchForm:workbenchTabs:policyNumber']",var_PolicyNumber,"FALSE","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_DATE","//input[@id='workbenchForm:workbenchTabs:promptDate_input']",var_SurrenderDate,"FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_FULLSURRENDER","//span[contains(text(),'Full')]","TGTYPESCREENREG");

        TAP.$("ELE_SELECT_LINKBASEBENEFIT","//tr[@class='ui-widget-content ui-datatable-even ui-datatable-selectable']","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SHOWLINKS","//span[@class='ui-button-icon-left ui-icon ui-c fa fa-ellipsis-v fa-2x']","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        TAP.$("ELE_LINK_DISTRIBUTIONINFORMATION","//text()[.='Distribution Information']/ancestor::a[1]","TGTYPESCREENREG");

		takeFullScreenShot();
                 LOGGER.info("Executed Step = TAKEFULLSCREENSHOT,TGTYPESCREENREG");
        TAP.$("ELE_BUTTON_VALIDATE","//span[contains(text(),'Validate')]","TGTYPESCREENREG");

        WAIT.$(4,"TGTYPESCREENREG");

        ASSERT.$("ELE_MSG_VALIDATIONSUCCESS","//div[@id='workbenchForm:workbenchTabs:messages']//span[@class='ui-messages-info-summary'][contains(text(),'Validation completed successfully with no errors.')]","VISIBLE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SAVE","//span[contains(text(),'Save')]","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        TAP.$("ELE_DROPDWN_DISTRIBUTIONCODE","//div[@class='ui-selectonemenu-trigger ui-state-default ui-corner-right']","TGTYPESCREENREG");

        TAP.$("ELE_DROPDWNSELECT_NORMALDISTRIBUTION","//li[@id='workbenchForm:workbenchTabs:distributionReasonCode_full_7']","TGTYPESCREENREG");

		takeFullScreenShot();
                 LOGGER.info("Executed Step = TAKEFULLSCREENSHOT,TGTYPESCREENREG");
        TAP.$("ELE_BUTTON_SUBMIT","//span[contains(text(),'Submit')]","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

        ASSERT.$("ELE_GETVAL_SUCCESSMESSAGE","//div[@id='workbenchForm:workbenchTabs:messages']//span[@class='ui-messages-info-summary'][contains(text(),'Request completed successfully.')]","VISIBLE","TGTYPESCREENREG");

		takeFullScreenShot();
                 LOGGER.info("Executed Step = TAKEFULLSCREENSHOT,TGTYPESCREENREG");
        TAP.$("ELE_BUTTON_GIASHOME","//span[text()='GIAS']","TGTYPESCREENREG");

        CALL.$("PolicyPostInquirySurrender","TGTYPESCREENREG");

        TAP.$("ELE_TXTBOX_SEARCHTASKS","//input[@id='workbenchForm:workbenchTabs:taskSearchContainer:taskSearchSearch']","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_SEARCHTASKS","//input[@id='workbenchForm:workbenchTabs:taskSearchContainer:taskSearchSearch']","Policy Inquiry","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_TASKSEARCH","//a[@id='workbenchForm:workbenchTabs:taskSearchContainer:tb_buttonSearch']//span[@class='fa fa-search fa-lg']","TGTYPESCREENREG");

        TAP.$("ELE_LINK_POLICYINQUIRY","//span[contains(text(),'Policy Inquiry')]","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_POLICYNUMBER","//input[@id='workbenchForm:workbenchTabs:policyNumber']",var_PolicyNumber,"FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_CONTINUE","//span[contains(text(),'Continue')]","TGTYPESCREENREG");

        ASSERT.$("ELE_CHECKPOLICYSTATUS_CASHSURRENDER","//span[text()='Cash Surrender']","=","Cash Surrender","TGTYPESCREENREG");

		takeFullScreenShot();
                 LOGGER.info("Executed Step = TAKEFULLSCREENSHOT,TGTYPESCREENREG");
        TAP.$("ELE_BUTTON_SHOWLINKS","//span[@class='ui-button-icon-left ui-icon ui-c fa fa-ellipsis-v fa-2x']","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_CLIENTTRANSACTIONHISTORY","//span[contains(text(),'Transaction History')]","TGTYPESCREENREG");

		takeFullScreenShot();
                 LOGGER.info("Executed Step = TAKEFULLSCREENSHOT,TGTYPESCREENREG");
        TAP.$("ELE_LOGO_GIAS1","//span[contains(text(),'GIAS')]","TGTYPESCREENREG");

        WAIT.$(3,"TGTYPESCREENREG");

        SWIPE.$("DOWN","ELE_TXTBOX_SEARCHTASKS","//input[@id='workbenchForm:workbenchTabs:taskSearchContainer:taskSearchSearch']","TGTYPESCREENREG");

        WAIT.$("ELE_BUTTON_TASKSEARCH","//a[@id='workbenchForm:workbenchTabs:taskSearchContainer:tb_buttonSearch']//span[@class='fa fa-search fa-lg']","VISIBLE","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_SEARCHTASKS","//input[@id='workbenchForm:workbenchTabs:taskSearchContainer:taskSearchSearch']","Ledger Extract","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_TASKSEARCH","//a[@id='workbenchForm:workbenchTabs:taskSearchContainer:tb_buttonSearch']//span[@class='fa fa-search fa-lg']","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

        TAP.$("ELE_LINK_LEDGEREXTRACTENTRIES","//span[contains(text(),'Ledger Extract Entries')]","TGTYPESCREENREG");

		takeFullScreenShot();
                 LOGGER.info("Executed Step = TAKEFULLSCREENSHOT,TGTYPESCREENREG");
        WAIT.$(2,"TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_LASTPAGE","//span[text()='E']","TGTYPESCREENREG");

		takeFullScreenShot();
                 LOGGER.info("Executed Step = TAKEFULLSCREENSHOT,TGTYPESCREENREG");

    }


    @Test
    public void mm40reg04premiumapplicationsuspensemaintenance() {

        // No need to declare / print method name here, handled by TestNg listener
        // declare("mm40reg04premiumapplicationsuspensemaintenance");

        CALL.$("LoginToGIAS4","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENNO");

        TYPE.$("ELE_TXTBOX_USERNAME","//input[@id='guestForm:username']","Mano","FALSE","TGTYPESCREENNO");

        TYPE.$("ELE_TXTBOX_PASSWORD","//input[@id='guestForm:password']","Cnx12345!","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_LOGIN","//button[@id='guestForm:loginButton']","TGTYPESCREENNO");

        CALL.$("PolicyPremSuspensePayment1","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_TASKSEARCH","//a[@id='workbenchForm:workbenchTabs:taskSearchContainer:tb_buttonSearch']//span[@class='fa fa-search fa-lg']","TGTYPESCREENREG");

		int var_Count = 0;
                 LOGGER.info("Executed Step = VAR,Integer,var_Count,0,TGTYPESCREENREG");
		com.jayway.jsonpath.ReadContext var_CSVData = getJsonData("http://10.1.104.229/json/20200916/s04V6A.json");
                 LOGGER.info("Executed Step = VAR,Json,var_CSVData,http://10.1.104.229/json/20200916/s04V6A.json,TGTYPESCREENREG");
		String var_PolicyNumber;
                 LOGGER.info("Executed Step = VAR,String,var_PolicyNumber,TGTYPESCREENREG");
		var_PolicyNumber = getJsonData(var_CSVData , "$.records.["+var_Count+"].PolicyNumber");
                 LOGGER.info("Executed Step = STORE,var_PolicyNumber,var_CSVData,$.records.[{var_Count}].PolicyNumber,TGTYPESCREENREG");
		String var_AmountofChange;
                 LOGGER.info("Executed Step = VAR,String,var_AmountofChange,TGTYPESCREENREG");
		var_AmountofChange = getJsonData(var_CSVData , "$.records.["+var_Count+"].AmountofChange");
                 LOGGER.info("Executed Step = STORE,var_AmountofChange,var_CSVData,$.records.[{var_Count}].AmountofChange,TGTYPESCREENREG");
		String var_CommentsSuspence;
                 LOGGER.info("Executed Step = VAR,String,var_CommentsSuspence,TGTYPESCREENREG");
		var_CommentsSuspence = getJsonData(var_CSVData , "$.records.["+var_Count+"].CommentsSuspence");
                 LOGGER.info("Executed Step = STORE,var_CommentsSuspence,var_CSVData,$.records.[{var_Count}].CommentsSuspence,TGTYPESCREENREG");
        TYPE.$("ELE_TXTBOX_SEARCHTASKS","//input[@id='workbenchForm:workbenchTabs:taskSearchContainer:taskSearchSearch']","Policy Inquiry","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_TASKSEARCH","//a[@id='workbenchForm:workbenchTabs:taskSearchContainer:tb_buttonSearch']//span[@class='fa fa-search fa-lg']","TGTYPESCREENREG");

        TAP.$("ELE_LINK_POLICYINQUIRY","//span[contains(text(),'Policy Inquiry')]","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_POLICYNUMBER","//input[@id='workbenchForm:workbenchTabs:policyNumber']",var_PolicyNumber,"FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_CONTINUE","//span[contains(text(),'Continue')]","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        ASSERT.$("ELE_CHECK_POLICYSTATUSPREMIUMPAYING","//span[text()='Premium Paying']","=","Premium Paying","TGTYPESCREENREG");

		String var_PaidtoDate = "null";
                 LOGGER.info("Executed Step = VAR,String,var_PaidtoDate,null,TGTYPESCREENREG");
		var_PaidtoDate = ActionWrapper.getElementValue("ELE_GETVAL_PAIDTODATE", "//span[@id='workbenchForm:workbenchTabs:paidToDate']");
                 LOGGER.info("Executed Step = STORE,var_PaidtoDate,ELE_GETVAL_PAIDTODATE,TGTYPESCREENREG");
		takeFullScreenShot();
                 LOGGER.info("Executed Step = TAKEFULLSCREENSHOT,TGTYPESCREENREG");
        TAP.$("ELE_LINK_GIASHOME","//button[@id='headerForm:j_idt11']//span[@class='ui-button-text ui-c'][contains(text(),'GIAS')]","TGTYPESCREENREG");

        CALL.$("AddSuspensePremiumApplicationSuspenseMaintenance","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_SEARCHTASKS","//input[@id='workbenchForm:workbenchTabs:taskSearchContainer:taskSearchSearch']","Suspense","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_TASKSEARCH","//a[@id='workbenchForm:workbenchTabs:taskSearchContainer:tb_buttonSearch']//span[@class='fa fa-search fa-lg']","TGTYPESCREENREG");

        TAP.$("ELE_LINK_SUSPENSEMAINTENCE","//span[contains(text(),'Suspense Maintenance')]","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_ADD","//button[@id='workbenchForm:workbenchTabs:tb_buttonAdd']//span[@class='ui-button-text ui-c'][contains(text(),'Add')]","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_SUSPENCECONTROLNUMBER","//input[@id='workbenchForm:workbenchTabs:suspenseControlNumb']",var_PolicyNumber,"FALSE","TGTYPESCREENREG");

        TAP.$("ELE_DROPDWN_TYPEADDSUSPENSE","//div[@id='workbenchForm:workbenchTabs:suspenseType']//div[@class='ui-selectonemenu-trigger ui-state-default ui-corner-right']","TGTYPESCREENREG");

        TAP.$("ELE_DROPDWN_SELECTPREMIUMSUSPENSEADDSUSPENSE","//li[@id='workbenchForm:workbenchTabs:suspenseType_11']","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_SUSPENCEAMOUNTCHANGE","//input[@id='workbenchForm:workbenchTabs:amountOfChange_input']",var_AmountofChange,"FALSE","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_SUSPENSECOMMENT","//input[@id='workbenchForm:workbenchTabs:suspenseComment']",var_CommentsSuspence,"FALSE","TGTYPESCREENREG");

        TAP.$("ELE_RADIOBUTTON_GENERALLEDGER","//div[@class='ui-radiobutton-box ui-widget ui-corner-all ui-state-default ui-state-active']","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_VALIDATE","//span[contains(text(),'Validate')]","TGTYPESCREENREG");

        LOGGER.info("Executed Step = START IF");
        if (check("ELE_MSG_SUSPENSEBALANCEALREADYEXISTSFORTHISSUSPENSE","//span[text()='Suspense balance already exists for this suspense type.']","VISIBLE","TGTYPESCREENREG")) {
         LOGGER.info("Executed Step = IF,ELE_MSG_SUSPENSEBALANCEALREADYEXISTSFORTHISSUSPENSE,VISIBLE,TGTYPESCREENREG");
        TAP.$("ELE_BUTTON_SAVE","//span[contains(text(),'Save')]","TGTYPESCREENREG");

		try { 
		 		takeFullScreenShot();		 		 
		driver.switchTo().frame(0);
		         driver.findElement(By.xpath("//span[text()='Yes']")).click(); 
		takeFullScreenShot();
		 
		 ;} 
		 catch(Exception e) { PrintVal("Renderer Script Exception : "+e.toString()); } 
		 PrintVal("Executed Step = CALLRENDERERSCRIPT,SELECTYESBUTTON"); 
        }
         else {
          LOGGER.info("Executed Step = ELSE,TGTYPESCREENREG");
        ASSERT.$("ELE_MSG_VALIDATIONSUCCESS","//div[@id='workbenchForm:workbenchTabs:messages']//span[@class='ui-messages-info-summary'][contains(text(),'Validation completed successfully with no errors.')]","VISIBLE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SAVE","//span[contains(text(),'Save')]","TGTYPESCREENREG");

        ASSERT.$("ELE_LABEL_ITEMSUCCESSFULLYADDED","//div[@id='workbenchForm:workbenchTabs:messages']//span[@class='ui-messages-info-summary'][contains(text(),'Item successfully added.')]","VISIBLE","TGTYPESCREENREG");

        }
          LOGGER.info("Executed Step = ENDIF,TGTYPESCREENREG");
        TAP.$("ELE_BUTTON_GIASHOME","//span[text()='GIAS']","TGTYPESCREENREG");

        CALL.$("PolicyPremPay","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_SEARCHTASKS","//input[@id='workbenchForm:workbenchTabs:taskSearchContainer:taskSearchSearch']","Policy Payment","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_TASKSEARCH","//a[@id='workbenchForm:workbenchTabs:taskSearchContainer:tb_buttonSearch']//span[@class='fa fa-search fa-lg']","TGTYPESCREENREG");

        TAP.$("ELE_LINKNEW_POLICYPAYMENT","//button[@id='workbenchForm:workbenchTabs:taskSearchContainer:2:j_idt84']//span[@class='ui-button-text ui-c'][contains(text(),'Policy Payment')]","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_POLICYNUMBER","//input[@id='workbenchForm:workbenchTabs:policyNumber']",var_PolicyNumber,"FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_CONTINUE","//span[contains(text(),'Continue')]","TGTYPESCREENREG");

		String var_ModalPremium;
                 LOGGER.info("Executed Step = VAR,String,var_ModalPremium,TGTYPESCREENREG");
		var_ModalPremium = ActionWrapper.getElementValue("ELE_GETVAL_MODALPREMIUM", "//span[@id='workbenchForm:workbenchTabs:modalPremium']");
                 LOGGER.info("Executed Step = STORE,var_ModalPremium,ELE_GETVAL_MODALPREMIUM,TGTYPESCREENREG");
		String var_NetPayment;
                 LOGGER.info("Executed Step = VAR,String,var_NetPayment,TGTYPESCREENREG");
		var_NetPayment = ActionWrapper.getElementValue("ELE_LABEL_NETPAYMENT", "//span[@id='workbenchForm:workbenchTabs:netPremium']");
                 LOGGER.info("Executed Step = STORE,var_NetPayment,ELE_LABEL_NETPAYMENT,TGTYPESCREENREG");
        ASSERT.$(var_NetPayment,"=",var_ModalPremium,"TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_DATETOAPPLYPOLICYPAYMENT","//input[@id='workbenchForm:workbenchTabs:applyDate_input']",var_PaidtoDate,"FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SUBMIT","//span[contains(text(),'Submit')]","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

		try { 
		 		takeFullScreenShot();		 		 
		driver.switchTo().frame(0);
		         driver.findElement(By.xpath("//span[text()='Yes']")).click(); 
		takeFullScreenShot();
		 
		 ;} 
		 catch(Exception e) { PrintVal("Renderer Script Exception : "+e.toString()); } 
		 PrintVal("Executed Step = CALLRENDERERSCRIPT,SELECTYESBUTTON"); 
        WAIT.$(5,"TGTYPESCREENREG");

		takeFullScreenShot();
                 LOGGER.info("Executed Step = TAKEFULLSCREENSHOT,TGTYPESCREENREG");
        TAP.$("ELE_BUTTON_GIASHOME","//span[text()='GIAS']","TGTYPESCREENREG");

        CALL.$("PolicyPostInquiryPremPay","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_SEARCHTASKS","//input[@id='workbenchForm:workbenchTabs:taskSearchContainer:taskSearchSearch']","Policy Inquiry","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_TASKSEARCH","//a[@id='workbenchForm:workbenchTabs:taskSearchContainer:tb_buttonSearch']//span[@class='fa fa-search fa-lg']","TGTYPESCREENREG");

        TAP.$("ELE_LINK_POLICYINQUIRY","//span[contains(text(),'Policy Inquiry')]","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_POLICYNUMBER","//input[@id='workbenchForm:workbenchTabs:policyNumber']",var_PolicyNumber,"FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_CONTINUE","//span[contains(text(),'Continue')]","TGTYPESCREENREG");

		takeFullScreenShot();
                 LOGGER.info("Executed Step = TAKEFULLSCREENSHOT,TGTYPESCREENREG");
        TAP.$("ELE_BUTTON_SHOWLINKS","//span[@class='ui-button-icon-left ui-icon ui-c fa fa-ellipsis-v fa-2x']","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_CLIENTTRANSACTIONHISTORY","//span[contains(text(),'Transaction History')]","TGTYPESCREENREG");

		takeFullScreenShot();
                 LOGGER.info("Executed Step = TAKEFULLSCREENSHOT,TGTYPESCREENREG");
        WAIT.$(2,"TGTYPESCREENREG");

        TAP.$("ELE_TAB_MYDASHBOARD","//a[contains(text(),'My Dashboard')]","TGTYPESCREENREG");

        WAIT.$(4,"TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_TASKSEARCH","//a[@id='workbenchForm:workbenchTabs:taskSearchContainer:tb_buttonSearch']//span[@class='fa fa-search fa-lg']","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_SEARCHTASKS","//input[@id='workbenchForm:workbenchTabs:taskSearchContainer:taskSearchSearch']","Ledger Extract","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_TASKSEARCH","//a[@id='workbenchForm:workbenchTabs:taskSearchContainer:tb_buttonSearch']//span[@class='fa fa-search fa-lg']","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

        TAP.$("ELE_LINK_LEDGEREXTRACTENTRIES","//span[contains(text(),'Ledger Extract Entries')]","TGTYPESCREENREG");

		takeFullScreenShot();
                 LOGGER.info("Executed Step = TAKEFULLSCREENSHOT,TGTYPESCREENREG");
        WAIT.$(2,"TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_LASTPAGE","//span[text()='E']","TGTYPESCREENREG");

		takeFullScreenShot();
                 LOGGER.info("Executed Step = TAKEFULLSCREENSHOT,TGTYPESCREENREG");

    }


    @Test
    public void mm40reg10loanprocessingapplytheloan() {

        // No need to declare / print method name here, handled by TestNg listener
        // declare("mm40reg10loanprocessingapplytheloan");

        CALL.$("LoginToGIAS4","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENNO");

        TYPE.$("ELE_TXTBOX_USERNAME","//input[@id='guestForm:username']","Mano","FALSE","TGTYPESCREENNO");

        TYPE.$("ELE_TXTBOX_PASSWORD","//input[@id='guestForm:password']","Cnx12345!","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_LOGIN","//button[@id='guestForm:loginButton']","TGTYPESCREENNO");

        CALL.$("PreInquiryForLoanProcessing","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_TASKSEARCH","//a[@id='workbenchForm:workbenchTabs:taskSearchContainer:tb_buttonSearch']//span[@class='fa fa-search fa-lg']","TGTYPESCREENREG");

		int var_Count = 0;
                 LOGGER.info("Executed Step = VAR,Integer,var_Count,0,TGTYPESCREENREG");
		com.jayway.jsonpath.ReadContext var_CSVData = getJsonData("http://10.1.104.229/json/20200916/Ikyxsr.json");
                 LOGGER.info("Executed Step = VAR,Json,var_CSVData,http://10.1.104.229/json/20200916/Ikyxsr.json,TGTYPESCREENREG");
		String var_SessionDate;
                 LOGGER.info("Executed Step = VAR,String,var_SessionDate,TGTYPESCREENREG");
		var_SessionDate = getJsonData(var_CSVData , "$.records.["+var_Count+"].SessionDate");
                 LOGGER.info("Executed Step = STORE,var_SessionDate,var_CSVData,$.records.[{var_Count}].SessionDate,TGTYPESCREENREG");
		String var_PolicyNumber;
                 LOGGER.info("Executed Step = VAR,String,var_PolicyNumber,TGTYPESCREENREG");
		var_PolicyNumber = getJsonData(var_CSVData , "$.records.["+var_Count+"].PolicyNumber");
                 LOGGER.info("Executed Step = STORE,var_PolicyNumber,var_CSVData,$.records.[{var_Count}].PolicyNumber,TGTYPESCREENREG");
		String var_RequestedLoanAmount;
                 LOGGER.info("Executed Step = VAR,String,var_RequestedLoanAmount,TGTYPESCREENREG");
		var_RequestedLoanAmount = getJsonData(var_CSVData , "$.records.["+var_Count+"].RequestedLoanAmount");
                 LOGGER.info("Executed Step = STORE,var_RequestedLoanAmount,var_CSVData,$.records.[{var_Count}].RequestedLoanAmount,TGTYPESCREENREG");
        TAP.$("ELE_BUTTON_SESSIONDATE","//a[@id='headerForm:sessionDatelink']","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_NEWSESSIONDATE","//input[@id='sessionDateForm:sessionDateInput_input']",var_SessionDate,"FALSE","TGTYPESCREENREG");

        TAP.$("ELE_CHANGEDATE_BUTTON","//span[contains(text(),'Change Date')]","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_TASKSEARCH","//a[@id='workbenchForm:workbenchTabs:taskSearchContainer:tb_buttonSearch']//span[@class='fa fa-search fa-lg']","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_SEARCHTASKS","//input[@id='workbenchForm:workbenchTabs:taskSearchContainer:taskSearchSearch']","Policy Inquiry","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_TASKSEARCH","//a[@id='workbenchForm:workbenchTabs:taskSearchContainer:tb_buttonSearch']//span[@class='fa fa-search fa-lg']","TGTYPESCREENREG");

        TAP.$("ELE_LINK_POLICYINQUIRY","//span[contains(text(),'Policy Inquiry')]","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_POLICYNUMBER","//input[@id='workbenchForm:workbenchTabs:policyNumber']",var_PolicyNumber,"FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_CONTINUE","//span[contains(text(),'Continue')]","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        WAIT.$("ELE_GETVAL_POLICYSTATUS","//span[@id='workbenchForm:workbenchTabs:statusText']","VISIBLE","TGTYPESCREENREG");

        ASSERT.$("ELE_GETVAL_POLICYSTATUS","//span[@id='workbenchForm:workbenchTabs:statusText']","=","Premium Paying","TGTYPESCREENREG");

		takeFullScreenShot();
                 LOGGER.info("Executed Step = TAKEFULLSCREENSHOT,TGTYPESCREENREG");
        TAP.$("ELE_BUTTON_GIASHOME","//span[text()='GIAS']","TGTYPESCREENREG");

        CALL.$("PolicyLoanRequest","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_TASKSEARCH","//a[@id='workbenchForm:workbenchTabs:taskSearchContainer:tb_buttonSearch']//span[@class='fa fa-search fa-lg']","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_SEARCHTASKS","//input[@id='workbenchForm:workbenchTabs:taskSearchContainer:taskSearchSearch']","Policy Loan Request","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_TASKSEARCH","//a[@id='workbenchForm:workbenchTabs:taskSearchContainer:tb_buttonSearch']//span[@class='fa fa-search fa-lg']","TGTYPESCREENREG");

        TAP.$("ELE_LINK_POLICYLOANREQUEST","//span[text()='Policy Loan Request']","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_POLICYNUMBER","//input[@id='workbenchForm:workbenchTabs:policyNumber']",var_PolicyNumber,"FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_CONTINUE","//span[contains(text(),'Continue')]","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        WAIT.$("ELE_BUTTON_SHOWLINKS","//span[@class='ui-button-icon-left ui-icon ui-c fa fa-ellipsis-v fa-2x']","VISIBLE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SHOWLINKS","//span[@class='ui-button-icon-left ui-icon ui-c fa fa-ellipsis-v fa-2x']","TGTYPESCREENREG");

        TAP.$("ELE_LINK_LOANVALUES","//span[contains(text(),'Loan Values')]","TGTYPESCREENREG");

        WAIT.$(3,"TGTYPESCREENREG");

        TAP.$("ELE_TAB_LOANREQUEST","//a[contains(text(),'Loan')]","TGTYPESCREENREG");

        WAIT.$("ELE_BUTTON_QUOTEBT","//span[contains(text(),'Quote')]","VISIBLE","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_REQUESTEDLOANAMOUNT","//input[@id='workbenchForm:workbenchTabs:loanRequest_input']",var_RequestedLoanAmount,"FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_QUOTEBT","//span[contains(text(),'Quote')]","TGTYPESCREENREG");

        WAIT.$("ELE_BUTTON_SHOWLINKS","//span[@class='ui-button-icon-left ui-icon ui-c fa fa-ellipsis-v fa-2x']","VISIBLE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SHOWLINKS","//span[@class='ui-button-icon-left ui-icon ui-c fa fa-ellipsis-v fa-2x']","TGTYPESCREENREG");

        WAIT.$(4,"TGTYPESCREENREG");

        ASSERT.$("ELE_MSG_DISTRIBUTIONINFO","//div[@id='workbenchForm:workbenchTabs:messages']//span[@class='ui-messages-warn-summary'][contains(text(),'Distribution information must be confirmed.')]","VISIBLE","TGTYPESCREENREG");

        WAIT.$("ELE_LINK_DISTRIBUTTIONINFORMATION","//span[contains(text(),'Distribution Information')]","VISIBLE","TGTYPESCREENREG");

        TAP.$("ELE_LINK_DISTRIBUTTIONINFORMATION","//span[contains(text(),'Distribution Information')]","TGTYPESCREENREG");

        WAIT.$("ELE_BUTTON_SAVE","//span[contains(text(),'Save')]","VISIBLE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SAVE","//span[contains(text(),'Save')]","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

        WAIT.$("ELE_BUTTON_SUBMIT","//span[contains(text(),'Submit')]","VISIBLE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SUBMIT","//span[contains(text(),'Submit')]","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        ASSERT.$("ELE_GETVAL_SUCCESSMESSAGE","//div[@id='workbenchForm:workbenchTabs:messages']//span[@class='ui-messages-info-summary'][contains(text(),'Request completed successfully.')]","VISIBLE","TGTYPESCREENREG");

        WAIT.$(3,"TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_GIASHOME","//span[text()='GIAS']","TGTYPESCREENREG");

        CALL.$("PostLoanPolicyInquiry","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_SEARCHTASKS","//input[@id='workbenchForm:workbenchTabs:taskSearchContainer:taskSearchSearch']","Policy Inquiry","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_TASKSEARCH","//a[@id='workbenchForm:workbenchTabs:taskSearchContainer:tb_buttonSearch']//span[@class='fa fa-search fa-lg']","TGTYPESCREENREG");

        TAP.$("ELE_LINK_POLICYINQUIRY","//span[contains(text(),'Policy Inquiry')]","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_POLICYNUMBER","//input[@id='workbenchForm:workbenchTabs:policyNumber']",var_PolicyNumber,"FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_CONTINUE","//span[contains(text(),'Continue')]","TGTYPESCREENREG");

		takeFullScreenShot();
                 LOGGER.info("Executed Step = TAKEFULLSCREENSHOT,TGTYPESCREENREG");
        TAP.$("ELE_BUTTON_SHOWLINKS","//span[@class='ui-button-icon-left ui-icon ui-c fa fa-ellipsis-v fa-2x']","TGTYPESCREENREG");

        TAP.$("ELE_LINK_LOANS","//span[contains(text(),'Loans')]","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        TAP.$("ELE_SELECT_LINKBASEBENEFIT","//tr[@class='ui-widget-content ui-datatable-even ui-datatable-selectable']","TGTYPESCREENREG");

        WAIT.$(3,"TGTYPESCREENREG");

        ASSERT.$("ELE_POLICYLOANS_PRINCIPALAMOUNT","//span[@id='workbenchForm:workbenchTabs:loanBAmount']","VISIBLE","TGTYPESCREENREG");

        ASSERT.$("ELE_POLICYLOAN_ANUALINTERESTRATE","//span[text()='8.00%']","=","8.00%","TGTYPESCREENREG");

        ASSERT.$("ELE_POLICYLOAN_LOANSTATUSACTIVE","//span[@id='workbenchForm:workbenchTabs:loanStatusText']","=","Active","TGTYPESCREENREG");

		takeFullScreenShot();
                 LOGGER.info("Executed Step = TAKEFULLSCREENSHOT,TGTYPESCREENREG");
        TAP.$("ELE_TAB_ENTERPOLICYNUMBER","//a[contains(text(),'Enter Policy Number')]","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_CONTINUE","//span[contains(text(),'Continue')]","TGTYPESCREENREG");

        WAIT.$(4,"TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SHOWLINKS","//span[@class='ui-button-icon-left ui-icon ui-c fa fa-ellipsis-v fa-2x']","TGTYPESCREENREG");

        TAP.$("ELE_LINK_POLICY_VALUE","//span[contains(text(),'Policy Values')]","TGTYPESCREENREG");

        TAP.$("ELE_LOGO_GIAS1","//span[contains(text(),'GIAS')]","TGTYPESCREENREG");

        WAIT.$(3,"TGTYPESCREENREG");

        SWIPE.$("DOWN","ELE_BUTTON_TASKSEARCH","//a[@id='workbenchForm:workbenchTabs:taskSearchContainer:tb_buttonSearch']//span[@class='fa fa-search fa-lg']","TGTYPESCREENREG");

        WAIT.$("ELE_BUTTON_TASKSEARCH","//a[@id='workbenchForm:workbenchTabs:taskSearchContainer:tb_buttonSearch']//span[@class='fa fa-search fa-lg']","VISIBLE","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_SEARCHTASKS","//input[@id='workbenchForm:workbenchTabs:taskSearchContainer:taskSearchSearch']","Ledger Extract","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_TASKSEARCH","//a[@id='workbenchForm:workbenchTabs:taskSearchContainer:tb_buttonSearch']//span[@class='fa fa-search fa-lg']","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

        TAP.$("ELE_LINK_LEDGEREXTRACTENTRIES","//span[contains(text(),'Ledger Extract Entries')]","TGTYPESCREENREG");

		takeFullScreenShot();
                 LOGGER.info("Executed Step = TAKEFULLSCREENSHOT,TGTYPESCREENREG");
        TYPE.$("ELE_TXTBOX_REFERENCE1LEDEXTRACT","//input[@id='workbenchForm:workbenchTabs:grid:referenceNumber1_Col:filter']",var_PolicyNumber,"FALSE","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

		takeFullScreenShot();
                 LOGGER.info("Executed Step = TAKEFULLSCREENSHOT,TGTYPESCREENREG");

    }


    @Test
    public void mm40reg31nfoprocessing() {

        // No need to declare / print method name here, handled by TestNg listener
        // declare("mm40reg31nfoprocessing");

        CALL.$("LoginToGIAS4","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENNO");

        TYPE.$("ELE_TXTBOX_USERNAME","//input[@id='guestForm:username']","Mano","FALSE","TGTYPESCREENNO");

        TYPE.$("ELE_TXTBOX_PASSWORD","//input[@id='guestForm:password']","Cnx12345!","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_LOGIN","//button[@id='guestForm:loginButton']","TGTYPESCREENNO");

        CALL.$("PolicyPreInquiryNFO","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_TASKSEARCH","//a[@id='workbenchForm:workbenchTabs:taskSearchContainer:tb_buttonSearch']//span[@class='fa fa-search fa-lg']","TGTYPESCREENREG");

		int var_Count = 0;
                 LOGGER.info("Executed Step = VAR,Integer,var_Count,0,TGTYPESCREENREG");
		com.jayway.jsonpath.ReadContext var_CSVData = getJsonData("http://10.1.104.229/json/20200916/Dp3VI6.json");
                 LOGGER.info("Executed Step = VAR,Json,var_CSVData,http://10.1.104.229/json/20200916/Dp3VI6.json,TGTYPESCREENREG");
		String var_PolicyNumber;
                 LOGGER.info("Executed Step = VAR,String,var_PolicyNumber,TGTYPESCREENREG");
		var_PolicyNumber = getJsonData(var_CSVData , "$.records["+var_Count+"].PolicyNumber");
                 LOGGER.info("Executed Step = STORE,var_PolicyNumber,var_CSVData,$.records[{var_Count}].PolicyNumber,TGTYPESCREENREG");
		String var_SessionDate;
                 LOGGER.info("Executed Step = VAR,String,var_SessionDate,TGTYPESCREENREG");
		var_SessionDate = getJsonData(var_CSVData , "$.records.["+var_Count+"].SessionDate");
                 LOGGER.info("Executed Step = STORE,var_SessionDate,var_CSVData,$.records.[{var_Count}].SessionDate,TGTYPESCREENREG");
        TAP.$("ELE_BUTTON_SESSIONDATE","//a[@id='headerForm:sessionDatelink']","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_NEWSESSIONDATE","//input[@id='sessionDateForm:sessionDateInput_input']",var_SessionDate,"FALSE","TGTYPESCREENREG");

        TAP.$("ELE_CHANGEDATE_BUTTON","//span[contains(text(),'Change Date')]","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_SEARCHTASKS","//input[@id='workbenchForm:workbenchTabs:taskSearchContainer:taskSearchSearch']","Policy Inquiry","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_TASKSEARCH","//a[@id='workbenchForm:workbenchTabs:taskSearchContainer:tb_buttonSearch']//span[@class='fa fa-search fa-lg']","TGTYPESCREENREG");

        TAP.$("ELE_LINK_POLICYINQUIRY","//span[contains(text(),'Policy Inquiry')]","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_POLICYNUMBER","//input[@id='workbenchForm:workbenchTabs:policyNumber']",var_PolicyNumber,"FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_CONTINUE","//span[contains(text(),'Continue')]","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        WAIT.$("ELE_GETVAL_POLICYSTATUS","//span[@id='workbenchForm:workbenchTabs:statusText']","VISIBLE","TGTYPESCREENREG");

        ASSERT.$("ELE_GETVAL_POLICYSTATUS","//span[@id='workbenchForm:workbenchTabs:statusText']","=","Premium Paying","TGTYPESCREENREG");

		takeFullScreenShot();
                 LOGGER.info("Executed Step = TAKEFULLSCREENSHOT,TGTYPESCREENREG");
        TAP.$("ELE_BUTTON_SHOWLINKS","//span[@class='ui-button-icon-left ui-icon ui-c fa fa-ellipsis-v fa-2x']","TGTYPESCREENREG");

        WAIT.$(3,"TGTYPESCREENREG");

        TAP.$("ELE_LINK_BASEBENEFIT","//span[contains(text(),'Base Benefit')]","TGTYPESCREENREG");

        ASSERT.$("ELE_LABEL_FACEAMOUNT","//span[@id='workbenchForm:workbenchTabs:faceAmount']","<>",0,"TGTYPESCREENREG");

        ASSERT.$("ELE_LABEL_UNITOFINSURED","//span[@id='workbenchForm:workbenchTabs:unitsOfInsurance']","<>",0,"TGTYPESCREENREG");

		takeFullScreenShot();
                 LOGGER.info("Executed Step = TAKEFULLSCREENSHOT,TGTYPESCREENREG");
        TAP.$("ELE_BUTTON_GIASHOME","//span[text()='GIAS']","TGTYPESCREENREG");

        CALL.$("NFOExtendedTermProcessing","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_TASKSEARCH","//a[@id='workbenchForm:workbenchTabs:taskSearchContainer:tb_buttonSearch']//span[@class='fa fa-search fa-lg']","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_SEARCHTASKS","//input[@id='workbenchForm:workbenchTabs:taskSearchContainer:taskSearchSearch']","ETI","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_TASKSEARCH","//a[@id='workbenchForm:workbenchTabs:taskSearchContainer:tb_buttonSearch']//span[@class='fa fa-search fa-lg']","TGTYPESCREENREG");

        TAP.$("ELE_LINK_ETIORRPU","//span[contains(text(),'Request ETI or RPU')]","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_POLICYNUMBER","//input[@id='workbenchForm:workbenchTabs:policyNumber']",var_PolicyNumber,"FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_CONTINUE","//span[contains(text(),'Continue')]","TGTYPESCREENREG");

        ASSERT.$("ELE_GETTEXT_EFFECTIVEDATE","//span[@id='workbenchForm:workbenchTabs:effectiveDate']","VISIBLE","TGTYPESCREENREG");

        ASSERT.$("ELE_GETVAL_PAIDTODATE","//span[@id='workbenchForm:workbenchTabs:paidToDate']","VISIBLE","TGTYPESCREENREG");

        TAP.$("ELE_RADIO_BUTTON_REDUCED_PAID_UP","//label[contains(text(),'Reduced Paid Up')]","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_QUOTE","//span[@class='ui-button-text ui-c'][contains(text(),'Quote')]","TGTYPESCREENREG");

        WAIT.$(3,"TGTYPESCREENREG");

        ASSERT.$("ELE_GETVAL_RPU_REDUCEFACEPAID","//span[@id='workbenchForm:workbenchTabs:rpuFaceAmount']","VISIBLE","TGTYPESCREENREG");

        ASSERT.$("ELE_GETVAL_RPU_NETPOLICYVALUE","//span[@id='workbenchForm:workbenchTabs:rpuCashValue']","VISIBLE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SUBMIT","//span[contains(text(),'Submit')]","TGTYPESCREENREG");

        ASSERT.$("ELE_GETVAL_SUCCESSMESSAGE","//div[@id='workbenchForm:workbenchTabs:messages']//span[@class='ui-messages-info-summary'][contains(text(),'Request completed successfully.')]","VISIBLE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_GIASHOME","//span[text()='GIAS']","TGTYPESCREENREG");

        CALL.$("PostPolicyInqNFOProcessing","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_TASKSEARCH","//a[@id='workbenchForm:workbenchTabs:taskSearchContainer:tb_buttonSearch']//span[@class='fa fa-search fa-lg']","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_SEARCHTASKS","//input[@id='workbenchForm:workbenchTabs:taskSearchContainer:taskSearchSearch']","Policy Inquiry","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_TASKSEARCH","//a[@id='workbenchForm:workbenchTabs:taskSearchContainer:tb_buttonSearch']//span[@class='fa fa-search fa-lg']","TGTYPESCREENREG");

        TAP.$("ELE_LINK_POLICYINQUIRY","//span[contains(text(),'Policy Inquiry')]","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_POLICYNUMBER","//input[@id='workbenchForm:workbenchTabs:policyNumber']",var_PolicyNumber,"FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_CONTINUE","//span[contains(text(),'Continue')]","TGTYPESCREENREG");

        ASSERT.$("ELE_LABEL_POLICYSTATUSREDUCEDPAIDUP","//span[text()='Reduced Paid Up']","VISIBLE","Reduced Paid Up","TGTYPESCREENREG");

		takeFullScreenShot();
                 LOGGER.info("Executed Step = TAKEFULLSCREENSHOT,TGTYPESCREENREG");
        TAP.$("ELE_BUTTON_SHOWLINKS","//span[@class='ui-button-icon-left ui-icon ui-c fa fa-ellipsis-v fa-2x']","TGTYPESCREENREG");

        TAP.$("ELE_LINK_TRANSACTIONHISTORY","//span[text()='Transaction History']","TGTYPESCREENREG");

		takeFullScreenShot();
                 LOGGER.info("Executed Step = TAKEFULLSCREENSHOT,TGTYPESCREENREG");
        SWIPE.$("DOWN","TGTYPESCREENREG");

        SWIPE.$("DOWN","TGTYPESCREENREG");

        TAP.$("ELE_LINK_TRANSACTIONETIRPU","//span[contains(text(),'ETI/RPU')]","TGTYPESCREENREG");

        WAIT.$(3,"TGTYPESCREENREG");

        ASSERT.$("ELE_LABEL_BENEFITTRANSACTIONDESCRIPTION","//span[@id='workbenchForm:workbenchTabs:eventType']","VISIBLE","TGTYPESCREENREG");

        ASSERT.$("ELE_LABEL_NFOOPTIONPROCESSED","//span[@id='workbenchForm:workbenchTabs:nfoOptionProcessed']","VISIBLE","TGTYPESCREENREG");

        TAP.$("ELE_TAB_POLICYINQUIRY","//a[contains(text(),'Policy')]","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_CONTINUE","//span[contains(text(),'Continue')]","TGTYPESCREENREG");

        WAIT.$(3,"TGTYPESCREENREG");

        WAIT.$("ELE_BUTTON_SHOWLINKS","//span[@class='ui-button-icon-left ui-icon ui-c fa fa-ellipsis-v fa-2x']","VISIBLE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SHOWLINKS","//span[@class='ui-button-icon-left ui-icon ui-c fa fa-ellipsis-v fa-2x']","TGTYPESCREENREG");

        WAIT.$(3,"TGTYPESCREENREG");

        TAP.$("ELE_LINK_BASEBENEFIT","//span[contains(text(),'Base Benefit')]","TGTYPESCREENREG");

        WAIT.$(3,"TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SHOWLINKS","//span[@class='ui-button-icon-left ui-icon ui-c fa fa-ellipsis-v fa-2x']","TGTYPESCREENREG");

        WAIT.$(3,"TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_AGENT","//span[contains(text(),'Agent')]","TGTYPESCREENREG");

        WAIT.$(3,"TGTYPESCREENREG");

        TAP.$("ELE_LINK_AGENT_AGENTTYPE","//tr[@class='ui-widget-content ui-datatable-even ui-datatable-selectable']","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        ASSERT.$("ELE_GETVAL_BENEFIT","//span[@id='workbenchForm:workbenchTabs:baseRiderCodeText']","=","All","TGTYPESCREENREG");

        ASSERT.$("ELE_LABEL_DISTRIBUTIONLINE","//span[@id='workbenchForm:workbenchTabs:distributionLine']","=","All","TGTYPESCREENREG");

        ASSERT.$("ELE_GETTEXT_EFFECTIVEDATE","//span[@id='workbenchForm:workbenchTabs:effectiveDate']","VISIBLE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_GIASHOME","//span[text()='GIAS']","TGTYPESCREENREG");


    }


    @Test
    public void mm40regfreelookprocessing() {

        // No need to declare / print method name here, handled by TestNg listener
        // declare("mm40regfreelookprocessing");

        CALL.$("LoginToGIAS4","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENNO");

        TYPE.$("ELE_TXTBOX_USERNAME","//input[@id='guestForm:username']","Mano","FALSE","TGTYPESCREENNO");

        TYPE.$("ELE_TXTBOX_PASSWORD","//input[@id='guestForm:password']","Cnx12345!","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_LOGIN","//button[@id='guestForm:loginButton']","TGTYPESCREENNO");

        CALL.$("PolicyPreInquiry","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_TASKSEARCH","//a[@id='workbenchForm:workbenchTabs:taskSearchContainer:tb_buttonSearch']//span[@class='fa fa-search fa-lg']","TGTYPESCREENREG");

		int var_Count = 0;
                 LOGGER.info("Executed Step = VAR,Integer,var_Count,0,TGTYPESCREENREG");
		com.jayway.jsonpath.ReadContext var_CSVData = getJsonData("http://10.1.104.229/json/20200916/FFKiBZ.json");
                 LOGGER.info("Executed Step = VAR,Json,var_CSVData,http://10.1.104.229/json/20200916/FFKiBZ.json,TGTYPESCREENREG");
		String var_PolicyNumber;
                 LOGGER.info("Executed Step = VAR,String,var_PolicyNumber,TGTYPESCREENREG");
		var_PolicyNumber = getJsonData(var_CSVData , "$.records["+var_Count+"].PolicyNumber");
                 LOGGER.info("Executed Step = STORE,var_PolicyNumber,var_CSVData,$.records[{var_Count}].PolicyNumber,TGTYPESCREENREG");
		String var_SessionDate;
                 LOGGER.info("Executed Step = VAR,String,var_SessionDate,TGTYPESCREENREG");
		var_SessionDate = getJsonData(var_CSVData , "$.records.["+var_Count+"].SessionDate");
                 LOGGER.info("Executed Step = STORE,var_SessionDate,var_CSVData,$.records.[{var_Count}].SessionDate,TGTYPESCREENREG");
        TAP.$("ELE_BUTTON_SESSIONDATE","//a[@id='headerForm:sessionDatelink']","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_NEWSESSIONDATE","//input[@id='sessionDateForm:sessionDateInput_input']",var_SessionDate,"FALSE","TGTYPESCREENREG");

        TAP.$("ELE_CHANGEDATE_BUTTON","//span[contains(text(),'Change Date')]","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_SEARCHTASKS","//input[@id='workbenchForm:workbenchTabs:taskSearchContainer:taskSearchSearch']","Policy Inquiry","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_TASKSEARCH","//a[@id='workbenchForm:workbenchTabs:taskSearchContainer:tb_buttonSearch']//span[@class='fa fa-search fa-lg']","TGTYPESCREENREG");

        TAP.$("ELE_LINK_POLICYINQUIRY","//span[contains(text(),'Policy Inquiry')]","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_POLICYNUMBER","//input[@id='workbenchForm:workbenchTabs:policyNumber']",var_PolicyNumber,"FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_CONTINUE","//span[contains(text(),'Continue')]","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        WAIT.$("ELE_GETVAL_POLICYSTATUS","//span[@id='workbenchForm:workbenchTabs:statusText']","VISIBLE","TGTYPESCREENREG");

        ASSERT.$("ELE_GETVAL_POLICYSTATUS","//span[@id='workbenchForm:workbenchTabs:statusText']","VISIBLE","TGTYPESCREENREG");

		String var_EffectiveDate;
                 LOGGER.info("Executed Step = VAR,String,var_EffectiveDate,TGTYPESCREENREG");
		var_EffectiveDate = ActionWrapper.getElementValue("ELE_LABEL_EFFECTIVE_DATE", "//span[@id='workbenchForm:workbenchTabs:benefitTable:0:effectiveDate']");
                 LOGGER.info("Executed Step = STORE,var_EffectiveDate,ELE_LABEL_EFFECTIVE_DATE,TGTYPESCREENREG");
		takeFullScreenShot();
                 LOGGER.info("Executed Step = TAKEFULLSCREENSHOT,TGTYPESCREENREG");
        TAP.$("ELE_LINK_GIASHOME","//button[@id='headerForm:j_idt11']//span[@class='ui-button-text ui-c'][contains(text(),'GIAS')]","TGTYPESCREENREG");

        CALL.$("FreeLookProcessing","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_TASKSEARCH","//a[@id='workbenchForm:workbenchTabs:taskSearchContainer:tb_buttonSearch']//span[@class='fa fa-search fa-lg']","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_SEARCHTASKS","//input[@id='workbenchForm:workbenchTabs:taskSearchContainer:taskSearchSearch']","policy benefit","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_TASKSEARCH","//a[@id='workbenchForm:workbenchTabs:taskSearchContainer:tb_buttonSearch']//span[@class='fa fa-search fa-lg']","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

        TAP.$("ELE_LINK_CHANGEPOLICYBENEFITSTATUS","//span[contains(text(),\"Change a Policy Benefit's Status\")]","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_POLICYNUMBER","//input[@id='workbenchForm:workbenchTabs:policyNumber']",var_PolicyNumber,"FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_CONTINUE","//span[contains(text(),'Continue')]","TGTYPESCREENREG");

        TAP.$("ELE_SELECT_LINKBASEBENEFIT","//tr[@class='ui-widget-content ui-datatable-even ui-datatable-selectable']","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

        TAP.$("ELE_DROPDWN_NEWBENEFITSTATUS","//label[@id='workbenchForm:workbenchTabs:newBenefitStatus_label']","TGTYPESCREENREG");

        TAP.$("ELE_LINK_BENEFITSTATUSNOTTAKEN","//li[@id='workbenchForm:workbenchTabs:newBenefitStatus_5']","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SHOWLINKS","//span[@class='ui-button-icon-left ui-icon ui-c fa fa-ellipsis-v fa-2x']","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

        TAP.$("ELE_LINK_DISTRIBUTTIONINFORMATION","//span[contains(text(),'Distribution Information')]","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SAVE","//span[contains(text(),'Save')]","TGTYPESCREENREG");

        ASSERT.$("ELE_MSG_DISTRIBUTIONINFORMATIONHASBEENUPDATED","//div[@id='workbenchForm:workbenchTabs:messages']//span[@class='ui-messages-info-summary'][contains(text(),'Distribution information has been updated.')]","VISIBLE","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_TERMINATIONDATE","//input[@id='workbenchForm:workbenchTabs:terminationDate_input']",var_EffectiveDate,"FALSE","TGTYPESCREENREG");

        TAP.$("ELE_RDOBTN_YES","//span[@class='ui-radiobutton-icon ui-icon ui-icon-blank ui-c']","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_VALIDATE","//span[contains(text(),'Validate')]","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        WAIT.$("ELE_BUTTON_SUBMIT","//span[contains(text(),'Submit')]","VISIBLE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SUBMIT","//span[contains(text(),'Submit')]","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

        LOGGER.info("Executed Step = START IF");
        if (check("ELE_CONFIRMACTION_YES","//div[@class='ui-dialog-content ui-widget-content ui-df-content']//iframe","VISIBLE","TGTYPESCREENREG")) {
         LOGGER.info("Executed Step = IF,ELE_CONFIRMACTION_YES,VISIBLE,TGTYPESCREENREG");
		try { 
		 driver.switchTo().frame(0);
		driver.findElement(By.xpath("//form[@id='confirmDialogForm']")).click();
		Thread.sleep(1000);
		driver.findElement(By.xpath("//button[@id='j_idt9']")).click();
		// Add Screenshot will only work for Android platform

		takeScreenshot();

		 
		 ;} 
		 catch(Exception e) { PrintVal("Renderer Script Exception : "+e.toString()); } 
		 PrintVal("Executed Step = CALLRENDERERSCRIPT,CLICKYESONCONFIRMACTION"); 
        }
          LOGGER.info("Executed Step = ENDIF,TGTYPESCREENREG");
        WAIT.$("ELE_GETVAL_SUCCESSMESSAGE","//div[@id='workbenchForm:workbenchTabs:messages']//span[@class='ui-messages-info-summary'][contains(text(),'Request completed successfully.')]","VISIBLE","TGTYPESCREENREG");

        ASSERT.$("ELE_GETVAL_SUCCESSMESSAGE","//div[@id='workbenchForm:workbenchTabs:messages']//span[@class='ui-messages-info-summary'][contains(text(),'Request completed successfully.')]","VISIBLE","TGTYPESCREENREG");

        WAIT.$(3,"TGTYPESCREENREG");

        TAP.$("ELE_LINK_GIASHOME","//button[@id='headerForm:j_idt11']//span[@class='ui-button-text ui-c'][contains(text(),'GIAS')]","TGTYPESCREENREG");

        CALL.$("PolicyPostInquiry","TGTYPESCREENREG");

        TAP.$("ELE_TXTBOX_SEARCHTASKS","//input[@id='workbenchForm:workbenchTabs:taskSearchContainer:taskSearchSearch']","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_SEARCHTASKS","//input[@id='workbenchForm:workbenchTabs:taskSearchContainer:taskSearchSearch']","Policy Inquiry","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_TASKSEARCH","//a[@id='workbenchForm:workbenchTabs:taskSearchContainer:tb_buttonSearch']//span[@class='fa fa-search fa-lg']","TGTYPESCREENREG");

        TAP.$("ELE_LINK_POLICYINQUIRY","//span[contains(text(),'Policy Inquiry')]","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_POLICYNUMBER","//input[@id='workbenchForm:workbenchTabs:policyNumber']",var_PolicyNumber,"FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_CONTINUE","//span[contains(text(),'Continue')]","TGTYPESCREENREG");

        WAIT.$(3,"TGTYPESCREENREG");

		takeFullScreenShot();
                 LOGGER.info("Executed Step = TAKEFULLSCREENSHOT,TGTYPESCREENREG");
        TAP.$("ELE_BUTTON_SHOWLINKS","//span[@class='ui-button-icon-left ui-icon ui-c fa fa-ellipsis-v fa-2x']","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_CLIENTTRANSACTIONHISTORY","//span[contains(text(),'Transaction History')]","TGTYPESCREENREG");

		takeFullScreenShot();
                 LOGGER.info("Executed Step = TAKEFULLSCREENSHOT,TGTYPESCREENREG");
        TAP.$("ELE_LOGO_GIAS1","//span[contains(text(),'GIAS')]","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");


    }


    @Test
    public void mm40reg23cancellation() {

        // No need to declare / print method name here, handled by TestNg listener
        // declare("mm40reg23cancellation");

        CALL.$("LoginToGIAS4","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENNO");

        TYPE.$("ELE_TXTBOX_USERNAME","//input[@id='guestForm:username']","Mano","FALSE","TGTYPESCREENNO");

        TYPE.$("ELE_TXTBOX_PASSWORD","//input[@id='guestForm:password']","Cnx12345!","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_LOGIN","//button[@id='guestForm:loginButton']","TGTYPESCREENNO");

        CALL.$("PolicyPreInquiryCancellation","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_TASKSEARCH","//a[@id='workbenchForm:workbenchTabs:taskSearchContainer:tb_buttonSearch']//span[@class='fa fa-search fa-lg']","TGTYPESCREENREG");

		int var_Count = 0;
                 LOGGER.info("Executed Step = VAR,Integer,var_Count,0,TGTYPESCREENREG");
		com.jayway.jsonpath.ReadContext var_CSVData = getJsonData("http://10.1.104.229/json/20201020/bUCLxP.json");
                 LOGGER.info("Executed Step = VAR,Json,var_CSVData,http://10.1.104.229/json/20201020/bUCLxP.json,TGTYPESCREENREG");
		String var_PolicyNumber;
                 LOGGER.info("Executed Step = VAR,String,var_PolicyNumber,TGTYPESCREENREG");
		var_PolicyNumber = getJsonData(var_CSVData , "$.records["+var_Count+"].PolicyNumber");
                 LOGGER.info("Executed Step = STORE,var_PolicyNumber,var_CSVData,$.records[{var_Count}].PolicyNumber,TGTYPESCREENREG");
		String var_SessionDate;
                 LOGGER.info("Executed Step = VAR,String,var_SessionDate,TGTYPESCREENREG");
		var_SessionDate = getJsonData(var_CSVData , "$.records.["+var_Count+"].SessionDate");
                 LOGGER.info("Executed Step = STORE,var_SessionDate,var_CSVData,$.records.[{var_Count}].SessionDate,TGTYPESCREENREG");
        TAP.$("ELE_BUTTON_SESSIONDATE","//a[@id='headerForm:sessionDatelink']","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_NEWSESSIONDATE","//input[@id='sessionDateForm:sessionDateInput_input']",var_SessionDate,"FALSE","TGTYPESCREENREG");

        TAP.$("ELE_CHANGEDATE_BUTTON","//span[contains(text(),'Change Date')]","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_SEARCHTASKS","//input[@id='workbenchForm:workbenchTabs:taskSearchContainer:taskSearchSearch']","Policy Inquiry","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_TASKSEARCH","//a[@id='workbenchForm:workbenchTabs:taskSearchContainer:tb_buttonSearch']//span[@class='fa fa-search fa-lg']","TGTYPESCREENREG");

        TAP.$("ELE_LINK_POLICYINQUIRY","//span[contains(text(),'Policy Inquiry')]","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_POLICYNUMBER","//input[@id='workbenchForm:workbenchTabs:policyNumber']",var_PolicyNumber,"FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_CONTINUE","//span[contains(text(),'Continue')]","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        WAIT.$("ELE_GETVAL_POLICYSTATUS","//span[@id='workbenchForm:workbenchTabs:statusText']","VISIBLE","TGTYPESCREENREG");

        ASSERT.$("ELE_GETVAL_POLICYSTATUS","//span[@id='workbenchForm:workbenchTabs:statusText']","VISIBLE","TGTYPESCREENREG");

        SWIPE.$("DOWN","TGTYPESCREENREG");

        SWIPE.$("DOWN","TGTYPESCREENREG");

		String var_PaidtoDate;
                 LOGGER.info("Executed Step = VAR,String,var_PaidtoDate,TGTYPESCREENREG");
		var_PaidtoDate = ActionWrapper.getElementValue("ELE_GETVAL_PAIDTODATE", "//span[@id='workbenchForm:workbenchTabs:paidToDate']");
                 LOGGER.info("Executed Step = STORE,var_PaidtoDate,ELE_GETVAL_PAIDTODATE,TGTYPESCREENREG");
		takeFullScreenShot();
                 LOGGER.info("Executed Step = TAKEFULLSCREENSHOT,TGTYPESCREENREG");
        TAP.$("ELE_LINK_GIASHOME","//button[@id='headerForm:j_idt11']//span[@class='ui-button-text ui-c'][contains(text(),'GIAS')]","TGTYPESCREENREG");

        CALL.$("BenefitCancellation","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_TASKSEARCH","//a[@id='workbenchForm:workbenchTabs:taskSearchContainer:tb_buttonSearch']//span[@class='fa fa-search fa-lg']","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_SEARCHTASKS","//input[@id='workbenchForm:workbenchTabs:taskSearchContainer:taskSearchSearch']","policy benefit","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_TASKSEARCH","//a[@id='workbenchForm:workbenchTabs:taskSearchContainer:tb_buttonSearch']//span[@class='fa fa-search fa-lg']","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

        WAIT.$("ELE_LINK_CHANGEPOLICYBENEFITSTATUS","//span[contains(text(),\"Change a Policy Benefit's Status\")]","VISIBLE","TGTYPESCREENREG");

        TAP.$("ELE_LINK_CHANGEPOLICYBENEFITSTATUS","//span[contains(text(),\"Change a Policy Benefit's Status\")]","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_POLICYNUMBER","//input[@id='workbenchForm:workbenchTabs:policyNumber']",var_PolicyNumber,"FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_CONTINUE","//span[contains(text(),'Continue')]","TGTYPESCREENREG");

        TAP.$("ELE_SELECT_LINKBASEBENEFIT","//tr[@class='ui-widget-content ui-datatable-even ui-datatable-selectable']","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        TAP.$("ELE_DROPDWN_NEWBENEFITSTATUS","//label[@id='workbenchForm:workbenchTabs:newBenefitStatus_label']","TGTYPESCREENREG");

        TAP.$("ELE_LINK_BENEFITSTATUSCANCEL","//li[text()='Cancelled']","TGTYPESCREENREG");

        SWIPE.$("DOWN","TGTYPESCREENREG");

        SWIPE.$("DOWN","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_TERMINATIONDATE","//input[@id='workbenchForm:workbenchTabs:terminationDate_input']",var_PaidtoDate,"FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_VALIDATE","//span[contains(text(),'Validate')]","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SUBMIT","//span[contains(text(),'Submit')]","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

        ASSERT.$("ELE_GETVAL_SUCCESSMESSAGE","//div[@id='workbenchForm:workbenchTabs:messages']//span[@class='ui-messages-info-summary'][contains(text(),'Request completed successfully.')]","VISIBLE","TGTYPESCREENREG");

        TAP.$("ELE_LINK_GIASHOME","//button[@id='headerForm:j_idt11']//span[@class='ui-button-text ui-c'][contains(text(),'GIAS')]","TGTYPESCREENREG");

        CALL.$("PolicyPostInquiry","TGTYPESCREENREG");

        TAP.$("ELE_TXTBOX_SEARCHTASKS","//input[@id='workbenchForm:workbenchTabs:taskSearchContainer:taskSearchSearch']","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_SEARCHTASKS","//input[@id='workbenchForm:workbenchTabs:taskSearchContainer:taskSearchSearch']","Policy Inquiry","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_TASKSEARCH","//a[@id='workbenchForm:workbenchTabs:taskSearchContainer:tb_buttonSearch']//span[@class='fa fa-search fa-lg']","TGTYPESCREENREG");

        TAP.$("ELE_LINK_POLICYINQUIRY","//span[contains(text(),'Policy Inquiry')]","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_POLICYNUMBER","//input[@id='workbenchForm:workbenchTabs:policyNumber']",var_PolicyNumber,"FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_CONTINUE","//span[contains(text(),'Continue')]","TGTYPESCREENREG");

        WAIT.$(3,"TGTYPESCREENREG");

		takeFullScreenShot();
                 LOGGER.info("Executed Step = TAKEFULLSCREENSHOT,TGTYPESCREENREG");
        TAP.$("ELE_BUTTON_SHOWLINKS","//span[@class='ui-button-icon-left ui-icon ui-c fa fa-ellipsis-v fa-2x']","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_CLIENTTRANSACTIONHISTORY","//span[contains(text(),'Transaction History')]","TGTYPESCREENREG");

		takeFullScreenShot();
                 LOGGER.info("Executed Step = TAKEFULLSCREENSHOT,TGTYPESCREENREG");
        TAP.$("ELE_LOGO_GIAS1","//span[contains(text(),'GIAS')]","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");


    }


    @Test
    public void mm40reg06benefitchanges() {

        // No need to declare / print method name here, handled by TestNg listener
        // declare("mm40reg06benefitchanges");

        CALL.$("LoginToGIAS4","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENNO");

        TYPE.$("ELE_TXTBOX_USERNAME","//input[@id='guestForm:username']","Mano","FALSE","TGTYPESCREENNO");

        TYPE.$("ELE_TXTBOX_PASSWORD","//input[@id='guestForm:password']","Cnx12345!","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_LOGIN","//button[@id='guestForm:loginButton']","TGTYPESCREENNO");

        CALL.$("PolicyPreInquiryBenefitChange","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_TASKSEARCH","//a[@id='workbenchForm:workbenchTabs:taskSearchContainer:tb_buttonSearch']//span[@class='fa fa-search fa-lg']","TGTYPESCREENREG");

		int var_Count = 0;
                 LOGGER.info("Executed Step = VAR,Integer,var_Count,0,TGTYPESCREENREG");
		com.jayway.jsonpath.ReadContext var_CSVData = getJsonData("http://10.1.104.229/json/20200916/XnvFFr.json");
                 LOGGER.info("Executed Step = VAR,Json,var_CSVData,http://10.1.104.229/json/20200916/XnvFFr.json,TGTYPESCREENREG");
		String var_PolicyNumber;
                 LOGGER.info("Executed Step = VAR,String,var_PolicyNumber,TGTYPESCREENREG");
		var_PolicyNumber = getJsonData(var_CSVData , "$.records["+var_Count+"].PolicyNumber");
                 LOGGER.info("Executed Step = STORE,var_PolicyNumber,var_CSVData,$.records[{var_Count}].PolicyNumber,TGTYPESCREENREG");
		String var_SessionDate;
                 LOGGER.info("Executed Step = VAR,String,var_SessionDate,TGTYPESCREENREG");
		var_SessionDate = getJsonData(var_CSVData , "$.records.["+var_Count+"].SessionDate");
                 LOGGER.info("Executed Step = STORE,var_SessionDate,var_CSVData,$.records.[{var_Count}].SessionDate,TGTYPESCREENREG");
        TAP.$("ELE_BUTTON_SESSIONDATE","//a[@id='headerForm:sessionDatelink']","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_NEWSESSIONDATE","//input[@id='sessionDateForm:sessionDateInput_input']",var_SessionDate,"FALSE","TGTYPESCREENREG");

        TAP.$("ELE_CHANGEDATE_BUTTON","//span[contains(text(),'Change Date')]","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_SEARCHTASKS","//input[@id='workbenchForm:workbenchTabs:taskSearchContainer:taskSearchSearch']","Policy Inquiry","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_TASKSEARCH","//a[@id='workbenchForm:workbenchTabs:taskSearchContainer:tb_buttonSearch']//span[@class='fa fa-search fa-lg']","TGTYPESCREENREG");

        TAP.$("ELE_LINK_POLICYINQUIRY","//span[contains(text(),'Policy Inquiry')]","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_POLICYNUMBER","//input[@id='workbenchForm:workbenchTabs:policyNumber']",var_PolicyNumber,"FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_CONTINUE","//span[contains(text(),'Continue')]","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        WAIT.$("ELE_GETVAL_POLICYSTATUS","//span[@id='workbenchForm:workbenchTabs:statusText']","VISIBLE","TGTYPESCREENREG");

        ASSERT.$("ELE_GETVAL_POLICYSTATUS","//span[@id='workbenchForm:workbenchTabs:statusText']","VISIBLE","TGTYPESCREENREG");

		String var_PaidtoDate;
                 LOGGER.info("Executed Step = VAR,String,var_PaidtoDate,TGTYPESCREENREG");
		var_PaidtoDate = ActionWrapper.getElementValue("ELE_GETVAL_PAIDTODATE", "//span[@id='workbenchForm:workbenchTabs:paidToDate']");
                 LOGGER.info("Executed Step = STORE,var_PaidtoDate,ELE_GETVAL_PAIDTODATE,TGTYPESCREENREG");
		takeFullScreenShot();
                 LOGGER.info("Executed Step = TAKEFULLSCREENSHOT,TGTYPESCREENREG");
        TAP.$("ELE_LINK_GIASHOME","//button[@id='headerForm:j_idt11']//span[@class='ui-button-text ui-c'][contains(text(),'GIAS')]","TGTYPESCREENREG");

        CALL.$("BenefitChanges","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_TASKSEARCH","//a[@id='workbenchForm:workbenchTabs:taskSearchContainer:tb_buttonSearch']//span[@class='fa fa-search fa-lg']","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_SEARCHTASKS","//input[@id='workbenchForm:workbenchTabs:taskSearchContainer:taskSearchSearch']","policy benefit","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_TASKSEARCH","//a[@id='workbenchForm:workbenchTabs:taskSearchContainer:tb_buttonSearch']//span[@class='fa fa-search fa-lg']","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

        TAP.$("ELE_LINK_CHANGEAPOLICYBENEFIT","//button[@id='workbenchForm:workbenchTabs:taskSearchContainer:1:j_idt84']//span[@class='ui-button-text ui-c'][contains(text(),'Change a Policy Benefit')]","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_POLICYNUMBER","//input[@id='workbenchForm:workbenchTabs:policyNumber']",var_PolicyNumber,"FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_CONTINUE","//span[contains(text(),'Continue')]","TGTYPESCREENREG");

        WAIT.$(3,"TGTYPESCREENREG");

        TAP.$("ELE_SELECT_LINKBASEBENEFIT","//tr[@class='ui-widget-content ui-datatable-even ui-datatable-selectable']","TGTYPESCREENREG");

        WAIT.$(3,"TGTYPESCREENREG");

        WAIT.$("ELE_RDO_SELECTYES","//table[@id='workbenchForm:workbenchTabs:recalculatePremium']//label[contains(text(),'Yes')]","VISIBLE","TGTYPESCREENREG");

        TAP.$("ELE_RDO_SELECTYES","//table[@id='workbenchForm:workbenchTabs:recalculatePremium']//label[contains(text(),'Yes')]","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_VALIDATE","//span[contains(text(),'Validate')]","TGTYPESCREENREG");

        ASSERT.$("ELE_MSG_VALIDATIONSUCCESS","//div[@id='workbenchForm:workbenchTabs:messages']//span[@class='ui-messages-info-summary'][contains(text(),'Validation completed successfully with no errors.')]","VISIBLE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SAVE","//span[contains(text(),'Save')]","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

        LOGGER.info("Executed Step = START IF");
        if (check("ELE_CONFIRMACTION_YES","//div[@class='ui-dialog-content ui-widget-content ui-df-content']//iframe","VISIBLE","TGTYPESCREENREG")) {
         LOGGER.info("Executed Step = IF,ELE_CONFIRMACTION_YES,VISIBLE,TGTYPESCREENREG");
		try { 
		 driver.switchTo().frame(0);
		driver.findElement(By.xpath("//form[@id='confirmDialogForm']")).click();
		Thread.sleep(1000);
		driver.findElement(By.xpath("//button[@id='j_idt9']")).click();
		// Add Screenshot will only work for Android platform

		takeScreenshot();

		 
		 ;} 
		 catch(Exception e) { PrintVal("Renderer Script Exception : "+e.toString()); } 
		 PrintVal("Executed Step = CALLRENDERERSCRIPT,CLICKYESONCONFIRMACTION"); 
        }
          LOGGER.info("Executed Step = ENDIF,TGTYPESCREENREG");
        WAIT.$(5,"TGTYPESCREENREG");

        ASSERT.$("ELE_GETVAL_SUCCESSMESSAGE","//div[@id='workbenchForm:workbenchTabs:messages']//span[@class='ui-messages-info-summary'][contains(text(),'Request completed successfully.')]","VISIBLE","TGTYPESCREENREG");

        TAP.$("ELE_LINK_GIASHOME","//button[@id='headerForm:j_idt11']//span[@class='ui-button-text ui-c'][contains(text(),'GIAS')]","TGTYPESCREENREG");

        CALL.$("PolicyPostInquiryBenefitChanges","TGTYPESCREENREG");

        TAP.$("ELE_TXTBOX_SEARCHTASKS","//input[@id='workbenchForm:workbenchTabs:taskSearchContainer:taskSearchSearch']","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_SEARCHTASKS","//input[@id='workbenchForm:workbenchTabs:taskSearchContainer:taskSearchSearch']","Policy Inquiry","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_TASKSEARCH","//a[@id='workbenchForm:workbenchTabs:taskSearchContainer:tb_buttonSearch']//span[@class='fa fa-search fa-lg']","TGTYPESCREENREG");

        TAP.$("ELE_LINK_POLICYINQUIRY","//span[contains(text(),'Policy Inquiry')]","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_POLICYNUMBER","//input[@id='workbenchForm:workbenchTabs:policyNumber']",var_PolicyNumber,"FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_CONTINUE","//span[contains(text(),'Continue')]","TGTYPESCREENREG");

        WAIT.$(3,"TGTYPESCREENREG");

		takeFullScreenShot();
                 LOGGER.info("Executed Step = TAKEFULLSCREENSHOT,TGTYPESCREENREG");
        TAP.$("ELE_BUTTON_SHOWLINKS","//span[@class='ui-button-icon-left ui-icon ui-c fa fa-ellipsis-v fa-2x']","TGTYPESCREENREG");

        WAIT.$(3,"TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_CLIENTTRANSACTIONHISTORY","//span[contains(text(),'Transaction History')]","TGTYPESCREENREG");

		takeFullScreenShot();
                 LOGGER.info("Executed Step = TAKEFULLSCREENSHOT,TGTYPESCREENREG");
        WAIT.$(3,"TGTYPESCREENREG");

        TAP.$("ELE_LOGO_GIAS1","//span[contains(text(),'GIAS')]","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");


    }


    @Test
    public void mm04reg20accelerateddeath() {

        // No need to declare / print method name here, handled by TestNg listener
        // declare("mm04reg20accelerateddeath");

        CALL.$("LoginToGIAS4","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENNO");

        TYPE.$("ELE_TXTBOX_USERNAME","//input[@id='guestForm:username']","Mano","FALSE","TGTYPESCREENNO");

        TYPE.$("ELE_TXTBOX_PASSWORD","//input[@id='guestForm:password']","Cnx12345!","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_LOGIN","//button[@id='guestForm:loginButton']","TGTYPESCREENNO");

        CALL.$("PolicyPreInquiryAccDeath","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_TASKSEARCH","//a[@id='workbenchForm:workbenchTabs:taskSearchContainer:tb_buttonSearch']//span[@class='fa fa-search fa-lg']","TGTYPESCREENREG");

		int var_Count = 0;
                 LOGGER.info("Executed Step = VAR,Integer,var_Count,0,TGTYPESCREENREG");
		com.jayway.jsonpath.ReadContext var_CSVData = getJsonData("http://10.1.104.229/json/20200930/xyTfu9.json");
                 LOGGER.info("Executed Step = VAR,Json,var_CSVData,http://10.1.104.229/json/20200930/xyTfu9.json,TGTYPESCREENREG");
		String var_PolicyNumber;
                 LOGGER.info("Executed Step = VAR,String,var_PolicyNumber,TGTYPESCREENREG");
		var_PolicyNumber = getJsonData(var_CSVData , "$.records["+var_Count+"].PolicyNumber");
                 LOGGER.info("Executed Step = STORE,var_PolicyNumber,var_CSVData,$.records[{var_Count}].PolicyNumber,TGTYPESCREENREG");
		String var_SessionDate;
                 LOGGER.info("Executed Step = VAR,String,var_SessionDate,TGTYPESCREENREG");
		var_SessionDate = getJsonData(var_CSVData , "$.records.["+var_Count+"].SessionDate");
                 LOGGER.info("Executed Step = STORE,var_SessionDate,var_CSVData,$.records.[{var_Count}].SessionDate,TGTYPESCREENREG");
		String var_FirstName;
                 LOGGER.info("Executed Step = VAR,String,var_FirstName,TGTYPESCREENREG");
		var_FirstName = getJsonData(var_CSVData , "$.records.["+var_Count+"].FirstName");
                 LOGGER.info("Executed Step = STORE,var_FirstName,var_CSVData,$.records.[{var_Count}].FirstName,TGTYPESCREENREG");
		String var_LastName;
                 LOGGER.info("Executed Step = VAR,String,var_LastName,TGTYPESCREENREG");
		var_LastName = getJsonData(var_CSVData , "$.records.["+var_Count+"].LastName");
                 LOGGER.info("Executed Step = STORE,var_LastName,var_CSVData,$.records.[{var_Count}].LastName,TGTYPESCREENREG");
		String var_ApplyDate;
                 LOGGER.info("Executed Step = VAR,String,var_ApplyDate,TGTYPESCREENREG");
		var_ApplyDate = getJsonData(var_CSVData , "$.records.["+var_Count+"].ApplyDate");
                 LOGGER.info("Executed Step = STORE,var_ApplyDate,var_CSVData,$.records.[{var_Count}].ApplyDate,TGTYPESCREENREG");
        TAP.$("ELE_BUTTON_SESSIONDATE","//a[@id='headerForm:sessionDatelink']","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_NEWSESSIONDATE","//input[@id='sessionDateForm:sessionDateInput_input']",var_SessionDate,"FALSE","TGTYPESCREENREG");

        TAP.$("ELE_CHANGEDATE_BUTTON","//span[contains(text(),'Change Date')]","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_SEARCHTASKS","//input[@id='workbenchForm:workbenchTabs:taskSearchContainer:taskSearchSearch']","Policy Inquiry","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_TASKSEARCH","//a[@id='workbenchForm:workbenchTabs:taskSearchContainer:tb_buttonSearch']//span[@class='fa fa-search fa-lg']","TGTYPESCREENREG");

        TAP.$("ELE_LINK_POLICYINQUIRY","//span[contains(text(),'Policy Inquiry')]","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_POLICYNUMBER","//input[@id='workbenchForm:workbenchTabs:policyNumber']",var_PolicyNumber,"FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_CONTINUE","//span[contains(text(),'Continue')]","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        WAIT.$("ELE_GETVAL_POLICYSTATUS","//span[@id='workbenchForm:workbenchTabs:statusText']","VISIBLE","TGTYPESCREENREG");

        ASSERT.$("ELE_GETVAL_POLICYSTATUS","//span[@id='workbenchForm:workbenchTabs:statusText']","=","Premium Paying","TGTYPESCREENREG");

		String var_LablePolicyNumber;
                 LOGGER.info("Executed Step = VAR,String,var_LablePolicyNumber,TGTYPESCREENREG");
		var_LablePolicyNumber = ActionWrapper.getElementValue("ELE_LABEL_POLICYNUMBER", "//span[@id='workbenchForm:workbenchTabs:policyNumber']");
                 LOGGER.info("Executed Step = STORE,var_LablePolicyNumber,ELE_LABEL_POLICYNUMBER,TGTYPESCREENREG");
		String var_ClientsName;
                 LOGGER.info("Executed Step = VAR,String,var_ClientsName,TGTYPESCREENREG");
		var_ClientsName = ActionWrapper.getElementValue("ELE_GETVAL_CLIENTNAME", "//span[@id='workbenchForm:workbenchTabs:ownerName']");
                 LOGGER.info("Executed Step = STORE,var_ClientsName,ELE_GETVAL_CLIENTNAME,TGTYPESCREENREG");
		takeFullScreenShot();
                 LOGGER.info("Executed Step = TAKEFULLSCREENSHOT,TGTYPESCREENREG");
        TAP.$("ELE_LINK_GIASHOME","//button[@id='headerForm:j_idt11']//span[@class='ui-button-text ui-c'][contains(text(),'GIAS')]","TGTYPESCREENREG");

        CALL.$("ProcessingAcceleratedDeath","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_TASKSEARCH","//a[@id='workbenchForm:workbenchTabs:taskSearchContainer:tb_buttonSearch']//span[@class='fa fa-search fa-lg']","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_SEARCHTASKS","//input[@id='workbenchForm:workbenchTabs:taskSearchContainer:taskSearchSearch']","Policy death","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_TASKSEARCH","//a[@id='workbenchForm:workbenchTabs:taskSearchContainer:tb_buttonSearch']//span[@class='fa fa-search fa-lg']","TGTYPESCREENREG");

        TAP.$("ELE_LINK_POLICYDEATHANDLIVINGBENEFITCLAIMS","//span[contains(text(),'Policy Death and Living Benefit Claims')]","TGTYPESCREENREG");

        WAIT.$("ELE_LINK_ADDLIVINGBENEFIT","//span[contains(text(),'Add Living Benefit')]","VISIBLE","TGTYPESCREENREG");

        TAP.$("ELE_LINK_ADDLIVINGBENEFIT","//span[contains(text(),'Add Living Benefit')]","TGTYPESCREENREG");

        WAIT.$(3,"TGTYPESCREENREG");

        TAP.$("ELE_REASONFORCLAIM_ACCDEATHBENEFIT","//label[@id='workbenchForm:workbenchTabs:transactionRsnCode_label']","TGTYPESCREENREG");

        TAP.$("ELE_SELECTREASONCLAIM_ACCELERATEDDEATHBENEFIT","//li[@id='workbenchForm:workbenchTabs:transactionRsnCode_1']","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SHOWLINKS","//span[@class='ui-button-icon-left ui-icon ui-c fa fa-ellipsis-v fa-2x']","TGTYPESCREENREG");

        TAP.$("ELE_LINK_SELECTCLIENT","//span[contains(text(),'Select Client')]","TGTYPESCREENREG");

        TAP.$("ELE_TXTBOX_SEARCH","//input[@id='workbenchForm:workbenchTabs:searchName']","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_SEARCH","//input[@id='workbenchForm:workbenchTabs:searchName']",var_ClientsName,"FALSE","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SEARCH","//span[contains(text(),'Search')]","TGTYPESCREENREG");

        WAIT.$(3,"TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SELECTCLIENT","//button[@id='workbenchForm:workbenchTabs:button_select']","TGTYPESCREENREG");

        WAIT.$("ELE_BUTTON_CONTINUE","//span[contains(text(),'Continue')]","VISIBLE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_CONTINUE","//span[contains(text(),'Continue')]","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        WAIT.$("ELE_REASONFORCLAIM_ACCDEATHBENEFIT","//label[@id='workbenchForm:workbenchTabs:transactionRsnCode_label']","VISIBLE","TGTYPESCREENREG");

        SWIPE.$("UP","ELE_DROPDWN_BENEFITOPTION","//label[@id='workbenchForm:workbenchTabs:options_label']","TGTYPESCREENREG");

        TAP.$("ELE_DATE_TERMINALILLNESSSTARTDATE","//input[@id='workbenchForm:workbenchTabs:termIllStartDate_input']","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

        TYPE.$("ELE_DATE_TERMINALILLNESSSTARTDATE","//input[@id='workbenchForm:workbenchTabs:termIllStartDate_input']",var_ApplyDate,"FALSE","TGTYPESCREENREG");

        TAP.$("ELE_DROPDWN_BENEFITOPTION","//label[@id='workbenchForm:workbenchTabs:options_label']","TGTYPESCREENREG");

        SWIPE.$("DOWN","TGTYPESCREENREG");

        SWIPE.$("DOWN","TGTYPESCREENREG");

        TAP.$("ELE_DROPDWNREASONCLAIMSELECT_ONEREDUCTION75","//li[@id='workbenchForm:workbenchTabs:options_1']","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_VALIDATE","//span[contains(text(),'Validate')]","TGTYPESCREENREG");

        ASSERT.$("ELE_MSG_VALIDATIONSUCCESS","//div[@id='workbenchForm:workbenchTabs:messages']//span[@class='ui-messages-info-summary'][contains(text(),'Validation completed successfully with no errors.')]","VISIBLE","TGTYPESCREENREG");

		takeFullScreenShot();
                 LOGGER.info("Executed Step = TAKEFULLSCREENSHOT,TGTYPESCREENREG");
        TAP.$("ELE_BUTTON_SAVE","//span[contains(text(),'Save')]","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

		int var_AmountApproved;
                 LOGGER.info("Executed Step = VAR,Integer,var_AmountApproved,TGTYPESCREENREG");
		var_AmountApproved = ActionWrapper.getElementValueForVariableWithInt("ELE_DISPLAYCLAIMS_AMOUNTAPPROVED", "//span[@id='workbenchForm:workbenchTabs:totalClaimAmountAppr']");
                 LOGGER.info("Executed Step = STORE,var_AmountApproved,ELE_DISPLAYCLAIMS_AMOUNTAPPROVED,TGTYPESCREENREG");
        ASSERT.$("ELE_LABEL_ITEMSUCCESSFULLYADDED","//div[@id='workbenchForm:workbenchTabs:messages']//span[@class='ui-messages-info-summary'][contains(text(),'Item successfully added.')]","VISIBLE","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SHOWLINKS","//span[@class='ui-button-icon-left ui-icon ui-c fa fa-ellipsis-v fa-2x']","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

        TAP.$("ELE_LINK_CLAIMDETAIL","//span[contains(text(),'Claim Detail')]","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

        TAP.$("ELE_TAB_DISPLAYCLAIMACCD","//a[contains(text(),'Display Claim')]","TGTYPESCREENREG");

        WAIT.$(4,"TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SHOWLINKS","//span[@class='ui-button-icon-left ui-icon ui-c fa fa-ellipsis-v fa-2x']","TGTYPESCREENREG");

        WAIT.$(4,"TGTYPESCREENREG");

        TAP.$("ELE_LINK_PAYMENTS","//span[contains(text(),'Payments')]","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_ADDSUSPENCETASK","//span[contains(text(),'Add')]","TGTYPESCREENREG");

        TAP.$("ELE_RDOBUTTON_APPROVECLAIM","//label[contains(text(),'Approved')]","TGTYPESCREENREG");

        SWIPE.$("DOWN","ELE_TXTBOX_BENEFICIARYFIRSTNAME2","//input[@id='workbenchForm:workbenchTabs:firstName']","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_BENEFICIARYFIRSTNAME2","//input[@id='workbenchForm:workbenchTabs:firstName']",var_FirstName,"FALSE","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_BENEFICIARYLASTNAME2","//input[@id='workbenchForm:workbenchTabs:lastName']",var_LastName,"FALSE","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_CITY","//input[@id='workbenchForm:workbenchTabs:addressCity']","GREENVILLE","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_DRPDWN_ISSUESTATECOUNTRY","//label[@id='workbenchForm:workbenchTabs:countryAndStateCode_label']","TGTYPESCREENREG");

        TAP.$("ELE_SELECTSTATE_SOUTHCAROLINA","//li[@id='workbenchForm:workbenchTabs:countryAndStateCode_49']","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_ADDRESSLINE1","//input[@id='workbenchForm:workbenchTabs:addressLineOne']","DO NOT MAIL","FALSE","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

        SWIPE.$("DOWN","ELE_TXTBOX_TOTALPAYMENTAMOUNT","//input[@id='workbenchForm:workbenchTabs:totalClaimAmountAppr3_input']","TGTYPESCREENREG");

        SWIPE.$("DOWN","ELE_TXTBOX_TOTALPAYMENTAMOUNT","//input[@id='workbenchForm:workbenchTabs:totalClaimAmountAppr3_input']","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_TOTALPAYMENTAMOUNT","//input[@id='workbenchForm:workbenchTabs:totalClaimAmountAppr3_input']",var_AmountApproved,"FALSE","TGTYPESCREENREG");

        TAP.$("ELE_DRPDWN_INTERESTRATESTATE","//label[@id='workbenchForm:workbenchTabs:countryAndStateCodeForInterestRateLookup_label']","TGTYPESCREENREG");

        TAP.$("ELE_SELECT_INTERESTSATESC","//li[@id='workbenchForm:workbenchTabs:countryAndStateCodeForInterestRateLookup_49']","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_VALIDATE","//span[contains(text(),'Validate')]","TGTYPESCREENREG");

		takeFullScreenShot();
                 LOGGER.info("Executed Step = TAKEFULLSCREENSHOT,TGTYPESCREENREG");
        WAIT.$("ELE_BUTTON_SAVE","//span[contains(text(),'Save')]","VISIBLE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SAVE","//span[contains(text(),'Save')]","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

        TAP.$("ELE_TAB_DISPLAYCLAIMACCD","//a[contains(text(),'Display Claim')]","TGTYPESCREENREG");

        WAIT.$(4,"TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SHOWLINKS","//span[@class='ui-button-icon-left ui-icon ui-c fa fa-ellipsis-v fa-2x']","TGTYPESCREENREG");

        TAP.$("ELE_LINK_PAYMENTS","//span[contains(text(),'Payments')]","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

        TAP.$("ELE_SELECT_LINKBASEBENEFIT","//tr[@class='ui-widget-content ui-datatable-even ui-datatable-selectable']","TGTYPESCREENREG");

        WAIT.$(3,"TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_EDIT","//span[text()='Edit']","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

        TAP.$("ELE_RDOBTN_APPROVECLAIMPAYMENT","//table[@id='workbenchForm:workbenchTabs:beneficiaryPmtStat']//span[@class='ui-radiobutton-icon ui-icon ui-icon-blank ui-c']","TGTYPESCREENREG");

        SWIPE.$("DOWN","TGTYPESCREENREG");

        SWIPE.$("DOWN","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_DATECLAIMAPPROVED","//input[@id='workbenchForm:workbenchTabs:dateClaimApprDate_input']",var_SessionDate,"FALSE","TGTYPESCREENREG");

		takeFullScreenShot();
                 LOGGER.info("Executed Step = TAKEFULLSCREENSHOT,TGTYPESCREENREG");
        TAP.$("ELE_BUTTON_SAVE","//span[contains(text(),'Save')]","TGTYPESCREENREG");

        WAIT.$(4,"TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SHOWLINKS","//span[@class='ui-button-icon-left ui-icon ui-c fa fa-ellipsis-v fa-2x']","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

        TAP.$("ELE_LINK_PAYMENTS","//span[contains(text(),'Payments')]","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

        TAP.$("ELE_SELECT_LINKBASEBENEFIT","//tr[@class='ui-widget-content ui-datatable-even ui-datatable-selectable']","TGTYPESCREENREG");

        WAIT.$("ELE_LINK_PAYCLAIM","//span[contains(text(),'Pay Claim')]","VISIBLE","TGTYPESCREENREG");

        TAP.$("ELE_LINK_PAYCLAIM","//span[contains(text(),'Pay Claim')]","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_VALIDATE","//span[contains(text(),'Validate')]","TGTYPESCREENREG");

        ASSERT.$("ELE_MSG_VALIDATIONSUCCESS","//div[@id='workbenchForm:workbenchTabs:messages']//span[@class='ui-messages-info-summary'][contains(text(),'Validation completed successfully with no errors.')]","VISIBLE","TGTYPESCREENREG");

		takeFullScreenShot();
                 LOGGER.info("Executed Step = TAKEFULLSCREENSHOT,TGTYPESCREENREG");
        WAIT.$("ELE_BUTTON_SAVE","//span[contains(text(),'Save')]","VISIBLE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SAVE","//span[contains(text(),'Save')]","TGTYPESCREENREG");

        ASSERT.$("ELE_MSG_ITEMSUCCESSUPDATE","//div[@id='workbenchForm:workbenchTabs:messages']//span[@class='ui-messages-info-summary'][contains(text(),'Item successfully updated.')]","VISIBLE","TGTYPESCREENREG");

        WAIT.$(3,"TGTYPESCREENREG");

        TAP.$("ELE_TAB_DISPLAYCLAIMACCD","//a[contains(text(),'Display Claim')]","TGTYPESCREENREG");

		takeFullScreenShot();
                 LOGGER.info("Executed Step = TAKEFULLSCREENSHOT,TGTYPESCREENREG");
        TAP.$("ELE_LOGO_GIAS1","//span[contains(text(),'GIAS')]","TGTYPESCREENREG");

        CALL.$("PostPolicyInquiryAccDeath","TGTYPESCREENREG");

        TAP.$("ELE_TXTBOX_SEARCHTASKS","//input[@id='workbenchForm:workbenchTabs:taskSearchContainer:taskSearchSearch']","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_SEARCHTASKS","//input[@id='workbenchForm:workbenchTabs:taskSearchContainer:taskSearchSearch']","Policy Inquiry","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_TASKSEARCH","//a[@id='workbenchForm:workbenchTabs:taskSearchContainer:tb_buttonSearch']//span[@class='fa fa-search fa-lg']","TGTYPESCREENREG");

        TAP.$("ELE_LINK_POLICYINQUIRY","//span[contains(text(),'Policy Inquiry')]","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_POLICYNUMBER","//input[@id='workbenchForm:workbenchTabs:policyNumber']",var_PolicyNumber,"FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_CONTINUE","//span[contains(text(),'Continue')]","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        WAIT.$("ELE_GETVAL_POLICYSTATUS","//span[@id='workbenchForm:workbenchTabs:statusText']","VISIBLE","TGTYPESCREENREG");

		takeFullScreenShot();
                 LOGGER.info("Executed Step = TAKEFULLSCREENSHOT,TGTYPESCREENREG");
        TAP.$("ELE_BUTTON_SHOWLINKS","//span[@class='ui-button-icon-left ui-icon ui-c fa fa-ellipsis-v fa-2x']","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_CLIENTTRANSACTIONHISTORY","//span[contains(text(),'Transaction History')]","TGTYPESCREENREG");

		takeFullScreenShot();
                 LOGGER.info("Executed Step = TAKEFULLSCREENSHOT,TGTYPESCREENREG");
        WAIT.$(2,"TGTYPESCREENREG");

        TAP.$("ELE_LOGO_GIAS1","//span[contains(text(),'GIAS')]","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

        SWIPE.$("DOWN","TGTYPESCREENREG");

        SWIPE.$("DOWN","ELE_BUTTON_TASKSEARCH","//a[@id='workbenchForm:workbenchTabs:taskSearchContainer:tb_buttonSearch']//span[@class='fa fa-search fa-lg']","TGTYPESCREENREG");

        WAIT.$("ELE_BUTTON_TASKSEARCH","//a[@id='workbenchForm:workbenchTabs:taskSearchContainer:tb_buttonSearch']//span[@class='fa fa-search fa-lg']","VISIBLE","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_SEARCHTASKS","//input[@id='workbenchForm:workbenchTabs:taskSearchContainer:taskSearchSearch']","ledger extract","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_TASKSEARCH","//a[@id='workbenchForm:workbenchTabs:taskSearchContainer:tb_buttonSearch']//span[@class='fa fa-search fa-lg']","TGTYPESCREENREG");

        TAP.$("ELE_LINK_LEDGEREXTRACTENTRIES","//span[contains(text(),'Ledger Extract Entries')]","TGTYPESCREENREG");

        TYPE.$("ELE_LEDGEREXTRACT_REFERENCE2","//input[@id='workbenchForm:workbenchTabs:grid:referenceNumber2_Col:filter']",var_LablePolicyNumber,"FALSE","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

        TAP.$("ELE_LOGO_GIAS1","//span[contains(text(),'GIAS')]","TGTYPESCREENREG");


    }


    @Test
    public void mm40reg24ridercancellation() {

        // No need to declare / print method name here, handled by TestNg listener
        // declare("mm40reg24ridercancellation");

        CALL.$("LoginToGIAS4","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENNO");

        TYPE.$("ELE_TXTBOX_USERNAME","//input[@id='guestForm:username']","Mano","FALSE","TGTYPESCREENNO");

        TYPE.$("ELE_TXTBOX_PASSWORD","//input[@id='guestForm:password']","Cnx12345!","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_LOGIN","//button[@id='guestForm:loginButton']","TGTYPESCREENNO");

        CALL.$("PolicyPreinquiryRiderCancellation","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_TASKSEARCH","//a[@id='workbenchForm:workbenchTabs:taskSearchContainer:tb_buttonSearch']//span[@class='fa fa-search fa-lg']","TGTYPESCREENREG");

		int var_Count = 0;
                 LOGGER.info("Executed Step = VAR,Integer,var_Count,0,TGTYPESCREENREG");
		com.jayway.jsonpath.ReadContext var_CSVData = getJsonData("http://10.1.104.229/json/20200916/nQf7Sf.json");
                 LOGGER.info("Executed Step = VAR,Json,var_CSVData,http://10.1.104.229/json/20200916/nQf7Sf.json,TGTYPESCREENREG");
		String var_PolicyNumber;
                 LOGGER.info("Executed Step = VAR,String,var_PolicyNumber,TGTYPESCREENREG");
		var_PolicyNumber = getJsonData(var_CSVData , "$.records["+var_Count+"].PolicyNumber");
                 LOGGER.info("Executed Step = STORE,var_PolicyNumber,var_CSVData,$.records[{var_Count}].PolicyNumber,TGTYPESCREENREG");
		String var_SessionDate;
                 LOGGER.info("Executed Step = VAR,String,var_SessionDate,TGTYPESCREENREG");
		var_SessionDate = getJsonData(var_CSVData , "$.records.["+var_Count+"].SessionDate");
                 LOGGER.info("Executed Step = STORE,var_SessionDate,var_CSVData,$.records.[{var_Count}].SessionDate,TGTYPESCREENREG");
        TAP.$("ELE_BUTTON_SESSIONDATE","//a[@id='headerForm:sessionDatelink']","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_NEWSESSIONDATE","//input[@id='sessionDateForm:sessionDateInput_input']",var_SessionDate,"FALSE","TGTYPESCREENREG");

        TAP.$("ELE_CHANGEDATE_BUTTON","//span[contains(text(),'Change Date')]","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_SEARCHTASKS","//input[@id='workbenchForm:workbenchTabs:taskSearchContainer:taskSearchSearch']","Policy Inquiry","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_TASKSEARCH","//a[@id='workbenchForm:workbenchTabs:taskSearchContainer:tb_buttonSearch']//span[@class='fa fa-search fa-lg']","TGTYPESCREENREG");

        TAP.$("ELE_LINK_POLICYINQUIRY","//span[contains(text(),'Policy Inquiry')]","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_POLICYNUMBER","//input[@id='workbenchForm:workbenchTabs:policyNumber']",var_PolicyNumber,"FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_CONTINUE","//span[contains(text(),'Continue')]","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        WAIT.$("ELE_GETVAL_POLICYSTATUS","//span[@id='workbenchForm:workbenchTabs:statusText']","VISIBLE","TGTYPESCREENREG");

        ASSERT.$("ELE_GETVAL_POLICYSTATUS","//span[@id='workbenchForm:workbenchTabs:statusText']","VISIBLE","TGTYPESCREENREG");

		String var_PaidtoDate;
                 LOGGER.info("Executed Step = VAR,String,var_PaidtoDate,TGTYPESCREENREG");
		var_PaidtoDate = ActionWrapper.getElementValue("ELE_GETVAL_PAIDTODATE", "//span[@id='workbenchForm:workbenchTabs:paidToDate']");
                 LOGGER.info("Executed Step = STORE,var_PaidtoDate,ELE_GETVAL_PAIDTODATE,TGTYPESCREENREG");
		takeFullScreenShot();
                 LOGGER.info("Executed Step = TAKEFULLSCREENSHOT,TGTYPESCREENREG");
        TAP.$("ELE_LINK_GIASHOME","//button[@id='headerForm:j_idt11']//span[@class='ui-button-text ui-c'][contains(text(),'GIAS')]","TGTYPESCREENREG");

        CALL.$("RiderCancellation","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_TASKSEARCH","//a[@id='workbenchForm:workbenchTabs:taskSearchContainer:tb_buttonSearch']//span[@class='fa fa-search fa-lg']","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_SEARCHTASKS","//input[@id='workbenchForm:workbenchTabs:taskSearchContainer:taskSearchSearch']","policy benefit","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_TASKSEARCH","//a[@id='workbenchForm:workbenchTabs:taskSearchContainer:tb_buttonSearch']//span[@class='fa fa-search fa-lg']","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

        TAP.$("ELE_LINK_CHANGEPOLICYBENEFITSTATUS","//span[contains(text(),\"Change a Policy Benefit's Status\")]","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_POLICYNUMBER","//input[@id='workbenchForm:workbenchTabs:policyNumber']",var_PolicyNumber,"FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_CONTINUE","//span[contains(text(),'Continue')]","TGTYPESCREENREG");

        WAIT.$("ELE_BENEFIT_SELECTLINKRIDER1","//span[text()='Rider 1']","VISIBLE","TGTYPESCREENREG");

        TAP.$("ELE_BENEFIT_SELECTLINKRIDER1","//span[text()='Rider 1']","TGTYPESCREENREG");

        WAIT.$(3,"TGTYPESCREENREG");

        WAIT.$("ELE_DROPDWN_NEWBENEFITSTATUS","//label[@id='workbenchForm:workbenchTabs:newBenefitStatus_label']","VISIBLE","TGTYPESCREENREG");

        TAP.$("ELE_DROPDWN_NEWBENEFITSTATUS","//label[@id='workbenchForm:workbenchTabs:newBenefitStatus_label']","TGTYPESCREENREG");

        TAP.$("ELE_LINK_BENEFITSTATUSCANCEL","//li[text()='Cancelled']","TGTYPESCREENREG");

        SWIPE.$("UP","TGTYPESCREENREG");

        SWIPE.$("UP","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_TERMINATIONDATE","//input[@id='workbenchForm:workbenchTabs:terminationDate_input']",var_PaidtoDate,"FALSE","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_VALIDATE","//span[contains(text(),'Validate')]","TGTYPESCREENREG");

        WAIT.$(3,"TGTYPESCREENREG");

        ASSERT.$("ELE_MSG_VALIDATIONSUCCESS","//div[@id='workbenchForm:workbenchTabs:messages']//span[@class='ui-messages-info-summary'][contains(text(),'Validation completed successfully with no errors.')]","VISIBLE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SUBMIT","//span[contains(text(),'Submit')]","TGTYPESCREENREG");

        WAIT.$(3,"TGTYPESCREENREG");

        ASSERT.$("ELE_GETVAL_SUCCESSMESSAGE","//div[@id='workbenchForm:workbenchTabs:messages']//span[@class='ui-messages-info-summary'][contains(text(),'Request completed successfully.')]","VISIBLE","TGTYPESCREENREG");

        TAP.$("ELE_LINK_GIASHOME","//button[@id='headerForm:j_idt11']//span[@class='ui-button-text ui-c'][contains(text(),'GIAS')]","TGTYPESCREENREG");

        CALL.$("PolicyPostInquiry","TGTYPESCREENREG");

        TAP.$("ELE_TXTBOX_SEARCHTASKS","//input[@id='workbenchForm:workbenchTabs:taskSearchContainer:taskSearchSearch']","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_SEARCHTASKS","//input[@id='workbenchForm:workbenchTabs:taskSearchContainer:taskSearchSearch']","Policy Inquiry","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_TASKSEARCH","//a[@id='workbenchForm:workbenchTabs:taskSearchContainer:tb_buttonSearch']//span[@class='fa fa-search fa-lg']","TGTYPESCREENREG");

        TAP.$("ELE_LINK_POLICYINQUIRY","//span[contains(text(),'Policy Inquiry')]","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_POLICYNUMBER","//input[@id='workbenchForm:workbenchTabs:policyNumber']",var_PolicyNumber,"FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_CONTINUE","//span[contains(text(),'Continue')]","TGTYPESCREENREG");

        WAIT.$(3,"TGTYPESCREENREG");

		takeFullScreenShot();
                 LOGGER.info("Executed Step = TAKEFULLSCREENSHOT,TGTYPESCREENREG");
        TAP.$("ELE_BUTTON_SHOWLINKS","//span[@class='ui-button-icon-left ui-icon ui-c fa fa-ellipsis-v fa-2x']","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_CLIENTTRANSACTIONHISTORY","//span[contains(text(),'Transaction History')]","TGTYPESCREENREG");

		takeFullScreenShot();
                 LOGGER.info("Executed Step = TAKEFULLSCREENSHOT,TGTYPESCREENREG");
        TAP.$("ELE_LOGO_GIAS1","//span[contains(text(),'GIAS')]","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");


    }


    @Test
    public void mm40regdeathclaimwholelife() {

        // No need to declare / print method name here, handled by TestNg listener
        // declare("mm40regdeathclaimwholelife");

        CALL.$("LoginToGIAS4","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENNO");

        TYPE.$("ELE_TXTBOX_USERNAME","//input[@id='guestForm:username']","Mano","FALSE","TGTYPESCREENNO");

        TYPE.$("ELE_TXTBOX_PASSWORD","//input[@id='guestForm:password']","Cnx12345!","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_LOGIN","//button[@id='guestForm:loginButton']","TGTYPESCREENNO");

        CALL.$("PrePolicyDeathClaimWholeLife","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SESSIONDATE","//a[@id='headerForm:sessionDatelink']","TGTYPESCREENREG");

		int var_Count = 0;
                 LOGGER.info("Executed Step = VAR,Integer,var_Count,0,TGTYPESCREENREG");
		com.jayway.jsonpath.ReadContext var_CSVData = getJsonData("http://10.1.104.229/json/20200916/1czkQb.json");
                 LOGGER.info("Executed Step = VAR,Json,var_CSVData,http://10.1.104.229/json/20200916/1czkQb.json,TGTYPESCREENREG");
		String var_PolicyNumber;
                 LOGGER.info("Executed Step = VAR,String,var_PolicyNumber,TGTYPESCREENREG");
		var_PolicyNumber = getJsonData(var_CSVData , "$.records["+var_Count+"].PolicyNumber");
                 LOGGER.info("Executed Step = STORE,var_PolicyNumber,var_CSVData,$.records[{var_Count}].PolicyNumber,TGTYPESCREENREG");
		String var_SessionDate;
                 LOGGER.info("Executed Step = VAR,String,var_SessionDate,TGTYPESCREENREG");
		var_SessionDate = getJsonData(var_CSVData , "$.records.["+var_Count+"].SessionDate");
                 LOGGER.info("Executed Step = STORE,var_SessionDate,var_CSVData,$.records.[{var_Count}].SessionDate,TGTYPESCREENREG");
		String var_SessionDate1;
                 LOGGER.info("Executed Step = VAR,String,var_SessionDate1,TGTYPESCREENREG");
		var_SessionDate1 = getJsonData(var_CSVData , "$.records.["+var_Count+"].SessionDate1");
                 LOGGER.info("Executed Step = STORE,var_SessionDate1,var_CSVData,$.records.[{var_Count}].SessionDate1,TGTYPESCREENREG");
		String var_FirstName;
                 LOGGER.info("Executed Step = VAR,String,var_FirstName,TGTYPESCREENREG");
		var_FirstName = getJsonData(var_CSVData , "$.records.["+var_Count+"].FirstName");
                 LOGGER.info("Executed Step = STORE,var_FirstName,var_CSVData,$.records.[{var_Count}].FirstName,TGTYPESCREENREG");
		String var_LastName;
                 LOGGER.info("Executed Step = VAR,String,var_LastName,TGTYPESCREENREG");
		var_LastName = getJsonData(var_CSVData , "$.records.["+var_Count+"].LastName");
                 LOGGER.info("Executed Step = STORE,var_LastName,var_CSVData,$.records.[{var_Count}].LastName,TGTYPESCREENREG");
		String var_DateOfDeath;
                 LOGGER.info("Executed Step = VAR,String,var_DateOfDeath,TGTYPESCREENREG");
		var_DateOfDeath = getJsonData(var_CSVData , "$.records.["+var_Count+"].DateOfDeath");
                 LOGGER.info("Executed Step = STORE,var_DateOfDeath,var_CSVData,$.records.[{var_Count}].DateOfDeath,TGTYPESCREENREG");
		String var_NotificationDate;
                 LOGGER.info("Executed Step = VAR,String,var_NotificationDate,TGTYPESCREENREG");
		var_NotificationDate = getJsonData(var_CSVData , "$.records.["+var_Count+"].NotificationDate");
                 LOGGER.info("Executed Step = STORE,var_NotificationDate,var_CSVData,$.records.[{var_Count}].NotificationDate,TGTYPESCREENREG");
		String var_SSN;
                 LOGGER.info("Executed Step = VAR,String,var_SSN,TGTYPESCREENREG");
		var_SSN = getJsonData(var_CSVData , "$.records.["+var_Count+"].SSN");
                 LOGGER.info("Executed Step = STORE,var_SSN,var_CSVData,$.records.[{var_Count}].SSN,TGTYPESCREENREG");
        TYPE.$("ELE_TXTBOX_NEWSESSIONDATE","//input[@id='sessionDateForm:sessionDateInput_input']",var_SessionDate,"FALSE","TGTYPESCREENREG");

        TAP.$("ELE_CHANGEDATE_BUTTON","//span[contains(text(),'Change Date')]","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_SEARCHTASKS","//input[@id='workbenchForm:workbenchTabs:taskSearchContainer:taskSearchSearch']","Policy Inquiry","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_TASKSEARCH","//a[@id='workbenchForm:workbenchTabs:taskSearchContainer:tb_buttonSearch']//span[@class='fa fa-search fa-lg']","TGTYPESCREENREG");

        TAP.$("ELE_LINK_POLICYINQUIRY","//span[contains(text(),'Policy Inquiry')]","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_POLICYNUMBER","//input[@id='workbenchForm:workbenchTabs:policyNumber']",var_PolicyNumber,"FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_CONTINUE","//span[contains(text(),'Continue')]","TGTYPESCREENREG");

        WAIT.$("ELE_GETVAL_POLICYSTATUS","//span[@id='workbenchForm:workbenchTabs:statusText']","VISIBLE","TGTYPESCREENREG");

        ASSERT.$("ELE_GETVAL_POLICYSTATUS","//span[@id='workbenchForm:workbenchTabs:statusText']","VISIBLE","TGTYPESCREENREG");

		String var_LablePolicyNumber;
                 LOGGER.info("Executed Step = VAR,String,var_LablePolicyNumber,TGTYPESCREENREG");
		var_LablePolicyNumber = ActionWrapper.getElementValue("ELE_LABEL_POLICYNUMBER", "//span[@id='workbenchForm:workbenchTabs:policyNumber']");
                 LOGGER.info("Executed Step = STORE,var_LablePolicyNumber,ELE_LABEL_POLICYNUMBER,TGTYPESCREENREG");
		String var_ClientsName;
                 LOGGER.info("Executed Step = VAR,String,var_ClientsName,TGTYPESCREENREG");
		var_ClientsName = ActionWrapper.getElementValue("ELE_GETVAL_CLIENTNAME", "//span[@id='workbenchForm:workbenchTabs:ownerName']");
                 LOGGER.info("Executed Step = STORE,var_ClientsName,ELE_GETVAL_CLIENTNAME,TGTYPESCREENREG");
		takeFullScreenShot();
                 LOGGER.info("Executed Step = TAKEFULLSCREENSHOT,TGTYPESCREENREG");
        TAP.$("ELE_LINK_GIASHOME","//button[@id='headerForm:j_idt11']//span[@class='ui-button-text ui-c'][contains(text(),'GIAS')]","TGTYPESCREENREG");

        CALL.$("ProcessingDeathClaimForWholeLife","TGTYPESCREENREG");

        WAIT.$("ELE_TXTBOX_SEARCHTASKS","//input[@id='workbenchForm:workbenchTabs:taskSearchContainer:taskSearchSearch']","VISIBLE","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_SEARCHTASKS","//input[@id='workbenchForm:workbenchTabs:taskSearchContainer:taskSearchSearch']","Policy death","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_TASKSEARCH","//a[@id='workbenchForm:workbenchTabs:taskSearchContainer:tb_buttonSearch']//span[@class='fa fa-search fa-lg']","TGTYPESCREENREG");

        TAP.$("ELE_LINK_POLICYDEATHANDLIVINGBENEFITCLAIMS","//span[contains(text(),'Policy Death and Living Benefit Claims')]","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

        TAP.$("ELE_LINK_ADDDEATHCLAIM","//span[contains(text(),'Add Death Claim')]","TGTYPESCREENREG");

        WAIT.$(3,"TGTYPESCREENREG");

        TYPE.$("ELE_DATE_NOTIFICATONDATE","//input[@id='workbenchForm:workbenchTabs:notiOfDthDate_input']",var_NotificationDate,"FALSE","TGTYPESCREENREG");

        TYPE.$("ELE_DATE_DATEOFDEATH","//input[@id='workbenchForm:workbenchTabs:dateOfDeathDate_input']",var_DateOfDeath,"FALSE","TGTYPESCREENREG");

        TYPE.$("ELE_DATE_DEATHCERTRCVDDATE","//input[@id='workbenchForm:workbenchTabs:deathCertRcvdDate_input']",var_SessionDate,"FALSE","TGTYPESCREENREG");

        TAP.$("ELE_REASONFORCLAIM_ACCDEATHBENEFIT","//label[@id='workbenchForm:workbenchTabs:transactionRsnCode_label']","TGTYPESCREENREG");

        TAP.$("ELE_DROPDWNSELECT_CANCER","//li[@id='workbenchForm:workbenchTabs:transactionRsnCode_5']","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SHOWLINKS","//span[@class='ui-button-icon-left ui-icon ui-c fa fa-ellipsis-v fa-2x']","TGTYPESCREENREG");

        TAP.$("ELE_LINK_SELECTCLIENT","//span[contains(text(),'Select Client')]","TGTYPESCREENREG");

        TAP.$("ELE_TXTBOX_SEARCH","//input[@id='workbenchForm:workbenchTabs:searchName']","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_SEARCH","//input[@id='workbenchForm:workbenchTabs:searchName']",var_ClientsName,"FALSE","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SEARCH","//span[contains(text(),'Search')]","TGTYPESCREENREG");

        WAIT.$(3,"TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SELECTCLIENT","//button[@id='workbenchForm:workbenchTabs:button_select']","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_CONTINUE","//span[contains(text(),'Continue')]","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_VALIDATE","//span[contains(text(),'Validate')]","TGTYPESCREENREG");

        ASSERT.$("ELE_MSG_VALIDATIONSUCCESS","//div[@id='workbenchForm:workbenchTabs:messages']//span[@class='ui-messages-info-summary'][contains(text(),'Validation completed successfully with no errors.')]","VISIBLE","TGTYPESCREENREG");

		takeFullScreenShot();
                 LOGGER.info("Executed Step = TAKEFULLSCREENSHOT,TGTYPESCREENREG");
        TAP.$("ELE_BUTTON_SAVE","//span[contains(text(),'Save')]","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        WAIT.$("ELE_BUTTON_SHOWLINKS","//span[@class='ui-button-icon-left ui-icon ui-c fa fa-ellipsis-v fa-2x']","VISIBLE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SHOWLINKS","//span[@class='ui-button-icon-left ui-icon ui-c fa fa-ellipsis-v fa-2x']","TGTYPESCREENREG");

        TAP.$("ELE_LINK_CLAIMDETAIL","//span[contains(text(),'Claim Detail')]","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        WAIT.$("ELE_BUTTON_SHOWLINKS","//span[@class='ui-button-icon-left ui-icon ui-c fa fa-ellipsis-v fa-2x']","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SHOWLINKS","//span[@class='ui-button-icon-left ui-icon ui-c fa fa-ellipsis-v fa-2x']","TGTYPESCREENREG");

        TAP.$("ELE_LINK_ALLCLAIMPROCEEDS","//span[contains(text(),'All Claim Proceeds')]","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

        TAP.$("ELE_TAB_DISPLAYCLAIMACCD","//a[contains(text(),'Display Claim')]","TGTYPESCREENREG");

        WAIT.$("ELE_BUTTON_SHOWLINKS","//span[@class='ui-button-icon-left ui-icon ui-c fa fa-ellipsis-v fa-2x']","VISIBLE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SHOWLINKS","//span[@class='ui-button-icon-left ui-icon ui-c fa fa-ellipsis-v fa-2x']","TGTYPESCREENREG");

        TAP.$("ELE_LINK_CLIENT","//span[contains(text(),'Client')]","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        WAIT.$("ELE_TAB_DISPLAYCLAIMACCD","//a[contains(text(),'Display Claim')]","VISIBLE","TGTYPESCREENREG");

        TAP.$("ELE_TAB_DISPLAYCLAIMACCD","//a[contains(text(),'Display Claim')]","TGTYPESCREENREG");

        WAIT.$(3,"TGTYPESCREENREG");

        WAIT.$("ELE_BUTTON_SHOWLINKS","//span[@class='ui-button-icon-left ui-icon ui-c fa fa-ellipsis-v fa-2x']","VISIBLE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SHOWLINKS","//span[@class='ui-button-icon-left ui-icon ui-c fa fa-ellipsis-v fa-2x']","TGTYPESCREENREG");

        WAIT.$(3,"TGTYPESCREENREG");

        TAP.$("ELE_LINK_PAYMENTS","//span[contains(text(),'Payments')]","TGTYPESCREENREG");

        WAIT.$(3,"TGTYPESCREENREG");

        WAIT.$("ELE_BUTTON_ADDSUSPENCETASK","//span[contains(text(),'Add')]","VISIBLE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_ADDSUSPENCETASK","//span[contains(text(),'Add')]","TGTYPESCREENREG");

        WAIT.$("ELE_LABEL_CLAIMTYPE","//span[@id='workbenchForm:workbenchTabs:claimType']","VISIBLE","TGTYPESCREENREG");

        ASSERT.$("ELE_LABEL_CLAIMTYPE","//span[@id='workbenchForm:workbenchTabs:claimType']","=","Death","TGTYPESCREENREG");

        SWIPE.$("DOWN","ELE_TXTBOX_BENEFICIARYFIRSTNAME2","//input[@id='workbenchForm:workbenchTabs:firstName']","TGTYPESCREENREG");

        SWIPE.$("DOWN","ELE_TXTBOX_BENEFICIARYFIRSTNAME2","//input[@id='workbenchForm:workbenchTabs:firstName']","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_BENEFICIARYFIRSTNAME2","//input[@id='workbenchForm:workbenchTabs:firstName']",var_FirstName,"FALSE","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_BENEFICIARYLASTNAME2","//input[@id='workbenchForm:workbenchTabs:lastName']",var_LastName,"FALSE","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_CITY","//input[@id='workbenchForm:workbenchTabs:addressCity']","GREENVILLE","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_DRPDWN_ISSUESTATECOUNTRY","//label[@id='workbenchForm:workbenchTabs:countryAndStateCode_label']","TGTYPESCREENREG");

        TAP.$("ELE_SELECTSTATE_SOUTHCAROLINA","//li[@id='workbenchForm:workbenchTabs:countryAndStateCode_49']","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_ADDRESSLINE1","//input[@id='workbenchForm:workbenchTabs:addressLineOne']","DO NOT MAIL","FALSE","TGTYPESCREENREG");

        SWIPE.$("DOWN","TGTYPESCREENREG");

        SWIPE.$("DOWN","TGTYPESCREENREG");

        TAP.$("ELE_DRPDWN_INTERESTRATESTATE","//label[@id='workbenchForm:workbenchTabs:countryAndStateCodeForInterestRateLookup_label']","TGTYPESCREENREG");

        TAP.$("ELE_SELECT_INTERESTSATESC","//li[@id='workbenchForm:workbenchTabs:countryAndStateCodeForInterestRateLookup_49']","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_VALIDATE","//span[contains(text(),'Validate')]","TGTYPESCREENREG");

        ASSERT.$("ELE_MSG_VALIDATIONSUCCESS","//div[@id='workbenchForm:workbenchTabs:messages']//span[@class='ui-messages-info-summary'][contains(text(),'Validation completed successfully with no errors.')]","VISIBLE","TGTYPESCREENREG");

		takeFullScreenShot();
                 LOGGER.info("Executed Step = TAKEFULLSCREENSHOT,TGTYPESCREENREG");
        WAIT.$(3,"TGTYPESCREENREG");

        WAIT.$("ELE_BUTTON_SAVE","//span[contains(text(),'Save')]","VISIBLE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SAVE","//span[contains(text(),'Save')]","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

		takeFullScreenShot();
                 LOGGER.info("Executed Step = TAKEFULLSCREENSHOT,TGTYPESCREENREG");
        TAP.$("ELE_TAB_MYDASHBOARD","//a[contains(text(),'My Dashboard')]","TGTYPESCREENREG");

        WAIT.$("ELE_BUTTON_TASKSEARCH","//a[@id='workbenchForm:workbenchTabs:taskSearchContainer:tb_buttonSearch']//span[@class='fa fa-search fa-lg']","VISIBLE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_TASKSEARCH","//a[@id='workbenchForm:workbenchTabs:taskSearchContainer:tb_buttonSearch']//span[@class='fa fa-search fa-lg']","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_SEARCHTASKS","//input[@id='workbenchForm:workbenchTabs:taskSearchContainer:taskSearchSearch']","Policy Inquiry","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_TASKSEARCH","//a[@id='workbenchForm:workbenchTabs:taskSearchContainer:tb_buttonSearch']//span[@class='fa fa-search fa-lg']","TGTYPESCREENREG");

        TAP.$("ELE_LINK_POLICYINQUIRY","//span[contains(text(),'Policy Inquiry')]","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_POLICYNUMBER","//input[@id='workbenchForm:workbenchTabs:policyNumber']",var_PolicyNumber,"FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_CONTINUE","//span[contains(text(),'Continue')]","TGTYPESCREENREG");

        WAIT.$("ELE_GETVAL_POLICYSTATUS","//span[@id='workbenchForm:workbenchTabs:statusText']","VISIBLE","TGTYPESCREENREG");

        SWIPE.$("UP","TGTYPESCREENREG");

		takeFullScreenShot();
                 LOGGER.info("Executed Step = TAKEFULLSCREENSHOT,TGTYPESCREENREG");
        TAP.$("ELE_TAB_MYDASHBOARD","//a[contains(text(),'My Dashboard')]","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SESSIONDATE","//a[@id='headerForm:sessionDatelink']","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_NEWSESSIONDATE","//input[@id='sessionDateForm:sessionDateInput_input']",var_SessionDate1,"FALSE","TGTYPESCREENREG");

        TAP.$("ELE_CHANGEDATE_BUTTON","//span[contains(text(),'Change Date')]","TGTYPESCREENREG");

        TAP.$("ELE_TAB_CLAIM","//a[contains(text(),'Claims')]","TGTYPESCREENREG");

        WAIT.$("ELE_TXTBOX_CLAIMINSUREDNAME","//input[@id='workbenchForm:workbenchTabs:grid:individualName_Col:filter']","VISIBLE","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_CLAIMINSUREDNAME","//input[@id='workbenchForm:workbenchTabs:grid:individualName_Col:filter']",var_LastName,"FALSE","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        WAIT.$("ELE_SELECT_LINKBASEBENEFIT","//tr[@class='ui-widget-content ui-datatable-even ui-datatable-selectable']","VISIBLE","TGTYPESCREENREG");

        TAP.$("ELE_SELECT_LINKBASEBENEFIT","//tr[@class='ui-widget-content ui-datatable-even ui-datatable-selectable']","TGTYPESCREENREG");

        WAIT.$("ELE_BUTTON_EDITCLIENT","//span[@class='ui-button-text ui-c'][contains(text(),'Edit')]","VISIBLE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_EDITCLIENT","//span[@class='ui-button-text ui-c'][contains(text(),'Edit')]","TGTYPESCREENREG");

        TAP.$("ELE_RDOBUT_CLAIMSTATUSAPPROVED","//table[@id='workbenchForm:workbenchTabs:claimStatusCode']//label[contains(text(),'Approved')]","TGTYPESCREENREG");

        TAP.$("ELE_RDOBTN_ISSUEAGEVERIFIED","//table[@id='workbenchForm:workbenchTabs:wasIssueAgeVerified']//label[contains(text(),'Yes')]","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_DATECLAIMAPPROVED","//input[@id='workbenchForm:workbenchTabs:dateClaimApprDate_input']",var_SessionDate1,"FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SAVE","//span[contains(text(),'Save')]","TGTYPESCREENREG");

		takeFullScreenShot();
                 LOGGER.info("Executed Step = TAKEFULLSCREENSHOT,TGTYPESCREENREG");
        WAIT.$(3,"TGTYPESCREENREG");

        ASSERT.$("ELE_GETVAL_CLAIMSTATUS","//span[@id='workbenchForm:workbenchTabs:claimStatusCode']","=","Approved","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SHOWLINKS","//span[@class='ui-button-icon-left ui-icon ui-c fa fa-ellipsis-v fa-2x']","TGTYPESCREENREG");

        WAIT.$(4,"TGTYPESCREENREG");

        WAIT.$("ELE_LINK_PAYMENTS","//span[contains(text(),'Payments')]","VISIBLE","TGTYPESCREENREG");

        TAP.$("ELE_LINK_PAYMENTS","//span[contains(text(),'Payments')]","TGTYPESCREENREG");

		try { 
		         List<WebElement> col = driver.findElements(By.xpath("//tbody[@id='workbenchForm:workbenchTabs:grid_data']//tr"));

		        System.out.println("No of cols are : " +col.size());


		        for(int i= 0; i<col.size();i++) {

		            String a = Integer.toString(i);

		            String XpathFordateRow ="//span[@id='workbenchForm:workbenchTabs:grid:R:individualName']";
		            String Batchnumber = XpathFordateRow.replace("R", a);
		            WebElement batch = driver.findElement(By.xpath(Batchnumber));
		            String name = batch.getText();
		            if(name.contains(var_LastName)){
		                batch.click();
		                takeFullScreenShot();
		                i= col.size()+1;
		            }

		        }
		 
		 ;} 
		 catch(Exception e) { PrintVal("Renderer Script Exception : "+e.toString()); } 
		 PrintVal("Executed Step = CALLRENDERERSCRIPT,CLAIMPAYMENT_CLIENTSELECTLASTNAME"); 
        WAIT.$("ELE_BUTTON_EDITCLIENT","//span[@class='ui-button-text ui-c'][contains(text(),'Edit')]","VISIBLE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_EDITCLIENT","//span[@class='ui-button-text ui-c'][contains(text(),'Edit')]","TGTYPESCREENREG");

        WAIT.$(3,"TGTYPESCREENREG");

		String var_PaymentAmount;
                 LOGGER.info("Executed Step = VAR,String,var_PaymentAmount,TGTYPESCREENREG");
		var_PaymentAmount = ActionWrapper.getElementValue("ELE_UNPAIDDISBURSEMENTAMOUNT", "//span[@id='workbenchForm:workbenchTabs:unpaidClaimAmount']");
                 LOGGER.info("Executed Step = STORE,var_PaymentAmount,ELE_UNPAIDDISBURSEMENTAMOUNT,TGTYPESCREENREG");
        TAP.$("ELE_RDOBTN_APPROVEDCLAIMPAYMENT","//table[@id='workbenchForm:workbenchTabs:beneficiaryPmtStat']//label[contains(text(),'Approved')]","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_PERCENTAGEOFDISBURSEMENTAMOUNT","//input[@id='workbenchForm:workbenchTabs:sharePercentage_input']","100","FALSE","TGTYPESCREENREG");

        SWIPE.$("DOWN","TGTYPESCREENREG");

        SWIPE.$("DOWN","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_TOTALPAYMENTAMOUNT","//input[@id='workbenchForm:workbenchTabs:totalClaimAmountAppr3_input']",var_PaymentAmount,"FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SAVE","//span[contains(text(),'Save')]","TGTYPESCREENREG");

        ASSERT.$("ELE_MSG_ITEMSUCCESSUPDATE","//div[@id='workbenchForm:workbenchTabs:messages']//span[@class='ui-messages-info-summary'][contains(text(),'Item successfully updated.')]","VISIBLE","TGTYPESCREENREG");

        WAIT.$(3,"TGTYPESCREENREG");

        WAIT.$("ELE_LINK_PAYCLAIM","//span[contains(text(),'Pay Claim')]","VISIBLE","TGTYPESCREENREG");

        TAP.$("ELE_LINK_PAYCLAIM","//span[contains(text(),'Pay Claim')]","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_COMMENTSPAYCLAIMPAYMENT","//textarea[@id='workbenchForm:workbenchTabs:checkCommentsText']","Death claim proceeds","FALSE","TGTYPESCREENREG");

        SWIPE.$("DOWN","TGTYPESCREENREG");

        SWIPE.$("DOWN","TGTYPESCREENREG");

        TAP.$("ELE_DRPDWN_IDTYPE","//label[@id='workbenchForm:workbenchTabs:taxIdenUsag_label']","TGTYPESCREENREG");

        TAP.$("ELE_IDTYPESELECT_SSN","//li[@id='workbenchForm:workbenchTabs:taxIdenUsag_2']","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_IDNO","//input[@id='workbenchForm:workbenchTabs:identificationNumber']",var_SSN,"FALSE","TGTYPESCREENREG");

		takeFullScreenShot();
                 LOGGER.info("Executed Step = TAKEFULLSCREENSHOT,TGTYPESCREENREG");
        WAIT.$(4,"TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_VALIDATE","//span[contains(text(),'Validate')]","TGTYPESCREENREG");

        WAIT.$(4,"TGTYPESCREENREG");

        WAIT.$("ELE_BUTTON_SAVE","//span[contains(text(),'Save')]","VISIBLE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SAVE","//span[contains(text(),'Save')]","TGTYPESCREENREG");

        WAIT.$(3,"TGTYPESCREENREG");

		takeFullScreenShot();
                 LOGGER.info("Executed Step = TAKEFULLSCREENSHOT,TGTYPESCREENREG");
        ASSERT.$("ELE_MSG_ITEMSUCCESSUPDATE","//div[@id='workbenchForm:workbenchTabs:messages']//span[@class='ui-messages-info-summary'][contains(text(),'Item successfully updated.')]","VISIBLE","TGTYPESCREENREG");

        TAP.$("ELE_TAB_CLAIMPAYMENT","//li[@class='ui-state-default ui-corner-top']//a[contains(text(),'Claim Payment')]","TGTYPESCREENREG");

        WAIT.$(3,"TGTYPESCREENREG");

        TAP.$("ELE_LOGO_GIAS1","//span[contains(text(),'GIAS')]","TGTYPESCREENREG");

        WAIT.$(4,"TGTYPESCREENREG");

        CALL.$("PostPolicyDeathClaimWholeLife","TGTYPESCREENREG");

        TAP.$("ELE_TXTBOX_SEARCHTASKS","//input[@id='workbenchForm:workbenchTabs:taskSearchContainer:taskSearchSearch']","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_SEARCHTASKS","//input[@id='workbenchForm:workbenchTabs:taskSearchContainer:taskSearchSearch']","Policy Inquiry","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_TASKSEARCH","//a[@id='workbenchForm:workbenchTabs:taskSearchContainer:tb_buttonSearch']//span[@class='fa fa-search fa-lg']","TGTYPESCREENREG");

        TAP.$("ELE_LINK_POLICYINQUIRY","//span[contains(text(),'Policy Inquiry')]","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_POLICYNUMBER","//input[@id='workbenchForm:workbenchTabs:policyNumber']",var_PolicyNumber,"FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_CONTINUE","//span[contains(text(),'Continue')]","TGTYPESCREENREG");

        WAIT.$("ELE_GETVAL_POLICYSTATUS","//span[@id='workbenchForm:workbenchTabs:statusText']","VISIBLE","TGTYPESCREENREG");

        ASSERT.$("ELE_GETVAL_POLICYSTATUS","//span[@id='workbenchForm:workbenchTabs:statusText']","=","Death Claim","TGTYPESCREENREG");

        ASSERT.$("ELE_LABEL_SUSPENDEDREASON","//span[@id='workbenchForm:workbenchTabs:suspendText']","=","No","TGTYPESCREENREG");

		takeFullScreenShot();
                 LOGGER.info("Executed Step = TAKEFULLSCREENSHOT,TGTYPESCREENREG");
        TAP.$("ELE_BUTTON_SHOWLINKS","//span[@class='ui-button-icon-left ui-icon ui-c fa fa-ellipsis-v fa-2x']","TGTYPESCREENREG");

        TAP.$("ELE_LINK_BASEBENEFIT","//span[contains(text(),'Base Benefit')]","TGTYPESCREENREG");

		takeFullScreenShot();
                 LOGGER.info("Executed Step = TAKEFULLSCREENSHOT,TGTYPESCREENREG");
        WAIT.$(2,"TGTYPESCREENREG");

        TAP.$("ELE_TAB_POLICYINQUIRY","//a[contains(text(),'Policy')]","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_CONTINUE","//span[contains(text(),'Continue')]","TGTYPESCREENREG");

        WAIT.$("ELE_BUTTON_SHOWLINKS","//span[@class='ui-button-icon-left ui-icon ui-c fa fa-ellipsis-v fa-2x']","VISIBLE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SHOWLINKS","//span[@class='ui-button-icon-left ui-icon ui-c fa fa-ellipsis-v fa-2x']","TGTYPESCREENREG");

        WAIT.$("ELE_LINK_POLICY_VALUE","//span[contains(text(),'Policy Values')]","TGTYPESCREENREG");

        TAP.$("ELE_LINK_POLICY_VALUE","//span[contains(text(),'Policy Values')]","TGTYPESCREENREG");

		takeFullScreenShot();
                 LOGGER.info("Executed Step = TAKEFULLSCREENSHOT,TGTYPESCREENREG");
        TAP.$("ELE_TAB_POLICYINQUIRY","//a[contains(text(),'Policy')]","TGTYPESCREENREG");

        TAP.$("ELE_TAB_POLICYINQUIRY","//a[contains(text(),'Policy')]","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_CONTINUE","//span[contains(text(),'Continue')]","TGTYPESCREENREG");

        WAIT.$("ELE_BUTTON_SHOWLINKS","//span[@class='ui-button-icon-left ui-icon ui-c fa fa-ellipsis-v fa-2x']","VISIBLE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SHOWLINKS","//span[@class='ui-button-icon-left ui-icon ui-c fa fa-ellipsis-v fa-2x']","TGTYPESCREENREG");

        WAIT.$("ELE_BUTTON_CLIENTTRANSACTIONHISTORY","//span[contains(text(),'Transaction History')]","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_CLIENTTRANSACTIONHISTORY","//span[contains(text(),'Transaction History')]","TGTYPESCREENREG");

        WAIT.$(4,"TGTYPESCREENREG");

		takeFullScreenShot();
                 LOGGER.info("Executed Step = TAKEFULLSCREENSHOT,TGTYPESCREENREG");
        TAP.$("ELE_LOGO_GIAS1","//span[contains(text(),'GIAS')]","TGTYPESCREENREG");

        WAIT.$(3,"TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_TASKSEARCH","//a[@id='workbenchForm:workbenchTabs:taskSearchContainer:tb_buttonSearch']//span[@class='fa fa-search fa-lg']","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_SEARCHTASKS","//input[@id='workbenchForm:workbenchTabs:taskSearchContainer:taskSearchSearch']","ledger extract","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_TASKSEARCH","//a[@id='workbenchForm:workbenchTabs:taskSearchContainer:tb_buttonSearch']//span[@class='fa fa-search fa-lg']","TGTYPESCREENREG");

        TAP.$("ELE_LINK_LEDGEREXTRACTENTRIES","//span[contains(text(),'Ledger Extract Entries')]","TGTYPESCREENREG");

        WAIT.$("ELE_LEDGEREXTRACT_REFERENCE2","//input[@id='workbenchForm:workbenchTabs:grid:referenceNumber2_Col:filter']","VISIBLE","TGTYPESCREENREG");

        TYPE.$("ELE_LEDGEREXTRACT_REFERENCE2","//input[@id='workbenchForm:workbenchTabs:grid:referenceNumber2_Col:filter']",var_LablePolicyNumber,"FALSE","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

        TAP.$("ELE_LOGO_GIAS1","//span[contains(text(),'GIAS')]","TGTYPESCREENREG");


    }


    @Test
    public void mm40regdeathclaimuniversallife() {

        // No need to declare / print method name here, handled by TestNg listener
        // declare("mm40regdeathclaimuniversallife");

        CALL.$("LoginToGIAS4","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENNO");

        TYPE.$("ELE_TXTBOX_USERNAME","//input[@id='guestForm:username']","Mano","FALSE","TGTYPESCREENNO");

        TYPE.$("ELE_TXTBOX_PASSWORD","//input[@id='guestForm:password']","Cnx12345!","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_LOGIN","//button[@id='guestForm:loginButton']","TGTYPESCREENNO");

        CALL.$("PrePolicyDeathClaimUniversalLife","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_TASKSEARCH","//a[@id='workbenchForm:workbenchTabs:taskSearchContainer:tb_buttonSearch']//span[@class='fa fa-search fa-lg']","TGTYPESCREENREG");

		int var_Count = 0;
                 LOGGER.info("Executed Step = VAR,Integer,var_Count,0,TGTYPESCREENREG");
		com.jayway.jsonpath.ReadContext var_CSVData = getJsonData("http://10.1.104.229/json/20200916/ECx1U8.json");
                 LOGGER.info("Executed Step = VAR,Json,var_CSVData,http://10.1.104.229/json/20200916/ECx1U8.json,TGTYPESCREENREG");
		String var_PolicyNumber;
                 LOGGER.info("Executed Step = VAR,String,var_PolicyNumber,TGTYPESCREENREG");
		var_PolicyNumber = getJsonData(var_CSVData , "$.records["+var_Count+"].PolicyNumber");
                 LOGGER.info("Executed Step = STORE,var_PolicyNumber,var_CSVData,$.records[{var_Count}].PolicyNumber,TGTYPESCREENREG");
		String var_SessionDate;
                 LOGGER.info("Executed Step = VAR,String,var_SessionDate,TGTYPESCREENREG");
		var_SessionDate = getJsonData(var_CSVData , "$.records.["+var_Count+"].SessionDate");
                 LOGGER.info("Executed Step = STORE,var_SessionDate,var_CSVData,$.records.[{var_Count}].SessionDate,TGTYPESCREENREG");
		String var_SessionDate1;
                 LOGGER.info("Executed Step = VAR,String,var_SessionDate1,TGTYPESCREENREG");
		var_SessionDate1 = getJsonData(var_CSVData , "$.records.["+var_Count+"].SessionDate1");
                 LOGGER.info("Executed Step = STORE,var_SessionDate1,var_CSVData,$.records.[{var_Count}].SessionDate1,TGTYPESCREENREG");
		String var_FirstName;
                 LOGGER.info("Executed Step = VAR,String,var_FirstName,TGTYPESCREENREG");
		var_FirstName = getJsonData(var_CSVData , "$.records.["+var_Count+"].FirstName");
                 LOGGER.info("Executed Step = STORE,var_FirstName,var_CSVData,$.records.[{var_Count}].FirstName,TGTYPESCREENREG");
		String var_LastName;
                 LOGGER.info("Executed Step = VAR,String,var_LastName,TGTYPESCREENREG");
		var_LastName = getJsonData(var_CSVData , "$.records.["+var_Count+"].LastName");
                 LOGGER.info("Executed Step = STORE,var_LastName,var_CSVData,$.records.[{var_Count}].LastName,TGTYPESCREENREG");
		String var_EffectiveDate;
                 LOGGER.info("Executed Step = VAR,String,var_EffectiveDate,TGTYPESCREENREG");
		var_EffectiveDate = getJsonData(var_CSVData , "$.records.["+var_Count+"].EffectiveDate");
                 LOGGER.info("Executed Step = STORE,var_EffectiveDate,var_CSVData,$.records.[{var_Count}].EffectiveDate,TGTYPESCREENREG");
		String var_SSN;
                 LOGGER.info("Executed Step = VAR,String,var_SSN,TGTYPESCREENREG");
		var_SSN = getJsonData(var_CSVData , "$.records.["+var_Count+"].SSN");
                 LOGGER.info("Executed Step = STORE,var_SSN,var_CSVData,$.records.[{var_Count}].SSN,TGTYPESCREENREG");
        TAP.$("ELE_BUTTON_SESSIONDATE","//a[@id='headerForm:sessionDatelink']","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_NEWSESSIONDATE","//input[@id='sessionDateForm:sessionDateInput_input']",var_SessionDate,"FALSE","TGTYPESCREENREG");

        TAP.$("ELE_CHANGEDATE_BUTTON","//span[contains(text(),'Change Date')]","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_SEARCHTASKS","//input[@id='workbenchForm:workbenchTabs:taskSearchContainer:taskSearchSearch']","Policy Inquiry","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_TASKSEARCH","//a[@id='workbenchForm:workbenchTabs:taskSearchContainer:tb_buttonSearch']//span[@class='fa fa-search fa-lg']","TGTYPESCREENREG");

        TAP.$("ELE_LINK_POLICYINQUIRY","//span[contains(text(),'Policy Inquiry')]","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_POLICYNUMBER","//input[@id='workbenchForm:workbenchTabs:policyNumber']",var_PolicyNumber,"FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_CONTINUE","//span[contains(text(),'Continue')]","TGTYPESCREENREG");

        WAIT.$("ELE_GETVAL_POLICYSTATUS","//span[@id='workbenchForm:workbenchTabs:statusText']","VISIBLE","TGTYPESCREENREG");

        ASSERT.$("ELE_GETVAL_POLICYSTATUS","//span[@id='workbenchForm:workbenchTabs:statusText']","VISIBLE","TGTYPESCREENREG");

		String var_LablePolicyNumber;
                 LOGGER.info("Executed Step = VAR,String,var_LablePolicyNumber,TGTYPESCREENREG");
		var_LablePolicyNumber = ActionWrapper.getElementValue("ELE_LABEL_POLICYNUMBER", "//span[@id='workbenchForm:workbenchTabs:policyNumber']");
                 LOGGER.info("Executed Step = STORE,var_LablePolicyNumber,ELE_LABEL_POLICYNUMBER,TGTYPESCREENREG");
		String var_ClientsName;
                 LOGGER.info("Executed Step = VAR,String,var_ClientsName,TGTYPESCREENREG");
		var_ClientsName = ActionWrapper.getElementValue("ELE_GETVAL_CLIENTNAME", "//span[@id='workbenchForm:workbenchTabs:ownerName']");
                 LOGGER.info("Executed Step = STORE,var_ClientsName,ELE_GETVAL_CLIENTNAME,TGTYPESCREENREG");
		takeFullScreenShot();
                 LOGGER.info("Executed Step = TAKEFULLSCREENSHOT,TGTYPESCREENREG");
        TAP.$("ELE_LINK_GIASHOME","//button[@id='headerForm:j_idt11']//span[@class='ui-button-text ui-c'][contains(text(),'GIAS')]","TGTYPESCREENREG");

        CALL.$("ProcessingDeathClaimForUniversalLife","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_TASKSEARCH","//a[@id='workbenchForm:workbenchTabs:taskSearchContainer:tb_buttonSearch']//span[@class='fa fa-search fa-lg']","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_SEARCHTASKS","//input[@id='workbenchForm:workbenchTabs:taskSearchContainer:taskSearchSearch']","Policy death","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_TASKSEARCH","//a[@id='workbenchForm:workbenchTabs:taskSearchContainer:tb_buttonSearch']//span[@class='fa fa-search fa-lg']","TGTYPESCREENREG");

        TAP.$("ELE_LINK_POLICYDEATHANDLIVINGBENEFITCLAIMS","//span[contains(text(),'Policy Death and Living Benefit Claims')]","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

        TAP.$("ELE_LINK_ADDDEATHCLAIM","//span[contains(text(),'Add Death Claim')]","TGTYPESCREENREG");

        WAIT.$(3,"TGTYPESCREENREG");

        TYPE.$("ELE_DATE_NOTIFICATONDATE","//input[@id='workbenchForm:workbenchTabs:notiOfDthDate_input']",var_EffectiveDate,"FALSE","TGTYPESCREENREG");

        TYPE.$("ELE_DATE_DATEOFDEATH","//input[@id='workbenchForm:workbenchTabs:dateOfDeathDate_input']",var_SessionDate1,"FALSE","TGTYPESCREENREG");

        TYPE.$("ELE_DATE_DEATHCERTRCVDDATE","//input[@id='workbenchForm:workbenchTabs:deathCertRcvdDate_input']",var_SessionDate,"FALSE","TGTYPESCREENREG");

        TAP.$("ELE_REASONFORCLAIM_ACCDEATHBENEFIT","//label[@id='workbenchForm:workbenchTabs:transactionRsnCode_label']","TGTYPESCREENREG");

        TAP.$("ELE_DROPDWNSELECT_COVID19","//li[@id='workbenchForm:workbenchTabs:transactionRsnCode_4']","TGTYPESCREENREG");

        TAP.$("ELE_RDOBTN_ISSUEAGEVERIFIED","//table[@id='workbenchForm:workbenchTabs:wasIssueAgeVerified']//label[contains(text(),'Yes')]","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SHOWLINKS","//span[@class='ui-button-icon-left ui-icon ui-c fa fa-ellipsis-v fa-2x']","TGTYPESCREENREG");

        TAP.$("ELE_LINK_SELECTCLIENT","//span[contains(text(),'Select Client')]","TGTYPESCREENREG");

        TAP.$("ELE_TXTBOX_SEARCH","//input[@id='workbenchForm:workbenchTabs:searchName']","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_SEARCH","//input[@id='workbenchForm:workbenchTabs:searchName']",var_ClientsName,"FALSE","TGTYPESCREENREG");

        WAIT.$("ELE_BUTTON_SEARCH","//span[contains(text(),'Search')]","VISIBLE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SEARCH","//span[contains(text(),'Search')]","TGTYPESCREENREG");

        WAIT.$(3,"TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SELECTCLIENT","//button[@id='workbenchForm:workbenchTabs:button_select']","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_CONTINUE","//span[contains(text(),'Continue')]","TGTYPESCREENREG");

        WAIT.$("ELE_BUTTON_VALIDATE","//span[contains(text(),'Validate')]","VISIBLE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_VALIDATE","//span[contains(text(),'Validate')]","TGTYPESCREENREG");

        ASSERT.$("ELE_MSG_VALIDATIONSUCCESS","//div[@id='workbenchForm:workbenchTabs:messages']//span[@class='ui-messages-info-summary'][contains(text(),'Validation completed successfully with no errors.')]","VISIBLE","TGTYPESCREENREG");

		takeFullScreenShot();
                 LOGGER.info("Executed Step = TAKEFULLSCREENSHOT,TGTYPESCREENREG");
        TAP.$("ELE_BUTTON_SAVE","//span[contains(text(),'Save')]","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        WAIT.$("ELE_LABEL_CLAIMTYPE","//span[@id='workbenchForm:workbenchTabs:claimType']","VISIBLE","TGTYPESCREENREG");

        ASSERT.$("ELE_LABEL_CLAIMTYPE","//span[@id='workbenchForm:workbenchTabs:claimType']","=","Death","TGTYPESCREENREG");

        ASSERT.$("ELE_LABEL_CLAIMSTATUSPENDING","//span[@id='workbenchForm:workbenchTabs:claimStatusCode']","=","Pending","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_EDITCLIENT","//span[@class='ui-button-text ui-c'][contains(text(),'Edit')]","TGTYPESCREENREG");

        TAP.$("ELE_RDOBUT_CLAIMSTATUSAPPROVED","//table[@id='workbenchForm:workbenchTabs:claimStatusCode']//label[contains(text(),'Approved')]","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_DATECLAIMAPPROVED","//input[@id='workbenchForm:workbenchTabs:dateClaimApprDate_input']",var_SessionDate,"FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SAVE","//span[contains(text(),'Save')]","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        ASSERT.$("ELE_MSG_ITEMSUCCESSUPDATE","//div[@id='workbenchForm:workbenchTabs:messages']//span[@class='ui-messages-info-summary'][contains(text(),'Item successfully updated.')]","VISIBLE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SHOWLINKS","//span[@class='ui-button-icon-left ui-icon ui-c fa fa-ellipsis-v fa-2x']","TGTYPESCREENREG");

        TAP.$("ELE_LINK_PAYMENTS","//span[contains(text(),'Payments')]","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_ADD","//button[@id='workbenchForm:workbenchTabs:tb_buttonAdd']//span[@class='ui-button-text ui-c'][contains(text(),'Add')]","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        WAIT.$("ELE_RDOBUT_CLAIMSTATUSAPPROVED","//table[@id='workbenchForm:workbenchTabs:claimStatusCode']//label[contains(text(),'Approved')]","VISIBLE","TGTYPESCREENREG");

        TAP.$("ELE_RDOBTN_APPROVEDCLAIMPAYMENT","//table[@id='workbenchForm:workbenchTabs:beneficiaryPmtStat']//label[contains(text(),'Approved')]","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_PERCENTAGEOFDISBURSEMENTAMOUNT","//input[@id='workbenchForm:workbenchTabs:sharePercentage_input']","100","FALSE","TGTYPESCREENREG");

        SWIPE.$("DOWN","TGTYPESCREENREG");

        SWIPE.$("DOWN","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_BENEFICIARYFIRSTNAME2","//input[@id='workbenchForm:workbenchTabs:firstName']",var_FirstName,"FALSE","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_BENEFICIARYLASTNAME2","//input[@id='workbenchForm:workbenchTabs:lastName']",var_LastName,"FALSE","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_CITY","//input[@id='workbenchForm:workbenchTabs:addressCity']","GREENVILLE","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_DRPDWN_ISSUESTATECOUNTRY","//label[@id='workbenchForm:workbenchTabs:countryAndStateCode_label']","TGTYPESCREENREG");

        TAP.$("ELE_SELECTSTATE_SOUTHCAROLINA","//li[@id='workbenchForm:workbenchTabs:countryAndStateCode_49']","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_ZIPCODE","//input[@id='workbenchForm:workbenchTabs:zipCode']","29662","FALSE","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_ADDRESSLINE1","//input[@id='workbenchForm:workbenchTabs:addressLineOne']","DO NOT MAIL","FALSE","TGTYPESCREENREG");

        SWIPE.$("DOWN","TGTYPESCREENREG");

        SWIPE.$("DOWN","TGTYPESCREENREG");

        TAP.$("ELE_DRPDWN_INTERESTRATESTATE","//label[@id='workbenchForm:workbenchTabs:countryAndStateCodeForInterestRateLookup_label']","TGTYPESCREENREG");

        TAP.$("ELE_SELECT_INTERESTSATESC","//li[@id='workbenchForm:workbenchTabs:countryAndStateCodeForInterestRateLookup_49']","TGTYPESCREENREG");

		takeFullScreenShot();
                 LOGGER.info("Executed Step = TAKEFULLSCREENSHOT,TGTYPESCREENREG");
        WAIT.$(3,"TGTYPESCREENREG");

        WAIT.$("ELE_BUTTON_SAVE","//span[contains(text(),'Save')]","VISIBLE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SAVE","//span[contains(text(),'Save')]","TGTYPESCREENREG");

        WAIT.$(3,"TGTYPESCREENREG");

        TAP.$("ELE_TAB_DISPLAYCLAIMACCD","//a[contains(text(),'Display Claim')]","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SHOWLINKS","//span[@class='ui-button-icon-left ui-icon ui-c fa fa-ellipsis-v fa-2x']","TGTYPESCREENREG");

        TAP.$("ELE_LINK_PAYMENTS","//span[contains(text(),'Payments')]","TGTYPESCREENREG");

		try { 
		         List<WebElement> col = driver.findElements(By.xpath("//tbody[@id='workbenchForm:workbenchTabs:grid_data']//tr"));

		        System.out.println("No of cols are : " +col.size());


		        for(int i= 0; i<col.size();i++) {

		            String a = Integer.toString(i);

		            String XpathFordateRow ="//span[@id='workbenchForm:workbenchTabs:grid:R:individualName']";
		            String Batchnumber = XpathFordateRow.replace("R", a);
		            WebElement batch = driver.findElement(By.xpath(Batchnumber));
		            String name = batch.getText();
		            if(name.contains(var_LastName)){
		                batch.click();
		                takeFullScreenShot();
		                i= col.size()+1;
		            }

		        }
		 
		 ;} 
		 catch(Exception e) { PrintVal("Renderer Script Exception : "+e.toString()); } 
		 PrintVal("Executed Step = CALLRENDERERSCRIPT,CLAIMPAYMENT_CLIENTSELECTLASTNAME"); 
        WAIT.$("ELE_LINK_PAYCLAIM","//span[contains(text(),'Pay Claim')]","VISIBLE","TGTYPESCREENREG");

        TAP.$("ELE_LINK_PAYCLAIM","//span[contains(text(),'Pay Claim')]","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_COMMENTSPAYCLAIMPAYMENT","//textarea[@id='workbenchForm:workbenchTabs:checkCommentsText']","Death claim proceeds","FALSE","TGTYPESCREENREG");

        SWIPE.$("DOWN","TGTYPESCREENREG");

        SWIPE.$("DOWN","TGTYPESCREENREG");

        TAP.$("ELE_DRPDWN_IDTYPE","//label[@id='workbenchForm:workbenchTabs:taxIdenUsag_label']","TGTYPESCREENREG");

        TAP.$("ELE_IDTYPESELECT_SSN","//li[@id='workbenchForm:workbenchTabs:taxIdenUsag_2']","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_IDNO","//input[@id='workbenchForm:workbenchTabs:identificationNumber']",var_SSN,"FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_VALIDATE","//span[contains(text(),'Validate')]","TGTYPESCREENREG");

        ASSERT.$("ELE_MSG_VALIDATIONSUCCESS","//div[@id='workbenchForm:workbenchTabs:messages']//span[@class='ui-messages-info-summary'][contains(text(),'Validation completed successfully with no errors.')]","VISIBLE","TGTYPESCREENREG");

		takeFullScreenShot();
                 LOGGER.info("Executed Step = TAKEFULLSCREENSHOT,TGTYPESCREENREG");
        WAIT.$(3,"TGTYPESCREENREG");

        WAIT.$("ELE_BUTTON_SAVE","//span[contains(text(),'Save')]","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SAVE","//span[contains(text(),'Save')]","TGTYPESCREENREG");

        WAIT.$(3,"TGTYPESCREENREG");

        ASSERT.$("ELE_MSG_ITEMSUCCESSUPDATE","//div[@id='workbenchForm:workbenchTabs:messages']//span[@class='ui-messages-info-summary'][contains(text(),'Item successfully updated.')]","VISIBLE","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

		takeFullScreenShot();
                 LOGGER.info("Executed Step = TAKEFULLSCREENSHOT,TGTYPESCREENREG");
        TAP.$("ELE_LOGO_GIAS1","//span[contains(text(),'GIAS')]","TGTYPESCREENREG");

        CALL.$("PostPolicyDeathClaimUniversalLife","TGTYPESCREENREG");

        TAP.$("ELE_TXTBOX_SEARCHTASKS","//input[@id='workbenchForm:workbenchTabs:taskSearchContainer:taskSearchSearch']","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_SEARCHTASKS","//input[@id='workbenchForm:workbenchTabs:taskSearchContainer:taskSearchSearch']","Policy Inquiry","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_TASKSEARCH","//a[@id='workbenchForm:workbenchTabs:taskSearchContainer:tb_buttonSearch']//span[@class='fa fa-search fa-lg']","TGTYPESCREENREG");

        TAP.$("ELE_LINK_POLICYINQUIRY","//span[contains(text(),'Policy Inquiry')]","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_POLICYNUMBER","//input[@id='workbenchForm:workbenchTabs:policyNumber']",var_PolicyNumber,"FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_CONTINUE","//span[contains(text(),'Continue')]","TGTYPESCREENREG");

        WAIT.$("ELE_GETVAL_POLICYSTATUS","//span[@id='workbenchForm:workbenchTabs:statusText']","VISIBLE","TGTYPESCREENREG");

        ASSERT.$("ELE_GETVAL_POLICYSTATUS","//span[@id='workbenchForm:workbenchTabs:statusText']","=","Death Claim","TGTYPESCREENREG");

        ASSERT.$("ELE_LABEL_SUSPENDEDREASON","//span[@id='workbenchForm:workbenchTabs:suspendText']","=","No","TGTYPESCREENREG");

		takeFullScreenShot();
                 LOGGER.info("Executed Step = TAKEFULLSCREENSHOT,TGTYPESCREENREG");
        TAP.$("ELE_BUTTON_SHOWLINKS","//span[@class='ui-button-icon-left ui-icon ui-c fa fa-ellipsis-v fa-2x']","TGTYPESCREENREG");

        TAP.$("ELE_LINK_BASEBENEFIT","//span[contains(text(),'Base Benefit')]","TGTYPESCREENREG");

		takeFullScreenShot();
                 LOGGER.info("Executed Step = TAKEFULLSCREENSHOT,TGTYPESCREENREG");
        TAP.$("ELE_LOGO_GIAS1","//span[contains(text(),'GIAS')]","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_TASKSEARCH","//a[@id='workbenchForm:workbenchTabs:taskSearchContainer:tb_buttonSearch']//span[@class='fa fa-search fa-lg']","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_SEARCHTASKS","//input[@id='workbenchForm:workbenchTabs:taskSearchContainer:taskSearchSearch']","Policy Inquiry","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_TASKSEARCH","//a[@id='workbenchForm:workbenchTabs:taskSearchContainer:tb_buttonSearch']//span[@class='fa fa-search fa-lg']","TGTYPESCREENREG");

        TAP.$("ELE_LINK_POLICYINQUIRY","//span[contains(text(),'Policy Inquiry')]","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_POLICYNUMBER","//input[@id='workbenchForm:workbenchTabs:policyNumber']",var_PolicyNumber,"FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_CONTINUE","//span[contains(text(),'Continue')]","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SHOWLINKS","//span[@class='ui-button-icon-left ui-icon ui-c fa fa-ellipsis-v fa-2x']","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_CLIENTTRANSACTIONHISTORY","//span[contains(text(),'Transaction History')]","TGTYPESCREENREG");

        WAIT.$(3,"TGTYPESCREENREG");

        TAP.$("ELE_LOGO_GIAS1","//span[contains(text(),'GIAS')]","TGTYPESCREENREG");

        WAIT.$(3,"TGTYPESCREENREG");

        SWIPE.$("DOWN","TGTYPESCREENREG");

        SWIPE.$("DOWN","TGTYPESCREENREG");

        WAIT.$("ELE_BUTTON_TASKSEARCH","//a[@id='workbenchForm:workbenchTabs:taskSearchContainer:tb_buttonSearch']//span[@class='fa fa-search fa-lg']","VISIBLE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_TASKSEARCH","//a[@id='workbenchForm:workbenchTabs:taskSearchContainer:tb_buttonSearch']//span[@class='fa fa-search fa-lg']","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_SEARCHTASKS","//input[@id='workbenchForm:workbenchTabs:taskSearchContainer:taskSearchSearch']","ledger extract","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_TASKSEARCH","//a[@id='workbenchForm:workbenchTabs:taskSearchContainer:tb_buttonSearch']//span[@class='fa fa-search fa-lg']","TGTYPESCREENREG");

        TAP.$("ELE_LINK_LEDGEREXTRACTENTRIES","//span[contains(text(),'Ledger Extract Entries')]","TGTYPESCREENREG");

        TYPE.$("ELE_LEDGEREXTRACT_REFERENCE2","//input[@id='workbenchForm:workbenchTabs:grid:referenceNumber2_Col:filter']",var_LablePolicyNumber,"FALSE","TGTYPESCREENREG");

        WAIT.$(3,"TGTYPESCREENREG");

        TAP.$("ELE_LOGO_GIAS1","//span[contains(text(),'GIAS')]","TGTYPESCREENREG");


    }


    @Test
    public void mm40reggroupsetup() {

        // No need to declare / print method name here, handled by TestNg listener
        // declare("mm40reggroupsetup");

        CALL.$("LoginToGIAS4","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENNO");

        TYPE.$("ELE_TXTBOX_USERNAME","//input[@id='guestForm:username']","Mano","FALSE","TGTYPESCREENNO");

        TYPE.$("ELE_TXTBOX_PASSWORD","//input[@id='guestForm:password']","Cnx12345!","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_LOGIN","//button[@id='guestForm:loginButton']","TGTYPESCREENNO");

        CALL.$("ListBillGroupSetUP","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_TASKSEARCH","//a[@id='workbenchForm:workbenchTabs:taskSearchContainer:tb_buttonSearch']//span[@class='fa fa-search fa-lg']","TGTYPESCREENREG");

		int var_Count = 0;
                 LOGGER.info("Executed Step = VAR,Integer,var_Count,0,TGTYPESCREENREG");
		com.jayway.jsonpath.ReadContext var_CSVData = getJsonData("http://10.1.104.229/json/20200916/xhfOdI.json");
                 LOGGER.info("Executed Step = VAR,Json,var_CSVData,http://10.1.104.229/json/20200916/xhfOdI.json,TGTYPESCREENREG");
		String var_ListBillNumber;
                 LOGGER.info("Executed Step = VAR,String,var_ListBillNumber,TGTYPESCREENREG");
		var_ListBillNumber = getJsonData(var_CSVData , "$.records.["+var_Count+"].ListBillNumber");
                 LOGGER.info("Executed Step = STORE,var_ListBillNumber,var_CSVData,$.records.[{var_Count}].ListBillNumber,TGTYPESCREENREG");
		String var_SessionDate;
                 LOGGER.info("Executed Step = VAR,String,var_SessionDate,TGTYPESCREENREG");
		var_SessionDate = getJsonData(var_CSVData , "$.records.["+var_Count+"].SessionDate");
                 LOGGER.info("Executed Step = STORE,var_SessionDate,var_CSVData,$.records.[{var_Count}].SessionDate,TGTYPESCREENREG");
		String var_EffectiveDate;
                 LOGGER.info("Executed Step = VAR,String,var_EffectiveDate,TGTYPESCREENREG");
		var_EffectiveDate = getJsonData(var_CSVData , "$.records.["+var_Count+"].EffectiveDate");
                 LOGGER.info("Executed Step = STORE,var_EffectiveDate,var_CSVData,$.records.[{var_Count}].EffectiveDate,TGTYPESCREENREG");
		String var_FirstName;
                 LOGGER.info("Executed Step = VAR,String,var_FirstName,TGTYPESCREENREG");
		var_FirstName = getJsonData(var_CSVData , "$.records.["+var_Count+"].FirstName");
                 LOGGER.info("Executed Step = STORE,var_FirstName,var_CSVData,$.records.[{var_Count}].FirstName,TGTYPESCREENREG");
		String var_LastName;
                 LOGGER.info("Executed Step = VAR,String,var_LastName,TGTYPESCREENREG");
		var_LastName = getJsonData(var_CSVData , "$.records.["+var_Count+"].LastName");
                 LOGGER.info("Executed Step = STORE,var_LastName,var_CSVData,$.records.[{var_Count}].LastName,TGTYPESCREENREG");
		String var_StartDate;
                 LOGGER.info("Executed Step = VAR,String,var_StartDate,TGTYPESCREENREG");
		var_StartDate = getJsonData(var_CSVData , "$.records.["+var_Count+"].StartDate");
                 LOGGER.info("Executed Step = STORE,var_StartDate,var_CSVData,$.records.[{var_Count}].StartDate,TGTYPESCREENREG");
		String var_EndDate;
                 LOGGER.info("Executed Step = VAR,String,var_EndDate,TGTYPESCREENREG");
		var_EndDate = getJsonData(var_CSVData , "$.records.["+var_Count+"].EndDate");
                 LOGGER.info("Executed Step = STORE,var_EndDate,var_CSVData,$.records.[{var_Count}].EndDate,TGTYPESCREENREG");
        TAP.$("ELE_BUTTON_SESSIONDATE","//a[@id='headerForm:sessionDatelink']","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_NEWSESSIONDATE","//input[@id='sessionDateForm:sessionDateInput_input']",var_SessionDate,"FALSE","TGTYPESCREENREG");

        TAP.$("ELE_CHANGEDATE_BUTTON","//span[contains(text(),'Change Date')]","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_SEARCHTASKS","//input[@id='workbenchForm:workbenchTabs:taskSearchContainer:taskSearchSearch']","List Bills","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_TASKSEARCH","//a[@id='workbenchForm:workbenchTabs:taskSearchContainer:tb_buttonSearch']//span[@class='fa fa-search fa-lg']","TGTYPESCREENREG");

        TAP.$("ELE_LINK_LISTBILLS","//button[@id='workbenchForm:workbenchTabs:taskSearchContainer:0:j_idt84']//span[@class='ui-button-text ui-c'][contains(text(),'List Bills')]","TGTYPESCREENREG");

        WAIT.$("ELE_BUTTON_ADD","//button[@id='workbenchForm:workbenchTabs:tb_buttonAdd']//span[@class='ui-button-text ui-c'][contains(text(),'Add')]","VISIBLE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_ADD","//button[@id='workbenchForm:workbenchTabs:tb_buttonAdd']//span[@class='ui-button-text ui-c'][contains(text(),'Add')]","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

        WAIT.$("ELE_TXTBOX_LISTBILLNUMBER","//input[@id='workbenchForm:workbenchTabs:groupNumber']","VISIBLE","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_LISTBILLNUMBER","//input[@id='workbenchForm:workbenchTabs:groupNumber']",var_ListBillNumber,"FALSE","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_EFFECTIVEDATE","//input[@id='workbenchForm:workbenchTabs:effectiveDate_input']",var_EffectiveDate,"FALSE","TGTYPESCREENREG");

        TAP.$("ELE_DRPDWN_PAYMETHOD","//label[@id='workbenchForm:workbenchTabs:paymentCode_label']","TGTYPESCREENREG");

        TAP.$("ELE_DROPDWNSELECT_LISTBILLPM","//li[@id='workbenchForm:workbenchTabs:paymentCode_2']","TGTYPESCREENREG");

        TAP.$("ELE_LABEL_PAYMENTFREQUENCY","//label[@id='workbenchForm:workbenchTabs:billingMode_label']","TGTYPESCREENREG");

        TAP.$("ELE_DROPDWNSELECT_ANNUALPAYMENTFREQ","//li[@id='workbenchForm:workbenchTabs:billingMode_1']","TGTYPESCREENREG");

        TYPE.$("ELE_DATE_BILLTODATE","//input[@id='workbenchForm:workbenchTabs:billToDate_input']",var_SessionDate,"FALSE","TGTYPESCREENREG");

        SWIPE.$("DOWN","TGTYPESCREENREG");

        SWIPE.$("DOWN","TGTYPESCREENREG");

        TAP.$("ELE_DROPDWN_LISTBILLBILLING","//label[@id='workbenchForm:workbenchTabs:groupBillingCode_label']","TGTYPESCREENREG");

        TAP.$("ELE_DROPDWNSELECT_REGULARBILLING","//li[@id='workbenchForm:workbenchTabs:groupBillingCode_3']","TGTYPESCREENREG");

        TAP.$("ELE_OVERRIDEBILLINGLEADDAYS_SELECTNO","//table[@id='workbenchForm:workbenchTabs:userField1']//label[contains(text(),'No')]","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_NAICSCODE","//input[@id='workbenchForm:workbenchTabs:NAICSCODE']","110000","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_RDOBTN_24HOURS","//label[contains(text(),'24 hours')]","TGTYPESCREENREG");

        TAP.$("ELE_DROPDWN_DISABILITYINCOMEBENEFITPERIOD","//label[@id='workbenchForm:workbenchTabs:userField6_label']","TGTYPESCREENREG");

        TAP.$("ELE_DROPDWNSELECT_0DAYEP6MONTHBENEFITPERIOD","//li[@id='workbenchForm:workbenchTabs:userField6_1']","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_STATEOFISSUE","//input[@id='workbenchForm:workbenchTabs:userField4']","AZ","FALSE","TGTYPESCREENREG");

        TYPE.$("ELE_DATE_GROUPEFFECTIVEPRINTDATE","//input[@id='workbenchForm:workbenchTabs:GRPEFF_input']",var_EffectiveDate,"FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SHOWLINKS","//span[@class='ui-button-icon-left ui-icon ui-c fa fa-ellipsis-v fa-2x']","TGTYPESCREENREG");

        TAP.$("ELE_LINK_SELECTCLIENT","//span[contains(text(),'Select Client')]","TGTYPESCREENREG");

        WAIT.$(4,"TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_ADDGROUPCLIENT","//span[contains(text(),'Add Client')]","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_FIRSTNAME","//input[@id='workbenchForm:workbenchTabs:nameFirst']",var_FirstName,"FALSE","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_LASTNAME","//input[@id='workbenchForm:workbenchTabs:nameLast']",var_LastName,"FALSE","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_EMAILADDRESS1","//input[@id='workbenchForm:workbenchTabs:emailAddress']","testListBill@auto.com","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_VALIDATE","//span[contains(text(),'Validate')]","TGTYPESCREENREG");

        ASSERT.$("ELE_MSG_VALIDATIONSUCCESS","//div[@id='workbenchForm:workbenchTabs:messages']//span[@class='ui-messages-info-summary'][contains(text(),'Validation completed successfully with no errors.')]","VISIBLE","TGTYPESCREENREG");

        WAIT.$(3,"TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SAVE","//span[contains(text(),'Save')]","TGTYPESCREENREG");

        WAIT.$("ELE_TXTBOX_ADDRESSLINE1","//input[@id='workbenchForm:workbenchTabs:addressLineOne']","VISIBLE","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_ADDRESSLINE1","//input[@id='workbenchForm:workbenchTabs:addressLineOne']","Do not mail","FALSE","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_CITY","//input[@id='workbenchForm:workbenchTabs:addressCity']","Phoenix","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_DRPDWN_ISSUESTATECOUNTRY","//label[@id='workbenchForm:workbenchTabs:countryAndStateCode_label']","TGTYPESCREENREG");

        TAP.$("ELE_DROPDWNSELECT_STATEARIZONA","//li[@id='workbenchForm:workbenchTabs:countryAndStateCode_4']","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_ZIPCODE","//input[@id='workbenchForm:workbenchTabs:zipCode']","85001","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_VALIDATE","//span[contains(text(),'Validate')]","TGTYPESCREENREG");

        ASSERT.$("ELE_MSG_VALIDATIONSUCCESS","//div[@id='workbenchForm:workbenchTabs:messages']//span[@class='ui-messages-info-summary'][contains(text(),'Validation completed successfully with no errors.')]","VISIBLE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SAVE","//span[contains(text(),'Save')]","TGTYPESCREENREG");

        WAIT.$(3,"TGTYPESCREENREG");

        WAIT.$("ELE_BUTTON_CONTINUE","//span[contains(text(),'Continue')]","VISIBLE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_CONTINUE","//span[contains(text(),'Continue')]","TGTYPESCREENREG");

        WAIT.$(3,"TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_VALIDATE","//span[contains(text(),'Validate')]","TGTYPESCREENREG");

        ASSERT.$("ELE_MSG_VALIDATIONSUCCESS","//div[@id='workbenchForm:workbenchTabs:messages']//span[@class='ui-messages-info-summary'][contains(text(),'Validation completed successfully with no errors.')]","VISIBLE","TGTYPESCREENREG");

        WAIT.$(3,"TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SAVE","//span[contains(text(),'Save')]","TGTYPESCREENREG");

        WAIT.$(8,"TGTYPESCREENREG");

        WAIT.$("ELE_BUTTON_SHOWLINKS","//span[@class='ui-button-icon-left ui-icon ui-c fa fa-ellipsis-v fa-2x']","VISIBLE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SHOWLINKS","//span[@class='ui-button-icon-left ui-icon ui-c fa fa-ellipsis-v fa-2x']","TGTYPESCREENREG");

        TAP.$("ELE_LINK_GROUPSMAX","//span[contains(text(),'Group Smax')]","TGTYPESCREENREG");

        WAIT.$("ELE_BUTTON_ADD","//button[@id='workbenchForm:workbenchTabs:tb_buttonAdd']//span[@class='ui-button-text ui-c'][contains(text(),'Add')]","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_ADD","//button[@id='workbenchForm:workbenchTabs:tb_buttonAdd']//span[@class='ui-button-text ui-c'][contains(text(),'Add')]","TGTYPESCREENREG");

        WAIT.$("ELE_DATE_ENROLLMENTSTARTDATE","//input[@id='workbenchForm:workbenchTabs:startDate_input']","VISIBLE","TGTYPESCREENREG");

        TYPE.$("ELE_DATE_ENROLLMENTSTARTDATE","//input[@id='workbenchForm:workbenchTabs:startDate_input']",var_StartDate,"FALSE","TGTYPESCREENREG");

        TYPE.$("ELE_DATE_ENROLLMENTENDDATE","//input[@id='workbenchForm:workbenchTabs:endDate_input']",var_EndDate,"FALSE","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_GROUPSIZE","//input[@id='workbenchForm:workbenchTabs:groupSize_input']","100","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_RDOBTN_IMAXNO","//table[@id='workbenchForm:workbenchTabs:isImaxUsed']//label[contains(text(),'No')]","TGTYPESCREENREG");

        SWIPE.$("DOWN","TGTYPESCREENREG");

        SWIPE.$("DOWN","TGTYPESCREENREG");

        TAP.$("ELE_RDOBTN_ELYES","//table[@id='workbenchForm:workbenchTabs:underwritingTypeSi']//label[contains(text(),'Yes')]","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_ELMINIMUMLIVES","//input[@id='workbenchForm:workbenchTabs:minLivesSi_input']","5","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_RDOBTN_GIEZOFFERYES","//table[@id='workbenchForm:workbenchTabs:underwritingTypeGi']//label[contains(text(),'Yes')]","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_PARTICIPATIONGIEZ","//input[@id='workbenchForm:workbenchTabs:partPercGi_input']","1","FALSE","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_GIEZMINIMUMLIVES","//input[@id='workbenchForm:workbenchTabs:minLivesGi_input']","5","FALSE","TGTYPESCREENREG");

        SWIPE.$("DOWN","TGTYPESCREENREG");

        SWIPE.$("DOWN","TGTYPESCREENREG");

        TAP.$("ELE_RDOBTN_SIOFFERNO","//table[@id='workbenchForm:workbenchTabs:siOfferCI']//label[contains(text(),'No')]","TGTYPESCREENREG");

        TAP.$("ELE_RDOBTN_GIOFFERNO","//table[@id='workbenchForm:workbenchTabs:giOfferCI']//label[contains(text(),'No')]","TGTYPESCREENREG");

        TAP.$("ELE_RDOBTN_ACCGINO","//table[@id='workbenchForm:workbenchTabs:giOfferAI']//label[contains(text(),'No')]","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_MAXIMUMEMPLOYEELIFETIMELIMIT","//input[@id='workbenchForm:workbenchTabs:maxLifetimeEmployee_input']","500000","FALSE","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_MAXIMUMSPOUSELIFETIMELIMIT","//input[@id='workbenchForm:workbenchTabs:maxLifetimeSpouse_input']","300000","FALSE","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_MAXIMUMCHILDLIFETIMELIMIT","//input[@id='workbenchForm:workbenchTabs:maxLifetimeChild_input']","50000","FALSE","TGTYPESCREENREG");

		takeFullScreenShot();
                 LOGGER.info("Executed Step = TAKEFULLSCREENSHOT,TGTYPESCREENREG");
        TAP.$("ELE_BUTTON_VALIDATE","//span[contains(text(),'Validate')]","TGTYPESCREENREG");

        ASSERT.$("ELE_MSG_VALIDATIONSUCCESS","//div[@id='workbenchForm:workbenchTabs:messages']//span[@class='ui-messages-info-summary'][contains(text(),'Validation completed successfully with no errors.')]","VISIBLE","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

        WAIT.$("ELE_BUTTON_SAVE","//span[contains(text(),'Save')]","VISIBLE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SAVE","//span[contains(text(),'Save')]","TGTYPESCREENREG");

        SWIPE.$("DOWN","TGTYPESCREENREG");

        SWIPE.$("DOWN","TGTYPESCREENREG");

        WAIT.$("ELE_TAB_GROUPSMAX","//li[@class='ui-state-default ui-corner-top']//a[contains(text(),'Group Smax')]","VISIBLE","TGTYPESCREENREG");

        TAP.$("ELE_TAB_GROUPSMAX","//li[@class='ui-state-default ui-corner-top']//a[contains(text(),'Group Smax')]","TGTYPESCREENREG");

        TAP.$("ELE_REQRESULT_LISTBILL","//tbody[@id='workbenchForm:workbenchTabs:grid_data']","TGTYPESCREENREG");

        WAIT.$("ELE_TAB_DISPLAYLISTBILL","//a[contains(text(),'Display List Bill')]","VISIBLE","TGTYPESCREENREG");

        TAP.$("ELE_TAB_DISPLAYLISTBILL","//a[contains(text(),'Display List Bill')]","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SHOWLINKS","//span[@class='ui-button-icon-left ui-icon ui-c fa fa-ellipsis-v fa-2x']","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

        TAP.$("ELE_LINK_CLASSCATEGORY","//span[contains(text(),'Class Category')]","TGTYPESCREENREG");

        TAP.$("ELE_REQRESULT_LISTBILL","//tbody[@id='workbenchForm:workbenchTabs:grid_data']","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_EDITCLASSCAT","//div[@id='workbenchForm:workbenchTabs:tab_7']//span[@class='ui-button-text ui-c'][contains(text(),'Edit')]","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_MAXIMUMSILIMITIWLEZLIMIT","//input[@id='workbenchForm:workbenchTabs:maxLimitSi_input']","100000","FALSE","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_MAXIMUMGILIMITIWLEILIMIT","//input[@id='workbenchForm:workbenchTabs:maxLimitGi_input']","10000","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_VALIDATE","//span[contains(text(),'Validate')]","TGTYPESCREENREG");

        ASSERT.$("ELE_MSG_VALIDATIONSUCCESS","//div[@id='workbenchForm:workbenchTabs:messages']//span[@class='ui-messages-info-summary'][contains(text(),'Validation completed successfully with no errors.')]","VISIBLE","TGTYPESCREENREG");

        WAIT.$("ELE_BUTTON_SAVE","//span[contains(text(),'Save')]","VISIBLE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SAVE","//span[contains(text(),'Save')]","TGTYPESCREENREG");

        WAIT.$(3,"TGTYPESCREENREG");

        WAIT.$("ELE_TAB_DISPLAYLISTBILL","//a[contains(text(),'Display List Bill')]","VISIBLE","TGTYPESCREENREG");

        TAP.$("ELE_TAB_DISPLAYLISTBILL","//a[contains(text(),'Display List Bill')]","TGTYPESCREENREG");

        WAIT.$("ELE_BUTTON_SHOWLINKS","//span[@class='ui-button-icon-left ui-icon ui-c fa fa-ellipsis-v fa-2x']","VISIBLE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SHOWLINKS","//span[@class='ui-button-icon-left ui-icon ui-c fa fa-ellipsis-v fa-2x']","TGTYPESCREENREG");

        TAP.$("ELE_LINK_BILLINGNOTICESUPPRESSION","//span[contains(text(),'Billing Notice Suppression')]","TGTYPESCREENREG");

        WAIT.$("ELE_BUTTON_SAVE","//span[contains(text(),'Save')]","VISIBLE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SAVE","//span[contains(text(),'Save')]","TGTYPESCREENREG");

        WAIT.$(3,"TGTYPESCREENREG");

        TAP.$("ELE_LOGO_GIAS1","//span[contains(text(),'GIAS')]","TGTYPESCREENREG");


    }


    @Test
    public void mm40regspouseriderdeathclaimprocess() {

        // No need to declare / print method name here, handled by TestNg listener
        // declare("mm40regspouseriderdeathclaimprocess");

        CALL.$("LoginToGIAS4","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENNO");

        TYPE.$("ELE_TXTBOX_USERNAME","//input[@id='guestForm:username']","Mano","FALSE","TGTYPESCREENNO");

        TYPE.$("ELE_TXTBOX_PASSWORD","//input[@id='guestForm:password']","Cnx12345!","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_LOGIN","//button[@id='guestForm:loginButton']","TGTYPESCREENNO");

        CALL.$("PrePolicyInquirySpouseRiderDeathClaim","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_TASKSEARCH","//a[@id='workbenchForm:workbenchTabs:taskSearchContainer:tb_buttonSearch']//span[@class='fa fa-search fa-lg']","TGTYPESCREENREG");

		int var_Count = 0;
                 LOGGER.info("Executed Step = VAR,Integer,var_Count,0,TGTYPESCREENREG");
		com.jayway.jsonpath.ReadContext var_CSVData = getJsonData("http://10.1.104.229/json/20200916/aCa2S6.json");
                 LOGGER.info("Executed Step = VAR,Json,var_CSVData,http://10.1.104.229/json/20200916/aCa2S6.json,TGTYPESCREENREG");
		String var_PolicyNumber;
                 LOGGER.info("Executed Step = VAR,String,var_PolicyNumber,TGTYPESCREENREG");
		var_PolicyNumber = getJsonData(var_CSVData , "$.records["+var_Count+"].PolicyNumber");
                 LOGGER.info("Executed Step = STORE,var_PolicyNumber,var_CSVData,$.records[{var_Count}].PolicyNumber,TGTYPESCREENREG");
		String var_SessionDate;
                 LOGGER.info("Executed Step = VAR,String,var_SessionDate,TGTYPESCREENREG");
		var_SessionDate = getJsonData(var_CSVData , "$.records.["+var_Count+"].SessionDate");
                 LOGGER.info("Executed Step = STORE,var_SessionDate,var_CSVData,$.records.[{var_Count}].SessionDate,TGTYPESCREENREG");
		String var_SessionDate1;
                 LOGGER.info("Executed Step = VAR,String,var_SessionDate1,TGTYPESCREENREG");
		var_SessionDate1 = getJsonData(var_CSVData , "$.records.["+var_Count+"].SessionDate1");
                 LOGGER.info("Executed Step = STORE,var_SessionDate1,var_CSVData,$.records.[{var_Count}].SessionDate1,TGTYPESCREENREG");
		String var_FirstName;
                 LOGGER.info("Executed Step = VAR,String,var_FirstName,TGTYPESCREENREG");
		var_FirstName = getJsonData(var_CSVData , "$.records.["+var_Count+"].FirstName");
                 LOGGER.info("Executed Step = STORE,var_FirstName,var_CSVData,$.records.[{var_Count}].FirstName,TGTYPESCREENREG");
		String var_LastName;
                 LOGGER.info("Executed Step = VAR,String,var_LastName,TGTYPESCREENREG");
		var_LastName = getJsonData(var_CSVData , "$.records.["+var_Count+"].LastName");
                 LOGGER.info("Executed Step = STORE,var_LastName,var_CSVData,$.records.[{var_Count}].LastName,TGTYPESCREENREG");
		String var_DateOfDeath;
                 LOGGER.info("Executed Step = VAR,String,var_DateOfDeath,TGTYPESCREENREG");
		var_DateOfDeath = getJsonData(var_CSVData , "$.records.["+var_Count+"].DateOfDeath");
                 LOGGER.info("Executed Step = STORE,var_DateOfDeath,var_CSVData,$.records.[{var_Count}].DateOfDeath,TGTYPESCREENREG");
		String var_NotificationDate;
                 LOGGER.info("Executed Step = VAR,String,var_NotificationDate,TGTYPESCREENREG");
		var_NotificationDate = getJsonData(var_CSVData , "$.records.["+var_Count+"].NotificationDate");
                 LOGGER.info("Executed Step = STORE,var_NotificationDate,var_CSVData,$.records.[{var_Count}].NotificationDate,TGTYPESCREENREG");
		String var_SSN;
                 LOGGER.info("Executed Step = VAR,String,var_SSN,TGTYPESCREENREG");
		var_SSN = getJsonData(var_CSVData , "$.records.["+var_Count+"].SSN");
                 LOGGER.info("Executed Step = STORE,var_SSN,var_CSVData,$.records.[{var_Count}].SSN,TGTYPESCREENREG");
        TAP.$("ELE_BUTTON_SESSIONDATE","//a[@id='headerForm:sessionDatelink']","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_NEWSESSIONDATE","//input[@id='sessionDateForm:sessionDateInput_input']",var_SessionDate,"FALSE","TGTYPESCREENREG");

        TAP.$("ELE_CHANGEDATE_BUTTON","//span[contains(text(),'Change Date')]","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_SEARCHTASKS","//input[@id='workbenchForm:workbenchTabs:taskSearchContainer:taskSearchSearch']","Policy Inquiry","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_TASKSEARCH","//a[@id='workbenchForm:workbenchTabs:taskSearchContainer:tb_buttonSearch']//span[@class='fa fa-search fa-lg']","TGTYPESCREENREG");

        TAP.$("ELE_LINK_POLICYINQUIRY","//span[contains(text(),'Policy Inquiry')]","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_POLICYNUMBER","//input[@id='workbenchForm:workbenchTabs:policyNumber']",var_PolicyNumber,"FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_CONTINUE","//span[contains(text(),'Continue')]","TGTYPESCREENREG");

        WAIT.$("ELE_GETVAL_POLICYSTATUS","//span[@id='workbenchForm:workbenchTabs:statusText']","VISIBLE","TGTYPESCREENREG");

        ASSERT.$("ELE_GETVAL_POLICYSTATUS","//span[@id='workbenchForm:workbenchTabs:statusText']","VISIBLE","TGTYPESCREENREG");

		String var_LablePolicyNumber;
                 LOGGER.info("Executed Step = VAR,String,var_LablePolicyNumber,TGTYPESCREENREG");
		var_LablePolicyNumber = ActionWrapper.getElementValue("ELE_LABEL_POLICYNUMBER", "//span[@id='workbenchForm:workbenchTabs:policyNumber']");
                 LOGGER.info("Executed Step = STORE,var_LablePolicyNumber,ELE_LABEL_POLICYNUMBER,TGTYPESCREENREG");
		String var_ClientsName;
                 LOGGER.info("Executed Step = VAR,String,var_ClientsName,TGTYPESCREENREG");
		var_ClientsName = ActionWrapper.getElementValue("ELE_GETVAL_RIDER1NAME", "//label[@id='workbenchForm:workbenchTabs:dthbenAmount2_lbl']");
                 LOGGER.info("Executed Step = STORE,var_ClientsName,ELE_GETVAL_RIDER1NAME,TGTYPESCREENREG");
		takeFullScreenShot();
                 LOGGER.info("Executed Step = TAKEFULLSCREENSHOT,TGTYPESCREENREG");
        TAP.$("ELE_LINK_GIASHOME","//button[@id='headerForm:j_idt11']//span[@class='ui-button-text ui-c'][contains(text(),'GIAS')]","TGTYPESCREENREG");

        CALL.$("ProcessingSpouseRiderDeathClaim","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_TASKSEARCH","//a[@id='workbenchForm:workbenchTabs:taskSearchContainer:tb_buttonSearch']//span[@class='fa fa-search fa-lg']","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_SEARCHTASKS","//input[@id='workbenchForm:workbenchTabs:taskSearchContainer:taskSearchSearch']","Policy death","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_TASKSEARCH","//a[@id='workbenchForm:workbenchTabs:taskSearchContainer:tb_buttonSearch']//span[@class='fa fa-search fa-lg']","TGTYPESCREENREG");

        WAIT.$("ELE_LINK_POLICYDEATHANDLIVINGBENEFITCLAIMS","//span[contains(text(),'Policy Death and Living Benefit Claims')]","VISIBLE","TGTYPESCREENREG");

        TAP.$("ELE_LINK_POLICYDEATHANDLIVINGBENEFITCLAIMS","//span[contains(text(),'Policy Death and Living Benefit Claims')]","TGTYPESCREENREG");

        WAIT.$("ELE_LINK_ADDDEATHCLAIM","//span[contains(text(),'Add Death Claim')]","VISIBLE","TGTYPESCREENREG");

        TAP.$("ELE_LINK_ADDDEATHCLAIM","//span[contains(text(),'Add Death Claim')]","TGTYPESCREENREG");

        WAIT.$(3,"TGTYPESCREENREG");

        TYPE.$("ELE_DATE_NOTIFICATONDATE","//input[@id='workbenchForm:workbenchTabs:notiOfDthDate_input']",var_NotificationDate,"FALSE","TGTYPESCREENREG");

        TYPE.$("ELE_DATE_DATEOFDEATH","//input[@id='workbenchForm:workbenchTabs:dateOfDeathDate_input']",var_DateOfDeath,"FALSE","TGTYPESCREENREG");

        TYPE.$("ELE_DATE_DEATHCERTRCVDDATE","//input[@id='workbenchForm:workbenchTabs:deathCertRcvdDate_input']",var_SessionDate,"FALSE","TGTYPESCREENREG");

        TAP.$("ELE_REASONFORCLAIM_ACCDEATHBENEFIT","//label[@id='workbenchForm:workbenchTabs:transactionRsnCode_label']","TGTYPESCREENREG");

        TAP.$("ELE_DROPDWNSELECT_CONTAGIOUSDISEASES","//li[@id='workbenchForm:workbenchTabs:transactionRsnCode_6']","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SHOWLINKS","//span[@class='ui-button-icon-left ui-icon ui-c fa fa-ellipsis-v fa-2x']","TGTYPESCREENREG");

        TAP.$("ELE_LINK_SELECTCLIENT","//span[contains(text(),'Select Client')]","TGTYPESCREENREG");

        TAP.$("ELE_TXTBOX_SEARCH","//input[@id='workbenchForm:workbenchTabs:searchName']","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_SEARCH","//input[@id='workbenchForm:workbenchTabs:searchName']",var_ClientsName,"FALSE","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SEARCH","//span[contains(text(),'Search')]","TGTYPESCREENREG");

        WAIT.$(3,"TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SELECTCLIENT","//button[@id='workbenchForm:workbenchTabs:button_select']","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_CONTINUE","//span[contains(text(),'Continue')]","TGTYPESCREENREG");

        WAIT.$("ELE_BUTTON_VALIDATE","//span[contains(text(),'Validate')]","VISIBLE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_VALIDATE","//span[contains(text(),'Validate')]","TGTYPESCREENREG");

        ASSERT.$("ELE_MSG_VALIDATIONSUCCESS","//div[@id='workbenchForm:workbenchTabs:messages']//span[@class='ui-messages-info-summary'][contains(text(),'Validation completed successfully with no errors.')]","VISIBLE","TGTYPESCREENREG");

		takeFullScreenShot();
                 LOGGER.info("Executed Step = TAKEFULLSCREENSHOT,TGTYPESCREENREG");
        TAP.$("ELE_BUTTON_SAVE","//span[contains(text(),'Save')]","TGTYPESCREENREG");

        WAIT.$(8,"TGTYPESCREENREG");

        WAIT.$("ELE_BUTTON_SHOWLINKS","//span[@class='ui-button-icon-left ui-icon ui-c fa fa-ellipsis-v fa-2x']","VISIBLE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SHOWLINKS","//span[@class='ui-button-icon-left ui-icon ui-c fa fa-ellipsis-v fa-2x']","TGTYPESCREENREG");

        TAP.$("ELE_LINK_CLAIMDETAIL","//span[contains(text(),'Claim Detail')]","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        WAIT.$("ELE_BUTTON_SHOWLINKS","//span[@class='ui-button-icon-left ui-icon ui-c fa fa-ellipsis-v fa-2x']","VISIBLE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SHOWLINKS","//span[@class='ui-button-icon-left ui-icon ui-c fa fa-ellipsis-v fa-2x']","TGTYPESCREENREG");

        WAIT.$("ELE_LINK_ALLCLAIMPROCEEDS","//span[contains(text(),'All Claim Proceeds')]","VISIBLE","TGTYPESCREENREG");

        TAP.$("ELE_LINK_ALLCLAIMPROCEEDS","//span[contains(text(),'All Claim Proceeds')]","TGTYPESCREENREG");

        WAIT.$(3,"TGTYPESCREENREG");

        TAP.$("ELE_TAB_DISPLAYCLAIMACCD","//a[contains(text(),'Display Claim')]","TGTYPESCREENREG");

        WAIT.$("ELE_BUTTON_SHOWLINKS","//span[@class='ui-button-icon-left ui-icon ui-c fa fa-ellipsis-v fa-2x']","VISIBLE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SHOWLINKS","//span[@class='ui-button-icon-left ui-icon ui-c fa fa-ellipsis-v fa-2x']","TGTYPESCREENREG");

        TAP.$("ELE_LINK_CLIENT","//span[contains(text(),'Client')]","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        WAIT.$("ELE_TAB_DISPLAYCLAIMACCD","//a[contains(text(),'Display Claim')]","VISIBLE","TGTYPESCREENREG");

        TAP.$("ELE_TAB_DISPLAYCLAIMACCD","//a[contains(text(),'Display Claim')]","TGTYPESCREENREG");

        WAIT.$(3,"TGTYPESCREENREG");

        WAIT.$("ELE_BUTTON_SHOWLINKS","//span[@class='ui-button-icon-left ui-icon ui-c fa fa-ellipsis-v fa-2x']","VISIBLE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SHOWLINKS","//span[@class='ui-button-icon-left ui-icon ui-c fa fa-ellipsis-v fa-2x']","TGTYPESCREENREG");

        WAIT.$("ELE_LINK_PAYMENTS","//span[contains(text(),'Payments')]","VISIBLE","TGTYPESCREENREG");

        TAP.$("ELE_LINK_PAYMENTS","//span[contains(text(),'Payments')]","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        WAIT.$("ELE_BUTTON_ADDSUSPENCETASK","//span[contains(text(),'Add')]","VISIBLE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_ADDSUSPENCETASK","//span[contains(text(),'Add')]","TGTYPESCREENREG");

        ASSERT.$("ELE_LABEL_CLAIMTYPE","//span[@id='workbenchForm:workbenchTabs:claimType']","=","Death","TGTYPESCREENREG");

        SWIPE.$("DOWN","TGTYPESCREENREG");

        SWIPE.$("DOWN","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_BENEFICIARYFIRSTNAME2","//input[@id='workbenchForm:workbenchTabs:firstName']",var_FirstName,"FALSE","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_BENEFICIARYLASTNAME2","//input[@id='workbenchForm:workbenchTabs:lastName']",var_LastName,"FALSE","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_CITY","//input[@id='workbenchForm:workbenchTabs:addressCity']","GREENVILLE","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_DRPDWN_ISSUESTATECOUNTRY","//label[@id='workbenchForm:workbenchTabs:countryAndStateCode_label']","TGTYPESCREENREG");

        TAP.$("ELE_SELECTSTATE_SOUTHCAROLINA","//li[@id='workbenchForm:workbenchTabs:countryAndStateCode_49']","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_ADDRESSLINE1","//input[@id='workbenchForm:workbenchTabs:addressLineOne']","DO NOT MAIL","FALSE","TGTYPESCREENREG");

        SWIPE.$("DOWN","TGTYPESCREENREG");

        SWIPE.$("DOWN","TGTYPESCREENREG");

        TAP.$("ELE_DRPDWN_INTERESTRATESTATE","//label[@id='workbenchForm:workbenchTabs:countryAndStateCodeForInterestRateLookup_label']","TGTYPESCREENREG");

        TAP.$("ELE_SELECT_INTERESTSATESC","//li[@id='workbenchForm:workbenchTabs:countryAndStateCodeForInterestRateLookup_49']","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_VALIDATE","//span[contains(text(),'Validate')]","TGTYPESCREENREG");

        ASSERT.$("ELE_MSG_VALIDATIONSUCCESS","//div[@id='workbenchForm:workbenchTabs:messages']//span[@class='ui-messages-info-summary'][contains(text(),'Validation completed successfully with no errors.')]","VISIBLE","TGTYPESCREENREG");

        WAIT.$(3,"TGTYPESCREENREG");

        WAIT.$("ELE_BUTTON_SAVE","//span[contains(text(),'Save')]","VISIBLE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SAVE","//span[contains(text(),'Save')]","TGTYPESCREENREG");

		takeFullScreenShot();
                 LOGGER.info("Executed Step = TAKEFULLSCREENSHOT,TGTYPESCREENREG");
        WAIT.$(3,"TGTYPESCREENREG");

		takeFullScreenShot();
                 LOGGER.info("Executed Step = TAKEFULLSCREENSHOT,TGTYPESCREENREG");
        TAP.$("ELE_TAB_MYDASHBOARD","//a[contains(text(),'My Dashboard')]","TGTYPESCREENREG");

        WAIT.$("ELE_BUTTON_TASKSEARCH","//a[@id='workbenchForm:workbenchTabs:taskSearchContainer:tb_buttonSearch']//span[@class='fa fa-search fa-lg']","VISIBLE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_TASKSEARCH","//a[@id='workbenchForm:workbenchTabs:taskSearchContainer:tb_buttonSearch']//span[@class='fa fa-search fa-lg']","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_SEARCHTASKS","//input[@id='workbenchForm:workbenchTabs:taskSearchContainer:taskSearchSearch']","Policy Inquiry","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_TASKSEARCH","//a[@id='workbenchForm:workbenchTabs:taskSearchContainer:tb_buttonSearch']//span[@class='fa fa-search fa-lg']","TGTYPESCREENREG");

        TAP.$("ELE_LINK_POLICYINQUIRY","//span[contains(text(),'Policy Inquiry')]","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_POLICYNUMBER","//input[@id='workbenchForm:workbenchTabs:policyNumber']",var_PolicyNumber,"FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_CONTINUE","//span[contains(text(),'Continue')]","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

		takeFullScreenShot();
                 LOGGER.info("Executed Step = TAKEFULLSCREENSHOT,TGTYPESCREENREG");
        WAIT.$("ELE_LABEL_SUSPENDEDREASON","//span[@id='workbenchForm:workbenchTabs:suspendText']","VISIBLE","TGTYPESCREENREG");

        SWIPE.$("UP","TGTYPESCREENREG");

        SWIPE.$("UP","TGTYPESCREENREG");

        ASSERT.$("ELE_LABEL_SUSPENDEDREASON","//span[@id='workbenchForm:workbenchTabs:suspendText']","=","Yes/ZD","TGTYPESCREENREG");

        TAP.$("ELE_TAB_MYDASHBOARD","//a[contains(text(),'My Dashboard')]","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        WAIT.$("ELE_BUTTON_SESSIONDATE","//a[@id='headerForm:sessionDatelink']","VISIBLE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SESSIONDATE","//a[@id='headerForm:sessionDatelink']","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_NEWSESSIONDATE","//input[@id='sessionDateForm:sessionDateInput_input']",var_SessionDate1,"FALSE","TGTYPESCREENREG");

        TAP.$("ELE_CHANGEDATE_BUTTON","//span[contains(text(),'Change Date')]","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        TAP.$("ELE_TAB_CLAIM","//a[contains(text(),'Claims')]","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_CLAIMINSUREDNAME","//input[@id='workbenchForm:workbenchTabs:grid:individualName_Col:filter']",var_LastName,"FALSE","TGTYPESCREENREG");

        WAIT.$(3,"TGTYPESCREENREG");

        WAIT.$("ELE_SELECT_LINKBASEBENEFIT","//tr[@class='ui-widget-content ui-datatable-even ui-datatable-selectable']","VISIBLE","TGTYPESCREENREG");

        TAP.$("ELE_SELECT_LINKBASEBENEFIT","//tr[@class='ui-widget-content ui-datatable-even ui-datatable-selectable']","TGTYPESCREENREG");

        WAIT.$(3,"TGTYPESCREENREG");

        WAIT.$("ELE_BUTTON_SHOWLINKS","//span[@class='ui-button-icon-left ui-icon ui-c fa fa-ellipsis-v fa-2x']","VISIBLE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SHOWLINKS","//span[@class='ui-button-icon-left ui-icon ui-c fa fa-ellipsis-v fa-2x']","TGTYPESCREENREG");

        TAP.$("ELE_LINK_PAYMENTS","//span[contains(text(),'Payments')]","TGTYPESCREENREG");

        WAIT.$("ELE_SELECT_LINKBASEBENEFIT","//tr[@class='ui-widget-content ui-datatable-even ui-datatable-selectable']","VISIBLE","TGTYPESCREENREG");

        TAP.$("ELE_SELECT_LINKBASEBENEFIT","//tr[@class='ui-widget-content ui-datatable-even ui-datatable-selectable']","TGTYPESCREENREG");

		try { 
		         List<WebElement> col = driver.findElements(By.xpath("//tbody[@id='workbenchForm:workbenchTabs:grid_data']//tr"));

		        System.out.println("No of cols are : " +col.size());


		        for(int i= 0; i<col.size();i++) {

		            String a = Integer.toString(i);

		            String XpathFordateRow ="//span[@id='workbenchForm:workbenchTabs:grid:R:individualName']";
		            String Batchnumber = XpathFordateRow.replace("R", a);
		            WebElement batch = driver.findElement(By.xpath(Batchnumber));
		            String name = batch.getText();
		            if(name.contains(var_LastName)){
		                batch.click();
		                takeFullScreenShot();
		                i= col.size()+1;
		            }

		        }
		 
		 ;} 
		 catch(Exception e) { PrintVal("Renderer Script Exception : "+e.toString()); } 
		 PrintVal("Executed Step = CALLRENDERERSCRIPT,CLAIMPAYMENT_CLIENTSELECTLASTNAME"); 
        WAIT.$("ELE_BUTTON_EDITCLIENT","//span[@class='ui-button-text ui-c'][contains(text(),'Edit')]","VISIBLE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_EDITCLIENT","//span[@class='ui-button-text ui-c'][contains(text(),'Edit')]","TGTYPESCREENREG");

        TAP.$("ELE_RDOBTN_APPROVE","//label[text()='Approved']","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_PERCENTAGEOFDISBURSEMENTAMOUNT","//input[@id='workbenchForm:workbenchTabs:sharePercentage_input']","100","FALSE","TGTYPESCREENREG");

        SWIPE.$("DOWN","TGTYPESCREENREG");

        SWIPE.$("DOWN","TGTYPESCREENREG");

		String var_PaymentAmount;
                 LOGGER.info("Executed Step = VAR,String,var_PaymentAmount,TGTYPESCREENREG");
		var_PaymentAmount = ActionWrapper.getElementValue("ELE_UNPAIDDISBURSEMENTAMOUNT", "//span[@id='workbenchForm:workbenchTabs:unpaidClaimAmount']");
                 LOGGER.info("Executed Step = STORE,var_PaymentAmount,ELE_UNPAIDDISBURSEMENTAMOUNT,TGTYPESCREENREG");
        TYPE.$("ELE_TXTBOX_TOTALPAYMENTAMOUNT","//input[@id='workbenchForm:workbenchTabs:totalClaimAmountAppr3_input']",var_PaymentAmount,"FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SAVE","//span[contains(text(),'Save')]","TGTYPESCREENREG");

		takeFullScreenShot();
                 LOGGER.info("Executed Step = TAKEFULLSCREENSHOT,TGTYPESCREENREG");
        WAIT.$(3,"TGTYPESCREENREG");

        TAP.$("ELE_TAB_DISPLAYCLAIMACCD","//a[contains(text(),'Display Claim')]","TGTYPESCREENREG");

        WAIT.$(4,"TGTYPESCREENREG");

        WAIT.$("ELE_BUTTON_EDITCLIENT","//span[@class='ui-button-text ui-c'][contains(text(),'Edit')]","VISIBLE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_EDITCLIENT","//span[@class='ui-button-text ui-c'][contains(text(),'Edit')]","TGTYPESCREENREG");

        TAP.$("ELE_RDOBUT_CLAIMSTATUSAPPROVED","//table[@id='workbenchForm:workbenchTabs:claimStatusCode']//label[contains(text(),'Approved')]","TGTYPESCREENREG");

        TAP.$("ELE_RDOBTN_ISSUEAGEVERIFIED","//table[@id='workbenchForm:workbenchTabs:wasIssueAgeVerified']//label[contains(text(),'Yes')]","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_DATECLAIMAPPROVED","//input[@id='workbenchForm:workbenchTabs:dateClaimApprDate_input']",var_SessionDate1,"FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SAVE","//span[contains(text(),'Save')]","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SHOWLINKS","//span[@class='ui-button-icon-left ui-icon ui-c fa fa-ellipsis-v fa-2x']","TGTYPESCREENREG");

        TAP.$("ELE_LINK_PAYMENTS","//span[contains(text(),'Payments')]","TGTYPESCREENREG");

        TAP.$("ELE_SELECT_LINKBASEBENEFIT","//tr[@class='ui-widget-content ui-datatable-even ui-datatable-selectable']","TGTYPESCREENREG");

        WAIT.$(3,"TGTYPESCREENREG");

        WAIT.$("ELE_LINK_PAYCLAIM","//span[contains(text(),'Pay Claim')]","VISIBLE","TGTYPESCREENREG");

        TAP.$("ELE_LINK_PAYCLAIM","//span[contains(text(),'Pay Claim')]","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_COMMENTSPAYCLAIMPAYMENT","//textarea[@id='workbenchForm:workbenchTabs:checkCommentsText']","Death claim proceeds","FALSE","TGTYPESCREENREG");

        SWIPE.$("DOWN","TGTYPESCREENREG");

        SWIPE.$("DOWN","TGTYPESCREENREG");

        TAP.$("ELE_DRPDWN_IDTYPE","//label[@id='workbenchForm:workbenchTabs:taxIdenUsag_label']","TGTYPESCREENREG");

        TAP.$("ELE_IDTYPESELECT_SSN","//li[@id='workbenchForm:workbenchTabs:taxIdenUsag_2']","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_IDNO","//input[@id='workbenchForm:workbenchTabs:identificationNumber']",var_SSN,"FALSE","TGTYPESCREENREG");

		takeFullScreenShot();
                 LOGGER.info("Executed Step = TAKEFULLSCREENSHOT,TGTYPESCREENREG");
        WAIT.$(4,"TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_VALIDATE","//span[contains(text(),'Validate')]","TGTYPESCREENREG");

        WAIT.$(4,"TGTYPESCREENREG");

        ASSERT.$("ELE_MSG_VALIDATIONSUCCESS","//div[@id='workbenchForm:workbenchTabs:messages']//span[@class='ui-messages-info-summary'][contains(text(),'Validation completed successfully with no errors.')]","VISIBLE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SAVE","//span[contains(text(),'Save')]","TGTYPESCREENREG");

        WAIT.$(3,"TGTYPESCREENREG");

        ASSERT.$("ELE_MSG_ITEMSUCCESSUPDATE","//div[@id='workbenchForm:workbenchTabs:messages']//span[@class='ui-messages-info-summary'][contains(text(),'Item successfully updated.')]","VISIBLE","TGTYPESCREENREG");

        TAP.$("ELE_TAB_CLAIMPAYMENT","//li[@class='ui-state-default ui-corner-top']//a[contains(text(),'Claim Payment')]","TGTYPESCREENREG");

        WAIT.$(4,"TGTYPESCREENREG");

        TAP.$("ELE_LOGO_GIAS1","//span[contains(text(),'GIAS')]","TGTYPESCREENREG");

        WAIT.$(4,"TGTYPESCREENREG");

        WAIT.$("ELE_BUTTON_TASKSEARCH","//a[@id='workbenchForm:workbenchTabs:taskSearchContainer:tb_buttonSearch']//span[@class='fa fa-search fa-lg']","VISIBLE","TGTYPESCREENREG");

        CALL.$("PostPolicyInquirySpouseRiderDeathClaim","TGTYPESCREENREG");

        SWIPE.$("DOWN","TGTYPESCREENREG");

        SWIPE.$("DOWN","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_TASKSEARCH","//a[@id='workbenchForm:workbenchTabs:taskSearchContainer:tb_buttonSearch']//span[@class='fa fa-search fa-lg']","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_SEARCHTASKS","//input[@id='workbenchForm:workbenchTabs:taskSearchContainer:taskSearchSearch']","Policy Inquiry","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_TASKSEARCH","//a[@id='workbenchForm:workbenchTabs:taskSearchContainer:tb_buttonSearch']//span[@class='fa fa-search fa-lg']","TGTYPESCREENREG");

        TAP.$("ELE_LINK_POLICYINQUIRY","//span[contains(text(),'Policy Inquiry')]","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_POLICYNUMBER","//input[@id='workbenchForm:workbenchTabs:policyNumber']",var_PolicyNumber,"FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_CONTINUE","//span[contains(text(),'Continue')]","TGTYPESCREENREG");

        WAIT.$("ELE_GETVAL_POLICYSTATUS","//span[@id='workbenchForm:workbenchTabs:statusText']","VISIBLE","TGTYPESCREENREG");

        ASSERT.$("ELE_LABEL_SUSPENDEDREASON","//span[@id='workbenchForm:workbenchTabs:suspendText']","=","No","TGTYPESCREENREG");

		takeFullScreenShot();
                 LOGGER.info("Executed Step = TAKEFULLSCREENSHOT,TGTYPESCREENREG");
        TAP.$("ELE_BUTTON_SHOWLINKS","//span[@class='ui-button-icon-left ui-icon ui-c fa fa-ellipsis-v fa-2x']","TGTYPESCREENREG");

        TAP.$("ELE_LINK_BENEFITSOPTION","//span[text()='Benefits']","TGTYPESCREENREG");

		takeFullScreenShot();
                 LOGGER.info("Executed Step = TAKEFULLSCREENSHOT,TGTYPESCREENREG");
        WAIT.$("ELE_SELECT_LINKBASEBENEFIT","//tr[@class='ui-widget-content ui-datatable-even ui-datatable-selectable']","VISIBLE","TGTYPESCREENREG");

        TAP.$("ELE_SELECT_DISPLAYBENEFITRIDER1","//text()[.='Rider 1']/ancestor::td[1]","TGTYPESCREENREG");

        WAIT.$("ELE_LABEL_BENEFITSTATUS","//span[@id='workbenchForm:workbenchTabs:benefitStatusText']","VISIBLE","TGTYPESCREENREG");

        ASSERT.$("ELE_LABEL_BENEFITSTATUS","//span[@id='workbenchForm:workbenchTabs:benefitStatusText']","=","Death Claim","TGTYPESCREENREG");

		takeFullScreenShot();
                 LOGGER.info("Executed Step = TAKEFULLSCREENSHOT,TGTYPESCREENREG");
        TAP.$("ELE_LOGO_GIAS1","//span[contains(text(),'GIAS')]","TGTYPESCREENREG");

        SWIPE.$("DOWN","TGTYPESCREENREG");

        SWIPE.$("DOWN","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_TASKSEARCH","//a[@id='workbenchForm:workbenchTabs:taskSearchContainer:tb_buttonSearch']//span[@class='fa fa-search fa-lg']","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_SEARCHTASKS","//input[@id='workbenchForm:workbenchTabs:taskSearchContainer:taskSearchSearch']","Policy Inquiry","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_TASKSEARCH","//a[@id='workbenchForm:workbenchTabs:taskSearchContainer:tb_buttonSearch']//span[@class='fa fa-search fa-lg']","TGTYPESCREENREG");

        TAP.$("ELE_LINK_POLICYINQUIRY","//span[contains(text(),'Policy Inquiry')]","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_POLICYNUMBER","//input[@id='workbenchForm:workbenchTabs:policyNumber']",var_PolicyNumber,"FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_CONTINUE","//span[contains(text(),'Continue')]","TGTYPESCREENREG");

        WAIT.$("ELE_BUTTON_SHOWLINKS","//span[@class='ui-button-icon-left ui-icon ui-c fa fa-ellipsis-v fa-2x']","VISIBLE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SHOWLINKS","//span[@class='ui-button-icon-left ui-icon ui-c fa fa-ellipsis-v fa-2x']","TGTYPESCREENREG");

        WAIT.$("ELE_LINK_POLICY_VALUE","//span[contains(text(),'Policy Values')]","TGTYPESCREENREG");

        TAP.$("ELE_LINK_POLICY_VALUE","//span[contains(text(),'Policy Values')]","TGTYPESCREENREG");

        TAP.$("ELE_LOGO_GIAS1","//span[contains(text(),'GIAS')]","TGTYPESCREENREG");

        SWIPE.$("DOWN","TGTYPESCREENREG");

        SWIPE.$("DOWN","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_TASKSEARCH","//a[@id='workbenchForm:workbenchTabs:taskSearchContainer:tb_buttonSearch']//span[@class='fa fa-search fa-lg']","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_SEARCHTASKS","//input[@id='workbenchForm:workbenchTabs:taskSearchContainer:taskSearchSearch']","Policy Inquiry","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_TASKSEARCH","//a[@id='workbenchForm:workbenchTabs:taskSearchContainer:tb_buttonSearch']//span[@class='fa fa-search fa-lg']","TGTYPESCREENREG");

        TAP.$("ELE_LINK_POLICYINQUIRY","//span[contains(text(),'Policy Inquiry')]","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_POLICYNUMBER","//input[@id='workbenchForm:workbenchTabs:policyNumber']",var_PolicyNumber,"FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_CONTINUE","//span[contains(text(),'Continue')]","TGTYPESCREENREG");

        WAIT.$("ELE_BUTTON_SHOWLINKS","//span[@class='ui-button-icon-left ui-icon ui-c fa fa-ellipsis-v fa-2x']","VISIBLE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SHOWLINKS","//span[@class='ui-button-icon-left ui-icon ui-c fa fa-ellipsis-v fa-2x']","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

        WAIT.$("ELE_BUTTON_CLIENTTRANSACTIONHISTORY","//span[contains(text(),'Transaction History')]","VISIBLE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_CLIENTTRANSACTIONHISTORY","//span[contains(text(),'Transaction History')]","TGTYPESCREENREG");

		takeFullScreenShot();
                 LOGGER.info("Executed Step = TAKEFULLSCREENSHOT,TGTYPESCREENREG");
        TAP.$("ELE_LOGO_GIAS1","//span[contains(text(),'GIAS')]","TGTYPESCREENREG");

        WAIT.$(3,"TGTYPESCREENREG");

        SWIPE.$("DOWN","TGTYPESCREENREG");

        SWIPE.$("DOWN","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_TASKSEARCH","//a[@id='workbenchForm:workbenchTabs:taskSearchContainer:tb_buttonSearch']//span[@class='fa fa-search fa-lg']","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_SEARCHTASKS","//input[@id='workbenchForm:workbenchTabs:taskSearchContainer:taskSearchSearch']","ledger extract","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_TASKSEARCH","//a[@id='workbenchForm:workbenchTabs:taskSearchContainer:tb_buttonSearch']//span[@class='fa fa-search fa-lg']","TGTYPESCREENREG");

        TAP.$("ELE_LINK_LEDGEREXTRACTENTRIES","//span[contains(text(),'Ledger Extract Entries')]","TGTYPESCREENREG");

        WAIT.$("ELE_LEDGEREXTRACT_REFERENCE2","//input[@id='workbenchForm:workbenchTabs:grid:referenceNumber2_Col:filter']","VISIBLE","TGTYPESCREENREG");

        TYPE.$("ELE_LEDGEREXTRACT_REFERENCE2","//input[@id='workbenchForm:workbenchTabs:grid:referenceNumber2_Col:filter']",var_LablePolicyNumber,"FALSE","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

        TAP.$("ELE_LOGO_GIAS1","//span[contains(text(),'GIAS')]","TGTYPESCREENREG");


    }


    @Test
    public void mm40regpremiumpayappgrouplistbill() {

        // No need to declare / print method name here, handled by TestNg listener
        // declare("mm40regpremiumpayappgrouplistbill");

        CALL.$("LoginToGIAS4","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENNO");

        TYPE.$("ELE_TXTBOX_USERNAME","//input[@id='guestForm:username']","Mano","FALSE","TGTYPESCREENNO");

        TYPE.$("ELE_TXTBOX_PASSWORD","//input[@id='guestForm:password']","Cnx12345!","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_LOGIN","//button[@id='guestForm:loginButton']","TGTYPESCREENNO");

        CALL.$("PremiumPaymentApplicationGroupListBill","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_TASKSEARCH","//a[@id='workbenchForm:workbenchTabs:taskSearchContainer:tb_buttonSearch']//span[@class='fa fa-search fa-lg']","TGTYPESCREENREG");

		int var_Count = 0;
                 LOGGER.info("Executed Step = VAR,Integer,var_Count,0,TGTYPESCREENREG");
		com.jayway.jsonpath.ReadContext var_CSVData = getJsonData("http://10.1.104.229/json/20200916/6wGXAI.json");
                 LOGGER.info("Executed Step = VAR,Json,var_CSVData,http://10.1.104.229/json/20200916/6wGXAI.json,TGTYPESCREENREG");
		String var_ListBillNumber;
                 LOGGER.info("Executed Step = VAR,String,var_ListBillNumber,TGTYPESCREENREG");
		var_ListBillNumber = getJsonData(var_CSVData , "$.records.["+var_Count+"].ListBillNumber");
                 LOGGER.info("Executed Step = STORE,var_ListBillNumber,var_CSVData,$.records.[{var_Count}].ListBillNumber,TGTYPESCREENREG");
		String var_CollectionNumber;
                 LOGGER.info("Executed Step = VAR,String,var_CollectionNumber,TGTYPESCREENREG");
		var_CollectionNumber = getJsonData(var_CSVData , "$.records.["+var_Count+"].CollectionNumber");
                 LOGGER.info("Executed Step = STORE,var_CollectionNumber,var_CSVData,$.records.[{var_Count}].CollectionNumber,TGTYPESCREENREG");
		String var_SessionDate;
                 LOGGER.info("Executed Step = VAR,String,var_SessionDate,TGTYPESCREENREG");
		var_SessionDate = getJsonData(var_CSVData , "$.records.["+var_Count+"].SessionDate");
                 LOGGER.info("Executed Step = STORE,var_SessionDate,var_CSVData,$.records.[{var_Count}].SessionDate,TGTYPESCREENREG");
		String var_AmountOfChange;
                 LOGGER.info("Executed Step = VAR,String,var_AmountOfChange,TGTYPESCREENREG");
		var_AmountOfChange = getJsonData(var_CSVData , "$.records.["+var_Count+"].AmountOfChange");
                 LOGGER.info("Executed Step = STORE,var_AmountOfChange,var_CSVData,$.records.[{var_Count}].AmountOfChange,TGTYPESCREENREG");
        TAP.$("ELE_BUTTON_SESSIONDATE","//a[@id='headerForm:sessionDatelink']","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_NEWSESSIONDATE","//input[@id='sessionDateForm:sessionDateInput_input']",var_SessionDate,"FALSE","TGTYPESCREENREG");

        TAP.$("ELE_CHANGEDATE_BUTTON","//span[contains(text(),'Change Date')]","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_SEARCHTASKS","//input[@id='workbenchForm:workbenchTabs:taskSearchContainer:taskSearchSearch']","Open Billing","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_TASKSEARCH","//a[@id='workbenchForm:workbenchTabs:taskSearchContainer:tb_buttonSearch']//span[@class='fa fa-search fa-lg']","TGTYPESCREENREG");

        TAP.$("ELE_LINK_OPENBILLING","//span[contains(text(),'Open Billing')]","TGTYPESCREENREG");

        WAIT.$("ELE_TXTBOX_COLLECTIONNUMBER","//input[@id='workbenchForm:workbenchTabs:collectionNumber']","VISIBLE","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_COLLECTIONNUMBER","//input[@id='workbenchForm:workbenchTabs:collectionNumber']",var_CollectionNumber,"FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_CONTINUE","//span[contains(text(),'Continue')]","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        TAP.$("ELE_LOGO_GIAS1","//span[contains(text(),'GIAS')]","TGTYPESCREENREG");

        WAIT.$("ELE_BUTTON_TASKSEARCH","//a[@id='workbenchForm:workbenchTabs:taskSearchContainer:tb_buttonSearch']//span[@class='fa fa-search fa-lg']","VISIBLE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_TASKSEARCH","//a[@id='workbenchForm:workbenchTabs:taskSearchContainer:tb_buttonSearch']//span[@class='fa fa-search fa-lg']","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_SEARCHTASKS","//input[@id='workbenchForm:workbenchTabs:taskSearchContainer:taskSearchSearch']","Suspense Maintenance","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_TASKSEARCH","//a[@id='workbenchForm:workbenchTabs:taskSearchContainer:tb_buttonSearch']//span[@class='fa fa-search fa-lg']","TGTYPESCREENREG");

        TAP.$("ELE_LINK_SUSPENSEMAINTENCE","//span[contains(text(),'Suspense Maintenance')]","TGTYPESCREENREG");

        WAIT.$("ELE_BUTTON_ADD","//button[@id='workbenchForm:workbenchTabs:tb_buttonAdd']//span[@class='ui-button-text ui-c'][contains(text(),'Add')]","VISIBLE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_ADD","//button[@id='workbenchForm:workbenchTabs:tb_buttonAdd']//span[@class='ui-button-text ui-c'][contains(text(),'Add')]","TGTYPESCREENREG");

        WAIT.$("ELE_TXTBOX_SUSPENCECONTROLNUMBER","//input[@id='workbenchForm:workbenchTabs:suspenseControlNumb']","VISIBLE","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_SUSPENCECONTROLNUMBER","//input[@id='workbenchForm:workbenchTabs:suspenseControlNumb']",var_CollectionNumber,"FALSE","TGTYPESCREENREG");

        TAP.$("ELE_DRPDWN_SUSPENSETYPE","//label[@id='workbenchForm:workbenchTabs:suspenseType_label']","TGTYPESCREENREG");

        TAP.$("ELE_DROPDWNSELECT_GROUPSUSPENSE","//li[@id='workbenchForm:workbenchTabs:suspenseType_6']","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_AMTOFCHANGE","//input[@id='workbenchForm:workbenchTabs:amountOfChange_input']",var_AmountOfChange,"FALSE","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_SUSPENSECOMMENT","//input[@id='workbenchForm:workbenchTabs:suspenseComment']","Applying Payment to Group","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_VALIDATE","//span[contains(text(),'Validate')]","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SAVE","//span[contains(text(),'Save')]","TGTYPESCREENREG");

        WAIT.$(3,"TGTYPESCREENREG");

        WAIT.$("ELE_CONFIRMACTION_YES","//div[@class='ui-dialog-content ui-widget-content ui-df-content']//iframe","VISIBLE","TGTYPESCREENREG");

		try { 
		 driver.switchTo().frame(0);
		driver.findElement(By.xpath("//form[@id='confirmDialogForm']")).click();
		Thread.sleep(1000);
		driver.findElement(By.xpath("//button[@id='j_idt9']")).click();
		// Add Screenshot will only work for Android platform

		takeScreenshot();

		 
		 ;} 
		 catch(Exception e) { PrintVal("Renderer Script Exception : "+e.toString()); } 
		 PrintVal("Executed Step = CALLRENDERERSCRIPT,CLICKYESONCONFIRMACTION"); 
        WAIT.$(3,"TGTYPESCREENREG");

        TAP.$("ELE_TAB_SUSPENSE","//li[@class='ui-state-default ui-corner-top']//a[contains(text(),'Suspense')]","TGTYPESCREENREG");

        WAIT.$("ELE_TXTBOX_SUSPCONTROLNUMBER","//input[@id='workbenchForm:workbenchTabs:grid:suspenseControlNumb_Col:filter']","VISIBLE","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_SUSPCONTROLNUMBER","//input[@id='workbenchForm:workbenchTabs:grid:suspenseControlNumb_Col:filter']",var_CollectionNumber,"FALSE","TGTYPESCREENREG");

        WAIT.$(3,"TGTYPESCREENREG");

        WAIT.$("ELE_SELECT_LINKBASEBENEFIT","//tr[@class='ui-widget-content ui-datatable-even ui-datatable-selectable']","VISIBLE","TGTYPESCREENREG");

        TAP.$("ELE_SELECT_LINKBASEBENEFIT","//tr[@class='ui-widget-content ui-datatable-even ui-datatable-selectable']","TGTYPESCREENREG");

        TAP.$("ELE_LOGO_GIAS1","//span[contains(text(),'GIAS')]","TGTYPESCREENREG");

        WAIT.$(4,"TGTYPESCREENREG");

        WAIT.$("ELE_BUTTON_TASKSEARCH","//a[@id='workbenchForm:workbenchTabs:taskSearchContainer:tb_buttonSearch']//span[@class='fa fa-search fa-lg']","VISIBLE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_TASKSEARCH","//a[@id='workbenchForm:workbenchTabs:taskSearchContainer:tb_buttonSearch']//span[@class='fa fa-search fa-lg']","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_SEARCHTASKS","//input[@id='workbenchForm:workbenchTabs:taskSearchContainer:taskSearchSearch']","List Bill Payments","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_TASKSEARCH","//a[@id='workbenchForm:workbenchTabs:taskSearchContainer:tb_buttonSearch']//span[@class='fa fa-search fa-lg']","TGTYPESCREENREG");

        TAP.$("ELE_LINK_LISTBILLPAYMENTS","//span[contains(text(),'List Bill Payments')]","TGTYPESCREENREG");

        WAIT.$(4,"TGTYPESCREENREG");

        WAIT.$("ELE_TXTBOX_LISTBILLNUMBERGRID","//input[@id='workbenchForm:workbenchTabs:grid:groupNumber_Col:filter']","VISIBLE","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_LISTBILLNUMBERGRID","//input[@id='workbenchForm:workbenchTabs:grid:groupNumber_Col:filter']",var_ListBillNumber,"FALSE","TGTYPESCREENREG");

        WAIT.$(4,"TGTYPESCREENREG");

        TAP.$("ELE_SELECT_LINKBASEBENEFIT","//tr[@class='ui-widget-content ui-datatable-even ui-datatable-selectable']","TGTYPESCREENREG");

        WAIT.$("ELE_BUTTON_CONTINUE","//span[contains(text(),'Continue')]","VISIBLE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_CONTINUE","//span[contains(text(),'Continue')]","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_VALIDATE","//span[contains(text(),'Validate')]","TGTYPESCREENREG");

        WAIT.$(3,"TGTYPESCREENREG");

        WAIT.$("ELE_BUTTON_SUBMIT","//span[contains(text(),'Submit')]","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SUBMIT","//span[contains(text(),'Submit')]","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        TAP.$("ELE_LOGO_GIAS1","//span[contains(text(),'GIAS')]","TGTYPESCREENREG");

        SWIPE.$("DOWN","TGTYPESCREENREG");

        SWIPE.$("DOWN","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_TASKSEARCH","//a[@id='workbenchForm:workbenchTabs:taskSearchContainer:tb_buttonSearch']//span[@class='fa fa-search fa-lg']","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_SEARCHTASKS","//input[@id='workbenchForm:workbenchTabs:taskSearchContainer:taskSearchSearch']","Ledger Extract","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_TASKSEARCH","//a[@id='workbenchForm:workbenchTabs:taskSearchContainer:tb_buttonSearch']//span[@class='fa fa-search fa-lg']","TGTYPESCREENREG");

        TAP.$("ELE_LINK_LEDGEREXTRACTENTRIES","//span[contains(text(),'Ledger Extract Entries')]","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_LASTPAGE","//span[text()='E']","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        WAIT.$("ELE_LEDGEREXTRACT_REFERENCE2","//input[@id='workbenchForm:workbenchTabs:grid:referenceNumber2_Col:filter']","VISIBLE","TGTYPESCREENREG");

        TYPE.$("ELE_LEDGEREXTRACT_REFERENCE2","//input[@id='workbenchForm:workbenchTabs:grid:referenceNumber2_Col:filter']",var_CollectionNumber,"FALSE","TGTYPESCREENREG");

		takeFullScreenShot();
                 LOGGER.info("Executed Step = TAKEFULLSCREENSHOT,TGTYPESCREENREG");
        WAIT.$(3,"TGTYPESCREENREG");

        TAP.$("ELE_LOGO_GIAS1","//span[contains(text(),'GIAS')]","TGTYPESCREENREG");


    }


    @Test
    public void mm40regreinstatement() {

        // No need to declare / print method name here, handled by TestNg listener
        // declare("mm40regreinstatement");

        CALL.$("LoginToGIAS4","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENNO");

        TYPE.$("ELE_TXTBOX_USERNAME","//input[@id='guestForm:username']","Mano","FALSE","TGTYPESCREENNO");

        TYPE.$("ELE_TXTBOX_PASSWORD","//input[@id='guestForm:password']","Cnx12345!","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_LOGIN","//button[@id='guestForm:loginButton']","TGTYPESCREENNO");

        CALL.$("PolicyPreInquiryReinstatement","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_TASKSEARCH","//a[@id='workbenchForm:workbenchTabs:taskSearchContainer:tb_buttonSearch']//span[@class='fa fa-search fa-lg']","TGTYPESCREENREG");

		int var_Count = 0;
                 LOGGER.info("Executed Step = VAR,Integer,var_Count,0,TGTYPESCREENREG");
		com.jayway.jsonpath.ReadContext var_CSVData = getJsonData("http://10.1.104.229/json/20200916/1rctAy.json");
                 LOGGER.info("Executed Step = VAR,Json,var_CSVData,http://10.1.104.229/json/20200916/1rctAy.json,TGTYPESCREENREG");
		String var_PolicyNumber;
                 LOGGER.info("Executed Step = VAR,String,var_PolicyNumber,TGTYPESCREENREG");
		var_PolicyNumber = getJsonData(var_CSVData , "$.records["+var_Count+"].PolicyNumber");
                 LOGGER.info("Executed Step = STORE,var_PolicyNumber,var_CSVData,$.records[{var_Count}].PolicyNumber,TGTYPESCREENREG");
		String var_SessionDate;
                 LOGGER.info("Executed Step = VAR,String,var_SessionDate,TGTYPESCREENREG");
		var_SessionDate = getJsonData(var_CSVData , "$.records.["+var_Count+"].SessionDate");
                 LOGGER.info("Executed Step = STORE,var_SessionDate,var_CSVData,$.records.[{var_Count}].SessionDate,TGTYPESCREENREG");
        TAP.$("ELE_BUTTON_SESSIONDATE","//a[@id='headerForm:sessionDatelink']","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_NEWSESSIONDATE","//input[@id='sessionDateForm:sessionDateInput_input']",var_SessionDate,"FALSE","TGTYPESCREENREG");

        TAP.$("ELE_CHANGEDATE_BUTTON","//span[contains(text(),'Change Date')]","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_SEARCHTASKS","//input[@id='workbenchForm:workbenchTabs:taskSearchContainer:taskSearchSearch']","Policy Inquiry","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_TASKSEARCH","//a[@id='workbenchForm:workbenchTabs:taskSearchContainer:tb_buttonSearch']//span[@class='fa fa-search fa-lg']","TGTYPESCREENREG");

        TAP.$("ELE_LINK_POLICYINQUIRY","//span[contains(text(),'Policy Inquiry')]","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_POLICYNUMBER","//input[@id='workbenchForm:workbenchTabs:policyNumber']",var_PolicyNumber,"FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_CONTINUE","//span[contains(text(),'Continue')]","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        WAIT.$("ELE_GETVAL_POLICYSTATUS","//span[@id='workbenchForm:workbenchTabs:statusText']","VISIBLE","TGTYPESCREENREG");

        ASSERT.$("ELE_GETVAL_POLICYSTATUS","//span[@id='workbenchForm:workbenchTabs:statusText']","VISIBLE","TGTYPESCREENREG");

		String var_LabelPolNumber;
                 LOGGER.info("Executed Step = VAR,String,var_LabelPolNumber,TGTYPESCREENREG");
		var_LabelPolNumber = ActionWrapper.getElementValue("ELE_LABEL_POLICYNUMBER", "//span[@id='workbenchForm:workbenchTabs:policyNumber']");
                 LOGGER.info("Executed Step = STORE,var_LabelPolNumber,ELE_LABEL_POLICYNUMBER,TGTYPESCREENREG");
		String var_PaidTODate;
                 LOGGER.info("Executed Step = VAR,String,var_PaidTODate,TGTYPESCREENREG");
		var_PaidTODate = ActionWrapper.getElementValue("ELE_GETVAL_PAIDTODATE", "//span[@id='workbenchForm:workbenchTabs:paidToDate']");
                 LOGGER.info("Executed Step = STORE,var_PaidTODate,ELE_GETVAL_PAIDTODATE,TGTYPESCREENREG");
		String var_EffectiveDate;
                 LOGGER.info("Executed Step = VAR,String,var_EffectiveDate,TGTYPESCREENREG");
		var_EffectiveDate = ActionWrapper.getElementValue("ELE_LABEL_EFFECTIVE_DATE", "//span[@id='workbenchForm:workbenchTabs:benefitTable:0:effectiveDate']");
                 LOGGER.info("Executed Step = STORE,var_EffectiveDate,ELE_LABEL_EFFECTIVE_DATE,TGTYPESCREENREG");
		takeFullScreenShot();
                 LOGGER.info("Executed Step = TAKEFULLSCREENSHOT,TGTYPESCREENREG");
        TAP.$("ELE_LINK_GIASHOME","//button[@id='headerForm:j_idt11']//span[@class='ui-button-text ui-c'][contains(text(),'GIAS')]","TGTYPESCREENREG");

        CALL.$("Reinstatement","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_TASKSEARCH","//a[@id='workbenchForm:workbenchTabs:taskSearchContainer:tb_buttonSearch']//span[@class='fa fa-search fa-lg']","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_SEARCHTASKS","//input[@id='workbenchForm:workbenchTabs:taskSearchContainer:taskSearchSearch']","Reinstate","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_TASKSEARCH","//a[@id='workbenchForm:workbenchTabs:taskSearchContainer:tb_buttonSearch']//span[@class='fa fa-search fa-lg']","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_REINSTATEMENT","//span[contains(text(),'Reinstate Terminated Policy')]","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_POLICYNUMBER","//input[@id='workbenchForm:workbenchTabs:policyNumber']",var_PolicyNumber,"FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_CONTINUE","//span[contains(text(),'Continue')]","TGTYPESCREENREG");

        WAIT.$("ELE_TXTBOX_REINSTATEMENTDATE","//input[@id='workbenchForm:workbenchTabs:reinstatementDate_input']","VISIBLE","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_REINSTATEMENTDATE","//input[@id='workbenchForm:workbenchTabs:reinstatementDate_input']",var_EffectiveDate,"FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_VALIDATE","//span[contains(text(),'Validate')]","TGTYPESCREENREG");

        ASSERT.$("ELE_MSG_VALIDATIONSUCCESS","//div[@id='workbenchForm:workbenchTabs:messages']//span[@class='ui-messages-info-summary'][contains(text(),'Validation completed successfully with no errors.')]","VISIBLE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_CONTINUE","//span[contains(text(),'Continue')]","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_STATUSCHANGE","//span[contains(text(),'Status Change')]","TGTYPESCREENREG");

        WAIT.$("ELE_SELECT_LINKBASEBENEFIT","//tr[@class='ui-widget-content ui-datatable-even ui-datatable-selectable']","VISIBLE","TGTYPESCREENREG");

        TAP.$("ELE_SELECT_LINKBASEBENEFIT","//tr[@class='ui-widget-content ui-datatable-even ui-datatable-selectable']","TGTYPESCREENREG");

        ASSERT.$("ELE_DROPDWN_NEWBENEFITSTATUS","//label[@id='workbenchForm:workbenchTabs:newBenefitStatus_label']","VISIBLE","TGTYPESCREENREG");

        TAP.$("ELE_DROPDWN_NEWBENEFITSTATUS","//label[@id='workbenchForm:workbenchTabs:newBenefitStatus_label']","TGTYPESCREENREG");

        TAP.$("ELE_DRPDWN_PREMIUMPAYINGOPT","//li[@id='workbenchForm:workbenchTabs:newBenefitStatus_7']","TGTYPESCREENREG");

        SWIPE.$("UP","TGTYPESCREENREG");

        SWIPE.$("UP","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_TERMINATIONDATE","//input[@id='workbenchForm:workbenchTabs:terminationDate_input']","0","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_VALIDATE","//span[contains(text(),'Validate')]","TGTYPESCREENREG");

        ASSERT.$("ELE_MSG_VALIDATIONSUCCESS","//div[@id='workbenchForm:workbenchTabs:messages']//span[@class='ui-messages-info-summary'][contains(text(),'Validation completed successfully with no errors.')]","VISIBLE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SUBMIT","//span[contains(text(),'Submit')]","TGTYPESCREENREG");

        ASSERT.$("ELE_GETVAL_SUCCESSMESSAGE","//div[@id='workbenchForm:workbenchTabs:messages']//span[@class='ui-messages-info-summary'][contains(text(),'Request completed successfully.')]","VISIBLE","TGTYPESCREENREG");

        TAP.$("ELE_TAB_REINSTATEMENTMAINTENANCE","//a[contains(text(),'Reinstatement Maintenance')]","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

        TAP.$("ELE_SKIP_LASTPAGE","//span[@class='ui-button-icon-left ui-icon ui-c fa fa-fast-forward']","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_FINISHREINSTATEMENT","//span[contains(text(),'Finish Reinstatement')]","TGTYPESCREENREG");

        ASSERT.$("ELE_GETVAL_SUCCESSMSGREINSTATE","//div[@id='workbenchForm:workbenchTabs:messages']//span[@class='ui-messages-info-summary'][contains(text(),'Policy successfully reinstated.')]","VISIBLE","TGTYPESCREENREG");

        TAP.$("ELE_TAB_MYDASHBOARD","//a[contains(text(),'My Dashboard')]","TGTYPESCREENREG");

        WAIT.$("ELE_BUTTON_TASKSEARCH","//a[@id='workbenchForm:workbenchTabs:taskSearchContainer:tb_buttonSearch']//span[@class='fa fa-search fa-lg']","VISIBLE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_TASKSEARCH","//a[@id='workbenchForm:workbenchTabs:taskSearchContainer:tb_buttonSearch']//span[@class='fa fa-search fa-lg']","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_SEARCHTASKS","//input[@id='workbenchForm:workbenchTabs:taskSearchContainer:taskSearchSearch']","Policy Inquiry","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_TASKSEARCH","//a[@id='workbenchForm:workbenchTabs:taskSearchContainer:tb_buttonSearch']//span[@class='fa fa-search fa-lg']","TGTYPESCREENREG");

        TAP.$("ELE_LINK_POLICYINQUIRY","//span[contains(text(),'Policy Inquiry')]","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_POLICYNUMBER","//input[@id='workbenchForm:workbenchTabs:policyNumber']",var_PolicyNumber,"FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_CONTINUE","//span[contains(text(),'Continue')]","TGTYPESCREENREG");

        ASSERT.$("ELE_GETVAL_POLICYSTATUS","//span[@id='workbenchForm:workbenchTabs:statusText']","VISIBLE","TGTYPESCREENREG");

        ASSERT.$("ELE_GETVAL_POLICYSTATUS","//span[@id='workbenchForm:workbenchTabs:statusText']","=","Premium Paying","TGTYPESCREENREG");

		takeFullScreenShot();
                 LOGGER.info("Executed Step = TAKEFULLSCREENSHOT,TGTYPESCREENREG");
        TAP.$("ELE_TAB_MYDASHBOARD","//a[contains(text(),'My Dashboard')]","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_TASKSEARCH","//a[@id='workbenchForm:workbenchTabs:taskSearchContainer:tb_buttonSearch']//span[@class='fa fa-search fa-lg']","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_SEARCHTASKS","//input[@id='workbenchForm:workbenchTabs:taskSearchContainer:taskSearchSearch']","Policy Payment","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_TASKSEARCH","//a[@id='workbenchForm:workbenchTabs:taskSearchContainer:tb_buttonSearch']//span[@class='fa fa-search fa-lg']","TGTYPESCREENREG");

        TAP.$("ELE_LINKNEW_POLICYPAYMENT","//button[@id='workbenchForm:workbenchTabs:taskSearchContainer:2:j_idt84']//span[@class='ui-button-text ui-c'][contains(text(),'Policy Payment')]","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_POLICYNUMBER","//input[@id='workbenchForm:workbenchTabs:policyNumber']",var_PolicyNumber,"FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_CONTINUE","//span[contains(text(),'Continue')]","TGTYPESCREENREG");

        WAIT.$("ELE_GETVAL_MODALPREMIUM","//span[@id='workbenchForm:workbenchTabs:modalPremium']","VISIBLE","TGTYPESCREENREG");

		String var_ModalPremium;
                 LOGGER.info("Executed Step = VAR,String,var_ModalPremium,TGTYPESCREENREG");
		var_ModalPremium = ActionWrapper.getElementValue("ELE_GETVAL_MODALPREMIUM", "//span[@id='workbenchForm:workbenchTabs:modalPremium']");
                 LOGGER.info("Executed Step = STORE,var_ModalPremium,ELE_GETVAL_MODALPREMIUM,TGTYPESCREENREG");
        TAP.$("ELE_TAB_MYDASHBOARD","//a[contains(text(),'My Dashboard')]","TGTYPESCREENREG");

        WAIT.$("ELE_BUTTON_TASKSEARCH","//a[@id='workbenchForm:workbenchTabs:taskSearchContainer:tb_buttonSearch']//span[@class='fa fa-search fa-lg']","VISIBLE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_TASKSEARCH","//a[@id='workbenchForm:workbenchTabs:taskSearchContainer:tb_buttonSearch']//span[@class='fa fa-search fa-lg']","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_SEARCHTASKS","//input[@id='workbenchForm:workbenchTabs:taskSearchContainer:taskSearchSearch']","Suspense","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_TASKSEARCH","//a[@id='workbenchForm:workbenchTabs:taskSearchContainer:tb_buttonSearch']//span[@class='fa fa-search fa-lg']","TGTYPESCREENREG");

        TAP.$("ELE_LINK_SUSPENSEMAINTENCE","//span[contains(text(),'Suspense Maintenance')]","TGTYPESCREENREG");

        WAIT.$("ELE_BUTTON_ADD","//button[@id='workbenchForm:workbenchTabs:tb_buttonAdd']//span[@class='ui-button-text ui-c'][contains(text(),'Add')]","VISIBLE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_ADD","//button[@id='workbenchForm:workbenchTabs:tb_buttonAdd']//span[@class='ui-button-text ui-c'][contains(text(),'Add')]","TGTYPESCREENREG");

        WAIT.$("ELE_TXTBOX_SUSPENCECONTROLNUMBER","//input[@id='workbenchForm:workbenchTabs:suspenseControlNumb']","VISIBLE","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_SUSPENCECONTROLNUMBER","//input[@id='workbenchForm:workbenchTabs:suspenseControlNumb']",var_PolicyNumber,"FALSE","TGTYPESCREENREG");

        TAP.$("ELE_DRPDWN_SUSPENSETYPE","//label[@id='workbenchForm:workbenchTabs:suspenseType_label']","TGTYPESCREENREG");

        TAP.$("ELE_DROPDWNSELECT_PREMIUMSUSPENSE","//li[@id='workbenchForm:workbenchTabs:suspenseType_11']","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_SUSPENCEAMOUNTCHANGE","//input[@id='workbenchForm:workbenchTabs:amountOfChange_input']",var_ModalPremium,"FALSE","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_SUSPENSECOMMENT","//input[@id='workbenchForm:workbenchTabs:suspenseComment']","Add Premium Suspense","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_VALIDATE","//span[contains(text(),'Validate')]","TGTYPESCREENREG");

        ASSERT.$("ELE_MSG_VALIDATIONSUCCESS","//div[@id='workbenchForm:workbenchTabs:messages']//span[@class='ui-messages-info-summary'][contains(text(),'Validation completed successfully with no errors.')]","VISIBLE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SAVE","//span[contains(text(),'Save')]","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        TAP.$("ELE_TAB_POLICYPAYMENT","//a[contains(text(),'Payment')]","TGTYPESCREENREG");

        WAIT.$("ELE_BUTTON_SUBMIT","//span[contains(text(),'Submit')]","VISIBLE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SUBMIT","//span[contains(text(),'Submit')]","TGTYPESCREENREG");

        ASSERT.$("ELE_GETVAL_SUCCESSMESSAGE","//div[@id='workbenchForm:workbenchTabs:messages']//span[@class='ui-messages-info-summary'][contains(text(),'Request completed successfully.')]","VISIBLE","TGTYPESCREENREG");

        TAP.$("ELE_TAB_REINSTATEMENTMAINTENANCE","//a[contains(text(),'Reinstatement Maintenance')]","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        TAP.$("ELE_LOGO_GIAS1","//span[contains(text(),'GIAS')]","TGTYPESCREENREG");

        CALL.$("PolicyPostInquiryReinstatement","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_TASKSEARCH","//a[@id='workbenchForm:workbenchTabs:taskSearchContainer:tb_buttonSearch']//span[@class='fa fa-search fa-lg']","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_SEARCHTASKS","//input[@id='workbenchForm:workbenchTabs:taskSearchContainer:taskSearchSearch']","Policy Inquiry","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_TASKSEARCH","//a[@id='workbenchForm:workbenchTabs:taskSearchContainer:tb_buttonSearch']//span[@class='fa fa-search fa-lg']","TGTYPESCREENREG");

        TAP.$("ELE_LINK_POLICYINQUIRY","//span[contains(text(),'Policy Inquiry')]","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_POLICYNUMBER","//input[@id='workbenchForm:workbenchTabs:policyNumber']",var_PolicyNumber,"FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_CONTINUE","//span[contains(text(),'Continue')]","TGTYPESCREENREG");

        WAIT.$("ELE_GETVAL_POLICYSTATUS","//span[@id='workbenchForm:workbenchTabs:statusText']","VISIBLE","TGTYPESCREENREG");

		takeFullScreenShot();
                 LOGGER.info("Executed Step = TAKEFULLSCREENSHOT,TGTYPESCREENREG");
        TAP.$("ELE_BUTTON_SHOWLINKS","//span[@class='ui-button-icon-left ui-icon ui-c fa fa-ellipsis-v fa-2x']","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_CLIENTTRANSACTIONHISTORY","//span[contains(text(),'Transaction History')]","TGTYPESCREENREG");

		takeFullScreenShot();
                 LOGGER.info("Executed Step = TAKEFULLSCREENSHOT,TGTYPESCREENREG");
        TAP.$("ELE_TAB_MYDASHBOARD","//a[contains(text(),'My Dashboard')]","TGTYPESCREENREG");

        SWIPE.$("DOWN","ELE_BUTTON_TASKSEARCH","//a[@id='workbenchForm:workbenchTabs:taskSearchContainer:tb_buttonSearch']//span[@class='fa fa-search fa-lg']","TGTYPESCREENREG");

        WAIT.$("ELE_BUTTON_TASKSEARCH","//a[@id='workbenchForm:workbenchTabs:taskSearchContainer:tb_buttonSearch']//span[@class='fa fa-search fa-lg']","VISIBLE","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_SEARCHTASKS","//input[@id='workbenchForm:workbenchTabs:taskSearchContainer:taskSearchSearch']","ledger extract","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_TASKSEARCH","//a[@id='workbenchForm:workbenchTabs:taskSearchContainer:tb_buttonSearch']//span[@class='fa fa-search fa-lg']","TGTYPESCREENREG");

        TAP.$("ELE_LINK_LEDGEREXTRACTENTRIES","//span[contains(text(),'Ledger Extract Entries')]","TGTYPESCREENREG");

        WAIT.$("ELE_LEDGEREXTRACT_REFERENCE2","//input[@id='workbenchForm:workbenchTabs:grid:referenceNumber2_Col:filter']","VISIBLE","TGTYPESCREENREG");

        TYPE.$("ELE_LEDGEREXTRACT_REFERENCE2","//input[@id='workbenchForm:workbenchTabs:grid:referenceNumber2_Col:filter']",var_LabelPolNumber,"FALSE","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

        TAP.$("ELE_LOGO_GIAS1","//span[contains(text(),'GIAS')]","TGTYPESCREENREG");


    }


    @Test
    public void mm40regpaymentfreqchange() {

        // No need to declare / print method name here, handled by TestNg listener
        // declare("mm40regpaymentfreqchange");

        CALL.$("LoginToGIAS4","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENNO");

        TYPE.$("ELE_TXTBOX_USERNAME","//input[@id='guestForm:username']","Mano","FALSE","TGTYPESCREENNO");

        TYPE.$("ELE_TXTBOX_PASSWORD","//input[@id='guestForm:password']","Cnx12345!","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_LOGIN","//button[@id='guestForm:loginButton']","TGTYPESCREENNO");

        CALL.$("PolicyPreInquiryPayFreqChange","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_TASKSEARCH","//a[@id='workbenchForm:workbenchTabs:taskSearchContainer:tb_buttonSearch']//span[@class='fa fa-search fa-lg']","TGTYPESCREENREG");

		int var_Count = 0;
                 LOGGER.info("Executed Step = VAR,Integer,var_Count,0,TGTYPESCREENREG");
		com.jayway.jsonpath.ReadContext var_CSVData = getJsonData("http://10.1.104.229/json/20200916/llCRA4.json");
                 LOGGER.info("Executed Step = VAR,Json,var_CSVData,http://10.1.104.229/json/20200916/llCRA4.json,TGTYPESCREENREG");
		String var_PolicyNumber;
                 LOGGER.info("Executed Step = VAR,String,var_PolicyNumber,TGTYPESCREENREG");
		var_PolicyNumber = getJsonData(var_CSVData , "$.records["+var_Count+"].PolicyNumber");
                 LOGGER.info("Executed Step = STORE,var_PolicyNumber,var_CSVData,$.records[{var_Count}].PolicyNumber,TGTYPESCREENREG");
		String var_SessionDate;
                 LOGGER.info("Executed Step = VAR,String,var_SessionDate,TGTYPESCREENREG");
		var_SessionDate = getJsonData(var_CSVData , "$.records.["+var_Count+"].SessionDate");
                 LOGGER.info("Executed Step = STORE,var_SessionDate,var_CSVData,$.records.[{var_Count}].SessionDate,TGTYPESCREENREG");
		String var_ClientsName;
                 LOGGER.info("Executed Step = VAR,String,var_ClientsName,TGTYPESCREENREG");
		var_ClientsName = ActionWrapper.getElementValue("ELE_GETVAL_CLIENTNAME", "//span[@id='workbenchForm:workbenchTabs:ownerName']");
                 LOGGER.info("Executed Step = STORE,var_ClientsName,ELE_GETVAL_CLIENTNAME,TGTYPESCREENREG");
        TAP.$("ELE_BUTTON_SESSIONDATE","//a[@id='headerForm:sessionDatelink']","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_NEWSESSIONDATE","//input[@id='sessionDateForm:sessionDateInput_input']",var_SessionDate,"FALSE","TGTYPESCREENREG");

        TAP.$("ELE_CHANGEDATE_BUTTON","//span[contains(text(),'Change Date')]","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_SEARCHTASKS","//input[@id='workbenchForm:workbenchTabs:taskSearchContainer:taskSearchSearch']","Policy Inquiry","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_TASKSEARCH","//a[@id='workbenchForm:workbenchTabs:taskSearchContainer:tb_buttonSearch']//span[@class='fa fa-search fa-lg']","TGTYPESCREENREG");

        TAP.$("ELE_LINK_POLICYINQUIRY","//span[contains(text(),'Policy Inquiry')]","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_POLICYNUMBER","//input[@id='workbenchForm:workbenchTabs:policyNumber']",var_PolicyNumber,"FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_CONTINUE","//span[contains(text(),'Continue')]","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        WAIT.$("ELE_GETVAL_POLICYSTATUS","//span[@id='workbenchForm:workbenchTabs:statusText']","VISIBLE","TGTYPESCREENREG");

        ASSERT.$("ELE_GETVAL_POLICYSTATUS","//span[@id='workbenchForm:workbenchTabs:statusText']","=","Premium Paying","TGTYPESCREENREG");

		String var_LabelPolNumber;
                 LOGGER.info("Executed Step = VAR,String,var_LabelPolNumber,TGTYPESCREENREG");
		var_LabelPolNumber = ActionWrapper.getElementValue("ELE_LABEL_POLICYNUMBER", "//span[@id='workbenchForm:workbenchTabs:policyNumber']");
                 LOGGER.info("Executed Step = STORE,var_LabelPolNumber,ELE_LABEL_POLICYNUMBER,TGTYPESCREENREG");
		String var_PaidToDate;
                 LOGGER.info("Executed Step = VAR,String,var_PaidToDate,TGTYPESCREENREG");
		var_PaidToDate = ActionWrapper.getElementValue("ELE_GETVAL_PAIDTODATE", "//span[@id='workbenchForm:workbenchTabs:paidToDate']");
                 LOGGER.info("Executed Step = STORE,var_PaidToDate,ELE_GETVAL_PAIDTODATE,TGTYPESCREENREG");
		takeFullScreenShot();
                 LOGGER.info("Executed Step = TAKEFULLSCREENSHOT,TGTYPESCREENREG");
        TAP.$("ELE_LINK_GIASHOME","//button[@id='headerForm:j_idt11']//span[@class='ui-button-text ui-c'][contains(text(),'GIAS')]","TGTYPESCREENREG");

        CALL.$("PaymentFrequencyChanges","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_TASKSEARCH","//a[@id='workbenchForm:workbenchTabs:taskSearchContainer:tb_buttonSearch']//span[@class='fa fa-search fa-lg']","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_SEARCHTASKS","//input[@id='workbenchForm:workbenchTabs:taskSearchContainer:taskSearchSearch']","Client Search","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_TASKSEARCH","//a[@id='workbenchForm:workbenchTabs:taskSearchContainer:tb_buttonSearch']//span[@class='fa fa-search fa-lg']","TGTYPESCREENREG");

        TAP.$("ELE_LINK_CLIENTSEARCH","//span[contains(text(),'Client Search')]","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

        TYPE.$("ELE_TEXTINPUT_SEARCHCLIENTNAME","//input[@id='workbenchForm:workbenchTabs:clientGrid:individualName_Col:filter']",var_ClientsName,"FALSE","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

        TAP.$("ELE_LINK_CLIENTSEARCHEDNAMEGRID","//tbody[@id='workbenchForm:workbenchTabs:clientGrid_data']//tr","TGTYPESCREENREG");

        TAP.$("ELE_LOGO_GIAS1","//span[contains(text(),'GIAS')]","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_TASKSEARCH","//a[@id='workbenchForm:workbenchTabs:taskSearchContainer:tb_buttonSearch']//span[@class='fa fa-search fa-lg']","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_SEARCHTASKS","//input[@id='workbenchForm:workbenchTabs:taskSearchContainer:taskSearchSearch']","Policy Contract","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_TASKSEARCH","//a[@id='workbenchForm:workbenchTabs:taskSearchContainer:tb_buttonSearch']//span[@class='fa fa-search fa-lg']","TGTYPESCREENREG");

        TAP.$("ELE_LINK_CHANGEPOLICYCONTRACT","//span[contains(text(),'Change a Policy Contract')]","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_POLICYNUMBER","//input[@id='workbenchForm:workbenchTabs:policyNumber']",var_PolicyNumber,"FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_CONTINUE","//span[contains(text(),'Continue')]","TGTYPESCREENREG");

		String var_ListBillNo;
                 LOGGER.info("Executed Step = VAR,String,var_ListBillNo,TGTYPESCREENREG");
		var_ListBillNo = ActionWrapper.getElementValue("ELE_TXTBOX_LISTBILLNUMBER", "//input[@id='workbenchForm:workbenchTabs:groupNumber']");
                 LOGGER.info("Executed Step = STORE,var_ListBillNo,ELE_TXTBOX_LISTBILLNUMBER,TGTYPESCREENREG");
        TAP.$("ELE_LABEL_PAYMENTFREQUENCY","//label[@id='workbenchForm:workbenchTabs:billingMode_label']","TGTYPESCREENREG");

        TAP.$("ELE_DROPDWNSELECT_QUARTERLY","//li[@id='workbenchForm:workbenchTabs:billingMode_3']","TGTYPESCREENREG");

        TAP.$("ELE_DRPDWN_PAYMETHOD","//label[@id='workbenchForm:workbenchTabs:paymentCode_label']","TGTYPESCREENREG");

        TAP.$("ELE_DROPDWNSELECT_DIRECTPREMIUMNOTICE","//li[@id='workbenchForm:workbenchTabs:paymentCode_2']","TGTYPESCREENREG");

        SWIPE.$("DOWN","TGTYPESCREENREG");

        SWIPE.$("DOWN","TGTYPESCREENREG");

		try { 
		 WebElement reqdate =driver.findElement(By.xpath("//input[@id='workbenchForm:workbenchTabs:groupNumber']"));
		               
		                reqdate.clear();
		 
		 ;} 
		 catch(Exception e) { PrintVal("Renderer Script Exception : "+e.toString()); } 
		 PrintVal("Executed Step = CALLRENDERERSCRIPT,CLEARFIELDTEXTBOX"); 
        TYPE.$("ELE_TXTBOX_OLDGROUPNUMBER","//input[@id='workbenchForm:workbenchTabs:OLDGRPNUM']",var_ListBillNo,"FALSE","TGTYPESCREENREG");

        TYPE.$("ELE_DATE_DISABILITYDATE","//input[@id='workbenchForm:workbenchTabs:userDefinedField10A2_input']","0","FALSE","TGTYPESCREENREG");

		takeFullScreenShot();
                 LOGGER.info("Executed Step = TAKEFULLSCREENSHOT,TGTYPESCREENREG");
        TAP.$("ELE_BUTTON_VALIDATE","//span[contains(text(),'Validate')]","TGTYPESCREENREG");

        WAIT.$(3,"TGTYPESCREENREG");

        SWIPE.$("DOWN","TGTYPESCREENREG");

        ASSERT.$("ELE_MSG_VALIDATIONSUCCESS","//div[@id='workbenchForm:workbenchTabs:messages']//span[@class='ui-messages-info-summary'][contains(text(),'Validation completed successfully with no errors.')]","VISIBLE","TGTYPESCREENREG");

        WAIT.$(3,"TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SUBMIT","//span[contains(text(),'Submit')]","TGTYPESCREENREG");

        SWIPE.$("DOWN","TGTYPESCREENREG");

        ASSERT.$("ELE_GETVAL_SUCCESSMESSAGE","//div[@id='workbenchForm:workbenchTabs:messages']//span[@class='ui-messages-info-summary'][contains(text(),'Request completed successfully.')]","VISIBLE","TGTYPESCREENREG");

		takeFullScreenShot();
                 LOGGER.info("Executed Step = TAKEFULLSCREENSHOT,TGTYPESCREENREG");
        TAP.$("ELE_LOGO_GIAS1","//span[contains(text(),'GIAS')]","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        CALL.$("PolicyPostInquiryPayFreqChange","TGTYPESCREENREG");

        TAP.$("ELE_TXTBOX_SEARCHTASKS","//input[@id='workbenchForm:workbenchTabs:taskSearchContainer:taskSearchSearch']","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_SEARCHTASKS","//input[@id='workbenchForm:workbenchTabs:taskSearchContainer:taskSearchSearch']","Policy Inquiry","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_TASKSEARCH","//a[@id='workbenchForm:workbenchTabs:taskSearchContainer:tb_buttonSearch']//span[@class='fa fa-search fa-lg']","TGTYPESCREENREG");

        TAP.$("ELE_LINK_POLICYINQUIRY","//span[contains(text(),'Policy Inquiry')]","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_POLICYNUMBER","//input[@id='workbenchForm:workbenchTabs:policyNumber']",var_PolicyNumber,"FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_CONTINUE","//span[contains(text(),'Continue')]","TGTYPESCREENREG");

        WAIT.$("ELE_GETVAL_POLICYSTATUS","//span[@id='workbenchForm:workbenchTabs:statusText']","VISIBLE","TGTYPESCREENREG");

		takeFullScreenShot();
                 LOGGER.info("Executed Step = TAKEFULLSCREENSHOT,TGTYPESCREENREG");
        TAP.$("ELE_BUTTON_SHOWLINKS","//span[@class='ui-button-icon-left ui-icon ui-c fa fa-ellipsis-v fa-2x']","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_CLIENTTRANSACTIONHISTORY","//span[contains(text(),'Transaction History')]","TGTYPESCREENREG");

		takeFullScreenShot();
                 LOGGER.info("Executed Step = TAKEFULLSCREENSHOT,TGTYPESCREENREG");
        TAP.$("ELE_TAB_MYDASHBOARD","//a[contains(text(),'My Dashboard')]","TGTYPESCREENREG");

        WAIT.$(3,"TGTYPESCREENREG");

        SWIPE.$("DOWN","ELE_BUTTON_TASKSEARCH","//a[@id='workbenchForm:workbenchTabs:taskSearchContainer:tb_buttonSearch']//span[@class='fa fa-search fa-lg']","TGTYPESCREENREG");

        WAIT.$("ELE_BUTTON_TASKSEARCH","//a[@id='workbenchForm:workbenchTabs:taskSearchContainer:tb_buttonSearch']//span[@class='fa fa-search fa-lg']","VISIBLE","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_SEARCHTASKS","//input[@id='workbenchForm:workbenchTabs:taskSearchContainer:taskSearchSearch']","ledger extract","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_TASKSEARCH","//a[@id='workbenchForm:workbenchTabs:taskSearchContainer:tb_buttonSearch']//span[@class='fa fa-search fa-lg']","TGTYPESCREENREG");

        TAP.$("ELE_LINK_LEDGEREXTRACTENTRIES","//span[contains(text(),'Ledger Extract Entries')]","TGTYPESCREENREG");

        WAIT.$("ELE_LEDGEREXTRACT_REFERENCE2","//input[@id='workbenchForm:workbenchTabs:grid:referenceNumber2_Col:filter']","VISIBLE","TGTYPESCREENREG");

        TYPE.$("ELE_LEDGEREXTRACT_REFERENCE2","//input[@id='workbenchForm:workbenchTabs:grid:referenceNumber2_Col:filter']",var_LabelPolNumber,"FALSE","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

        TAP.$("ELE_LOGO_GIAS1","//span[contains(text(),'GIAS')]","TGTYPESCREENREG");


    }


    @Test
    public void mm40regeftsetup() {

        // No need to declare / print method name here, handled by TestNg listener
        // declare("mm40regeftsetup");

        CALL.$("LoginToGIAS4","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENNO");

        TYPE.$("ELE_TXTBOX_USERNAME","//input[@id='guestForm:username']","Mano","FALSE","TGTYPESCREENNO");

        TYPE.$("ELE_TXTBOX_PASSWORD","//input[@id='guestForm:password']","Cnx12345!","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_LOGIN","//button[@id='guestForm:loginButton']","TGTYPESCREENNO");

        CALL.$("PreInquiryEFTSetUP","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_TASKSEARCH","//a[@id='workbenchForm:workbenchTabs:taskSearchContainer:tb_buttonSearch']//span[@class='fa fa-search fa-lg']","TGTYPESCREENREG");

		int var_Count = 0;
                 LOGGER.info("Executed Step = VAR,Integer,var_Count,0,TGTYPESCREENREG");
		com.jayway.jsonpath.ReadContext var_CSVData = getJsonData("http://10.1.104.229/json/20201029/oWG4rR.json");
                 LOGGER.info("Executed Step = VAR,Json,var_CSVData,http://10.1.104.229/json/20201029/oWG4rR.json,TGTYPESCREENREG");
		String var_PolicyNumber;
                 LOGGER.info("Executed Step = VAR,String,var_PolicyNumber,TGTYPESCREENREG");
		var_PolicyNumber = getJsonData(var_CSVData , "$.records["+var_Count+"].PolicyNumber");
                 LOGGER.info("Executed Step = STORE,var_PolicyNumber,var_CSVData,$.records[{var_Count}].PolicyNumber,TGTYPESCREENREG");
		String var_SessionDate;
                 LOGGER.info("Executed Step = VAR,String,var_SessionDate,TGTYPESCREENREG");
		var_SessionDate = getJsonData(var_CSVData , "$.records.["+var_Count+"].SessionDate");
                 LOGGER.info("Executed Step = STORE,var_SessionDate,var_CSVData,$.records.[{var_Count}].SessionDate,TGTYPESCREENREG");
		String var_DebitDate;
                 LOGGER.info("Executed Step = VAR,String,var_DebitDate,TGTYPESCREENREG");
		var_DebitDate = getJsonData(var_CSVData , "$.records.["+var_Count+"].DebitDate");
                 LOGGER.info("Executed Step = STORE,var_DebitDate,var_CSVData,$.records.[{var_Count}].DebitDate,TGTYPESCREENREG");
		String var_BankAccountNo;
                 LOGGER.info("Executed Step = VAR,String,var_BankAccountNo,TGTYPESCREENREG");
		var_BankAccountNo = getJsonData(var_CSVData , "$.records.["+var_Count+"].BankAccountNo");
                 LOGGER.info("Executed Step = STORE,var_BankAccountNo,var_CSVData,$.records.[{var_Count}].BankAccountNo,TGTYPESCREENREG");
        TAP.$("ELE_BUTTON_SESSIONDATE","//a[@id='headerForm:sessionDatelink']","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_NEWSESSIONDATE","//input[@id='sessionDateForm:sessionDateInput_input']",var_SessionDate,"FALSE","TGTYPESCREENREG");

        TAP.$("ELE_CHANGEDATE_BUTTON","//span[contains(text(),'Change Date')]","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_SEARCHTASKS","//input[@id='workbenchForm:workbenchTabs:taskSearchContainer:taskSearchSearch']","Policy Inquiry","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_TASKSEARCH","//a[@id='workbenchForm:workbenchTabs:taskSearchContainer:tb_buttonSearch']//span[@class='fa fa-search fa-lg']","TGTYPESCREENREG");

        TAP.$("ELE_LINK_POLICYINQUIRY","//span[contains(text(),'Policy Inquiry')]","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_POLICYNUMBER","//input[@id='workbenchForm:workbenchTabs:policyNumber']",var_PolicyNumber,"FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_CONTINUE","//span[contains(text(),'Continue')]","TGTYPESCREENREG");

        WAIT.$("ELE_GETVAL_POLICYSTATUS","//span[@id='workbenchForm:workbenchTabs:statusText']","VISIBLE","TGTYPESCREENREG");

        ASSERT.$("ELE_GETVAL_POLICYSTATUS","//span[@id='workbenchForm:workbenchTabs:statusText']","VISIBLE","TGTYPESCREENREG");

		String var_LablePolicyNumber;
                 LOGGER.info("Executed Step = VAR,String,var_LablePolicyNumber,TGTYPESCREENREG");
		var_LablePolicyNumber = ActionWrapper.getElementValue("ELE_LABEL_POLICYNUMBER", "//span[@id='workbenchForm:workbenchTabs:policyNumber']");
                 LOGGER.info("Executed Step = STORE,var_LablePolicyNumber,ELE_LABEL_POLICYNUMBER,TGTYPESCREENREG");
		String var_ClientsName;
                 LOGGER.info("Executed Step = VAR,String,var_ClientsName,TGTYPESCREENREG");
		var_ClientsName = ActionWrapper.getElementValue("ELE_GETVAL_CLIENTNAME", "//span[@id='workbenchForm:workbenchTabs:ownerName']");
                 LOGGER.info("Executed Step = STORE,var_ClientsName,ELE_GETVAL_CLIENTNAME,TGTYPESCREENREG");
		takeFullScreenShot();
                 LOGGER.info("Executed Step = TAKEFULLSCREENSHOT,TGTYPESCREENREG");
        TAP.$("ELE_LINK_GIASHOME","//button[@id='headerForm:j_idt11']//span[@class='ui-button-text ui-c'][contains(text(),'GIAS')]","TGTYPESCREENREG");

        CALL.$("EFTSetUP","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_TASKSEARCH","//a[@id='workbenchForm:workbenchTabs:taskSearchContainer:tb_buttonSearch']//span[@class='fa fa-search fa-lg']","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_SEARCHTASKS","//input[@id='workbenchForm:workbenchTabs:taskSearchContainer:taskSearchSearch']","EFT","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_TASKSEARCH","//a[@id='workbenchForm:workbenchTabs:taskSearchContainer:tb_buttonSearch']//span[@class='fa fa-search fa-lg']","TGTYPESCREENREG");

        TAP.$("ELE_LINK_EFTANDCREDITCARDSETUP","//span[contains(text(),'EFT and Credit Card Setup')]","TGTYPESCREENREG");

        WAIT.$("ELE_BUTTON_ADD","//button[@id='workbenchForm:workbenchTabs:tb_buttonAdd']//span[@class='ui-button-text ui-c'][contains(text(),'Add')]","VISIBLE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_ADD","//button[@id='workbenchForm:workbenchTabs:tb_buttonAdd']//span[@class='ui-button-text ui-c'][contains(text(),'Add')]","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_ACCOUNTCONTROLNUMBER","//input[@type='text'][@name='workbenchForm:workbenchTabs:accountControlNumber']",var_PolicyNumber,"FALSE","TGTYPESCREENREG");

        TAP.$("ELE_RADIO_EFTLABEL","//label[contains(text(),'Electronic Funds Transfer')]","TGTYPESCREENREG");

        TAP.$("ELE_LABEL_PAYMENTFREQUENCY","//label[@id='workbenchForm:workbenchTabs:billingMode_label']","TGTYPESCREENREG");

        TAP.$("ELE_DROPDWNSELECT_ANNUALPAYMENTFREQ","//li[@id='workbenchForm:workbenchTabs:billingMode_1']","TGTYPESCREENREG");

        SWIPE.$("UP","TGTYPESCREENREG");

        TYPE.$("ELE_DATE_NEXTDEBITDATE","//input[@id='workbenchForm:workbenchTabs:nextDebitDate_input']",var_DebitDate,"FALSE","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_ROUTINGNUMBER","//input[@id='workbenchForm:workbenchTabs:bankRoutingNumber']","01100039","FALSE","TGTYPESCREENREG");

        SWIPE.$("UP","TGTYPESCREENREG");

        SWIPE.$("UP","TGTYPESCREENREG");

        TAP.$("ELE_DROPDWN_ACCTYPE","//label[@id='workbenchForm:workbenchTabs:bankAccountType_label']","TGTYPESCREENREG");

        TAP.$("ELE_DROPDWN_CHECKINGACCTYPE","//li[@id='workbenchForm:workbenchTabs:bankAccountType_1']","TGTYPESCREENREG");

        SWIPE.$("DOWN","TGTYPESCREENREG");

        SWIPE.$("DOWN","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_BANKACCNUMBER","//input[@id='workbenchForm:workbenchTabs:bankAccountNumber']",var_BankAccountNo,"FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SHOWLINKS","//span[@class='ui-button-icon-left ui-icon ui-c fa fa-ellipsis-v fa-2x']","TGTYPESCREENREG");

        TAP.$("ELE_LINK_SELECTCLIENT","//span[contains(text(),'Select Client')]","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_SEARCH","//input[@id='workbenchForm:workbenchTabs:searchName']",var_ClientsName,"FALSE","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SEARCH","//span[contains(text(),'Search')]","TGTYPESCREENREG");

        WAIT.$(3,"TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SELECTCLIENT","//button[@id='workbenchForm:workbenchTabs:button_select']","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_CONTINUE","//span[contains(text(),'Continue')]","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_VALIDATE","//span[contains(text(),'Validate')]","TGTYPESCREENREG");

        ASSERT.$("ELE_MSG_VALIDATIONSUCCESS","//div[@id='workbenchForm:workbenchTabs:messages']//span[@class='ui-messages-info-summary'][contains(text(),'Validation completed successfully with no errors.')]","VISIBLE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SAVE","//span[contains(text(),'Save')]","TGTYPESCREENREG");

        WAIT.$(3,"TGTYPESCREENREG");

        ASSERT.$("ELE_GETVAL_SUCCESSMESSAGE","//div[@id='workbenchForm:workbenchTabs:messages']//span[@class='ui-messages-info-summary'][contains(text(),'Request completed successfully.')]","VISIBLE","TGTYPESCREENREG");

        TAP.$("ELE_LINK_GIASHOME","//button[@id='headerForm:j_idt11']//span[@class='ui-button-text ui-c'][contains(text(),'GIAS')]","TGTYPESCREENREG");

        CALL.$("PostInquiryEFTSetUP","TGTYPESCREENREG");

        TAP.$("ELE_TXTBOX_SEARCHTASKS","//input[@id='workbenchForm:workbenchTabs:taskSearchContainer:taskSearchSearch']","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_SEARCHTASKS","//input[@id='workbenchForm:workbenchTabs:taskSearchContainer:taskSearchSearch']","Policy Inquiry","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_TASKSEARCH","//a[@id='workbenchForm:workbenchTabs:taskSearchContainer:tb_buttonSearch']//span[@class='fa fa-search fa-lg']","TGTYPESCREENREG");

        TAP.$("ELE_LINK_POLICYINQUIRY","//span[contains(text(),'Policy Inquiry')]","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_POLICYNUMBER","//input[@id='workbenchForm:workbenchTabs:policyNumber']",var_PolicyNumber,"FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_CONTINUE","//span[contains(text(),'Continue')]","TGTYPESCREENREG");

        WAIT.$("ELE_GETVAL_POLICYSTATUS","//span[@id='workbenchForm:workbenchTabs:statusText']","VISIBLE","TGTYPESCREENREG");

        ASSERT.$("ELE_GETVAL_POLICYSTATUS","//span[@id='workbenchForm:workbenchTabs:statusText']","=","Death Claim","TGTYPESCREENREG");

        ASSERT.$("ELE_LABEL_SUSPENDEDREASON","//span[@id='workbenchForm:workbenchTabs:suspendText']","=","No","TGTYPESCREENREG");

		takeFullScreenShot();
                 LOGGER.info("Executed Step = TAKEFULLSCREENSHOT,TGTYPESCREENREG");
        TAP.$("ELE_BUTTON_SHOWLINKS","//span[@class='ui-button-icon-left ui-icon ui-c fa fa-ellipsis-v fa-2x']","TGTYPESCREENREG");

        TAP.$("ELE_LINK_BASEBENEFIT","//span[contains(text(),'Base Benefit')]","TGTYPESCREENREG");

		takeFullScreenShot();
                 LOGGER.info("Executed Step = TAKEFULLSCREENSHOT,TGTYPESCREENREG");
        WAIT.$(2,"TGTYPESCREENREG");

        TAP.$("ELE_TAB_POLICYINQUIRY","//a[contains(text(),'Policy')]","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_CONTINUE","//span[contains(text(),'Continue')]","TGTYPESCREENREG");

        WAIT.$("ELE_BUTTON_SHOWLINKS","//span[@class='ui-button-icon-left ui-icon ui-c fa fa-ellipsis-v fa-2x']","VISIBLE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SHOWLINKS","//span[@class='ui-button-icon-left ui-icon ui-c fa fa-ellipsis-v fa-2x']","TGTYPESCREENREG");

        WAIT.$("ELE_LINK_POLICY_VALUE","//span[contains(text(),'Policy Values')]","TGTYPESCREENREG");

        TAP.$("ELE_LINK_POLICY_VALUE","//span[contains(text(),'Policy Values')]","TGTYPESCREENREG");

		takeFullScreenShot();
                 LOGGER.info("Executed Step = TAKEFULLSCREENSHOT,TGTYPESCREENREG");
        TAP.$("ELE_TAB_POLICYINQUIRY","//a[contains(text(),'Policy')]","TGTYPESCREENREG");

        TAP.$("ELE_TAB_POLICYINQUIRY","//a[contains(text(),'Policy')]","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_CONTINUE","//span[contains(text(),'Continue')]","TGTYPESCREENREG");

        WAIT.$("ELE_BUTTON_SHOWLINKS","//span[@class='ui-button-icon-left ui-icon ui-c fa fa-ellipsis-v fa-2x']","VISIBLE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SHOWLINKS","//span[@class='ui-button-icon-left ui-icon ui-c fa fa-ellipsis-v fa-2x']","TGTYPESCREENREG");

        WAIT.$("ELE_BUTTON_CLIENTTRANSACTIONHISTORY","//span[contains(text(),'Transaction History')]","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_CLIENTTRANSACTIONHISTORY","//span[contains(text(),'Transaction History')]","TGTYPESCREENREG");

        WAIT.$(4,"TGTYPESCREENREG");

		takeFullScreenShot();
                 LOGGER.info("Executed Step = TAKEFULLSCREENSHOT,TGTYPESCREENREG");
        TAP.$("ELE_LOGO_GIAS1","//span[contains(text(),'GIAS')]","TGTYPESCREENREG");

        WAIT.$(3,"TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_TASKSEARCH","//a[@id='workbenchForm:workbenchTabs:taskSearchContainer:tb_buttonSearch']//span[@class='fa fa-search fa-lg']","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_SEARCHTASKS","//input[@id='workbenchForm:workbenchTabs:taskSearchContainer:taskSearchSearch']","ledger extract","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_TASKSEARCH","//a[@id='workbenchForm:workbenchTabs:taskSearchContainer:tb_buttonSearch']//span[@class='fa fa-search fa-lg']","TGTYPESCREENREG");

        TAP.$("ELE_LINK_LEDGEREXTRACTENTRIES","//span[contains(text(),'Ledger Extract Entries')]","TGTYPESCREENREG");

        WAIT.$("ELE_LEDGEREXTRACT_REFERENCE2","//input[@id='workbenchForm:workbenchTabs:grid:referenceNumber2_Col:filter']","VISIBLE","TGTYPESCREENREG");

        TYPE.$("ELE_LEDGEREXTRACT_REFERENCE2","//input[@id='workbenchForm:workbenchTabs:grid:referenceNumber2_Col:filter']",var_LablePolicyNumber,"FALSE","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

        TAP.$("ELE_LOGO_GIAS1","//span[contains(text(),'GIAS')]","TGTYPESCREENREG");


    }


    @Test
    public void mm40regownerchange() {

        // No need to declare / print method name here, handled by TestNg listener
        // declare("mm40regownerchange");

        CALL.$("LoginToGIAS4","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENNO");

        TYPE.$("ELE_TXTBOX_USERNAME","//input[@id='guestForm:username']","Mano","FALSE","TGTYPESCREENNO");

        TYPE.$("ELE_TXTBOX_PASSWORD","//input[@id='guestForm:password']","Cnx12345!","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_LOGIN","//button[@id='guestForm:loginButton']","TGTYPESCREENNO");

        CALL.$("OwnerChangeProcess","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_TASKSEARCH","//a[@id='workbenchForm:workbenchTabs:taskSearchContainer:tb_buttonSearch']//span[@class='fa fa-search fa-lg']","TGTYPESCREENREG");

		int var_Count = 0;
                 LOGGER.info("Executed Step = VAR,Integer,var_Count,0,TGTYPESCREENREG");
		com.jayway.jsonpath.ReadContext var_CSVData = getJsonData("http://10.1.104.229/json/20201013/0XXZ0u.json");
                 LOGGER.info("Executed Step = VAR,Json,var_CSVData,http://10.1.104.229/json/20201013/0XXZ0u.json,TGTYPESCREENREG");
		String var_PolicyNumber;
                 LOGGER.info("Executed Step = VAR,String,var_PolicyNumber,TGTYPESCREENREG");
		var_PolicyNumber = getJsonData(var_CSVData , "$.records["+var_Count+"].PolicyNumber");
                 LOGGER.info("Executed Step = STORE,var_PolicyNumber,var_CSVData,$.records[{var_Count}].PolicyNumber,TGTYPESCREENREG");
		String var_SessionDate;
                 LOGGER.info("Executed Step = VAR,String,var_SessionDate,TGTYPESCREENREG");
		var_SessionDate = getJsonData(var_CSVData , "$.records.["+var_Count+"].SessionDate");
                 LOGGER.info("Executed Step = STORE,var_SessionDate,var_CSVData,$.records.[{var_Count}].SessionDate,TGTYPESCREENREG");
		String var_FirstName;
                 LOGGER.info("Executed Step = VAR,String,var_FirstName,TGTYPESCREENREG");
		var_FirstName = getJsonData(var_CSVData , "$.records.["+var_Count+"].FirstName");
                 LOGGER.info("Executed Step = STORE,var_FirstName,var_CSVData,$.records.[{var_Count}].FirstName,TGTYPESCREENREG");
		String var_DOB;
                 LOGGER.info("Executed Step = VAR,String,var_DOB,TGTYPESCREENREG");
		var_DOB = getJsonData(var_CSVData , "$.records.["+var_Count+"].DOB");
                 LOGGER.info("Executed Step = STORE,var_DOB,var_CSVData,$.records.[{var_Count}].DOB,TGTYPESCREENREG");
		String var_SSN;
                 LOGGER.info("Executed Step = VAR,String,var_SSN,TGTYPESCREENREG");
		var_SSN = getJsonData(var_CSVData , "$.records.["+var_Count+"].SSN");
                 LOGGER.info("Executed Step = STORE,var_SSN,var_CSVData,$.records.[{var_Count}].SSN,TGTYPESCREENREG");
        TAP.$("ELE_BUTTON_SESSIONDATE","//a[@id='headerForm:sessionDatelink']","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_NEWSESSIONDATE","//input[@id='sessionDateForm:sessionDateInput_input']",var_SessionDate,"FALSE","TGTYPESCREENREG");

        TAP.$("ELE_CHANGEDATE_BUTTON","//span[contains(text(),'Change Date')]","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_SEARCHTASKS","//input[@id='workbenchForm:workbenchTabs:taskSearchContainer:taskSearchSearch']","Policy Inquiry","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_TASKSEARCH","//a[@id='workbenchForm:workbenchTabs:taskSearchContainer:tb_buttonSearch']//span[@class='fa fa-search fa-lg']","TGTYPESCREENREG");

        TAP.$("ELE_LINK_POLICYINQUIRY","//span[contains(text(),'Policy Inquiry')]","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_POLICYNUMBER","//input[@id='workbenchForm:workbenchTabs:policyNumber']",var_PolicyNumber,"FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_CONTINUE","//span[contains(text(),'Continue')]","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        WAIT.$("ELE_GETVAL_POLICYSTATUS","//span[@id='workbenchForm:workbenchTabs:statusText']","VISIBLE","TGTYPESCREENREG");

        ASSERT.$("ELE_GETVAL_POLICYSTATUS","//span[@id='workbenchForm:workbenchTabs:statusText']","=","Premium Paying","TGTYPESCREENREG");

		String var_LabelPolNumber;
                 LOGGER.info("Executed Step = VAR,String,var_LabelPolNumber,TGTYPESCREENREG");
		var_LabelPolNumber = ActionWrapper.getElementValue("ELE_LABEL_POLICYNUMBER", "//span[@id='workbenchForm:workbenchTabs:policyNumber']");
                 LOGGER.info("Executed Step = STORE,var_LabelPolNumber,ELE_LABEL_POLICYNUMBER,TGTYPESCREENREG");
        TAP.$("ELE_BUTTON_SHOWLINKS","//span[@class='ui-button-icon-left ui-icon ui-c fa fa-ellipsis-v fa-2x']","TGTYPESCREENREG");

        WAIT.$("ELE_BUTTON_CLIENTS","//span[contains(text(),'Clients')]","VISIBLE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_CLIENTS","//span[contains(text(),'Clients')]","TGTYPESCREENREG");

        WAIT.$(3,"TGTYPESCREENREG");

        TAP.$("ELE_RELATIONTYPE_OWNERONE","//span[@id='workbenchForm:workbenchTabs:grid:2:userDefnRelaType']","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SHOWLINKS","//span[@class='ui-button-icon-left ui-icon ui-c fa fa-ellipsis-v fa-2x']","TGTYPESCREENREG");

        TAP.$("ELE_LINK_DISPLAYCLIENT","//span[contains(text(),'Display Client')]","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

        WAIT.$("ELE_BUTTON_EDIT","//span[text()='Edit']","VISIBLE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_EDIT","//span[text()='Edit']","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_FIRSTNAME","//input[@id='workbenchForm:workbenchTabs:nameFirst']",var_FirstName,"FALSE","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_DOB","//input[@id='workbenchForm:workbenchTabs:dateOfBirth_input']",var_DOB,"FALSE","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_IDTYPE","//input[@id='workbenchForm:workbenchTabs:identificationNumber']",var_SSN,"FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_VALIDATE","//span[contains(text(),'Validate')]","TGTYPESCREENREG");

        ASSERT.$("ELE_MSG_VALIDATIONSUCCESS","//div[@id='workbenchForm:workbenchTabs:messages']//span[@class='ui-messages-info-summary'][contains(text(),'Validation completed successfully with no errors.')]","VISIBLE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SAVE","//span[contains(text(),'Save')]","TGTYPESCREENREG");

        WAIT.$(3,"TGTYPESCREENREG");

        TAP.$("ELE_TAB_TABRELATIONSHIPS","//a[contains(text(),'Relationships')]","TGTYPESCREENREG");

        WAIT.$("ELE_RELATIONTYPE_OWNERONE","//span[@id='workbenchForm:workbenchTabs:grid:2:userDefnRelaType']","VISIBLE","TGTYPESCREENREG");

        TAP.$("ELE_RELATIONTYPE_OWNERONE","//span[@id='workbenchForm:workbenchTabs:grid:2:userDefnRelaType']","TGTYPESCREENREG");

        WAIT.$(3,"TGTYPESCREENREG");

        TAP.$("ELE_LINK_GIASHOME","//button[@id='headerForm:j_idt11']//span[@class='ui-button-text ui-c'][contains(text(),'GIAS')]","TGTYPESCREENREG");

        WAIT.$(3,"TGTYPESCREENREG");


    }


    @Test
    public void mm40regaddresschange() {

        // No need to declare / print method name here, handled by TestNg listener
        // declare("mm40regaddresschange");

        CALL.$("LoginToGIAS4","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENNO");

        TYPE.$("ELE_TXTBOX_USERNAME","//input[@id='guestForm:username']","Mano","FALSE","TGTYPESCREENNO");

        TYPE.$("ELE_TXTBOX_PASSWORD","//input[@id='guestForm:password']","Cnx12345!","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_LOGIN","//button[@id='guestForm:loginButton']","TGTYPESCREENNO");

        CALL.$("AddressChange","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_TASKSEARCH","//a[@id='workbenchForm:workbenchTabs:taskSearchContainer:tb_buttonSearch']//span[@class='fa fa-search fa-lg']","TGTYPESCREENREG");

		int var_Count = 0;
                 LOGGER.info("Executed Step = VAR,Integer,var_Count,0,TGTYPESCREENREG");
		com.jayway.jsonpath.ReadContext var_CSVData = getJsonData("http://10.1.104.229/json/20201013/8ilWd2.json");
                 LOGGER.info("Executed Step = VAR,Json,var_CSVData,http://10.1.104.229/json/20201013/8ilWd2.json,TGTYPESCREENREG");
		String var_PolicyNumber;
                 LOGGER.info("Executed Step = VAR,String,var_PolicyNumber,TGTYPESCREENREG");
		var_PolicyNumber = getJsonData(var_CSVData , "$.records["+var_Count+"].PolicyNumber");
                 LOGGER.info("Executed Step = STORE,var_PolicyNumber,var_CSVData,$.records[{var_Count}].PolicyNumber,TGTYPESCREENREG");
		String var_SessionDate;
                 LOGGER.info("Executed Step = VAR,String,var_SessionDate,TGTYPESCREENREG");
		var_SessionDate = getJsonData(var_CSVData , "$.records.["+var_Count+"].SessionDate");
                 LOGGER.info("Executed Step = STORE,var_SessionDate,var_CSVData,$.records.[{var_Count}].SessionDate,TGTYPESCREENREG");
        TAP.$("ELE_BUTTON_SESSIONDATE","//a[@id='headerForm:sessionDatelink']","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_NEWSESSIONDATE","//input[@id='sessionDateForm:sessionDateInput_input']",var_SessionDate,"FALSE","TGTYPESCREENREG");

        TAP.$("ELE_CHANGEDATE_BUTTON","//span[contains(text(),'Change Date')]","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_SEARCHTASKS","//input[@id='workbenchForm:workbenchTabs:taskSearchContainer:taskSearchSearch']","Policy Inquiry","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_TASKSEARCH","//a[@id='workbenchForm:workbenchTabs:taskSearchContainer:tb_buttonSearch']//span[@class='fa fa-search fa-lg']","TGTYPESCREENREG");

        TAP.$("ELE_LINK_POLICYINQUIRY","//span[contains(text(),'Policy Inquiry')]","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_POLICYNUMBER","//input[@id='workbenchForm:workbenchTabs:policyNumber']",var_PolicyNumber,"FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_CONTINUE","//span[contains(text(),'Continue')]","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        WAIT.$("ELE_GETVAL_POLICYSTATUS","//span[@id='workbenchForm:workbenchTabs:statusText']","VISIBLE","TGTYPESCREENREG");

        ASSERT.$("ELE_GETVAL_POLICYSTATUS","//span[@id='workbenchForm:workbenchTabs:statusText']","=","Premium Paying","TGTYPESCREENREG");

		String var_LabelPolNumber;
                 LOGGER.info("Executed Step = VAR,String,var_LabelPolNumber,TGTYPESCREENREG");
		var_LabelPolNumber = ActionWrapper.getElementValue("ELE_LABEL_POLICYNUMBER", "//span[@id='workbenchForm:workbenchTabs:policyNumber']");
                 LOGGER.info("Executed Step = STORE,var_LabelPolNumber,ELE_LABEL_POLICYNUMBER,TGTYPESCREENREG");
        SWIPE.$("UP","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SHOWLINKS","//span[@class='ui-button-icon-left ui-icon ui-c fa fa-ellipsis-v fa-2x']","TGTYPESCREENREG");

        WAIT.$("ELE_LINK_OWNERSADDRESSES","//span[contains(text(),\"Owner's Addresses\")]","VISIBLE","TGTYPESCREENREG");

        TAP.$("ELE_LINK_OWNERSADDRESSES","//span[contains(text(),\"Owner's Addresses\")]","TGTYPESCREENREG");

        WAIT.$(4,"TGTYPESCREENREG");

        WAIT.$("ELE_LINK_ADDRESSINFORMATION_FIRSTLINE","//span[@id='workbenchForm:workbenchTabs:grid:0:address']","VISIBLE","TGTYPESCREENREG");

        TAP.$("ELE_LINK_ADDRESSINFORMATION_FIRSTLINE","//span[@id='workbenchForm:workbenchTabs:grid:0:address']","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_EDIT","//span[text()='Edit']","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_CITY","//input[@id='workbenchForm:workbenchTabs:addressCity']","Greenville","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_DRPDWN_ADDRESSSTATE","//label[@id='workbenchForm:workbenchTabs:countryAndStateCode_label']","TGTYPESCREENREG");

        TAP.$("ELE_SELECTSTATE_SOUTHCAROLINA","//li[@id='workbenchForm:workbenchTabs:countryAndStateCode_49']","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_ZIPCODE","//input[@id='workbenchForm:workbenchTabs:zipCode']","29601","FALSE","TGTYPESCREENREG");

		takeFullScreenShot();
                 LOGGER.info("Executed Step = TAKEFULLSCREENSHOT,TGTYPESCREENREG");
        TAP.$("ELE_BUTTON_VALIDATE","//span[contains(text(),'Validate')]","TGTYPESCREENREG");

        ASSERT.$("ELE_MSG_VALIDATIONSUCCESS","//div[@id='workbenchForm:workbenchTabs:messages']//span[@class='ui-messages-info-summary'][contains(text(),'Validation completed successfully with no errors.')]","VISIBLE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SAVE","//span[contains(text(),'Save')]","TGTYPESCREENREG");

        ASSERT.$("ELE_MSG_ITEMSUCCESSUPDATE","//div[@id='workbenchForm:workbenchTabs:messages']//span[@class='ui-messages-info-summary'][contains(text(),'Item successfully updated.')]","VISIBLE","TGTYPESCREENREG");

		takeFullScreenShot();
                 LOGGER.info("Executed Step = TAKEFULLSCREENSHOT,TGTYPESCREENREG");
        TAP.$("ELE_TAB_ENTERPOLICYNUMBER","//a[contains(text(),'Enter Policy Number')]","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_CONTINUE","//span[contains(text(),'Continue')]","TGTYPESCREENREG");

		takeFullScreenShot();
                 LOGGER.info("Executed Step = TAKEFULLSCREENSHOT,TGTYPESCREENREG");
        WAIT.$("ELE_BUTTON_SHOWLINKS","//span[@class='ui-button-icon-left ui-icon ui-c fa fa-ellipsis-v fa-2x']","VISIBLE","TGTYPESCREENREG");

        SWIPE.$("UP","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SHOWLINKS","//span[@class='ui-button-icon-left ui-icon ui-c fa fa-ellipsis-v fa-2x']","TGTYPESCREENREG");

        WAIT.$("ELE_LINK_OWNERSADDRESSES","//span[contains(text(),\"Owner's Addresses\")]","VISIBLE","TGTYPESCREENREG");

        TAP.$("ELE_LINK_OWNERSADDRESSES","//span[contains(text(),\"Owner's Addresses\")]","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        TAP.$("ELE_LINK_GIASHOME","//button[@id='headerForm:j_idt11']//span[@class='ui-button-text ui-c'][contains(text(),'GIAS')]","TGTYPESCREENREG");

        WAIT.$(4,"TGTYPESCREENREG");


    }


    @Test
    public void mm40regaddabeneficiary() {

        // No need to declare / print method name here, handled by TestNg listener
        // declare("mm40regaddabeneficiary");

        CALL.$("LoginToGIAS4","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENNO");

        TYPE.$("ELE_TXTBOX_USERNAME","//input[@id='guestForm:username']","Mano","FALSE","TGTYPESCREENNO");

        TYPE.$("ELE_TXTBOX_PASSWORD","//input[@id='guestForm:password']","Cnx12345!","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_LOGIN","//button[@id='guestForm:loginButton']","TGTYPESCREENNO");

        CALL.$("AddaBeneficiary","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_TASKSEARCH","//a[@id='workbenchForm:workbenchTabs:taskSearchContainer:tb_buttonSearch']//span[@class='fa fa-search fa-lg']","TGTYPESCREENREG");

		int var_Count = 0;
                 LOGGER.info("Executed Step = VAR,Integer,var_Count,0,TGTYPESCREENREG");
		com.jayway.jsonpath.ReadContext var_CSVData = getJsonData("http://10.1.104.229/json/20201020/djNsX9.json");
                 LOGGER.info("Executed Step = VAR,Json,var_CSVData,http://10.1.104.229/json/20201020/djNsX9.json,TGTYPESCREENREG");
		String var_PolicyNumber;
                 LOGGER.info("Executed Step = VAR,String,var_PolicyNumber,TGTYPESCREENREG");
		var_PolicyNumber = getJsonData(var_CSVData , "$.records["+var_Count+"].PolicyNumber");
                 LOGGER.info("Executed Step = STORE,var_PolicyNumber,var_CSVData,$.records[{var_Count}].PolicyNumber,TGTYPESCREENREG");
		String var_SessionDate;
                 LOGGER.info("Executed Step = VAR,String,var_SessionDate,TGTYPESCREENREG");
		var_SessionDate = getJsonData(var_CSVData , "$.records.["+var_Count+"].SessionDate");
                 LOGGER.info("Executed Step = STORE,var_SessionDate,var_CSVData,$.records.[{var_Count}].SessionDate,TGTYPESCREENREG");
		String var_FirstName;
                 LOGGER.info("Executed Step = VAR,String,var_FirstName,TGTYPESCREENREG");
		var_FirstName = getJsonData(var_CSVData , "$.records.["+var_Count+"].FirstName");
                 LOGGER.info("Executed Step = STORE,var_FirstName,var_CSVData,$.records.[{var_Count}].FirstName,TGTYPESCREENREG");
		String var_LastName;
                 LOGGER.info("Executed Step = VAR,String,var_LastName,TGTYPESCREENREG");
		var_LastName = getJsonData(var_CSVData , "$.records.["+var_Count+"].LastName");
                 LOGGER.info("Executed Step = STORE,var_LastName,var_CSVData,$.records.[{var_Count}].LastName,TGTYPESCREENREG");
		String var_DOB;
                 LOGGER.info("Executed Step = VAR,String,var_DOB,TGTYPESCREENREG");
		var_DOB = getJsonData(var_CSVData , "$.records.["+var_Count+"].DOB");
                 LOGGER.info("Executed Step = STORE,var_DOB,var_CSVData,$.records.[{var_Count}].DOB,TGTYPESCREENREG");
		String var_SSN;
                 LOGGER.info("Executed Step = VAR,String,var_SSN,TGTYPESCREENREG");
		var_SSN = getJsonData(var_CSVData , "$.records.["+var_Count+"].SSN");
                 LOGGER.info("Executed Step = STORE,var_SSN,var_CSVData,$.records.[{var_Count}].SSN,TGTYPESCREENREG");
        TAP.$("ELE_BUTTON_SESSIONDATE","//a[@id='headerForm:sessionDatelink']","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_NEWSESSIONDATE","//input[@id='sessionDateForm:sessionDateInput_input']",var_SessionDate,"FALSE","TGTYPESCREENREG");

        TAP.$("ELE_CHANGEDATE_BUTTON","//span[contains(text(),'Change Date')]","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_SEARCHTASKS","//input[@id='workbenchForm:workbenchTabs:taskSearchContainer:taskSearchSearch']","Policy Inquiry","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_TASKSEARCH","//a[@id='workbenchForm:workbenchTabs:taskSearchContainer:tb_buttonSearch']//span[@class='fa fa-search fa-lg']","TGTYPESCREENREG");

        TAP.$("ELE_LINK_POLICYINQUIRY","//span[contains(text(),'Policy Inquiry')]","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_POLICYNUMBER","//input[@id='workbenchForm:workbenchTabs:policyNumber']",var_PolicyNumber,"FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_CONTINUE","//span[contains(text(),'Continue')]","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        WAIT.$("ELE_GETVAL_POLICYSTATUS","//span[@id='workbenchForm:workbenchTabs:statusText']","VISIBLE","TGTYPESCREENREG");

        ASSERT.$("ELE_GETVAL_POLICYSTATUS","//span[@id='workbenchForm:workbenchTabs:statusText']","=","Premium Paying","TGTYPESCREENREG");

		String var_LabelPolNumber;
                 LOGGER.info("Executed Step = VAR,String,var_LabelPolNumber,TGTYPESCREENREG");
		var_LabelPolNumber = ActionWrapper.getElementValue("ELE_LABEL_POLICYNUMBER", "//span[@id='workbenchForm:workbenchTabs:policyNumber']");
                 LOGGER.info("Executed Step = STORE,var_LabelPolNumber,ELE_LABEL_POLICYNUMBER,TGTYPESCREENREG");
		takeFullScreenShot();
                 LOGGER.info("Executed Step = TAKEFULLSCREENSHOT,TGTYPESCREENREG");
        TAP.$("ELE_BUTTON_SHOWLINKS","//span[@class='ui-button-icon-left ui-icon ui-c fa fa-ellipsis-v fa-2x']","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_CLIENTS","//span[contains(text(),'Clients')]","TGTYPESCREENREG");

        WAIT.$("ELE_BUTTON_ADD","//button[@id='workbenchForm:workbenchTabs:tb_buttonAdd']//span[@class='ui-button-text ui-c'][contains(text(),'Add')]","VISIBLE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_ADD","//button[@id='workbenchForm:workbenchTabs:tb_buttonAdd']//span[@class='ui-button-text ui-c'][contains(text(),'Add')]","TGTYPESCREENREG");

        TAP.$("ELE_DRPDWN_RELATIONSHIPTYPE","//label[@id='workbenchForm:workbenchTabs:userDefnRelaType_label']","TGTYPESCREENREG");

        TAP.$("ELE_DROPDWNSELECT_BENEFICIARY","//li[@id='workbenchForm:workbenchTabs:userDefnRelaType_5']","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_RELATIONSHIP_PERCENTAGE","//input[@id='workbenchForm:workbenchTabs:sharePercentage_input']","100","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SHOWLINKS","//span[@class='ui-button-icon-left ui-icon ui-c fa fa-ellipsis-v fa-2x']","TGTYPESCREENREG");

        TAP.$("ELE_LINK_SELECTCLIENT","//span[contains(text(),'Select Client')]","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_ADDCLIENT","//button[@id='workbenchForm:workbenchTabs:button_add']","TGTYPESCREENREG");

        WAIT.$("ELE_TXTBOX_FIRSTNAME","//input[@id='workbenchForm:workbenchTabs:nameFirst']","VISIBLE","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_FIRSTNAME","//input[@id='workbenchForm:workbenchTabs:nameFirst']",var_FirstName,"FALSE","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_LASTNAME","//input[@id='workbenchForm:workbenchTabs:nameLast']",var_LastName,"FALSE","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_DOB","//input[@id='workbenchForm:workbenchTabs:dateOfBirth_input']",var_DOB,"FALSE","TGTYPESCREENREG");

        TAP.$("ELE_DRPDWN_IDTYPE","//label[@id='workbenchForm:workbenchTabs:taxIdenUsag_label']","TGTYPESCREENREG");

        TAP.$("ELE_IDTYPESELECT_SSN","//li[@id='workbenchForm:workbenchTabs:taxIdenUsag_2']","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_IDNO","//input[@id='workbenchForm:workbenchTabs:identificationNumber']",var_SSN,"FALSE","TGTYPESCREENREG");

		takeFullScreenShot();
                 LOGGER.info("Executed Step = TAKEFULLSCREENSHOT,TGTYPESCREENREG");
        TAP.$("ELE_BUTTON_VALIDATE","//span[contains(text(),'Validate')]","TGTYPESCREENREG");

        ASSERT.$("ELE_MSG_VALIDATIONSUCCESS","//div[@id='workbenchForm:workbenchTabs:messages']//span[@class='ui-messages-info-summary'][contains(text(),'Validation completed successfully with no errors.')]","VISIBLE","TGTYPESCREENREG");

        WAIT.$(3,"TGTYPESCREENREG");

        WAIT.$("ELE_BUTTON_SAVE","//span[contains(text(),'Save')]","VISIBLE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SAVE","//span[contains(text(),'Save')]","TGTYPESCREENREG");

        WAIT.$("ELE_TXTBOX_ADDRESSLINE1","//input[@id='workbenchForm:workbenchTabs:addressLineOne']","VISIBLE","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_ADDRESSLINE1","//input[@id='workbenchForm:workbenchTabs:addressLineOne']","DO NOT MAIL","FALSE","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_CITY","//input[@id='workbenchForm:workbenchTabs:addressCity']","Greenville","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_DRPDWN_ISSUESTATECOUNTRY","//label[@id='workbenchForm:workbenchTabs:countryAndStateCode_label']","TGTYPESCREENREG");

        TAP.$("ELE_SELECTSTATE_SOUTHCAROLINA","//li[@id='workbenchForm:workbenchTabs:countryAndStateCode_49']","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_ZIPCODE","//input[@id='workbenchForm:workbenchTabs:zipCode']","29601","FALSE","TGTYPESCREENREG");

		takeFullScreenShot();
                 LOGGER.info("Executed Step = TAKEFULLSCREENSHOT,TGTYPESCREENREG");
        TAP.$("ELE_BUTTON_VALIDATE","//span[contains(text(),'Validate')]","TGTYPESCREENREG");

        ASSERT.$("ELE_MSG_VALIDATIONSUCCESS","//div[@id='workbenchForm:workbenchTabs:messages']//span[@class='ui-messages-info-summary'][contains(text(),'Validation completed successfully with no errors.')]","VISIBLE","TGTYPESCREENREG");

        WAIT.$(4,"TGTYPESCREENREG");

        WAIT.$("ELE_BUTTON_SAVE","//span[contains(text(),'Save')]","VISIBLE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SAVE","//span[contains(text(),'Save')]","TGTYPESCREENREG");

        WAIT.$("ELE_BUTTON_CONTINUE","//span[contains(text(),'Continue')]","VISIBLE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_CONTINUE","//span[contains(text(),'Continue')]","TGTYPESCREENREG");

        WAIT.$("ELE_BUTTON_VALIDATE","//span[contains(text(),'Validate')]","VISIBLE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_VALIDATE","//span[contains(text(),'Validate')]","TGTYPESCREENREG");

        ASSERT.$("ELE_BUTTON_VALIDATE","//span[contains(text(),'Validate')]","VISIBLE","TGTYPESCREENREG");

        WAIT.$(4,"TGTYPESCREENREG");

        WAIT.$("ELE_BUTTON_SAVE","//span[contains(text(),'Save')]","VISIBLE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SAVE","//span[contains(text(),'Save')]","TGTYPESCREENREG");

        WAIT.$(4,"TGTYPESCREENREG");

        TAP.$("ELE_TAB_ENTERPOLICYNUMBER","//a[contains(text(),'Enter Policy Number')]","TGTYPESCREENREG");

        WAIT.$("ELE_BUTTON_CONTINUE","//span[contains(text(),'Continue')]","VISIBLE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_CONTINUE","//span[contains(text(),'Continue')]","TGTYPESCREENREG");

        WAIT.$("ELE_GETVAL_POLICYSTATUS","//span[@id='workbenchForm:workbenchTabs:statusText']","VISIBLE","TGTYPESCREENREG");

		takeFullScreenShot();
                 LOGGER.info("Executed Step = TAKEFULLSCREENSHOT,TGTYPESCREENREG");
        TAP.$("ELE_BUTTON_SHOWLINKS","//span[@class='ui-button-icon-left ui-icon ui-c fa fa-ellipsis-v fa-2x']","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_CLIENTS","//span[contains(text(),'Clients')]","TGTYPESCREENREG");

        WAIT.$("ELE_LABEL_FIRST_RELATIONSHIPTYPE","//span[@id='workbenchForm:workbenchTabs:grid:0:userDefnRelaType']","VISIBLE","TGTYPESCREENREG");

        TAP.$("ELE_LABEL_FIRST_RELATIONSHIPTYPE","//span[@id='workbenchForm:workbenchTabs:grid:0:userDefnRelaType']","TGTYPESCREENREG");

        WAIT.$(4,"TGTYPESCREENREG");

        TAP.$("ELE_LINK_GIASHOME","//button[@id='headerForm:j_idt11']//span[@class='ui-button-text ui-c'][contains(text(),'GIAS')]","TGTYPESCREENREG");

        WAIT.$(3,"TGTYPESCREENREG");


    }


    @Test
    public void mm40regdividentchange() {

        // No need to declare / print method name here, handled by TestNg listener
        // declare("mm40regdividentchange");

        CALL.$("LoginToGIAS4","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENNO");

        TYPE.$("ELE_TXTBOX_USERNAME","//input[@id='guestForm:username']","Mano","FALSE","TGTYPESCREENNO");

        TYPE.$("ELE_TXTBOX_PASSWORD","//input[@id='guestForm:password']","Cnx12345!","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_LOGIN","//button[@id='guestForm:loginButton']","TGTYPESCREENNO");

        CALL.$("DividentOptionChange","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_TASKSEARCH","//a[@id='workbenchForm:workbenchTabs:taskSearchContainer:tb_buttonSearch']//span[@class='fa fa-search fa-lg']","TGTYPESCREENREG");

		int var_Count = 0;
                 LOGGER.info("Executed Step = VAR,Integer,var_Count,0,TGTYPESCREENREG");
		com.jayway.jsonpath.ReadContext var_CSVData = getJsonData("http://10.1.104.229/json/20201020/YHyY5K.json");
                 LOGGER.info("Executed Step = VAR,Json,var_CSVData,http://10.1.104.229/json/20201020/YHyY5K.json,TGTYPESCREENREG");
		String var_PolicyNumber;
                 LOGGER.info("Executed Step = VAR,String,var_PolicyNumber,TGTYPESCREENREG");
		var_PolicyNumber = getJsonData(var_CSVData , "$.records["+var_Count+"].PolicyNumber");
                 LOGGER.info("Executed Step = STORE,var_PolicyNumber,var_CSVData,$.records[{var_Count}].PolicyNumber,TGTYPESCREENREG");
		String var_SessionDate;
                 LOGGER.info("Executed Step = VAR,String,var_SessionDate,TGTYPESCREENREG");
		var_SessionDate = getJsonData(var_CSVData , "$.records.["+var_Count+"].SessionDate");
                 LOGGER.info("Executed Step = STORE,var_SessionDate,var_CSVData,$.records.[{var_Count}].SessionDate,TGTYPESCREENREG");
        TAP.$("ELE_BUTTON_SESSIONDATE","//a[@id='headerForm:sessionDatelink']","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_NEWSESSIONDATE","//input[@id='sessionDateForm:sessionDateInput_input']",var_SessionDate,"FALSE","TGTYPESCREENREG");

        TAP.$("ELE_CHANGEDATE_BUTTON","//span[contains(text(),'Change Date')]","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_SEARCHTASKS","//input[@id='workbenchForm:workbenchTabs:taskSearchContainer:taskSearchSearch']","Policy Inquiry","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_TASKSEARCH","//a[@id='workbenchForm:workbenchTabs:taskSearchContainer:tb_buttonSearch']//span[@class='fa fa-search fa-lg']","TGTYPESCREENREG");

        TAP.$("ELE_LINK_POLICYINQUIRY","//span[contains(text(),'Policy Inquiry')]","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_POLICYNUMBER","//input[@id='workbenchForm:workbenchTabs:policyNumber']",var_PolicyNumber,"FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_CONTINUE","//span[contains(text(),'Continue')]","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        WAIT.$("ELE_GETVAL_POLICYSTATUS","//span[@id='workbenchForm:workbenchTabs:statusText']","VISIBLE","TGTYPESCREENREG");

        ASSERT.$("ELE_GETVAL_POLICYSTATUS","//span[@id='workbenchForm:workbenchTabs:statusText']","=","Premium Paying","TGTYPESCREENREG");

		String var_LabelPolNumber;
                 LOGGER.info("Executed Step = VAR,String,var_LabelPolNumber,TGTYPESCREENREG");
		var_LabelPolNumber = ActionWrapper.getElementValue("ELE_LABEL_POLICYNUMBER", "//span[@id='workbenchForm:workbenchTabs:policyNumber']");
                 LOGGER.info("Executed Step = STORE,var_LabelPolNumber,ELE_LABEL_POLICYNUMBER,TGTYPESCREENREG");
        WAIT.$(3,"TGTYPESCREENREG");

        TAP.$("ELE_LOGO_GIAS1","//span[contains(text(),'GIAS')]","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_TASKSEARCH","//a[@id='workbenchForm:workbenchTabs:taskSearchContainer:tb_buttonSearch']//span[@class='fa fa-search fa-lg']","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_SEARCHTASKS","//input[@id='workbenchForm:workbenchTabs:taskSearchContainer:taskSearchSearch']","Change a Policy Contract","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_TASKSEARCH","//a[@id='workbenchForm:workbenchTabs:taskSearchContainer:tb_buttonSearch']//span[@class='fa fa-search fa-lg']","TGTYPESCREENREG");

        TAP.$("ELE_LINK_CHANGEPOLICYCONTRACT","//span[contains(text(),'Change a Policy Contract')]","TGTYPESCREENREG");

        WAIT.$(3,"TGTYPESCREENREG");

        WAIT.$("ELE_TXTBOX_POLICYNUMBER","//input[@id='workbenchForm:workbenchTabs:policyNumber']","VISIBLE","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_POLICYNUMBER","//input[@id='workbenchForm:workbenchTabs:policyNumber']",var_PolicyNumber,"FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_CONTINUE","//span[contains(text(),'Continue')]","TGTYPESCREENREG");

        TAP.$("ELE_LABEL_PRIMARYDIVOPTIONS","//label[@id='workbenchForm:workbenchTabs:dividendOptionPrim_label']","TGTYPESCREENREG");

        TAP.$("ELE_DROPDWNSELECT_PAIDINCASH","//li[@id='workbenchForm:workbenchTabs:dividendOptionPrim_2']","TGTYPESCREENREG");

		takeFullScreenShot();
                 LOGGER.info("Executed Step = TAKEFULLSCREENSHOT,TGTYPESCREENREG");
        TAP.$("ELE_BUTTON_VALIDATE","//span[contains(text(),'Validate')]","TGTYPESCREENREG");

        ASSERT.$("ELE_MSG_VALIDATIONSUCCESS","//div[@id='workbenchForm:workbenchTabs:messages']//span[@class='ui-messages-info-summary'][contains(text(),'Validation completed successfully with no errors.')]","VISIBLE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SUBMIT","//span[contains(text(),'Submit')]","TGTYPESCREENREG");

        WAIT.$(3,"TGTYPESCREENREG");

        TAP.$("ELE_LOGO_GIAS1","//span[contains(text(),'GIAS')]","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_SEARCHTASKS","//input[@id='workbenchForm:workbenchTabs:taskSearchContainer:taskSearchSearch']","policy inquiry","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_TASKSEARCH","//a[@id='workbenchForm:workbenchTabs:taskSearchContainer:tb_buttonSearch']//span[@class='fa fa-search fa-lg']","TGTYPESCREENREG");

        TAP.$("ELE_LINK_POLICYINQUIRY","//span[contains(text(),'Policy Inquiry')]","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_POLICYNUMBER","//input[@id='workbenchForm:workbenchTabs:policyNumber']",var_PolicyNumber,"FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_CONTINUE","//span[contains(text(),'Continue')]","TGTYPESCREENREG");

        WAIT.$("ELE_BUTTON_SHOWLINKS","//span[@class='ui-button-icon-left ui-icon ui-c fa fa-ellipsis-v fa-2x']","VISIBLE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SHOWLINKS","//span[@class='ui-button-icon-left ui-icon ui-c fa fa-ellipsis-v fa-2x']","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_CLIENTTRANSACTIONHISTORY","//span[contains(text(),'Transaction History')]","TGTYPESCREENREG");

		takeFullScreenShot();
                 LOGGER.info("Executed Step = TAKEFULLSCREENSHOT,TGTYPESCREENREG");
        WAIT.$(3,"TGTYPESCREENREG");

        TAP.$("ELE_LOGO_GIAS1","//span[contains(text(),'GIAS')]","TGTYPESCREENREG");

        WAIT.$(3,"TGTYPESCREENREG");


    }


}
