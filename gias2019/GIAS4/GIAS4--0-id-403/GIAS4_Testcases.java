package appium.tgo;

import com.tg.octopus.base.TGOWrapper;
import com.tg.octopus.libs.testng.listener.TestListener;
import static com.tg.octopus.base.TGOWrapper.Action.*;
import static com.tg.octopus.base.TGOWrapper.LOGGER;
import com.tg.octopus.wrapper.ActionWrapper;

import com.jayway.jsonpath.JsonPath;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.SessionNotCreatedException;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.Augmenter;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.remote.UnreachableBrowserException;

import org.testng.Assert;
import org.testng.ITestContext;
import org.testng.ITestResult;
import org.testng.Reporter;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;
import java.net.HttpURLConnection;
import java.net.URL;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import org.json.JSONArray;
import org.json.JSONObject;
import helper.WebTestUtility;
import java.sql.*;
import com.testautomationguru.utility.PDFUtil;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.multipdf.Splitter;

import java.util.*;
import org.openqa.selenium.*;
import java.io.*;
import java.net.*;

import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;


@Listeners(TestListener.class)
public class GIAS4_Testcases extends TGOWrapper {



    static {
       needsresetoncebeforetest ="false";
       needsresetbeforeeverymethod ="true";
       captureverystepsnap ="true";
       appendlastsnap ="false";
       ishybridapp ="true";

    }


    @Test
    public void tc01createpolicy() {

        // No need to declare / print method name here, handled by TestNg listener
        // declare("tc01createpolicy");

        CALL.$("LoginToGIAS4","TGTYPESCREENREG");

        TYPE.$("ELE_USERNAMETEXTFIELD","//input[@id='guestForm:username']","x5823","FALSE","TGTYPESCREENREG");

        TYPE.$("ELE_PASSWORDTEXTFIELD","//input[@id='guestForm:password']","Welcome123$","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_LOGINBUTTON","//input[@id='guestForm:loginButton']","TGTYPESCREENREG");

		String var_dataToPass;
                 LOGGER.info("Executed Step = VAR,String,var_dataToPass,TGTYPESCREENREG");
        CALL.$("AddDistributionChannel","TGTYPESCREENREG");

		com.jayway.jsonpath.ReadContext var_CSVData = getJsonData("C:/GIAS Automation/4.0 BaseRegression/InputData/4_0_CreateAPolicy.csv");
                 LOGGER.info("Executed Step = VAR,Json,var_CSVData,C:/GIAS Automation/4.0 BaseRegression/InputData/4_0_CreateAPolicy.csv,TGTYPESCREENREG");
		String var_DistributionChannelDescription;
                 LOGGER.info("Executed Step = VAR,String,var_DistributionChannelDescription,TGTYPESCREENREG");
		String var_DistributionChannelName;
                 LOGGER.info("Executed Step = VAR,String,var_DistributionChannelName,TGTYPESCREENREG");
		var_DistributionChannelName = getJsonData(var_CSVData , "$.records["+var_Count+"].DistributionChannelName");
                 LOGGER.info("Executed Step = STORE,var_DistributionChannelName,var_CSVData,$.records[{var_Count}].DistributionChannelName,TGTYPESCREENREG");
		var_DistributionChannelDescription = getJsonData(var_CSVData , "$.records["+var_Count+"].DistributionChannelDescription");
                 LOGGER.info("Executed Step = STORE,var_DistributionChannelDescription,var_CSVData,$.records[{var_Count}].DistributionChannelDescription,TGTYPESCREENREG");
        TAP.$("ELE_DASHBOARDPAGENUMBERTWO","//div[@id='workbenchTabs:tabForm0:menuboardContainer_paginator_top']//a[@class='ui-paginator-page ui-state-default ui-corner-all'][contains(text(),'2')]","TGTYPESCREENREG");

        TAP.$("ELE_DISTRIBUTIONCHANNELLINK","//span[contains(text(),'Distribution Channel')]","TGTYPESCREENREG");

        TAP.$("ELE_DISTRIBUTIONCHANNELTAB","//a[contains(text(),'Distribution Channel')]","TGTYPESCREENREG");

        TAP.$("ELE_ADDBUTTON","//span[contains(text(),'Add')]","TGTYPESCREENREG");

        TYPE.$("ELE_DISTRIBUTIONCHANNELNAME","//input[@id='workbenchTabs:tabForm2:column_panel_0:distributionChanName']",var_DistributionChannelName,"TRUE","TGTYPESCREENREG");

        TYPE.$("ELE_DISTRIBUTIONCHANNELDESCRIPTION","//input[@id='workbenchTabs:tabForm2:column_panel_0:channelDescription']",var_DistributionChannelDescription,"TRUE","TGTYPESCREENREG");

        TAP.$("ELE_VALIDATEBUTTON","//span[contains(text(),'Validate')]","TGTYPESCREENREG");

        TAP.$("ELE_SAVEBUTTON","//span[contains(text(),'Save')]","TGTYPESCREENREG");

        TAP.$("ELE_CLOSINGTAB1","span[class='ui-icon ui-icon-close']TGWEBCOMMACssSelectorTGWEBCOMMA0","TGTYPESCREENREG");

        TAP.$("ELE_CLOSINGTAB1","span[class='ui-icon ui-icon-close']TGWEBCOMMACssSelectorTGWEBCOMMA0","TGTYPESCREENREG");

        CALL.$("AddAgentRankCodes","TGTYPESCREENREG");

        TAP.$("ELE_MYDASHBOARDTAB","//a[contains(text(),'My Dashboard')]","TGTYPESCREENREG");

        TAP.$("ELE_AGENTRANKCODESLINK","//span[contains(text(),'Agent Rank Codes')]","TGTYPESCREENREG");

        TAP.$("ELE_AGENTRANKCODESTAB","//a[contains(text(),'Agent Rank Codes')]","TGTYPESCREENREG");

        TAP.$("ELE_ADDBUTTON","//span[contains(text(),'Add')]","TGTYPESCREENREG");

		String var_RankDescription;
                 LOGGER.info("Executed Step = VAR,String,var_RankDescription,TGTYPESCREENREG");
		String var_RankCode;
                 LOGGER.info("Executed Step = VAR,String,var_RankCode,TGTYPESCREENREG");
		var_dataToPass = getJsonData(var_CSVData , "$.records["+var_Count+"].DistributionChannelDescription");
                 LOGGER.info("Executed Step = STORE,var_dataToPass,var_CSVData,$.records[{var_Count}].DistributionChannelDescription,TGTYPESCREENREG");
		var_RankCode = getJsonData(var_CSVData , "$.records["+var_Count+"].AgencyRankCode");
                 LOGGER.info("Executed Step = STORE,var_RankCode,var_CSVData,$.records[{var_Count}].AgencyRankCode,TGTYPESCREENREG");
		var_RankDescription = getJsonData(var_CSVData , "$.records["+var_Count+"].AgencyRankDescription");
                 LOGGER.info("Executed Step = STORE,var_RankDescription,var_CSVData,$.records[{var_Count}].AgencyRankDescription,TGTYPESCREENREG");
        TAP.$("ELE_RANKCODEDISTRIBUTIONCHANNELDRPDWN","//label[@id='workbenchTabs:tabForm2:column_panel_0:distributionChannel_label']","TGTYPESCREENREG");

		try { 
		 WebElement selectValueOfDropDown= driver.findElement(By.xpath("//ul[@id='workbenchTabs:tabForm2:column_panel_0:distributionChannel_items']//li[@data-label='"+ var_dataToPass +"']"));
				selectValueOfDropDown.click();
		             Thread.sleep(1000);
		 
		 ;} 
		 catch(Exception e) { PrintVal("Renderer Script Exception : "+e.toString()); } 
		 PrintVal("Executed Step = CALLRENDERERSCRIPT,SELECTDISTRIBUTIONCHANNELDROPDOWN"); 
        TYPE.$("ELE_RANKLEVELTEXTFIELD","//input[@id='workbenchTabs:tabForm2:column_panel_0:agencyRankLevel_input']","1","FALSE","TGTYPESCREENREG");

        TYPE.$("ELE_RANKCODETEXTFIELD","//input[@id='workbenchTabs:tabForm2:column_panel_0:agencyRankCode']",var_RankCode,"FALSE","TGTYPESCREENREG");

        TYPE.$("ELE_RANKDESCRIPTIONTEXTFIELD","//input[@id='workbenchTabs:tabForm2:column_panel_0:description']",var_RankDescription,"FALSE","TGTYPESCREENREG");

        TAP.$("ELE_VALIDATEBUTTON","//span[contains(text(),'Validate')]","TGTYPESCREENREG");

        TAP.$("ELE_SAVEBUTTON","//span[contains(text(),'Save')]","TGTYPESCREENREG");

        TAP.$("ELE_CLOSINGTAB1","span[class='ui-icon ui-icon-close']TGWEBCOMMACssSelectorTGWEBCOMMA0","TGTYPESCREENREG");

        TAP.$("ELE_CLOSINGTAB1","span[class='ui-icon ui-icon-close']TGWEBCOMMACssSelectorTGWEBCOMMA0","TGTYPESCREENREG");

        CALL.$("AddCommissionScheduleRates","TGTYPESCREENREG");

        TAP.$("ELE_DASHBOARDPAGENUMBERTWO","//div[@id='workbenchTabs:tabForm0:menuboardContainer_paginator_top']//a[@class='ui-paginator-page ui-state-default ui-corner-all'][contains(text(),'2')]","TGTYPESCREENREG");

        TAP.$("ELE_COMMISSIONSCHEDULERATESLINK","//span[contains(text(),'Commission Schedule Rates')]","TGTYPESCREENREG");

        TAP.$("ELE_COMMISSIONSCHEDULETAB","//a[contains(text(),'Commission Schedule')]","TGTYPESCREENREG");

        TAP.$("ELE_ADDBUTTON","//span[contains(text(),'Add')]","TGTYPESCREENREG");

		String var_CommissionDescription;
                 LOGGER.info("Executed Step = VAR,String,var_CommissionDescription,TGTYPESCREENREG");
		String var_CommissionTable;
                 LOGGER.info("Executed Step = VAR,String,var_CommissionTable,TGTYPESCREENREG");
		var_CommissionTable = getJsonData(var_CSVData , "$.records["+var_Count+"].TableName");
                 LOGGER.info("Executed Step = STORE,var_CommissionTable,var_CSVData,$.records[{var_Count}].TableName,TGTYPESCREENREG");
		var_CommissionDescription = getJsonData(var_CSVData , "$.records["+var_Count+"].TableDescription");
                 LOGGER.info("Executed Step = STORE,var_CommissionDescription,var_CSVData,$.records[{var_Count}].TableDescription,TGTYPESCREENREG");
        TYPE.$("ELE_COMMISSIONSCHEDULERATESTABLENAME","//input[@id='workbenchTabs:tabForm2:column_panel_0:tableName']",var_CommissionTable,"FALSE","TGTYPESCREENREG");

        TYPE.$("ELE_COMMISSIONSCHEDULERATESDESCRIPTION","//input[@id='workbenchTabs:tabForm2:column_panel_0:description']",var_CommissionDescription,"FALSE","TGTYPESCREENREG");

		var_dataToPass = getJsonData(var_CSVData , "$.records["+var_Count+"].DistributionChannelDescription");
                 LOGGER.info("Executed Step = STORE,var_dataToPass,var_CSVData,$.records[{var_Count}].DistributionChannelDescription,TGTYPESCREENREG");
        TAP.$("ELE_RANKCODEDISTRIBUTIONCHANNELDRPDWN","//label[@id='workbenchTabs:tabForm2:column_panel_0:distributionChannel_label']","TGTYPESCREENREG");

		try { 
		 WebElement selectValueOfDropDown= driver.findElement(By.xpath("//ul[@id='workbenchTabs:tabForm2:column_panel_0:distributionChannel_items']//li[@data-label='"+ var_dataToPass +"']"));
				selectValueOfDropDown.click();
		             Thread.sleep(1000);
		 
		 ;} 
		 catch(Exception e) { PrintVal("Renderer Script Exception : "+e.toString()); } 
		 PrintVal("Executed Step = CALLRENDERERSCRIPT,SELECTDISTRIBUTIONCHANNELDROPDOWN"); 
        TAP.$("ELE_CONTINUEBUTTON","//span[contains(text(),'Continue')]","TGTYPESCREENREG");

        TYPE.$("ELE_COMMSCHRATESDURATION1","//input[@id='workbenchTabs:tabForm2:rateTable:endingDuration1_input']","1","FALSE","TGTYPESCREENREG");

        TYPE.$("ELE_COMMSCHRATESDURATION2","//input[@id='workbenchTabs:tabForm2:rateTable:endingDuration2_input']","5","FALSE","TGTYPESCREENREG");

        TYPE.$("ELE_COMMSCHRATESDURATION3","//input[@id='workbenchTabs:tabForm2:rateTable:endingDuration3_input']","120","FALSE","TGTYPESCREENREG");

		var_dataToPass = getJsonData(var_CSVData , "$.records["+var_Count+"].AgencyRankDescription");
                 LOGGER.info("Executed Step = STORE,var_dataToPass,var_CSVData,$.records[{var_Count}].AgencyRankDescription,TGTYPESCREENREG");
        TAP.$("ELE_COMMISSIONRATEAGENCYGRID","//div[@id='workbenchTabs:tabForm2:rateTable']//tr[1]//td[1]","TGTYPESCREENREG");

        TAP.$("ELE_COMMISSIONRATEAGENCYGRID","//div[@id='workbenchTabs:tabForm2:rateTable']//tr[1]//td[1]","TGTYPESCREENREG");

        WAIT.$("ELE_COMMRATESAGENCYRANKCODEDRPDWN","//select[@id='workbenchTabs:tabForm2:rateTable:0:in_agencyRankCode_Col']","VISIBLE","TGTYPESCREENREG");

        SELECT.$("ELE_COMMRATESAGENCYRANKCODEDRPDWN","//select[@id='workbenchTabs:tabForm2:rateTable:0:in_agencyRankCode_Col']",var_dataToPass,"TGTYPESCREENREG");

        WAIT.$("ELE_COMMISSIONRATEGRID1","//div[@id='workbenchTabs:tabForm2:rateTable']//tr[1]//td[2]","VISIBLE","TGTYPESCREENREG");

        TAP.$("ELE_COMMISSIONRATEGRID1","//div[@id='workbenchTabs:tabForm2:rateTable']//tr[1]//td[2]","TGTYPESCREENREG");

		try { 
		 WebElement clearRateField = driver.findElement(By.cssSelector("input[type=text][name='workbenchTabs:tabForm2:rateTable:0:in_commissionRate1_input']"));

				clearRateField.click();
				 clearRateField.sendKeys(Keys.BACK_SPACE);
				 clearRateField.sendKeys(Keys.BACK_SPACE);
				 clearRateField.sendKeys(Keys.BACK_SPACE);
				 clearRateField.sendKeys(Keys.BACK_SPACE);
				 clearRateField.sendKeys(Keys.BACK_SPACE);
				 clearRateField.sendKeys(Keys.BACK_SPACE);
				 clearRateField.sendKeys(Keys.BACK_SPACE);
				 		 clearRateField.sendKeys("33");
		 
		 ;} 
		 catch(Exception e) { PrintVal("Renderer Script Exception : "+e.toString()); } 
		 PrintVal("Executed Step = CALLRENDERERSCRIPT,CLEARRATEFIELD1"); 
        WAIT.$("ELE_COMMISSIONRATEGRID2","//div[@id='workbenchTabs:tabForm2:rateTable']//tr[1]//td[3]","VISIBLE","TGTYPESCREENREG");

        TAP.$("ELE_COMMISSIONRATEGRID2","//div[@id='workbenchTabs:tabForm2:rateTable']//tr[1]//td[3]","TGTYPESCREENREG");

		try { 
		 WebElement clearRateField = driver.findElement(By.cssSelector("input[type=text][name='workbenchTabs:tabForm2:rateTable:0:in_commissionRate2_input']"));

				clearRateField.click();
				 clearRateField.sendKeys(Keys.BACK_SPACE);
				 clearRateField.sendKeys(Keys.BACK_SPACE);
				 clearRateField.sendKeys(Keys.BACK_SPACE);
				 clearRateField.sendKeys(Keys.BACK_SPACE);
				 clearRateField.sendKeys(Keys.BACK_SPACE);
				 clearRateField.sendKeys(Keys.BACK_SPACE);
				 clearRateField.sendKeys(Keys.BACK_SPACE);
				 		 clearRateField.sendKeys("22.22");
		 
		 ;} 
		 catch(Exception e) { PrintVal("Renderer Script Exception : "+e.toString()); } 
		 PrintVal("Executed Step = CALLRENDERERSCRIPT,CLEARRATEFIELD2"); 
        WAIT.$("ELE_COMMISSIONRATEGRID3","//div[@id='workbenchTabs:tabForm2:rateTable']//tr[1]//td[4]","VISIBLE","TGTYPESCREENREG");

        TAP.$("ELE_COMMISSIONRATEGRID3","//div[@id='workbenchTabs:tabForm2:rateTable']//tr[1]//td[4]","TGTYPESCREENREG");

		try { 
		  WebElement clearRateField = driver.findElement(By.cssSelector("input[type=text][name='workbenchTabs:tabForm2:rateTable:0:in_commissionRate3_input']"));

				clearRateField.click();
				 clearRateField.sendKeys(Keys.BACK_SPACE);
				 clearRateField.sendKeys(Keys.BACK_SPACE);
				 clearRateField.sendKeys(Keys.BACK_SPACE);
				 clearRateField.sendKeys(Keys.BACK_SPACE);
				 clearRateField.sendKeys(Keys.BACK_SPACE);
				 clearRateField.sendKeys(Keys.BACK_SPACE);
				 clearRateField.sendKeys(Keys.BACK_SPACE);
				 		 clearRateField.sendKeys("12.12");
		 
		 ;} 
		 catch(Exception e) { PrintVal("Renderer Script Exception : "+e.toString()); } 
		 PrintVal("Executed Step = CALLRENDERERSCRIPT,CLEARRATEFIELD3"); 
        TAP.$("ELE_COMMISSIONRATEGRID4","//div[@id='workbenchTabs:tabForm2:rateTable']//tr[1]//td[5]","TGTYPESCREENREG");

        WAIT.$(1,"TGTYPESCREENREG");

        TAP.$("ELE_VALIDATEBUTTON","//span[contains(text(),'Validate')]","TGTYPESCREENREG");

        WAIT.$("ELE_SAVEBUTTON","//span[contains(text(),'Save')]","VISIBLE","TGTYPESCREENREG");

        TAP.$("ELE_SAVEBUTTON","//span[contains(text(),'Save')]","TGTYPESCREENREG");

        WAIT.$(1,"TGTYPESCREENREG");

        TAP.$("ELE_CLOSINGTAB1","span[class='ui-icon ui-icon-close']TGWEBCOMMACssSelectorTGWEBCOMMA0","TGTYPESCREENREG");

        WAIT.$(1,"TGTYPESCREENREG");

        TAP.$("ELE_CLOSINGTAB1","span[class='ui-icon ui-icon-close']TGWEBCOMMACssSelectorTGWEBCOMMA0","TGTYPESCREENREG");

        CALL.$("AddCommissionContracts","TGTYPESCREENREG");

        TAP.$("ELE_MYDASHBOARDTAB","//a[contains(text(),'My Dashboard')]","TGTYPESCREENREG");

        TAP.$("ELE_DASHBOARDPAGENUMBERTWO","//div[@id='workbenchTabs:tabForm0:menuboardContainer_paginator_top']//a[@class='ui-paginator-page ui-state-default ui-corner-all'][contains(text(),'2')]","TGTYPESCREENREG");

        TAP.$("ELE_COMMISSIONCONTRACTSLINK","//span[contains(text(),'Commission Contracts')]","TGTYPESCREENREG");

        TAP.$("ELE_COMMISSIONCONTRACTSTAB","//a[contains(text(),'Commission Contracts')]","TGTYPESCREENREG");

        TAP.$("ELE_ADDBUTTON","//span[contains(text(),'Add')]","TGTYPESCREENREG");

		String var_contractNumber;
                 LOGGER.info("Executed Step = VAR,String,var_contractNumber,TGTYPESCREENREG");
		String var_contractDescription;
                 LOGGER.info("Executed Step = VAR,String,var_contractDescription,TGTYPESCREENREG");
		var_contractNumber = getJsonData(var_CSVData , "$.records["+var_Count+"].CommissionContractNumber");
                 LOGGER.info("Executed Step = STORE,var_contractNumber,var_CSVData,$.records[{var_Count}].CommissionContractNumber,TGTYPESCREENREG");
		var_contractDescription = getJsonData(var_CSVData , "$.records["+var_Count+"].CommissionContractDescription");
                 LOGGER.info("Executed Step = STORE,var_contractDescription,var_CSVData,$.records[{var_Count}].CommissionContractDescription,TGTYPESCREENREG");
        TYPE.$("ELE_COMMCONTRACTNUMBERTEXTFIELD","//input[@type='text'][@id='workbenchTabs:tabForm2:column_panel_0:commissionContNumb_input']",var_contractNumber,"FALSE","TGTYPESCREENREG");

        TYPE.$("ELE_COMMICONTRACTDESCRIPTIOTEXTFIELD","//input[@id='workbenchTabs:tabForm2:column_panel_0:longDescription']",var_contractDescription,"FALSE","TGTYPESCREENREG");

		var_dataToPass = getJsonData(var_CSVData , "$.records["+var_Count+"].DistributionChannelDescription");
                 LOGGER.info("Executed Step = STORE,var_dataToPass,var_CSVData,$.records[{var_Count}].DistributionChannelDescription,TGTYPESCREENREG");
        TAP.$("ELE_COMMCONTRACTDISTRIBUTIONCHANNELDRPDWN","//label[@id='workbenchTabs:tabForm2:column_panel_0:distributionChannel_label']","TGTYPESCREENREG");

		try { 
		 WebElement selectValueOfDropDown= driver.findElement(By.xpath("//ul[@id='workbenchTabs:tabForm2:column_panel_0:distributionChannel_items']//li[@data-label='"+ var_dataToPass +"']"));
				selectValueOfDropDown.click();
		             Thread.sleep(1000);
		 
		 ;} 
		 catch(Exception e) { PrintVal("Renderer Script Exception : "+e.toString()); } 
		 PrintVal("Executed Step = CALLRENDERERSCRIPT,SELECTDISTRIBUTIONCHANNELDROPDOWN"); 
        TAP.$("ELE_COMMISSIONCONTRACTEFFECTIVEDATETEXTFIELD","//input[@id='workbenchTabs:tabForm2:column_panel_0:effectiveDate_input']","TGTYPESCREENREG");

		try { 
		 WebElement chooseCurrentDate= driver.findElement(By.xpath("//div[@id='ui-datepicker-div']//a[@class='ui-state-default ui-state-highlight']"));
						chooseCurrentDate.click();
						Thread.sleep(1000);
		 
		 ;} 
		 catch(Exception e) { PrintVal("Renderer Script Exception : "+e.toString()); } 
		 PrintVal("Executed Step = CALLRENDERERSCRIPT,CHOOSECURRENTDATE"); 
		var_dataToPass = "100% of Commission Earned";
                 LOGGER.info("Executed Step = STORE,var_dataToPass,100% of Commission Earned,TGTYPESCREENREG");
        WAIT.$("ELE_COMMCONTRACTADVANCEPAYBACKMETHODDRPDWN","//label[@id='workbenchTabs:tabForm2:column_panel_1:advancePaybackMethod_label']","TGTYPESCREENREG");

        TAP.$("ELE_COMMCONTRACTADVANCEPAYBACKMETHODDRPDWN","//label[@id='workbenchTabs:tabForm2:column_panel_1:advancePaybackMethod_label']","TGTYPESCREENREG");

		try { 
		  WebElement selectValueOfDropDown= driver.findElement(By.xpath("//li[@data-label='"+ var_dataToPass +"']"));
		selectValueOfDropDown.click();
		 Thread.sleep(1000);
		 
		 ;} 
		 catch(Exception e) { PrintVal("Renderer Script Exception : "+e.toString()); } 
		 PrintVal("Executed Step = CALLRENDERERSCRIPT,SELECTDROPDOWNONLY"); 
		var_dataToPass = getJsonData(var_CSVData , "$.records["+var_Count+"].AgencyRankDescription");
                 LOGGER.info("Executed Step = STORE,var_dataToPass,var_CSVData,$.records[{var_Count}].AgencyRankDescription,TGTYPESCREENREG");
        WAIT.$("ELE_COMMCONTRACTADVANCETHROUGHAGENTRANKDRPDWN","//label[@id='workbenchTabs:tabForm2:column_panel_1:topLevelForAdvance_label']","VISIBLE","TGTYPESCREENREG");

        TAP.$("ELE_COMMCONTRACTADVANCETHROUGHAGENTRANKDRPDWN","//label[@id='workbenchTabs:tabForm2:column_panel_1:topLevelForAdvance_label']","TGTYPESCREENREG");

		try { 
		  WebElement selectValueOfDropDown= driver.findElement(By.xpath("//li[@data-label='"+ var_dataToPass +"']"));
		selectValueOfDropDown.click();
		 Thread.sleep(1000);
		 
		 ;} 
		 catch(Exception e) { PrintVal("Renderer Script Exception : "+e.toString()); } 
		 PrintVal("Executed Step = CALLRENDERERSCRIPT,SELECTDROPDOWNONLY"); 
        TAP.$("ELE_VALIDATEBUTTON","//span[contains(text(),'Validate')]","TGTYPESCREENREG");

        TAP.$("ELE_COMMISSIONCONTRACTEFFECTIVEDATETEXTFIELD","//input[@id='workbenchTabs:tabForm2:column_panel_0:effectiveDate_input']","TGTYPESCREENREG");

		try { 
		 WebElement chooseCurrentDate= driver.findElement(By.xpath("//div[@id='ui-datepicker-div']//a[@class='ui-state-default ui-state-highlight']"));
						chooseCurrentDate.click();
						Thread.sleep(1000);
		 
		 ;} 
		 catch(Exception e) { PrintVal("Renderer Script Exception : "+e.toString()); } 
		 PrintVal("Executed Step = CALLRENDERERSCRIPT,CHOOSECURRENTDATE"); 
        TAP.$("ELE_VALIDATEBUTTON","//span[contains(text(),'Validate')]","TGTYPESCREENREG");

        WAIT.$("ELE_SAVEBUTTON","//span[contains(text(),'Save')]","VISIBLE","TGTYPESCREENREG");

        TAP.$("ELE_SAVEBUTTON","//span[contains(text(),'Save')]","TGTYPESCREENREG");

        WAIT.$(1,"TGTYPESCREENREG");

        TAP.$("ELE_CLOSINGTAB1","span[class='ui-icon ui-icon-close']TGWEBCOMMACssSelectorTGWEBCOMMA0","TGTYPESCREENREG");

        WAIT.$(1,"TGTYPESCREENREG");

        TAP.$("ELE_CLOSINGTAB1","span[class='ui-icon ui-icon-close']TGWEBCOMMACssSelectorTGWEBCOMMA0","TGTYPESCREENREG");

        CALL.$("AddCommissionSchedule","TGTYPESCREENREG");

        TAP.$("ELE_MYDASHBOARDTAB","//a[contains(text(),'My Dashboard')]","TGTYPESCREENREG");

        TAP.$("ELE_DASHBOARDPAGENUMBERTWO","//div[@id='workbenchTabs:tabForm0:menuboardContainer_paginator_top']//a[@class='ui-paginator-page ui-state-default ui-corner-all'][contains(text(),'2')]","TGTYPESCREENREG");

        TAP.$("ELE_COMMISSIONCONTRACTSLINK","//span[contains(text(),'Commission Contracts')]","TGTYPESCREENREG");

		String var_ContractNumber;
                 LOGGER.info("Executed Step = VAR,String,var_ContractNumber,TGTYPESCREENREG");
		var_ContractNumber = getJsonData(var_CSVData , "$.records["+var_Count+"].CommissionContractNumber");
                 LOGGER.info("Executed Step = STORE,var_ContractNumber,var_CSVData,$.records[{var_Count}].CommissionContractNumber,TGTYPESCREENREG");
        TYPE.$("ELE_CONTRACTNUMBERCSS","input[type=text][class='ui-inputfield ui-inputtext ui-widget ui-state-default ui-corner-all']TGWEBCOMMACssSelectorTGWEBCOMMA1",var_ContractNumber,"FALSE","TGTYPESCREENREG");

        WAIT.$("ELE_COMMCONTRACTIDLINK","span[id='workbenchTabs:tabForm1:grid:0:commissionContNumb']TGWEBCOMMACssSelectorTGWEBCOMMA0","VISIBLE","TGTYPESCREENREG");

        TAP.$("ELE_COMMCONTRACTIDLINK","span[id='workbenchTabs:tabForm1:grid:0:commissionContNumb']TGWEBCOMMACssSelectorTGWEBCOMMA0","TGTYPESCREENREG");

        TAP.$("ELE_CREATEPOLICYSHOWLINK","//span[contains(text(),'Show Links')]","TGTYPESCREENREG");

        TAP.$("ELE_COMMISSIONSCHEDULELINK","//span[contains(text(),'Commission Schedule')]","TGTYPESCREENREG");

        TAP.$("ELE_COMMISSIONSCHEDULETAB","//a[contains(text(),'Commission Schedule')]","TGTYPESCREENREG");

        TAP.$("ELE_ADDBUTTON","//span[contains(text(),'Add')]","TGTYPESCREENREG");

		var_dataToPass = "Ordinary life";
                 LOGGER.info("Executed Step = STORE,var_dataToPass,Ordinary life,TGTYPESCREENREG");
        TAP.$("ELE_COMMCONTRACTTYPEDRPDWN","//label[@id='workbenchTabs:tabForm4:column_panel_0:commissionContType_label']","TGTYPESCREENREG");

		try { 
		  WebElement selectValueOfDropDown= driver.findElement(By.xpath("//li[@data-label='"+ var_dataToPass +"']"));
		selectValueOfDropDown.click();
		 Thread.sleep(1000);
		 
		 ;} 
		 catch(Exception e) { PrintVal("Renderer Script Exception : "+e.toString()); } 
		 PrintVal("Executed Step = CALLRENDERERSCRIPT,SELECTDROPDOWNONLY"); 
        TAP.$("ELE_COMMSCHEDULERATETABLENAMEDRPDWN","//label[@id='workbenchTabs:tabForm4:column_panel_0:commissionRateTableName_label']","TGTYPESCREENREG");

		var_dataToPass = getJsonData(var_CSVData , "$.records["+var_Count+"].TableDescription");
                 LOGGER.info("Executed Step = STORE,var_dataToPass,var_CSVData,$.records[{var_Count}].TableDescription,TGTYPESCREENREG");
		try { 
		  WebElement selectValueOfDropDown= driver.findElement(By.xpath("//li[@data-label='"+ var_dataToPass +"']"));
		selectValueOfDropDown.click();
		 Thread.sleep(1000);
		 
		 ;} 
		 catch(Exception e) { PrintVal("Renderer Script Exception : "+e.toString()); } 
		 PrintVal("Executed Step = CALLRENDERERSCRIPT,SELECTDROPDOWNONLY"); 
		String var_LineDescription;
                 LOGGER.info("Executed Step = VAR,String,var_LineDescription,TGTYPESCREENREG");
		var_LineDescription = getJsonData(var_CSVData , "$.records["+var_Count+"].LineDescription");
                 LOGGER.info("Executed Step = STORE,var_LineDescription,var_CSVData,$.records[{var_Count}].LineDescription,TGTYPESCREENREG");
        TAP.$("ELE_COMMSCHEDULELINEDESCRIPTIONTEXTFIELD","input[type=text][id='workbenchTabs:tabForm4:column_panel_0:shortDescription']TGWEBCOMMACssSelectorTGWEBCOMMA0","TGTYPESCREENREG");

        TYPE.$("ELE_COMMSCHEDULELINEDESCRIPTIONTEXTFIELD","input[type=text][id='workbenchTabs:tabForm4:column_panel_0:shortDescription']TGWEBCOMMACssSelectorTGWEBCOMMA0",var_LineDescription,"FALSE","TGTYPESCREENREG");

        TAP.$("ELE_VALIDATEBUTTON","//span[contains(text(),'Validate')]","TGTYPESCREENREG");

        TAP.$("ELE_COMMISSIONSCHEDULEEFFECTIVEDATE","//input[@id='workbenchTabs:tabForm4:column_panel_0:effectiveDate_input']","TGTYPESCREENREG");

		try { 
		 WebElement chooseCurrentDate= driver.findElement(By.xpath("//div[@id='ui-datepicker-div']//a[@class='ui-state-default ui-state-highlight']"));
						chooseCurrentDate.click();
						Thread.sleep(1000);
		 
		 ;} 
		 catch(Exception e) { PrintVal("Renderer Script Exception : "+e.toString()); } 
		 PrintVal("Executed Step = CALLRENDERERSCRIPT,CHOOSECURRENTDATE"); 
        TAP.$("ELE_VALIDATEBUTTON","//span[contains(text(),'Validate')]","TGTYPESCREENREG");

        TAP.$("ELE_SAVEBUTTON","//span[contains(text(),'Save')]","TGTYPESCREENREG");

        SWIPE.$("DOWN","TGTYPESCREENREG");

        TAP.$("ELE_CLOSINGTAB1","span[class='ui-icon ui-icon-close']TGWEBCOMMACssSelectorTGWEBCOMMA0","TGTYPESCREENREG");

        TAP.$("ELE_CLOSINGTAB1","span[class='ui-icon ui-icon-close']TGWEBCOMMACssSelectorTGWEBCOMMA0","TGTYPESCREENREG");

        TAP.$("ELE_CLOSINGTAB1","span[class='ui-icon ui-icon-close']TGWEBCOMMACssSelectorTGWEBCOMMA0","TGTYPESCREENREG");

        TAP.$("ELE_CLOSINGTAB1","span[class='ui-icon ui-icon-close']TGWEBCOMMACssSelectorTGWEBCOMMA0","TGTYPESCREENREG");

        CALL.$("AddPolicyContract","TGTYPESCREENREG");

        TAP.$("ELE_MYDASHBOARDTAB","//a[contains(text(),'My Dashboard')]","TGTYPESCREENREG");

        TAP.$("ELE_APPLICATIONENTRYUPDATLINK","//span[contains(text(),'Application Entry/Update')]","TGTYPESCREENREG");

        TAP.$("ELE_APPLICATIONENTRYUPDATETAB","//a[contains(text(),'Application Entry/Update')]","TGTYPESCREENREG");

		var_dataToPass = "Quarterly";
                 LOGGER.info("Executed Step = STORE,var_dataToPass,Quarterly,TGTYPESCREENREG");
		try { 
		  WebElement selectValueOfDropDown= driver.findElement(By.xpath("//li[@data-label='"+ var_dataToPass +"']"));
		selectValueOfDropDown.click();
		 Thread.sleep(1000);
		 
		 ;} 
		 catch(Exception e) { PrintVal("Renderer Script Exception : "+e.toString()); } 
		 PrintVal("Executed Step = CALLRENDERERSCRIPT,SELECTDROPDOWNONLY"); 
		String var_PolicyNumber;
                 LOGGER.info("Executed Step = VAR,String,var_PolicyNumber,TGTYPESCREENREG");
		var_PolicyNumber = getJsonData(var_CSVData , "$.records["+var_Count+"].PolicyNumber");
                 LOGGER.info("Executed Step = STORE,var_PolicyNumber,var_CSVData,$.records[{var_Count}].PolicyNumber,TGTYPESCREENREG");
		String var_ApplicationNumber;
                 LOGGER.info("Executed Step = VAR,String,var_ApplicationNumber,TGTYPESCREENREG");
		var_ApplicationNumber = getJsonData(var_CSVData , "$.records["+var_Count+"].PolicyNumber");
                 LOGGER.info("Executed Step = STORE,var_ApplicationNumber,var_CSVData,$.records[{var_Count}].PolicyNumber,TGTYPESCREENREG");
        TAP.$("ELE_ADDBUTTON","//span[contains(text(),'Add')]","TGTYPESCREENREG");

        TYPE.$("ELE_POLICYNUMBERTEXTFIELD","//input[@id='workbenchTabs:tabForm2:column_panel_0:policyNumber']",var_PolicyNumber,"FALSE","TGTYPESCREENREG");

        TAP.$("ELE_CREATEPOLICYPAYMENTFREQUENCYDRPDWN","//label[@id='workbenchTabs:tabForm2:column_panel_0:paymentMode_label']","TGTYPESCREENREG");

		try { 
		  WebElement selectValueOfDropDown= driver.findElement(By.xpath("//li[@data-label='"+ var_dataToPass +"']"));
		selectValueOfDropDown.click();
		 Thread.sleep(1000);
		 
		 ;} 
		 catch(Exception e) { PrintVal("Renderer Script Exception : "+e.toString()); } 
		 PrintVal("Executed Step = CALLRENDERERSCRIPT,SELECTDROPDOWNONLY"); 
        TYPE.$("ELE_CREATEPOLICYAPPLICATIONNUMBERTEXTFIELD","//input[@id='workbenchTabs:tabForm2:column_panel_1:applicationNumber']",var_ApplicationNumber,"FALSE","TGTYPESCREENREG");

        TAP.$("ELE_CREATEPOLICYCASHWITHAPPLICATIONTEXTFIELD","//input[@id='workbenchTabs:tabForm2:column_panel_1:cashWithApplication_input']","TGTYPESCREENREG");

		try { 
		  WebElement CashApplicationTextField = driver.findElement(By.cssSelector("input[type=text][name='workbenchTabs:tabForm2:column_panel_1:cashWithApplication_input']"));
		 CashApplicationTextField.sendKeys(Keys.BACK_SPACE);
		CashApplicationTextField.sendKeys(Keys.BACK_SPACE);
		CashApplicationTextField.sendKeys(Keys.BACK_SPACE);
		 
		 ;} 
		 catch(Exception e) { PrintVal("Renderer Script Exception : "+e.toString()); } 
		 PrintVal("Executed Step = CALLRENDERERSCRIPT,CLEARCASHWITHAPPTEXTFIELD"); 
        TAP.$("ELE_COMMISSIONADJUST","//input[@type='text'][@id='workbenchTabs:tabForm2:column_panel_2:commissionAdjuCode']","TGTYPESCREENREG");

        SWIPE.$("UP","TGTYPESCREENREG");

        SWIPE.$("UP","TGTYPESCREENREG");

        TAP.$("ELE_AGENTGRID","td.ui-editable-columnTGWEBCOMMACssSelectorTGWEBCOMMA0","TGTYPESCREENREG");

        TYPE.$("ELE_CREATEPOLICYAGENTNUMBER","workbenchTabs:tabForm2:grid:0:in_agentNumber_ColTGWEBCOMMAIdTGWEBCOMMA0","A13000000","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_SPLITPERCENTAGEGRID","td.ui-editable-columnTGWEBCOMMACssSelectorTGWEBCOMMA2","TGTYPESCREENREG");

        TYPE.$("ELE_CREATEPOLICYSPLITPERCENTTEXTFIELD","workbenchTabs:tabForm2:grid:0:in_agentSplitPercentage_Col_inputTGWEBCOMMAIdTGWEBCOMMA0","100","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_VALIDATEBUTTON","//span[contains(text(),'Validate')]","TGTYPESCREENREG");

        TAP.$("ELE_SAVEBUTTON","//span[contains(text(),'Save')]","TGTYPESCREENREG");

        TAP.$("ELE_CREATEPOLICYSHOWLINK","//span[contains(text(),'Show Links')]","TGTYPESCREENREG");

        TAP.$("ELE_CREATEPOLICYSELECTCLIENT","//span[contains(text(),'Select Client')]","TGTYPESCREENREG");

        TYPE.$("ELE_SELECTCLIENTSEARCHFIELD","input[type=text][id='workbenchTabs:tabForm2:searchName']TGWEBCOMMACssSelectorTGWEBCOMMA0","Address Test","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_SEARCHBUTTON","//span[contains(text(),'Search')]","TGTYPESCREENREG");

        TAP.$("ELE_SELECTBUTTON","//span[contains(text(),'Select')]","TGTYPESCREENREG");

        TAP.$("ELE_PAYORGRIDLINK","//span[@id='workbenchTabs:tabForm2:grid1:2:individualName']","TGTYPESCREENREG");

        TAP.$("ELE_SEARCHBUTTON","//span[contains(text(),'Search')]","TGTYPESCREENREG");

        TAP.$("ELE_SELECTBUTTON","//span[contains(text(),'Select')]","TGTYPESCREENREG");

        TAP.$("ELE_CONTINUEBUTTON","//span[contains(text(),'Continue')]","TGTYPESCREENREG");

        TAP.$("ELE_SAVEBUTTON","//span[contains(text(),'Save')]","TGTYPESCREENREG");

        TAP.$("ELE_CREATEPOLICYSHOWLINK","//span[contains(text(),'Show Links')]","TGTYPESCREENREG");

        TAP.$("ELE_BENEFITOPTION","//span[contains(text(),'Benefits')]","TGTYPESCREENREG");

        TAP.$("ELE_ADDNEWBUSINESSBENEFITTAB","//a[contains(text(),'Add New Business Benefit')]","TGTYPESCREENREG");

        TAP.$("ELE_CONTINUEBUTTON","//span[contains(text(),'Continue')]","TGTYPESCREENREG");

        TYPE.$("ELE_UNITSOFINSURANCE","//input[@class='ui-inputfield ui-inputtext ui-widget ui-state-default ui-corner-all ui-state-error']","10.000","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_SAVEBUTTON","//span[contains(text(),'Save')]","TGTYPESCREENREG");

        TAP.$("ELE_DISPLAYNEWBUSINESSPOLICYCONTRACTTAB"," //a[contains(text(),'Display New Business Policy Contract')]","TGTYPESCREENREG");

        TAP.$("ELE_CREATEPOLICYSHOWLINK","//span[contains(text(),'Show Links')]","TGTYPESCREENREG");

        TAP.$("ELE_ISSUEOPTION","//span[contains(text(),'Issue')]","TGTYPESCREENREG");

        TAP.$("ELE_UPDATESTATUSCHANGETOISSUEDECLINEWITHDRAWNTAB","//a[contains(text(),'Update Status Change to Issue/Decline/Withdrawn')]","TGTYPESCREENREG");

        WAIT.$("ELE_UPDATESTATUSCHANGETOISSUEDECLINEWITHDRAWNTAB","//a[contains(text(),'Update Status Change to Issue/Decline/Withdrawn')]","VISIBLE","TGTYPESCREENREG");

        TAP.$("ELE_ISSUEEFFECTIVEDATE","//input[@class='ui-inputfield ui-widget ui-state-default ui-corner-all hasDatepicker']","TGTYPESCREENREG");

		try { 
		 WebElement chooseCurrentDate= driver.findElement(By.xpath("//div[@id='ui-datepicker-div']//a[@class='ui-state-default ui-state-highlight']"));
						chooseCurrentDate.click();
						Thread.sleep(1000);
		 
		 ;} 
		 catch(Exception e) { PrintVal("Renderer Script Exception : "+e.toString()); } 
		 PrintVal("Executed Step = CALLRENDERERSCRIPT,CHOOSECURRENTDATE"); 
        TAP.$("ELE_SUBMITBUTTON","//span[contains(text(),'Submit')]","TGTYPESCREENREG");

        TAP.$("ELE_DISPLAYNEWBUSINESSPOLICYCONTRACTTAB"," //a[contains(text(),'Display New Business Policy Contract')]","TGTYPESCREENREG");

        WAIT.$("ELE_DISPLAYNEWBUSINESSPOLICYCONTRACTTAB"," //a[contains(text(),'Display New Business Policy Contract')]","VISIBLE",0,"TGTYPESCREENREG");

        TAP.$("ELE_CREATEPOLICYSHOWLINK","//span[contains(text(),'Show Links')]","TGTYPESCREENREG");

        TAP.$("ELE_SETTLEOPTION","//span[contains(text(),'Settle')]","TGTYPESCREENREG");

        TAP.$("ELE_SAVEBUTTON","//span[contains(text(),'Save')]","TGTYPESCREENREG");

        TAP.$("ELE_UPDATESTATUSCHANGETOACTIVETAB","//a[contains(text(),'Update Status Change to Active')]","TGTYPESCREENREG");

        WAIT.$("ELE_UPDATESTATUSCHANGETOACTIVETAB","//a[contains(text(),'Update Status Change to Active')]","VISIBLE","TGTYPESCREENREG");

        TAP.$("ELE_SUBMITBUTTON","//span[contains(text(),'Submit')]","TGTYPESCREENREG");


    }


    @Test
    public void policysurrender() {

        // No need to declare / print method name here, handled by TestNg listener
        // declare("policysurrender");

    }


    @Test
    public void suspensemaintenance() {

        // No need to declare / print method name here, handled by TestNg listener
        // declare("suspensemaintenance");

    }


    @Test
    public void tc01createpolicyonly() {

        // No need to declare / print method name here, handled by TestNg listener
        // declare("tc01createpolicyonly");

        CALL.$("LoginToGIAS4","TGTYPESCREENREG");

        TYPE.$("ELE_USERNAMETEXTFIELD","//input[@id='guestForm:username']","x5823","FALSE","TGTYPESCREENREG");

        TYPE.$("ELE_PASSWORDTEXTFIELD","//input[@id='guestForm:password']","Welcome123$","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_LOGINBUTTON","//input[@id='guestForm:loginButton']","TGTYPESCREENREG");

		String var_dataToPass;
                 LOGGER.info("Executed Step = VAR,String,var_dataToPass,TGTYPESCREENREG");
		int var_Count = 0;
                 LOGGER.info("Executed Step = VAR,Integer,var_Count,0,TGTYPESCREENREG");
        LOGGER.info("Executed Step = REPEAT IF");
    	while (check(var_Count,"<",1,"TGTYPESCREENREG")) {
          LOGGER.info("Executed Step = WHILE,var_Count,<,1,TGTYPESCREENREG");
        CALL.$("AddDistributionChannel","TGTYPESCREENREG");

		com.jayway.jsonpath.ReadContext var_CSVData = getJsonData("C:/GIAS Automation/4.0 BaseRegression/InputData/4_0_CreateAPolicy.csv");
                 LOGGER.info("Executed Step = VAR,Json,var_CSVData,C:/GIAS Automation/4.0 BaseRegression/InputData/4_0_CreateAPolicy.csv,TGTYPESCREENREG");
		String var_DistributionChannelDescription;
                 LOGGER.info("Executed Step = VAR,String,var_DistributionChannelDescription,TGTYPESCREENREG");
		String var_DistributionChannelName;
                 LOGGER.info("Executed Step = VAR,String,var_DistributionChannelName,TGTYPESCREENREG");
		var_DistributionChannelName = getJsonData(var_CSVData , "$.records["+var_Count+"].DistributionChannelName");
                 LOGGER.info("Executed Step = STORE,var_DistributionChannelName,var_CSVData,$.records[{var_Count}].DistributionChannelName,TGTYPESCREENREG");
		var_DistributionChannelDescription = getJsonData(var_CSVData , "$.records["+var_Count+"].DistributionChannelDescription");
                 LOGGER.info("Executed Step = STORE,var_DistributionChannelDescription,var_CSVData,$.records[{var_Count}].DistributionChannelDescription,TGTYPESCREENREG");
        TAP.$("ELE_DASHBOARDPAGENUMBERTWO","//div[@id='workbenchTabs:tabForm0:menuboardContainer_paginator_top']//a[@class='ui-paginator-page ui-state-default ui-corner-all'][contains(text(),'2')]","TGTYPESCREENREG");

        TAP.$("ELE_DISTRIBUTIONCHANNELLINK","//span[contains(text(),'Distribution Channel')]","TGTYPESCREENREG");

        TAP.$("ELE_DISTRIBUTIONCHANNELTAB","//a[contains(text(),'Distribution Channel')]","TGTYPESCREENREG");

        TAP.$("ELE_ADDBUTTON","//span[contains(text(),'Add')]","TGTYPESCREENREG");

        TYPE.$("ELE_DISTRIBUTIONCHANNELNAME","//input[@id='workbenchTabs:tabForm2:column_panel_0:distributionChanName']",var_DistributionChannelName,"TRUE","TGTYPESCREENREG");

        TYPE.$("ELE_DISTRIBUTIONCHANNELDESCRIPTION","//input[@id='workbenchTabs:tabForm2:column_panel_0:channelDescription']",var_DistributionChannelDescription,"TRUE","TGTYPESCREENREG");

        TAP.$("ELE_VALIDATEBUTTON","//span[contains(text(),'Validate')]","TGTYPESCREENREG");

        TAP.$("ELE_SAVEBUTTON","//span[contains(text(),'Save')]","TGTYPESCREENREG");

        TAP.$("ELE_CLOSINGTAB1","span[class='ui-icon ui-icon-close']TGWEBCOMMACssSelectorTGWEBCOMMA0","TGTYPESCREENREG");

        TAP.$("ELE_CLOSINGTAB1","span[class='ui-icon ui-icon-close']TGWEBCOMMACssSelectorTGWEBCOMMA0","TGTYPESCREENREG");

        CALL.$("AddAgentRankCodes","TGTYPESCREENREG");

        TAP.$("ELE_MYDASHBOARDTAB","//a[contains(text(),'My Dashboard')]","TGTYPESCREENREG");

        TAP.$("ELE_AGENTRANKCODESLINK","//span[contains(text(),'Agent Rank Codes')]","TGTYPESCREENREG");

        TAP.$("ELE_AGENTRANKCODESTAB","//a[contains(text(),'Agent Rank Codes')]","TGTYPESCREENREG");

        TAP.$("ELE_ADDBUTTON","//span[contains(text(),'Add')]","TGTYPESCREENREG");

		String var_RankDescription;
                 LOGGER.info("Executed Step = VAR,String,var_RankDescription,TGTYPESCREENREG");
		String var_RankCode;
                 LOGGER.info("Executed Step = VAR,String,var_RankCode,TGTYPESCREENREG");
		var_dataToPass = getJsonData(var_CSVData , "$.records["+var_Count+"].DistributionChannelDescription");
                 LOGGER.info("Executed Step = STORE,var_dataToPass,var_CSVData,$.records[{var_Count}].DistributionChannelDescription,TGTYPESCREENREG");
		var_RankCode = getJsonData(var_CSVData , "$.records["+var_Count+"].AgencyRankCode");
                 LOGGER.info("Executed Step = STORE,var_RankCode,var_CSVData,$.records[{var_Count}].AgencyRankCode,TGTYPESCREENREG");
		var_RankDescription = getJsonData(var_CSVData , "$.records["+var_Count+"].AgencyRankDescription");
                 LOGGER.info("Executed Step = STORE,var_RankDescription,var_CSVData,$.records[{var_Count}].AgencyRankDescription,TGTYPESCREENREG");
        TAP.$("ELE_RANKCODEDISTRIBUTIONCHANNELDRPDWN","//label[@id='workbenchTabs:tabForm2:column_panel_0:distributionChannel_label']","TGTYPESCREENREG");

		try { 
		 WebElement selectValueOfDropDown= driver.findElement(By.xpath("//ul[@id='workbenchTabs:tabForm2:column_panel_0:distributionChannel_items']//li[@data-label='"+ var_dataToPass +"']"));
				selectValueOfDropDown.click();
		             Thread.sleep(1000);
		 
		 ;} 
		 catch(Exception e) { PrintVal("Renderer Script Exception : "+e.toString()); } 
		 PrintVal("Executed Step = CALLRENDERERSCRIPT,SELECTDISTRIBUTIONCHANNELDROPDOWN"); 
        TYPE.$("ELE_RANKLEVELTEXTFIELD","//input[@id='workbenchTabs:tabForm2:column_panel_0:agencyRankLevel_input']","1","FALSE","TGTYPESCREENREG");

        TYPE.$("ELE_RANKCODETEXTFIELD","//input[@id='workbenchTabs:tabForm2:column_panel_0:agencyRankCode']",var_RankCode,"FALSE","TGTYPESCREENREG");

        TYPE.$("ELE_RANKDESCRIPTIONTEXTFIELD","//input[@id='workbenchTabs:tabForm2:column_panel_0:description']",var_RankDescription,"FALSE","TGTYPESCREENREG");

        TAP.$("ELE_VALIDATEBUTTON","//span[contains(text(),'Validate')]","TGTYPESCREENREG");

        TAP.$("ELE_SAVEBUTTON","//span[contains(text(),'Save')]","TGTYPESCREENREG");

        TAP.$("ELE_CLOSINGTAB1","span[class='ui-icon ui-icon-close']TGWEBCOMMACssSelectorTGWEBCOMMA0","TGTYPESCREENREG");

        TAP.$("ELE_CLOSINGTAB1","span[class='ui-icon ui-icon-close']TGWEBCOMMACssSelectorTGWEBCOMMA0","TGTYPESCREENREG");

        CALL.$("AddCommissionScheduleRates","TGTYPESCREENREG");

        TAP.$("ELE_DASHBOARDPAGENUMBERTWO","//div[@id='workbenchTabs:tabForm0:menuboardContainer_paginator_top']//a[@class='ui-paginator-page ui-state-default ui-corner-all'][contains(text(),'2')]","TGTYPESCREENREG");

        TAP.$("ELE_COMMISSIONSCHEDULERATESLINK","//span[contains(text(),'Commission Schedule Rates')]","TGTYPESCREENREG");

        TAP.$("ELE_COMMISSIONSCHEDULETAB","//a[contains(text(),'Commission Schedule')]","TGTYPESCREENREG");

        TAP.$("ELE_ADDBUTTON","//span[contains(text(),'Add')]","TGTYPESCREENREG");

		String var_CommissionDescription;
                 LOGGER.info("Executed Step = VAR,String,var_CommissionDescription,TGTYPESCREENREG");
		String var_CommissionTable;
                 LOGGER.info("Executed Step = VAR,String,var_CommissionTable,TGTYPESCREENREG");
		var_CommissionTable = getJsonData(var_CSVData , "$.records["+var_Count+"].TableName");
                 LOGGER.info("Executed Step = STORE,var_CommissionTable,var_CSVData,$.records[{var_Count}].TableName,TGTYPESCREENREG");
		var_CommissionDescription = getJsonData(var_CSVData , "$.records["+var_Count+"].TableDescription");
                 LOGGER.info("Executed Step = STORE,var_CommissionDescription,var_CSVData,$.records[{var_Count}].TableDescription,TGTYPESCREENREG");
        TYPE.$("ELE_COMMISSIONSCHEDULERATESTABLENAME","//input[@id='workbenchTabs:tabForm2:column_panel_0:tableName']",var_CommissionTable,"FALSE","TGTYPESCREENREG");

        TYPE.$("ELE_COMMISSIONSCHEDULERATESDESCRIPTION","//input[@id='workbenchTabs:tabForm2:column_panel_0:description']",var_CommissionDescription,"FALSE","TGTYPESCREENREG");

		var_dataToPass = getJsonData(var_CSVData , "$.records["+var_Count+"].DistributionChannelDescription");
                 LOGGER.info("Executed Step = STORE,var_dataToPass,var_CSVData,$.records[{var_Count}].DistributionChannelDescription,TGTYPESCREENREG");
        TAP.$("ELE_RANKCODEDISTRIBUTIONCHANNELDRPDWN","//label[@id='workbenchTabs:tabForm2:column_panel_0:distributionChannel_label']","TGTYPESCREENREG");

		try { 
		 WebElement selectValueOfDropDown= driver.findElement(By.xpath("//ul[@id='workbenchTabs:tabForm2:column_panel_0:distributionChannel_items']//li[@data-label='"+ var_dataToPass +"']"));
				selectValueOfDropDown.click();
		             Thread.sleep(1000);
		 
		 ;} 
		 catch(Exception e) { PrintVal("Renderer Script Exception : "+e.toString()); } 
		 PrintVal("Executed Step = CALLRENDERERSCRIPT,SELECTDISTRIBUTIONCHANNELDROPDOWN"); 
        TAP.$("ELE_CONTINUEBUTTON","//span[contains(text(),'Continue')]","TGTYPESCREENREG");

        TYPE.$("ELE_COMMSCHRATESDURATION1","//input[@id='workbenchTabs:tabForm2:rateTable:endingDuration1_input']","1","FALSE","TGTYPESCREENREG");

        TYPE.$("ELE_COMMSCHRATESDURATION2","//input[@id='workbenchTabs:tabForm2:rateTable:endingDuration2_input']","5","FALSE","TGTYPESCREENREG");

        TYPE.$("ELE_COMMSCHRATESDURATION3","//input[@id='workbenchTabs:tabForm2:rateTable:endingDuration3_input']","120","FALSE","TGTYPESCREENREG");

		var_dataToPass = getJsonData(var_CSVData , "$.records["+var_Count+"].AgencyRankDescription");
                 LOGGER.info("Executed Step = STORE,var_dataToPass,var_CSVData,$.records[{var_Count}].AgencyRankDescription,TGTYPESCREENREG");
        TAP.$("ELE_COMMISSIONRATEAGENCYGRID","//div[@id='workbenchTabs:tabForm2:rateTable']//tr[1]//td[1]","TGTYPESCREENREG");

        TAP.$("ELE_COMMISSIONRATEAGENCYGRID","//div[@id='workbenchTabs:tabForm2:rateTable']//tr[1]//td[1]","TGTYPESCREENREG");

        WAIT.$("ELE_COMMRATESAGENCYRANKCODEDRPDWN","//select[@id='workbenchTabs:tabForm2:rateTable:0:in_agencyRankCode_Col']","VISIBLE","TGTYPESCREENREG");

        SELECT.$("ELE_COMMRATESAGENCYRANKCODEDRPDWN","//select[@id='workbenchTabs:tabForm2:rateTable:0:in_agencyRankCode_Col']",var_dataToPass,"TGTYPESCREENREG");

        WAIT.$("ELE_COMMISSIONRATEGRID1","//div[@id='workbenchTabs:tabForm2:rateTable']//tr[1]//td[2]","VISIBLE","TGTYPESCREENREG");

        TAP.$("ELE_COMMISSIONRATEGRID1","//div[@id='workbenchTabs:tabForm2:rateTable']//tr[1]//td[2]","TGTYPESCREENREG");

		try { 
		 WebElement clearRateField = driver.findElement(By.cssSelector("input[type=text][name='workbenchTabs:tabForm2:rateTable:0:in_commissionRate1_input']"));

				clearRateField.click();
				 clearRateField.sendKeys(Keys.BACK_SPACE);
				 clearRateField.sendKeys(Keys.BACK_SPACE);
				 clearRateField.sendKeys(Keys.BACK_SPACE);
				 clearRateField.sendKeys(Keys.BACK_SPACE);
				 clearRateField.sendKeys(Keys.BACK_SPACE);
				 clearRateField.sendKeys(Keys.BACK_SPACE);
				 clearRateField.sendKeys(Keys.BACK_SPACE);
				 		 clearRateField.sendKeys("33");
		 
		 ;} 
		 catch(Exception e) { PrintVal("Renderer Script Exception : "+e.toString()); } 
		 PrintVal("Executed Step = CALLRENDERERSCRIPT,CLEARRATEFIELD1"); 
        WAIT.$("ELE_COMMISSIONRATEGRID2","//div[@id='workbenchTabs:tabForm2:rateTable']//tr[1]//td[3]","VISIBLE","TGTYPESCREENREG");

        TAP.$("ELE_COMMISSIONRATEGRID2","//div[@id='workbenchTabs:tabForm2:rateTable']//tr[1]//td[3]","TGTYPESCREENREG");

		try { 
		 WebElement clearRateField = driver.findElement(By.cssSelector("input[type=text][name='workbenchTabs:tabForm2:rateTable:0:in_commissionRate2_input']"));

				clearRateField.click();
				 clearRateField.sendKeys(Keys.BACK_SPACE);
				 clearRateField.sendKeys(Keys.BACK_SPACE);
				 clearRateField.sendKeys(Keys.BACK_SPACE);
				 clearRateField.sendKeys(Keys.BACK_SPACE);
				 clearRateField.sendKeys(Keys.BACK_SPACE);
				 clearRateField.sendKeys(Keys.BACK_SPACE);
				 clearRateField.sendKeys(Keys.BACK_SPACE);
				 		 clearRateField.sendKeys("22.22");
		 
		 ;} 
		 catch(Exception e) { PrintVal("Renderer Script Exception : "+e.toString()); } 
		 PrintVal("Executed Step = CALLRENDERERSCRIPT,CLEARRATEFIELD2"); 
        WAIT.$("ELE_COMMISSIONRATEGRID3","//div[@id='workbenchTabs:tabForm2:rateTable']//tr[1]//td[4]","VISIBLE","TGTYPESCREENREG");

        TAP.$("ELE_COMMISSIONRATEGRID3","//div[@id='workbenchTabs:tabForm2:rateTable']//tr[1]//td[4]","TGTYPESCREENREG");

		try { 
		  WebElement clearRateField = driver.findElement(By.cssSelector("input[type=text][name='workbenchTabs:tabForm2:rateTable:0:in_commissionRate3_input']"));

				clearRateField.click();
				 clearRateField.sendKeys(Keys.BACK_SPACE);
				 clearRateField.sendKeys(Keys.BACK_SPACE);
				 clearRateField.sendKeys(Keys.BACK_SPACE);
				 clearRateField.sendKeys(Keys.BACK_SPACE);
				 clearRateField.sendKeys(Keys.BACK_SPACE);
				 clearRateField.sendKeys(Keys.BACK_SPACE);
				 clearRateField.sendKeys(Keys.BACK_SPACE);
				 		 clearRateField.sendKeys("12.12");
		 
		 ;} 
		 catch(Exception e) { PrintVal("Renderer Script Exception : "+e.toString()); } 
		 PrintVal("Executed Step = CALLRENDERERSCRIPT,CLEARRATEFIELD3"); 
        TAP.$("ELE_COMMISSIONRATEGRID4","//div[@id='workbenchTabs:tabForm2:rateTable']//tr[1]//td[5]","TGTYPESCREENREG");

        WAIT.$(1,"TGTYPESCREENREG");

        TAP.$("ELE_VALIDATEBUTTON","//span[contains(text(),'Validate')]","TGTYPESCREENREG");

        WAIT.$("ELE_SAVEBUTTON","//span[contains(text(),'Save')]","VISIBLE","TGTYPESCREENREG");

        TAP.$("ELE_SAVEBUTTON","//span[contains(text(),'Save')]","TGTYPESCREENREG");

        WAIT.$(1,"TGTYPESCREENREG");

        TAP.$("ELE_CLOSINGTAB1","span[class='ui-icon ui-icon-close']TGWEBCOMMACssSelectorTGWEBCOMMA0","TGTYPESCREENREG");

        WAIT.$(1,"TGTYPESCREENREG");

        TAP.$("ELE_CLOSINGTAB1","span[class='ui-icon ui-icon-close']TGWEBCOMMACssSelectorTGWEBCOMMA0","TGTYPESCREENREG");

        CALL.$("AddCommissionContracts","TGTYPESCREENREG");

        TAP.$("ELE_MYDASHBOARDTAB","//a[contains(text(),'My Dashboard')]","TGTYPESCREENREG");

        TAP.$("ELE_DASHBOARDPAGENUMBERTWO","//div[@id='workbenchTabs:tabForm0:menuboardContainer_paginator_top']//a[@class='ui-paginator-page ui-state-default ui-corner-all'][contains(text(),'2')]","TGTYPESCREENREG");

        TAP.$("ELE_COMMISSIONCONTRACTSLINK","//span[contains(text(),'Commission Contracts')]","TGTYPESCREENREG");

        TAP.$("ELE_COMMISSIONCONTRACTSTAB","//a[contains(text(),'Commission Contracts')]","TGTYPESCREENREG");

        TAP.$("ELE_ADDBUTTON","//span[contains(text(),'Add')]","TGTYPESCREENREG");

		String var_contractNumber;
                 LOGGER.info("Executed Step = VAR,String,var_contractNumber,TGTYPESCREENREG");
		String var_contractDescription;
                 LOGGER.info("Executed Step = VAR,String,var_contractDescription,TGTYPESCREENREG");
		var_contractNumber = getJsonData(var_CSVData , "$.records["+var_Count+"].CommissionContractNumber");
                 LOGGER.info("Executed Step = STORE,var_contractNumber,var_CSVData,$.records[{var_Count}].CommissionContractNumber,TGTYPESCREENREG");
		var_contractDescription = getJsonData(var_CSVData , "$.records["+var_Count+"].CommissionContractDescription");
                 LOGGER.info("Executed Step = STORE,var_contractDescription,var_CSVData,$.records[{var_Count}].CommissionContractDescription,TGTYPESCREENREG");
        TYPE.$("ELE_COMMCONTRACTNUMBERTEXTFIELD","//input[@type='text'][@id='workbenchTabs:tabForm2:column_panel_0:commissionContNumb_input']",var_contractNumber,"FALSE","TGTYPESCREENREG");

        TYPE.$("ELE_COMMICONTRACTDESCRIPTIOTEXTFIELD","//input[@id='workbenchTabs:tabForm2:column_panel_0:longDescription']",var_contractDescription,"FALSE","TGTYPESCREENREG");

		var_dataToPass = getJsonData(var_CSVData , "$.records["+var_Count+"].DistributionChannelDescription");
                 LOGGER.info("Executed Step = STORE,var_dataToPass,var_CSVData,$.records[{var_Count}].DistributionChannelDescription,TGTYPESCREENREG");
        TAP.$("ELE_COMMCONTRACTDISTRIBUTIONCHANNELDRPDWN","//label[@id='workbenchTabs:tabForm2:column_panel_0:distributionChannel_label']","TGTYPESCREENREG");

		try { 
		 WebElement selectValueOfDropDown= driver.findElement(By.xpath("//ul[@id='workbenchTabs:tabForm2:column_panel_0:distributionChannel_items']//li[@data-label='"+ var_dataToPass +"']"));
				selectValueOfDropDown.click();
		             Thread.sleep(1000);
		 
		 ;} 
		 catch(Exception e) { PrintVal("Renderer Script Exception : "+e.toString()); } 
		 PrintVal("Executed Step = CALLRENDERERSCRIPT,SELECTDISTRIBUTIONCHANNELDROPDOWN"); 
        TAP.$("ELE_COMMISSIONCONTRACTEFFECTIVEDATETEXTFIELD","//input[@id='workbenchTabs:tabForm2:column_panel_0:effectiveDate_input']","TGTYPESCREENREG");

		try { 
		 WebElement chooseCurrentDate= driver.findElement(By.xpath("//div[@id='ui-datepicker-div']//a[@class='ui-state-default ui-state-highlight']"));
						chooseCurrentDate.click();
						Thread.sleep(1000);
		 
		 ;} 
		 catch(Exception e) { PrintVal("Renderer Script Exception : "+e.toString()); } 
		 PrintVal("Executed Step = CALLRENDERERSCRIPT,CHOOSECURRENTDATE"); 
		var_dataToPass = "100% of Commission Earned";
                 LOGGER.info("Executed Step = STORE,var_dataToPass,100% of Commission Earned,TGTYPESCREENREG");
        WAIT.$("ELE_COMMCONTRACTADVANCEPAYBACKMETHODDRPDWN","//label[@id='workbenchTabs:tabForm2:column_panel_1:advancePaybackMethod_label']","TGTYPESCREENREG");

        TAP.$("ELE_COMMCONTRACTADVANCEPAYBACKMETHODDRPDWN","//label[@id='workbenchTabs:tabForm2:column_panel_1:advancePaybackMethod_label']","TGTYPESCREENREG");

		try { 
		  WebElement selectValueOfDropDown= driver.findElement(By.xpath("//li[@data-label='"+ var_dataToPass +"']"));
		selectValueOfDropDown.click();
		 Thread.sleep(1000);
		 
		 ;} 
		 catch(Exception e) { PrintVal("Renderer Script Exception : "+e.toString()); } 
		 PrintVal("Executed Step = CALLRENDERERSCRIPT,SELECTDROPDOWNONLY"); 
		var_dataToPass = getJsonData(var_CSVData , "$.records["+var_Count+"].AgencyRankDescription");
                 LOGGER.info("Executed Step = STORE,var_dataToPass,var_CSVData,$.records[{var_Count}].AgencyRankDescription,TGTYPESCREENREG");
        WAIT.$("ELE_COMMCONTRACTADVANCETHROUGHAGENTRANKDRPDWN","//label[@id='workbenchTabs:tabForm2:column_panel_1:topLevelForAdvance_label']","VISIBLE","TGTYPESCREENREG");

        TAP.$("ELE_COMMCONTRACTADVANCETHROUGHAGENTRANKDRPDWN","//label[@id='workbenchTabs:tabForm2:column_panel_1:topLevelForAdvance_label']","TGTYPESCREENREG");

		try { 
		  WebElement selectValueOfDropDown= driver.findElement(By.xpath("//li[@data-label='"+ var_dataToPass +"']"));
		selectValueOfDropDown.click();
		 Thread.sleep(1000);
		 
		 ;} 
		 catch(Exception e) { PrintVal("Renderer Script Exception : "+e.toString()); } 
		 PrintVal("Executed Step = CALLRENDERERSCRIPT,SELECTDROPDOWNONLY"); 
        TAP.$("ELE_VALIDATEBUTTON","//span[contains(text(),'Validate')]","TGTYPESCREENREG");

        TAP.$("ELE_COMMISSIONCONTRACTEFFECTIVEDATETEXTFIELD","//input[@id='workbenchTabs:tabForm2:column_panel_0:effectiveDate_input']","TGTYPESCREENREG");

		try { 
		 WebElement chooseCurrentDate= driver.findElement(By.xpath("//div[@id='ui-datepicker-div']//a[@class='ui-state-default ui-state-highlight']"));
						chooseCurrentDate.click();
						Thread.sleep(1000);
		 
		 ;} 
		 catch(Exception e) { PrintVal("Renderer Script Exception : "+e.toString()); } 
		 PrintVal("Executed Step = CALLRENDERERSCRIPT,CHOOSECURRENTDATE"); 
        TAP.$("ELE_VALIDATEBUTTON","//span[contains(text(),'Validate')]","TGTYPESCREENREG");

        WAIT.$("ELE_SAVEBUTTON","//span[contains(text(),'Save')]","VISIBLE","TGTYPESCREENREG");

        TAP.$("ELE_SAVEBUTTON","//span[contains(text(),'Save')]","TGTYPESCREENREG");

        WAIT.$(1,"TGTYPESCREENREG");

        TAP.$("ELE_CLOSINGTAB1","span[class='ui-icon ui-icon-close']TGWEBCOMMACssSelectorTGWEBCOMMA0","TGTYPESCREENREG");

        WAIT.$(1,"TGTYPESCREENREG");

        TAP.$("ELE_CLOSINGTAB1","span[class='ui-icon ui-icon-close']TGWEBCOMMACssSelectorTGWEBCOMMA0","TGTYPESCREENREG");

        CALL.$("AddCommissionSchedule","TGTYPESCREENREG");

        TAP.$("ELE_MYDASHBOARDTAB","//a[contains(text(),'My Dashboard')]","TGTYPESCREENREG");

        TAP.$("ELE_DASHBOARDPAGENUMBERTWO","//div[@id='workbenchTabs:tabForm0:menuboardContainer_paginator_top']//a[@class='ui-paginator-page ui-state-default ui-corner-all'][contains(text(),'2')]","TGTYPESCREENREG");

        TAP.$("ELE_COMMISSIONCONTRACTSLINK","//span[contains(text(),'Commission Contracts')]","TGTYPESCREENREG");

		String var_ContractNumber;
                 LOGGER.info("Executed Step = VAR,String,var_ContractNumber,TGTYPESCREENREG");
		var_ContractNumber = getJsonData(var_CSVData , "$.records["+var_Count+"].CommissionContractNumber");
                 LOGGER.info("Executed Step = STORE,var_ContractNumber,var_CSVData,$.records[{var_Count}].CommissionContractNumber,TGTYPESCREENREG");
        TYPE.$("ELE_CONTRACTNUMBERCSS","input[type=text][class='ui-inputfield ui-inputtext ui-widget ui-state-default ui-corner-all']TGWEBCOMMACssSelectorTGWEBCOMMA1",var_ContractNumber,"FALSE","TGTYPESCREENREG");

        WAIT.$("ELE_COMMCONTRACTIDLINK","span[id='workbenchTabs:tabForm1:grid:0:commissionContNumb']TGWEBCOMMACssSelectorTGWEBCOMMA0","VISIBLE","TGTYPESCREENREG");

        TAP.$("ELE_COMMCONTRACTIDLINK","span[id='workbenchTabs:tabForm1:grid:0:commissionContNumb']TGWEBCOMMACssSelectorTGWEBCOMMA0","TGTYPESCREENREG");

        TAP.$("ELE_CREATEPOLICYSHOWLINK","//span[contains(text(),'Show Links')]","TGTYPESCREENREG");

        TAP.$("ELE_COMMISSIONSCHEDULELINK","//span[contains(text(),'Commission Schedule')]","TGTYPESCREENREG");

        TAP.$("ELE_COMMISSIONSCHEDULETAB","//a[contains(text(),'Commission Schedule')]","TGTYPESCREENREG");

        TAP.$("ELE_ADDBUTTON","//span[contains(text(),'Add')]","TGTYPESCREENREG");

		var_dataToPass = "Ordinary life";
                 LOGGER.info("Executed Step = STORE,var_dataToPass,Ordinary life,TGTYPESCREENREG");
        TAP.$("ELE_COMMCONTRACTTYPEDRPDWN","//label[@id='workbenchTabs:tabForm4:column_panel_0:commissionContType_label']","TGTYPESCREENREG");

		try { 
		  WebElement selectValueOfDropDown= driver.findElement(By.xpath("//li[@data-label='"+ var_dataToPass +"']"));
		selectValueOfDropDown.click();
		 Thread.sleep(1000);
		 
		 ;} 
		 catch(Exception e) { PrintVal("Renderer Script Exception : "+e.toString()); } 
		 PrintVal("Executed Step = CALLRENDERERSCRIPT,SELECTDROPDOWNONLY"); 
        TAP.$("ELE_COMMSCHEDULERATETABLENAMEDRPDWN","//label[@id='workbenchTabs:tabForm4:column_panel_0:commissionRateTableName_label']","TGTYPESCREENREG");

		var_dataToPass = getJsonData(var_CSVData , "$.records["+var_Count+"].TableDescription");
                 LOGGER.info("Executed Step = STORE,var_dataToPass,var_CSVData,$.records[{var_Count}].TableDescription,TGTYPESCREENREG");
		try { 
		  WebElement selectValueOfDropDown= driver.findElement(By.xpath("//li[@data-label='"+ var_dataToPass +"']"));
		selectValueOfDropDown.click();
		 Thread.sleep(1000);
		 
		 ;} 
		 catch(Exception e) { PrintVal("Renderer Script Exception : "+e.toString()); } 
		 PrintVal("Executed Step = CALLRENDERERSCRIPT,SELECTDROPDOWNONLY"); 
		String var_LineDescription;
                 LOGGER.info("Executed Step = VAR,String,var_LineDescription,TGTYPESCREENREG");
		var_LineDescription = getJsonData(var_CSVData , "$.records["+var_Count+"].LineDescription");
                 LOGGER.info("Executed Step = STORE,var_LineDescription,var_CSVData,$.records[{var_Count}].LineDescription,TGTYPESCREENREG");
        TAP.$("ELE_COMMSCHEDULELINEDESCRIPTIONTEXTFIELD","input[type=text][id='workbenchTabs:tabForm4:column_panel_0:shortDescription']TGWEBCOMMACssSelectorTGWEBCOMMA0","TGTYPESCREENREG");

        TYPE.$("ELE_COMMSCHEDULELINEDESCRIPTIONTEXTFIELD","input[type=text][id='workbenchTabs:tabForm4:column_panel_0:shortDescription']TGWEBCOMMACssSelectorTGWEBCOMMA0",var_LineDescription,"FALSE","TGTYPESCREENREG");

        TAP.$("ELE_VALIDATEBUTTON","//span[contains(text(),'Validate')]","TGTYPESCREENREG");

        TAP.$("ELE_COMMISSIONSCHEDULEEFFECTIVEDATE","//input[@id='workbenchTabs:tabForm4:column_panel_0:effectiveDate_input']","TGTYPESCREENREG");

		try { 
		 WebElement chooseCurrentDate= driver.findElement(By.xpath("//div[@id='ui-datepicker-div']//a[@class='ui-state-default ui-state-highlight']"));
						chooseCurrentDate.click();
						Thread.sleep(1000);
		 
		 ;} 
		 catch(Exception e) { PrintVal("Renderer Script Exception : "+e.toString()); } 
		 PrintVal("Executed Step = CALLRENDERERSCRIPT,CHOOSECURRENTDATE"); 
        TAP.$("ELE_VALIDATEBUTTON","//span[contains(text(),'Validate')]","TGTYPESCREENREG");

        TAP.$("ELE_SAVEBUTTON","//span[contains(text(),'Save')]","TGTYPESCREENREG");

        SWIPE.$("DOWN","TGTYPESCREENREG");

        TAP.$("ELE_CLOSINGTAB1","span[class='ui-icon ui-icon-close']TGWEBCOMMACssSelectorTGWEBCOMMA0","TGTYPESCREENREG");

        TAP.$("ELE_CLOSINGTAB1","span[class='ui-icon ui-icon-close']TGWEBCOMMACssSelectorTGWEBCOMMA0","TGTYPESCREENREG");

        TAP.$("ELE_CLOSINGTAB1","span[class='ui-icon ui-icon-close']TGWEBCOMMACssSelectorTGWEBCOMMA0","TGTYPESCREENREG");

        TAP.$("ELE_CLOSINGTAB1","span[class='ui-icon ui-icon-close']TGWEBCOMMACssSelectorTGWEBCOMMA0","TGTYPESCREENREG");

        CALL.$("AddPolicyContract","TGTYPESCREENREG");

        TAP.$("ELE_MYDASHBOARDTAB","//a[contains(text(),'My Dashboard')]","TGTYPESCREENREG");

        TAP.$("ELE_APPLICATIONENTRYUPDATLINK","//span[contains(text(),'Application Entry/Update')]","TGTYPESCREENREG");

        TAP.$("ELE_APPLICATIONENTRYUPDATETAB","//a[contains(text(),'Application Entry/Update')]","TGTYPESCREENREG");

		var_dataToPass = "Quarterly";
                 LOGGER.info("Executed Step = STORE,var_dataToPass,Quarterly,TGTYPESCREENREG");
		try { 
		  WebElement selectValueOfDropDown= driver.findElement(By.xpath("//li[@data-label='"+ var_dataToPass +"']"));
		selectValueOfDropDown.click();
		 Thread.sleep(1000);
		 
		 ;} 
		 catch(Exception e) { PrintVal("Renderer Script Exception : "+e.toString()); } 
		 PrintVal("Executed Step = CALLRENDERERSCRIPT,SELECTDROPDOWNONLY"); 
		String var_PolicyNumber;
                 LOGGER.info("Executed Step = VAR,String,var_PolicyNumber,TGTYPESCREENREG");
		var_PolicyNumber = getJsonData(var_CSVData , "$.records["+var_Count+"].PolicyNumber");
                 LOGGER.info("Executed Step = STORE,var_PolicyNumber,var_CSVData,$.records[{var_Count}].PolicyNumber,TGTYPESCREENREG");
		String var_ApplicationNumber;
                 LOGGER.info("Executed Step = VAR,String,var_ApplicationNumber,TGTYPESCREENREG");
		var_ApplicationNumber = getJsonData(var_CSVData , "$.records["+var_Count+"].PolicyNumber");
                 LOGGER.info("Executed Step = STORE,var_ApplicationNumber,var_CSVData,$.records[{var_Count}].PolicyNumber,TGTYPESCREENREG");
        TAP.$("ELE_ADDBUTTON","//span[contains(text(),'Add')]","TGTYPESCREENREG");

        TYPE.$("ELE_POLICYNUMBERTEXTFIELD","//input[@id='workbenchTabs:tabForm2:column_panel_0:policyNumber']",var_PolicyNumber,"FALSE","TGTYPESCREENREG");

        TAP.$("ELE_CREATEPOLICYPAYMENTFREQUENCYDRPDWN","//label[@id='workbenchTabs:tabForm2:column_panel_0:paymentMode_label']","TGTYPESCREENREG");

		try { 
		  WebElement selectValueOfDropDown= driver.findElement(By.xpath("//li[@data-label='"+ var_dataToPass +"']"));
		selectValueOfDropDown.click();
		 Thread.sleep(1000);
		 
		 ;} 
		 catch(Exception e) { PrintVal("Renderer Script Exception : "+e.toString()); } 
		 PrintVal("Executed Step = CALLRENDERERSCRIPT,SELECTDROPDOWNONLY"); 
        TYPE.$("ELE_CREATEPOLICYAPPLICATIONNUMBERTEXTFIELD","//input[@id='workbenchTabs:tabForm2:column_panel_1:applicationNumber']",var_ApplicationNumber,"FALSE","TGTYPESCREENREG");

        TAP.$("ELE_CREATEPOLICYCASHWITHAPPLICATIONTEXTFIELD","//input[@id='workbenchTabs:tabForm2:column_panel_1:cashWithApplication_input']","TGTYPESCREENREG");

		try { 
		  WebElement CashApplicationTextField = driver.findElement(By.cssSelector("input[type=text][name='workbenchTabs:tabForm2:column_panel_1:cashWithApplication_input']"));
		 CashApplicationTextField.sendKeys(Keys.BACK_SPACE);
		CashApplicationTextField.sendKeys(Keys.BACK_SPACE);
		CashApplicationTextField.sendKeys(Keys.BACK_SPACE);
		 
		 ;} 
		 catch(Exception e) { PrintVal("Renderer Script Exception : "+e.toString()); } 
		 PrintVal("Executed Step = CALLRENDERERSCRIPT,CLEARCASHWITHAPPTEXTFIELD"); 
        TAP.$("ELE_COMMISSIONADJUST","//input[@type='text'][@id='workbenchTabs:tabForm2:column_panel_2:commissionAdjuCode']","TGTYPESCREENREG");

        SWIPE.$("UP","TGTYPESCREENREG");

        SWIPE.$("UP","TGTYPESCREENREG");

        TAP.$("ELE_AGENTGRID","td.ui-editable-columnTGWEBCOMMACssSelectorTGWEBCOMMA0","TGTYPESCREENREG");

        TYPE.$("ELE_CREATEPOLICYAGENTNUMBER","workbenchTabs:tabForm2:grid:0:in_agentNumber_ColTGWEBCOMMAIdTGWEBCOMMA0","A13000000","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_SPLITPERCENTAGEGRID","td.ui-editable-columnTGWEBCOMMACssSelectorTGWEBCOMMA2","TGTYPESCREENREG");

        TYPE.$("ELE_CREATEPOLICYSPLITPERCENTTEXTFIELD","workbenchTabs:tabForm2:grid:0:in_agentSplitPercentage_Col_inputTGWEBCOMMAIdTGWEBCOMMA0","100","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_VALIDATEBUTTON","//span[contains(text(),'Validate')]","TGTYPESCREENREG");

        TAP.$("ELE_SAVEBUTTON","//span[contains(text(),'Save')]","TGTYPESCREENREG");

        TAP.$("ELE_CREATEPOLICYSHOWLINK","//span[contains(text(),'Show Links')]","TGTYPESCREENREG");

        TAP.$("ELE_CREATEPOLICYSELECTCLIENT","//span[contains(text(),'Select Client')]","TGTYPESCREENREG");

        TYPE.$("ELE_SELECTCLIENTSEARCHFIELD","input[type=text][id='workbenchTabs:tabForm2:searchName']TGWEBCOMMACssSelectorTGWEBCOMMA0","Address Test","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_SEARCHBUTTON","//span[contains(text(),'Search')]","TGTYPESCREENREG");

        TAP.$("ELE_SELECTBUTTON","//span[contains(text(),'Select')]","TGTYPESCREENREG");

        TAP.$("ELE_PAYORGRIDLINK","//span[@id='workbenchTabs:tabForm2:grid1:2:individualName']","TGTYPESCREENREG");

        TAP.$("ELE_SEARCHBUTTON","//span[contains(text(),'Search')]","TGTYPESCREENREG");

        TAP.$("ELE_SELECTBUTTON","//span[contains(text(),'Select')]","TGTYPESCREENREG");

        TAP.$("ELE_CONTINUEBUTTON","//span[contains(text(),'Continue')]","TGTYPESCREENREG");

        TAP.$("ELE_SAVEBUTTON","//span[contains(text(),'Save')]","TGTYPESCREENREG");

        TAP.$("ELE_CREATEPOLICYSHOWLINK","//span[contains(text(),'Show Links')]","TGTYPESCREENREG");

        TAP.$("ELE_BENEFITOPTION","//span[contains(text(),'Benefits')]","TGTYPESCREENREG");

        TAP.$("ELE_ADDNEWBUSINESSBENEFITTAB","//a[contains(text(),'Add New Business Benefit')]","TGTYPESCREENREG");

        TAP.$("ELE_CONTINUEBUTTON","//span[contains(text(),'Continue')]","TGTYPESCREENREG");

        TYPE.$("ELE_UNITSOFINSURANCE","//input[@class='ui-inputfield ui-inputtext ui-widget ui-state-default ui-corner-all ui-state-error']","10.000","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_SAVEBUTTON","//span[contains(text(),'Save')]","TGTYPESCREENREG");

        TAP.$("ELE_DISPLAYNEWBUSINESSPOLICYCONTRACTTAB"," //a[contains(text(),'Display New Business Policy Contract')]","TGTYPESCREENREG");

        TAP.$("ELE_CREATEPOLICYSHOWLINK","//span[contains(text(),'Show Links')]","TGTYPESCREENREG");

        TAP.$("ELE_ISSUEOPTION","//span[contains(text(),'Issue')]","TGTYPESCREENREG");

        TAP.$("ELE_UPDATESTATUSCHANGETOISSUEDECLINEWITHDRAWNTAB","//a[contains(text(),'Update Status Change to Issue/Decline/Withdrawn')]","TGTYPESCREENREG");

        WAIT.$("ELE_UPDATESTATUSCHANGETOISSUEDECLINEWITHDRAWNTAB","//a[contains(text(),'Update Status Change to Issue/Decline/Withdrawn')]","VISIBLE","TGTYPESCREENREG");

        TAP.$("ELE_ISSUEEFFECTIVEDATE","//input[@class='ui-inputfield ui-widget ui-state-default ui-corner-all hasDatepicker']","TGTYPESCREENREG");

		try { 
		 WebElement chooseCurrentDate= driver.findElement(By.xpath("//div[@id='ui-datepicker-div']//a[@class='ui-state-default ui-state-highlight']"));
						chooseCurrentDate.click();
						Thread.sleep(1000);
		 
		 ;} 
		 catch(Exception e) { PrintVal("Renderer Script Exception : "+e.toString()); } 
		 PrintVal("Executed Step = CALLRENDERERSCRIPT,CHOOSECURRENTDATE"); 
        TAP.$("ELE_SUBMITBUTTON","//span[contains(text(),'Submit')]","TGTYPESCREENREG");

        TAP.$("ELE_DISPLAYNEWBUSINESSPOLICYCONTRACTTAB"," //a[contains(text(),'Display New Business Policy Contract')]","TGTYPESCREENREG");

        WAIT.$("ELE_DISPLAYNEWBUSINESSPOLICYCONTRACTTAB"," //a[contains(text(),'Display New Business Policy Contract')]","VISIBLE",0,"TGTYPESCREENREG");

        TAP.$("ELE_CREATEPOLICYSHOWLINK","//span[contains(text(),'Show Links')]","TGTYPESCREENREG");

        TAP.$("ELE_SETTLEOPTION","//span[contains(text(),'Settle')]","TGTYPESCREENREG");

        TAP.$("ELE_SAVEBUTTON","//span[contains(text(),'Save')]","TGTYPESCREENREG");

        TAP.$("ELE_UPDATESTATUSCHANGETOACTIVETAB","//a[contains(text(),'Update Status Change to Active')]","TGTYPESCREENREG");

        WAIT.$("ELE_UPDATESTATUSCHANGETOACTIVETAB","//a[contains(text(),'Update Status Change to Active')]","VISIBLE","TGTYPESCREENREG");

        TAP.$("ELE_SUBMITBUTTON","//span[contains(text(),'Submit')]","TGTYPESCREENREG");

		var_Count = var_Count + ( 1 );
                 LOGGER.info("Executed Step = INCREASEVAR,var_Count,1,TGTYPESCREENREG");
    	}
          LOGGER.info("Executed Step = WEND,TGTYPESCREENREG");
    	resetAttemps();

    }


}
