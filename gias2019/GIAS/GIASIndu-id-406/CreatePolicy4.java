package appium.tgo;

import com.tg.octopus.base.TGOWrapper;
import com.tg.octopus.libs.testng.listener.TestListener;
import static com.tg.octopus.base.TGOWrapper.Action.*;
import static com.tg.octopus.base.TGOWrapper.LOGGER;
import com.tg.octopus.wrapper.ActionWrapper;

import com.jayway.jsonpath.JsonPath;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.SessionNotCreatedException;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.Augmenter;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.remote.UnreachableBrowserException;

import org.testng.Assert;
import org.testng.ITestContext;
import org.testng.ITestResult;
import org.testng.Reporter;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;
import java.net.HttpURLConnection;
import java.net.URL;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import org.json.JSONArray;
import org.json.JSONObject;
import helper.WebTestUtility;
import java.sql.*;
import com.testautomationguru.utility.PDFUtil;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.multipdf.Splitter;

import java.util.*;
import org.openqa.selenium.*;
import java.io.*;
import java.net.*;

import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;


@Listeners(TestListener.class)
public class CreatePolicy4 extends TGOWrapper {



    static {
       needsresetoncebeforetest ="false";
       needsresetbeforeeverymethod ="true";
       captureverystepsnap ="true";
       appendlastsnap ="false";
       ishybridapp ="true";

    }


    @Test
    public void tc01createpolicy() {

        // No need to declare / print method name here, handled by TestNg listener
        // declare("tc01createpolicy");

        CALL.$("LoginToGIAS4","TGTYPESCREENREG");

        TYPE.$("ELE_USERNAMETEXTFIELD","//input[@id='guestForm:username']","x5823","FALSE","TGTYPESCREENREG");

        TYPE.$("ELE_PASSWORDTEXTFIELD","//input[@id='guestForm:password']","Welcome123$","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_LOGINBUTTON","//input[@id='guestForm:loginButton']","TGTYPESCREENREG");

        CALL.$("AddDistributionChannel","TGTYPESCREENREG");

        TAP.$("ELE_DASHBOARDPAGENUMBERTWO","//div[@id='workbenchTabs:tabForm0:menuboardContainer_paginator_top']//a[@class='ui-paginator-page ui-state-default ui-corner-all'][contains(text(),'2')]","TGTYPESCREENREG");

        TAP.$("ELE_DISTRIBUTIONCHANNELLINK","//span[contains(text(),'Distribution Channel')]","TGTYPESCREENREG");

        TAP.$("ELE_ADDBUTTON","//span[contains(text(),'Add')]","TGTYPESCREENREG");

        TYPE.$("ELE_DISTRIBUTIONCHANNELNAME","//input[@id='workbenchTabs:tabForm2:column_panel_0:distributionChanName']","asdasdas","TRUE","TGTYPESCREENREG");

        TYPE.$("ELE_DISTRIBUTIONCHANNELDESCRIPTION","//input[@id='workbenchTabs:tabForm2:column_panel_0:channelDescription']","qweqwe","TRUE","TGTYPESCREENREG");

        TAP.$("ELE_VALIDATEBUTTON","//span[contains(text(),'Validate')]","TGTYPESCREENREG");

        TAP.$("ELE_SAVEBUTTON","//span[contains(text(),'Save')]","TGTYPESCREENREG");

        TAP.$("ELE_CLOSEICON1","(//span[@class='ui-icon ui-icon-close'])[1]","TGTYPESCREENREG");

        TAP.$("ELE_CLOSINGTABICON2","(//span[@class='ui-icon ui-icon-close'])[2]","TGTYPESCREENREG");

        CALL.$("AddAgentRankCodes","TGTYPESCREENREG");

        TAP.$("ELE_MYDASHBOARDTAB","//a[contains(text(),'My Dashboard')]","TGTYPESCREENREG");

        TAP.$("ELE_AGENTRANKCODESLINK","//span[contains(text(),'Agent Rank Codes')]","TGTYPESCREENREG");

        TAP.$("ELE_ADDBUTTON","//span[contains(text(),'Add')]","TGTYPESCREENREG");

        SELECT.$("ELE_RANKCODEDISTRIBUTIONCHANNELDRPDWN","//label[@id='workbenchTabs:tabForm5:column_panel_0:distributionChannel_label']",1,"TGTYPESCREENREG");

        TYPE.$("ELE_RANKLEVELTEXTFIELD","//input[@id='workbenchTabs:tabForm5:column_panel_0:agencyRankLevel_input']","32425","FALSE","TGTYPESCREENREG");

        TYPE.$("ELE_RANKCODETEXTFIELD","//input[@id='workbenchTabs:tabForm5:column_panel_0:agencyRankCode']","adadad","FALSE","TGTYPESCREENREG");

        TYPE.$("ELE_RANKDESCRIPTIONTEXTFIELD","//input[@id='workbenchTabs:tabForm5:column_panel_0:description']","dsdfsdf","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_VALIDATEBUTTON","//span[contains(text(),'Validate')]","TGTYPESCREENREG");

        TAP.$("ELE_SAVEBUTTON","//span[contains(text(),'Save')]","TGTYPESCREENREG");

        TAP.$("ELE_AGENTRANKCODESTAB","//a[contains(text(),'Agent Rank Codes')]","TGTYPESCREENREG");

        TAP.$("ELE_CLOSINGTABICON","//li[@class='ui-state-default ui-corner-top ui-state-hover ui-tabs-selected ui-state-active']//span[@class='ui-icon ui-icon-close']","TGTYPESCREENREG");

        TAP.$("ELE_ADDAGENTRANKCODESTAB","//a[contains(text(),'Add Agent Rank Codes')]","TGTYPESCREENREG");

        TAP.$("ELE_CLOSINGTABICON","//li[@class='ui-state-default ui-corner-top ui-state-hover ui-tabs-selected ui-state-active']//span[@class='ui-icon ui-icon-close']","TGTYPESCREENREG");

        CALL.$("AddCommissionScheduleRates","TGTYPESCREENREG");

        TAP.$("ELE_DASHBOARDPAGENUMBERTWO","//div[@id='workbenchTabs:tabForm0:menuboardContainer_paginator_top']//a[@class='ui-paginator-page ui-state-default ui-corner-all'][contains(text(),'2')]","TGTYPESCREENREG");

        TAP.$("ELE_COMMISSIONSCHEDULERATESLINK","//span[contains(text(),'Commission Schedule Rates')]","TGTYPESCREENREG");

        TAP.$("ELE_ADDBUTTON","//span[contains(text(),'Add')]","TGTYPESCREENREG");

        TYPE.$("ELE_COMMISSIONSCHEDULERATESTABLENAME","//input[@id='workbenchTabs:tabForm2:column_panel_0:tableName']","addja","FALSE","TGTYPESCREENREG");

        TYPE.$("ELE_COMMISSIONSCHEDULERATESDESCRIPTION","//input[@id='workbenchTabs:tabForm2:column_panel_0:description']","afsfsf","FALSE","TGTYPESCREENREG");

        SELECT.$("ELE_COMMSCHRATESDISTRIBUTIONCHANNELDRPDWN","//label[@id='workbenchTabs:tabForm2:column_panel_0:distributionChannel_label']",1,"TGTYPESCREENREG");

        TAP.$("ELE_CONTINUEBUTTON","//span[contains(text(),'Continue')]","TGTYPESCREENREG");

        TYPE.$("ELE_COMMSCHRATESDURATION1","//input[@id='workbenchTabs:tabForm2:rateTable:endingDuration1_input']","0","FALSE","TGTYPESCREENREG");

        TYPE.$("ELE_COMMSCHRATESDURATION2","//input[@id='workbenchTabs:tabForm2:rateTable:endingDuration2_input']","120","FALSE","TGTYPESCREENREG");

        SELECT.$("ELE_COMMRATESAGENCYRANKCODEDRPDWN","//select[@id='workbenchTabs:tabForm2:rateTable:0:in_agencyRankCode_Col']","abc","TGTYPESCREENREG");

        TYPE.$("ELE_COMMSCHRATESCOMMISSIONRATE1","//span[@id='workbenchTabs:tabForm2:rateTable:0:out_commissionRate1']x","33.33","FALSE","TGTYPESCREENREG");

        TYPE.$("ELE_COMMSCHRATESCOMMISSIONRATE2","//span[@id='workbenchTabs:tabForm2:rateTable:0:out_commissionRate2']","33.33","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_ADDCOMMISSIONSCHEDULERATESTEXT","//a[contains(text(),'Add Commission Schedule Rates')]","TGTYPESCREENREG");

        TAP.$("ELE_CLOSINGTABICON","//li[@class='ui-state-default ui-corner-top ui-state-hover ui-tabs-selected ui-state-active']//span[@class='ui-icon ui-icon-close']","TGTYPESCREENREG");

        TAP.$("ELE_COMMISSIONSCHEDULERATESTEXT","//a[contains(text(),'Commission Schedule Rates')]","TGTYPESCREENREG");

        TAP.$("ELE_CLOSINGTABICON","//li[@class='ui-state-default ui-corner-top ui-state-hover ui-tabs-selected ui-state-active']//span[@class='ui-icon ui-icon-close']","TGTYPESCREENREG");

        CALL.$("AddCommissionContracts","TGTYPESCREENREG");

        TAP.$("ELE_MYDASHBOARDTAB","//a[contains(text(),'My Dashboard')]","TGTYPESCREENREG");

        TAP.$("ELE_DASHBOARDPAGENUMBERTWO","//div[@id='workbenchTabs:tabForm0:menuboardContainer_paginator_top']//a[@class='ui-paginator-page ui-state-default ui-corner-all'][contains(text(),'2')]","TGTYPESCREENREG");

        TAP.$("ELE_COMMISSIONCONTRACTSLINK","//span[contains(text(),'Commission Contracts')]","TGTYPESCREENREG");

        TAP.$("ELE_ADDBUTTON","//span[contains(text(),'Add')]","TGTYPESCREENREG");

        TYPE.$("ELE_COMMCONTRACTNUMBERTEXTFIELD","//input[@id='workbenchTabs:tabForm2:column_panel_0:commissionContNumb_input']","1002","FALSE","TGTYPESCREENREG");

        TYPE.$("ELE_COMMICONTRACTDESCRIPTIOTEXTFIELD","//input[@id='workbenchTabs:tabForm2:column_panel_0:longDescription']","adaf","FALSE","TGTYPESCREENREG");

        SELECT.$("ELE_DISTRIBUTIONCHANNELDESCRIPTION","//input[@id='workbenchTabs:tabForm2:column_panel_0:channelDescription']",1,"TGTYPESCREENREG");

        TYPE.$("ELE_COMMISSIONCONTRACTEFFECTIVEDATETEXTFIELD","//input[@id='workbenchTabs:tabForm2:column_panel_0:effectiveDate_input']","nncncn","FALSE","TGTYPESCREENREG");

        TYPE.$("ELE_COMMISSIONCONTRACTTERMINATIONDATETEXTFIELD","//input[@id='workbenchTabs:tabForm2:column_panel_0:terminationDate_input']","hdfj","FALSE","TGTYPESCREENREG");

        SELECT.$("ELE_COMMCONTRACTADVANCEPAYBACKMETHODDRPDWN","//label[@id='workbenchTabs:tabForm2:column_panel_1:advancePaybackMethod_label']","100% Commission Earned","TGTYPESCREENREG");

        WAIT.$("ELE_COMMCONTRACTADVANCETHROUGHAGENTRANKDRPDWN","//label[@id='workbenchTabs:tabForm2:column_panel_1:topLevelForAdvance_label']","VISIBLE","TGTYPESCREENREG");

        SELECT.$("ELE_COMMCONTRACTADVANCETHROUGHAGENTRANKDRPDWN","//label[@id='workbenchTabs:tabForm2:column_panel_1:topLevelForAdvance_label']","rank 1","TGTYPESCREENREG");

        TAP.$("ELE_VALIDATEBUTTON","//span[contains(text(),'Validate')]","TGTYPESCREENREG");

        TAP.$("ELE_SAVEBUTTON","//span[contains(text(),'Save')]","TGTYPESCREENREG");

        CALL.$("AddCommissionSchedule","TGTYPESCREENREG");

        TAP.$("ELE_COMMISSIONCONTRACTSLINK","//span[contains(text(),'Commission Contracts')]","TGTYPESCREENREG");

        TYPE.$("ELE_COMMCONTRACTNUMBERTEXTFIELD","//input[@id='workbenchTabs:tabForm2:column_panel_0:commissionContNumb_input']","1002","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_COMMCONTRACTIDLINK","//span[@id='workbenchTabs:tabForm1:grid:0:commissionContNumb']","TGTYPESCREENREG");

        TAP.$("ELE_CREATEPOLICYSHOWLINK","//span[contains(text(),'Show Links')]","TGTYPESCREENREG");

        TAP.$("ELE_COMMISSIONSCHEDULELINK","//span[contains(text(),'Commission Schedule')]","TGTYPESCREENREG");

        TAP.$("ELE_ADDBUTTON","//span[contains(text(),'Add')]","TGTYPESCREENREG");

        SELECT.$("ELE_COMMCONTRACTTYPEDRPDWN","//label[@id='workbenchTabs:tabForm4:column_panel_0:commissionContType_label']","25%","TGTYPESCREENREG");

        TYPE.$("ELE_COMMISSIONSCHEDULEEFFECTIVEDATE","//input[@id='workbenchTabs:tabForm4:column_panel_0:effectiveDate_input']","current date or any prior date date","FALSE","TGTYPESCREENREG");

        SELECT.$("ELE_COMMSCHEDULERATETABLENAMEDRPDWN","//label[@id='workbenchTabs:tabForm4:column_panel_0:commissionRateTableName_label']","4FCMSC1002 Table Description","TGTYPESCREENREG");

        TYPE.$("ELE_COMMSCHEDULELINEDESCRIPTIONTEXTFIELD","//input[@id='workbenchTabs:tabForm4:column_panel_0:shortDescription']","4FComm Sch Line Desc","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_VALIDATEBUTTON","//span[contains(text(),'Validate')]","TGTYPESCREENREG");

        TAP.$("ELE_SAVEBUTTON","//span[contains(text(),'Save')]","TGTYPESCREENREG");

        CALL.$("AddPolicyContract","TGTYPESCREENREG");

        TAP.$("ELE_MYDASHBOARDTAB","//a[contains(text(),'My Dashboard')]","TGTYPESCREENREG");

        TAP.$("ELE_APPLICATIONENTRYUPDATLINK","//span[contains(text(),'Application Entry/Update')]","TGTYPESCREENREG");

        TAP.$("ELE_APPLICATIONENTRYUPDATETAB","//a[contains(text(),'Application Entry/Update')]","TGTYPESCREENREG");

        TAP.$("ELE_ADDBUTTON","//span[contains(text(),'Add')]","TGTYPESCREENREG");

        TYPE.$("ELE_POLICYNUMBERTEXTFIELD","//input[@id='workbenchTabs:tabForm2:column_panel_0:policyNumber']","4F1AUT1002","FALSE","TGTYPESCREENREG");

        TYPE.$("ELE_CREATEPOLICYAPPLICATIONNUMBERTEXTFIELD","//input[@id='workbenchTabs:tabForm2:column_panel_1:applicationNumber']","1234","FALSE","TGTYPESCREENREG");

        TYPE.$("ELE_CREATEPOLICYCASHWITHAPPLICATIONTEXTFIELD","//input[@id='workbenchTabs:tabForm2:column_panel_1:cashWithApplication_input']","100.00","FALSE","TGTYPESCREENREG");

        SCROLL.$("ELE_FOOTER","//img[@id='footerImage2']","DOWN","TGTYPESCREENREG");

        SWIPE.$("DOWN","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

        SWIPE.$("DOWN","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

        TYPE.$("ELE_CREATEPOLICYAGENTNUMBER","workbenchTabs:tabForm2:grid:0:in_agentNumber_ColTGWEBCOMMAIdTGWEBCOMMA0","A13000000","FALSE","TGTYPESCREENREG");

        TYPE.$("ELE_CREATEPOLICYSPLITPERCENTTEXTFIELD","workbenchTabs:tabForm2:grid:0:out_agentSplitPercentage_ColTGWEBCOMMAIdTGWEBCOMMA0","80","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_VALIDATEBUTTON","//span[contains(text(),'Validate')]","TGTYPESCREENREG");

        TAP.$("ELE_SAVEBUTTON","//span[contains(text(),'Save')]","TGTYPESCREENREG");

        TAP.$("ELE_CREATEPOLICYSHOWLINK","//span[contains(text(),'Show Links')]","TGTYPESCREENREG");

        TAP.$("ELE_CREATEPOLICYSELECTCLIENT","//span[contains(text(),'Select Client')]","TGTYPESCREENREG");

        TYPE.$("ELE_SELECTCLIENTSEARCHFIELD","//input[@class='ui-inputfield ui-inputtext ui-widget ui-state-default ui-corner-all InputGeneral']","Address Test","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_SEARCHBUTTON","//span[contains(text(),'Search')]","TGTYPESCREENREG");

        TAP.$("ELE_SELECTBUTTON","//span[contains(text(),'Select')]","TGTYPESCREENREG");

        TAP.$("ELE_CONTINUEBUTTON","//span[contains(text(),'Continue')]","TGTYPESCREENREG");

        TAP.$("ELE_SAVEBUTTON","//span[contains(text(),'Save')]","TGTYPESCREENREG");

        TAP.$("ELE_CREATEPOLICYSHOWLINK","//span[contains(text(),'Show Links')]","TGTYPESCREENREG");

        TAP.$("ELE_BENEFITOPTION","//span[contains(text(),'Benefits')]","TGTYPESCREENREG");

        TAP.$("ELE_ADDNEWBUSINESSBENEFITTAB","//a[contains(text(),'Add New Business Benefit')]","TGTYPESCREENREG");

        TAP.$("ELE_CONTINUEBUTTON","//span[contains(text(),'Continue')]","TGTYPESCREENREG");

        TAP.$("ELE_SAVEBUTTON","//span[contains(text(),'Save')]","TGTYPESCREENREG");

        TYPE.$("ELE_UNITSOFINSURANCE","//input[@class='ui-inputfield ui-inputtext ui-widget ui-state-default ui-corner-all ui-state-error']","10.000","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_SAVEBUTTON","//span[contains(text(),'Save')]","TGTYPESCREENREG");

        TAP.$("ELE_DISPLAYNEWBUSINESSPOLICYCONTRACTTAB"," //a[contains(text(),'Display New Business Policy Contract')]","TGTYPESCREENREG");

        TAP.$("ELE_CREATEPOLICYSHOWLINK","//span[contains(text(),'Show Links')]","TGTYPESCREENREG");

        TAP.$("ELE_ISSUEOPTION","//span[contains(text(),'Issue')]","TGTYPESCREENREG");

        TAP.$("ELE_UPDATESTATUSCHANGETOACTIVETAB","//a[contains(text(),'Update Status Change to Active')]","TGTYPESCREENREG");

        TYPE.$("ELE_ISSUEEFFECTIVEDATE","//input[@class='ui-inputfield ui-widget ui-state-default ui-corner-all hasDatepicker']","03/27/2019","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_SUBMITBUTTON","//span[contains(text(),'Submit')]","TGTYPESCREENREG");

        TAP.$("ELE_DISPLAYNEWBUSINESSPOLICYCONTRACTTAB"," //a[contains(text(),'Display New Business Policy Contract')]","TGTYPESCREENREG");

        TAP.$("ELE_CREATEPOLICYSHOWLINK","//span[contains(text(),'Show Links')]","TGTYPESCREENREG");

        TAP.$("ELE_SETTLEOPTION","//span[contains(text(),'Settle')]","TGTYPESCREENREG");

        TAP.$("ELE_SUBMITBUTTON","//span[contains(text(),'Submit')]","TGTYPESCREENREG");


    }


    @Test
    public void policysurrender() {

        // No need to declare / print method name here, handled by TestNg listener
        // declare("policysurrender");

    }


    @Test
    public void suspensemaintenance() {

        // No need to declare / print method name here, handled by TestNg listener
        // declare("suspensemaintenance");

    }


    @Test
    public void tc01createpolicyonly() {

        // No need to declare / print method name here, handled by TestNg listener
        // declare("tc01createpolicyonly");

        CALL.$("LoginToGIAS4","TGTYPESCREENREG");

        TYPE.$("ELE_USERNAMETEXTFIELD","//input[@id='guestForm:username']","x5823","FALSE","TGTYPESCREENREG");

        TYPE.$("ELE_PASSWORDTEXTFIELD","//input[@id='guestForm:password']","Welcome123$","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_LOGINBUTTON","//input[@id='guestForm:loginButton']","TGTYPESCREENREG");

        CALL.$("AddPolicyContract","TGTYPESCREENREG");

        TAP.$("ELE_MYDASHBOARDTAB","//a[contains(text(),'My Dashboard')]","TGTYPESCREENREG");

        TAP.$("ELE_APPLICATIONENTRYUPDATLINK","//span[contains(text(),'Application Entry/Update')]","TGTYPESCREENREG");

        TAP.$("ELE_APPLICATIONENTRYUPDATETAB","//a[contains(text(),'Application Entry/Update')]","TGTYPESCREENREG");

        TAP.$("ELE_ADDBUTTON","//span[contains(text(),'Add')]","TGTYPESCREENREG");

        TYPE.$("ELE_POLICYNUMBERTEXTFIELD","//input[@id='workbenchTabs:tabForm2:column_panel_0:policyNumber']","4F1AUT1002","FALSE","TGTYPESCREENREG");

        TYPE.$("ELE_CREATEPOLICYAPPLICATIONNUMBERTEXTFIELD","//input[@id='workbenchTabs:tabForm2:column_panel_1:applicationNumber']","1234","FALSE","TGTYPESCREENREG");

        TYPE.$("ELE_CREATEPOLICYCASHWITHAPPLICATIONTEXTFIELD","//input[@id='workbenchTabs:tabForm2:column_panel_1:cashWithApplication_input']","100.00","FALSE","TGTYPESCREENREG");

        SCROLL.$("ELE_FOOTER","//img[@id='footerImage2']","DOWN","TGTYPESCREENREG");

        SWIPE.$("DOWN","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

        SWIPE.$("DOWN","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

        TYPE.$("ELE_CREATEPOLICYAGENTNUMBER","workbenchTabs:tabForm2:grid:0:in_agentNumber_ColTGWEBCOMMAIdTGWEBCOMMA0","A13000000","FALSE","TGTYPESCREENREG");

        TYPE.$("ELE_CREATEPOLICYSPLITPERCENTTEXTFIELD","workbenchTabs:tabForm2:grid:0:out_agentSplitPercentage_ColTGWEBCOMMAIdTGWEBCOMMA0","80","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_VALIDATEBUTTON","//span[contains(text(),'Validate')]","TGTYPESCREENREG");

        TAP.$("ELE_SAVEBUTTON","//span[contains(text(),'Save')]","TGTYPESCREENREG");

        TAP.$("ELE_CREATEPOLICYSHOWLINK","//span[contains(text(),'Show Links')]","TGTYPESCREENREG");

        TAP.$("ELE_CREATEPOLICYSELECTCLIENT","//span[contains(text(),'Select Client')]","TGTYPESCREENREG");

        TYPE.$("ELE_SELECTCLIENTSEARCHFIELD","//input[@class='ui-inputfield ui-inputtext ui-widget ui-state-default ui-corner-all InputGeneral']","Address Test","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_SEARCHBUTTON","//span[contains(text(),'Search')]","TGTYPESCREENREG");

        TAP.$("ELE_SELECTBUTTON","//span[contains(text(),'Select')]","TGTYPESCREENREG");

        TAP.$("ELE_CONTINUEBUTTON","//span[contains(text(),'Continue')]","TGTYPESCREENREG");

        TAP.$("ELE_SAVEBUTTON","//span[contains(text(),'Save')]","TGTYPESCREENREG");

        TAP.$("ELE_CREATEPOLICYSHOWLINK","//span[contains(text(),'Show Links')]","TGTYPESCREENREG");

        TAP.$("ELE_BENEFITOPTION","//span[contains(text(),'Benefits')]","TGTYPESCREENREG");

        TAP.$("ELE_ADDNEWBUSINESSBENEFITTAB","//a[contains(text(),'Add New Business Benefit')]","TGTYPESCREENREG");

        TAP.$("ELE_CONTINUEBUTTON","//span[contains(text(),'Continue')]","TGTYPESCREENREG");

        TAP.$("ELE_SAVEBUTTON","//span[contains(text(),'Save')]","TGTYPESCREENREG");

        TYPE.$("ELE_UNITSOFINSURANCE","//input[@class='ui-inputfield ui-inputtext ui-widget ui-state-default ui-corner-all ui-state-error']","10.000","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_SAVEBUTTON","//span[contains(text(),'Save')]","TGTYPESCREENREG");

        TAP.$("ELE_DISPLAYNEWBUSINESSPOLICYCONTRACTTAB"," //a[contains(text(),'Display New Business Policy Contract')]","TGTYPESCREENREG");

        TAP.$("ELE_CREATEPOLICYSHOWLINK","//span[contains(text(),'Show Links')]","TGTYPESCREENREG");

        TAP.$("ELE_ISSUEOPTION","//span[contains(text(),'Issue')]","TGTYPESCREENREG");

        TAP.$("ELE_UPDATESTATUSCHANGETOACTIVETAB","//a[contains(text(),'Update Status Change to Active')]","TGTYPESCREENREG");

        TYPE.$("ELE_ISSUEEFFECTIVEDATE","//input[@class='ui-inputfield ui-widget ui-state-default ui-corner-all hasDatepicker']","03/27/2019","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_SUBMITBUTTON","//span[contains(text(),'Submit')]","TGTYPESCREENREG");

        TAP.$("ELE_DISPLAYNEWBUSINESSPOLICYCONTRACTTAB"," //a[contains(text(),'Display New Business Policy Contract')]","TGTYPESCREENREG");

        TAP.$("ELE_CREATEPOLICYSHOWLINK","//span[contains(text(),'Show Links')]","TGTYPESCREENREG");

        TAP.$("ELE_SETTLEOPTION","//span[contains(text(),'Settle')]","TGTYPESCREENREG");

        TAP.$("ELE_SUBMITBUTTON","//span[contains(text(),'Submit')]","TGTYPESCREENREG");


    }


}
