package appium.tgo;

import com.tg.octopus.base.TGOWrapper;
import com.tg.octopus.libs.testng.listener.TestListener;
import static com.tg.octopus.base.TGOWrapper.Action.*;
import static com.tg.octopus.base.TGOWrapper.LOGGER;
import com.tg.octopus.wrapper.ActionWrapper;

import com.jayway.jsonpath.JsonPath;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.SessionNotCreatedException;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.Augmenter;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.remote.UnreachableBrowserException;

import org.testng.Assert;
import org.testng.ITestContext;
import org.testng.ITestResult;
import org.testng.Reporter;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;
import java.net.HttpURLConnection;
import java.net.URL;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import org.json.JSONArray;
import org.json.JSONObject;
import helper.WebTestUtility;
import java.sql.*;
import com.testautomationguru.utility.PDFUtil;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.multipdf.Splitter;

import java.util.*;
import org.openqa.selenium.*;
import java.io.*;
import java.net.*;

import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;


@Listeners(TestListener.class)
public class CreatePolicy_Settled extends TGOWrapper {



    static {
       needsresetoncebeforetest ="false";
       needsresetbeforeeverymethod ="true";
       captureverystepsnap ="true";
       appendlastsnap ="false";
       ishybridapp ="true";

    }


    @Test
    public void t01createnewbusinesspolicy() {

        // No need to declare / print method name here, handled by TestNg listener
        // declare("t01createnewbusinesspolicy");

        CALL.$("loginToGIAS","TGTYPESCREENREG");

        TYPE.$("ELE_USERNAMETEXTFIELD","outerForm:usernameTGWEBCOMMAIdTGWEBCOMMA0","Raghu","TRUE","TGTYPESCREENREG");

        TYPE.$("ELE_PASSWORDTEXTFIELD","outerForm:passwordTGWEBCOMMAIdTGWEBCOMMA0","123456a","TRUE","TGTYPESCREENREG");

        TAP.$("ELE_LOGINBUTTON","outerForm:loginButtonTGWEBCOMMAIdTGWEBCOMMA0","TGTYPESCREENREG");

		int var_Count;
                 LOGGER.info("Executed Step = VAR,Integer,var_Count,TGTYPESCREENREG");
		var_Count = 0;
                 LOGGER.info("Executed Step = STORE,var_Count,0,TGTYPESCREENREG");
        LOGGER.info("Executed Step = REPEAT IF");
    	while (check(var_Count,"<",2,"TGTYPESCREENREG")) {
          LOGGER.info("Executed Step = WHILE,var_Count,<,2,TGTYPESCREENREG");
        CALL.$("AddDistributionChannel","TGTYPESCREENREG");

		com.jayway.jsonpath.ReadContext var_CSVData = getJsonData("C:/Users/x5823/Desktop/GIAS Base Regression/GIAS Automation/GIAS Automation/InputData.csv");
                 LOGGER.info("Executed Step = VAR,Json,var_CSVData,C:/Users/x5823/Desktop/GIAS Base Regression/GIAS Automation/GIAS Automation/InputData.csv,TGTYPESCREENREG");
		String var_DistributionChannelName;
                 LOGGER.info("Executed Step = VAR,String,var_DistributionChannelName,TGTYPESCREENREG");
		var_DistributionChannelName = getJsonData(var_CSVData , "$.records["+var_Count+"].DistributionChannelName");
                 LOGGER.info("Executed Step = STORE,var_DistributionChannelName,var_CSVData,$.records[{var_Count}].DistributionChannelName,TGTYPESCREENREG");
		String var_DistributionChannelDescription;
                 LOGGER.info("Executed Step = VAR,String,var_DistributionChannelDescription,TGTYPESCREENREG");
		var_DistributionChannelDescription = getJsonData(var_CSVData , "$.records["+var_Count+"].DistributionChannelDescription");
                 LOGGER.info("Executed Step = STORE,var_DistributionChannelDescription,var_CSVData,$.records[{var_Count}].DistributionChannelDescription,TGTYPESCREENREG");
        HOVER.$("ELE_AGENCYTAB","//td[text()='Agency']","TGTYPESCREENREG");

        TAP.$("ELE_DISTRIBUTIONCHANNELLINK","//td[text()='Distribution Channel']","TGTYPESCREENREG");

        TAP.$("ELE_ADDBUTTONDISTRIBUTIONCHANNEL","input[type=submit][name='outerForm:Add']TGWEBCOMMACssSelectorTGWEBCOMMA0","TGTYPESCREENREG");

        TYPE.$("ELE_DISTRIBUTIONCHANNELTEXTFIELD","//input[@type='text'][@name='outerForm:distributionChanName']",var_DistributionChannelName,"FALSE","TGTYPESCREENREG");

        TYPE.$("ELE_DESCRIPTIONTEXTFIELDFORDISTRICHANNAL","input[type=text][name='outerForm:channelDescription']TGWEBCOMMACssSelectorTGWEBCOMMA0",var_DistributionChannelDescription,"FALSE","TGTYPESCREENREG");

        TYPE.$("ELE_DISTRIBUTIONCHANNELCODETEXTFIELD","input[type=text][name='outerForm:duf_dist_code']TGWEBCOMMACssSelectorTGWEBCOMMA0",var_DistributionChannelName,"FALSE","TGTYPESCREENREG");

        TAP.$("ELE_SUBMITBUTTONDISTCHANNEL","input[type=submit][name='outerForm:Submit']TGWEBCOMMACssSelectorTGWEBCOMMA0","TGTYPESCREENREG");

        CALL.$("AddAgentRankCode","TGTYPESCREENREG");

        HOVER.$("ELE_AGENCYTAB","//td[text()='Agency']","TGTYPESCREENREG");

		String var_DistributionChannelDescription2;
                 LOGGER.info("Executed Step = VAR,String,var_DistributionChannelDescription2,TGTYPESCREENREG");
		var_DistributionChannelDescription2 = getJsonData(var_CSVData , "$.records["+var_Count+"].DistributionChannelDescription");
                 LOGGER.info("Executed Step = STORE,var_DistributionChannelDescription2,var_CSVData,$.records[{var_Count}].DistributionChannelDescription,TGTYPESCREENREG");
		String var_AgencyRankDescription;
                 LOGGER.info("Executed Step = VAR,String,var_AgencyRankDescription,TGTYPESCREENREG");
		var_AgencyRankDescription = getJsonData(var_CSVData , "$.records["+var_Count+"].AgencyRankDescription");
                 LOGGER.info("Executed Step = STORE,var_AgencyRankDescription,var_CSVData,$.records[{var_Count}].AgencyRankDescription,TGTYPESCREENREG");
		String var_AgencyRankCode;
                 LOGGER.info("Executed Step = VAR,String,var_AgencyRankCode,TGTYPESCREENREG");
		var_AgencyRankCode = getJsonData(var_CSVData , "$.records["+var_Count+"].AgencyRankCode");
                 LOGGER.info("Executed Step = STORE,var_AgencyRankCode,var_CSVData,$.records[{var_Count}].AgencyRankCode,TGTYPESCREENREG");
        TAP.$("ELE_AGENRANKCODESLINK","//td[text()='Agent Rank Codes']","TGTYPESCREENREG");

        TAP.$("ELE_ADDBUTTONAGENTCODES","//input[@type='submit'][@name='outerForm:Add']","TGTYPESCREENREG");

        SELECT.$("ELE_DROPDOWNDISTRIBUTIONCHANNELONAGENTCODES","//select[@name='outerForm:distributionChannel']",var_DistributionChannelDescription2,"TGTYPESCREENREG");

        TYPE.$("ELE_AGENCYRANKLAVELTEXTFIELD","//input[@type='text'][@name='outerForm:agencyRankLevel']","1","FALSE","TGTYPESCREENREG");

        TYPE.$("ELE_AGENCYRANKCODETEXTFIELD","//input[@type='text'][@name='outerForm:agencyRankCode']",var_AgencyRankCode,"FALSE","TGTYPESCREENREG");

        TYPE.$("ELE_AGENCYRANKDESCRIPTION","//input[@type='text'][@name='outerForm:description']",var_AgencyRankDescription,"FALSE","TGTYPESCREENREG");

        TAP.$("ELE_SUBMITBUTTONAGENTRANKCODESSCREEN","//input[@type='submit'][@name='outerForm:Submit']","TGTYPESCREENREG");

        CALL.$("AddCommissionScheduleRates","TGTYPESCREENREG");

        HOVER.$("ELE_AGENCYTAB","//td[text()='Agency']","TGTYPESCREENREG");

		String var_TableName;
                 LOGGER.info("Executed Step = VAR,String,var_TableName,TGTYPESCREENREG");
		var_TableName = getJsonData(var_CSVData , "$.records["+var_Count+"].TableName");
                 LOGGER.info("Executed Step = STORE,var_TableName,var_CSVData,$.records[{var_Count}].TableName,TGTYPESCREENREG");
		String var_TableDescription;
                 LOGGER.info("Executed Step = VAR,String,var_TableDescription,TGTYPESCREENREG");
		var_TableDescription = getJsonData(var_CSVData , "$.records["+var_Count+"].TableDescription");
                 LOGGER.info("Executed Step = STORE,var_TableDescription,var_CSVData,$.records[{var_Count}].TableDescription,TGTYPESCREENREG");
		String var_DistributionChannelDescription3;
                 LOGGER.info("Executed Step = VAR,String,var_DistributionChannelDescription3,TGTYPESCREENREG");
		var_DistributionChannelDescription3 = getJsonData(var_CSVData , "$.records["+var_Count+"].DistributionChannelDescription");
                 LOGGER.info("Executed Step = STORE,var_DistributionChannelDescription3,var_CSVData,$.records[{var_Count}].DistributionChannelDescription,TGTYPESCREENREG");
		String var_AgencyRankDescription3;
                 LOGGER.info("Executed Step = VAR,String,var_AgencyRankDescription3,TGTYPESCREENREG");
		var_AgencyRankDescription3 = getJsonData(var_CSVData , "$.records["+var_Count+"].AgencyRankDescription");
                 LOGGER.info("Executed Step = STORE,var_AgencyRankDescription3,var_CSVData,$.records[{var_Count}].AgencyRankDescription,TGTYPESCREENREG");
        TAP.$("ELE_COMMISSIONSCHEDULERATESLINK","//td[text()='Commission Schedule Rates']","TGTYPESCREENREG");

        TAP.$("ELE_ADDBUTTONCOMMISSIONSCHEDULE","//input[@type='submit'][@name='outerForm:Add']","TGTYPESCREENREG");

        TYPE.$("ELE_TABLENAMETEXTFIELD","//input[@type='text'][@name='outerForm:tableName']",var_TableName,"FALSE","TGTYPESCREENREG");

        TYPE.$("ELE_TABLEDESCRIPTIONTEXTFIELD","//input[@type='text'][@name='outerForm:description']",var_TableDescription,"FALSE","TGTYPESCREENREG");

        SELECT.$("ELE_DROPDOWNDISTRIBUTIONCHANNELCOMMISSIONSCHEDULE","//select[@name='outerForm:distributionChannel']",var_DistributionChannelDescription3,"TGTYPESCREENREG");

        TAP.$("ELE_CONTINUEBUTTONCOMMISSIONSCHEDULE","//input[@type='submit'][@name='outerForm:Continue']","TGTYPESCREENREG");

        SELECT.$("ELE_DROPPDOWNAGENCYRANKCODE","//select[@name='outerForm:grid:0:agencyRankCode']",var_AgencyRankDescription,"TGTYPESCREENREG");

        TYPE.$("ELE_ENDDURATION0TEXTFIELD","//input[@type='text'][@name='outerForm:grid:endingDuration1']","1","FALSE","TGTYPESCREENREG");

        TYPE.$("ELE_ENDDURATION1TEXTFIELD","//input[@type='text'][@name='outerForm:grid:endingDuration2']","5","FALSE","TGTYPESCREENREG");

        TYPE.$("ELE_ENDDURATION2TEXTFIELD","//input[@type='text'][@name='outerForm:grid:endingDuration3']","120","FALSE","TGTYPESCREENREG");

        TYPE.$("ELE_COMMISSIONRATE0TEXTFIELD","//input[@type='text'][@name='outerForm:grid:0:commissionRate1']","20","FALSE","TGTYPESCREENREG");

        TYPE.$("ELE_COMMISSIONRATE1TEXTFIELD","//input[@type='text'][@name='outerForm:grid:0:commissionRate2']","15","FALSE","TGTYPESCREENREG");

        TYPE.$("ELE_COMMISSIONRATE2TEXTFIELD","//input[@type='text'][@name='outerForm:grid:0:commissionRate3']","10","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_SUBMITBUTTONCOMMISSIONSCHEDULE","//input[@type='submit'][@name='outerForm:Submit']","TGTYPESCREENREG");

        CALL.$("AddCommissionContract","TGTYPESCREENREG");

        HOVER.$("ELE_AGENCYTAB","//td[text()='Agency']","TGTYPESCREENREG");

		String var_CommissionContractNumber;
                 LOGGER.info("Executed Step = VAR,String,var_CommissionContractNumber,TGTYPESCREENREG");
		var_CommissionContractNumber = getJsonData(var_CSVData , "$.records["+var_Count+"].CommissionContractNumber");
                 LOGGER.info("Executed Step = STORE,var_CommissionContractNumber,var_CSVData,$.records[{var_Count}].CommissionContractNumber,TGTYPESCREENREG");
		String var_CommissionContractDescription;
                 LOGGER.info("Executed Step = VAR,String,var_CommissionContractDescription,TGTYPESCREENREG");
		var_CommissionContractDescription = getJsonData(var_CSVData , "$.records["+var_Count+"].CommissionContractDescription");
                 LOGGER.info("Executed Step = STORE,var_CommissionContractDescription,var_CSVData,$.records[{var_Count}].CommissionContractDescription,TGTYPESCREENREG");
		String var_DistributionChannelDescription4;
                 LOGGER.info("Executed Step = VAR,String,var_DistributionChannelDescription4,TGTYPESCREENREG");
		var_DistributionChannelDescription4 = getJsonData(var_CSVData , "$.records["+var_Count+"].DistributionChannelDescription");
                 LOGGER.info("Executed Step = STORE,var_DistributionChannelDescription4,var_CSVData,$.records[{var_Count}].DistributionChannelDescription,TGTYPESCREENREG");
		String var_AgencyRankDescription4;
                 LOGGER.info("Executed Step = VAR,String,var_AgencyRankDescription4,TGTYPESCREENREG");
		var_AgencyRankDescription4 = getJsonData(var_CSVData , "$.records["+var_Count+"].AgencyRankDescription");
                 LOGGER.info("Executed Step = STORE,var_AgencyRankDescription4,var_CSVData,$.records[{var_Count}].AgencyRankDescription,TGTYPESCREENREG");
        TAP.$("ELE_COMMISSIONCONTRACTSLINK","//td[text()='Commission Contracts']","TGTYPESCREENREG");

        TAP.$("ELE_ADDBUTTONCOMMISSIONCONTRACT","//input[@type='submit'][@name='outerForm:Add']","TGTYPESCREENREG");

        TYPE.$("ELE_COMMSSIONCONTRACTNOTEXTFIELD","//input[@type='text'][@name='outerForm:commissionContNumb']",var_CommissionContractNumber,"FALSE","TGTYPESCREENREG");

        TYPE.$("ELE_COMMISSIONCONTRACTDESCTEXTFIELD","//input[@type='text'][@name='outerForm:longDescription']",var_CommissionContractDescription,"FALSE","TGTYPESCREENREG");

        SELECT.$("ELE_DISTRIBUTIONCHANNELLISTCOMMISSIONCONTRACT","//select[@name='outerForm:distributionChannel']",var_DistributionChannelDescription4,"TGTYPESCREENREG");

        TYPE.$("ELE_EFFECTIVEDATETEXTFIELD","//input[@type='text'][@name='outerForm:effectiveDate']","01/01/2010","FALSE","TGTYPESCREENREG");

        SELECT.$("ELE_PAYBACKMETHODLIST","//select[@name='outerForm:advancePaybackMethod']","100% of Commission Earned","TGTYPESCREENREG");

        SELECT.$("ELE_ADVANCETHROUGHAGENTRANKLIST","//select[@name='outerForm:topLevelForAdvance']",var_AgencyRankDescription4,"TGTYPESCREENREG");

        TYPE.$("ELE_DAYSPASTDUETEXTFIELD","//input[@type='text'][@name='outerForm:daysLateBeforeChar']","3","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_SUBMITBUTTONCOMMISSIONCONTRACT","//input[@type='submit'][@name='outerForm:Submit']","TGTYPESCREENREG");

        CALL.$("AddCommissionSchedule","TGTYPESCREENREG");

        HOVER.$("ELE_AGENCYTAB","//td[text()='Agency']","TGTYPESCREENREG");

		String var_CommissionContractNumber5;
                 LOGGER.info("Executed Step = VAR,String,var_CommissionContractNumber5,TGTYPESCREENREG");
		var_CommissionContractNumber5 = getJsonData(var_CSVData , "$.records["+var_Count+"].CommissionContractNumber");
                 LOGGER.info("Executed Step = STORE,var_CommissionContractNumber5,var_CSVData,$.records[{var_Count}].CommissionContractNumber,TGTYPESCREENREG");
		String var_TableDescription5;
                 LOGGER.info("Executed Step = VAR,String,var_TableDescription5,TGTYPESCREENREG");
		var_TableDescription5 = getJsonData(var_CSVData , "$.records["+var_Count+"].TableDescription");
                 LOGGER.info("Executed Step = STORE,var_TableDescription5,var_CSVData,$.records[{var_Count}].TableDescription,TGTYPESCREENREG");
		String var_LineDescription;
                 LOGGER.info("Executed Step = VAR,String,var_LineDescription,TGTYPESCREENREG");
		var_LineDescription = getJsonData(var_CSVData , "$.records["+var_Count+"].LineDescription");
                 LOGGER.info("Executed Step = STORE,var_LineDescription,var_CSVData,$.records[{var_Count}].LineDescription,TGTYPESCREENREG");
        TAP.$("ELE_COMMISSIONCONTRACTSLINK","//td[text()='Commission Contracts']","TGTYPESCREENREG");

        TYPE.$("ELE_COMMISSIONCONTRACTNOSEARCHTEXTFIELD","//input[@type='text'][@name='outerForm:grid:commissionContNumb']",var_CommissionContractNumber5,"FALSE","TGTYPESCREENREG");

        TAP.$("ELE_FINDPOSITIONTOBUTTON","//img[@id='outerForm:gnFindImage']","TGTYPESCREENREG");

        TAP.$("ELE_COMMISSIONCONTRACT2224","span[id='outerForm:grid:0:commissionContNumbOut1']TGWEBCOMMACssSelectorTGWEBCOMMA0","TGTYPESCREENREG");

        TAP.$("ELE_COMMISSIONSCHEDULELINK","//span[text()='Commission Schedule']","TGTYPESCREENREG");

        TAP.$("ELE_ADDBUTTONCOMMISSIONSCHEDULE","//input[@type='submit'][@name='outerForm:Add']","TGTYPESCREENREG");

        SELECT.$("ELE_CONTRACTTYPELIST","//select[@name='outerForm:commissionContType']","Health","TGTYPESCREENREG");

        TYPE.$("ELE_EFFECTIVEDATE2","//input[@type='text'][@name='outerForm:effectiveDate']","01/01/2019","FALSE","TGTYPESCREENREG");

        SELECT.$("ELE_COMMISSIONRATETABLELIST","//select[@name='outerForm:commissionRateTableName']",var_TableDescription5,"TGTYPESCREENREG");

        TYPE.$("ELE_LINEDESCRIPTIONTEXTFIELD","//input[@type='text'][@name='outerForm:shortDescription']",var_LineDescription,"FALSE","TGTYPESCREENREG");

        TAP.$("ELE_SUBMITTBUTTONADDCOMMSCHE","//input[@type='submit'][@name='outerForm:Submit']","TGTYPESCREENREG");

        CALL.$("AddNewBusinessPolicy","TGTYPESCREENREG");

        HOVER.$("ELE_NEWBUSINESSTAB","//td[text()='New Business']","TGTYPESCREENREG");

        TAP.$("ELE_APPLICATIONENTRYUPDATELINK","//td[text()='Application Entry/Update']","TGTYPESCREENREG");

        TAP.$("ELE_ADDBUTTONAPPLICATIONENTRY","//input[@type='submit'][@name='outerForm:Add']","TGTYPESCREENREG");

		String var_PolicyNumber;
                 LOGGER.info("Executed Step = VAR,String,var_PolicyNumber,TGTYPESCREENREG");
		var_PolicyNumber = getJsonData(var_CSVData , "$.records["+var_Count+"].PolicyNumber");
                 LOGGER.info("Executed Step = STORE,var_PolicyNumber,var_CSVData,$.records[{var_Count}].PolicyNumber,TGTYPESCREENREG");
		String var_FirstName;
                 LOGGER.info("Executed Step = VAR,String,var_FirstName,TGTYPESCREENREG");
		var_FirstName = getJsonData(var_CSVData , "$.records["+var_Count+"].FirstName");
                 LOGGER.info("Executed Step = STORE,var_FirstName,var_CSVData,$.records[{var_Count}].FirstName,TGTYPESCREENREG");
		String var_LastName;
                 LOGGER.info("Executed Step = VAR,String,var_LastName,TGTYPESCREENREG");
		var_LastName = getJsonData(var_CSVData , "$.records["+var_Count+"].LastName");
                 LOGGER.info("Executed Step = STORE,var_LastName,var_CSVData,$.records[{var_Count}].LastName,TGTYPESCREENREG");
		String var_IDNumber;
                 LOGGER.info("Executed Step = VAR,String,var_IDNumber,TGTYPESCREENREG");
		var_IDNumber = getJsonData(var_CSVData , "$.records["+var_Count+"].IDNumber");
                 LOGGER.info("Executed Step = STORE,var_IDNumber,var_CSVData,$.records[{var_Count}].IDNumber,TGTYPESCREENREG");
		String var_ClientSearchName;
                 LOGGER.info("Executed Step = VAR,String,var_ClientSearchName,TGTYPESCREENREG");
		var_ClientSearchName = getJsonData(var_CSVData , "$.records["+var_Count+"].ClientSearchName");
                 LOGGER.info("Executed Step = STORE,var_ClientSearchName,var_CSVData,$.records[{var_Count}].ClientSearchName,TGTYPESCREENREG");
		String var_AgentNumber;
                 LOGGER.info("Executed Step = VAR,String,var_AgentNumber,TGTYPESCREENREG");
		var_AgentNumber = getJsonData(var_CSVData , "$.records["+var_Count+"].AgentNumber");
                 LOGGER.info("Executed Step = STORE,var_AgentNumber,var_CSVData,$.records[{var_Count}].AgentNumber,TGTYPESCREENREG");
        TYPE.$("ELE_POLICYNUMBERTEXTFIELD","//input[@type='text'][@name='outerForm:policyNumber']",var_PolicyNumber,"FALSE","TGTYPESCREENREG");

        SELECT.$("ELE_CURRENCYCODELIST","//select[@name='outerForm:currencyCode']","Dollars [US]","TGTYPESCREENREG");

        SELECT.$("ELE_COUNTRYANDSTATECODELIST","//select[@name='outerForm:countryAndStateCode']","Missouri US","TGTYPESCREENREG");

        TYPE.$("ELE_CASHWITHAPPLICATIONTEXTFIELD","//input[@type='text'][@name='outerForm:cashWithApplication']","500000","FALSE","TGTYPESCREENREG");

        SELECT.$("ELE_PAYMENTFREQUENCYLIST","//select[@name='outerForm:paymentMode']","Annual","TGTYPESCREENREG");

        SELECT.$("ELE_PAYMENTMETHODLIST","//select[@name='outerForm:paymentCode']","Coupon Book","TGTYPESCREENREG");

        SELECT.$("ELE_TAXQUALIFIEDDESCRIPTIONLIST","//select[@name='outerForm:taxQualifiedCode']","NON QUALIFIED","TGTYPESCREENREG");

        SELECT.$("ELE_TAXWITHHOLDINGLIST","//select[@name='outerForm:taxWithholdingCode']","No Withholding","TGTYPESCREENREG");

        TAP.$("ELE_SELECTCLIENTLINK","//span[text()='Select Client']","TGTYPESCREENREG");

        TAP.$("ELE_ADDBUTTONCLIENTRELATIONSHOPPAGE","//input[@type='submit'][@name='outerForm:Add']","TGTYPESCREENREG");

        TYPE.$("ELE_FIRSTNAMETEXTFIELD","//input[@type='text'][@name='outerForm:nameFirst']",var_FirstName,"FALSE","TGTYPESCREENREG");

        TYPE.$("ELE_LASTNAMETEXTFIELD","//input[@type='text'][@name='outerForm:nameLast']",var_LastName,"FALSE","TGTYPESCREENREG");

        TYPE.$("ELE_CLIENT_DOB","//input[@type='text'][@name='outerForm:dateOfBirth2']","01/01/1980","FALSE","TGTYPESCREENREG");

        SELECT.$("ELE_CLIENT_GENDER_LIST","//select[@name='outerForm:sexCode2']","Male","TGTYPESCREENREG");

        SELECT.$("ELE_IDTYPELIST","//select[@name='outerForm:taxIdenUsag2']","Social Security Number","TGTYPESCREENREG");

        TYPE.$("ELE_IDNUMBERTEXTFIELD","//input[@type='text'][@name='outerForm:identificationNumber2']",var_IDNumber,"FALSE","TGTYPESCREENREG");

        TAP.$("ELE_SUBMITBUTTONADDCLIENTPAGE","//input[@type='submit'][@name='outerForm:Submit']","TGTYPESCREENREG");

        TYPE.$("ELE_ADDRESSLINE1TEXTFIELD","//input[@type='text'][@name='outerForm:addressLineOne']","123 Main St","FALSE","TGTYPESCREENREG");

        TYPE.$("ELE_CITYTEXTFIELD","//input[@type='text'][@name='outerForm:addressCity']","Kansas City","FALSE","TGTYPESCREENREG");

        SELECT.$("ELE_COUNTRYANDSTATECODELIST","//select[@name='outerForm:countryAndStateCode']","Missouri US","TGTYPESCREENREG");

        TYPE.$("ELE_ZIPCODETEXTFIELD","//input[@type='text'][@name='outerForm:zipCode']","64153","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_SUBMITBUTTONADDADDRESSPAGE","//input[@type='submit'][@name='outerForm:Submit']","TGTYPESCREENREG");

        TAP.$("ELE_SELECTCLIENTRELATIONSHIPSLINK","//a[text()='Select Client Relationships']","TGTYPESCREENREG");

        TYPE.$("ELE_CLIENTNAMESEARCHTEXTFIELD","//input[@type='text'][@name='outerForm:grid:individualName']",var_ClientSearchName,"FALSE","TGTYPESCREENREG");

        TAP.$("ELE_FINDPOSITIONTOCLIENTNAMEBUTTON","//img[@id='outerForm:gnFindImage']","TGTYPESCREENREG");

        SELECT.$("ELE_RELATIONSHIPGRID0LIST","//select[@name='outerForm:grid:0:relationship']","Owner 1","TGTYPESCREENREG");

        TAP.$("ELE_SUBMITBUTTONSELECTCLIENTRELATIONPAGE","//input[@type='submit'][@name='outerForm:Submit']","TGTYPESCREENREG");

        TYPE.$("ELE_AGENTNUMBERTEXTFIELD","//input[@type='text'][@name='outerForm:grid:0:agentNumberOut2']",var_AgentNumber,"FALSE","TGTYPESCREENREG");

        TAP.$("ELE_SUBMITBUTTONADDNEWBUSINESS","//*[@name='outerForm:Submit']","TGTYPESCREENREG");

        CALL.$("SettlePolicy","TGTYPESCREENREG");

        TAP.$("ELE_CLICKBENEFITSLINKAFTERCREATINGPOLICY","//span[text()='Benefits']","TGTYPESCREENREG");

        SELECT.$("ELE_DROPDOWNTOADDABENEFIT","//select[@name='outerForm:initialPlanCode']","016270 - OL with wpb (tot ben premium) and ADB","TGTYPESCREENREG");

        TAP.$("ELE_CONTINUEAFTERSELECTINGBENEFIT","//input[@type='submit'][@name='outerForm:Continue']","TGTYPESCREENREG");

        TYPE.$("ELE_TEXTTOENTERUNITSAFTERADDINGBENEFIT","//input[@type='text'][@name='outerForm:unitsOfInsurance']	","1.000","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_SUBMITBENEFITWITHUNITS","//input[@type='submit'][@name='outerForm:Submit']","TGTYPESCREENREG");

        TAP.$("ELE_CANCELAFTERADDINGBENEFITS","//input[@type='submit'][@name='outerForm:Cancel']","TGTYPESCREENREG");

        TAP.$("ELE_CLICKISSUEDECLINEWITHDRAWLINK","//span[text()='Issue/Decline/Withdraw']","TGTYPESCREENREG");

        TYPE.$("ELE_SELECTDATEINISSUEDECLINEWITHDRAWPAGE","//input[@type='text'][@name='outerForm:effectiveDate']","03/20/2019","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_CLICKISSUEBUTTONTOISSUETHEPOLICY","//input[@type='submit'][@name='outerForm:Issue']	","TGTYPESCREENREG");

        TAP.$("ELE_CLICKSETTLELINKTOSETTLETHEPOLICY","//span[text()='Settle']	","TGTYPESCREENREG");

        TAP.$("ELE_CLICKSUBMITTOSETTLETHEPOLICY","//input[@type='submit'][@name='outerForm:Submit']	","TGTYPESCREENREG");

        CALL.$("WritePolicyDetailToCSV","TGTYPESCREENREG");

        HOVER.$("ELE_NEWBUSINESSTAB","//td[text()='New Business']","TGTYPESCREENREG");

        TAP.$("ELE_APPLICATIONENTRYUPDATELINK","//td[text()='Application Entry/Update']","TGTYPESCREENREG");

        TYPE.$("ELE_POLICYNUMBERSEARCHFIELD2","//input[@type='text'][@name='outerForm:grid:policyNumber']",var_PolicyNumber,"FALSE","TGTYPESCREENREG");

        TAP.$("ELE_GOTOPOLICYBUTTON","//img[@id='outerForm:gnFindImage']","TGTYPESCREENREG");

        TAP.$("ELE_FIRSTPOLICYSEARCHRESULT","span[id='outerForm:grid:0:policyNumberOut1']TGWEBCOMMACssSelectorTGWEBCOMMA0","TGTYPESCREENREG");

		writeToCSV("ELE_POLICYNUMBER_POLICYPAGE " , ActionWrapper.getWebElementValueForVariable("span[id='outerForm:policyNumber']", "CssSelector", 0, "span[id='outerForm:policyNumber']TGWEBCOMMACssSelectorTGWEBCOMMA0"));
                 LOGGER.info("Executed Step = WRITETOCSV,ELE_POLICYNUMBER_POLICYPAGE,TGTYPESCREENREG");
		writeToCSV("ELE_POLICYSTATUS_POLICYPAGE " , ActionWrapper.getWebElementValueForVariable("span[id='outerForm:contractStatusNewBusText']", "CssSelector", 0, "span[id='outerForm:contractStatusNewBusText']TGWEBCOMMACssSelectorTGWEBCOMMA0"));
                 LOGGER.info("Executed Step = WRITETOCSV,ELE_POLICYSTATUS_POLICYPAGE,TGTYPESCREENREG");
		writeToCSV("ELE_POLICYSTATE_POLICYPAGE " , ActionWrapper.getWebElementValueForVariable("span[id='outerForm:countryAndStateCode']", "CssSelector", 0, "span[id='outerForm:countryAndStateCode']TGWEBCOMMACssSelectorTGWEBCOMMA0"));
                 LOGGER.info("Executed Step = WRITETOCSV,ELE_POLICYSTATE_POLICYPAGE,TGTYPESCREENREG");
		writeToCSV("ELE_CASHWITHAPPLICATION_POLICYPAGE " , ActionWrapper.getWebElementValueForVariable("span[id='outerForm:cashWithApplication']", "CssSelector", 0, "span[id='outerForm:cashWithApplication']TGWEBCOMMACssSelectorTGWEBCOMMA0"));
                 LOGGER.info("Executed Step = WRITETOCSV,ELE_CASHWITHAPPLICATION_POLICYPAGE,TGTYPESCREENREG");
		writeToCSV("ELE_AGENTNUMBER_POLICYPAGE " , ActionWrapper.getWebElementValueForVariable("span[id='outerForm:grid:0:agentNumberOut2']", "CssSelector", 0, "span[id='outerForm:grid:0:agentNumberOut2']TGWEBCOMMACssSelectorTGWEBCOMMA0"));
                 LOGGER.info("Executed Step = WRITETOCSV,ELE_AGENTNUMBER_POLICYPAGE,TGTYPESCREENREG");
		var_Count = var_Count + ( 1 );
                 LOGGER.info("Executed Step = INCREASEVAR,var_Count,1,TGTYPESCREENREG");
    	}
          LOGGER.info("Executed Step = WEND,TGTYPESCREENREG");
    	resetAttemps();

    }


}
