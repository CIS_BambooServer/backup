package appium.tgo;

import com.tg.octopus.base.TGOWrapper;
import com.tg.octopus.libs.testng.listener.TestListener;
import static com.tg.octopus.base.TGOWrapper.Action.*;
import static com.tg.octopus.base.TGOWrapper.LOGGER;
import com.tg.octopus.wrapper.ActionWrapper;

import com.jayway.jsonpath.JsonPath;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.SessionNotCreatedException;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.Augmenter;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.remote.UnreachableBrowserException;

import org.testng.Assert;
import org.testng.ITestContext;
import org.testng.ITestResult;
import org.testng.Reporter;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;
import java.net.HttpURLConnection;
import java.net.URL;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import org.json.JSONArray;
import org.json.JSONObject;
import helper.WebTestUtility;
import java.sql.*;
import com.testautomationguru.utility.PDFUtil;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.multipdf.Splitter;

import java.util.*;
import org.openqa.selenium.*;
import java.io.*;
import java.net.*;

import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;


@Listeners(TestListener.class)
public class TestCases extends TGOWrapper {



    static {
       needsresetoncebeforetest ="false";
       needsresetbeforeeverymethod ="true";
       captureverystepsnap ="true";
       appendlastsnap ="false";
       ishybridapp ="true";

    }


    @Test
    public void t01createnewbusinesspolicy() {

        // No need to declare / print method name here, handled by TestNg listener
        // declare("t01createnewbusinesspolicy");

        CALL.$("AddDistributionChannel","TGTYPESCREENREG");

        CALL.$("loginToGIAS","TGTYPESCREENREG");

        WAIT.$("ELE_USERNAMELABEL","outerForm:label_useridTGWEBCOMMAIdTGWEBCOMMA0","VISIBLE","TGTYPESCREENREG");

        ASSERT.$("ELE_USERNAMELABEL","outerForm:label_useridTGWEBCOMMAIdTGWEBCOMMA0","VISIBLE","TGTYPESCREENREG");

        TYPE.$("ELE_USERNAMETEXTFIELD","outerForm:usernameTGWEBCOMMAIdTGWEBCOMMA0","Raghu","TRUE","TGTYPESCREENREG");

        WAIT.$("ELE_PASSWORDTEXTFIELD","outerForm:passwordTGWEBCOMMAIdTGWEBCOMMA0","VISIBLE","TGTYPESCREENREG");

        TYPE.$("ELE_PASSWORDTEXTFIELD","outerForm:passwordTGWEBCOMMAIdTGWEBCOMMA0","123456a","TRUE","TGTYPESCREENREG");

        WAIT.$("ELE_LOGINBUTTON","outerForm:loginButtonTGWEBCOMMAIdTGWEBCOMMA0","VISIBLE","TGTYPESCREENREG");

        ASSERT.$("ELE_LOGINBUTTON","outerForm:loginButtonTGWEBCOMMAIdTGWEBCOMMA0","VISIBLE","TGTYPESCREENREG");

        TAP.$("ELE_LOGINBUTTON","outerForm:loginButtonTGWEBCOMMAIdTGWEBCOMMA0","TGTYPESCREENREG");

        WAIT.$("ELE_HOMEPAGELABEL","HeaderTitle2TGWEBCOMMAIdTGWEBCOMMA0","VISIBLE","TGTYPESCREENREG");

        ASSERT.$("ELE_HOMEPAGELABEL","HeaderTitle2TGWEBCOMMAIdTGWEBCOMMA0","VISIBLE","TGTYPESCREENREG");

        HOVER.$("ELE_AGENCYTAB","//td[text()='Agency']","TGTYPESCREENREG");

        WAIT.$("ELE_DISTRIBUTIONCHANNELLINK","//td[text()='Distribution Channel']","VISIBLE","TGTYPESCREENREG");

        TAP.$("ELE_DISTRIBUTIONCHANNELLINK","//td[text()='Distribution Channel']","TGTYPESCREENREG");

        WAIT.$("ELE_ADDBUTTONDISTRIBUTIONCHANNEL","input[type=submit][name='outerForm:Add']TGWEBCOMMACssSelectorTGWEBCOMMA0","VISIBLE","TGTYPESCREENREG");

        TAP.$("ELE_ADDBUTTONDISTRIBUTIONCHANNEL","input[type=submit][name='outerForm:Add']TGWEBCOMMACssSelectorTGWEBCOMMA0","TGTYPESCREENREG");

        WAIT.$("ELE_DISTRIBUTIONCHANNELTEXTFIELD","//input[@type='text'][@name='outerForm:distributionChanName']","VISIBLE","TGTYPESCREENREG");

        TYPE.$("ELE_DISTRIBUTIONCHANNELTEXTFIELD","//input[@type='text'][@name='outerForm:distributionChanName']","AutoChan2","FALSE","TGTYPESCREENREG");

        TYPE.$("ELE_DESCRIPTIONTEXTFIELDFORDISTRICHANNAL","input[type=text][name='outerForm:channelDescription']TGWEBCOMMACssSelectorTGWEBCOMMA0","AutoChan2Description","FALSE","TGTYPESCREENREG");

        TYPE.$("ELE_DISTRIBUTIONCHANNELCODETEXTFIELD","input[type=text][name='outerForm:duf_dist_code']TGWEBCOMMACssSelectorTGWEBCOMMA0","AutoChan2","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_SUBMITBUTTONDISTCHANNEL","input[type=submit][name='outerForm:Submit']TGWEBCOMMACssSelectorTGWEBCOMMA0","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        CALL.$("AddAgentRankCode","TGTYPESCREENREG");

        HOVER.$("ELE_AGENCYTAB","//td[text()='Agency']","TGTYPESCREENREG");

        WAIT.$("ELE_AGENRANKCODESLINK","//td[text()='Agent Rank Codes']","VISIBLE","TGTYPESCREENREG");

        TAP.$("ELE_AGENRANKCODESLINK","//td[text()='Agent Rank Codes']","TGTYPESCREENREG");

        WAIT.$("ELE_ADDBUTTONAGENTCODES","//input[@type='submit'][@name='outerForm:Add']","VISIBLE","TGTYPESCREENREG");

        TAP.$("ELE_ADDBUTTONAGENTCODES","//input[@type='submit'][@name='outerForm:Add']","TGTYPESCREENREG");

        WAIT.$("ELE_DROPDOWNDISTRIBUTIONCHANNELONAGENTCODES","//select[@name='outerForm:distributionChannel']","VISIBLE","TGTYPESCREENREG");

        SELECT.$("ELE_DROPDOWNDISTRIBUTIONCHANNELONAGENTCODES","//select[@name='outerForm:distributionChannel']","AutoChan2Description","TGTYPESCREENREG");

        TYPE.$("ELE_AGENCYRANKLAVELTEXTFIELD","//input[@type='text'][@name='outerForm:agencyRankLevel']","1","FALSE","TGTYPESCREENREG");

        TYPE.$("ELE_AGENCYRANKCODETEXTFIELD","//input[@type='text'][@name='outerForm:agencyRankCode']","TGRank2","FALSE","TGTYPESCREENREG");

        TYPE.$("ELE_AGENCYRANKDESCRIPTION","//input[@type='text'][@name='outerForm:description']","AutoChan2AgencyRankDescription","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_SUBMITBUTTONAGENTRANKCODESSCREEN","//input[@type='submit'][@name='outerForm:Submit']","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        CALL.$("AddCommissionScheduleRates","TGTYPESCREENREG");

        HOVER.$("ELE_AGENCYTAB","//td[text()='Agency']","TGTYPESCREENREG");

        WAIT.$("ELE_COMMISSIONSCHEDULERATESLINK","//td[text()='Commission Schedule Rates']","VISIBLE","TGTYPESCREENREG");

        TAP.$("ELE_COMMISSIONSCHEDULERATESLINK","//td[text()='Commission Schedule Rates']","TGTYPESCREENREG");

        WAIT.$("ELE_ADDBUTTONCOMMISSIONSCHEDULE","//input[@type='submit'][@name='outerForm:Add']","VISIBLE","TGTYPESCREENREG");

        TAP.$("ELE_ADDBUTTONCOMMISSIONSCHEDULE","//input[@type='submit'][@name='outerForm:Add']","TGTYPESCREENREG");

        WAIT.$("ELE_TABLENAMETEXTFIELD","//input[@type='text'][@name='outerForm:tableName']","VISIBLE","TGTYPESCREENREG");

        TYPE.$("ELE_TABLENAMETEXTFIELD","//input[@type='text'][@name='outerForm:tableName']","AutoChan2Tab","FALSE","TGTYPESCREENREG");

        TYPE.$("ELE_TABLEDESCRIPTIONTEXTFIELD","//input[@type='text'][@name='outerForm:description']","AutoChan2TableDescription","FALSE","TGTYPESCREENREG");

        SELECT.$("ELE_DROPDOWNDISTRIBUTIONCHANNELCOMMISSIONSCHEDULE","//select[@name='outerForm:distributionChannel']","AutoChan2Description","TGTYPESCREENREG");

        WAIT.$("ELE_CONTINUEBUTTONCOMMISSIONSCHEDULE","//input[@type='submit'][@name='outerForm:Continue']","VISIBLE","TGTYPESCREENREG");

        TAP.$("ELE_CONTINUEBUTTONCOMMISSIONSCHEDULE","//input[@type='submit'][@name='outerForm:Continue']","TGTYPESCREENREG");

        WAIT.$("ELE_DROPPDOWNAGENCYRANKCODE","//select[@name='outerForm:grid:0:agencyRankCode']","VISIBLE","TGTYPESCREENREG");

        SELECT.$("ELE_DROPPDOWNAGENCYRANKCODE","//select[@name='outerForm:grid:0:agencyRankCode']","AutoChan2AgencyRankDescription","TGTYPESCREENREG");

        TYPE.$("ELE_ENDDURATION0TEXTFIELD","//input[@type='text'][@name='outerForm:grid:endingDuration1']","1","FALSE","TGTYPESCREENREG");

        TYPE.$("ELE_ENDDURATION1TEXTFIELD","//input[@type='text'][@name='outerForm:grid:endingDuration2']","5","FALSE","TGTYPESCREENREG");

        TYPE.$("ELE_ENDDURATION2TEXTFIELD","//input[@type='text'][@name='outerForm:grid:endingDuration3']","120","FALSE","TGTYPESCREENREG");

        TYPE.$("ELE_COMMISSIONRATE0TEXTFIELD","//input[@type='text'][@name='outerForm:grid:0:commissionRate1']","20","FALSE","TGTYPESCREENREG");

        TYPE.$("ELE_COMMISSIONRATE1TEXTFIELD","//input[@type='text'][@name='outerForm:grid:0:commissionRate2']","15","FALSE","TGTYPESCREENREG");

        TYPE.$("ELE_COMMISSIONRATE2TEXTFIELD","//input[@type='text'][@name='outerForm:grid:0:commissionRate3']","10","FALSE","TGTYPESCREENREG");

        WAIT.$("ELE_SUBMITBUTTONCOMMISSIONSCHEDULE","//input[@type='submit'][@name='outerForm:Submit']","VISIBLE","TGTYPESCREENREG");

        TAP.$("ELE_SUBMITBUTTONCOMMISSIONSCHEDULE","//input[@type='submit'][@name='outerForm:Submit']","TGTYPESCREENREG");

        CALL.$("AddCommissionContract","TGTYPESCREENREG");

        HOVER.$("ELE_AGENCYTAB","//td[text()='Agency']","TGTYPESCREENREG");

        WAIT.$("ELE_COMMISSIONCONTRACTSLINK","//td[text()='Commission Contracts']","VISIBLE","TGTYPESCREENREG");

        TAP.$("ELE_COMMISSIONCONTRACTSLINK","//td[text()='Commission Contracts']","TGTYPESCREENREG");

        WAIT.$("ELE_ADDBUTTONCOMMISSIONCONTRACT","//input[@type='submit'][@name='outerForm:Add']","VISIBLE","TGTYPESCREENREG");

        TAP.$("ELE_ADDBUTTONCOMMISSIONCONTRACT","//input[@type='submit'][@name='outerForm:Add']","TGTYPESCREENREG");

        WAIT.$("ELE_COMMSSIONCONTRACTNOTEXTFIELD","//input[@type='text'][@name='outerForm:commissionContNumb']","VISIBLE","TGTYPESCREENREG");

        TYPE.$("ELE_COMMSSIONCONTRACTNOTEXTFIELD","//input[@type='text'][@name='outerForm:commissionContNumb']","2224","FALSE","TGTYPESCREENREG");

        TYPE.$("ELE_COMMISSIONCONTRACTDESCTEXTFIELD","//input[@type='text'][@name='outerForm:longDescription']","AutoChan2DescriptionCommissionContract","FALSE","TGTYPESCREENREG");

        SELECT.$("ELE_DISTRIBUTIONCHANNELLISTCOMMISSIONCONTRACT","//select[@name='outerForm:distributionChannel']","AutoChan2Description","TGTYPESCREENREG");

        TYPE.$("ELE_EFFECTIVEDATETEXTFIELD","//input[@type='text'][@name='outerForm:effectiveDate']","01/01/2010","FALSE","TGTYPESCREENREG");

        SELECT.$("ELE_PAYBACKMETHODLIST","//select[@name='outerForm:advancePaybackMethod']","100% of Commission Earned","TGTYPESCREENREG");

        SELECT.$("ELE_ADVANCETHROUGHAGENTRANKLIST","//select[@name='outerForm:topLevelForAdvance']","AutoChan2AgencyRankDescription","TGTYPESCREENREG");

        TYPE.$("ELE_DAYSPASTDUETEXTFIELD","//input[@type='text'][@name='outerForm:daysLateBeforeChar']","3","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_SUBMITBUTTONCOMMISSIONCONTRACT","//input[@type='submit'][@name='outerForm:Submit']","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        CALL.$("AddCommissionSchedule","TGTYPESCREENREG");

        HOVER.$("ELE_AGENCYTAB","//td[text()='Agency']","TGTYPESCREENREG");

        TAP.$("ELE_COMMISSIONCONTRACTSLINK","//td[text()='Commission Contracts']","TGTYPESCREENREG");

        WAIT.$("ELE_COMMISSIONCONTRACTNOSEARCHTEXTFIELD","//input[@type='text'][@name='outerForm:grid:commissionContNumb']","VISIBLE","TGTYPESCREENREG");

        TYPE.$("ELE_COMMISSIONCONTRACTNOSEARCHTEXTFIELD","//input[@type='text'][@name='outerForm:grid:commissionContNumb']","2224","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_FINDPOSITIONTOBUTTON","//img[@id='outerForm:gnFindImage']","TGTYPESCREENREG");

        WAIT.$("ELE_COMMISSIONCONTRACT2224","//span[text()='2224']","TGTYPESCREENREG");

        TAP.$("ELE_COMMISSIONCONTRACT2224","//span[text()='2224']","TGTYPESCREENREG");

        WAIT.$("ELE_COMMISSIONSCHEDULELINK","//span[text()='Commission Schedule']","VISIBLE","TGTYPESCREENREG");

        TAP.$("ELE_COMMISSIONSCHEDULELINK","//span[text()='Commission Schedule']","TGTYPESCREENREG");

        WAIT.$("ELE_ADDBUTTONCOMMISSIONSCHEDULEPAGE","//input[@type='submit'][@name='outerForm:Add']","VISIBLE","TGTYPESCREENREG");

        TAP.$("ELE_ADDBUTTONCOMMISSIONSCHEDULE","//input[@type='submit'][@name='outerForm:Add']","TGTYPESCREENREG");

        WAIT.$("ELE_CONTRACTTYPELIST","//select[@name='outerForm:commissionContType']","VISIBLE","TGTYPESCREENREG");

        SELECT.$("ELE_CONTRACTTYPELIST","//select[@name='outerForm:commissionContType']","FLX52","TGTYPESCREENREG");

        TYPE.$("ELE_EFFECTIVEDATE2","//input[@type='text'][@name='outerForm:effectiveDate']","01/01/2019","FALSE","TGTYPESCREENREG");

        SELECT.$("ELE_COMMISSIONRATETABLELIST","//select[@name='outerForm:commissionRateTableName']","AutoChan2TableDescription","TGTYPESCREENREG");

        TYPE.$("ELE_LINEDESCRIPTIONTEXTFIELD","//input[@type='text'][@name='outerForm:shortDescription']","AutoChan2LineDescription","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_SUBMITTBUTTONADDCOMMSCHE","//input[@type='submit'][@name='outerForm:Submit']","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        CALL.$("AddNewBusinessPolicy","TGTYPESCREENREG");

        HOVER.$("ELE_NEWBUSINESSTAB","//td[text()='New Business']","TGTYPESCREENREG");

        WAIT.$("ELE_APPLICATIONENTRYUPDATELINK","//td[text()='Application Entry/Update']","VISIBLE","TGTYPESCREENREG");

        TAP.$("ELE_APPLICATIONENTRYUPDATELINK","//td[text()='Application Entry/Update']","TGTYPESCREENREG");

        WAIT.$("ELE_ADDBUTTONAPPLICATIONENTRY","//input[@type='submit'][@name='outerForm:Add']","VISIBLE","TGTYPESCREENREG");

        TAP.$("ELE_ADDBUTTONAPPLICATIONENTRY","//input[@type='submit'][@name='outerForm:Add']","TGTYPESCREENREG");

        WAIT.$("ELE_POLICYNUMBERTEXTFIELD","//input[@type='text'][@name='outerForm:policyNumber']","VISIBLE","TGTYPESCREENREG");

        TYPE.$("ELE_POLICYNUMBERTEXTFIELD","//input[@type='text'][@name='outerForm:policyNumber']","TGA123459","FALSE","TGTYPESCREENREG");

        SELECT.$("ELE_CURRENCYCODELIST","//select[@name='outerForm:currencyCode']","Dollars [US]","TGTYPESCREENREG");

        SELECT.$("ELE_COUNTRYANDSTATECODELIST","//select[@name='outerForm:countryAndStateCode']","Massachusetts US","TGTYPESCREENREG");

        TYPE.$("ELE_CASHWITHAPPLICATIONTEXTFIELD","//input[@type='text'][@name='outerForm:cashWithApplication']","500000","FALSE","TGTYPESCREENREG");

        SELECT.$("ELE_PAYMENTFREQUENCYLIST","//select[@name='outerForm:paymentMode']","Annual","TGTYPESCREENREG");

        SELECT.$("ELE_PAYMENTMETHODLIST","//select[@name='outerForm:paymentCode']","Coupon Book","TGTYPESCREENREG");

        SELECT.$("ELE_TAXQUALIFIEDDESCRIPTIONLIST","//select[@name='outerForm:taxQualifiedCode']","NON QUALIFIED","TGTYPESCREENREG");

        SELECT.$("ELE_TAXWITHHOLDINGLIST","//select[@name='outerForm:taxWithholdingCode']","No Withholding","TGTYPESCREENREG");

        TAP.$("ELE_SELECTCLIENTLINK","//span[text()='Select Client']","TGTYPESCREENREG");

        WAIT.$("ELE_ADDBUTTONCLIENTRELATIONSHOPPAGE","//input[@type='submit'][@name='outerForm:Add']","VISIBLE","TGTYPESCREENREG");

        TAP.$("ELE_ADDBUTTONCLIENTRELATIONSHOPPAGE","//input[@type='submit'][@name='outerForm:Add']","TGTYPESCREENREG");

        WAIT.$("ELE_FIRSTNAMETEXTFIELD","//input[@type='text'][@name='outerForm:nameFirst']","VISIBLE","TGTYPESCREENREG");

        TYPE.$("ELE_FIRSTNAMETEXTFIELD","//input[@type='text'][@name='outerForm:nameFirst']","John3","FALSE","TGTYPESCREENREG");

        TYPE.$("ELE_LASTNAMETEXTFIELD","//input[@type='text'][@name='outerForm:nameLast']","Appleseed3","FALSE","TGTYPESCREENREG");

        SELECT.$("ELE_IDTYPELIST","//select[@name='outerForm:taxIdenUsag2']","Social Security Number","TGTYPESCREENREG");

        TYPE.$("ELE_IDNUMBERTEXTFIELD","//input[@type='text'][@name='outerForm:identificationNumber2']","111111113","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_SUBMITBUTTONADDCLIENTPAGE","//input[@type='submit'][@name='outerForm:Submit']","TGTYPESCREENREG");

        WAIT.$("ELE_ADDRESSLINE1TEXTFIELD","//input[@type='text'][@name='outerForm:addressLineOne']","VISIBLE","TGTYPESCREENREG");

        TYPE.$("ELE_ADDRESSLINE1TEXTFIELD","//input[@type='text'][@name='outerForm:addressLineOne']","123 Main St","FALSE","TGTYPESCREENREG");

        TYPE.$("ELE_CITYTEXTFIELD","//input[@type='text'][@name='outerForm:addressCity']","Boston","FALSE","TGTYPESCREENREG");

        SELECT.$("ELE_COUNTRYANDSTATECODELIST","//select[@name='outerForm:countryAndStateCode']","Massachusetts US","TGTYPESCREENREG");

        TYPE.$("ELE_ZIPCODETEXTFIELD","//input[@type='text'][@name='outerForm:zipCode']","02110","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_SUBMITBUTTONADDADDRESSPAGE","//input[@type='submit'][@name='outerForm:Submit']","TGTYPESCREENREG");

        WAIT.$("ELE_SELECTCLIENTRELATIONSHIPSLINK","//a[text()='Select Client Relationships']","VISIBLE","TGTYPESCREENREG");

        TAP.$("ELE_SELECTCLIENTRELATIONSHIPSLINK","//a[text()='Select Client Relationships']","TGTYPESCREENREG");

        WAIT.$("ELE_CLIENTNAMESEARCHTEXTFIELD","//input[@type='text'][@name='outerForm:grid:individualName']","VISIBLE","TGTYPESCREENREG");

        TYPE.$("ELE_CLIENTNAMESEARCHTEXTFIELD","//input[@type='text'][@name='outerForm:grid:individualName']","Appleseed3 John3","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_FINDPOSITIONTOCLIENTNAMEBUTTON","//img[@id='outerForm:gnFindImage']","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        SELECT.$("ELE_RELATIONSHIPGRID0LIST","//select[@name='outerForm:grid:0:relationship']","Owner 1","TGTYPESCREENREG");

        TAP.$("ELE_SUBMITBUTTONSELECTCLIENTRELATIONPAGE","//input[@type='submit'][@name='outerForm:Submit']","TGTYPESCREENREG");

        WAIT.$("ELE_SUBMITBUTTONADDNEWBUSINESS","//*[@name='outerForm:Submit']","VISIBLE","TGTYPESCREENREG");

        TYPE.$("ELE_AGENTNUMBERTEXTFIELD","//input[@type='text'][@name='outerForm:grid:0:agentNumberOut2']","15500000","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_SUBMITBUTTONADDNEWBUSINESS","//*[@name='outerForm:Submit']","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");


    }


}
