package appium.tgo;

import com.tg.octopus.base.TGOWrapper;
import com.tg.octopus.libs.testng.listener.TestListener;
import static com.tg.octopus.base.TGOWrapper.Action.*;
import static com.tg.octopus.base.TGOWrapper.LOGGER;
import com.tg.octopus.wrapper.ActionWrapper;

import com.jayway.jsonpath.JsonPath;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.SessionNotCreatedException;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.Augmenter;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.remote.UnreachableBrowserException;

import org.testng.Assert;
import org.testng.ITestContext;
import org.testng.ITestResult;
import org.testng.Reporter;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;
import java.net.HttpURLConnection;
import java.net.URL;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import org.json.JSONArray;
import org.json.JSONObject;
import helper.WebTestUtility;
import java.sql.*;
import com.testautomationguru.utility.PDFUtil;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.multipdf.Splitter;

import java.util.*;
import org.openqa.selenium.*;
import java.io.*;
import java.net.*;

import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;


@Listeners(TestListener.class)
public class Test extends TGOWrapper {



    static {
       needsresetoncebeforetest ="false";
       needsresetbeforeeverymethod ="true";
       captureverystepsnap ="true";
       appendlastsnap ="false";
       ishybridapp ="true";

    }


    @Test
    public void logintest() {

        // No need to declare / print method name here, handled by TestNg listener
        // declare("logintest");

        CALL.$("login","TGTYPESCREENREG");

        TYPE.$("ELE_LOGINUSER","outerForm:usernameTGWEBCOMMAIdTGWEBCOMMA0","Anusha","FALSE","TGTYPESCREENREG");

        TYPE.$("ELE_PASSWORD","outerForm:passwordTGWEBCOMMAIdTGWEBCOMMA0","cnx12345","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_LOGINBUTTON","outerForm:loginButtonTGWEBCOMMAIdTGWEBCOMMA0","TGTYPESCREENREG");

        HOVER.$("ELE_POLICYOWNERTAB","//td[text()='Policyowner Services']","TGTYPESCREENREG");

        HOVER.$("ELE_SURRENDERTAB","//td[@class='ThemePanelMenuFolderText'][contains(text(),'Surrender')]","TGTYPESCREENREG");

        TAP.$("ELE_PARTIALSURRENDERTAB","//td[text()='Full/Partial Surrender']","TGTYPESCREENREG");

        TYPE.$("ELE_POLICYNUMBER","//input[@type='text'][@name='outerForm:policyNumber']","36R1AT1221","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_PATITIALBUTTON","//input[@type='submit'][@name='outerForm:PartialSurrender']","TGTYPESCREENREG");

        TAP.$("ELE_BASEBUTTON","//span[text()='Base']","TGTYPESCREENREG");

        TYPE.$("ELE_SURRENDERAMOUNT","//input[@type='text'][@name='outerForm:requestedSurrenderAmount_partial']","103","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_DISTRIBUTIONINFOLINK","//span[text()='Distribution Information']","TGTYPESCREENREG");

        TAP.$("ELE_CONTINUEBUTTON","//input[@type='submit'][@name='outerForm:Continue']","TGTYPESCREENREG");

		try { 
		  String var_surrenderCharge = driver.findElement(By.id("outerForm:unprotectedSurrenderChargeDisplay_partial")).getAttribute("value");
		 String var_misCharge = driver.findElement(By.id("outerForm:miscellaneousSurrenderChargeDisplay_partial")).getAttribute("value");
		 String var_adminCharge = driver.findElement(By.id("outerForm:administrativeChargeDisplay_partial")).getAttribute("value");

		String var_enteredsurrenderamount = driver.findElement(By.id("outerForm:requestedSurrenderAmount_partial")).getAttribute("value");
		String var_amounttoowner = driver.findElement(By.id("outerForm:netSurrenderAmount_partial")).getAttribute("value");


		Double var_totalCharge;

		var_totalCharge =  (Double.valueOf(var_surrenderCharge))  +  (Double.valueOf(var_misCharge))+  ( Double.valueOf(var_adminCharge));

		Double var_caluclatedCharge;

		var_caluclatedCharge = (Double.valueOf(var_enteredsurrenderamount)) - var_totalCharge ;

		var_expectedamounttoowner = (String.var_caluclatedCharge );

		Assert.assertTrue(var_caluclatedCharge.equals(Double.valueOf(var_amounttoowner)), "amount_is_notmatched");
		 
		 ;} 
		 catch(Exception e) { PrintVal("Renderer Script Exception : "+e.toString()); } 
		 PrintVal("Executed Step = CALLRENDERERSCRIPT,CALUCULATION"); 

    }


    @Test
    public void loantest() {

        // No need to declare / print method name here, handled by TestNg listener
        // declare("loantest");

        CALL.$("LoanCaluculation","TGTYPESCREENREG");

        TYPE.$("ELE_LOGINUSER","outerForm:usernameTGWEBCOMMAIdTGWEBCOMMA0","Anusha","FALSE","TGTYPESCREENREG");

        TYPE.$("ELE_PASSWORD","outerForm:passwordTGWEBCOMMAIdTGWEBCOMMA0","cnx12345","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_LOGINBUTTON","outerForm:loginButtonTGWEBCOMMAIdTGWEBCOMMA0","TGTYPESCREENREG");

        HOVER.$("ELE_POLICYOWNERTAB","//td[text()='Policyowner Services']","TGTYPESCREENREG");

        HOVER.$("ELE_LOANORLEANTAB","//td[text()='Loan/Lien']","TGTYPESCREENREG");

        TAP.$("ELE_LOANTAB","//div[@id='cmSubMenuID41']//tbody//tr[2]//td[2]","TGTYPESCREENREG");

        TYPE.$("ELE_POLICYNUMBERFEILD","//input[@type='text'][@name='outerForm:policyNumber']","36R1AT1243","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_CONTINUEONLOANBUTTON","//input[@type='submit'][@name='outerForm:Continue']","TGTYPESCREENREG");

        TYPE.$("ELE_REQUESTEDLOANFEILD","//input[@type='text'][@name='outerForm:loanRequest']","10","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_NETORGRASSRADIO","//input[@type='radio'][@name='outerForm:grossOrNetLoan']","TGTYPESCREENREG");

        TAP.$("ELE_QUOTEBUTTONONLOAN","//input[@type='submit'][@name='outerForm:Quote']","TGTYPESCREENREG");

        TAP.$("ELE_DISTRIBUTIONINFOLINKLOAN","//span[text()='Distribution Information']","TGTYPESCREENREG");

        TAP.$("ELE_CONTINUEONDIPAGE","//input[@type='submit'][@name='outerForm:Continue']","TGTYPESCREENREG");

        CALL.$("LoanCaluculation","TGTYPESCREENREG");

        TYPE.$("ELE_LOGINUSER","outerForm:usernameTGWEBCOMMAIdTGWEBCOMMA0","Anusha","FALSE","TGTYPESCREENREG");

        TYPE.$("ELE_PASSWORD","outerForm:passwordTGWEBCOMMAIdTGWEBCOMMA0","cnx12345","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_LOGINBUTTON","outerForm:loginButtonTGWEBCOMMAIdTGWEBCOMMA0","TGTYPESCREENREG");

        HOVER.$("ELE_POLICYOWNERTAB","//td[text()='Policyowner Services']","TGTYPESCREENREG");

        HOVER.$("ELE_LOANORLEANTAB","//td[text()='Loan/Lien']","TGTYPESCREENREG");

        TAP.$("ELE_LOANTAB","//div[@id='cmSubMenuID41']//tbody//tr[2]//td[2]","TGTYPESCREENREG");

        TYPE.$("ELE_POLICYNUMBERFEILD","//input[@type='text'][@name='outerForm:policyNumber']","36R1AT1243","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_CONTINUEONLOANBUTTON","//input[@type='submit'][@name='outerForm:Continue']","TGTYPESCREENREG");

        TYPE.$("ELE_REQUESTEDLOANFEILD","//input[@type='text'][@name='outerForm:loanRequest']","10","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_NETORGRASSRADIO","//input[@type='radio'][@name='outerForm:grossOrNetLoan']","TGTYPESCREENREG");

        TAP.$("ELE_QUOTEBUTTONONLOAN","//input[@type='submit'][@name='outerForm:Quote']","TGTYPESCREENREG");

        TAP.$("ELE_DISTRIBUTIONINFOLINKLOAN","//span[text()='Distribution Information']","TGTYPESCREENREG");

        TAP.$("ELE_CONTINUEONDIPAGE","//input[@type='submit'][@name='outerForm:Continue']","TGTYPESCREENREG");


    }


    @Test
    public void loancalcvs() {

        // No need to declare / print method name here, handled by TestNg listener
        // declare("loancalcvs");

        CALL.$("loancaluculation1","TGTYPESCREENREG");

        TYPE.$("ELE_LOGINUSER","outerForm:usernameTGWEBCOMMAIdTGWEBCOMMA0","Anusha","FALSE","TGTYPESCREENREG");

        TYPE.$("ELE_PASSWORD","outerForm:passwordTGWEBCOMMAIdTGWEBCOMMA0","cnx12345","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_LOGINBUTTON","outerForm:loginButtonTGWEBCOMMAIdTGWEBCOMMA0","TGTYPESCREENREG");

        HOVER.$("ELE_POLICYOWNERTAB","//td[text()='Policyowner Services']","TGTYPESCREENREG");

        HOVER.$("ELE_LOANORLEANTAB","//td[text()='Loan/Lien']","TGTYPESCREENREG");

        TAP.$("ELE_LOANTAB","//div[@id='cmSubMenuID41']//tbody//tr[2]//td[2]","TGTYPESCREENREG");

        TYPE.$("ELE_POLICYNUMBERFEILD","//input[@type='text'][@name='outerForm:policyNumber']","36R1AT1243","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_CONTINUEONLOANBUTTON","//input[@type='submit'][@name='outerForm:Continue']","TGTYPESCREENREG");

        TYPE.$("ELE_REQUESTEDLOANFEILD","//input[@type='text'][@name='outerForm:loanRequest']","10","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_NETORGRASSRADIO","//input[@type='radio'][@name='outerForm:grossOrNetLoan']","TGTYPESCREENREG");

        TAP.$("ELE_QUOTEBUTTONONLOAN","//input[@type='submit'][@name='outerForm:Quote']","TGTYPESCREENREG");

        TAP.$("ELE_DISTRIBUTIONINFOLINKLOAN","//span[text()='Distribution Information']","TGTYPESCREENREG");

        TAP.$("ELE_CONTINUEONDIPAGE","//input[@type='submit'][@name='outerForm:Continue']","TGTYPESCREENREG");

        CALL.$("LoanCaluculation","TGTYPESCREENREG");

        TYPE.$("ELE_LOGINUSER","outerForm:usernameTGWEBCOMMAIdTGWEBCOMMA0","Anusha","FALSE","TGTYPESCREENREG");

        TYPE.$("ELE_PASSWORD","outerForm:passwordTGWEBCOMMAIdTGWEBCOMMA0","cnx12345","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_LOGINBUTTON","outerForm:loginButtonTGWEBCOMMAIdTGWEBCOMMA0","TGTYPESCREENREG");

        HOVER.$("ELE_POLICYOWNERTAB","//td[text()='Policyowner Services']","TGTYPESCREENREG");

        HOVER.$("ELE_LOANORLEANTAB","//td[text()='Loan/Lien']","TGTYPESCREENREG");

        TAP.$("ELE_LOANTAB","//div[@id='cmSubMenuID41']//tbody//tr[2]//td[2]","TGTYPESCREENREG");

        TYPE.$("ELE_POLICYNUMBERFEILD","//input[@type='text'][@name='outerForm:policyNumber']","36R1AT1243","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_CONTINUEONLOANBUTTON","//input[@type='submit'][@name='outerForm:Continue']","TGTYPESCREENREG");

        TYPE.$("ELE_REQUESTEDLOANFEILD","//input[@type='text'][@name='outerForm:loanRequest']","10","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_NETORGRASSRADIO","//input[@type='radio'][@name='outerForm:grossOrNetLoan']","TGTYPESCREENREG");

        TAP.$("ELE_QUOTEBUTTONONLOAN","//input[@type='submit'][@name='outerForm:Quote']","TGTYPESCREENREG");

        TAP.$("ELE_DISTRIBUTIONINFOLINKLOAN","//span[text()='Distribution Information']","TGTYPESCREENREG");

        TAP.$("ELE_CONTINUEONDIPAGE","//input[@type='submit'][@name='outerForm:Continue']","TGTYPESCREENREG");

        CALL.$("LoanCaluculation","TGTYPESCREENREG");

        TYPE.$("ELE_LOGINUSER","outerForm:usernameTGWEBCOMMAIdTGWEBCOMMA0","Anusha","FALSE","TGTYPESCREENREG");

        TYPE.$("ELE_PASSWORD","outerForm:passwordTGWEBCOMMAIdTGWEBCOMMA0","cnx12345","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_LOGINBUTTON","outerForm:loginButtonTGWEBCOMMAIdTGWEBCOMMA0","TGTYPESCREENREG");

        HOVER.$("ELE_POLICYOWNERTAB","//td[text()='Policyowner Services']","TGTYPESCREENREG");

        HOVER.$("ELE_LOANORLEANTAB","//td[text()='Loan/Lien']","TGTYPESCREENREG");

        TAP.$("ELE_LOANTAB","//div[@id='cmSubMenuID41']//tbody//tr[2]//td[2]","TGTYPESCREENREG");

        TYPE.$("ELE_POLICYNUMBERFEILD","//input[@type='text'][@name='outerForm:policyNumber']","36R1AT1243","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_CONTINUEONLOANBUTTON","//input[@type='submit'][@name='outerForm:Continue']","TGTYPESCREENREG");

        TYPE.$("ELE_REQUESTEDLOANFEILD","//input[@type='text'][@name='outerForm:loanRequest']","10","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_NETORGRASSRADIO","//input[@type='radio'][@name='outerForm:grossOrNetLoan']","TGTYPESCREENREG");

        TAP.$("ELE_QUOTEBUTTONONLOAN","//input[@type='submit'][@name='outerForm:Quote']","TGTYPESCREENREG");

        TAP.$("ELE_DISTRIBUTIONINFOLINKLOAN","//span[text()='Distribution Information']","TGTYPESCREENREG");

        TAP.$("ELE_CONTINUEONDIPAGE","//input[@type='submit'][@name='outerForm:Continue']","TGTYPESCREENREG");


    }


    @Test
    public void logincsvtest() {

        // No need to declare / print method name here, handled by TestNg listener
        // declare("logincsvtest");

        CALL.$("logincsv","TGTYPESCREENREG");

		int var_Count = 0;
                 LOGGER.info("Executed Step = VAR,Integer,var_Count,0,TGTYPESCREENREG");
		com.jayway.jsonpath.ReadContext var_CVSData = getJsonData("C:/GIAS_Automation/inputfiles/36Reg_Login.csv");
                 LOGGER.info("Executed Step = VAR,Json,var_CVSData,C:/GIAS_Automation/inputfiles/36Reg_Login.csv,TGTYPESCREENREG");
		String var_Name;
                 LOGGER.info("Executed Step = VAR,String,var_Name,TGTYPESCREENREG");
		var_Name = getJsonData(var_CVSData , "$.records["+var_Count+"].Name");
                 LOGGER.info("Executed Step = STORE,var_Name,var_CVSData,$.records[{var_Count}].Name,TGTYPESCREENREG");
        TYPE.$("ELE_LOGINUSER","outerForm:usernameTGWEBCOMMAIdTGWEBCOMMA0",var_Name,"FALSE","TGTYPESCREENREG");


    }


    @Test
    public void loancaluculationcsvfile() {

        // No need to declare / print method name here, handled by TestNg listener
        // declare("loancaluculationcsvfile");

        CALL.$("LoanCaluculationCSV","TGTYPESCREENREG");

		int var_Count = 0;
                 LOGGER.info("Executed Step = VAR,Integer,var_Count,0,TGTYPESCREENREG");
		com.jayway.jsonpath.ReadContext var_CSVData = getJsonData("C:/GIAS_Automation/inputfiles/36Reg_Login.csv");
                 LOGGER.info("Executed Step = VAR,Json,var_CSVData,C:/GIAS_Automation/inputfiles/36Reg_Login.csv,TGTYPESCREENREG");
		String var_Username;
                 LOGGER.info("Executed Step = VAR,String,var_Username,TGTYPESCREENREG");
		var_Username = getJsonData(var_CSVData , "$.records["+var_Count+"].Name");
                 LOGGER.info("Executed Step = STORE,var_Username,var_CSVData,$.records[{var_Count}].Name,TGTYPESCREENREG");
        TYPE.$("ELE_LOGINUSER","outerForm:usernameTGWEBCOMMAIdTGWEBCOMMA0",var_Username,"FALSE","TGTYPESCREENREG");

		String var_Passcode;
                 LOGGER.info("Executed Step = VAR,String,var_Passcode,TGTYPESCREENREG");
		var_Passcode = getJsonData(var_CSVData , "$.records["+var_Count+"].passcode");
                 LOGGER.info("Executed Step = STORE,var_Passcode,var_CSVData,$.records[{var_Count}].passcode,TGTYPESCREENREG");
        TYPE.$("ELE_PASSWORD","outerForm:passwordTGWEBCOMMAIdTGWEBCOMMA0",var_Passcode,"FALSE","TGTYPESCREENREG");

        TAP.$("ELE_LOGINBUTTON","outerForm:loginButtonTGWEBCOMMAIdTGWEBCOMMA0","TGTYPESCREENREG");

        HOVER.$("ELE_POLICYOWNERTAB","//td[text()='Policyowner Services']","TGTYPESCREENREG");

        HOVER.$("ELE_LOANORLEANTAB","//td[text()='Loan/Lien']","TGTYPESCREENREG");

        TAP.$("ELE_LOANTAB","//div[@id='cmSubMenuID41']//tbody//tr[2]//td[2]","TGTYPESCREENREG");

		String var_PolicyNumber;
                 LOGGER.info("Executed Step = VAR,String,var_PolicyNumber,TGTYPESCREENREG");
		var_PolicyNumber = getJsonData(var_CSVData , "$.records.["+var_Count+"].PolicyNumber");
                 LOGGER.info("Executed Step = STORE,var_PolicyNumber,var_CSVData,$.records.[{var_Count}].PolicyNumber,TGTYPESCREENREG");
        TYPE.$("ELE_POLICYNUMBERFEILD","//input[@type='text'][@name='outerForm:policyNumber']",var_PolicyNumber,"FALSE","TGTYPESCREENREG");

        TAP.$("ELE_CONTINUEONLOANBUTTON","//input[@type='submit'][@name='outerForm:Continue']","TGTYPESCREENREG");

		String var_Loanamount;
                 LOGGER.info("Executed Step = VAR,String,var_Loanamount,TGTYPESCREENREG");
		var_Loanamount = getJsonData(var_CSVData , "$.records.["+var_Count+"].Loan");
                 LOGGER.info("Executed Step = STORE,var_Loanamount,var_CSVData,$.records.[{var_Count}].Loan,TGTYPESCREENREG");
        TYPE.$("ELE_REQUESTEDLOANFEILD","//input[@type='text'][@name='outerForm:loanRequest']",var_Loanamount,"FALSE","TGTYPESCREENREG");

        TAP.$("ELE_NETORGRASSRADIO","//input[@type='radio'][@name='outerForm:grossOrNetLoan']","TGTYPESCREENREG");

        TAP.$("ELE_QUOTEBUTTONONLOAN","//input[@type='submit'][@name='outerForm:Quote']","TGTYPESCREENREG");

        TAP.$("ELE_DISTRIBUTIONINFOLINKLOAN","//span[text()='Distribution Information']","TGTYPESCREENREG");

        TAP.$("ELE_CONTINUEONDIPAGE","//input[@type='submit'][@name='outerForm:Continue']","TGTYPESCREENREG");

        CALL.$("LoanCaluculation","TGTYPESCREENREG");

        TYPE.$("ELE_LOGINUSER","outerForm:usernameTGWEBCOMMAIdTGWEBCOMMA0","Anusha","FALSE","TGTYPESCREENREG");

        TYPE.$("ELE_PASSWORD","outerForm:passwordTGWEBCOMMAIdTGWEBCOMMA0","cnx12345","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_LOGINBUTTON","outerForm:loginButtonTGWEBCOMMAIdTGWEBCOMMA0","TGTYPESCREENREG");

        HOVER.$("ELE_POLICYOWNERTAB","//td[text()='Policyowner Services']","TGTYPESCREENREG");

        HOVER.$("ELE_LOANORLEANTAB","//td[text()='Loan/Lien']","TGTYPESCREENREG");

        TAP.$("ELE_LOANTAB","//div[@id='cmSubMenuID41']//tbody//tr[2]//td[2]","TGTYPESCREENREG");

        TYPE.$("ELE_POLICYNUMBERFEILD","//input[@type='text'][@name='outerForm:policyNumber']","36R1AT1243","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_CONTINUEONLOANBUTTON","//input[@type='submit'][@name='outerForm:Continue']","TGTYPESCREENREG");

        TYPE.$("ELE_REQUESTEDLOANFEILD","//input[@type='text'][@name='outerForm:loanRequest']","10","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_NETORGRASSRADIO","//input[@type='radio'][@name='outerForm:grossOrNetLoan']","TGTYPESCREENREG");

        TAP.$("ELE_QUOTEBUTTONONLOAN","//input[@type='submit'][@name='outerForm:Quote']","TGTYPESCREENREG");

        TAP.$("ELE_DISTRIBUTIONINFOLINKLOAN","//span[text()='Distribution Information']","TGTYPESCREENREG");

        TAP.$("ELE_CONTINUEONDIPAGE","//input[@type='submit'][@name='outerForm:Continue']","TGTYPESCREENREG");

        CALL.$("LoanCaluculation","TGTYPESCREENREG");

        TYPE.$("ELE_LOGINUSER","outerForm:usernameTGWEBCOMMAIdTGWEBCOMMA0","Anusha","FALSE","TGTYPESCREENREG");

        TYPE.$("ELE_PASSWORD","outerForm:passwordTGWEBCOMMAIdTGWEBCOMMA0","cnx12345","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_LOGINBUTTON","outerForm:loginButtonTGWEBCOMMAIdTGWEBCOMMA0","TGTYPESCREENREG");

        HOVER.$("ELE_POLICYOWNERTAB","//td[text()='Policyowner Services']","TGTYPESCREENREG");

        HOVER.$("ELE_LOANORLEANTAB","//td[text()='Loan/Lien']","TGTYPESCREENREG");

        TAP.$("ELE_LOANTAB","//div[@id='cmSubMenuID41']//tbody//tr[2]//td[2]","TGTYPESCREENREG");

        TYPE.$("ELE_POLICYNUMBERFEILD","//input[@type='text'][@name='outerForm:policyNumber']","36R1AT1243","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_CONTINUEONLOANBUTTON","//input[@type='submit'][@name='outerForm:Continue']","TGTYPESCREENREG");

        TYPE.$("ELE_REQUESTEDLOANFEILD","//input[@type='text'][@name='outerForm:loanRequest']","10","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_NETORGRASSRADIO","//input[@type='radio'][@name='outerForm:grossOrNetLoan']","TGTYPESCREENREG");

        TAP.$("ELE_QUOTEBUTTONONLOAN","//input[@type='submit'][@name='outerForm:Quote']","TGTYPESCREENREG");

        TAP.$("ELE_DISTRIBUTIONINFOLINKLOAN","//span[text()='Distribution Information']","TGTYPESCREENREG");

        TAP.$("ELE_CONTINUEONDIPAGE","//input[@type='submit'][@name='outerForm:Continue']","TGTYPESCREENREG");


    }


    @Test
    public void testtransactiontest() {

        // No need to declare / print method name here, handled by TestNg listener
        // declare("testtransactiontest");

    }


}
