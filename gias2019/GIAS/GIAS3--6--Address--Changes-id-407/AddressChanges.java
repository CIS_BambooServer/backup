package appium.tgo;

import com.tg.octopus.base.TGOWrapper;
import com.tg.octopus.libs.testng.listener.TestListener;
import static com.tg.octopus.base.TGOWrapper.Action.*;
import static com.tg.octopus.base.TGOWrapper.LOGGER;
import com.tg.octopus.wrapper.ActionWrapper;

import com.jayway.jsonpath.JsonPath;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.SessionNotCreatedException;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.Augmenter;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.remote.UnreachableBrowserException;

import org.testng.Assert;
import org.testng.ITestContext;
import org.testng.ITestResult;
import org.testng.Reporter;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;
import java.net.HttpURLConnection;
import java.net.URL;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import org.json.JSONArray;
import org.json.JSONObject;
import helper.WebTestUtility;
import java.sql.*;
import com.testautomationguru.utility.PDFUtil;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.multipdf.Splitter;

import java.util.*;
import org.openqa.selenium.*;
import java.io.*;
import java.net.*;

import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;


@Listeners(TestListener.class)
public class AddressChanges extends TGOWrapper {



    static {
       needsresetoncebeforetest ="false";
       needsresetbeforeeverymethod ="true";
       captureverystepsnap ="true";
       appendlastsnap ="false";
       ishybridapp ="true";

    }


    @Test
    public void addresschanges() {

        // No need to declare / print method name here, handled by TestNg listener
        // declare("addresschanges");

		int var_Count;
                 LOGGER.info("Executed Step = VAR,Integer,var_Count,TGTYPESCREENREG");
		var_Count = 0;
                 LOGGER.info("Executed Step = STORE,var_Count,0,TGTYPESCREENREG");
        LOGGER.info("Executed Step = REPEAT IF");
    	while (check(var_Count,"<",1,"TGTYPESCREENREG")) {
          LOGGER.info("Executed Step = WHILE,var_Count,<,1,TGTYPESCREENREG");
        CALL.$("loginToGIAS","TGTYPESCREENREG");

        TYPE.$("ELE_USERNAMETEXTFIELD","outerForm:usernameTGWEBCOMMAIdTGWEBCOMMA0","Raghu","TRUE","TGTYPESCREENREG");

        TYPE.$("ELE_PASSWORDTEXTFIELD","outerForm:passwordTGWEBCOMMAIdTGWEBCOMMA0","123456a","TRUE","TGTYPESCREENREG");

        TAP.$("ELE_LOGINBUTTON","outerForm:loginButtonTGWEBCOMMAIdTGWEBCOMMA0","TGTYPESCREENREG");

        CALL.$("AddressChanges","TGTYPESCREENREG");

        HOVER.$("ELE_CLICKCLIENT","//td[text()='Client']","TGTYPESCREENREG");

        TAP.$("ELE_CLICKNAMESADDRESSES","//td[text()='Names/Addresses']","TGTYPESCREENREG");

		com.jayway.jsonpath.ReadContext var_CSVData = getJsonData("C:/Users/x5824/Desktop/CGI_Accelarator/Input Files/AddressChanges.csv");
                 LOGGER.info("Executed Step = VAR,Json,var_CSVData,C:/Users/x5824/Desktop/CGI_Accelarator/Input Files/AddressChanges.csv,TGTYPESCREENREG");
		String var_ClientLastNameFirstName;
                 LOGGER.info("Executed Step = VAR,String,var_ClientLastNameFirstName,TGTYPESCREENREG");
		var_ClientLastNameFirstName = getJsonData(var_CSVData , "$.records["+var_Count+"].ClientLastNameFirstName");
                 LOGGER.info("Executed Step = STORE,var_ClientLastNameFirstName,var_CSVData,$.records[{var_Count}].ClientLastNameFirstName,TGTYPESCREENREG");
        TYPE.$("ELE_ENTERFIRSTLASTNAMES","input[type=text][name='outerForm:grid:individualName']TGWEBCOMMACssSelectorTGWEBCOMMA0",var_ClientLastNameFirstName,"FALSE","TGTYPESCREENREG");

        TAP.$("ELE_FINDPOSITIONTOCLIENTNAMEBUTTON","//img[@id='outerForm:gnFindImage']","TGTYPESCREENREG");

        TAP.$("ELE_SELECTNAME","span[id='outerForm:grid:0:individualNameOut1']TGWEBCOMMACssSelectorTGWEBCOMMA0","TGTYPESCREENREG");

        TAP.$("ELE_CLICKONADDRESSINFORMATION1","//span[text()='Address Information']","TGTYPESCREENREG");

        TAP.$("ELE_SELECTDISPLAYEDADDRESS","span[id='outerForm:grid:0:addressOut1']TGWEBCOMMACssSelectorTGWEBCOMMA0","TGTYPESCREENREG");

		writeToCSV("ELE_CLIENTLASTNAMEFIRSTNAME " , ActionWrapper.getWebElementValueForVariable("span[id='outerForm:individualName']", "CssSelector", 0, "span[id='outerForm:individualName']TGWEBCOMMACssSelectorTGWEBCOMMA0"));
                 LOGGER.info("Executed Step = WRITETOCSV,ELE_CLIENTLASTNAMEFIRSTNAME,TGTYPESCREENREG");
		writeToCSV("ELE_CLIENTINITIALSTREETADDRESS " , ActionWrapper.getWebElementValueForVariable("span[id='outerForm:addressLineOne']", "CssSelector", 0, "span[id='outerForm:addressLineOne']TGWEBCOMMACssSelectorTGWEBCOMMA0"));
                 LOGGER.info("Executed Step = WRITETOCSV,ELE_CLIENTINITIALSTREETADDRESS,TGTYPESCREENREG");
		writeToCSV("ELE_CLIENTINITIALCITY " , ActionWrapper.getWebElementValueForVariable("span[id='outerForm:addressCity']", "CssSelector", 0, "span[id='outerForm:addressCity']TGWEBCOMMACssSelectorTGWEBCOMMA0"));
                 LOGGER.info("Executed Step = WRITETOCSV,ELE_CLIENTINITIALCITY,TGTYPESCREENREG");
		writeToCSV("ELE_CLIENTINITIALSTATE " , ActionWrapper.getWebElementValueForVariable("span[id='outerForm:countryAndStateCode']", "CssSelector", 0, "span[id='outerForm:countryAndStateCode']TGWEBCOMMACssSelectorTGWEBCOMMA0"));
                 LOGGER.info("Executed Step = WRITETOCSV,ELE_CLIENTINITIALSTATE,TGTYPESCREENREG");
		writeToCSV("ELE_CLIENTINITIALZIP " , ActionWrapper.getWebElementValueForVariable("span[id='outerForm:zipCode']", "CssSelector", 0, "span[id='outerForm:zipCode']TGWEBCOMMACssSelectorTGWEBCOMMA0"));
                 LOGGER.info("Executed Step = WRITETOCSV,ELE_CLIENTINITIALZIP,TGTYPESCREENREG");
        TAP.$("ELE_CLICKUPDATETOCHANGEADDRESS","//input[@type='submit'][@name='outerForm:Update']","TGTYPESCREENREG");

        TYPE.$("ELE_UPDATESTREETADDRESS","//input[@type='text'][@name='outerForm:addressLineOne']","111 New Main Rd","FALSE","TGTYPESCREENREG");

        TYPE.$("ELE_UPDATECITY","//input[@type='text'][@name='outerForm:addressCity']","St Louis","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_CLICKSUBMITTOCHANGEADDRESS","//input[@type='submit'][@name='outerForm:Submit']","TGTYPESCREENREG");

		writeToCSV("ELE_CLIENTNEWSTREETADDRESS " , ActionWrapper.getWebElementValueForVariable("span[id='outerForm:addressLineOne']", "CssSelector", 0, "span[id='outerForm:addressLineOne']TGWEBCOMMACssSelectorTGWEBCOMMA0"));
                 LOGGER.info("Executed Step = WRITETOCSV,ELE_CLIENTNEWSTREETADDRESS,TGTYPESCREENREG");
		writeToCSV("ELE_CLIENTNEWCITY " , ActionWrapper.getWebElementValueForVariable("span[id='outerForm:addressCity']", "CssSelector", 0, "span[id='outerForm:addressCity']TGWEBCOMMACssSelectorTGWEBCOMMA0"));
                 LOGGER.info("Executed Step = WRITETOCSV,ELE_CLIENTNEWCITY,TGTYPESCREENREG");
		writeToCSV("ELE_CLIENTNEWSTATE " , ActionWrapper.getWebElementValueForVariable("span[id='outerForm:countryAndStateCode']", "CssSelector", 0, "span[id='outerForm:countryAndStateCode']TGWEBCOMMACssSelectorTGWEBCOMMA0"));
                 LOGGER.info("Executed Step = WRITETOCSV,ELE_CLIENTNEWSTATE,TGTYPESCREENREG");
		writeToCSV("ELE_CLIENTNEWZIP " , ActionWrapper.getWebElementValueForVariable("span[id='outerForm:zipCode']", "CssSelector", 0, "span[id='outerForm:zipCode']TGWEBCOMMACssSelectorTGWEBCOMMA0"));
                 LOGGER.info("Executed Step = WRITETOCSV,ELE_CLIENTNEWZIP,TGTYPESCREENREG");
		var_Count = var_Count + ( 1 );
                 LOGGER.info("Executed Step = INCREASEVAR,var_Count,1,TGTYPESCREENREG");
    	}
          LOGGER.info("Executed Step = WEND,TGTYPESCREENREG");
    	resetAttemps();

    }


}
