package appium.tgo;

import com.tg.octopus.base.TGOWrapper;
import com.tg.octopus.libs.testng.listener.TestListener;
import static com.tg.octopus.base.TGOWrapper.Action.*;
import static com.tg.octopus.base.TGOWrapper.LOGGER;
import com.tg.octopus.wrapper.ActionWrapper;

import com.jayway.jsonpath.JsonPath;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.SessionNotCreatedException;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.Augmenter;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.remote.UnreachableBrowserException;

import org.testng.Assert;
import org.testng.ITestContext;
import org.testng.ITestResult;
import org.testng.Reporter;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;
import java.net.HttpURLConnection;
import java.net.URL;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import org.json.JSONArray;
import org.json.JSONObject;
import helper.WebTestUtility;
import java.sql.*;
import com.testautomationguru.utility.PDFUtil;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.multipdf.Splitter;

import java.util.*;
import org.openqa.selenium.*;
import java.io.*;
import java.net.*;

import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;


@Listeners(TestListener.class)
public class Base36GIAS extends TGOWrapper {



    static {
       needsresetoncebeforetest ="false";
       needsresetbeforeeverymethod ="true";
       captureverystepsnap ="true";
       appendlastsnap ="false";
       ishybridapp ="true";

    }


    @Test
    public void tc01createnewbusinesspolicy() {

        // No need to declare / print method name here, handled by TestNg listener
        // declare("tc01createnewbusinesspolicy");

        CALL.$("LoginToGIAS36","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_USERNAME","outerForm:usernameTGWEBCOMMAIdTGWEBCOMMA0","GIAS36ILC","FALSE","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_PASSWORD","outerForm:passwordTGWEBCOMMAIdTGWEBCOMMA0","Automation","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_LOGIN","outerForm:loginButtonTGWEBCOMMAIdTGWEBCOMMA0","TGTYPESCREENREG");

		int var_Count = 0;
                 LOGGER.info("Executed Step = VAR,Integer,var_Count,0,TGTYPESCREENREG");
        LOGGER.info("Executed Step = REPEAT IF");
    	while (check(var_Count,"<",1,"TGTYPESCREENREG")) {
          LOGGER.info("Executed Step = WHILE,var_Count,<,1,TGTYPESCREENREG");
		com.jayway.jsonpath.ReadContext var_CSVData = getJsonData("C:/GIAS_Automation/3.6 BaseRegression/InputData/36Reg_CreateAPolicy.csv");
                 LOGGER.info("Executed Step = VAR,Json,var_CSVData,C:/GIAS_Automation/3.6 BaseRegression/InputData/36Reg_CreateAPolicy.csv,TGTYPESCREENREG");
		String var_effectiveDate;
                 LOGGER.info("Executed Step = VAR,String,var_effectiveDate,TGTYPESCREENREG");
		var_effectiveDate = getJsonData(var_CSVData , "$.records["+var_Count+"].EffectiveDate");
                 LOGGER.info("Executed Step = STORE,var_effectiveDate,var_CSVData,$.records[{var_Count}].EffectiveDate,TGTYPESCREENREG");
        CALL.$("AddDistributionChannel","TGTYPESCREENREG");

		String var_DistributionChannelName;
                 LOGGER.info("Executed Step = VAR,String,var_DistributionChannelName,TGTYPESCREENREG");
		var_DistributionChannelName = getJsonData(var_CSVData , "$.records["+var_Count+"].DistributionChannelName");
                 LOGGER.info("Executed Step = STORE,var_DistributionChannelName,var_CSVData,$.records[{var_Count}].DistributionChannelName,TGTYPESCREENREG");
		String var_DistributionChannelDescription;
                 LOGGER.info("Executed Step = VAR,String,var_DistributionChannelDescription,TGTYPESCREENREG");
		var_DistributionChannelDescription = getJsonData(var_CSVData , "$.records["+var_Count+"].DistributionChannelDescription");
                 LOGGER.info("Executed Step = STORE,var_DistributionChannelDescription,var_CSVData,$.records[{var_Count}].DistributionChannelDescription,TGTYPESCREENREG");
        HOVER.$("ELE_MENU_AGENCY","//td[text()='Agency']","TGTYPESCREENREG");

        TAP.$("ELE_SUBMEN_DISTRIBUTIONCHANNEL","//td[text()='Distribution Channel']","TGTYPESCREENREG");

        TAP.$("ELE_RNDBTN_ADD","//img[@id='outerForm:gnAddImage']","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_DISTRIBUTIONCHANNEL","//input[@type='text'][@name='outerForm:distributionChanName']",var_DistributionChannelName,"FALSE","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_DISTRICHANNALDESCRIPTION","input[type=text][name='outerForm:channelDescription']TGWEBCOMMACssSelectorTGWEBCOMMA0",var_DistributionChannelDescription,"FALSE","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_DISTRIBUTIONCHANNELCODE","input[type=text][name='outerForm:duf_dist_code']TGWEBCOMMACssSelectorTGWEBCOMMA0",var_DistributionChannelName,"FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SUBMIT","//input[@type='submit'][@name='outerForm:Submit']","TGTYPESCREENREG");

        ASSERT.$("ELE_GETMSG_SUCCESS","//li[@class='MessageInfo']","CONTAINS","successfully","TGTYPESCREENREG");

        CALL.$("AddAgentRankCode","TGTYPESCREENREG");

        HOVER.$("ELE_MENU_AGENCY","//td[text()='Agency']","TGTYPESCREENREG");

		String var_DistributionChannelDescription2;
                 LOGGER.info("Executed Step = VAR,String,var_DistributionChannelDescription2,TGTYPESCREENREG");
		var_DistributionChannelDescription2 = getJsonData(var_CSVData , "$.records["+var_Count+"].DistributionChannelDescription");
                 LOGGER.info("Executed Step = STORE,var_DistributionChannelDescription2,var_CSVData,$.records[{var_Count}].DistributionChannelDescription,TGTYPESCREENREG");
		String var_AgencyRankDescription;
                 LOGGER.info("Executed Step = VAR,String,var_AgencyRankDescription,TGTYPESCREENREG");
		var_AgencyRankDescription = getJsonData(var_CSVData , "$.records["+var_Count+"].AgencyRankDescription");
                 LOGGER.info("Executed Step = STORE,var_AgencyRankDescription,var_CSVData,$.records[{var_Count}].AgencyRankDescription,TGTYPESCREENREG");
		String var_AgencyRankCode;
                 LOGGER.info("Executed Step = VAR,String,var_AgencyRankCode,TGTYPESCREENREG");
		var_AgencyRankCode = getJsonData(var_CSVData , "$.records["+var_Count+"].AgencyRankCode");
                 LOGGER.info("Executed Step = STORE,var_AgencyRankCode,var_CSVData,$.records[{var_Count}].AgencyRankCode,TGTYPESCREENREG");
        TAP.$("ELE_SUBMEN_AGENTRANKCODES","//td[text()='Agent Rank Codes']","TGTYPESCREENREG");

        TAP.$("ELE_RNDBTN_ADD","//img[@id='outerForm:gnAddImage']","TGTYPESCREENREG");

        SELECT.$("ELE_DRPDWN_DISTRIBUTIONCHANNEL","//select[@name='outerForm:distributionChannel']",var_DistributionChannelDescription2,"TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_AGENCYRANKLEVEL","//input[@type='text'][@name='outerForm:agencyRankLevel']","1","FALSE","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_AGENCYRANKCODE","//input[@type='text'][@name='outerForm:agencyRankCode']",var_AgencyRankCode,"FALSE","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_AGENCYRANKDESCRIPTION","//input[@type='text'][@name='outerForm:description']",var_AgencyRankDescription,"FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SUBMIT","//input[@type='submit'][@name='outerForm:Submit']","TGTYPESCREENREG");

        ASSERT.$("ELE_GETMSG_SUCCESS","//li[@class='MessageInfo']","CONTAINS","successfully","TGTYPESCREENREG");

        CALL.$("AddCommissionScheduleRates","TGTYPESCREENREG");

        HOVER.$("ELE_MENU_AGENCY","//td[text()='Agency']","TGTYPESCREENREG");

		String var_TableName;
                 LOGGER.info("Executed Step = VAR,String,var_TableName,TGTYPESCREENREG");
		var_TableName = getJsonData(var_CSVData , "$.records["+var_Count+"].TableName");
                 LOGGER.info("Executed Step = STORE,var_TableName,var_CSVData,$.records[{var_Count}].TableName,TGTYPESCREENREG");
		String var_TableDescription;
                 LOGGER.info("Executed Step = VAR,String,var_TableDescription,TGTYPESCREENREG");
		var_TableDescription = getJsonData(var_CSVData , "$.records["+var_Count+"].TableDescription");
                 LOGGER.info("Executed Step = STORE,var_TableDescription,var_CSVData,$.records[{var_Count}].TableDescription,TGTYPESCREENREG");
		String var_DistributionChannelDescription3;
                 LOGGER.info("Executed Step = VAR,String,var_DistributionChannelDescription3,TGTYPESCREENREG");
		var_DistributionChannelDescription3 = getJsonData(var_CSVData , "$.records["+var_Count+"].DistributionChannelDescription");
                 LOGGER.info("Executed Step = STORE,var_DistributionChannelDescription3,var_CSVData,$.records[{var_Count}].DistributionChannelDescription,TGTYPESCREENREG");
		String var_AgencyRankDescription3;
                 LOGGER.info("Executed Step = VAR,String,var_AgencyRankDescription3,TGTYPESCREENREG");
		var_AgencyRankDescription3 = getJsonData(var_CSVData , "$.records["+var_Count+"].AgencyRankDescription");
                 LOGGER.info("Executed Step = STORE,var_AgencyRankDescription3,var_CSVData,$.records[{var_Count}].AgencyRankDescription,TGTYPESCREENREG");
        TAP.$("ELE_SUBMEN_COMMISSIONSCHEDULERATES","//td[text()='Commission Schedule Rates']","TGTYPESCREENREG");

        TAP.$("ELE_RECBTN_ADD","//input[@type='submit'][@name='outerForm:Add']","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_TABLENAME","//input[@type='text'][@name='outerForm:tableName']",var_TableName,"FALSE","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_TABLEDESCRIPTION","//input[@type='text'][@name='outerForm:description']",var_TableDescription,"FALSE","TGTYPESCREENREG");

        SELECT.$("ELE_DRPDWN_DISTRIBUTIONCHANNEL","//select[@name='outerForm:distributionChannel']",var_DistributionChannelDescription3,"TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_CONTINUE","//input[@type='submit'][@name='outerForm:Continue']","TGTYPESCREENREG");

        SELECT.$("ELE_DRPPDWN_AGENCYRANKCODE","//select[@name='outerForm:grid:0:agencyRankCode']",var_AgencyRankDescription,"TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_ENDDURATION0","//input[@type='text'][@name='outerForm:grid:endingDuration1']","1","FALSE","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_ENDDURATION1","//input[@type='text'][@name='outerForm:grid:endingDuration2']","5","FALSE","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_ENDDURATION2","//input[@type='text'][@name='outerForm:grid:endingDuration3']","120","FALSE","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_COMMISSIONRATE0","//input[@type='text'][@name='outerForm:grid:0:commissionRate1']","20","FALSE","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_COMMISSIONRATE1","//input[@type='text'][@name='outerForm:grid:0:commissionRate2']","15","FALSE","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_COMMISSIONRATE2","//input[@type='text'][@name='outerForm:grid:0:commissionRate3']","10","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SUBMIT","//input[@type='submit'][@name='outerForm:Submit']","TGTYPESCREENREG");

        ASSERT.$("ELE_GETMSG_SUCCESS","//li[@class='MessageInfo']","CONTAINS","successfully","TGTYPESCREENREG");

        CALL.$("AddCommissionContract","TGTYPESCREENREG");

        HOVER.$("ELE_MENU_AGENCY","//td[text()='Agency']","TGTYPESCREENREG");

		String var_CommissionContractNumber;
                 LOGGER.info("Executed Step = VAR,String,var_CommissionContractNumber,TGTYPESCREENREG");
		var_CommissionContractNumber = getJsonData(var_CSVData , "$.records["+var_Count+"].CommissionContractNumber");
                 LOGGER.info("Executed Step = STORE,var_CommissionContractNumber,var_CSVData,$.records[{var_Count}].CommissionContractNumber,TGTYPESCREENREG");
		String var_CommissionContractDescription;
                 LOGGER.info("Executed Step = VAR,String,var_CommissionContractDescription,TGTYPESCREENREG");
		var_CommissionContractDescription = getJsonData(var_CSVData , "$.records["+var_Count+"].CommissionContractDescription");
                 LOGGER.info("Executed Step = STORE,var_CommissionContractDescription,var_CSVData,$.records[{var_Count}].CommissionContractDescription,TGTYPESCREENREG");
		String var_DistributionChannelDescription4;
                 LOGGER.info("Executed Step = VAR,String,var_DistributionChannelDescription4,TGTYPESCREENREG");
		var_DistributionChannelDescription4 = getJsonData(var_CSVData , "$.records["+var_Count+"].DistributionChannelDescription");
                 LOGGER.info("Executed Step = STORE,var_DistributionChannelDescription4,var_CSVData,$.records[{var_Count}].DistributionChannelDescription,TGTYPESCREENREG");
		String var_AgencyRankDescription4;
                 LOGGER.info("Executed Step = VAR,String,var_AgencyRankDescription4,TGTYPESCREENREG");
		var_AgencyRankDescription4 = getJsonData(var_CSVData , "$.records["+var_Count+"].AgencyRankDescription");
                 LOGGER.info("Executed Step = STORE,var_AgencyRankDescription4,var_CSVData,$.records[{var_Count}].AgencyRankDescription,TGTYPESCREENREG");
        TAP.$("ELE_SUBMEN_COMMISSIONCONTRACTS","//td[text()='Commission Contracts']","TGTYPESCREENREG");

        TAP.$("ELE_RNDBTN_ADD","//img[@id='outerForm:gnAddImage']","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_COMMSSIONCONTRACTNUMBER","//input[@type='text'][@name='outerForm:commissionContNumb']",var_CommissionContractNumber,"FALSE","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_COMMISSIONCONTRACTDESCRIPTION","//input[@type='text'][@name='outerForm:longDescription']",var_CommissionContractDescription,"FALSE","TGTYPESCREENREG");

        SELECT.$("ELE_DRPDWN_DISTRIBUTIONCHANNEL","//select[@name='outerForm:distributionChannel']",var_DistributionChannelDescription4,"TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_EFFECTIVEDATE","//input[@type='text'][@name='outerForm:effectiveDate']",var_effectiveDate,"TRUE","TGTYPESCREENREG");

        SELECT.$("ELE_DRPDWN_PAYBACKMETHOD","//select[@name='outerForm:advancePaybackMethod']","100% of Commission Earned","TGTYPESCREENREG");

        SELECT.$("ELE_DRPDWN_ADVANCETHROUGHAGENTRANK","//select[@name='outerForm:topLevelForAdvance']",var_AgencyRankDescription4,"TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_DAYSPASTDUE","//input[@type='text'][@name='outerForm:daysLateBeforeChar']","3","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SUBMIT","//input[@type='submit'][@name='outerForm:Submit']","TGTYPESCREENREG");

        ASSERT.$("ELE_GETMSG_SUCCESS","//li[@class='MessageInfo']","CONTAINS","successfully","TGTYPESCREENREG");

        CALL.$("AddCommissionSchedule","TGTYPESCREENREG");

        HOVER.$("ELE_MENU_AGENCY","//td[text()='Agency']","TGTYPESCREENREG");

		String var_CommissionContractNumber5;
                 LOGGER.info("Executed Step = VAR,String,var_CommissionContractNumber5,TGTYPESCREENREG");
		var_CommissionContractNumber5 = getJsonData(var_CSVData , "$.records["+var_Count+"].CommissionContractNumber");
                 LOGGER.info("Executed Step = STORE,var_CommissionContractNumber5,var_CSVData,$.records[{var_Count}].CommissionContractNumber,TGTYPESCREENREG");
		String var_TableDescription5;
                 LOGGER.info("Executed Step = VAR,String,var_TableDescription5,TGTYPESCREENREG");
		var_TableDescription5 = getJsonData(var_CSVData , "$.records["+var_Count+"].TableDescription");
                 LOGGER.info("Executed Step = STORE,var_TableDescription5,var_CSVData,$.records[{var_Count}].TableDescription,TGTYPESCREENREG");
		String var_LineDescription;
                 LOGGER.info("Executed Step = VAR,String,var_LineDescription,TGTYPESCREENREG");
		var_LineDescription = getJsonData(var_CSVData , "$.records["+var_Count+"].LineDescription");
                 LOGGER.info("Executed Step = STORE,var_LineDescription,var_CSVData,$.records[{var_Count}].LineDescription,TGTYPESCREENREG");
        TAP.$("ELE_SUBMEN_COMMISSIONCONTRACTS","//td[text()='Commission Contracts']","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_SEACHCOMMISSIONCONTRACTNO","//input[@type='text'][@name='outerForm:grid:commissionContNumb']",var_CommissionContractNumber5,"FALSE","TGTYPESCREENREG");

        TAP.$("ELE_RNDBTN_FINDPOSITIONTO","//img[@id='outerForm:gnFindImage']","TGTYPESCREENREG");

        TAP.$("ELE_LINK_SEARCHEDFIRSTCOMMISSIONCONTRACT","span[id='outerForm:grid:0:commissionContNumbOut1']TGWEBCOMMACssSelectorTGWEBCOMMA0","TGTYPESCREENREG");

        TAP.$("ELE_LINK_COMMISSIONSCHEDULE","//span[text()='Commission Schedule']","TGTYPESCREENREG");

        TAP.$("ELE_RNDBTN_ADD","//img[@id='outerForm:gnAddImage']","TGTYPESCREENREG");

        SELECT.$("ELE_DRPDWN_CONTRACTTYPE","//select[@name='outerForm:commissionContType']","Health","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_EFFECTIVEDATE","//input[@type='text'][@name='outerForm:effectiveDate']",var_effectiveDate,"TRUE","TGTYPESCREENREG");

        SELECT.$("ELE_DRPDWN_COMMISSIONRATETABLENAME","//select[@name='outerForm:commissionRateTableName']",var_TableDescription5,"TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_LINEDESCRIPTION","//input[@type='text'][@name='outerForm:shortDescription']",var_LineDescription,"FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SUBMIT","//input[@type='submit'][@name='outerForm:Submit']","TGTYPESCREENREG");

        ASSERT.$("ELE_GETMSG_SUCCESS","//li[@class='MessageInfo']","CONTAINS","successfully","TGTYPESCREENREG");

        CALL.$("AddNewBusinessPolicy","TGTYPESCREENREG");

        HOVER.$("ELE_MENU_NEWBUSINESS","//td[text()='New Business']","TGTYPESCREENREG");

        TAP.$("ELE_SUBMEN_APPLICATIONENTRYUPDATE","//td[text()='Application Entry/Update']","TGTYPESCREENREG");

        TAP.$("ELE_RNDBTN_ADD","//img[@id='outerForm:gnAddImage']","TGTYPESCREENREG");

		String var_PolicyNumber;
                 LOGGER.info("Executed Step = VAR,String,var_PolicyNumber,TGTYPESCREENREG");
		var_PolicyNumber = getJsonData(var_CSVData , "$.records["+var_Count+"].PolicyNumber");
                 LOGGER.info("Executed Step = STORE,var_PolicyNumber,var_CSVData,$.records[{var_Count}].PolicyNumber,TGTYPESCREENREG");
		String var_FirstName;
                 LOGGER.info("Executed Step = VAR,String,var_FirstName,TGTYPESCREENREG");
		var_FirstName = getJsonData(var_CSVData , "$.records["+var_Count+"].FirstName");
                 LOGGER.info("Executed Step = STORE,var_FirstName,var_CSVData,$.records[{var_Count}].FirstName,TGTYPESCREENREG");
		String var_LastName;
                 LOGGER.info("Executed Step = VAR,String,var_LastName,TGTYPESCREENREG");
		var_LastName = getJsonData(var_CSVData , "$.records["+var_Count+"].LastName");
                 LOGGER.info("Executed Step = STORE,var_LastName,var_CSVData,$.records[{var_Count}].LastName,TGTYPESCREENREG");
		String var_IDNumber;
                 LOGGER.info("Executed Step = VAR,String,var_IDNumber,TGTYPESCREENREG");
		var_IDNumber = getJsonData(var_CSVData , "$.records["+var_Count+"].IDNumber");
                 LOGGER.info("Executed Step = STORE,var_IDNumber,var_CSVData,$.records[{var_Count}].IDNumber,TGTYPESCREENREG");
		String var_ClientSearchName;
                 LOGGER.info("Executed Step = VAR,String,var_ClientSearchName,TGTYPESCREENREG");
		var_ClientSearchName = getJsonData(var_CSVData , "$.records["+var_Count+"].ClientSearchName");
                 LOGGER.info("Executed Step = STORE,var_ClientSearchName,var_CSVData,$.records[{var_Count}].ClientSearchName,TGTYPESCREENREG");
		String var_AgentNumber;
                 LOGGER.info("Executed Step = VAR,String,var_AgentNumber,TGTYPESCREENREG");
		var_AgentNumber = getJsonData(var_CSVData , "$.records["+var_Count+"].AgentNumber");
                 LOGGER.info("Executed Step = STORE,var_AgentNumber,var_CSVData,$.records[{var_Count}].AgentNumber,TGTYPESCREENREG");
        TYPE.$("ELE_TXTBOX_POLICYNUMBER","//input[@type='text'][@name='outerForm:policyNumber']",var_PolicyNumber,"FALSE","TGTYPESCREENREG");

        SELECT.$("ELE_DRPDWN_CURRENCYCODE","//select[@id='outerForm:currencyCode']","Dollars [US]","TGTYPESCREENREG");

        SELECT.$("ELE_DRPDWN_STATECOUNTRY","//select[@name='outerForm:countryAndStateCode']	","Missouri US","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_EFFECTIVEDATE","//input[@type='text'][@name='outerForm:effectiveDate']",var_effectiveDate,"TRUE","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_CASHWITHAPPLICATION","//input[@type='text'][@name='outerForm:cashWithApplication']","25000","FALSE","TGTYPESCREENREG");

        SELECT.$("ELE_DRPDWN_PAYMENTFREQUENCY","//select[@name='outerForm:paymentMode']","Annual","TGTYPESCREENREG");

        SELECT.$("ELE_DRPDWN_PAYMENTMETHOD","//select[@name='outerForm:paymentCode']","Coupon Book","TGTYPESCREENREG");

        SELECT.$("ELE_DRPDWN_TAXQUALIFIEDDESCRIPTION","//select[@name='outerForm:taxQualifiedCode']","NON QUALIFIED","TGTYPESCREENREG");

        SELECT.$("ELE_DRPDWN_TAXWITHHOLDING","//select[@name='outerForm:taxWithholdingCode']","No Withholding","TGTYPESCREENREG");

        TAP.$("ELE_LINK_SELECTCLIENT","//span[text()='Select Client']","TGTYPESCREENREG");

        TAP.$("ELE_RNDBTN_ADD","//img[@id='outerForm:gnAddImage']","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_FIRSTNAME","//input[@type='text'][@name='outerForm:nameFirst']",var_FirstName,"FALSE","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_LASTNAME","//input[@type='text'][@name='outerForm:nameLast']",var_LastName,"FALSE","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_CLIENTDOB","//input[@type='text'][@name='outerForm:dateOfBirth2']","01/01/1980","FALSE","TGTYPESCREENREG");

        SELECT.$("ELE_DRPDWN_CLIENTGENDER","//select[@name='outerForm:sexCode2']","Male","TGTYPESCREENREG");

        SELECT.$("ELE_DRPDWN_IDTYPE","//select[@name='outerForm:taxIdenUsag2']","Social Security Number","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_IDNUMBER","//input[@type='text'][@name='outerForm:identificationNumber2']",var_IDNumber,"FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SUBMIT","//input[@type='submit'][@name='outerForm:Submit']","TGTYPESCREENREG");

        ASSERT.$("ELE_GETMSG_SUCCESS","//li[@class='MessageInfo']","CONTAINS","successfully","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_ADDRESSLINE1","//input[@type='text'][@name='outerForm:addressLineOne']","123 Main St","FALSE","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_CITY","//input[@type='text'][@name='outerForm:addressCity']","Kansas City","FALSE","TGTYPESCREENREG");

        SELECT.$("ELE_DRPDWN_STATECOUNTRY","//select[@name='outerForm:countryAndStateCode']	","Missouri US","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_ZIPCODE","//input[@type='text'][@name='outerForm:zipCode']","64153","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SUBMIT","//input[@type='submit'][@name='outerForm:Submit']","TGTYPESCREENREG");

        ASSERT.$("ELE_GETMSG_SUCCESS","//li[@class='MessageInfo']","CONTAINS","successfully","TGTYPESCREENREG");

        TAP.$("ELE_LINK_SELECTCLIENTRELATIONSHIPS","//a[text()='Select Client Relationships']","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_SEARCHCLIENTNAME","//input[@type='text'][@name='outerForm:grid:individualName']",var_ClientSearchName,"FALSE","TGTYPESCREENREG");

        TAP.$("ELE_RNDBTN_FINDPOSITIONTO","//img[@id='outerForm:gnFindImage']","TGTYPESCREENREG");

        SELECT.$("ELE_DRPDWN_RELATIONSHIPGRID","//select[@name='outerForm:grid:0:relationship']","Owner 1","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SUBMIT","//input[@type='submit'][@name='outerForm:Submit']","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_AGENTNUMBER","//input[@type='text'][@name='outerForm:grid:0:agentNumberOut2']",var_AgentNumber,"FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SUBMIT","//input[@type='submit'][@name='outerForm:Submit']","TGTYPESCREENREG");

        ASSERT.$("ELE_GETMSG_SUCCESS","//li[@class='MessageInfo']","CONTAINS","successfully","TGTYPESCREENREG");

        CALL.$("SettlePolicy","TGTYPESCREENREG");

        TAP.$("ELE_LINK_BENEFITS","//span[text()='Benefits']","TGTYPESCREENREG");

		String var_BenefitPlan = "null";
                 LOGGER.info("Executed Step = VAR,String,var_BenefitPlan,null,TGTYPESCREENREG");
		var_BenefitPlan = getJsonData(var_CSVData , "$.records["+var_Count+"].BenefitPlan");
                 LOGGER.info("Executed Step = STORE,var_BenefitPlan,var_CSVData,$.records[{var_Count}].BenefitPlan,TGTYPESCREENREG");
		String var_universalBenefitPlan = "ULAT1";
                 LOGGER.info("Executed Step = VAR,String,var_universalBenefitPlan,ULAT1,TGTYPESCREENREG");
        SELECT.$("ELE_DRPDWN_ADDABENEFIT","//select[@name='outerForm:initialPlanCode']",var_BenefitPlan,"TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_CONTINUE","//input[@type='submit'][@name='outerForm:Continue']","TGTYPESCREENREG");

        LOGGER.info("Executed Step = START IF");
        if (check(var_BenefitPlan,"CONTAINS",var_universalBenefitPlan,"TGTYPESCREENREG")) {
         LOGGER.info("Executed Step = IF,var_BenefitPlan,CONTAINS,var_universalBenefitPlan,TGTYPESCREENREG");
        TYPE.$("ELE_TXTBOX_UNITSAFTERADDINGBENEFIT","//input[@type='text'][@name='outerForm:unitsOfInsurance']	","250.000","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_RDOBTN_FACEAMOUNT","//input[@id='outerForm:deathBenefitOption:0']","TGTYPESCREENREG");

        SWIPE.$("UP","TGTYPESCREENREG");

        }
         else {
          LOGGER.info("Executed Step = ELSE,TGTYPESCREENREG");
        TYPE.$("ELE_TXTBOX_UNITSAFTERADDINGBENEFIT","//input[@type='text'][@name='outerForm:unitsOfInsurance']	","25.00","TRUE","TGTYPESCREENREG");

		try { 
		  List<WebElement> checkBoxes = driver.findElements(By.xpath("//input[@type='checkbox']"));
				int numberOfBoxes =  checkBoxes.size();
				if( numberOfBoxes > 0) {
						for(int i=0;i<numberOfBoxes;i++)
						{
					 driver.findElement(By.xpath("//input[@id='outerForm:gridSupp:" + i + ":selectedCheck']")).click();

		 if(driver.findElements(By.xpath("//input[@id='outerForm:gridSupp:"+ i + ":unitsOut2']")).size()!= 0)
			 {	 
				driver.findElement(By.xpath("//input[@id='outerForm:gridSupp:" + i + ":unitsOut2']")).clear();
				 driver.findElement(By.xpath("//input[@id='outerForm:gridSupp:" + i + ":unitsOut2']")).sendKeys("25.00");
			}
			else { System.out.println("Not found"); }	
			}
		}
		 
		 ;} 
		 catch(Exception e) { PrintVal("Renderer Script Exception : "+e.toString()); } 
		 PrintVal("Executed Step = CALLRENDERERSCRIPT,COUNTSUPPLEMENTCHECKBOX"); 
        SWIPE.$("UP","TGTYPESCREENREG");

        }
          LOGGER.info("Executed Step = ENDIF,TGTYPESCREENREG");
        TAP.$("ELE_BUTTON_SUBMIT","//input[@type='submit'][@name='outerForm:Submit']","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_CANCEL","//input[@type='submit'][@name='outerForm:Cancel']","TGTYPESCREENREG");

        TAP.$("ELE_LINK_ISSUEDECLINEWITHDRAW","//span[text()='Issue/Decline/Withdraw']","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_EFFECTIVEDATE","//input[@type='text'][@name='outerForm:effectiveDate']",var_effectiveDate,"TRUE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_ISSUE","//input[@type='submit'][@name='outerForm:Issue']	","TGTYPESCREENREG");

        TAP.$("ELE_LINK_SETTLE","//span[text()='Settle']	","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SUBMIT","//input[@type='submit'][@name='outerForm:Submit']","TGTYPESCREENREG");

        ASSERT.$("ELE_GETMSG_SUCCESS","//li[@class='MessageInfo']","CONTAINS","successfully","TGTYPESCREENREG");

        CALL.$("WritePolicyQuickInquiryToCSV","TGTYPESCREENREG");

        HOVER.$("ELE_MENU_INQUIRY","//td[text()='Inquiry']","TGTYPESCREENREG");

        TAP.$("ELE_SUBMEN_QUICKINQUIRY","//td[contains(text(),'Quick Inquiry')]","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_POLICYNUMBER","//input[@type='text'][@name='outerForm:policyNumber']",var_PolicyNumber,"FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_CONTINUE","//input[@type='submit'][@name='outerForm:Continue']","TGTYPESCREENREG");

		writeToCSV ("Policy Number " , ActionWrapper.getElementValue("ELE_GETVAL_POLICYNUMBER", "//span[@id='outerForm:policyNumber']"));
                 LOGGER.info("Executed Step = WRITETOCSV,ELE_GETVAL_POLICYNUMBER,Policy Number,TGTYPESCREENREG");
		writeToCSV ("Status " , ActionWrapper.getElementValue("ELE_GETVAL_STATUS", "//span[@id='outerForm:statusText']"));
                 LOGGER.info("Executed Step = WRITETOCSV,ELE_GETVAL_STATUS,Status,TGTYPESCREENREG");
		writeToCSV ("Effective Date " , ActionWrapper.getElementValue("ELE_GETVAL_EFFECTIVEDATETEXTFROMGRID", "//span[@id='outerForm:grid:0:effectiveDateOut2']"));
                 LOGGER.info("Executed Step = WRITETOCSV,ELE_GETVAL_EFFECTIVEDATETEXTFROMGRID,Effective Date,TGTYPESCREENREG");
		writeToCSV ("State country " , ActionWrapper.getElementValue("ELE_GETVAL_STATECOUNTRY", "//span[@id='outerForm:ownerCityStateZip']"));
                 LOGGER.info("Executed Step = WRITETOCSV,ELE_GETVAL_STATECOUNTRY,State country,TGTYPESCREENREG");
		writeToCSV ("Agent Number " , ActionWrapper.getElementValue("ELE_GETVAL_AGENTNUMBER", "//span[@id='outerForm:agentNumber']"));
                 LOGGER.info("Executed Step = WRITETOCSV,ELE_GETVAL_AGENTNUMBER,Agent Number,TGTYPESCREENREG");
		var_Count = var_Count + ( 1 );
                 LOGGER.info("Executed Step = INCREASEVAR,var_Count,1,TGTYPESCREENREG");
    	}
          LOGGER.info("Executed Step = WEND,TGTYPESCREENREG");
    	resetAttemps();

    }


}
