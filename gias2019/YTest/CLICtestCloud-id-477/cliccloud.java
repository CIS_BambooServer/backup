package appium.tgo;

import com.tg.octopus.base.TGOWrapper;
import com.tg.octopus.libs.testng.listener.TestListener;
import static com.tg.octopus.base.TGOWrapper.Action.*;
import static com.tg.octopus.base.TGOWrapper.LOGGER;
import com.tg.octopus.wrapper.ActionWrapper;

import com.jayway.jsonpath.JsonPath;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.SessionNotCreatedException;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.Augmenter;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.remote.UnreachableBrowserException;

import org.testng.Assert;
import org.testng.ITestContext;
import org.testng.ITestResult;
import org.testng.Reporter;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;
import java.net.HttpURLConnection;
import java.net.URL;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import org.json.JSONArray;
import org.json.JSONObject;
import helper.WebTestUtility;
import java.sql.*;
import com.testautomationguru.utility.PDFUtil;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.multipdf.Splitter;

import java.util.*;
import org.openqa.selenium.*;
import java.io.*;
import java.net.*;

import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;


@Listeners(TestListener.class)
public class cliccloud extends TGOWrapper {



    static {
       needsresetoncebeforetest ="false";
       needsresetbeforeeverymethod ="true";
       captureverystepsnap ="true";
       appendlastsnap ="false";
       ishybridapp ="true";

    }


    @Test
    public void clic36reg29applytheloan() {

        // No need to declare / print method name here, handled by TestNg listener
        // declare("clic36reg29applytheloan");

		// Start of the \CLIC36Reg29ApplytheLoanname \ test Script;
		// TestScript Description:This TestScript is used for Applying loan;
		// Input File Name :CLIC36_IP_Reg29_ApplytheLoan;
		// Input file Location: C:/GIAS_Automation/CLIC Regression/InputData;
        CALL.$("LoginToCLICGIAS","TGTYPESCREENREG","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_USER","//input[@type='text'][@name='outerForm:username']","x7047","FALSE","TGTYPESCREENREG","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_PASSWORD","outerForm:passwordTGWEBCOMMAIdTGWEBCOMMA0","Big2blue2!","FALSE","TGTYPESCREENREG","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_LOGIN","outerForm:loginButtonTGWEBCOMMAIdTGWEBCOMMA0","TGTYPESCREENREG");

		int var_Count;
                 LOGGER.info("Executed Step = VAR,Integer,var_Count,0,TGTYPESCREENREG,TGTYPESCREENREG");
        LOGGER.info("Executed Step = REPEAT IF");
    	while (check(var_Count,"<",1,"TGTYPESCREENREG","TGTYPESCREENREG")) {
          LOGGER.info("Executed Step = WHILE,var_Count,<,1,TGTYPESCREENREG,TGTYPESCREENREG");
		com.jayway.jsonpath.ReadContext var_CSVData;
                 LOGGER.info("Executed Step = VAR,Json,var_CSVData,C:/GIAS_Automation/CLIC Regression/InputData/CLIC36_IP_Reg29_ApplytheLoan.csv,TGTYPESCREENREG,TGTYPESCREENREG");
		String var_PolicyNumber = "TGTYPESCREENREG";
                 LOGGER.info("Executed Step = VAR,String,var_PolicyNumber,TGTYPESCREENREG,TGTYPESCREENREG");
		var_PolicyNumber = var_CSVData;
                 LOGGER.info("Executed Step = STORE,var_PolicyNumber,var_CSVData,$.records[{var_Count}].PolicyNumber,TGTYPESCREENREG,TGTYPESCREENREG");
        CALL.$("InquirytoCheckPolicyNetValue","TGTYPESCREENREG","TGTYPESCREENREG");

        HOVER.$("ELE_MENU_INQUIRY","//td[text()='Inquiry']","TGTYPESCREENNO","TGTYPESCREENREG");

        TAP.$("ELE_SUBMEN_POLICYDETAIL","//td[text()='Policy Detail']","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_POLICYNUMBER","//input[@type='text'][@name='outerForm:policyNumber']",var_PolicyNumber,"FALSE","TGTYPESCREENREG","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_CONTINUE","//input[@type='submit'][@name='outerForm:Continue']","TGTYPESCREENREG");

        ASSERT.$("ELE_GETVAL_POLICYSTATUS","//span[@id='outerForm:statusText']","CONTAINS","Premium Paying","TGTYPESCREENREG","TGTYPESCREENREG");

        ASSERT.$("ELE_GETVAL_SUSPENDREASON","//span[@id='outerForm:suspendReasonCodeText']","CONTAINS","No","TGTYPESCREENREG","TGTYPESCREENREG");

		takeFullScreenShot();
                 LOGGER.info("Executed Step = TAKEFULLSCREENSHOT,TGTYPESCREENFULL,TGTYPESCREENREG");
        TAP.$("ELE_LINK_POLICYVALUES","//span[contains(text(),'Policy Values')]","TGTYPESCREENREG");

        ASSERT.$("ELE_GETVAL_NETPOLICYVALUE","//span[@id='outerForm:netCashValue']",">",0,"TGTYPESCREENREG","TGTYPESCREENREG");

        CALL.$("ProcessTheLoanTransaction","TGTYPESCREENREG","TGTYPESCREENREG");

        HOVER.$("ELE_MENU_POLICYOWNERSERVICES","//td[@class='ThemePanelMainFolderText'][contains(text(),'Policyowner Services')]","TGTYPESCREENNO","TGTYPESCREENREG");

		String var_TotalCalculatedGrossLoan;
                 LOGGER.info("Executed Step = VAR,String,var_TotalCalculatedGrossLoan,null,TGTYPESCREENREG,TGTYPESCREENREG");
        HOVER.$("ELE_SUBMEN_LOANLIEN","//td[contains(text(),'Loan/Lien')]","TGTYPESCREENNO","TGTYPESCREENREG");

        TAP.$("ELE_SUBMEN_LOAN","//div[@id='cmSubMenuID87']//tbody//tr[2]//td[2]","TGTYPESCREENNO","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_POLICYNUMBER","//input[@type='text'][@name='outerForm:policyNumber']",var_PolicyNumber,"FALSE","TGTYPESCREENREG","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_CONTINUE","//input[@type='submit'][@name='outerForm:Continue']","TGTYPESCREENREG","TGTYPESCREENREG");

		String var_RequestLoanAmt = "TGTYPESCREENREG";
                 LOGGER.info("Executed Step = VAR,String,var_RequestLoanAmt,TGTYPESCREENREG,TGTYPESCREENREG");
		var_RequestLoanAmt = ActionWrapper.getElementValue("ELE_GETVAL_MAXIMUMAVAILABLENETLOAN", "//span[@id='outerForm:maximumAvailableNetLoan']");
                 LOGGER.info("Executed Step = STORE,var_RequestLoanAmt,ELE_GETVAL_MAXIMUMAVAILABLENETLOAN,TGTYPESCREENREG,TGTYPESCREENREG");
		String var_RequestedLoanAmount = "TGTYPESCREENREG";
                 LOGGER.info("Executed Step = VAR,String,var_RequestedLoanAmount,TGTYPESCREENREG,TGTYPESCREENREG");
		var_RequestedLoanAmount = var_CSVData;
                 LOGGER.info("Executed Step = STORE,var_RequestedLoanAmount,var_CSVData,$.records[{var_Count}].RequestedLoanAmount,TGTYPESCREENREG,TGTYPESCREENREG");
		String var_MaximumAvailableNetLoan = "TGTYPESCREENREG";
                 LOGGER.info("Executed Step = VAR,String,var_MaximumAvailableNetLoan,TGTYPESCREENREG,TGTYPESCREENREG");
		var_MaximumAvailableNetLoan = ActionWrapper.getElementValue("ELE_GETVAL_MAXIMUMAVAILABLENETLOAN", "//span[@id='outerForm:maximumAvailableNetLoan']");
                 LOGGER.info("Executed Step = STORE,var_MaximumAvailableNetLoan,ELE_GETVAL_MAXIMUMAVAILABLENETLOAN,TGTYPESCREENREG,TGTYPESCREENREG");
		writeToCSV ("ELE_GETVAL_MAXIMUMAVAILABLENETLOAN " , ActionWrapper.getElementValue("ELE_GETVAL_MAXIMUMAVAILABLENETLOAN", "//span[@id='outerForm:maximumAvailableNetLoan']"));
                 LOGGER.info("Executed Step = WRITETOCSV,ELE_GETVAL_MAXIMUMAVAILABLENETLOAN,Maximum Loan Amount,TGTYPESCREENREG,TGTYPESCREENREG");
		String var_MaxLoanRequestType = "TGTYPESCREENREG";
                 LOGGER.info("Executed Step = VAR,String,var_MaxLoanRequestType,TGTYPESCREENREG,TGTYPESCREENREG");
		var_MaxLoanRequestType = var_CSVData;
                 LOGGER.info("Executed Step = STORE,var_MaxLoanRequestType,var_CSVData,$.records[{var_Count}].MaxLoan,TGTYPESCREENREG,TGTYPESCREENREG");
		String var_MaxLoanYes;
                 LOGGER.info("Executed Step = VAR,String,var_MaxLoanYes,Yes,TGTYPESCREENREG,TGTYPESCREENREG");
		String var_GrossorNetLoan = "TGTYPESCREENREG";
                 LOGGER.info("Executed Step = VAR,String,var_GrossorNetLoan,TGTYPESCREENREG,TGTYPESCREENREG");
		var_GrossorNetLoan = var_CSVData;
                 LOGGER.info("Executed Step = STORE,var_GrossorNetLoan,var_CSVData,$.records[{var_Count}].GrossorNet,TGTYPESCREENREG,TGTYPESCREENREG");
        LOGGER.info("Executed Step = START IF");
        if (check(var_MaxLoanRequestType,"CONTAINS",var_MaxLoanYes,"TGTYPESCREENREG","TGTYPESCREENREG")) {
         LOGGER.info("Executed Step = IF,var_MaxLoanRequestType,CONTAINS,var_MaxLoanYes,TGTYPESCREENREG,TGTYPESCREENREG");
        TAP.$("ELE_RDOBTN_MAXLOANREQUESTYES","//input[@id='outerForm:maximumLoanRequest:1']","TGTYPESCREENREG","TGTYPESCREENREG");

        WAIT.$(1,"TGTYPESCREENNO","TGTYPESCREENREG");

		String var_NetLoan = "TGTYPESCREENREG";
                 LOGGER.info("Executed Step = VAR,String,var_NetLoan,TGTYPESCREENREG,TGTYPESCREENREG");
		var_NetLoan = ActionWrapper.getElementValue("ELE_GETVAL_NETLOAN", "//span[@id='outerForm:netLoan']");
                 LOGGER.info("Executed Step = STORE,var_NetLoan,ELE_GETVAL_NETLOAN,TGTYPESCREENREG,TGTYPESCREENREG");
		String var_LoanInterestAmount = "TGTYPESCREENREG";
                 LOGGER.info("Executed Step = VAR,String,var_LoanInterestAmount,TGTYPESCREENREG,TGTYPESCREENREG");
		var_LoanInterestAmount = ActionWrapper.getElementValue("ELE_GETVAL_LOANINTERESTAMOUNT", "//span[@id='outerForm:loanInterestAmount']");
                 LOGGER.info("Executed Step = STORE,var_LoanInterestAmount,ELE_GETVAL_LOANINTERESTAMOUNT,TGTYPESCREENREG,TGTYPESCREENREG");
		String var_MaxRequestedLoanAmt = "TGTYPESCREENREG";
                 LOGGER.info("Executed Step = VAR,String,var_MaxRequestedLoanAmt,TGTYPESCREENREG,TGTYPESCREENREG");
		var_MaxRequestedLoanAmt = ActionWrapper.getElementValue("ELE_GETVAL_REQUESTEDLOANAMOUNT", "//span[@id='outerForm:loanRequest']");
                 LOGGER.info("Executed Step = STORE,var_MaxRequestedLoanAmt,ELE_GETVAL_REQUESTEDLOANAMOUNT,TGTYPESCREENREG,TGTYPESCREENREG");
		String var_AmounttoOwner = "TGTYPESCREENREG";
                 LOGGER.info("Executed Step = VAR,String,var_AmounttoOwner,TGTYPESCREENREG,TGTYPESCREENREG");
		var_AmounttoOwner = ActionWrapper.getElementValue("ELE_GETVAL_GROSSLOAN", "//span[@id='outerForm:grossLoan']");
                 LOGGER.info("Executed Step = STORE,var_AmounttoOwner,ELE_GETVAL_GROSSLOAN,TGTYPESCREENREG,TGTYPESCREENREG");
		try { 
		 0
		 
		 ;} 
		 catch(Exception e) { PrintVal("Renderer Script Exception : "+e.toString()); } 
		 PrintVal("Executed Step = CALLRENDERERSCRIPT,CHECKGROSSLOANAMOUNT"); 
		try { 
		 0
		 
		 ;} 
		 catch(Exception e) { PrintVal("Renderer Script Exception : "+e.toString()); } 
		 PrintVal("Executed Step = CALLRENDERERSCRIPT,TRIMMINGAMOUNTTOOWNERVALUE"); 
        ASSERT.$(var_MaxRequestedLoanAmt,"=",var_MaximumAvailableNetLoan,"TGTYPESCREENREG","TGTYPESCREENREG");

        ASSERT.$("ELE_GETMSG_WARN","//li[@class='MessageWarn']","CONTAINS","Distribution information","TGTYPESCREENREG","TGTYPESCREENREG");

        }
         else {
          LOGGER.info("Executed Step = ELSE,TGTYPESCREENREG,TGTYPESCREENREG");
        TAP.$("ELE_RDOBTN_MAXLOANREQUESTNO","//input[@id='outerForm:maximumLoanRequest:0']","TGTYPESCREENREG");

        LOGGER.info("Executed Step = START IF");
        if (check(var_GrossorNetLoan,"CONTAINS","Gross","TGTYPESCREENREG","TGTYPESCREENREG")) {
         LOGGER.info("Executed Step = IF,var_GrossorNetLoan,CONTAINS,Gross,TGTYPESCREENREG,TGTYPESCREENREG");
        TAP.$("ELE_RDOBTN_GROSS","//input[@id='outerForm:grossOrNetLoan:0']","TGTYPESCREENREG");

        }
          LOGGER.info("Executed Step = ENDIF,TGTYPESCREENREG,TGTYPESCREENREG");
        TYPE.$("ELE_TXTBOX_REQUESTEDLOANAMOUNT","//input[@type='text'][@name='outerForm:loanRequest']",var_RequestedLoanAmount,"FALSE","TGTYPESCREENREG","TGTYPESCREENREG");

		takeFullScreenShot();
                 LOGGER.info("Executed Step = TAKEFULLSCREENSHOT,TGTYPESCREENFULL,TGTYPESCREENREG");
        TAP.$("ELE_BUTTON_QUOTE2","//input[@id='outerForm:Quote']","TGTYPESCREENREG");

		String var_AmounttoOwner = "TGTYPESCREENREG";
                 LOGGER.info("Executed Step = VAR,String,var_AmounttoOwner,TGTYPESCREENREG,TGTYPESCREENREG");
		var_AmounttoOwner = ActionWrapper.getElementValue("ELE_GETVAL_GROSSLOAN", "//span[@id='outerForm:grossLoan']");
                 LOGGER.info("Executed Step = STORE,var_AmounttoOwner,ELE_GETVAL_GROSSLOAN,TGTYPESCREENREG,TGTYPESCREENREG");
		String var_NetLoan = "TGTYPESCREENREG";
                 LOGGER.info("Executed Step = VAR,String,var_NetLoan,TGTYPESCREENREG,TGTYPESCREENREG");
		var_NetLoan = ActionWrapper.getElementValue("ELE_GETVAL_NETLOAN", "//span[@id='outerForm:netLoan']");
                 LOGGER.info("Executed Step = STORE,var_NetLoan,ELE_GETVAL_NETLOAN,TGTYPESCREENREG,TGTYPESCREENREG");
		String var_LoanInterestAmount = "TGTYPESCREENREG";
                 LOGGER.info("Executed Step = VAR,String,var_LoanInterestAmount,TGTYPESCREENREG,TGTYPESCREENREG");
		var_LoanInterestAmount = ActionWrapper.getElementValue("ELE_GETVAL_LOANINTERESTAMOUNT", "//span[@id='outerForm:loanInterestAmount']");
                 LOGGER.info("Executed Step = STORE,var_LoanInterestAmount,ELE_GETVAL_LOANINTERESTAMOUNT,TGTYPESCREENREG,TGTYPESCREENREG");
		try { 
		 0
		 
		 ;} 
		 catch(Exception e) { PrintVal("Renderer Script Exception : "+e.toString()); } 
		 PrintVal("Executed Step = CALLRENDERERSCRIPT,CHECKGROSSLOANAMOUNT"); 
		try { 
		 0
		 
		 ;} 
		 catch(Exception e) { PrintVal("Renderer Script Exception : "+e.toString()); } 
		 PrintVal("Executed Step = CALLRENDERERSCRIPT,TRIMMINGAMOUNTTOOWNERVALUE"); 
        ASSERT.$(var_RequestedLoanAmount,"<=",var_MaximumAvailableNetLoan,"TGTYPESCREENREG","TGTYPESCREENREG");

        ASSERT.$("ELE_GETMSG_WARN","//li[@class='MessageWarn']","VISIBLE","TGTYPESCREENREG","TGTYPESCREENREG");

        }
          LOGGER.info("Executed Step = ENDIF,TGTYPESCREENREG,TGTYPESCREENREG");
        CALL.$("VerifyDistributionInformationandSubmitLoan","TGTYPESCREENREG","TGTYPESCREENREG");

        TAP.$("ELE_LINK_DISTRIBUTIONINFORMATION","//span[@id='outerForm:DistributionInformationText']","TGTYPESCREENREG");

        SELECT.$("ELE_DRPDWN_DISBURSEMENTCURRENCY","//select[@id='outerForm:disbursementCurrency']","Dollars [US]","TGTYPESCREENREG","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_CONTINUE","//input[@type='submit'][@name='outerForm:Continue']","TGTYPESCREENREG","TGTYPESCREENREG");

		takeFullScreenShot();
                 LOGGER.info("Executed Step = TAKEFULLSCREENSHOT,TGTYPESCREENFULL,TGTYPESCREENREG");
        TAP.$("ELE_BUTTON_SUBMIT","//input[@type='submit'][@name='outerForm:Submit']","TGTYPESCREENREG","TGTYPESCREENREG");

        ASSERT.$("ELE_GETMSG_SUCCESS","//li[@class='MessageInfo']","CONTAINS","Request completed successfully","TGTYPESCREENREG","TGTYPESCREENREG");

        CALL.$("ValidateLoan","TGTYPESCREENREG","TGTYPESCREENREG");

        HOVER.$("ELE_MENU_INQUIRY","//td[text()='Inquiry']","TGTYPESCREENNO","TGTYPESCREENREG");

        TAP.$("ELE_SUBMEN_POLICYDETAIL","//td[text()='Policy Detail']","TGTYPESCREENREG");

		String var_PolicyNumber2 = "TGTYPESCREENREG";
                 LOGGER.info("Executed Step = VAR,String,var_PolicyNumber2,TGTYPESCREENREG,TGTYPESCREENREG");
		var_PolicyNumber2 = var_CSVData;
                 LOGGER.info("Executed Step = STORE,var_PolicyNumber2,var_CSVData,$.records[{var_Count}].PolicyNumber,TGTYPESCREENREG,TGTYPESCREENREG");
		String var_LoanBal = "TGTYPESCREENREG";
                 LOGGER.info("Executed Step = VAR,String,var_LoanBal,TGTYPESCREENREG,TGTYPESCREENREG");
		var_LoanBal = ActionWrapper.getElementValue("ELE_GETVAL_LOANBALANCE", "//span[@id='outerForm:grid:0:currentLoanAmountOut2']");
                 LOGGER.info("Executed Step = STORE,var_LoanBal,ELE_GETVAL_LOANBALANCE,TGTYPESCREENREG,TGTYPESCREENREG");
        TYPE.$("ELE_TXTBOX_POLICYNUMBER","//input[@type='text'][@name='outerForm:policyNumber']",var_PolicyNumber2,"FALSE","TGTYPESCREENREG","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_CONTINUE","//input[@type='submit'][@name='outerForm:Continue']","TGTYPESCREENREG");

		writeToCSV ("ELE_GETVAL_POLICYNUMBER " , ActionWrapper.getElementValue("ELE_GETVAL_POLICYNUMBER", "//span[@id='outerForm:policyNumber']"));
                 LOGGER.info("Executed Step = WRITETOCSV,ELE_GETVAL_POLICYNUMBER,Policy Number,TGTYPESCREENREG,TGTYPESCREENREG");
        ASSERT.$("ELE_LINK_LOANS","//span[text()='Loans']","VISIBLE","TGTYPESCREENREG","TGTYPESCREENREG");

        TAP.$("ELE_LINK_LOANS","//span[text()='Loans']","TGTYPESCREENREG");

		String var_TotalLoanBal = "TGTYPESCREENREG";
                 LOGGER.info("Executed Step = VAR,String,var_TotalLoanBal,TGTYPESCREENREG,TGTYPESCREENREG");
		var_TotalLoanBal = ActionWrapper.getElementValue("ELE_GETVAL_LOANBALANCE", "//span[@id='outerForm:grid:0:currentLoanAmountOut2']");
                 LOGGER.info("Executed Step = STORE,var_TotalLoanBal,ELE_GETVAL_LOANBALANCE,TGTYPESCREENREG,TGTYPESCREENREG");
		writeToCSV ("ELE_GETVAL_LOANBALANCE " , ActionWrapper.getElementValue("ELE_GETVAL_LOANBALANCE", "//span[@id='outerForm:grid:0:currentLoanAmountOut2']"));
                 LOGGER.info("Executed Step = WRITETOCSV,ELE_GETVAL_LOANBALANCE,Loan Balance,TGTYPESCREENREG,TGTYPESCREENREG");
		var_Count = var_Count + ( 1 );
                 LOGGER.info("Executed Step = INCREASEVAR,var_Count,1,TGTYPESCREENREG,TGTYPESCREENREG");
    	}
          LOGGER.info("Executed Step = WEND,TGTYPESCREENREG,TGTYPESCREENREG");
    	resetAttemps();
		// End of TestScript CLIC36Reg29ApplytheLoanname ;

    }


}
