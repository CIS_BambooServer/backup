package appium.tgo;

import com.tg.octopus.base.TGOWrapper;
import com.tg.octopus.libs.testng.listener.TestListener;
import static com.tg.octopus.base.TGOWrapper.Action.*;
import static com.tg.octopus.base.TGOWrapper.LOGGER;
import com.tg.octopus.wrapper.ActionWrapper;

import com.jayway.jsonpath.JsonPath;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.SessionNotCreatedException;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.Augmenter;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.remote.UnreachableBrowserException;

import org.testng.Assert;
import org.testng.ITestContext;
import org.testng.ITestResult;
import org.testng.Reporter;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;
import java.net.HttpURLConnection;
import java.net.URL;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import org.json.JSONArray;
import org.json.JSONObject;
import helper.WebTestUtility;
import java.sql.*;
import com.testautomationguru.utility.PDFUtil;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.multipdf.Splitter;

import java.util.*;
import org.openqa.selenium.*;
import java.io.*;
import java.net.*;

import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;


@Listeners(TestListener.class)
public class Testinputfile extends TGOWrapper {



    static {
       needsresetoncebeforetest ="false";
       needsresetbeforeeverymethod ="true";
       captureverystepsnap ="true";
       appendlastsnap ="false";
       ishybridapp ="false";

    }


    @Test
    public void logintestcase() {

        // No need to declare / print method name here, handled by TestNg listener
        // declare("logintestcase");

        CALL.$("LOGINTOCLIC","TGTYPESCREENREG");

		int var_Count = 0;
                 LOGGER.info("Executed Step = VAR,Integer,var_Count,0,TGTYPESCREENREG");
		com.jayway.jsonpath.ReadContext var_CSVData = getJsonData("C:/GIAS_Automation/CLIC Regression/InputData/Login.csv");
                 LOGGER.info("Executed Step = VAR,Json,var_CSVData,C:/GIAS_Automation/CLIC Regression/InputData/Login.csv,TGTYPESCREENREG");
		String var_UserName;
                 LOGGER.info("Executed Step = VAR,String,var_UserName,TGTYPESCREENREG");
		var_UserName = getJsonData(var_CSVData , "$.records.["+var_Count+"].Username");
                 LOGGER.info("Executed Step = STORE,var_UserName,var_CSVData,$.records.[{var_Count}].Username,TGTYPESCREENREG");
		String var_Password;
                 LOGGER.info("Executed Step = VAR,String,var_Password,TGTYPESCREENREG");
		var_Password = getJsonData(var_CSVData , "$.records.["+var_Count+"].Password");
                 LOGGER.info("Executed Step = STORE,var_Password,var_CSVData,$.records.[{var_Count}].Password,TGTYPESCREENREG");
        TYPE.$("ELE_TXTBOX_USERNAME","//input[@id='outerForm:username']",var_UserName,"FALSE","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_PASSWORD","//input[@id='outerForm:password']",var_Password,"FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_LOGIN","//input[@id='outerForm:loginButton']","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");


    }


}
