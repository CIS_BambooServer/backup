package appium.tgo;

import com.tg.octopus.base.TGOWrapper;
import com.tg.octopus.libs.testng.listener.TestListener;
import static com.tg.octopus.base.TGOWrapper.Action.*;
import static com.tg.octopus.base.TGOWrapper.LOGGER;
import com.tg.octopus.wrapper.ActionWrapper;

import com.jayway.jsonpath.JsonPath;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.SessionNotCreatedException;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.Augmenter;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.remote.UnreachableBrowserException;

import org.testng.Assert;
import org.testng.ITestContext;
import org.testng.ITestResult;
import org.testng.Reporter;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;
import java.net.HttpURLConnection;
import java.net.URL;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import org.json.JSONArray;
import org.json.JSONObject;
import helper.WebTestUtility;
import java.sql.*;
import com.testautomationguru.utility.PDFUtil;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.multipdf.Splitter;

import java.util.*;
import org.openqa.selenium.*;
import java.io.*;
import java.net.*;

import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;


@Listeners(TestListener.class)
public class GIAS40_BaseGLC extends TGOWrapper {



    static {
       needsresetoncebeforetest ="false";
       needsresetbeforeeverymethod ="true";
       captureverystepsnap ="true";
       appendlastsnap ="false";
       ishybridapp ="false";

    }


    @Test
    public void addgroup() {

        // No need to declare / print method name here, handled by TestNg listener
        // declare("addgroup");

		int var_Count = 0;
                 LOGGER.info("Executed Step = VAR,Integer,var_Count,0,TGTYPESCREENREG");
		com.jayway.jsonpath.ReadContext var_CSVData = getJsonData("http://10.1.104.229/json/20200720/BwPwg3.json");
                 LOGGER.info("Executed Step = VAR,Json,var_CSVData,http://10.1.104.229/json/20200720/BwPwg3.json,TGTYPESCREENREG");
		String var_GroupName = "null";
                 LOGGER.info("Executed Step = VAR,String,var_GroupName,null,TGTYPESCREENREG");
		var_GroupName = getJsonData(var_CSVData , "$.records["+var_Count+"].GroupName");
                 LOGGER.info("Executed Step = STORE,var_GroupName,var_CSVData,$.records[{var_Count}].GroupName,TGTYPESCREENREG");
		String var_IDType = "null";
                 LOGGER.info("Executed Step = VAR,String,var_IDType,null,TGTYPESCREENREG");
		var_IDType = getJsonData(var_CSVData , "$.records["+var_Count+"].IDType");
                 LOGGER.info("Executed Step = STORE,var_IDType,var_CSVData,$.records[{var_Count}].IDType,TGTYPESCREENREG");
		String var_IDNumber = "null";
                 LOGGER.info("Executed Step = VAR,String,var_IDNumber,null,TGTYPESCREENREG");
		var_IDNumber = getJsonData(var_CSVData , "$.records["+var_Count+"].IDNumber");
                 LOGGER.info("Executed Step = STORE,var_IDNumber,var_CSVData,$.records[{var_Count}].IDNumber,TGTYPESCREENREG");
		String var_Address1 = "null";
                 LOGGER.info("Executed Step = VAR,String,var_Address1,null,TGTYPESCREENREG");
		var_Address1 = getJsonData(var_CSVData , "$.records["+var_Count+"].Address1");
                 LOGGER.info("Executed Step = STORE,var_Address1,var_CSVData,$.records[{var_Count}].Address1,TGTYPESCREENREG");
		String var_City = "null";
                 LOGGER.info("Executed Step = VAR,String,var_City,null,TGTYPESCREENREG");
		var_City = getJsonData(var_CSVData , "$.records["+var_Count+"].City");
                 LOGGER.info("Executed Step = STORE,var_City,var_CSVData,$.records[{var_Count}].City,TGTYPESCREENREG");
		String var_State = "null";
                 LOGGER.info("Executed Step = VAR,String,var_State,null,TGTYPESCREENREG");
		var_State = getJsonData(var_CSVData , "$.records["+var_Count+"].State");
                 LOGGER.info("Executed Step = STORE,var_State,var_CSVData,$.records[{var_Count}].State,TGTYPESCREENREG");
		String var_ZipCode = "null";
                 LOGGER.info("Executed Step = VAR,String,var_ZipCode,null,TGTYPESCREENREG");
		var_ZipCode = getJsonData(var_CSVData , "$.records["+var_Count+"].ZipCode");
                 LOGGER.info("Executed Step = STORE,var_ZipCode,var_CSVData,$.records[{var_Count}].ZipCode,TGTYPESCREENREG");
		String var_EffectiveDate = "null";
                 LOGGER.info("Executed Step = VAR,String,var_EffectiveDate,null,TGTYPESCREENREG");
		var_EffectiveDate = getJsonData(var_CSVData , "$.records["+var_Count+"].EffectiveDate");
                 LOGGER.info("Executed Step = STORE,var_EffectiveDate,var_CSVData,$.records[{var_Count}].EffectiveDate,TGTYPESCREENREG");
		String var_OriginalEffectiveDate = "null";
                 LOGGER.info("Executed Step = VAR,String,var_OriginalEffectiveDate,null,TGTYPESCREENREG");
		var_OriginalEffectiveDate = getJsonData(var_CSVData , "$.records["+var_Count+"].OriginalEffectiveDate");
                 LOGGER.info("Executed Step = STORE,var_OriginalEffectiveDate,var_CSVData,$.records[{var_Count}].OriginalEffectiveDate,TGTYPESCREENREG");
		String var_PlanYearBeginMonth = "null";
                 LOGGER.info("Executed Step = VAR,String,var_PlanYearBeginMonth,null,TGTYPESCREENREG");
		var_PlanYearBeginMonth = getJsonData(var_CSVData , "$.records["+var_Count+"].PlanYearBeginMonth");
                 LOGGER.info("Executed Step = STORE,var_PlanYearBeginMonth,var_CSVData,$.records[{var_Count}].PlanYearBeginMonth,TGTYPESCREENREG");
		String var_Currency = "null";
                 LOGGER.info("Executed Step = VAR,String,var_Currency,null,TGTYPESCREENREG");
		var_Currency = getJsonData(var_CSVData , "$.records["+var_Count+"].Currency");
                 LOGGER.info("Executed Step = STORE,var_Currency,var_CSVData,$.records[{var_Count}].Currency,TGTYPESCREENREG");
		String var_BenefitClassificationOption = "null";
                 LOGGER.info("Executed Step = VAR,String,var_BenefitClassificationOption,null,TGTYPESCREENREG");
		var_BenefitClassificationOption = getJsonData(var_CSVData , "$.records["+var_Count+"].BenefitClassificationOption");
                 LOGGER.info("Executed Step = STORE,var_BenefitClassificationOption,var_CSVData,$.records[{var_Count}].BenefitClassificationOption,TGTYPESCREENREG");
		String var_BillingScheduleTable = "null";
                 LOGGER.info("Executed Step = VAR,String,var_BillingScheduleTable,null,TGTYPESCREENREG");
		var_BillingScheduleTable = getJsonData(var_CSVData , "$.records["+var_Count+"].BillingScheduleTable");
                 LOGGER.info("Executed Step = STORE,var_BillingScheduleTable,var_CSVData,$.records[{var_Count}].BillingScheduleTable,TGTYPESCREENREG");
		String var_DeniedChargesRemarksTable = "null";
                 LOGGER.info("Executed Step = VAR,String,var_DeniedChargesRemarksTable,null,TGTYPESCREENREG");
		var_DeniedChargesRemarksTable = getJsonData(var_CSVData , "$.records["+var_Count+"].DeniedChargesRemarksTable");
                 LOGGER.info("Executed Step = STORE,var_DeniedChargesRemarksTable,var_CSVData,$.records[{var_Count}].DeniedChargesRemarksTable,TGTYPESCREENREG");
		String var_Monday = "null";
                 LOGGER.info("Executed Step = VAR,String,var_Monday,null,TGTYPESCREENREG");
		var_Monday = getJsonData(var_CSVData , "$.records["+var_Count+"].Monday");
                 LOGGER.info("Executed Step = STORE,var_Monday,var_CSVData,$.records[{var_Count}].Monday,TGTYPESCREENREG");
		String var_Tuesday = "null";
                 LOGGER.info("Executed Step = VAR,String,var_Tuesday,null,TGTYPESCREENREG");
		var_Tuesday = getJsonData(var_CSVData , "$.records["+var_Count+"].Tuesday");
                 LOGGER.info("Executed Step = STORE,var_Tuesday,var_CSVData,$.records[{var_Count}].Tuesday,TGTYPESCREENREG");
		String var_Wednesday = "null";
                 LOGGER.info("Executed Step = VAR,String,var_Wednesday,null,TGTYPESCREENREG");
		var_Wednesday = getJsonData(var_CSVData , "$.records["+var_Count+"].Wednesday");
                 LOGGER.info("Executed Step = STORE,var_Wednesday,var_CSVData,$.records[{var_Count}].Wednesday,TGTYPESCREENREG");
		String var_Thursday = "null";
                 LOGGER.info("Executed Step = VAR,String,var_Thursday,null,TGTYPESCREENREG");
		var_Thursday = getJsonData(var_CSVData , "$.records["+var_Count+"].Thursday");
                 LOGGER.info("Executed Step = STORE,var_Thursday,var_CSVData,$.records[{var_Count}].Thursday,TGTYPESCREENREG");
		String var_Friday = "null";
                 LOGGER.info("Executed Step = VAR,String,var_Friday,null,TGTYPESCREENREG");
		var_Friday = getJsonData(var_CSVData , "$.records["+var_Count+"].Friday");
                 LOGGER.info("Executed Step = STORE,var_Friday,var_CSVData,$.records[{var_Count}].Friday,TGTYPESCREENREG");
		String var_CommissionType = "null";
                 LOGGER.info("Executed Step = VAR,String,var_CommissionType,null,TGTYPESCREENREG");
		var_CommissionType = getJsonData(var_CSVData , "$.records["+var_Count+"].CommissionType");
                 LOGGER.info("Executed Step = STORE,var_CommissionType,var_CSVData,$.records[{var_Count}].CommissionType,TGTYPESCREENREG");
		String var_BeginningCheckNumber = "null";
                 LOGGER.info("Executed Step = VAR,String,var_BeginningCheckNumber,null,TGTYPESCREENREG");
		var_BeginningCheckNumber = getJsonData(var_CSVData , "$.records["+var_Count+"].BeginningCheckNumber");
                 LOGGER.info("Executed Step = STORE,var_BeginningCheckNumber,var_CSVData,$.records[{var_Count}].BeginningCheckNumber,TGTYPESCREENREG");
		String var_CheckFormNumber = "null";
                 LOGGER.info("Executed Step = VAR,String,var_CheckFormNumber,null,TGTYPESCREENREG");
		var_CheckFormNumber = getJsonData(var_CSVData , "$.records["+var_Count+"].CheckFormNumber");
                 LOGGER.info("Executed Step = STORE,var_CheckFormNumber,var_CSVData,$.records[{var_Count}].CheckFormNumber,TGTYPESCREENREG");
		String var_BankName = "null";
                 LOGGER.info("Executed Step = VAR,String,var_BankName,null,TGTYPESCREENREG");
		var_BankName = getJsonData(var_CSVData , "$.records["+var_Count+"].BankName");
                 LOGGER.info("Executed Step = STORE,var_BankName,var_CSVData,$.records[{var_Count}].BankName,TGTYPESCREENREG");
		String var_BankAddress = "null";
                 LOGGER.info("Executed Step = VAR,String,var_BankAddress,null,TGTYPESCREENREG");
		var_BankAddress = getJsonData(var_CSVData , "$.records["+var_Count+"].BankAddress");
                 LOGGER.info("Executed Step = STORE,var_BankAddress,var_CSVData,$.records[{var_Count}].BankAddress,TGTYPESCREENREG");
		String var_FederalBankFraction = "null";
                 LOGGER.info("Executed Step = VAR,String,var_FederalBankFraction,null,TGTYPESCREENREG");
		var_FederalBankFraction = getJsonData(var_CSVData , "$.records["+var_Count+"].FederalBankFraction");
                 LOGGER.info("Executed Step = STORE,var_FederalBankFraction,var_CSVData,$.records[{var_Count}].FederalBankFraction,TGTYPESCREENREG");
		String var_TransitRoutingNumber = "null";
                 LOGGER.info("Executed Step = VAR,String,var_TransitRoutingNumber,null,TGTYPESCREENREG");
		var_TransitRoutingNumber = getJsonData(var_CSVData , "$.records["+var_Count+"].TransitRoutingNumber");
                 LOGGER.info("Executed Step = STORE,var_TransitRoutingNumber,var_CSVData,$.records[{var_Count}].TransitRoutingNumber,TGTYPESCREENREG");
		String var_CheckDigit = "null";
                 LOGGER.info("Executed Step = VAR,String,var_CheckDigit,null,TGTYPESCREENREG");
		var_CheckDigit = getJsonData(var_CSVData , "$.records["+var_Count+"].CheckDigit");
                 LOGGER.info("Executed Step = STORE,var_CheckDigit,var_CSVData,$.records[{var_Count}].CheckDigit,TGTYPESCREENREG");
		String var_BranchAccountNumber = "null";
                 LOGGER.info("Executed Step = VAR,String,var_BranchAccountNumber,null,TGTYPESCREENREG");
		var_BranchAccountNumber = getJsonData(var_CSVData , "$.records["+var_Count+"].BranchAccountNumber");
                 LOGGER.info("Executed Step = STORE,var_BranchAccountNumber,var_CSVData,$.records[{var_Count}].BranchAccountNumber,TGTYPESCREENREG");
		String var_CheckNumberPrintPosition = "null";
                 LOGGER.info("Executed Step = VAR,String,var_CheckNumberPrintPosition,null,TGTYPESCREENREG");
		var_CheckNumberPrintPosition = getJsonData(var_CSVData , "$.records["+var_Count+"].CheckNumberPrintPosition");
                 LOGGER.info("Executed Step = STORE,var_CheckNumberPrintPosition,var_CSVData,$.records[{var_Count}].CheckNumberPrintPosition,TGTYPESCREENREG");
		String var_CheckNumberLength = "null";
                 LOGGER.info("Executed Step = VAR,String,var_CheckNumberLength,null,TGTYPESCREENREG");
		var_CheckNumberLength = getJsonData(var_CSVData , "$.records["+var_Count+"].CheckNumberLength");
                 LOGGER.info("Executed Step = STORE,var_CheckNumberLength,var_CSVData,$.records[{var_Count}].CheckNumberLength,TGTYPESCREENREG");
		String var_EOBFormNumber = "null";
                 LOGGER.info("Executed Step = VAR,String,var_EOBFormNumber,null,TGTYPESCREENREG");
		var_EOBFormNumber = getJsonData(var_CSVData , "$.records["+var_Count+"].EOBFormNumber");
                 LOGGER.info("Executed Step = STORE,var_EOBFormNumber,var_CSVData,$.records[{var_Count}].EOBFormNumber,TGTYPESCREENREG");
		String var_ReEstimateForm = "null";
                 LOGGER.info("Executed Step = VAR,String,var_ReEstimateForm,null,TGTYPESCREENREG");
		var_ReEstimateForm = getJsonData(var_CSVData , "$.records["+var_Count+"].ReEstimateForm");
                 LOGGER.info("Executed Step = STORE,var_ReEstimateForm,var_CSVData,$.records[{var_Count}].ReEstimateForm,TGTYPESCREENREG");
		String var_BulkCheckOption = "null";
                 LOGGER.info("Executed Step = VAR,String,var_BulkCheckOption,null,TGTYPESCREENREG");
		var_BulkCheckOption = getJsonData(var_CSVData , "$.records["+var_Count+"].BulkCheckOption");
                 LOGGER.info("Executed Step = STORE,var_BulkCheckOption,var_CSVData,$.records[{var_Count}].BulkCheckOption,TGTYPESCREENREG");
		String var_dataToPass = "null";
                 LOGGER.info("Executed Step = VAR,String,var_dataToPass,null,TGTYPESCREENREG");
        CALL.$("LoginGIAS","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENNO");

        TYPE.$("ELE_TXTBOX_USERNAME","//input[@id='guestForm:username']","x5992","FALSE","TGTYPESCREENNO");

        TYPE.$("ELE_TXTBOX_PASSWORD","//input[@id='guestForm:password']","P@ssword1","FALSE","TGTYPESCREENNO");

        TAP.$("ELE_BUTTON_LOGIN","//span[@class='ui-button-text ui-c']","TGTYPESCREENNO");

        CALL.$("NewGroup","TGTYPESCREENREG");

        WAIT.$(3,"TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_TASKSEARCH","//input[@id='workbenchForm:workbenchTabs:taskSearchContainer:taskSearchSearch']","Groups","FALSE","TGTYPESCREENREG");

        WAIT.$(1,"TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SEARCH","//a[@id='workbenchForm:workbenchTabs:taskSearchContainer:tb_buttonSearch']//span[@class='fa fa-search fa-lg']","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

        TAP.$("ELE_LINK_GROUPS","//button[@id='workbenchForm:workbenchTabs:taskSearchContainer:0:j_idt84']//span[@class='ui-button-text ui-c'][contains(text(),'Groups')]","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_ADD","//button[@id='workbenchForm:workbenchTabs:tb_buttonAdd']//span[@class='ui-button-text ui-c'][contains(text(),'Add')]","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_GROUPNAME","//input[@id='workbenchForm:workbenchTabs:groupNumber']",var_GroupName,"FALSE","TGTYPESCREENREG");

        WAIT.$(1,"TGTYPESCREENREG");

        TAP.$("ELE_LINK_SHOWLINKS","//span[@class='ui-button-icon-left ui-icon ui-c fa fa-ellipsis-v fa-2x']","TGTYPESCREENREG");

        WAIT.$(1,"TGTYPESCREENREG");

        TAP.$("ELE_LINK_SELECTCLIENT","//a[@class='ui-menuitem-link ui-corner-all']","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_ADDCLIENT","//span[contains(text(),'Add Client')]","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_CHANGETOCOMPANY","//span[contains(text(),'Change to Company')]","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_COMPANYNAME","//input[@id='workbenchForm:workbenchTabs:individualName']",var_GroupName,"FALSE","TGTYPESCREENREG");

        WAIT.$(1,"TGTYPESCREENREG");

		var_dataToPass = getJsonData(var_CSVData , "$.records["+var_Count+"].IDType");
                 LOGGER.info("Executed Step = STORE,var_dataToPass,var_CSVData,$.records[{var_Count}].IDType,TGTYPESCREENREG");
        TAP.$("ELE_DRPDWN_IDTYPE","//label[@id='workbenchForm:workbenchTabs:taxIdenUsag_label']","TGTYPESCREENREG");

        WAIT.$(1,"TGTYPESCREENREG");

		try { 
		 Thread.sleep(1000);
		WebElement selectValueOfDropDown= driver.findElement(By.xpath("//li[@data-label='"+ var_dataToPass +"']"));

		takeScreenshot();

				selectValueOfDropDown.click();
		// Add Screenshot will only work for Android platform

		takeScreenshot();

				 Thread.sleep(1000);
		 
		 ;} 
		 catch(Exception e) { PrintVal("Renderer Script Exception : "+e.toString()); } 
		 PrintVal("Executed Step = CALLRENDERERSCRIPT,SELECTVALUEFROMDROPDOWN"); 
        WAIT.$(2,"TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_IDNUMBER","//input[@id='workbenchForm:workbenchTabs:identificationNumber']",var_IDNumber,"FALSE","TGTYPESCREENREG");

        WAIT.$(1,"TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SAVE","//span[contains(text(),'Save')]","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

        ASSERT.$("ELE_MSG_ITEMADDED","//div[@id='workbenchForm:workbenchTabs:messages']//span[@class='ui-messages-info-summary'][contains(text(),'Item successfully added.')]","VISIBLE","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_ADDRESSLINE1","//input[@id='workbenchForm:workbenchTabs:addressLineOne']",var_Address1,"FALSE","TGTYPESCREENREG");

        WAIT.$(1,"TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_CITY","//input[@id='workbenchForm:workbenchTabs:addressCity']",var_City,"FALSE","TGTYPESCREENREG");

        WAIT.$(1,"TGTYPESCREENREG");

		var_dataToPass = getJsonData(var_CSVData , "$.records["+var_Count+"].State");
                 LOGGER.info("Executed Step = STORE,var_dataToPass,var_CSVData,$.records[{var_Count}].State,TGTYPESCREENREG");
        TAP.$("ELE_DRPDWN_STATEANDCOUNTRY","//label[@id='workbenchForm:workbenchTabs:countryAndStateCode_label']","TGTYPESCREENREG");

        WAIT.$(1,"TGTYPESCREENREG");

		try { 
		 WebElement dropdown = driver.findElement(By.xpath("//ul[@id='workbenchForm:workbenchTabs:countryAndStateCode_items']")); 

		 


		       //ul[@id='workbenchForm:workbenchTabs:distributionChannel_items']
		       // dropdown.click();
		       
		        List<WebElement> options = dropdown.findElements(By.tagName("li"));
		        
		        System.out.println("Size"+options.size());
		        for (WebElement option : options)
		        {
		            System.out.print("Values of option"+option);
		            if (option.getText().equals(var_dataToPass))
		            {
		                option.click(); // click the desired option
		                break;
		            }
		        }
		 
		 ;} 
		 catch(Exception e) { PrintVal("Renderer Script Exception : "+e.toString()); } 
		 PrintVal("Executed Step = CALLRENDERERSCRIPT,SELECTVALUEFROMDROPDOWNR"); 
        WAIT.$(2,"TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_ZIPCODE","//input[@id='workbenchForm:workbenchTabs:zipCode']",var_ZipCode,"FALSE","TGTYPESCREENREG");

        WAIT.$(1,"TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SAVE","//span[contains(text(),'Save')]","TGTYPESCREENREG");

        WAIT.$(3,"TGTYPESCREENREG");

        ASSERT.$("ELE_MSG_ITEMADDED","//div[@id='workbenchForm:workbenchTabs:messages']//span[@class='ui-messages-info-summary'][contains(text(),'Item successfully added.')]","VISIBLE","TGTYPESCREENREG");

        ASSERT.$("ELE_GETVAL_IDNUMBER","//span[@id='workbenchForm:workbenchTabs:identificationNumber']","VISIBLE","TGTYPESCREENREG");

        ASSERT.$("ELE_GETVAL_COMPANYNAME","//span[@id='workbenchForm:workbenchTabs:individualName']","VISIBLE","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_CONTINUE","//span[contains(text(),'Continue')]","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_EFFECTIVEDATE","//input[@id='workbenchForm:workbenchTabs:groupEffectiveDate_input']",var_EffectiveDate,"FALSE","TGTYPESCREENREG");

        WAIT.$(1,"TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_ORIGINALEFFECTIVEDATE","//input[@id='workbenchForm:workbenchTabs:originalEffectiveDate_input']",var_OriginalEffectiveDate,"FALSE","TGTYPESCREENREG");

        WAIT.$(1,"TGTYPESCREENREG");

		var_dataToPass = getJsonData(var_CSVData , "$.records["+var_Count+"].PlanYearBeginMonth");
                 LOGGER.info("Executed Step = STORE,var_dataToPass,var_CSVData,$.records[{var_Count}].PlanYearBeginMonth,TGTYPESCREENREG");
        TAP.$("ELE_DRPDWN_PLANYEARBEGINMONTH","//label[@id='workbenchForm:workbenchTabs:planYearBeginningMonth_label']","TGTYPESCREENREG");

        WAIT.$(1,"TGTYPESCREENREG");

		try { 
		 Thread.sleep(1000);
		WebElement selectValueOfDropDown= driver.findElement(By.xpath("//li[@data-label='"+ var_dataToPass +"']"));

		takeScreenshot();

				selectValueOfDropDown.click();
		// Add Screenshot will only work for Android platform

		takeScreenshot();

				 Thread.sleep(1000);
		 
		 ;} 
		 catch(Exception e) { PrintVal("Renderer Script Exception : "+e.toString()); } 
		 PrintVal("Executed Step = CALLRENDERERSCRIPT,SELECTVALUEFROMDROPDOWN"); 
        WAIT.$(2,"TGTYPESCREENREG");

		var_dataToPass = getJsonData(var_CSVData , "$.records["+var_Count+"].Currency");
                 LOGGER.info("Executed Step = STORE,var_dataToPass,var_CSVData,$.records[{var_Count}].Currency,TGTYPESCREENREG");
        TAP.$("ELE_DRPDWN_CURRENCY","//label[@id='workbenchForm:workbenchTabs:currencyCode_label']","TGTYPESCREENREG");

        WAIT.$(1,"TGTYPESCREENREG");

		try { 
		 Thread.sleep(1000);
		WebElement selectValueOfDropDown= driver.findElement(By.xpath("//li[@data-label='"+ var_dataToPass +"']"));

		takeScreenshot();

				selectValueOfDropDown.click();
		// Add Screenshot will only work for Android platform

		takeScreenshot();

				 Thread.sleep(1000);
		 
		 ;} 
		 catch(Exception e) { PrintVal("Renderer Script Exception : "+e.toString()); } 
		 PrintVal("Executed Step = CALLRENDERERSCRIPT,SELECTVALUEFROMDROPDOWN"); 
        WAIT.$(1,"TGTYPESCREENREG");

		var_dataToPass = getJsonData(var_CSVData , "$.records["+var_Count+"].BenefitClassificationOption");
                 LOGGER.info("Executed Step = STORE,var_dataToPass,var_CSVData,$.records[{var_Count}].BenefitClassificationOption,TGTYPESCREENREG");
        TAP.$("ELE_DRPDWN_BENEFITCLASSIFICATIONOPTION","//label[@id='workbenchForm:workbenchTabs:productSetIndicator_label']","TGTYPESCREENREG");

        WAIT.$(1,"TGTYPESCREENREG");

		try { 
		 Thread.sleep(1000);
		WebElement selectValueOfDropDown= driver.findElement(By.xpath("//li[@data-label='"+ var_dataToPass +"']"));

		takeScreenshot();

				selectValueOfDropDown.click();
		// Add Screenshot will only work for Android platform

		takeScreenshot();

				 Thread.sleep(1000);
		 
		 ;} 
		 catch(Exception e) { PrintVal("Renderer Script Exception : "+e.toString()); } 
		 PrintVal("Executed Step = CALLRENDERERSCRIPT,SELECTVALUEFROMDROPDOWN"); 
        WAIT.$(1,"TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_BILLINGSCHEDULETABLE","//input[@id='workbenchForm:workbenchTabs:billingTableNumber_input']",var_BillingScheduleTable,"FALSE","TGTYPESCREENREG");

        WAIT.$(1,"TGTYPESCREENREG");

        SWIPE.$("DOWN","TGTYPESCREENREG");

        SWIPE.$("DOWN","TGTYPESCREENREG");

        SWIPE.$("DOWN","TGTYPESCREENREG");

        SWIPE.$("DOWN","TGTYPESCREENREG");

        SWIPE.$("LEFT","TGTYPESCREENREG");

        SWIPE.$("LEFT","TGTYPESCREENREG");

        SWIPE.$("DOWN","TGTYPESCREENREG");

        WAIT.$(1,"TGTYPESCREENREG");

		var_dataToPass = getJsonData(var_CSVData , "$.records["+var_Count+"].DeniedChargesRemarksTable");
                 LOGGER.info("Executed Step = STORE,var_dataToPass,var_CSVData,$.records[{var_Count}].DeniedChargesRemarksTable,TGTYPESCREENREG");
        TAP.$("ELE_DRPDWN_DENIEDCHARGESREMARKSTABLE2","//div[@id='workbenchForm:workbenchTabs:eobRemarkCodeTable']//span[@class='ui-icon ui-icon-triangle-1-s ui-c']","TGTYPESCREENREG");

        WAIT.$(1,"TGTYPESCREENREG");

		try { 
		 WebElement dropdown = driver.findElement(By.xpath("//ul[@id='workbenchForm:workbenchTabs:eobRemarkCodeTable_items']")); 

		 


		       //ul[@id='workbenchForm:workbenchTabs:distributionChannel_items']
		       // dropdown.click();
		       
		        List<WebElement> options = dropdown.findElements(By.tagName("li"));
		        
		        System.out.println("Size"+options.size());
		        for (WebElement option : options)
		        {
		            System.out.print("Values of option"+option);
		            if (option.getText().equals(var_dataToPass))
		            {
		                option.click(); // click the desired option
		                break;
		            }
		        }
		 
		 ;} 
		 catch(Exception e) { PrintVal("Renderer Script Exception : "+e.toString()); } 
		 PrintVal("Executed Step = CALLRENDERERSCRIPT,SELECTVALUEFROMDENIEDCHARGESREMARKS"); 
        WAIT.$(1,"TGTYPESCREENREG");

        SWIPE.$("UP","TGTYPESCREENREG");

        WAIT.$(1,"TGTYPESCREENREG");

		var_dataToPass = getJsonData(var_CSVData , "$.records["+var_Count+"].Monday");
                 LOGGER.info("Executed Step = STORE,var_dataToPass,var_CSVData,$.records[{var_Count}].Monday,TGTYPESCREENREG");
        TAP.$("ELE_DRPDWN_MONDAY","//label[@id='workbenchForm:workbenchTabs:checkProcessingDays_label']","TGTYPESCREENREG");

        WAIT.$(1,"TGTYPESCREENREG");

		try { 
		 Thread.sleep(1000);
		WebElement selectValueOfDropDown= driver.findElement(By.xpath("//li[@data-label='"+ var_dataToPass +"']"));

		takeScreenshot();

				selectValueOfDropDown.click();
		// Add Screenshot will only work for Android platform

		takeScreenshot();

				 Thread.sleep(1000);
		 
		 ;} 
		 catch(Exception e) { PrintVal("Renderer Script Exception : "+e.toString()); } 
		 PrintVal("Executed Step = CALLRENDERERSCRIPT,SELECTVALUEFROMDROPDOWN"); 
        WAIT.$(1,"TGTYPESCREENREG");

        SWIPE.$("UP","TGTYPESCREENREG");

        WAIT.$(1,"TGTYPESCREENREG");

		var_dataToPass = getJsonData(var_CSVData , "$.records["+var_Count+"].Tuesday");
                 LOGGER.info("Executed Step = STORE,var_dataToPass,var_CSVData,$.records[{var_Count}].Tuesday,TGTYPESCREENREG");
        TAP.$("ELE_DRPDWN_TUESDAY","//label[@id='workbenchForm:workbenchTabs:checkProcessingDays1_label']","TGTYPESCREENREG");

        WAIT.$(1,"TGTYPESCREENREG");

		try { 
		 WebElement dropdown = driver.findElement(By.xpath("//ul[@id='workbenchForm:workbenchTabs:checkProcessingDays1_items']")); 

		 


		       //ul[@id='workbenchForm:workbenchTabs:distributionChannel_items']
		       // dropdown.click();
		       
		        List<WebElement> options = dropdown.findElements(By.tagName("li"));
		        
		        System.out.println("Size"+options.size());
		        for (WebElement option : options)
		        {
		            System.out.print("Values of option"+option);
		            if (option.getText().equals(var_dataToPass))
		            {
		                option.click(); // click the desired option
		                break;
		            }
		        }
		 
		 ;} 
		 catch(Exception e) { PrintVal("Renderer Script Exception : "+e.toString()); } 
		 PrintVal("Executed Step = CALLRENDERERSCRIPT,SELECTTUESDAY"); 
        WAIT.$(2,"TGTYPESCREENREG");

		var_dataToPass = getJsonData(var_CSVData , "$.records["+var_Count+"].Wednesday");
                 LOGGER.info("Executed Step = STORE,var_dataToPass,var_CSVData,$.records[{var_Count}].Wednesday,TGTYPESCREENREG");
        TAP.$("ELE_DRPDWN_WEDNESDAY","//label[@id='workbenchForm:workbenchTabs:checkProcessingDays2_label']","TGTYPESCREENREG");

        WAIT.$(1,"TGTYPESCREENREG");

		try { 
		 WebElement dropdown = driver.findElement(By.xpath("//ul[@id='workbenchForm:workbenchTabs:checkProcessingDays2_items']")); 

		 


		       //ul[@id='workbenchForm:workbenchTabs:distributionChannel_items']
		       // dropdown.click();
		       
		        List<WebElement> options = dropdown.findElements(By.tagName("li"));
		        
		        System.out.println("Size"+options.size());
		        for (WebElement option : options)
		        {
		            System.out.print("Values of option"+option);
		            if (option.getText().equals(var_dataToPass))
		            {
		                option.click(); // click the desired option
		                break;
		            }
		        }
		 
		 ;} 
		 catch(Exception e) { PrintVal("Renderer Script Exception : "+e.toString()); } 
		 PrintVal("Executed Step = CALLRENDERERSCRIPT,SELECTWEDNESDAY"); 
        WAIT.$(2,"TGTYPESCREENREG");

		var_dataToPass = getJsonData(var_CSVData , "$.records["+var_Count+"].Thursday");
                 LOGGER.info("Executed Step = STORE,var_dataToPass,var_CSVData,$.records[{var_Count}].Thursday,TGTYPESCREENREG");
        TAP.$("ELE_DRPDWN_THURSDAY","//label[@id='workbenchForm:workbenchTabs:checkProcessingDays3_label']","TGTYPESCREENREG");

        WAIT.$(1,"TGTYPESCREENREG");

		try { 
		 WebElement dropdown = driver.findElement(By.xpath("//ul[@id='workbenchForm:workbenchTabs:checkProcessingDays3_items']")); 

		 


		       //ul[@id='workbenchForm:workbenchTabs:distributionChannel_items']
		       // dropdown.click();
		       
		        List<WebElement> options = dropdown.findElements(By.tagName("li"));
		        
		        System.out.println("Size"+options.size());
		        for (WebElement option : options)
		        {
		            System.out.print("Values of option"+option);
		            if (option.getText().equals(var_dataToPass))
		            {
		                option.click(); // click the desired option
		                break;
		            }
		        }
		 
		 ;} 
		 catch(Exception e) { PrintVal("Renderer Script Exception : "+e.toString()); } 
		 PrintVal("Executed Step = CALLRENDERERSCRIPT,SELECTTHURSDAY"); 
        WAIT.$(2,"TGTYPESCREENREG");

		var_dataToPass = getJsonData(var_CSVData , "$.records["+var_Count+"].Friday");
                 LOGGER.info("Executed Step = STORE,var_dataToPass,var_CSVData,$.records[{var_Count}].Friday,TGTYPESCREENREG");
        TAP.$("ELE_DRPDWN_FRIDAY","//label[@id='workbenchForm:workbenchTabs:checkProcessingDays4_label']","TGTYPESCREENREG");

        WAIT.$(1,"TGTYPESCREENREG");

		try { 
		 WebElement dropdown = driver.findElement(By.xpath("//ul[@id='workbenchForm:workbenchTabs:checkProcessingDays4_items']")); 

		 


		       //ul[@id='workbenchForm:workbenchTabs:distributionChannel_items']
		       // dropdown.click();
		       
		        List<WebElement> options = dropdown.findElements(By.tagName("li"));
		        
		        System.out.println("Size"+options.size());
		        for (WebElement option : options)
		        {
		            System.out.print("Values of option"+option);
		            if (option.getText().equals(var_dataToPass))
		            {
		                option.click(); // click the desired option
		                break;
		            }
		        }
		 
		 ;} 
		 catch(Exception e) { PrintVal("Renderer Script Exception : "+e.toString()); } 
		 PrintVal("Executed Step = CALLRENDERERSCRIPT,SELECTFRIDAY"); 
        WAIT.$(2,"TGTYPESCREENREG");

        SWIPE.$("UP","TGTYPESCREENREG");

		var_dataToPass = getJsonData(var_CSVData , "$.records["+var_Count+"].CommissionType");
                 LOGGER.info("Executed Step = STORE,var_dataToPass,var_CSVData,$.records[{var_Count}].CommissionType,TGTYPESCREENREG");
        TAP.$("ELE_DRPDWN_COMMISSIONTYPE","//label[@id='workbenchForm:workbenchTabs:commissionType_label']","TGTYPESCREENREG");

        WAIT.$(1,"TGTYPESCREENREG");

		try { 
		 Thread.sleep(1000);
		WebElement selectValueOfDropDown= driver.findElement(By.xpath("//li[@data-label='"+ var_dataToPass +"']"));

		takeScreenshot();

				selectValueOfDropDown.click();
		// Add Screenshot will only work for Android platform

		takeScreenshot();

				 Thread.sleep(1000);
		 
		 ;} 
		 catch(Exception e) { PrintVal("Renderer Script Exception : "+e.toString()); } 
		 PrintVal("Executed Step = CALLRENDERERSCRIPT,SELECTVALUEFROMDROPDOWN"); 
        WAIT.$(1,"TGTYPESCREENREG");

        SWIPE.$("DOWN","TGTYPESCREENREG");

        SWIPE.$("DOWN","TGTYPESCREENREG");

        SWIPE.$("DOWN","TGTYPESCREENREG");

        SWIPE.$("DOWN","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SAVE","//span[contains(text(),'Save')]","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

		try { 
		 driver.switchTo().frame(0);
		        
		 driver.findElement(By.xpath("//span[contains(text(),'Yes')]")).click();

		takeFullScreenShot();
		 
		 ;} 
		 catch(Exception e) { PrintVal("Renderer Script Exception : "+e.toString()); } 
		 PrintVal("Executed Step = CALLRENDERERSCRIPT,CONFIRMACTIONYES"); 
        WAIT.$(3,"TGTYPESCREENREG");

        TAP.$("ELE_LINK_SHOWLINKS","//span[@class='ui-button-icon-left ui-icon ui-c fa fa-ellipsis-v fa-2x']","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

        TAP.$("ELE_LINK_GROUPCHECKANDEOB","//span[contains(text(),'Group Check and EOB Details')]","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_EDIT","//span[@class='ui-button-text ui-c'][contains(text(),'Edit')]","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

        TAP.$("ELE_RDOBTN_PRINTCHECKNUMBERSANDMICRDATA_YES","//table[@id='workbenchForm:workbenchTabs:generateUniversalChecksY']//span[@class='ui-radiobutton-icon ui-icon ui-icon-blank ui-c']","TGTYPESCREENREG");

        WAIT.$(1,"TGTYPESCREENREG");

        TYPE.$("ELE_TEXTBOX_BEGINNINGCHECKNUMBER","//input[@id='workbenchForm:workbenchTabs:beginningCheckNumber_input']",var_BeginningCheckNumber,"FALSE","TGTYPESCREENREG");

        WAIT.$(1,"TGTYPESCREENREG");

		var_dataToPass = getJsonData(var_CSVData , "$.records["+var_Count+"].Option");
                 LOGGER.info("Executed Step = STORE,var_dataToPass,var_CSVData,$.records[{var_Count}].Option,TGTYPESCREENREG");
        TAP.$("ELE_DRPDWN_OPTION","//label[@id='workbenchForm:workbenchTabs:eobSpecialProcessOption_label']","TGTYPESCREENREG");

        WAIT.$(1,"TGTYPESCREENREG");

		try { 
		 Thread.sleep(1000);
		WebElement selectValueOfDropDown= driver.findElement(By.xpath("//li[@data-label='"+ var_dataToPass +"']"));

		takeScreenshot();

				selectValueOfDropDown.click();
		// Add Screenshot will only work for Android platform

		takeScreenshot();

				 Thread.sleep(1000);
		 
		 ;} 
		 catch(Exception e) { PrintVal("Renderer Script Exception : "+e.toString()); } 
		 PrintVal("Executed Step = CALLRENDERERSCRIPT,SELECTVALUEFROMDROPDOWN"); 
        WAIT.$(1,"TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_CHECKFORMNUMBER","//input[@id='workbenchForm:workbenchTabs:formsQueueCheckEob_input']",var_CheckFormNumber,"FALSE","TGTYPESCREENREG");

        WAIT.$(1,"TGTYPESCREENREG");

        TAP.$("ELE_RDOBTN_PRINTGROUPNAME_YES","//table[@id='workbenchForm:workbenchTabs:printGroupNameCheckEob']//span[@class='ui-radiobutton-icon ui-icon ui-icon-blank ui-c']","TGTYPESCREENREG");

        WAIT.$(1,"TGTYPESCREENREG");

        TAP.$("ELE_RDOBTN_PRINTLOCATIONNAME_YES","//table[@id='workbenchForm:workbenchTabs:printLocationNameCheckE']//span[@class='ui-radiobutton-icon ui-icon ui-icon-blank ui-c']","TGTYPESCREENREG");

        WAIT.$(1,"TGTYPESCREENREG");

        TAP.$("ELE_RDOBTN_PRINTPROVIDERTAXIDENTIFIER_YES","//table[@id='workbenchForm:workbenchTabs:printProviderTaxId']//span[@class='ui-radiobutton-icon ui-icon ui-icon-blank ui-c']","TGTYPESCREENREG");

        WAIT.$(1,"TGTYPESCREENREG");

		var_dataToPass = getJsonData(var_CSVData , "$.records["+var_Count+"].BulkCheckOption");
                 LOGGER.info("Executed Step = STORE,var_dataToPass,var_CSVData,$.records[{var_Count}].BulkCheckOption,TGTYPESCREENREG");
        TAP.$("ELE_DRPDWN_BULKCHECKOPTION","//label[@id='workbenchForm:workbenchTabs:bulkProviderIndicator_label']","TGTYPESCREENREG");

        WAIT.$(1,"TGTYPESCREENREG");

		try { 
		 Thread.sleep(1000);
		WebElement selectValueOfDropDown= driver.findElement(By.xpath("//li[@data-label='"+ var_dataToPass +"']"));

		takeScreenshot();

				selectValueOfDropDown.click();
		// Add Screenshot will only work for Android platform

		takeScreenshot();

				 Thread.sleep(1000);
		 
		 ;} 
		 catch(Exception e) { PrintVal("Renderer Script Exception : "+e.toString()); } 
		 PrintVal("Executed Step = CALLRENDERERSCRIPT,SELECTVALUEFROMDROPDOWN"); 
        WAIT.$(1,"TGTYPESCREENREG");

        SWIPE.$("DOWN","TGTYPESCREENREG");

        SWIPE.$("DOWN","TGTYPESCREENREG");

        WAIT.$(1,"TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_BANKNAME","//input[@id='workbenchForm:workbenchTabs:bankNameLines']",var_BankName,"FALSE","TGTYPESCREENREG");

        WAIT.$(1,"TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_BANKADDRESS","//input[@id='workbenchForm:workbenchTabs:bankNameLines1']",var_BankAddress,"FALSE","TGTYPESCREENREG");

        WAIT.$(1,"TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_FEDERALBANKFRACTION","//input[@id='workbenchForm:workbenchTabs:federalBankNumberFraction']",var_FederalBankFraction,"FALSE","TGTYPESCREENREG");

        WAIT.$(1,"TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_TRANSITROUTINGNUMBER","//input[@id='workbenchForm:workbenchTabs:bankRoutingNumber_input']",var_TransitRoutingNumber,"FALSE","TGTYPESCREENREG");

        WAIT.$(1,"TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_CHECKDIGIT","//input[@id='workbenchForm:workbenchTabs:checkDigit_input']",var_CheckDigit,"FALSE","TGTYPESCREENREG");

        WAIT.$(1,"TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_BRANCHACCOUNTNUMBER","//input[@id='workbenchForm:workbenchTabs:branchAndAccountNumbers']",var_BranchAccountNumber,"FALSE","TGTYPESCREENREG");

        WAIT.$(1,"TGTYPESCREENREG");

        TAP.$("ELE_RDOBTN_PRINTMICRCHECKAMOUNT_YES","//table[@id='workbenchForm:workbenchTabs:micrCheckAmountIndicator']//span[@class='ui-radiobutton-icon ui-icon ui-icon-blank ui-c']","TGTYPESCREENREG");

        WAIT.$(1,"TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_CHECKNUMBERPRINTPOSITION","//input[@id='workbenchForm:workbenchTabs:checkNumberPrintPosition_input']",var_CheckNumberPrintPosition,"FALSE","TGTYPESCREENREG");

        WAIT.$(1,"TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_CHECKNUMBERLENGTH","//input[@id='workbenchForm:workbenchTabs:checkNumberLength_input']",var_CheckNumberLength,"FALSE","TGTYPESCREENREG");

        WAIT.$(1,"TGTYPESCREENREG");

        SWIPE.$("DOWN","TGTYPESCREENREG");

        SWIPE.$("DOWN","TGTYPESCREENREG");

        SWIPE.$("DOWN","TGTYPESCREENREG");

        WAIT.$(1,"TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_EOBFORMNUMBER","//input[@id='workbenchForm:workbenchTabs:formsQueueNonPayments_input']",var_EOBFormNumber,"FALSE","TGTYPESCREENREG");

        WAIT.$(1,"TGTYPESCREENREG");

        TAP.$("ELE_RDOBTN_PRINTGROUPNAME_YES","//table[@id='workbenchForm:workbenchTabs:printGroupNameCheckEob']//span[@class='ui-radiobutton-icon ui-icon ui-icon-blank ui-c']","TGTYPESCREENREG");

        WAIT.$(1,"TGTYPESCREENREG");

        TAP.$("ELE_RDOBTN_PRINTLOCATIONNAME_YES","//table[@id='workbenchForm:workbenchTabs:printLocationNameCheckE']//span[@class='ui-radiobutton-icon ui-icon ui-icon-blank ui-c']","TGTYPESCREENREG");

        WAIT.$(1,"TGTYPESCREENREG");

        SWIPE.$("DOWN","TGTYPESCREENREG");

        SWIPE.$("DOWN","TGTYPESCREENREG");

        WAIT.$(1,"TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_PREESTIMATEFORMNUMBER","//input[@id='workbenchForm:workbenchTabs:formsQueuePreEstimates_input']",var_ReEstimateForm,"FALSE","TGTYPESCREENREG");

        WAIT.$(1,"TGTYPESCREENREG");

        SWIPE.$("DOWN","TGTYPESCREENREG");

        SWIPE.$("DOWN","TGTYPESCREENREG");

        WAIT.$(1,"TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SAVE","//span[contains(text(),'Save')]","TGTYPESCREENREG");

        WAIT.$(3,"TGTYPESCREENREG");

        SWIPE.$("DOWN","TGTYPESCREENREG");

        SWIPE.$("DOWN","TGTYPESCREENREG");

        SWIPE.$("DOWN","TGTYPESCREENREG");

        ASSERT.$("ELE_GETMSG_UPDATED","//div[@id='workbenchForm:workbenchTabs:messages']//span[@class='ui-messages-info-summary'][contains(text(),'Item successfully updated.')]","VISIBLE","TGTYPESCREENREG");


    }


    @Test
    public void groupproducts() {

        // No need to declare / print method name here, handled by TestNg listener
        // declare("groupproducts");

		int var_Count = 0;
                 LOGGER.info("Executed Step = VAR,Integer,var_Count,0,TGTYPESCREENREG");
		com.jayway.jsonpath.ReadContext var_CSVData = getJsonData("http://10.1.104.229/json/20200831/6VYq0i.json");
                 LOGGER.info("Executed Step = VAR,Json,var_CSVData,http://10.1.104.229/json/20200831/6VYq0i.json,TGTYPESCREENREG");
        LOGGER.info("Executed Step = REPEAT IF");
    	while (check(var_Count,"<",4,"TGTYPESCREENREG")) {
          LOGGER.info("Executed Step = WHILE,var_Count,<,4,TGTYPESCREENREG");
		String var_GroupName = "null";
                 LOGGER.info("Executed Step = VAR,String,var_GroupName,null,TGTYPESCREENREG");
		var_GroupName = getJsonData(var_CSVData , "$.records["+var_Count+"].GroupName");
                 LOGGER.info("Executed Step = STORE,var_GroupName,var_CSVData,$.records[{var_Count}].GroupName,TGTYPESCREENREG");
		String var_ProductIdentifier = "null";
                 LOGGER.info("Executed Step = VAR,String,var_ProductIdentifier,null,TGTYPESCREENREG");
		var_ProductIdentifier = getJsonData(var_CSVData , "$.records["+var_Count+"].ProductIdentifier");
                 LOGGER.info("Executed Step = STORE,var_ProductIdentifier,var_CSVData,$.records[{var_Count}].ProductIdentifier,TGTYPESCREENREG");
		String var_ProductName = "null";
                 LOGGER.info("Executed Step = VAR,String,var_ProductName,null,TGTYPESCREENREG");
		var_ProductName = getJsonData(var_CSVData , "$.records["+var_Count+"].ProductName");
                 LOGGER.info("Executed Step = STORE,var_ProductName,var_CSVData,$.records[{var_Count}].ProductName,TGTYPESCREENREG");
		String var_EffectiveDate = "null";
                 LOGGER.info("Executed Step = VAR,String,var_EffectiveDate,null,TGTYPESCREENREG");
		var_EffectiveDate = getJsonData(var_CSVData , "$.records["+var_Count+"].EffectiveDate");
                 LOGGER.info("Executed Step = STORE,var_EffectiveDate,var_CSVData,$.records[{var_Count}].EffectiveDate,TGTYPESCREENREG");
		String var_InsuranceType = "null";
                 LOGGER.info("Executed Step = VAR,String,var_InsuranceType,null,TGTYPESCREENREG");
		var_InsuranceType = getJsonData(var_CSVData , "$.records["+var_Count+"].InsuranceType");
                 LOGGER.info("Executed Step = STORE,var_InsuranceType,var_CSVData,$.records[{var_Count}].InsuranceType,TGTYPESCREENREG");
		String var_LineofBusiness = "null";
                 LOGGER.info("Executed Step = VAR,String,var_LineofBusiness,null,TGTYPESCREENREG");
		var_LineofBusiness = getJsonData(var_CSVData , "$.records["+var_Count+"].LineofBusiness");
                 LOGGER.info("Executed Step = STORE,var_LineofBusiness,var_CSVData,$.records[{var_Count}].LineofBusiness,TGTYPESCREENREG");
		String var_ProductLinkType = "null";
                 LOGGER.info("Executed Step = VAR,String,var_ProductLinkType,null,TGTYPESCREENREG");
		var_ProductLinkType = getJsonData(var_CSVData , "$.records["+var_Count+"].ProductLinkType");
                 LOGGER.info("Executed Step = STORE,var_ProductLinkType,var_CSVData,$.records[{var_Count}].ProductLinkType,TGTYPESCREENREG");
		String var_CoverageLevels = "null";
                 LOGGER.info("Executed Step = VAR,String,var_CoverageLevels,null,TGTYPESCREENREG");
		var_CoverageLevels = getJsonData(var_CSVData , "$.records["+var_Count+"].CoverageLevels");
                 LOGGER.info("Executed Step = STORE,var_CoverageLevels,var_CSVData,$.records[{var_Count}].CoverageLevels,TGTYPESCREENREG");
		String var_PGFirstYear = "null";
                 LOGGER.info("Executed Step = VAR,String,var_PGFirstYear,null,TGTYPESCREENREG");
		var_PGFirstYear = getJsonData(var_CSVData , "$.records["+var_Count+"].PGFirstYear");
                 LOGGER.info("Executed Step = STORE,var_PGFirstYear,var_CSVData,$.records[{var_Count}].PGFirstYear,TGTYPESCREENREG");
		String var_PGRenewal = "null";
                 LOGGER.info("Executed Step = VAR,String,var_PGRenewal,null,TGTYPESCREENREG");
		var_PGRenewal = getJsonData(var_CSVData , "$.records["+var_Count+"].PGRenewal");
                 LOGGER.info("Executed Step = STORE,var_PGRenewal,var_CSVData,$.records[{var_Count}].PGRenewal,TGTYPESCREENREG");
		String var_PRFirstYear = "null";
                 LOGGER.info("Executed Step = VAR,String,var_PRFirstYear,null,TGTYPESCREENREG");
		var_PRFirstYear = getJsonData(var_CSVData , "$.records["+var_Count+"].PRFirstYear");
                 LOGGER.info("Executed Step = STORE,var_PRFirstYear,var_CSVData,$.records[{var_Count}].PRFirstYear,TGTYPESCREENREG");
		String var_PRRenewal = "null";
                 LOGGER.info("Executed Step = VAR,String,var_PRRenewal,null,TGTYPESCREENREG");
		var_PRRenewal = getJsonData(var_CSVData , "$.records["+var_Count+"].PRRenewal");
                 LOGGER.info("Executed Step = STORE,var_PRRenewal,var_CSVData,$.records[{var_Count}].PRRenewal,TGTYPESCREENREG");
		String var_ClaimFirstYear = "null";
                 LOGGER.info("Executed Step = VAR,String,var_ClaimFirstYear,null,TGTYPESCREENREG");
		var_ClaimFirstYear = getJsonData(var_CSVData , "$.records["+var_Count+"].ClaimFirstYear");
                 LOGGER.info("Executed Step = STORE,var_ClaimFirstYear,var_CSVData,$.records[{var_Count}].ClaimFirstYear,TGTYPESCREENREG");
		String var_ClaimRenewal = "null";
                 LOGGER.info("Executed Step = VAR,String,var_ClaimRenewal,null,TGTYPESCREENREG");
		var_ClaimRenewal = getJsonData(var_CSVData , "$.records["+var_Count+"].ClaimRenewal");
                 LOGGER.info("Executed Step = STORE,var_ClaimRenewal,var_CSVData,$.records[{var_Count}].ClaimRenewal,TGTYPESCREENREG");
		String var_OriginalEffectiveDate = "null";
                 LOGGER.info("Executed Step = VAR,String,var_OriginalEffectiveDate,null,TGTYPESCREENREG");
		var_OriginalEffectiveDate = getJsonData(var_CSVData , "$.records["+var_Count+"].OriginalEffectiveDate");
                 LOGGER.info("Executed Step = STORE,var_OriginalEffectiveDate,var_CSVData,$.records[{var_Count}].OriginalEffectiveDate,TGTYPESCREENREG");
		String var_dataToPass = "null";
                 LOGGER.info("Executed Step = VAR,String,var_dataToPass,null,TGTYPESCREENREG");
        CALL.$("LoginGIAS","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENNO");

        TYPE.$("ELE_TXTBOX_USERNAME","//input[@id='guestForm:username']","x5992","FALSE","TGTYPESCREENNO");

        TYPE.$("ELE_TXTBOX_PASSWORD","//input[@id='guestForm:password']","P@ssword1","FALSE","TGTYPESCREENNO");

        TAP.$("ELE_BUTTON_LOGIN","//span[@class='ui-button-text ui-c']","TGTYPESCREENNO");

        CALL.$("GroupProductsReg183","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_TASKSEARCH","//input[@id='workbenchForm:workbenchTabs:taskSearchContainer:taskSearchSearch']","Group Products","FALSE","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SEARCH","//a[@id='workbenchForm:workbenchTabs:taskSearchContainer:tb_buttonSearch']//span[@class='fa fa-search fa-lg']","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        TAP.$("ELE_LINK_GROUPPRODUCTS","//span[contains(text(),'Group Products')]","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_ADD","//button[@id='workbenchForm:workbenchTabs:tb_buttonAdd']//span[@class='ui-button-text ui-c'][contains(text(),'Add')]","TGTYPESCREENREG");

        WAIT.$(1,"TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_GROUPNAME","//input[@id='workbenchForm:workbenchTabs:groupNumber']",var_GroupName,"FALSE","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        TAP.$("ELE_TXTBOX_PRODUCTIDENTIFIER","//input[@id='workbenchForm:workbenchTabs:productId']","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_PRODUCTIDENTIFIER","//input[@id='workbenchForm:workbenchTabs:productId']",var_ProductIdentifier,"FALSE","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_PRODUCTNAME","//input[@id='workbenchForm:workbenchTabs:productDescription']",var_ProductName,"FALSE","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_EFFECTIVEDATEGROUPPRODUCT","//input[@id='workbenchForm:workbenchTabs:effectiveDate_input']",var_EffectiveDate,"FALSE","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_ORIGINALEFFECTIVEDATE","//input[@id='workbenchForm:workbenchTabs:originalEffectiveDate_input']",var_OriginalEffectiveDate,"FALSE","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_INSURANCETYPE","//input[@id='workbenchForm:workbenchTabs:insuranceType']",var_InsuranceType,"FALSE","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

		var_dataToPass = getJsonData(var_CSVData , "$.records["+var_Count+"].LineofBusiness");
                 LOGGER.info("Executed Step = STORE,var_dataToPass,var_CSVData,$.records[{var_Count}].LineofBusiness,TGTYPESCREENREG");
        TAP.$("ELE_DRPDWN_LINEOFBUSINESS","//label[@id='workbenchForm:workbenchTabs:lineOfBusinessCode_label']","TGTYPESCREENREG");

        WAIT.$(1,"TGTYPESCREENREG");

		try { 
		 Thread.sleep(1000);
		WebElement selectValueOfDropDown= driver.findElement(By.xpath("//li[@data-label='"+ var_dataToPass +"']"));

		takeScreenshot();

				selectValueOfDropDown.click();
		// Add Screenshot will only work for Android platform

		takeScreenshot();

				 Thread.sleep(1000);
		 
		 ;} 
		 catch(Exception e) { PrintVal("Renderer Script Exception : "+e.toString()); } 
		 PrintVal("Executed Step = CALLRENDERERSCRIPT,SELECTVALUEFROMDROPDOWN"); 
        WAIT.$(2,"TGTYPESCREENREG");

		var_dataToPass = getJsonData(var_CSVData , "$.records["+var_Count+"].ProductLinkType");
                 LOGGER.info("Executed Step = STORE,var_dataToPass,var_CSVData,$.records[{var_Count}].ProductLinkType,TGTYPESCREENREG");
        TAP.$("ELE_DRPDWN_PRODUCTLINKTYPE","//label[@id='workbenchForm:workbenchTabs:productLinkIndicator_label']","TGTYPESCREENREG");

        WAIT.$(1,"TGTYPESCREENREG");

		try { 
		 Thread.sleep(1000);
		WebElement selectValueOfDropDown= driver.findElement(By.xpath("//li[@data-label='"+ var_dataToPass +"']"));

		takeScreenshot();

				selectValueOfDropDown.click();
		// Add Screenshot will only work for Android platform

		takeScreenshot();

				 Thread.sleep(1000);
		 
		 ;} 
		 catch(Exception e) { PrintVal("Renderer Script Exception : "+e.toString()); } 
		 PrintVal("Executed Step = CALLRENDERERSCRIPT,SELECTVALUEFROMDROPDOWN"); 
        WAIT.$(2,"TGTYPESCREENREG");

        TAP.$("ELE_RDOBTN_BENEFITPLANREQUIRED_YES","//table[@id='workbenchForm:workbenchTabs:planCodeRatedIndicator']//span[@class='ui-radiobutton-icon ui-icon ui-icon-blank ui-c']","TGTYPESCREENREG");

        WAIT.$(1,"TGTYPESCREENREG");

		var_dataToPass = getJsonData(var_CSVData , "$.records["+var_Count+"].CoverageLevels");
                 LOGGER.info("Executed Step = STORE,var_dataToPass,var_CSVData,$.records[{var_Count}].CoverageLevels,TGTYPESCREENREG");
        TAP.$("ELE_DRPDWN_COVERAGELEVELSAVAILABLE","//label[@id='workbenchForm:workbenchTabs:rateLevel_label']","TGTYPESCREENREG");

        WAIT.$(1,"TGTYPESCREENREG");

		try { 
		 Thread.sleep(1000);
		WebElement selectValueOfDropDown= driver.findElement(By.xpath("//li[@data-label='"+ var_dataToPass +"']"));

		takeScreenshot();

				selectValueOfDropDown.click();
		// Add Screenshot will only work for Android platform

		takeScreenshot();

				 Thread.sleep(1000);
		 
		 ;} 
		 catch(Exception e) { PrintVal("Renderer Script Exception : "+e.toString()); } 
		 PrintVal("Executed Step = CALLRENDERERSCRIPT,SELECTVALUEFROMDROPDOWN"); 
        WAIT.$(3,"TGTYPESCREENREG");

        SWIPE.$("UP","TGTYPESCREENREG");

		var_dataToPass = getJsonData(var_CSVData , "$.records["+var_Count+"].PGFirstYear");
                 LOGGER.info("Executed Step = STORE,var_dataToPass,var_CSVData,$.records[{var_Count}].PGFirstYear,TGTYPESCREENREG");
        TAP.$("ELE_DRPDWN_FIRSTYEAR","//label[@id='workbenchForm:workbenchTabs:glNumberFirstYear_label']","TGTYPESCREENREG");

        WAIT.$(1,"TGTYPESCREENREG");

		try { 
		 WebElement dropdown = driver.findElement(By.xpath("//ul[@id='workbenchForm:workbenchTabs:glNumberFirstYear_items']")); 

		 


		       //ul[@id='workbenchForm:workbenchTabs:distributionChannel_items']
		       // dropdown.click();
		       
		        List<WebElement> options = dropdown.findElements(By.tagName("li"));
		        
		        System.out.println("Size"+options.size());
		        for (WebElement option : options)
		        {
		            System.out.print("Values of option"+option);
		            if (option.getText().equals(var_dataToPass))
		            {
		                option.click(); // click the desired option
		                break;
		            }
		        }
		 
		 ;} 
		 catch(Exception e) { PrintVal("Renderer Script Exception : "+e.toString()); } 
		 PrintVal("Executed Step = CALLRENDERERSCRIPT,SELECTPGFIRSTYEAR"); 
        WAIT.$(2,"TGTYPESCREENREG");

		var_dataToPass = getJsonData(var_CSVData , "$.records["+var_Count+"].PGRenewal");
                 LOGGER.info("Executed Step = STORE,var_dataToPass,var_CSVData,$.records[{var_Count}].PGRenewal,TGTYPESCREENREG");
        TAP.$("ELE_DRPDWN_RENEWAL","//label[@id='workbenchForm:workbenchTabs:glNumberRenewal_label']","TGTYPESCREENREG");

        WAIT.$(1,"TGTYPESCREENREG");

		try { 
		 WebElement dropdown = driver.findElement(By.xpath("//ul[@id='workbenchForm:workbenchTabs:glNumberRenewal_items']")); 

		 


		       //ul[@id='workbenchForm:workbenchTabs:distributionChannel_items']
		       // dropdown.click();
		       
		        List<WebElement> options = dropdown.findElements(By.tagName("li"));
		        
		        System.out.println("Size"+options.size());
		        for (WebElement option : options)
		        {
		            System.out.print("Values of option"+option);
		            if (option.getText().equals(var_dataToPass))
		            {
		                option.click(); // click the desired option
		                break;
		            }
		        }
		 
		 ;} 
		 catch(Exception e) { PrintVal("Renderer Script Exception : "+e.toString()); } 
		 PrintVal("Executed Step = CALLRENDERERSCRIPT,SELECTPGRENEWAL"); 
        WAIT.$(3,"TGTYPESCREENREG");

		var_dataToPass = getJsonData(var_CSVData , "$.records["+var_Count+"].PRFirstYear");
                 LOGGER.info("Executed Step = STORE,var_dataToPass,var_CSVData,$.records[{var_Count}].PRFirstYear,TGTYPESCREENREG");
        TAP.$("ELE_DRPDWN_FIRSTYEARGL","//label[@id='workbenchForm:workbenchTabs:premReceiveFirstYearGL_label']","TGTYPESCREENREG");

        WAIT.$(1,"TGTYPESCREENREG");

		try { 
		 WebElement dropdown = driver.findElement(By.xpath("//ul[@id='workbenchForm:workbenchTabs:premReceiveFirstYearGL_items']")); 

		 


		       //ul[@id='workbenchForm:workbenchTabs:distributionChannel_items']
		       // dropdown.click();
		       
		        List<WebElement> options = dropdown.findElements(By.tagName("li"));
		        
		        System.out.println("Size"+options.size());
		        for (WebElement option : options)
		        {
		            System.out.print("Values of option"+option);
		            if (option.getText().equals(var_dataToPass))
		            {
		                option.click(); // click the desired option
		                break;
		            }
		        }
		 
		 ;} 
		 catch(Exception e) { PrintVal("Renderer Script Exception : "+e.toString()); } 
		 PrintVal("Executed Step = CALLRENDERERSCRIPT,SELECTPRFIRSTYEAR"); 
        WAIT.$(3,"TGTYPESCREENREG");

		var_dataToPass = getJsonData(var_CSVData , "$.records["+var_Count+"].PRRenewal");
                 LOGGER.info("Executed Step = STORE,var_dataToPass,var_CSVData,$.records[{var_Count}].PRRenewal,TGTYPESCREENREG");
        TAP.$("ELE_DRPDWN_RENEWALGL","//label[@id='workbenchForm:workbenchTabs:premReceiveRenewYearGl_label']","TGTYPESCREENREG");

        WAIT.$(1,"TGTYPESCREENREG");

		try { 
		 WebElement dropdown = driver.findElement(By.xpath("//ul[@id='workbenchForm:workbenchTabs:premReceiveRenewYearGl_items']")); 

		 


		       //ul[@id='workbenchForm:workbenchTabs:distributionChannel_items']
		       // dropdown.click();
		       
		        List<WebElement> options = dropdown.findElements(By.tagName("li"));
		        
		        System.out.println("Size"+options.size());
		        for (WebElement option : options)
		        {
		            System.out.print("Values of option"+option);
		            if (option.getText().equals(var_dataToPass))
		            {
		                option.click(); // click the desired option
		                break;
		            }
		        }
		 
		 ;} 
		 catch(Exception e) { PrintVal("Renderer Script Exception : "+e.toString()); } 
		 PrintVal("Executed Step = CALLRENDERERSCRIPT,SELECTPRRENEWAL"); 
        WAIT.$(3,"TGTYPESCREENREG");

        TAP.$("ELE_RDOBTN_BYPASSCOMMISSIONS_YES","//table[@id='workbenchForm:workbenchTabs:bypassCommission']//span[@class='ui-radiobutton-icon ui-icon ui-icon-blank ui-c']","TGTYPESCREENREG");

        WAIT.$(10,"TGTYPESCREENREG");

        SWIPE.$("DOWN","TGTYPESCREENREG");

        SWIPE.$("DOWN","TGTYPESCREENREG");

        SWIPE.$("LEFT","TGTYPESCREENREG");

        SWIPE.$("LEFT","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

		var_dataToPass = getJsonData(var_CSVData , "$.records["+var_Count+"].ClaimFirstYear");
                 LOGGER.info("Executed Step = STORE,var_dataToPass,var_CSVData,$.records[{var_Count}].ClaimFirstYear,TGTYPESCREENREG");
        TAP.$("ELE_DRPDWN_CLAIMEXPENSEFIRSTYEAR","//label[@id='workbenchForm:workbenchTabs:claimExpenseFirstYearGL_label']","TGTYPESCREENREG");

        WAIT.$(1,"TGTYPESCREENREG");

		try { 
		 WebElement dropdown = driver.findElement(By.xpath("//ul[@id='workbenchForm:workbenchTabs:claimExpenseFirstYearGL_items']")); 

		 


		       //ul[@id='workbenchForm:workbenchTabs:distributionChannel_items']
		       // dropdown.click();
		       
		        List<WebElement> options = dropdown.findElements(By.tagName("li"));
		        
		        System.out.println("Size"+options.size());
		        for (WebElement option : options)
		        {
		            System.out.print("Values of option"+option);
		            if (option.getText().equals(var_dataToPass))
		            {
		                option.click(); // click the desired option
		                break;
		            }
		        }
		 
		 ;} 
		 catch(Exception e) { PrintVal("Renderer Script Exception : "+e.toString()); } 
		 PrintVal("Executed Step = CALLRENDERERSCRIPT,SELECTCLAIMEXPENSEFIRSTYEAR"); 
        WAIT.$(3,"TGTYPESCREENREG");

		var_dataToPass = getJsonData(var_CSVData , "$.records["+var_Count+"].ClaimRenewal");
                 LOGGER.info("Executed Step = STORE,var_dataToPass,var_CSVData,$.records[{var_Count}].ClaimRenewal,TGTYPESCREENREG");
        TAP.$("ELE_DRPDWN_CLAIMEXPENSERENEWAL","//label[@id='workbenchForm:workbenchTabs:claimExpenseRenewalGL_label']","TGTYPESCREENREG");

        WAIT.$(1,"TGTYPESCREENREG");

		try { 
		 WebElement dropdown = driver.findElement(By.xpath("//ul[@id='workbenchForm:workbenchTabs:claimExpenseRenewalGL_items']")); 

		 


		       //ul[@id='workbenchForm:workbenchTabs:distributionChannel_items']
		       // dropdown.click();
		       
		        List<WebElement> options = dropdown.findElements(By.tagName("li"));
		        
		        System.out.println("Size"+options.size());
		        for (WebElement option : options)
		        {
		            System.out.print("Values of option"+option);
		            if (option.getText().equals(var_dataToPass))
		            {
		                option.click(); // click the desired option
		                break;
		            }
		        }
		 
		 ;} 
		 catch(Exception e) { PrintVal("Renderer Script Exception : "+e.toString()); } 
		 PrintVal("Executed Step = CALLRENDERERSCRIPT,SELECTCLAIMEXPENSERENEWAL"); 
        WAIT.$(3,"TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SAVE","//span[contains(text(),'Save')]","TGTYPESCREENREG");

        WAIT.$(3,"TGTYPESCREENREG");

        CALL.$("Logout","TGTYPESCREENREG");

        TAP.$("ELE_LINK_INITIALS","//span[@id='headerForm:avatarInitialsText']","TGTYPESCREENNO");

        WAIT.$(3,"TGTYPESCREENNO");

        TAP.$("ELE_LINK_LOGOUT","//span[contains(text(),'Log Out')]","TGTYPESCREENNO");

        WAIT.$(3,"TGTYPESCREENNO");

        TAP.$("ELE_LINK_GOTOLOGIN","//span[@class='ui-button-text ui-c']","TGTYPESCREENNO");

        WAIT.$(2,"TGTYPESCREENNO");

		var_Count = var_Count + ( 1 );
                 LOGGER.info("Executed Step = INCREASEVAR,var_Count,1,TGTYPESCREENREG");
    	}
          LOGGER.info("Executed Step = WEND,TGTYPESCREENREG");
    	resetAttemps();

    }


    @Test
    public void benefitclassification() {

        // No need to declare / print method name here, handled by TestNg listener
        // declare("benefitclassification");

		int var_Count = 0;
                 LOGGER.info("Executed Step = VAR,Integer,var_Count,0,TGTYPESCREENREG");
		com.jayway.jsonpath.ReadContext var_CSVData = getJsonData("C:/GIAS_Automation/GLCBaseRegression/InputData/BenefitClassification.csv");
                 LOGGER.info("Executed Step = VAR,Json,var_CSVData,C:/GIAS_Automation/GLCBaseRegression/InputData/BenefitClassification.csv,TGTYPESCREENREG");
		String var_GroupName = "null";
                 LOGGER.info("Executed Step = VAR,String,var_GroupName,null,TGTYPESCREENREG");
		var_GroupName = getJsonData(var_CSVData , "$.records["+var_Count+"].GroupName");
                 LOGGER.info("Executed Step = STORE,var_GroupName,var_CSVData,$.records[{var_Count}].GroupName,TGTYPESCREENREG");
		String var_Identifier = "null";
                 LOGGER.info("Executed Step = VAR,String,var_Identifier,null,TGTYPESCREENREG");
		var_Identifier = getJsonData(var_CSVData , "$.records["+var_Count+"].Identifier");
                 LOGGER.info("Executed Step = STORE,var_Identifier,var_CSVData,$.records[{var_Count}].Identifier,TGTYPESCREENREG");
		String var_Description = "null";
                 LOGGER.info("Executed Step = VAR,String,var_Description,null,TGTYPESCREENREG");
		var_Description = getJsonData(var_CSVData , "$.records["+var_Count+"].Description");
                 LOGGER.info("Executed Step = STORE,var_Description,var_CSVData,$.records[{var_Count}].Description,TGTYPESCREENREG");
        CALL.$("LoginGIAS","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENNO");

        TYPE.$("ELE_TXTBOX_USERNAME","//input[@id='guestForm:username']","x5992","FALSE","TGTYPESCREENNO");

        TYPE.$("ELE_TXTBOX_PASSWORD","//input[@id='guestForm:password']","P@ssword1","FALSE","TGTYPESCREENNO");

        TAP.$("ELE_BUTTON_LOGIN","//span[@class='ui-button-text ui-c']","TGTYPESCREENNO");

        CALL.$("BenefitClassificationReg196","TGTYPESCREENREG");

        WAIT.$(3,"TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_TASKSEARCH","//input[@id='workbenchForm:workbenchTabs:taskSearchContainer:taskSearchSearch']","Benefit Classification","FALSE","TGTYPESCREENREG");

        WAIT.$(1,"TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SEARCH","//a[@id='workbenchForm:workbenchTabs:taskSearchContainer:tb_buttonSearch']//span[@class='fa fa-search fa-lg']","TGTYPESCREENREG");

        WAIT.$("ELE_LINK_BENEFITCLASSIFICATION","//span[contains(text(),'Benefit Classifications')]","VISIBLE","TGTYPESCREENREG");

        TAP.$("ELE_LINK_BENEFITCLASSIFICATION","//span[contains(text(),'Benefit Classifications')]","TGTYPESCREENREG");

        WAIT.$("ELE_BUTTON_ADD","//button[@id='workbenchForm:workbenchTabs:tb_buttonAdd']//span[@class='ui-button-text ui-c'][contains(text(),'Add')]","VISIBLE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_ADD","//button[@id='workbenchForm:workbenchTabs:tb_buttonAdd']//span[@class='ui-button-text ui-c'][contains(text(),'Add')]","TGTYPESCREENREG");

        WAIT.$("ELE_TXTBOX_GROUPNAME","//input[@id='workbenchForm:workbenchTabs:groupNumber']","VISIBLE","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_GROUPNAME","//input[@id='workbenchForm:workbenchTabs:groupNumber']",var_GroupName,"FALSE","TGTYPESCREENREG");

        WAIT.$(1,"TGTYPESCREENREG");

        TAP.$("ELE_TXTBOX_PRODUCTIDENTIFIER","//input[@id='workbenchForm:workbenchTabs:productId']","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_IDENTIFIER","//input[@id='workbenchForm:workbenchTabs:productSetID']",var_Identifier,"FALSE","TGTYPESCREENREG");

        WAIT.$(1,"TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_DESCRIPTION","//input[@id='workbenchForm:workbenchTabs:productSetDescription']",var_Description,"FALSE","TGTYPESCREENREG");

        WAIT.$(1,"TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SAVE","//span[contains(text(),'Save')]","TGTYPESCREENREG");

        WAIT.$("ELE_MSG_ITEMADDED","//div[@id='workbenchForm:workbenchTabs:messages']//span[@class='ui-messages-info-summary'][contains(text(),'Item successfully added.')]","VISIBLE","TGTYPESCREENREG");

        ASSERT.$("ELE_MSG_ITEMADDED","//div[@id='workbenchForm:workbenchTabs:messages']//span[@class='ui-messages-info-summary'][contains(text(),'Item successfully added.')]","VISIBLE","TGTYPESCREENREG");


    }


    @Test
    public void employeraccount() {

        // No need to declare / print method name here, handled by TestNg listener
        // declare("employeraccount");

		int var_Count = 0;
                 LOGGER.info("Executed Step = VAR,Integer,var_Count,0,TGTYPESCREENREG");
		com.jayway.jsonpath.ReadContext var_CSVData = getJsonData("http://10.1.104.229/json/20200818/oBjCaJ.json");
                 LOGGER.info("Executed Step = VAR,Json,var_CSVData,http://10.1.104.229/json/20200818/oBjCaJ.json,TGTYPESCREENREG");
		String var_GroupName = "null";
                 LOGGER.info("Executed Step = VAR,String,var_GroupName,null,TGTYPESCREENREG");
		var_GroupName = getJsonData(var_CSVData , "$.records["+var_Count+"].GroupName");
                 LOGGER.info("Executed Step = STORE,var_GroupName,var_CSVData,$.records[{var_Count}].GroupName,TGTYPESCREENREG");
		String var_AccountNumber = "null";
                 LOGGER.info("Executed Step = VAR,String,var_AccountNumber,null,TGTYPESCREENREG");
		var_AccountNumber = getJsonData(var_CSVData , "$.records["+var_Count+"].AccountNumber");
                 LOGGER.info("Executed Step = STORE,var_AccountNumber,var_CSVData,$.records[{var_Count}].AccountNumber,TGTYPESCREENREG");
		String var_BusinessName = "null";
                 LOGGER.info("Executed Step = VAR,String,var_BusinessName,null,TGTYPESCREENREG");
		var_BusinessName = getJsonData(var_CSVData , "$.records["+var_Count+"].BusinessName");
                 LOGGER.info("Executed Step = STORE,var_BusinessName,var_CSVData,$.records[{var_Count}].BusinessName,TGTYPESCREENREG");
		String var_EffectiveDate = "null";
                 LOGGER.info("Executed Step = VAR,String,var_EffectiveDate,null,TGTYPESCREENREG");
		var_EffectiveDate = getJsonData(var_CSVData , "$.records["+var_Count+"].EffectiveDate");
                 LOGGER.info("Executed Step = STORE,var_EffectiveDate,var_CSVData,$.records[{var_Count}].EffectiveDate,TGTYPESCREENREG");
		String var_AccountType = "null";
                 LOGGER.info("Executed Step = VAR,String,var_AccountType,null,TGTYPESCREENREG");
		var_AccountType = getJsonData(var_CSVData , "$.records["+var_Count+"].AccountType");
                 LOGGER.info("Executed Step = STORE,var_AccountType,var_CSVData,$.records[{var_Count}].AccountType,TGTYPESCREENREG");
		String var_Jurisdiction = "null";
                 LOGGER.info("Executed Step = VAR,String,var_Jurisdiction,null,TGTYPESCREENREG");
		var_Jurisdiction = getJsonData(var_CSVData , "$.records["+var_Count+"].Jurisdiction");
                 LOGGER.info("Executed Step = STORE,var_Jurisdiction,var_CSVData,$.records[{var_Count}].Jurisdiction,TGTYPESCREENREG");
		String var_Language = "null";
                 LOGGER.info("Executed Step = VAR,String,var_Language,null,TGTYPESCREENREG");
		var_Language = getJsonData(var_CSVData , "$.records["+var_Count+"].Language");
                 LOGGER.info("Executed Step = STORE,var_Language,var_CSVData,$.records[{var_Count}].Language,TGTYPESCREENREG");
		String var_StatementStyle = "null";
                 LOGGER.info("Executed Step = VAR,String,var_StatementStyle,null,TGTYPESCREENREG");
		var_StatementStyle = getJsonData(var_CSVData , "$.records["+var_Count+"].StatementStyle");
                 LOGGER.info("Executed Step = STORE,var_StatementStyle,var_CSVData,$.records[{var_Count}].StatementStyle,TGTYPESCREENREG");
		String var_dataToPass;
                 LOGGER.info("Executed Step = VAR,String,var_dataToPass,TGTYPESCREENREG");
        CALL.$("LoginGIAS","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENNO");

        TYPE.$("ELE_TXTBOX_USERNAME","//input[@id='guestForm:username']","x5992","FALSE","TGTYPESCREENNO");

        TYPE.$("ELE_TXTBOX_PASSWORD","//input[@id='guestForm:password']","P@ssword1","FALSE","TGTYPESCREENNO");

        TAP.$("ELE_BUTTON_LOGIN","//span[@class='ui-button-text ui-c']","TGTYPESCREENNO");

        CALL.$("REG184","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_TASKSEARCH","//input[@id='workbenchForm:workbenchTabs:taskSearchContainer:taskSearchSearch']","Accounts","FALSE","TGTYPESCREENREG");

        WAIT.$(1,"TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SEARCH","//a[@id='workbenchForm:workbenchTabs:taskSearchContainer:tb_buttonSearch']//span[@class='fa fa-search fa-lg']","TGTYPESCREENREG");

        WAIT.$("ELE_LINK_ACCOUNTS","//button[@id='workbenchForm:workbenchTabs:taskSearchContainer:0:j_idt84']//span[@class='ui-button-text ui-c'][contains(text(),'Accounts')]","VISIBLE","TGTYPESCREENREG");

        TAP.$("ELE_LINK_ACCOUNTS","//button[@id='workbenchForm:workbenchTabs:taskSearchContainer:0:j_idt84']//span[@class='ui-button-text ui-c'][contains(text(),'Accounts')]","TGTYPESCREENREG");

        WAIT.$("ELE_BUTTON_ADD","//button[@id='workbenchForm:workbenchTabs:tb_buttonAdd']//span[@class='ui-button-text ui-c'][contains(text(),'Add')]","VISIBLE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_ADD","//button[@id='workbenchForm:workbenchTabs:tb_buttonAdd']//span[@class='ui-button-text ui-c'][contains(text(),'Add')]","TGTYPESCREENREG");

        WAIT.$(10,"TGTYPESCREENREG");

        TAP.$("ELE_LINK_SHOWLINKS","//span[@class='ui-button-icon-left ui-icon ui-c fa fa-ellipsis-v fa-2x']","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

        TAP.$("ELE_LINK_SELECTCLIENT","//a[@class='ui-menuitem-link ui-corner-all']","TGTYPESCREENREG");

        WAIT.$("ELE_TXTBOX_CLIENTNAME","//input[@id='workbenchForm:workbenchTabs:searchName']","VISIBLE","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_CLIENTNAME","//input[@id='workbenchForm:workbenchTabs:searchName']",var_GroupName,"FALSE","TGTYPESCREENREG");

        WAIT.$("ELE_BUTTON_SEARCHCLIENT","//span[contains(text(),'Search')]","VISIBLE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SEARCHCLIENT","//span[contains(text(),'Search')]","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        TAP.$("ELE_LINK_FIRSTRECORD","//div[@id='ContentPanel']//tr[1]//td[1]","TGTYPESCREENREG");

        WAIT.$("ELE_BUTTON_CHECKMARK","//button[@id='workbenchForm:workbenchTabs:button_select']//span[@class='ui-button-icon-left ui-icon ui-c fa fa-check']","VISIBLE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_CHECKMARK","//button[@id='workbenchForm:workbenchTabs:button_select']//span[@class='ui-button-icon-left ui-icon ui-c fa fa-check']","TGTYPESCREENREG");

        WAIT.$("ELE_BUTTON_CONTINUE","//span[contains(text(),'Continue')]","VISIBLE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_CONTINUE","//span[contains(text(),'Continue')]","TGTYPESCREENREG");

        WAIT.$("ELE_TXTBOX_GROUPNAME","//input[@id='workbenchForm:workbenchTabs:groupNumber']","VISIBLE","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_GROUPNAME","//input[@id='workbenchForm:workbenchTabs:groupNumber']",var_GroupName,"FALSE","TGTYPESCREENREG");

        TAP.$("ELE_TXTBOX_ACCOUNTNUMBER","//input[@id='workbenchForm:workbenchTabs:accountNumber_input']","TGTYPESCREENREG");

        WAIT.$(10,"TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_ACCOUNTNUMBER","//input[@id='workbenchForm:workbenchTabs:accountNumber_input']",var_AccountNumber,"FALSE","TGTYPESCREENREG");

        WAIT.$("ELE_TXTBOX_EFFECTIVEDATEGROUPPRODUCT","//input[@id='workbenchForm:workbenchTabs:effectiveDate_input']","VISIBLE","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_EFFECTIVEDATEGROUPPRODUCT","//input[@id='workbenchForm:workbenchTabs:effectiveDate_input']",var_EffectiveDate,"FALSE","TGTYPESCREENREG");

        WAIT.$("ELE_DRPDWN_ACCOUNTTYPE","//label[@id='workbenchForm:workbenchTabs:accountType_label']","VISIBLE","TGTYPESCREENREG");

		var_dataToPass = getJsonData(var_CSVData , "$.records["+var_Count+"].AccountType");
                 LOGGER.info("Executed Step = STORE,var_dataToPass,var_CSVData,$.records[{var_Count}].AccountType,TGTYPESCREENREG");
        TAP.$("ELE_DRPDWN_ACCOUNTTYPE","//label[@id='workbenchForm:workbenchTabs:accountType_label']","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

		try { 
		 WebElement dropdown = driver.findElement(By.xpath("//ul[@id='workbenchForm:workbenchTabs:accountType_items']")); 

		 


		       //ul[@id='workbenchForm:workbenchTabs:distributionChannel_items']
		       // dropdown.click();
		       
		        List<WebElement> options = dropdown.findElements(By.tagName("li"));
		        
		        System.out.println("Size"+options.size());
		        for (WebElement option : options)
		        {
		            System.out.print("Values of option"+option);
		            if (option.getText().equals(var_dataToPass))
		            {
		                option.click(); // click the desired option
		                break;
		            }
		        }
		 
		 ;} 
		 catch(Exception e) { PrintVal("Renderer Script Exception : "+e.toString()); } 
		 PrintVal("Executed Step = CALLRENDERERSCRIPT,SELECTACCOUNTTYPE"); 
        WAIT.$(4,"TGTYPESCREENREG");

		var_dataToPass = getJsonData(var_CSVData , "$.records["+var_Count+"].Jurisdiction");
                 LOGGER.info("Executed Step = STORE,var_dataToPass,var_CSVData,$.records[{var_Count}].Jurisdiction,TGTYPESCREENREG");
        TAP.$("ELE_DRPDWN_STATEANDCOUNTRY","//label[@id='workbenchForm:workbenchTabs:countryAndStateCode_label']","TGTYPESCREENREG");

        WAIT.$(1,"TGTYPESCREENREG");

		try { 
		 Thread.sleep(1000);
		WebElement selectValueOfDropDown= driver.findElement(By.xpath("//li[@data-label='"+ var_dataToPass +"']"));

		takeScreenshot();

				selectValueOfDropDown.click();
		// Add Screenshot will only work for Android platform

		takeScreenshot();

				 Thread.sleep(1000);
		 
		 ;} 
		 catch(Exception e) { PrintVal("Renderer Script Exception : "+e.toString()); } 
		 PrintVal("Executed Step = CALLRENDERERSCRIPT,SELECTVALUEFROMDROPDOWN"); 
        WAIT.$("ELE_DRPDWN_LANGUAGE","//label[@id='workbenchForm:workbenchTabs:languageIndicator_label']","VISIBLE","TGTYPESCREENREG");

		var_dataToPass = getJsonData(var_CSVData , "$.records["+var_Count+"].Language");
                 LOGGER.info("Executed Step = STORE,var_dataToPass,var_CSVData,$.records[{var_Count}].Language,TGTYPESCREENREG");
        TAP.$("ELE_DRPDWN_LANGUAGE","//label[@id='workbenchForm:workbenchTabs:languageIndicator_label']","TGTYPESCREENREG");

        WAIT.$(1,"TGTYPESCREENREG");

		try { 
		 WebElement dropdown = driver.findElement(By.xpath("//ul[@id='workbenchForm:workbenchTabs:languageIndicator_items']")); 

		 


		       //ul[@id='workbenchForm:workbenchTabs:distributionChannel_items']
		       // dropdown.click();
		       
		        List<WebElement> options = dropdown.findElements(By.tagName("li"));
		        
		        System.out.println("Size"+options.size());
		        for (WebElement option : options)
		        {
		            System.out.print("Values of option"+option);
		            if (option.getText().equals(var_dataToPass))
		            {
		                option.click(); // click the desired option
		                break;
		            }
		        }
		 
		 ;} 
		 catch(Exception e) { PrintVal("Renderer Script Exception : "+e.toString()); } 
		 PrintVal("Executed Step = CALLRENDERERSCRIPT,SELECTLANGUAGE"); 
        WAIT.$("ELE_TXTBOX_STATEMENTSTYLE","//input[@id='workbenchForm:workbenchTabs:billingCtlSortTableNumber_input']","VISIBLE","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_STATEMENTSTYLE","//input[@id='workbenchForm:workbenchTabs:billingCtlSortTableNumber_input']",var_StatementStyle,"FALSE","TGTYPESCREENREG");

        WAIT.$(3,"TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SAVE","//span[contains(text(),'Save')]","TGTYPESCREENREG");

        WAIT.$(10,"TGTYPESCREENREG");

		try { 
		 driver.switchTo().frame(0);
		        
		 driver.findElement(By.xpath("//span[contains(text(),'Yes')]")).click();

		takeFullScreenShot();
		 
		 ;} 
		 catch(Exception e) { PrintVal("Renderer Script Exception : "+e.toString()); } 
		 PrintVal("Executed Step = CALLRENDERERSCRIPT,CONFIRMACTIONYES"); 
        WAIT.$("ELE_MSG_ITEMADDED","//div[@id='workbenchForm:workbenchTabs:messages']//span[@class='ui-messages-info-summary'][contains(text(),'Item successfully added.')]","VISIBLE","TGTYPESCREENREG");

        ASSERT.$("ELE_MSG_ITEMADDED","//div[@id='workbenchForm:workbenchTabs:messages']//span[@class='ui-messages-info-summary'][contains(text(),'Item successfully added.')]","VISIBLE","TGTYPESCREENREG");

        WAIT.$(3,"TGTYPESCREENREG");


    }


    @Test
    public void benefitplan() {

        // No need to declare / print method name here, handled by TestNg listener
        // declare("benefitplan");

		int var_Count = 0;
                 LOGGER.info("Executed Step = VAR,Integer,var_Count,0,TGTYPESCREENREG");
		com.jayway.jsonpath.ReadContext var_CSVData = getJsonData("http://10.1.104.229/json/20200831/vnCeRF.json");
                 LOGGER.info("Executed Step = VAR,Json,var_CSVData,http://10.1.104.229/json/20200831/vnCeRF.json,TGTYPESCREENREG");
        LOGGER.info("Executed Step = REPEAT IF");
    	while (check(var_Count,"<",4,"TGTYPESCREENREG")) {
          LOGGER.info("Executed Step = WHILE,var_Count,<,4,TGTYPESCREENREG");
		String var_GroupName = "null";
                 LOGGER.info("Executed Step = VAR,String,var_GroupName,null,TGTYPESCREENREG");
		var_GroupName = getJsonData(var_CSVData , "$.records["+var_Count+"].GroupName");
                 LOGGER.info("Executed Step = STORE,var_GroupName,var_CSVData,$.records[{var_Count}].GroupName,TGTYPESCREENREG");
		String var_Product = "null";
                 LOGGER.info("Executed Step = VAR,String,var_Product,null,TGTYPESCREENREG");
		var_Product = getJsonData(var_CSVData , "$.records["+var_Count+"].Product");
                 LOGGER.info("Executed Step = STORE,var_Product,var_CSVData,$.records[{var_Count}].Product,TGTYPESCREENREG");
		String var_Benefit = "null";
                 LOGGER.info("Executed Step = VAR,String,var_Benefit,null,TGTYPESCREENREG");
		var_Benefit = getJsonData(var_CSVData , "$.records["+var_Count+"].Benefit");
                 LOGGER.info("Executed Step = STORE,var_Benefit,var_CSVData,$.records[{var_Count}].Benefit,TGTYPESCREENREG");
		String var_EffectiveDate = "null";
                 LOGGER.info("Executed Step = VAR,String,var_EffectiveDate,null,TGTYPESCREENREG");
		var_EffectiveDate = getJsonData(var_CSVData , "$.records["+var_Count+"].EffectiveDate");
                 LOGGER.info("Executed Step = STORE,var_EffectiveDate,var_CSVData,$.records[{var_Count}].EffectiveDate,TGTYPESCREENREG");
		String var_VolumeCalculation = "null";
                 LOGGER.info("Executed Step = VAR,String,var_VolumeCalculation,null,TGTYPESCREENREG");
		var_VolumeCalculation = getJsonData(var_CSVData , "$.records["+var_Count+"].VolumeCalculation");
                 LOGGER.info("Executed Step = STORE,var_VolumeCalculation,var_CSVData,$.records[{var_Count}].VolumeCalculation,TGTYPESCREENREG");
		String var_VolumeUnit = "null";
                 LOGGER.info("Executed Step = VAR,String,var_VolumeUnit,null,TGTYPESCREENREG");
		var_VolumeUnit = getJsonData(var_CSVData , "$.records["+var_Count+"].VolumeUnit");
                 LOGGER.info("Executed Step = STORE,var_VolumeUnit,var_CSVData,$.records[{var_Count}].VolumeUnit,TGTYPESCREENREG");
		String var_SalaryValue = "null";
                 LOGGER.info("Executed Step = VAR,String,var_SalaryValue,null,TGTYPESCREENREG");
		var_SalaryValue = getJsonData(var_CSVData , "$.records["+var_Count+"].SalaryValue");
                 LOGGER.info("Executed Step = STORE,var_SalaryValue,var_CSVData,$.records[{var_Count}].SalaryValue,TGTYPESCREENREG");
		String var_BenefitDescription = "null";
                 LOGGER.info("Executed Step = VAR,String,var_BenefitDescription,null,TGTYPESCREENREG");
		var_BenefitDescription = getJsonData(var_CSVData , "$.records["+var_Count+"].BenefitDescription");
                 LOGGER.info("Executed Step = STORE,var_BenefitDescription,var_CSVData,$.records[{var_Count}].BenefitDescription,TGTYPESCREENREG");
		String var_dataToPass;
                 LOGGER.info("Executed Step = VAR,String,var_dataToPass,TGTYPESCREENREG");
        CALL.$("LoginGIAS","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENNO");

        TYPE.$("ELE_TXTBOX_USERNAME","//input[@id='guestForm:username']","x5992","FALSE","TGTYPESCREENNO");

        TYPE.$("ELE_TXTBOX_PASSWORD","//input[@id='guestForm:password']","P@ssword1","FALSE","TGTYPESCREENNO");

        TAP.$("ELE_BUTTON_LOGIN","//span[@class='ui-button-text ui-c']","TGTYPESCREENNO");

        CALL.$("REG185","TGTYPESCREENREG");

        WAIT.$("ELE_TXTBOX_TASKSEARCH","//input[@id='workbenchForm:workbenchTabs:taskSearchContainer:taskSearchSearch']","VISIBLE","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_TASKSEARCH","//input[@id='workbenchForm:workbenchTabs:taskSearchContainer:taskSearchSearch']","Benefit Plan","FALSE","TGTYPESCREENREG");

        WAIT.$(1,"TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SEARCH","//a[@id='workbenchForm:workbenchTabs:taskSearchContainer:tb_buttonSearch']//span[@class='fa fa-search fa-lg']","TGTYPESCREENREG");

        WAIT.$("ELE_LINK_BENEFITPLANS","//span[contains(text(),'Benefit Plans')]","VISIBLE","TGTYPESCREENREG");

        TAP.$("ELE_LINK_BENEFITPLANS","//span[contains(text(),'Benefit Plans')]","TGTYPESCREENREG");

        WAIT.$("ELE_BUTTON_ADD","//button[@id='workbenchForm:workbenchTabs:tb_buttonAdd']//span[@class='ui-button-text ui-c'][contains(text(),'Add')]","VISIBLE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_ADD","//button[@id='workbenchForm:workbenchTabs:tb_buttonAdd']//span[@class='ui-button-text ui-c'][contains(text(),'Add')]","TGTYPESCREENREG");

        WAIT.$("ELE_TXTBOX_GROUPNAME","//input[@id='workbenchForm:workbenchTabs:groupNumber']","VISIBLE","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_GROUPNAME","//input[@id='workbenchForm:workbenchTabs:groupNumber']",var_GroupName,"FALSE","TGTYPESCREENREG");

        TAP.$("ELE_DRPDWN_PRODUCT","//label[@id='workbenchForm:workbenchTabs:productId_label']","TGTYPESCREENREG");

        WAIT.$(10,"TGTYPESCREENREG");

		var_dataToPass = getJsonData(var_CSVData , "$.records["+var_Count+"].Product");
                 LOGGER.info("Executed Step = STORE,var_dataToPass,var_CSVData,$.records[{var_Count}].Product,TGTYPESCREENREG");
        TAP.$("ELE_DRPDWN_PRODUCT","//label[@id='workbenchForm:workbenchTabs:productId_label']","TGTYPESCREENREG");

        WAIT.$(1,"TGTYPESCREENREG");

		try { 
		 WebElement dropdown = driver.findElement(By.xpath("//ul[@id='workbenchForm:workbenchTabs:productId_items']")); 

		 


		       //ul[@id='workbenchForm:workbenchTabs:distributionChannel_items']
		       // dropdown.click();
		       
		        List<WebElement> options = dropdown.findElements(By.tagName("li"));
		        
		        System.out.println("Size"+options.size());
		        for (WebElement option : options)
		        {
		            System.out.print("Values of option"+option);
		            if (option.getText().equals(var_dataToPass))
		            {
		                option.click(); // click the desired option
		                break;
		            }
		        }
		 
		 ;} 
		 catch(Exception e) { PrintVal("Renderer Script Exception : "+e.toString()); } 
		 PrintVal("Executed Step = CALLRENDERERSCRIPT,SELECTPRODUCT"); 
        WAIT.$("ELE_TXTBOX_BENEFITPLAN","//input[@id='workbenchForm:workbenchTabs:planCode']","VISIBLE","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_BENEFITPLAN","//input[@id='workbenchForm:workbenchTabs:planCode']",var_Benefit,"FALSE","TGTYPESCREENREG");

        WAIT.$("ELE_TXTBOX_BENEFITDESCRIPTION","//input[@id='workbenchForm:workbenchTabs:planDescription1']","VISIBLE","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_BENEFITDESCRIPTION","//input[@id='workbenchForm:workbenchTabs:planDescription1']",var_BenefitDescription,"FALSE","TGTYPESCREENREG");

        WAIT.$("ELE_TXTBOX_EFFECTIVEDATEGROUPPRODUCT","//input[@id='workbenchForm:workbenchTabs:effectiveDate_input']","VISIBLE","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_EFFECTIVEDATEGROUPPRODUCT","//input[@id='workbenchForm:workbenchTabs:effectiveDate_input']",var_EffectiveDate,"FALSE","TGTYPESCREENREG");

        WAIT.$(1,"TGTYPESCREENREG");

		var_dataToPass = getJsonData(var_CSVData , "$.records["+var_Count+"].VolumeCalculation");
                 LOGGER.info("Executed Step = STORE,var_dataToPass,var_CSVData,$.records[{var_Count}].VolumeCalculation,TGTYPESCREENREG");
        TAP.$("ELE_DRPDWN_VOLUMECALCULATIONMETHOD","//label[@id='workbenchForm:workbenchTabs:volumeType_label']","TGTYPESCREENREG");

        WAIT.$(1,"TGTYPESCREENREG");

		try { 
		 WebElement dropdown = driver.findElement(By.xpath("//ul[@id='workbenchForm:workbenchTabs:volumeType_items']")); 

		 


		       //ul[@id='workbenchForm:workbenchTabs:distributionChannel_items']
		       // dropdown.click();
		       
		        List<WebElement> options = dropdown.findElements(By.tagName("li"));
		        
		        System.out.println("Size"+options.size());
		        for (WebElement option : options)
		        {
		            System.out.print("Values of option"+option);
		            if (option.getText().equals(var_dataToPass))
		            {
		                option.click(); // click the desired option
		                break;
		            }
		        }
		 
		 ;} 
		 catch(Exception e) { PrintVal("Renderer Script Exception : "+e.toString()); } 
		 PrintVal("Executed Step = CALLRENDERERSCRIPT,SELECTVOLUMETYPE"); 
        WAIT.$("ELE_TXTBOX_VOLUMEOFEACHUNIT","//input[@id='workbenchForm:workbenchTabs:unitValue_input']","VISIBLE","TGTYPESCREENREG");

        LOGGER.info("Executed Step = START IF");
        if (check(var_VolumeUnit,"<>","null","TGTYPESCREENREG")) {
         LOGGER.info("Executed Step = IF,var_VolumeUnit,<>,null,TGTYPESCREENREG");
        TYPE.$("ELE_TXTBOX_VOLUMEOFEACHUNIT","//input[@id='workbenchForm:workbenchTabs:unitValue_input']",var_VolumeUnit,"FALSE","TGTYPESCREENREG");

        }
          LOGGER.info("Executed Step = ENDIF,TGTYPESCREENREG");
        WAIT.$("ELE_DRPDWN_SALARYVALUEFORCALCULATION","//label[@id='workbenchForm:workbenchTabs:basisMode_label']","VISIBLE","TGTYPESCREENREG");

        LOGGER.info("Executed Step = START IF");
        if (check(var_SalaryValue,"<>","null","TGTYPESCREENREG")) {
         LOGGER.info("Executed Step = IF,var_SalaryValue,<>,null,TGTYPESCREENREG");
		var_dataToPass = getJsonData(var_CSVData , "$.records["+var_Count+"].SalaryValue");
                 LOGGER.info("Executed Step = STORE,var_dataToPass,var_CSVData,$.records[{var_Count}].SalaryValue,TGTYPESCREENREG");
        TAP.$("ELE_DRPDWN_SALARYVALUEFORCALCULATION","//label[@id='workbenchForm:workbenchTabs:basisMode_label']","TGTYPESCREENREG");

        WAIT.$(1,"TGTYPESCREENREG");

		try { 
		 WebElement dropdown = driver.findElement(By.xpath("//ul[@id='workbenchForm:workbenchTabs:basisMode_items']")); 

		 


		       //ul[@id='workbenchForm:workbenchTabs:distributionChannel_items']
		       // dropdown.click();
		       
		        List<WebElement> options = dropdown.findElements(By.tagName("li"));
		        
		        System.out.println("Size"+options.size());
		        for (WebElement option : options)
		        {
		            System.out.print("Values of option"+option);
		            if (option.getText().equals(var_dataToPass))
		            {
		                option.click(); // click the desired option
		                break;
		            }
		        }
		 
		 ;} 
		 catch(Exception e) { PrintVal("Renderer Script Exception : "+e.toString()); } 
		 PrintVal("Executed Step = CALLRENDERERSCRIPT,SELECTSALARYVALUE"); 
        }
          LOGGER.info("Executed Step = ENDIF,TGTYPESCREENREG");
        WAIT.$("ELE_BUTTON_SAVE","//span[contains(text(),'Save')]","VISIBLE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SAVE","//span[contains(text(),'Save')]","TGTYPESCREENREG");

        WAIT.$("ELE_MSG_ITEMADDED","//div[@id='workbenchForm:workbenchTabs:messages']//span[@class='ui-messages-info-summary'][contains(text(),'Item successfully added.')]","VISIBLE","TGTYPESCREENREG");

        ASSERT.$("ELE_MSG_ITEMADDED","//div[@id='workbenchForm:workbenchTabs:messages']//span[@class='ui-messages-info-summary'][contains(text(),'Item successfully added.')]","VISIBLE","TGTYPESCREENREG");

        CALL.$("Logout","TGTYPESCREENREG");

        TAP.$("ELE_LINK_INITIALS","//span[@id='headerForm:avatarInitialsText']","TGTYPESCREENNO");

        WAIT.$(3,"TGTYPESCREENNO");

        TAP.$("ELE_LINK_LOGOUT","//span[contains(text(),'Log Out')]","TGTYPESCREENNO");

        WAIT.$(3,"TGTYPESCREENNO");

        TAP.$("ELE_LINK_GOTOLOGIN","//span[@class='ui-button-text ui-c']","TGTYPESCREENNO");

        WAIT.$(2,"TGTYPESCREENNO");

		var_Count = var_Count + ( 1 );
                 LOGGER.info("Executed Step = INCREASEVAR,var_Count,1,TGTYPESCREENREG");
    	}
          LOGGER.info("Executed Step = WEND,TGTYPESCREENREG");
    	resetAttemps();

    }


    @Test
    public void individualaccount() {

        // No need to declare / print method name here, handled by TestNg listener
        // declare("individualaccount");

		int var_Count = 0;
                 LOGGER.info("Executed Step = VAR,Integer,var_Count,0,TGTYPESCREENREG");
		com.jayway.jsonpath.ReadContext var_CSVData = getJsonData("http://10.1.104.229/json/20200831/OdI6wG.json");
                 LOGGER.info("Executed Step = VAR,Json,var_CSVData,http://10.1.104.229/json/20200831/OdI6wG.json,TGTYPESCREENREG");
		String var_GroupName = "null";
                 LOGGER.info("Executed Step = VAR,String,var_GroupName,null,TGTYPESCREENREG");
		var_GroupName = getJsonData(var_CSVData , "$.records["+var_Count+"].GroupName");
                 LOGGER.info("Executed Step = STORE,var_GroupName,var_CSVData,$.records[{var_Count}].GroupName,TGTYPESCREENREG");
		String var_AccountNumber = "null";
                 LOGGER.info("Executed Step = VAR,String,var_AccountNumber,null,TGTYPESCREENREG");
		var_AccountNumber = getJsonData(var_CSVData , "$.records["+var_Count+"].AccountNumber");
                 LOGGER.info("Executed Step = STORE,var_AccountNumber,var_CSVData,$.records[{var_Count}].AccountNumber,TGTYPESCREENREG");
		String var_EffectiveDate = "null";
                 LOGGER.info("Executed Step = VAR,String,var_EffectiveDate,null,TGTYPESCREENREG");
		var_EffectiveDate = getJsonData(var_CSVData , "$.records["+var_Count+"].EffectiveDate");
                 LOGGER.info("Executed Step = STORE,var_EffectiveDate,var_CSVData,$.records[{var_Count}].EffectiveDate,TGTYPESCREENREG");
		String var_AccountType = "null";
                 LOGGER.info("Executed Step = VAR,String,var_AccountType,null,TGTYPESCREENREG");
		var_AccountType = getJsonData(var_CSVData , "$.records["+var_Count+"].AccountType");
                 LOGGER.info("Executed Step = STORE,var_AccountType,var_CSVData,$.records[{var_Count}].AccountType,TGTYPESCREENREG");
		String var_State = "null";
                 LOGGER.info("Executed Step = VAR,String,var_State,null,TGTYPESCREENREG");
		var_State = getJsonData(var_CSVData , "$.records["+var_Count+"].State");
                 LOGGER.info("Executed Step = STORE,var_State,var_CSVData,$.records[{var_Count}].State,TGTYPESCREENREG");
		String var_Language = "null";
                 LOGGER.info("Executed Step = VAR,String,var_Language,null,TGTYPESCREENREG");
		var_Language = getJsonData(var_CSVData , "$.records["+var_Count+"].Language");
                 LOGGER.info("Executed Step = STORE,var_Language,var_CSVData,$.records[{var_Count}].Language,TGTYPESCREENREG");
		String var_StatementStyle = "null";
                 LOGGER.info("Executed Step = VAR,String,var_StatementStyle,null,TGTYPESCREENREG");
		var_StatementStyle = getJsonData(var_CSVData , "$.records["+var_Count+"].StatementStyle");
                 LOGGER.info("Executed Step = STORE,var_StatementStyle,var_CSVData,$.records[{var_Count}].StatementStyle,TGTYPESCREENREG");
		String var_BusinessName = "null";
                 LOGGER.info("Executed Step = VAR,String,var_BusinessName,null,TGTYPESCREENREG");
		var_BusinessName = getJsonData(var_CSVData , "$.records["+var_Count+"].BusinessName");
                 LOGGER.info("Executed Step = STORE,var_BusinessName,var_CSVData,$.records[{var_Count}].BusinessName,TGTYPESCREENREG");
		String var_dataToPass = "null";
                 LOGGER.info("Executed Step = VAR,String,var_dataToPass,null,TGTYPESCREENREG");
        CALL.$("LoginGIAS","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENNO");

        TYPE.$("ELE_TXTBOX_USERNAME","//input[@id='guestForm:username']","x5992","FALSE","TGTYPESCREENNO");

        TYPE.$("ELE_TXTBOX_PASSWORD","//input[@id='guestForm:password']","P@ssword1","FALSE","TGTYPESCREENNO");

        TAP.$("ELE_BUTTON_LOGIN","//span[@class='ui-button-text ui-c']","TGTYPESCREENNO");

        CALL.$("REG193","TGTYPESCREENREG");

        WAIT.$(3,"TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_TASKSEARCH","//input[@id='workbenchForm:workbenchTabs:taskSearchContainer:taskSearchSearch']","Accounts","FALSE","TGTYPESCREENREG");

        WAIT.$(1,"TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SEARCH","//a[@id='workbenchForm:workbenchTabs:taskSearchContainer:tb_buttonSearch']//span[@class='fa fa-search fa-lg']","TGTYPESCREENREG");

        WAIT.$("ELE_LINK_ACCOUNTS","//button[@id='workbenchForm:workbenchTabs:taskSearchContainer:0:j_idt84']//span[@class='ui-button-text ui-c'][contains(text(),'Accounts')]","VISIBLE","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

        TAP.$("ELE_LINK_ACCOUNTS","//button[@id='workbenchForm:workbenchTabs:taskSearchContainer:0:j_idt84']//span[@class='ui-button-text ui-c'][contains(text(),'Accounts')]","TGTYPESCREENREG");

        WAIT.$("ELE_BUTTON_ADD","//button[@id='workbenchForm:workbenchTabs:tb_buttonAdd']//span[@class='ui-button-text ui-c'][contains(text(),'Add')]","VISIBLE","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_ADD","//button[@id='workbenchForm:workbenchTabs:tb_buttonAdd']//span[@class='ui-button-text ui-c'][contains(text(),'Add')]","TGTYPESCREENREG");

        WAIT.$("ELE_TXTBOX_GROUPNAME","//input[@id='workbenchForm:workbenchTabs:groupNumber']","VISIBLE","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_GROUPNAME","//input[@id='workbenchForm:workbenchTabs:groupNumber']",var_GroupName,"FALSE","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

        TAP.$("ELE_LINK_SHOWLINKS","//span[@class='ui-button-icon-left ui-icon ui-c fa fa-ellipsis-v fa-2x']","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        TAP.$("ELE_LINK_SHOWLINKS","//span[@class='ui-button-icon-left ui-icon ui-c fa fa-ellipsis-v fa-2x']","TGTYPESCREENREG");

        WAIT.$("ELE_LINK_SELECTCLIENT","//a[@class='ui-menuitem-link ui-corner-all']","VISIBLE","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

        TAP.$("ELE_LINK_SELECTCLIENT","//a[@class='ui-menuitem-link ui-corner-all']","TGTYPESCREENREG");

        WAIT.$("ELE_TXTBOX_CLIENTNAME","//input[@id='workbenchForm:workbenchTabs:searchName']","VISIBLE","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_CLIENTNAME","//input[@id='workbenchForm:workbenchTabs:searchName']",var_GroupName,"FALSE","TGTYPESCREENREG");

        WAIT.$("ELE_BUTTON_SEARCHCLIENT","//span[contains(text(),'Search')]","VISIBLE","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SEARCHCLIENT","//span[contains(text(),'Search')]","TGTYPESCREENREG");

        WAIT.$("ELE_LINK_FIRSTRECORD","//div[@id='ContentPanel']//tr[1]//td[1]","VISIBLE","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

        TAP.$("ELE_LINK_FIRSTRECORD","//div[@id='ContentPanel']//tr[1]//td[1]","TGTYPESCREENREG");

        WAIT.$("ELE_BUTTON_CHECKMARK","//button[@id='workbenchForm:workbenchTabs:button_select']//span[@class='ui-button-icon-left ui-icon ui-c fa fa-check']","VISIBLE","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_CHECKMARK","//button[@id='workbenchForm:workbenchTabs:button_select']//span[@class='ui-button-icon-left ui-icon ui-c fa fa-check']","TGTYPESCREENREG");

        WAIT.$("ELE_BUTTON_CONTINUE","//span[contains(text(),'Continue')]","VISIBLE","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_CONTINUE","//span[contains(text(),'Continue')]","TGTYPESCREENREG");

        WAIT.$("ELE_TXTBOX_GROUPNAME","//input[@id='workbenchForm:workbenchTabs:groupNumber']","VISIBLE","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

        TAP.$("ELE_TXTBOX_ACCOUNTNUMBER","//input[@id='workbenchForm:workbenchTabs:accountNumber_input']","TGTYPESCREENREG");

        WAIT.$("ELE_TXTBOX_ACCOUNTNUMBER","//input[@id='workbenchForm:workbenchTabs:accountNumber_input']","VISIBLE","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_ACCOUNTNUMBER","//input[@id='workbenchForm:workbenchTabs:accountNumber_input']",var_AccountNumber,"FALSE","TGTYPESCREENREG");

        WAIT.$("ELE_TXTBOX_EFFECTIVEDATE","//input[@id='workbenchForm:workbenchTabs:groupEffectiveDate_input']","VISIBLE","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

        WAIT.$("ELE_TXTBOX_BUSINESSNAME","//input[@id='workbenchForm:workbenchTabs:prelistAccountName']","VISIBLE","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_BUSINESSNAME","//input[@id='workbenchForm:workbenchTabs:prelistAccountName']",var_BusinessName,"FALSE","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_EFFECTIVEDATEGROUPPRODUCT","//input[@id='workbenchForm:workbenchTabs:effectiveDate_input']",var_EffectiveDate,"FALSE","TGTYPESCREENREG");

        WAIT.$(3,"TGTYPESCREENREG");

		var_dataToPass = getJsonData(var_CSVData , "$.records["+var_Count+"].AccountType");
                 LOGGER.info("Executed Step = STORE,var_dataToPass,var_CSVData,$.records[{var_Count}].AccountType,TGTYPESCREENREG");
        TAP.$("ELE_DRPDWN_ACCOUNTTYPE","//label[@id='workbenchForm:workbenchTabs:accountType_label']","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

		try { 
		 WebElement dropdown = driver.findElement(By.xpath("//ul[@id='workbenchForm:workbenchTabs:accountType_items']")); 

		 


		       //ul[@id='workbenchForm:workbenchTabs:distributionChannel_items']
		       // dropdown.click();
		       
		        List<WebElement> options = dropdown.findElements(By.tagName("li"));
		        
		        System.out.println("Size"+options.size());
		        for (WebElement option : options)
		        {
		            System.out.print("Values of option"+option);
		            if (option.getText().equals(var_dataToPass))
		            {
		                option.click(); // click the desired option
		                break;
		            }
		        }
		 
		 ;} 
		 catch(Exception e) { PrintVal("Renderer Script Exception : "+e.toString()); } 
		 PrintVal("Executed Step = CALLRENDERERSCRIPT,SELECTACCOUNTTYPE"); 
        WAIT.$("ELE_DRPDWN_STATEANDCOUNTRY","//label[@id='workbenchForm:workbenchTabs:countryAndStateCode_label']","VISIBLE","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

		var_dataToPass = getJsonData(var_CSVData , "$.records["+var_Count+"].State");
                 LOGGER.info("Executed Step = STORE,var_dataToPass,var_CSVData,$.records[{var_Count}].State,TGTYPESCREENREG");
        TAP.$("ELE_DRPDWN_STATEANDCOUNTRY","//label[@id='workbenchForm:workbenchTabs:countryAndStateCode_label']","TGTYPESCREENREG");

        WAIT.$(1,"TGTYPESCREENREG");

		try { 
		 Thread.sleep(1000);
		WebElement selectValueOfDropDown= driver.findElement(By.xpath("//li[@data-label='"+ var_dataToPass +"']"));

		takeScreenshot();

				selectValueOfDropDown.click();
		// Add Screenshot will only work for Android platform

		takeScreenshot();

				 Thread.sleep(1000);
		 
		 ;} 
		 catch(Exception e) { PrintVal("Renderer Script Exception : "+e.toString()); } 
		 PrintVal("Executed Step = CALLRENDERERSCRIPT,SELECTVALUEFROMDROPDOWN"); 
        WAIT.$("ELE_DRPDWN_LANGUAGE","//label[@id='workbenchForm:workbenchTabs:languageIndicator_label']","VISIBLE","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

		var_dataToPass = getJsonData(var_CSVData , "$.records["+var_Count+"].Language");
                 LOGGER.info("Executed Step = STORE,var_dataToPass,var_CSVData,$.records[{var_Count}].Language,TGTYPESCREENREG");
        TAP.$("ELE_DRPDWN_LANGUAGE","//label[@id='workbenchForm:workbenchTabs:languageIndicator_label']","TGTYPESCREENREG");

        WAIT.$(1,"TGTYPESCREENREG");

		try { 
		 WebElement dropdown = driver.findElement(By.xpath("//ul[@id='workbenchForm:workbenchTabs:languageIndicator_items']")); 

		 


		       //ul[@id='workbenchForm:workbenchTabs:distributionChannel_items']
		       // dropdown.click();
		       
		        List<WebElement> options = dropdown.findElements(By.tagName("li"));
		        
		        System.out.println("Size"+options.size());
		        for (WebElement option : options)
		        {
		            System.out.print("Values of option"+option);
		            if (option.getText().equals(var_dataToPass))
		            {
		                option.click(); // click the desired option
		                break;
		            }
		        }
		 
		 ;} 
		 catch(Exception e) { PrintVal("Renderer Script Exception : "+e.toString()); } 
		 PrintVal("Executed Step = CALLRENDERERSCRIPT,SELECTLANGUAGE"); 
        WAIT.$("ELE_TXTBOX_STATEMENTSTYLE","//input[@id='workbenchForm:workbenchTabs:billingCtlSortTableNumber_input']","VISIBLE","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_STATEMENTSTYLE","//input[@id='workbenchForm:workbenchTabs:billingCtlSortTableNumber_input']",var_StatementStyle,"FALSE","TGTYPESCREENREG");

        WAIT.$("ELE_BUTTON_SAVE","//span[contains(text(),'Save')]","VISIBLE","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SAVE","//span[contains(text(),'Save')]","TGTYPESCREENREG");

        SWIPE.$("DOWN","TGTYPESCREENREG");

        SWIPE.$("DOWN","TGTYPESCREENREG");

        WAIT.$("ELE_MSG_ITEMADDED","//div[@id='workbenchForm:workbenchTabs:messages']//span[@class='ui-messages-info-summary'][contains(text(),'Item successfully added.')]","VISIBLE","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

        ASSERT.$("ELE_MSG_ITEMADDED","//div[@id='workbenchForm:workbenchTabs:messages']//span[@class='ui-messages-info-summary'][contains(text(),'Item successfully added.')]","VISIBLE","TGTYPESCREENREG");


    }


    @Test
    public void benefitplantables() {

        // No need to declare / print method name here, handled by TestNg listener
        // declare("benefitplantables");

		int var_Count = 0;
                 LOGGER.info("Executed Step = VAR,Integer,var_Count,0,TGTYPESCREENREG");
		com.jayway.jsonpath.ReadContext var_CSVData = getJsonData("http://10.1.104.229/json/20200831/rzmR4d.json");
                 LOGGER.info("Executed Step = VAR,Json,var_CSVData,http://10.1.104.229/json/20200831/rzmR4d.json,TGTYPESCREENREG");
        LOGGER.info("Executed Step = REPEAT IF");
    	while (check(var_Count,"<",4,"TGTYPESCREENREG")) {
          LOGGER.info("Executed Step = WHILE,var_Count,<,4,TGTYPESCREENREG");
		String var_GroupName = "null";
                 LOGGER.info("Executed Step = VAR,String,var_GroupName,null,TGTYPESCREENREG");
		var_GroupName = getJsonData(var_CSVData , "$.records["+var_Count+"].GroupName");
                 LOGGER.info("Executed Step = STORE,var_GroupName,var_CSVData,$.records[{var_Count}].GroupName,TGTYPESCREENREG");
		String var_Product = "null";
                 LOGGER.info("Executed Step = VAR,String,var_Product,null,TGTYPESCREENREG");
		var_Product = getJsonData(var_CSVData , "$.records["+var_Count+"].Product");
                 LOGGER.info("Executed Step = STORE,var_Product,var_CSVData,$.records[{var_Count}].Product,TGTYPESCREENREG");
		String var_Plan = "null";
                 LOGGER.info("Executed Step = VAR,String,var_Plan,null,TGTYPESCREENREG");
		var_Plan = getJsonData(var_CSVData , "$.records["+var_Count+"].Plan");
                 LOGGER.info("Executed Step = STORE,var_Plan,var_CSVData,$.records[{var_Count}].Plan,TGTYPESCREENREG");
		String var_Table = "null";
                 LOGGER.info("Executed Step = VAR,String,var_Table,null,TGTYPESCREENREG");
		var_Table = getJsonData(var_CSVData , "$.records["+var_Count+"].Table");
                 LOGGER.info("Executed Step = STORE,var_Table,var_CSVData,$.records[{var_Count}].Table,TGTYPESCREENREG");
		String var_dataToPass;
                 LOGGER.info("Executed Step = VAR,String,var_dataToPass,TGTYPESCREENREG");
        CALL.$("LoginGIAS","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENNO");

        TYPE.$("ELE_TXTBOX_USERNAME","//input[@id='guestForm:username']","x5992","FALSE","TGTYPESCREENNO");

        TYPE.$("ELE_TXTBOX_PASSWORD","//input[@id='guestForm:password']","P@ssword1","FALSE","TGTYPESCREENNO");

        TAP.$("ELE_BUTTON_LOGIN","//span[@class='ui-button-text ui-c']","TGTYPESCREENNO");

        CALL.$("REG197","TGTYPESCREENREG");

        WAIT.$("ELE_TXTBOX_TASKSEARCH","//input[@id='workbenchForm:workbenchTabs:taskSearchContainer:taskSearchSearch']","VISIBLE","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_TASKSEARCH","//input[@id='workbenchForm:workbenchTabs:taskSearchContainer:taskSearchSearch']","Benefit Plan Table","FALSE","TGTYPESCREENREG");

        WAIT.$(1,"TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SEARCH","//a[@id='workbenchForm:workbenchTabs:taskSearchContainer:tb_buttonSearch']//span[@class='fa fa-search fa-lg']","TGTYPESCREENREG");

        WAIT.$("ELE_LINK_BENEFITPLANTABLE","//span[contains(text(),'Benefit Plan Tables')]","VISIBLE","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

        TAP.$("ELE_LINK_BENEFITPLANTABLE","//span[contains(text(),'Benefit Plan Tables')]","TGTYPESCREENREG");

        WAIT.$("ELE_BUTTON_ADD","//button[@id='workbenchForm:workbenchTabs:tb_buttonAdd']//span[@class='ui-button-text ui-c'][contains(text(),'Add')]","VISIBLE","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_ADD","//button[@id='workbenchForm:workbenchTabs:tb_buttonAdd']//span[@class='ui-button-text ui-c'][contains(text(),'Add')]","TGTYPESCREENREG");

        WAIT.$("ELE_TXTBOX_GROUPNAME","//input[@id='workbenchForm:workbenchTabs:groupNumber']","VISIBLE","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_GROUPNAME","//input[@id='workbenchForm:workbenchTabs:groupNumber']",var_GroupName,"FALSE","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

        TAP.$("ELE_TXTBOX_TABLE","//input[@id='workbenchForm:workbenchTabs:planCodeTableNumber_input']","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_TABLE","//input[@id='workbenchForm:workbenchTabs:planCodeTableNumber_input']",var_Table,"FALSE","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

		var_dataToPass = getJsonData(var_CSVData , "$.records["+var_Count+"].Product");
                 LOGGER.info("Executed Step = STORE,var_dataToPass,var_CSVData,$.records[{var_Count}].Product,TGTYPESCREENREG");
        TAP.$("ELE_DRPDWN_PRODUCT","//label[@id='workbenchForm:workbenchTabs:productId_label']","TGTYPESCREENREG");

        WAIT.$(1,"TGTYPESCREENREG");

		try { 
		 WebElement dropdown = driver.findElement(By.xpath("//ul[@id='workbenchForm:workbenchTabs:productId_items']")); 

		 


		       //ul[@id='workbenchForm:workbenchTabs:distributionChannel_items']
		       // dropdown.click();
		       
		        List<WebElement> options = dropdown.findElements(By.tagName("li"));
		        
		        System.out.println("Size"+options.size());
		        for (WebElement option : options)
		        {
		            System.out.print("Values of option"+option);
		            if (option.getText().equals(var_dataToPass))
		            {
		                option.click(); // click the desired option
		                break;
		            }
		        }
		 
		 ;} 
		 catch(Exception e) { PrintVal("Renderer Script Exception : "+e.toString()); } 
		 PrintVal("Executed Step = CALLRENDERERSCRIPT,SELECTPRODUCTREG197"); 
        WAIT.$(2,"TGTYPESCREENREG");

        WAIT.$("ELE_DRPDWN_PLAN","//label[@id='workbenchForm:workbenchTabs:planCode_label']","VISIBLE","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

		var_dataToPass = getJsonData(var_CSVData , "$.records["+var_Count+"].Plan");
                 LOGGER.info("Executed Step = STORE,var_dataToPass,var_CSVData,$.records[{var_Count}].Plan,TGTYPESCREENREG");
        TAP.$("ELE_DRPDWN_PLAN","//label[@id='workbenchForm:workbenchTabs:planCode_label']","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

		try { 
		 WebElement dropdown = driver.findElement(By.xpath("//ul[@id='workbenchForm:workbenchTabs:planCode_items']")); 

		 


		       //ul[@id='workbenchForm:workbenchTabs:distributionChannel_items']
		       // dropdown.click();
		       
		        List<WebElement> options = dropdown.findElements(By.tagName("li"));
		        
		        System.out.println("Size"+options.size());
		        for (WebElement option : options)
		        {
		            System.out.print("Values of option"+option);
		            if (option.getText().equals(var_dataToPass))
		            {
		                option.click(); // click the desired option
		                break;
		            }
		        }
		 
		 ;} 
		 catch(Exception e) { PrintVal("Renderer Script Exception : "+e.toString()); } 
		 PrintVal("Executed Step = CALLRENDERERSCRIPT,SELECTPLANREG197"); 
        WAIT.$(3,"TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SAVE","//span[contains(text(),'Save')]","TGTYPESCREENREG");

        WAIT.$("ELE_MSG_ITEMADDED","//div[@id='workbenchForm:workbenchTabs:messages']//span[@class='ui-messages-info-summary'][contains(text(),'Item successfully added.')]","VISIBLE","TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

        ASSERT.$("ELE_MSG_ITEMADDED","//div[@id='workbenchForm:workbenchTabs:messages']//span[@class='ui-messages-info-summary'][contains(text(),'Item successfully added.')]","VISIBLE","TGTYPESCREENREG");

        WAIT.$(3,"TGTYPESCREENREG");

        CALL.$("Logout","TGTYPESCREENREG");

        TAP.$("ELE_LINK_INITIALS","//span[@id='headerForm:avatarInitialsText']","TGTYPESCREENNO");

        WAIT.$(3,"TGTYPESCREENNO");

        TAP.$("ELE_LINK_LOGOUT","//span[contains(text(),'Log Out')]","TGTYPESCREENNO");

        WAIT.$(3,"TGTYPESCREENNO");

        TAP.$("ELE_LINK_GOTOLOGIN","//span[@class='ui-button-text ui-c']","TGTYPESCREENNO");

        WAIT.$(2,"TGTYPESCREENNO");

		var_Count = var_Count + ( 1 );
                 LOGGER.info("Executed Step = INCREASEVAR,var_Count,1,TGTYPESCREENREG");
    	}
          LOGGER.info("Executed Step = WEND,TGTYPESCREENREG");
    	resetAttemps();

    }


}
