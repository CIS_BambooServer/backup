package appium.tgo;

import com.tg.octopus.base.TGOWrapper;
import com.tg.octopus.libs.testng.listener.TestListener;
import static com.tg.octopus.base.TGOWrapper.Action.*;
import static com.tg.octopus.base.TGOWrapper.LOGGER;
import com.tg.octopus.wrapper.ActionWrapper;

import com.jayway.jsonpath.JsonPath;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.SessionNotCreatedException;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.Augmenter;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.remote.UnreachableBrowserException;

import org.testng.Assert;
import org.testng.ITestContext;
import org.testng.ITestResult;
import org.testng.Reporter;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;
import java.net.HttpURLConnection;
import java.net.URL;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import org.json.JSONArray;
import org.json.JSONObject;
import helper.*;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import static helper.WebTestUtility.getSubStringFromString;
import static helper.WebTestUtility.getTextFileToArrayList;
import java.sql.*;
import com.testautomationguru.utility.PDFUtil;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.multipdf.Splitter;

import java.util.*;
import org.openqa.selenium.*;
import java.io.*;
import java.net.*;

import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;


@Listeners(TestListener.class)
public class GIAS_STL extends TGOWrapper {



    static {
       needsresetoncebeforetest ="false";
       needsresetbeforeeverymethod ="true";
       captureverystepsnap ="true";
       appendlastsnap ="false";
       ishybridapp ="false";

    }


    @Test
    public void temptc01reinsurancestatic() {

        // No need to declare / print method name here, handled by TestNg listener
        // declare("temptc01reinsurancestatic");

        CALL.$("LoginToGIAS","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        WAIT.$("ELE_TXTBOX_USERNAME","//input[@id='guestForm:username']","VISIBLE","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_USERNAME","//input[@id='guestForm:username']","x7121","FALSE","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_PASSWORD","//input[@id='guestForm:password']","Cnx12345%","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_LOGIN","//span[contains(text(),'Login')]","TGTYPESCREENREG");

        WAIT.$("ELE_LINK_GIAS_HOME","//*[@id=\"headerForm:headerGiasButton\"]","VISIBLE","TGTYPESCREENREG");

		String var_dataToPass;
                 LOGGER.info("Executed Step = VAR,String,var_dataToPass,TGTYPESCREENREG");
		int var_Count = 0;
                 LOGGER.info("Executed Step = VAR,Integer,var_Count,0,TGTYPESCREENREG");
		int var_NumberOfPolicy = 0;
                 LOGGER.info("Executed Step = VAR,Integer,var_NumberOfPolicy,0,TGTYPESCREENREG");
        LOGGER.info("Executed Step = REPEAT IF");
    	while (check(var_NumberOfPolicy,"<",1,"TGTYPESCREENREG")) {
          LOGGER.info("Executed Step = WHILE,var_NumberOfPolicy,<,1,TGTYPESCREENREG");
		com.jayway.jsonpath.ReadContext var_CSVData = getJsonData("http://10.1.104.229/json/20210325/RiPGE1.json");
                 LOGGER.info("Executed Step = VAR,Json,var_CSVData,http://10.1.104.229/json/20210325/RiPGE1.json,TGTYPESCREENREG");
		String var_policyNumber = "1234DENTST";
                 LOGGER.info("Executed Step = VAR,String,var_policyNumber,1234DENTST,TGTYPESCREENREG");
		String var_policyStatus = "Premium Paying";
                 LOGGER.info("Executed Step = VAR,String,var_policyStatus,Premium Paying,TGTYPESCREENREG");
		String var_effectiveDate = "01/01/2014";
                 LOGGER.info("Executed Step = VAR,String,var_effectiveDate,01/01/2014,TGTYPESCREENREG");
		String var_paidToDate = "01/01/2015";
                 LOGGER.info("Executed Step = VAR,String,var_paidToDate,01/01/2015,TGTYPESCREENREG");
		String var_issueType = "N";
                 LOGGER.info("Executed Step = VAR,String,var_issueType,N,TGTYPESCREENREG");
		String var_issueState = "Missouri";
                 LOGGER.info("Executed Step = VAR,String,var_issueState,Missouri,TGTYPESCREENREG");
		String var_ownerState = "MO";
                 LOGGER.info("Executed Step = VAR,String,var_ownerState,MO,TGTYPESCREENREG");
        CALL.$("TempTC01PolicyInquiryTest","TGTYPESCREENREG");

        TAP.$("ELE_LINK_GIAS_HOME","//*[@id=\"headerForm:headerGiasButton\"]","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        SWIPE.$("DOWN","ELE_TXTBOX_SEARCHTASKS","//input[@id='workbenchForm:workbenchTabs:taskSearchContainer:taskSearchSearch']","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_SEARCHTASKS","//input[@id='workbenchForm:workbenchTabs:taskSearchContainer:taskSearchSearch']","Policy inquiry","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SEARCHTASK","//a[@id='workbenchForm:workbenchTabs:taskSearchContainer:tb_buttonSearch']//span[@class='fa fa-search fa-lg']","TGTYPESCREENREG");

        WAIT.$("ELE_LINK_POLICYINQUIRY","//span[contains(text(),'Policy Inquiry')]","VISIBLE","TGTYPESCREENREG");

        TAP.$("ELE_LINK_POLICYINQUIRY","//span[contains(text(),'Policy Inquiry')]","TGTYPESCREENREG");

        WAIT.$("ELE_TXTBOX_POLICYNUMBER","//input[@id='workbenchForm:workbenchTabs:policyNumber']","VISIBLE","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_POLICYNUMBER","//input[@id='workbenchForm:workbenchTabs:policyNumber']",var_policyNumber,"FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_CONTINUE","//span[contains(text(),'Continue')]","TGTYPESCREENREG");

        WAIT.$("ELE_LABEL_POLICYNUMBER","//span[@id='workbenchForm:workbenchTabs:policyNumber']","VISIBLE","TGTYPESCREENREG");

        ASSERT.$("ELE_LABEL_POLICYNUMBER","//span[@id='workbenchForm:workbenchTabs:policyNumber']","=",var_policyNumber,"TGTYPESCREENREG");

        ASSERT.$("ELE_LABEL_POLICYSTATUS","//span[@id='workbenchForm:workbenchTabs:statusText']","=",var_policyStatus,"TGTYPESCREENREG");

        ASSERT.$("ELE_LABEL_PAIDTODATE","//*[@id=\"workbenchForm:workbenchTabs:paidToDate\"]","=",var_paidToDate,"TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SHOWLINKS","//button[@id='workbenchForm:workbenchTabs:tb_buttonLink']","TGTYPESCREENREG");

        TAP.$("ELE_LINK_CONTRACT","//span[contains(text(),'Contract')]","TGTYPESCREENREG");

        WAIT.$("ELE_LABEL_POLICYNUMBER","//span[@id='workbenchForm:workbenchTabs:policyNumber']","VISIBLE","TGTYPESCREENREG");

        ASSERT.$("ELE_LABEL_POLICYNUMBER","//span[@id='workbenchForm:workbenchTabs:policyNumber']","=",var_policyNumber,"TGTYPESCREENREG");

        ASSERT.$("ELE_LABEL_POLICYSTATUS","//span[@id='workbenchForm:workbenchTabs:statusText']","=",var_policyStatus,"TGTYPESCREENREG");

        ASSERT.$("ELE_LABEL_EFFECTIVEDATE_CONTRACT","//span[@id='workbenchForm:workbenchTabs:effectiveDate']","=",var_effectiveDate,"TGTYPESCREENREG");

        ASSERT.$("ELE_LABEL_PAIDTODATE","//*[@id=\"workbenchForm:workbenchTabs:paidToDate\"]","=",var_paidToDate,"TGTYPESCREENREG");

        SWIPE.$("UP","ELE_LABEL_ISSUE_TYPE","//span[@id='workbenchForm:workbenchTabs:ISSUETYPE']","TGTYPESCREENREG");

        ASSERT.$("ELE_LABEL_ISSUE_TYPE","//span[@id='workbenchForm:workbenchTabs:ISSUETYPE']","=",var_issueType,"TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

        TAP.$("ELE_LINK_GIAS_HOME","//*[@id=\"headerForm:headerGiasButton\"]","TGTYPESCREENREG");

        WAIT.$(5,"TGTYPESCREENREG");

		var_NumberOfPolicy = var_NumberOfPolicy + ( 1 );
                 LOGGER.info("Executed Step = INCREASEVAR,var_NumberOfPolicy,1,TGTYPESCREENREG");
    	}
          LOGGER.info("Executed Step = WEND,TGTYPESCREENREG");
    	resetAttemps();

    }


    @Test
    public void tc001reinsurance() {

        // No need to declare / print method name here, handled by TestNg listener
        // declare("tc001reinsurance");

		int var_CountPolicy = 0;
                 LOGGER.info("Executed Step = VAR,Integer,var_CountPolicy,0,TGTYPESCREENREG");
		try { 
		 		         ArrayList<String> list = getTextFileToArrayList("//cis-coreapd1/CORE/NWITG1/TRANSFER/NW/Outbound/NationWide/Reinsurance/NWReinsurance.txt");
				        ArrayList<NWReinsurance> NWReinsurance = new ArrayList<NWReinsurance>();

				        for (int i = 0; i < list.size(); i++) {
				          NWReinsurance Reinsurance = new NWReinsurance();
						            Reinsurance.processingCompanyNumber = getSubStringFromString(list.get(i), 1, 3);
						            Reinsurance.policyNumber = getSubStringFromString(list.get(i), 4, 13);
						            Reinsurance.base_riderSequence = getSubStringFromString(list.get(i),14,15);
						            Reinsurance.policyStatus = getSubStringFromString(list.get(i), 16, 18);
						           // Reinsurance.addedToFileDate = getSubStringFromString(list.get(i),19,26);
								     Reinsurance.addedToFileDate = getSubStringFromString(list.get(i),19,26);
		//				            String YearFromString = getSubStringFromString(DateString,1,4);
		//							String MonthFromString = getSubStringFromString(DateString,5,6);
		////							String DateFromString = getSubStringFromString(DateString,7,8);
		//							String DateFinal = DateString;
		//							Reinsurance.addedToFileDate = DateFinal;


									Reinsurance.paidToDate = getSubStringFromString(list.get(i),27,34);
		//							 YearFromString = getSubStringFromString(DateString,1,4);
		//							 MonthFromString = getSubStringFromString(DateString,5,6);
		//							 DateFromString = getSubStringFromString(DateString,7,8);
		//							 DateFinal = MonthFromString +"/"+ DateFromString + "/" + YearFromString;
		//							 Reinsurance.paidToDate = DateFinal;


							//Reinsurance.statusChangeDate  = getSubStringFromString(list.get(i), 35, 42);
		//							YearFromString = getSubStringFromString(DateString,1,4);
		//							MonthFromString = getSubStringFromString(DateString,5,6);
		//							DateFromString = getSubStringFromString(DateString,7,8);
		//							DateFinal = MonthFromString +"/"+ DateFromString + "/" + YearFromString;
		//							Reinsurance.statusChangeDate = DateFinal;

						            Reinsurance.statusChangeDate = getSubStringFromString(list.get(i), 35, 42);
						            Reinsurance.issueType = getSubStringFromString(list.get(i), 43, 43);
						            Reinsurance.planCode = getSubStringFromString(list.get(i),44,51);
									Reinsurance.productCode = getSubStringFromString(list.get(i), 52, 52);
						            Reinsurance.productCode1 = getSubStringFromString(list.get(i), 53, 53);
						            Reinsurance.productCode2 = getSubStringFromString(list.get(i), 54, 54);
						            Reinsurance.administrationReinsuranceSwitch = getSubStringFromString(list.get(i), 55, 55);
						            Reinsurance.issueState = getSubStringFromString(list.get(i),56,57);
						            Reinsurance.ownerState = getSubStringFromString(list.get(i),58,59);
						            Reinsurance.underwritingMethod = getSubStringFromString(list.get(i), 60, 60);
						            Reinsurance.participatingIndicator = getSubStringFromString(list.get(i),61,61);
						            Reinsurance.deathBenefitOption = getSubStringFromString(list.get(i), 62, 62);
						            Reinsurance.faceAmount = getSubStringFromString(list.get(i),63,71);
						            Reinsurance.mortalityRating = getSubStringFromString(list.get(i), 72, 75);
						            Reinsurance.faceAmount2ADB = getSubStringFromString(list.get(i), 76, 84);
						            Reinsurance.adbTableRating = getSubStringFromString(list.get(i), 85, 88);
						            Reinsurance.faceAmount3WP = getSubStringFromString(list.get(i), 89, 97);
						            Reinsurance.wpTableRating = getSubStringFromString(list.get(i), 98, 101);
						            Reinsurance.ultimateFaceAmount = getSubStringFromString(list.get(i), 102, 110);
						            Reinsurance.jointType = getSubStringFromString(list.get(i), 111, 111);
						            Reinsurance.life = getSubStringFromString(list.get(i),112,113);
						            Reinsurance.jointAge = getSubStringFromString(list.get(i), 114, 115);
						            Reinsurance.premiumWaived = getSubStringFromString(list.get(i), 116, 124);
						            Reinsurance.specialPremium = getSubStringFromString(list.get(i), 125, 133);
						            Reinsurance.specialPremiumType = getSubStringFromString(list.get(i), 134, 134);
						            Reinsurance.dividend = getSubStringFromString(list.get(i), 135, 143);
						            Reinsurance.oldPolicyNumber = getSubStringFromString(list.get(i),144,153);
						            Reinsurance.administrationCode = getSubStringFromString(list.get(i), 154, 154);
						            Reinsurance.policyFee = getSubStringFromString(list.get(i), 155, 159);
						            Reinsurance.groupId = getSubStringFromString(list.get(i), 160, 174);
						            Reinsurance.locationCode = getSubStringFromString(list.get(i), 175, 184);
						            Reinsurance.currencyCode = getSubStringFromString(list.get(i), 185, 187);
						            Reinsurance.valuesYear = getSubStringFromString(list.get(i), 188, 191);
						            Reinsurance.valuesMonth = getSubStringFromString(list.get(i), 192, 193);
						            Reinsurance.deathBenefit = getSubStringFromString(list.get(i), 194, 202);
						            Reinsurance.cashValue = getSubStringFromString(list.get(i), 203, 211);
						            Reinsurance.nextCashValue = getSubStringFromString(list.get(i), 212, 220);
						            Reinsurance.premiumForBaseCoverage = getSubStringFromString(list.get(i), 221, 229);
						            Reinsurance.premiumForAccidentalDeathBenefit = getSubStringFromString(list.get(i), 230, 238);
						            Reinsurance.premiumForWaiver = getSubStringFromString(list.get(i), 239, 247);
						            Reinsurance.filler = getSubStringFromString(list.get(i), 248, 278);
						            Reinsurance.clientId = getSubStringFromString(list.get(i), 279, 293);
						            Reinsurance.lastName = getSubStringFromString(list.get(i), 294, 313);
						            Reinsurance.firstName = getSubStringFromString(list.get(i), 314, 328);
						            Reinsurance.middleInitials = getSubStringFromString(list.get(i), 329, 329);
						            Reinsurance.pricingSex = getSubStringFromString(list.get(i), 330, 330);
						            Reinsurance.sex = getSubStringFromString(list.get(i), 331, 331);

		//							DateString = getSubStringFromString(list.get(i), 332, 339);
		//							YearFromString = getSubStringFromString(DateString,1,4);
		//							MonthFromString = getSubStringFromString(DateString,5,6);
		//							DateFromString = getSubStringFromString(DateString,7,8);
		//							DateFinal = MonthFromString +"/"+ DateFromString + "/" + YearFromString;
		//							Reinsurance.dateOfBirth = DateFinal;

						            Reinsurance.dateOfBirth = getSubStringFromString(list.get(i), 332, 339);
						            Reinsurance.insuredStatus = getSubStringFromString(list.get(i), 340, 340);
						            Reinsurance.issueAge = getSubStringFromString(list.get(i), 341, 342);
						            Reinsurance.premiumClass = getSubStringFromString(list.get(i), 343, 344);
						            Reinsurance.mortalityRating1 = getSubStringFromString(list.get(i), 345, 348);
						            Reinsurance.mortalityDuration = getSubStringFromString(list.get(i), 349, 350);
						            Reinsurance.permanentFlatExtraRate = getSubStringFromString(list.get(i), 351, 355);
						            Reinsurance.permanentFlatExtraPremiumRateDuration = getSubStringFromString(list.get(i), 356, 357);
						            Reinsurance.temporaryFlatExtraRate = getSubStringFromString(list.get(i), 358, 362);
						            Reinsurance.temporaryFlatExtraPremiumRateDuration = getSubStringFromString(list.get(i), 363, 364);
						            Reinsurance.clientIdSecondaryInsured = getSubStringFromString(list.get(i), 365, 379);
						            Reinsurance.lastNameSecondaryInsured = getSubStringFromString(list.get(i), 380, 399);
						            Reinsurance.firstNameSecondaryInsured = getSubStringFromString(list.get(i), 400, 414);
						            Reinsurance.middleInitialsSecondaryInsured = getSubStringFromString(list.get(i), 415, 415);
						            Reinsurance.pricingSexSecondaryInsured = getSubStringFromString(list.get(i), 416, 416);
						            Reinsurance.sexSecondaryInsured = getSubStringFromString(list.get(i), 417, 417);



		//							DateString = getSubStringFromString(list.get(i), 418, 425);
		//							YearFromString = getSubStringFromString(DateString,1,4);
		//							MonthFromString = getSubStringFromString(DateString,5,6);
		//							DateFromString = getSubStringFromString(DateString,7,8);
		//							DateFinal = MonthFromString +"/"+ DateFromString + "/" + YearFromString;
		//							Reinsurance.dateOfBirthSecondaryInsured = DateFinal;


						            Reinsurance.dateOfBirthSecondaryInsured = getSubStringFromString(list.get(i), 418, 425);
						            Reinsurance.insuredStatusSecondaryInsured = getSubStringFromString(list.get(i), 426, 426);
						            Reinsurance.issueAgeSecondaryInsured = getSubStringFromString(list.get(i), 427, 428);
						            Reinsurance.premiumClassSecondaryInsured = getSubStringFromString(list.get(i), 429, 430);
						            Reinsurance.mortalityRating1SecondaryInsured = getSubStringFromString(list.get(i), 431, 434);
						            Reinsurance.mortalityDurationSecondaryInsured = getSubStringFromString(list.get(i), 435, 436);
						            Reinsurance.permanentFlatExtraRateSecondaryInsured = getSubStringFromString(list.get(i), 437, 441);
						            Reinsurance.permanentFlatExtraPremiumRateDurationSecondaryInsured = getSubStringFromString(list.get(i), 442, 443);
						            Reinsurance.temporaryFlatExtraRateSecondaryInsured = getSubStringFromString(list.get(i), 444, 448);
						            Reinsurance.temporaryFlatExtraPremiumRateDurationSecondaryInsured = getSubStringFromString(list.get(i), 449, 450);
						            Reinsurance.occupationClass = getSubStringFromString(list.get(i), 365, 366);
						            Reinsurance.colaPercentage = getSubStringFromString(list.get(i), 367, 369);
						            Reinsurance.accidentBenefitMode = getSubStringFromString(list.get(i), 370, 370);
						            Reinsurance.accidentBenefitPeriod = getSubStringFromString(list.get(i), 371, 373);
						            Reinsurance.accidentEliminationPeriod = getSubStringFromString(list.get(i), 374, 376);
						            Reinsurance.sicknessBenefitMode = getSubStringFromString(list.get(i), 377, 377);
						            Reinsurance.sicknessBenefitPeriod = getSubStringFromString(list.get(i), 378, 380);
						            Reinsurance.sicknessEliminationPeriod = getSubStringFromString(list.get(i), 381, 383);
						            Reinsurance.diBenefit = getSubStringFromString(list.get(i), 384, 399);
						            Reinsurance.filler2 = getSubStringFromString(list.get(i), 400, 450);
						            Reinsurance.ltcBeginningDate = getSubStringFromString(list.get(i), 365, 367);
						            Reinsurance.ltcBenefitAccount = getSubStringFromString(list.get(i), 368, 376);
						            Reinsurance.ltcIncreaseOption = getSubStringFromString(list.get(i), 377, 377);
						            Reinsurance.ltcIncreasePercentage = getSubStringFromString(list.get(i), 378, 380);
						            Reinsurance.ltcBenefitPeriodMode = getSubStringFromString(list.get(i), 381, 381);
						            Reinsurance.ltcBenefitPeriod = getSubStringFromString(list.get(i), 382, 384);
						            Reinsurance.filler3 = getSubStringFromString(list.get(i), 385, 450);
						            Reinsurance.suspendedCoverPercentage = getSubStringFromString(list.get(i), 451, 453);
						            Reinsurance.pml_CV_D100_ADDS = getSubStringFromString(list.get(i), 454, 464);
						            Reinsurance.pml_LAST_ACCTNG_STORED = getSubStringFromString(list.get(i), 465, 472);
						            Reinsurance.pml_GMDBR_TYPE = getSubStringFromString(list.get(i), 473, 473);
						            Reinsurance.pml_RDR_PROCESS = getSubStringFromString(list.get(i), 474, 474);
						            Reinsurance.pml_GDBR_OVRD_LAPSE_SW = getSubStringFromString(list.get(i), 475, 475);
						            Reinsurance.pml_GDBR_GRACE_IND = getSubStringFromString(list.get(i), 476, 476);
						            Reinsurance.pml_GDBR_LAPSE_ACCT_VALUE = getSubStringFromString(list.get(i), 477, 485);
						            Reinsurance.pml_GDBR_ACCUM_LAPSE_PREM = getSubStringFromString(list.get(i), 486, 492);
						            Reinsurance.pml_GDBR_ACCUM_PREM = getSubStringFromString(list.get(i), 493, 503);
						            Reinsurance.pml_GDBR_COMM_PREM = getSubStringFromString(list.get(i), 504, 514);
						            Reinsurance.pml_GDBR_MIN_PREM = getSubStringFromString(list.get(i), 515, 525);
						            Reinsurance.pml_CVOVER_DIVOVER = getSubStringFromString(list.get(i), 526, 526);
						            Reinsurance.pml_ATP_TDO_PREM = getSubStringFromString(list.get(i), 527, 533);
						            Reinsurance.pml_CLOSED_PLAN_SW = getSubStringFromString(list.get(i), 534, 534);
						            Reinsurance.pml_IMAC_COV_FORM = getSubStringFromString(list.get(i), 535, 538);
						            Reinsurance.pml_IMAC_COV_B4M = getSubStringFromString(list.get(i), 539, 543);
						            Reinsurance.pml_NEXT_RIDER = getSubStringFromString(list.get(i), 544, 545);
						            Reinsurance.pml_REINS_AGREEMENT = getSubStringFromString(list.get(i), 546, 554);
						            Reinsurance.pml_COV_REL = getSubStringFromString(list.get(i), 555, 555);
						            Reinsurance.pml_POL_FEE = getSubStringFromString(list.get(i), 556, 560);
						            Reinsurance.pml_UNIT_PREM = getSubStringFromString(list.get(i), 561, 569);
						            Reinsurance.pml_ADDS_CV_SW = getSubStringFromString(list.get(i), 570, 570);
						            Reinsurance.pml_LFP_AMT = getSubStringFromString(list.get(i), 571, 579);
						            Reinsurance.pml_ATP_TDO_AMT = getSubStringFromString(list.get(i), 580, 588);
						            Reinsurance.pml_PUI_AMT = getSubStringFromString(list.get(i), 589, 597);
						            Reinsurance.pml_PUO_AMT = getSubStringFromString(list.get(i), 598, 606);
						            Reinsurance.pml_PERM_ADDS = getSubStringFromString(list.get(i), 607, 615);
						            Reinsurance.pml_OLD_CO = getSubStringFromString(list.get(i), 616, 618);
						            Reinsurance.pml_VAL_EXTRA_CD = getSubStringFromString(list.get(i), 619, 619);
						            Reinsurance.group_Id = getSubStringFromString(list.get(i), 620, 629);
						            Reinsurance.filter_4 = getSubStringFromString(list.get(i), 630, 650);

						            NWReinsurance.add(Reinsurance);
				        }

				              var_CountPolicy = NWReinsurance.size();
						System.out.println("Model Completed");
							LOGGER.info("Model Completed");

				        try (Writer writer = new FileWriter("C:/xampp/htdocs/testgrid/NWReinsurance/Reinsurance.json")) {

				       Gson gson = new GsonBuilder().create();

				            com.google.gson.JsonObject main = new com.google.gson.JsonObject();


				            com.google.gson.JsonArray records = new Gson().toJsonTree(NWReinsurance).getAsJsonArray();
				            main.add("records",records);
				            gson.toJson(main,writer);
											System.out.println("JSON Completed");
											LOGGER.info("JSON Completed");
				        } catch (IOException e) {
											System.out.println("JSON error");
											LOGGER.info("JSON error");
				            e.printStackTrace();
				        }
							System.out.println("Completed");
							LOGGER.info("Completed");


				    
				 
				 
		 
		 ;} 
		 catch(Exception e) { PrintVal("Renderer Script Exception : "+e.toString()); } 
		 PrintVal("Executed Step = CALLRENDERERSCRIPT,TC01REINSURENCECONVERTTXTTOJSON"); 
		int var_Count = 0;
                 LOGGER.info("Executed Step = VAR,Integer,var_Count,0,TGTYPESCREENREG");
		com.jayway.jsonpath.ReadContext var_CSVData = getJsonData("C:/xampp/htdocs/testgrid/NWReinsurance/Reinsurance.json");
                 LOGGER.info("Executed Step = VAR,Json,var_CSVData,C:/xampp/htdocs/testgrid/NWReinsurance/Reinsurance.json,TGTYPESCREENREG");
		String var_policyResult = "PASS";
                 LOGGER.info("Executed Step = VAR,String,var_policyResult,PASS,TGTYPESCREENREG");
		String var_failedResults = "Fields = ";
                 LOGGER.info("Executed Step = VAR,String,var_failedResults,Fields = ,TGTYPESCREENREG");
		String var_apiResult = "FAIL";
                 LOGGER.info("Executed Step = VAR,String,var_apiResult,FAIL,TGTYPESCREENREG");
        LOGGER.info("Executed Step = REPEAT IF");
    	while (check(var_Count,"<",var_CountPolicy,"TGTYPESCREENREG")) {
          LOGGER.info("Executed Step = WHILE,var_Count,<,var_CountPolicy,TGTYPESCREENREG");
        CALL.$("TC01ReinsuranceVariableDeclarationFirst","TGTYPESCREENREG");

		String var_processingCompanyNumber;
                 LOGGER.info("Executed Step = VAR,String,var_processingCompanyNumber,TGTYPESCREENREG");
		var_processingCompanyNumber = getJsonData(var_CSVData , "$.records["+var_Count+"].processingCompanyNumber");
                 LOGGER.info("Executed Step = STORE,var_processingCompanyNumber,var_CSVData,$.records[{var_Count}].processingCompanyNumber,TGTYPESCREENREG");
		String var_policyNumber;
                 LOGGER.info("Executed Step = VAR,String,var_policyNumber,TGTYPESCREENREG");
		var_policyNumber = getJsonData(var_CSVData , "$.records["+var_Count+"].policyNumber");
                 LOGGER.info("Executed Step = STORE,var_policyNumber,var_CSVData,$.records[{var_Count}].policyNumber,TGTYPESCREENREG");
		String var_baseriderSequence;
                 LOGGER.info("Executed Step = VAR,String,var_baseriderSequence,TGTYPESCREENREG");
		var_baseriderSequence = getJsonData(var_CSVData , "$.records["+var_Count+"].base_riderSequence");
                 LOGGER.info("Executed Step = STORE,var_baseriderSequence,var_CSVData,$.records[{var_Count}].base_riderSequence,TGTYPESCREENREG");
		String var_policyStatus;
                 LOGGER.info("Executed Step = VAR,String,var_policyStatus,TGTYPESCREENREG");
		var_policyStatus = getJsonData(var_CSVData , "$.records["+var_Count+"].policyStatus");
                 LOGGER.info("Executed Step = STORE,var_policyStatus,var_CSVData,$.records[{var_Count}].policyStatus,TGTYPESCREENREG");
		String var_addedToFileDate;
                 LOGGER.info("Executed Step = VAR,String,var_addedToFileDate,TGTYPESCREENREG");
		var_addedToFileDate = getJsonData(var_CSVData , "$.records["+var_Count+"].addedToFileDate");
                 LOGGER.info("Executed Step = STORE,var_addedToFileDate,var_CSVData,$.records[{var_Count}].addedToFileDate,TGTYPESCREENREG");
		String var_paidToDate;
                 LOGGER.info("Executed Step = VAR,String,var_paidToDate,TGTYPESCREENREG");
		var_paidToDate = getJsonData(var_CSVData , "$.records["+var_Count+"].paidToDate");
                 LOGGER.info("Executed Step = STORE,var_paidToDate,var_CSVData,$.records[{var_Count}].paidToDate,TGTYPESCREENREG");
		String var_statusChangeDate;
                 LOGGER.info("Executed Step = VAR,String,var_statusChangeDate,TGTYPESCREENREG");
		var_statusChangeDate = getJsonData(var_CSVData , "$.records["+var_Count+"].statusChangeDate");
                 LOGGER.info("Executed Step = STORE,var_statusChangeDate,var_CSVData,$.records[{var_Count}].statusChangeDate,TGTYPESCREENREG");
		String var_issueType;
                 LOGGER.info("Executed Step = VAR,String,var_issueType,TGTYPESCREENREG");
		var_issueType = getJsonData(var_CSVData , "$.records["+var_Count+"].issueType");
                 LOGGER.info("Executed Step = STORE,var_issueType,var_CSVData,$.records[{var_Count}].issueType,TGTYPESCREENREG");
		String var_planCode;
                 LOGGER.info("Executed Step = VAR,String,var_planCode,TGTYPESCREENREG");
		var_planCode = getJsonData(var_CSVData , "$.records["+var_Count+"].planCode");
                 LOGGER.info("Executed Step = STORE,var_planCode,var_CSVData,$.records[{var_Count}].planCode,TGTYPESCREENREG");
		String var_productCode;
                 LOGGER.info("Executed Step = VAR,String,var_productCode,TGTYPESCREENREG");
		var_productCode = getJsonData(var_CSVData , "$.records["+var_Count+"].productCode");
                 LOGGER.info("Executed Step = STORE,var_productCode,var_CSVData,$.records[{var_Count}].productCode,TGTYPESCREENREG");
		String var_productCode1;
                 LOGGER.info("Executed Step = VAR,String,var_productCode1,TGTYPESCREENREG");
		var_productCode1 = getJsonData(var_CSVData , "$.records["+var_Count+"].productCode1");
                 LOGGER.info("Executed Step = STORE,var_productCode1,var_CSVData,$.records[{var_Count}].productCode1,TGTYPESCREENREG");
		String var_productCode2;
                 LOGGER.info("Executed Step = VAR,String,var_productCode2,TGTYPESCREENREG");
		var_productCode2 = getJsonData(var_CSVData , "$.records["+var_Count+"].productCode2");
                 LOGGER.info("Executed Step = STORE,var_productCode2,var_CSVData,$.records[{var_Count}].productCode2,TGTYPESCREENREG");
		String var_administrationReinsuranceSwitch;
                 LOGGER.info("Executed Step = VAR,String,var_administrationReinsuranceSwitch,TGTYPESCREENREG");
		var_administrationReinsuranceSwitch = getJsonData(var_CSVData , "$.records["+var_Count+"].administrationReinsuranceSwitch");
                 LOGGER.info("Executed Step = STORE,var_administrationReinsuranceSwitch,var_CSVData,$.records[{var_Count}].administrationReinsuranceSwitch,TGTYPESCREENREG");
		String var_issueState;
                 LOGGER.info("Executed Step = VAR,String,var_issueState,TGTYPESCREENREG");
		var_issueState = getJsonData(var_CSVData , "$.records["+var_Count+"].issueState");
                 LOGGER.info("Executed Step = STORE,var_issueState,var_CSVData,$.records[{var_Count}].issueState,TGTYPESCREENREG");
		String var_ownerState;
                 LOGGER.info("Executed Step = VAR,String,var_ownerState,TGTYPESCREENREG");
		var_ownerState = getJsonData(var_CSVData , "$.records["+var_Count+"].ownerState");
                 LOGGER.info("Executed Step = STORE,var_ownerState,var_CSVData,$.records[{var_Count}].ownerState,TGTYPESCREENREG");
		String var_underwritingMethod;
                 LOGGER.info("Executed Step = VAR,String,var_underwritingMethod,TGTYPESCREENREG");
		var_underwritingMethod = getJsonData(var_CSVData , "$.records["+var_Count+"].underwritingMethod");
                 LOGGER.info("Executed Step = STORE,var_underwritingMethod,var_CSVData,$.records[{var_Count}].underwritingMethod,TGTYPESCREENREG");
		String var_participatingIndicator;
                 LOGGER.info("Executed Step = VAR,String,var_participatingIndicator,TGTYPESCREENREG");
		var_participatingIndicator = getJsonData(var_CSVData , "$.records["+var_Count+"].participatingIndicator");
                 LOGGER.info("Executed Step = STORE,var_participatingIndicator,var_CSVData,$.records[{var_Count}].participatingIndicator,TGTYPESCREENREG");
		String var_deathBenefitOption;
                 LOGGER.info("Executed Step = VAR,String,var_deathBenefitOption,TGTYPESCREENREG");
		var_deathBenefitOption = getJsonData(var_CSVData , "$.records["+var_Count+"].deathBenefitOption");
                 LOGGER.info("Executed Step = STORE,var_deathBenefitOption,var_CSVData,$.records[{var_Count}].deathBenefitOption,TGTYPESCREENREG");
		String var_faceAmount;
                 LOGGER.info("Executed Step = VAR,String,var_faceAmount,TGTYPESCREENREG");
		var_faceAmount = getJsonData(var_CSVData , "$.records["+var_Count+"].faceAmount");
                 LOGGER.info("Executed Step = STORE,var_faceAmount,var_CSVData,$.records[{var_Count}].faceAmount,TGTYPESCREENREG");
		String var_mortalityRating;
                 LOGGER.info("Executed Step = VAR,String,var_mortalityRating,TGTYPESCREENREG");
		var_mortalityRating = getJsonData(var_CSVData , "$.records["+var_Count+"].mortalityRating");
                 LOGGER.info("Executed Step = STORE,var_mortalityRating,var_CSVData,$.records[{var_Count}].mortalityRating,TGTYPESCREENREG");
		String var_faceAmount2ADB;
                 LOGGER.info("Executed Step = VAR,String,var_faceAmount2ADB,TGTYPESCREENREG");
		var_faceAmount2ADB = getJsonData(var_CSVData , "$.records["+var_Count+"].faceAmount2ADB");
                 LOGGER.info("Executed Step = STORE,var_faceAmount2ADB,var_CSVData,$.records[{var_Count}].faceAmount2ADB,TGTYPESCREENREG");
		String var_adbTableRating;
                 LOGGER.info("Executed Step = VAR,String,var_adbTableRating,TGTYPESCREENREG");
		var_adbTableRating = getJsonData(var_CSVData , "$.records["+var_Count+"].adbTableRating");
                 LOGGER.info("Executed Step = STORE,var_adbTableRating,var_CSVData,$.records[{var_Count}].adbTableRating,TGTYPESCREENREG");
		String var_faceAmount3WP;
                 LOGGER.info("Executed Step = VAR,String,var_faceAmount3WP,TGTYPESCREENREG");
		var_faceAmount3WP = getJsonData(var_CSVData , "$.records["+var_Count+"].faceAmount3WP");
                 LOGGER.info("Executed Step = STORE,var_faceAmount3WP,var_CSVData,$.records[{var_Count}].faceAmount3WP,TGTYPESCREENREG");
		String var_wpTableRating;
                 LOGGER.info("Executed Step = VAR,String,var_wpTableRating,TGTYPESCREENREG");
		var_wpTableRating = getJsonData(var_CSVData , "$.records["+var_Count+"].wpTableRating");
                 LOGGER.info("Executed Step = STORE,var_wpTableRating,var_CSVData,$.records[{var_Count}].wpTableRating,TGTYPESCREENREG");
		String var_ultimateFaceAmount;
                 LOGGER.info("Executed Step = VAR,String,var_ultimateFaceAmount,TGTYPESCREENREG");
		var_ultimateFaceAmount = getJsonData(var_CSVData , "$.records["+var_Count+"].ultimateFaceAmount");
                 LOGGER.info("Executed Step = STORE,var_ultimateFaceAmount,var_CSVData,$.records[{var_Count}].ultimateFaceAmount,TGTYPESCREENREG");
		String var_jointType;
                 LOGGER.info("Executed Step = VAR,String,var_jointType,TGTYPESCREENREG");
		var_jointType = getJsonData(var_CSVData , "$.records["+var_Count+"].jointType");
                 LOGGER.info("Executed Step = STORE,var_jointType,var_CSVData,$.records[{var_Count}].jointType,TGTYPESCREENREG");
		String var_life;
                 LOGGER.info("Executed Step = VAR,String,var_life,TGTYPESCREENREG");
		var_life = getJsonData(var_CSVData , "$.records["+var_Count+"].life");
                 LOGGER.info("Executed Step = STORE,var_life,var_CSVData,$.records[{var_Count}].life,TGTYPESCREENREG");
		String var_jointAge;
                 LOGGER.info("Executed Step = VAR,String,var_jointAge,TGTYPESCREENREG");
		var_jointAge = getJsonData(var_CSVData , "$.records["+var_Count+"].jointAge");
                 LOGGER.info("Executed Step = STORE,var_jointAge,var_CSVData,$.records[{var_Count}].jointAge,TGTYPESCREENREG");
		String var_premiumWaived;
                 LOGGER.info("Executed Step = VAR,String,var_premiumWaived,TGTYPESCREENREG");
		var_premiumWaived = getJsonData(var_CSVData , "$.records["+var_Count+"].premiumWaived");
                 LOGGER.info("Executed Step = STORE,var_premiumWaived,var_CSVData,$.records[{var_Count}].premiumWaived,TGTYPESCREENREG");
		String var_specialPremium;
                 LOGGER.info("Executed Step = VAR,String,var_specialPremium,TGTYPESCREENREG");
		var_specialPremium = getJsonData(var_CSVData , "$.records["+var_Count+"].specialPremium");
                 LOGGER.info("Executed Step = STORE,var_specialPremium,var_CSVData,$.records[{var_Count}].specialPremium,TGTYPESCREENREG");
		String var_specialPremiumType;
                 LOGGER.info("Executed Step = VAR,String,var_specialPremiumType,TGTYPESCREENREG");
		var_specialPremiumType = getJsonData(var_CSVData , "$.records["+var_Count+"].specialPremiumType");
                 LOGGER.info("Executed Step = STORE,var_specialPremiumType,var_CSVData,$.records[{var_Count}].specialPremiumType,TGTYPESCREENREG");
		String var_dividend;
                 LOGGER.info("Executed Step = VAR,String,var_dividend,TGTYPESCREENREG");
		var_dividend = getJsonData(var_CSVData , "$.records["+var_Count+"].dividend");
                 LOGGER.info("Executed Step = STORE,var_dividend,var_CSVData,$.records[{var_Count}].dividend,TGTYPESCREENREG");
		String var_oldPolicyNumber;
                 LOGGER.info("Executed Step = VAR,String,var_oldPolicyNumber,TGTYPESCREENREG");
		var_oldPolicyNumber = getJsonData(var_CSVData , "$.records["+var_Count+"].oldPolicyNumber");
                 LOGGER.info("Executed Step = STORE,var_oldPolicyNumber,var_CSVData,$.records[{var_Count}].oldPolicyNumber,TGTYPESCREENREG");
		String var_administrationCode;
                 LOGGER.info("Executed Step = VAR,String,var_administrationCode,TGTYPESCREENREG");
		var_administrationCode = getJsonData(var_CSVData , "$.records["+var_Count+"].administrationCode");
                 LOGGER.info("Executed Step = STORE,var_administrationCode,var_CSVData,$.records[{var_Count}].administrationCode,TGTYPESCREENREG");
		String var_policyFee;
                 LOGGER.info("Executed Step = VAR,String,var_policyFee,TGTYPESCREENREG");
		var_policyFee = getJsonData(var_CSVData , "$.records["+var_Count+"].policyFee");
                 LOGGER.info("Executed Step = STORE,var_policyFee,var_CSVData,$.records[{var_Count}].policyFee,TGTYPESCREENREG");
		String var_groupId;
                 LOGGER.info("Executed Step = VAR,String,var_groupId,TGTYPESCREENREG");
		var_groupId = getJsonData(var_CSVData , "$.records["+var_Count+"].groupId");
                 LOGGER.info("Executed Step = STORE,var_groupId,var_CSVData,$.records[{var_Count}].groupId,TGTYPESCREENREG");
		String var_locationCode;
                 LOGGER.info("Executed Step = VAR,String,var_locationCode,TGTYPESCREENREG");
		var_locationCode = getJsonData(var_CSVData , "$.records["+var_Count+"].locationCode");
                 LOGGER.info("Executed Step = STORE,var_locationCode,var_CSVData,$.records[{var_Count}].locationCode,TGTYPESCREENREG");
		String var_currencyCode;
                 LOGGER.info("Executed Step = VAR,String,var_currencyCode,TGTYPESCREENREG");
		var_currencyCode = getJsonData(var_CSVData , "$.records["+var_Count+"].currencyCode");
                 LOGGER.info("Executed Step = STORE,var_currencyCode,var_CSVData,$.records[{var_Count}].currencyCode,TGTYPESCREENREG");
		String var_valuesYear;
                 LOGGER.info("Executed Step = VAR,String,var_valuesYear,TGTYPESCREENREG");
		var_valuesYear = getJsonData(var_CSVData , "$.records["+var_Count+"].valuesYear");
                 LOGGER.info("Executed Step = STORE,var_valuesYear,var_CSVData,$.records[{var_Count}].valuesYear,TGTYPESCREENREG");
		String var_valuesMonth;
                 LOGGER.info("Executed Step = VAR,String,var_valuesMonth,TGTYPESCREENREG");
		var_valuesMonth = getJsonData(var_CSVData , "$.records["+var_Count+"].valuesMonth");
                 LOGGER.info("Executed Step = STORE,var_valuesMonth,var_CSVData,$.records[{var_Count}].valuesMonth,TGTYPESCREENREG");
		String var_deathBenefit;
                 LOGGER.info("Executed Step = VAR,String,var_deathBenefit,TGTYPESCREENREG");
		var_deathBenefit = getJsonData(var_CSVData , "$.records["+var_Count+"].deathBenefit");
                 LOGGER.info("Executed Step = STORE,var_deathBenefit,var_CSVData,$.records[{var_Count}].deathBenefit,TGTYPESCREENREG");
		String var_cashValue;
                 LOGGER.info("Executed Step = VAR,String,var_cashValue,TGTYPESCREENREG");
		var_cashValue = getJsonData(var_CSVData , "$.records["+var_Count+"].cashValue");
                 LOGGER.info("Executed Step = STORE,var_cashValue,var_CSVData,$.records[{var_Count}].cashValue,TGTYPESCREENREG");
		String var_nextCashValue;
                 LOGGER.info("Executed Step = VAR,String,var_nextCashValue,TGTYPESCREENREG");
		var_nextCashValue = getJsonData(var_CSVData , "$.records["+var_Count+"].nextCashValue");
                 LOGGER.info("Executed Step = STORE,var_nextCashValue,var_CSVData,$.records[{var_Count}].nextCashValue,TGTYPESCREENREG");
		String var_premiumForBaseCoverage;
                 LOGGER.info("Executed Step = VAR,String,var_premiumForBaseCoverage,TGTYPESCREENREG");
		var_premiumForBaseCoverage = getJsonData(var_CSVData , "$.records["+var_Count+"].premiumForBaseCoverage");
                 LOGGER.info("Executed Step = STORE,var_premiumForBaseCoverage,var_CSVData,$.records[{var_Count}].premiumForBaseCoverage,TGTYPESCREENREG");
		String var_premiumForAccidentalDeathBenefit;
                 LOGGER.info("Executed Step = VAR,String,var_premiumForAccidentalDeathBenefit,TGTYPESCREENREG");
		var_premiumForAccidentalDeathBenefit = getJsonData(var_CSVData , "$.records["+var_Count+"].premiumForAccidentalDeathBenefit");
                 LOGGER.info("Executed Step = STORE,var_premiumForAccidentalDeathBenefit,var_CSVData,$.records[{var_Count}].premiumForAccidentalDeathBenefit,TGTYPESCREENREG");
		String var_premiumForWaiver;
                 LOGGER.info("Executed Step = VAR,String,var_premiumForWaiver,TGTYPESCREENREG");
		var_premiumForWaiver = getJsonData(var_CSVData , "$.records["+var_Count+"].premiumForWaiver");
                 LOGGER.info("Executed Step = STORE,var_premiumForWaiver,var_CSVData,$.records[{var_Count}].premiumForWaiver,TGTYPESCREENREG");
		String var_filler;
                 LOGGER.info("Executed Step = VAR,String,var_filler,TGTYPESCREENREG");
		var_filler = getJsonData(var_CSVData , "$.records["+var_Count+"].filler");
                 LOGGER.info("Executed Step = STORE,var_filler,var_CSVData,$.records[{var_Count}].filler,TGTYPESCREENREG");
		String var_clientId;
                 LOGGER.info("Executed Step = VAR,String,var_clientId,TGTYPESCREENREG");
		var_clientId = getJsonData(var_CSVData , "$.records["+var_Count+"].clientId");
                 LOGGER.info("Executed Step = STORE,var_clientId,var_CSVData,$.records[{var_Count}].clientId,TGTYPESCREENREG");
		String var_lastName;
                 LOGGER.info("Executed Step = VAR,String,var_lastName,TGTYPESCREENREG");
		var_lastName = getJsonData(var_CSVData , "$.records["+var_Count+"].lastName");
                 LOGGER.info("Executed Step = STORE,var_lastName,var_CSVData,$.records[{var_Count}].lastName,TGTYPESCREENREG");
		String var_firstName;
                 LOGGER.info("Executed Step = VAR,String,var_firstName,TGTYPESCREENREG");
		var_firstName = getJsonData(var_CSVData , "$.records["+var_Count+"].firstName");
                 LOGGER.info("Executed Step = STORE,var_firstName,var_CSVData,$.records[{var_Count}].firstName,TGTYPESCREENREG");
		String var_middleInitials;
                 LOGGER.info("Executed Step = VAR,String,var_middleInitials,TGTYPESCREENREG");
		var_middleInitials = getJsonData(var_CSVData , "$.records["+var_Count+"].middleInitials");
                 LOGGER.info("Executed Step = STORE,var_middleInitials,var_CSVData,$.records[{var_Count}].middleInitials,TGTYPESCREENREG");
		String var_pricingSex;
                 LOGGER.info("Executed Step = VAR,String,var_pricingSex,TGTYPESCREENREG");
		var_pricingSex = getJsonData(var_CSVData , "$.records["+var_Count+"].pricingSex");
                 LOGGER.info("Executed Step = STORE,var_pricingSex,var_CSVData,$.records[{var_Count}].pricingSex,TGTYPESCREENREG");
		String var_sex;
                 LOGGER.info("Executed Step = VAR,String,var_sex,TGTYPESCREENREG");
		var_sex = getJsonData(var_CSVData , "$.records["+var_Count+"].sex");
                 LOGGER.info("Executed Step = STORE,var_sex,var_CSVData,$.records[{var_Count}].sex,TGTYPESCREENREG");
		String var_dateOfBirth;
                 LOGGER.info("Executed Step = VAR,String,var_dateOfBirth,TGTYPESCREENREG");
		var_dateOfBirth = getJsonData(var_CSVData , "$.records["+var_Count+"].dateOfBirth");
                 LOGGER.info("Executed Step = STORE,var_dateOfBirth,var_CSVData,$.records[{var_Count}].dateOfBirth,TGTYPESCREENREG");
		String var_insuredStatus;
                 LOGGER.info("Executed Step = VAR,String,var_insuredStatus,TGTYPESCREENREG");
		var_insuredStatus = getJsonData(var_CSVData , "$.records["+var_Count+"].insuredStatus");
                 LOGGER.info("Executed Step = STORE,var_insuredStatus,var_CSVData,$.records[{var_Count}].insuredStatus,TGTYPESCREENREG");
		String var_issueAge;
                 LOGGER.info("Executed Step = VAR,String,var_issueAge,TGTYPESCREENREG");
		var_issueAge = getJsonData(var_CSVData , "$.records["+var_Count+"].issueAge");
                 LOGGER.info("Executed Step = STORE,var_issueAge,var_CSVData,$.records[{var_Count}].issueAge,TGTYPESCREENREG");
		String var_premiumClass;
                 LOGGER.info("Executed Step = VAR,String,var_premiumClass,TGTYPESCREENREG");
		var_premiumClass = getJsonData(var_CSVData , "$.records["+var_Count+"].premiumClass");
                 LOGGER.info("Executed Step = STORE,var_premiumClass,var_CSVData,$.records[{var_Count}].premiumClass,TGTYPESCREENREG");
		String var_mortalityRating1;
                 LOGGER.info("Executed Step = VAR,String,var_mortalityRating1,TGTYPESCREENREG");
		var_mortalityRating1 = getJsonData(var_CSVData , "$.records["+var_Count+"].mortalityRating1");
                 LOGGER.info("Executed Step = STORE,var_mortalityRating1,var_CSVData,$.records[{var_Count}].mortalityRating1,TGTYPESCREENREG");
		String var_mortalityDuration;
                 LOGGER.info("Executed Step = VAR,String,var_mortalityDuration,TGTYPESCREENREG");
		var_mortalityDuration = getJsonData(var_CSVData , "$.records["+var_Count+"].mortalityDuration");
                 LOGGER.info("Executed Step = STORE,var_mortalityDuration,var_CSVData,$.records[{var_Count}].mortalityDuration,TGTYPESCREENREG");
		String var_permanentFlatExtraRate;
                 LOGGER.info("Executed Step = VAR,String,var_permanentFlatExtraRate,TGTYPESCREENREG");
		var_permanentFlatExtraRate = getJsonData(var_CSVData , "$.records["+var_Count+"].permanentFlatExtraRate");
                 LOGGER.info("Executed Step = STORE,var_permanentFlatExtraRate,var_CSVData,$.records[{var_Count}].permanentFlatExtraRate,TGTYPESCREENREG");
		String var_permanentFlatExtraPremiumRateDuration;
                 LOGGER.info("Executed Step = VAR,String,var_permanentFlatExtraPremiumRateDuration,TGTYPESCREENREG");
		var_permanentFlatExtraPremiumRateDuration = getJsonData(var_CSVData , "$.records["+var_Count+"].permanentFlatExtraPremiumRateDuration");
                 LOGGER.info("Executed Step = STORE,var_permanentFlatExtraPremiumRateDuration,var_CSVData,$.records[{var_Count}].permanentFlatExtraPremiumRateDuration,TGTYPESCREENREG");
		String var_temporaryFlatExtraRate;
                 LOGGER.info("Executed Step = VAR,String,var_temporaryFlatExtraRate,TGTYPESCREENREG");
		var_temporaryFlatExtraRate = getJsonData(var_CSVData , "$.records["+var_Count+"].temporaryFlatExtraRate");
                 LOGGER.info("Executed Step = STORE,var_temporaryFlatExtraRate,var_CSVData,$.records[{var_Count}].temporaryFlatExtraRate,TGTYPESCREENREG");
		String var_temporaryFlatExtraPremiumRateDuration;
                 LOGGER.info("Executed Step = VAR,String,var_temporaryFlatExtraPremiumRateDuration,TGTYPESCREENREG");
		var_temporaryFlatExtraPremiumRateDuration = getJsonData(var_CSVData , "$.records["+var_Count+"].temporaryFlatExtraPremiumRateDuration");
                 LOGGER.info("Executed Step = STORE,var_temporaryFlatExtraPremiumRateDuration,var_CSVData,$.records[{var_Count}].temporaryFlatExtraPremiumRateDuration,TGTYPESCREENREG");
        CALL.$("TC01ReinsuranceVariableDeclarationSecond","TGTYPESCREENREG");

		String var_clientIdSecondaryInsured;
                 LOGGER.info("Executed Step = VAR,String,var_clientIdSecondaryInsured,TGTYPESCREENREG");
		var_clientIdSecondaryInsured = getJsonData(var_CSVData , "$.records["+var_Count+"].clientIdSecondaryInsured");
                 LOGGER.info("Executed Step = STORE,var_clientIdSecondaryInsured,var_CSVData,$.records[{var_Count}].clientIdSecondaryInsured,TGTYPESCREENREG");
		String var_lastNameSecondaryInsured;
                 LOGGER.info("Executed Step = VAR,String,var_lastNameSecondaryInsured,TGTYPESCREENREG");
		var_lastNameSecondaryInsured = getJsonData(var_CSVData , "$.records["+var_Count+"].lastNameSecondaryInsured");
                 LOGGER.info("Executed Step = STORE,var_lastNameSecondaryInsured,var_CSVData,$.records[{var_Count}].lastNameSecondaryInsured,TGTYPESCREENREG");
		String var_firstNameSecondaryInsured;
                 LOGGER.info("Executed Step = VAR,String,var_firstNameSecondaryInsured,TGTYPESCREENREG");
		var_firstNameSecondaryInsured = getJsonData(var_CSVData , "$.records["+var_Count+"].firstNameSecondaryInsured");
                 LOGGER.info("Executed Step = STORE,var_firstNameSecondaryInsured,var_CSVData,$.records[{var_Count}].firstNameSecondaryInsured,TGTYPESCREENREG");
		String var_middleInitialsSecondaryInsured;
                 LOGGER.info("Executed Step = VAR,String,var_middleInitialsSecondaryInsured,TGTYPESCREENREG");
		var_middleInitialsSecondaryInsured = getJsonData(var_CSVData , "$.records["+var_Count+"].middleInitialsSecondaryInsured");
                 LOGGER.info("Executed Step = STORE,var_middleInitialsSecondaryInsured,var_CSVData,$.records[{var_Count}].middleInitialsSecondaryInsured,TGTYPESCREENREG");
		String var_pricingSexSecondaryInsured;
                 LOGGER.info("Executed Step = VAR,String,var_pricingSexSecondaryInsured,TGTYPESCREENREG");
		var_pricingSexSecondaryInsured = getJsonData(var_CSVData , "$.records["+var_Count+"].pricingSexSecondaryInsured");
                 LOGGER.info("Executed Step = STORE,var_pricingSexSecondaryInsured,var_CSVData,$.records[{var_Count}].pricingSexSecondaryInsured,TGTYPESCREENREG");
		String var_sexSecondaryInsured;
                 LOGGER.info("Executed Step = VAR,String,var_sexSecondaryInsured,TGTYPESCREENREG");
		var_sexSecondaryInsured = getJsonData(var_CSVData , "$.records["+var_Count+"].sexSecondaryInsured");
                 LOGGER.info("Executed Step = STORE,var_sexSecondaryInsured,var_CSVData,$.records[{var_Count}].sexSecondaryInsured,TGTYPESCREENREG");
		String var_dateOfBirthSecondaryInsured;
                 LOGGER.info("Executed Step = VAR,String,var_dateOfBirthSecondaryInsured,TGTYPESCREENREG");
		var_dateOfBirthSecondaryInsured = getJsonData(var_CSVData , "$.records["+var_Count+"].dateOfBirthSecondaryInsured");
                 LOGGER.info("Executed Step = STORE,var_dateOfBirthSecondaryInsured,var_CSVData,$.records[{var_Count}].dateOfBirthSecondaryInsured,TGTYPESCREENREG");
		String var_insuredStatusSecondaryInsured;
                 LOGGER.info("Executed Step = VAR,String,var_insuredStatusSecondaryInsured,TGTYPESCREENREG");
		var_insuredStatusSecondaryInsured = getJsonData(var_CSVData , "$.records["+var_Count+"].insuredStatusSecondaryInsured");
                 LOGGER.info("Executed Step = STORE,var_insuredStatusSecondaryInsured,var_CSVData,$.records[{var_Count}].insuredStatusSecondaryInsured,TGTYPESCREENREG");
		String var_issueAgeSecondaryInsured;
                 LOGGER.info("Executed Step = VAR,String,var_issueAgeSecondaryInsured,TGTYPESCREENREG");
		var_issueAgeSecondaryInsured = getJsonData(var_CSVData , "$.records["+var_Count+"].issueAgeSecondaryInsured");
                 LOGGER.info("Executed Step = STORE,var_issueAgeSecondaryInsured,var_CSVData,$.records[{var_Count}].issueAgeSecondaryInsured,TGTYPESCREENREG");
		String var_premiumClassSecondaryInsured;
                 LOGGER.info("Executed Step = VAR,String,var_premiumClassSecondaryInsured,TGTYPESCREENREG");
		var_premiumClassSecondaryInsured = getJsonData(var_CSVData , "$.records["+var_Count+"].premiumClassSecondaryInsured");
                 LOGGER.info("Executed Step = STORE,var_premiumClassSecondaryInsured,var_CSVData,$.records[{var_Count}].premiumClassSecondaryInsured,TGTYPESCREENREG");
		String var_mortalityRating1SecondaryInsured;
                 LOGGER.info("Executed Step = VAR,String,var_mortalityRating1SecondaryInsured,TGTYPESCREENREG");
		var_mortalityRating1SecondaryInsured = getJsonData(var_CSVData , "$.records["+var_Count+"].mortalityRating1SecondaryInsured");
                 LOGGER.info("Executed Step = STORE,var_mortalityRating1SecondaryInsured,var_CSVData,$.records[{var_Count}].mortalityRating1SecondaryInsured,TGTYPESCREENREG");
		String var_mortalityDurationSecondaryInsured;
                 LOGGER.info("Executed Step = VAR,String,var_mortalityDurationSecondaryInsured,TGTYPESCREENREG");
		var_mortalityDurationSecondaryInsured = getJsonData(var_CSVData , "$.records["+var_Count+"].mortalityDurationSecondaryInsured");
                 LOGGER.info("Executed Step = STORE,var_mortalityDurationSecondaryInsured,var_CSVData,$.records[{var_Count}].mortalityDurationSecondaryInsured,TGTYPESCREENREG");
		String var_permanentFlatExtraRateSecondaryInsured;
                 LOGGER.info("Executed Step = VAR,String,var_permanentFlatExtraRateSecondaryInsured,TGTYPESCREENREG");
		var_permanentFlatExtraRateSecondaryInsured = getJsonData(var_CSVData , "$.records["+var_Count+"].permanentFlatExtraRateSecondaryInsured");
                 LOGGER.info("Executed Step = STORE,var_permanentFlatExtraRateSecondaryInsured,var_CSVData,$.records[{var_Count}].permanentFlatExtraRateSecondaryInsured,TGTYPESCREENREG");
		String var_permanentFlatExtraPremiumRateDurationSecondaryInsured;
                 LOGGER.info("Executed Step = VAR,String,var_permanentFlatExtraPremiumRateDurationSecondaryInsured,TGTYPESCREENREG");
		var_permanentFlatExtraPremiumRateDurationSecondaryInsured = getJsonData(var_CSVData , "$.records["+var_Count+"].permanentFlatExtraPremiumRateDurationSecondaryInsured");
                 LOGGER.info("Executed Step = STORE,var_permanentFlatExtraPremiumRateDurationSecondaryInsured,var_CSVData,$.records[{var_Count}].permanentFlatExtraPremiumRateDurationSecondaryInsured,TGTYPESCREENREG");
		String var_temporaryFlatExtraRateSecondaryInsured;
                 LOGGER.info("Executed Step = VAR,String,var_temporaryFlatExtraRateSecondaryInsured,TGTYPESCREENREG");
		var_temporaryFlatExtraRateSecondaryInsured = getJsonData(var_CSVData , "$.records["+var_Count+"].temporaryFlatExtraRateSecondaryInsured");
                 LOGGER.info("Executed Step = STORE,var_temporaryFlatExtraRateSecondaryInsured,var_CSVData,$.records[{var_Count}].temporaryFlatExtraRateSecondaryInsured,TGTYPESCREENREG");
		String var_temporaryFlatExtraPremiumRateDurationSecondaryInsured;
                 LOGGER.info("Executed Step = VAR,String,var_temporaryFlatExtraPremiumRateDurationSecondaryInsured,TGTYPESCREENREG");
		var_temporaryFlatExtraPremiumRateDurationSecondaryInsured = getJsonData(var_CSVData , "$.records["+var_Count+"].temporaryFlatExtraPremiumRateDurationSecondaryInsured");
                 LOGGER.info("Executed Step = STORE,var_temporaryFlatExtraPremiumRateDurationSecondaryInsured,var_CSVData,$.records[{var_Count}].temporaryFlatExtraPremiumRateDurationSecondaryInsured,TGTYPESCREENREG");
		String var_occupationClass;
                 LOGGER.info("Executed Step = VAR,String,var_occupationClass,TGTYPESCREENREG");
		var_occupationClass = getJsonData(var_CSVData , "$.records["+var_Count+"].occupationClass");
                 LOGGER.info("Executed Step = STORE,var_occupationClass,var_CSVData,$.records[{var_Count}].occupationClass,TGTYPESCREENREG");
		String var_colaPercentage;
                 LOGGER.info("Executed Step = VAR,String,var_colaPercentage,TGTYPESCREENREG");
		var_colaPercentage = getJsonData(var_CSVData , "$.records["+var_Count+"].colaPercentage");
                 LOGGER.info("Executed Step = STORE,var_colaPercentage,var_CSVData,$.records[{var_Count}].colaPercentage,TGTYPESCREENREG");
		String var_accidentBenefitMode;
                 LOGGER.info("Executed Step = VAR,String,var_accidentBenefitMode,TGTYPESCREENREG");
		var_accidentBenefitMode = getJsonData(var_CSVData , "$.records["+var_Count+"].accidentBenefitMode");
                 LOGGER.info("Executed Step = STORE,var_accidentBenefitMode,var_CSVData,$.records[{var_Count}].accidentBenefitMode,TGTYPESCREENREG");
		String var_accidentBenefitPeriod;
                 LOGGER.info("Executed Step = VAR,String,var_accidentBenefitPeriod,TGTYPESCREENREG");
		var_accidentBenefitPeriod = getJsonData(var_CSVData , "$.records["+var_Count+"].accidentBenefitPeriod");
                 LOGGER.info("Executed Step = STORE,var_accidentBenefitPeriod,var_CSVData,$.records[{var_Count}].accidentBenefitPeriod,TGTYPESCREENREG");
		String var_accidentEliminationPeriod;
                 LOGGER.info("Executed Step = VAR,String,var_accidentEliminationPeriod,TGTYPESCREENREG");
		var_accidentEliminationPeriod = getJsonData(var_CSVData , "$.records["+var_Count+"].accidentEliminationPeriod");
                 LOGGER.info("Executed Step = STORE,var_accidentEliminationPeriod,var_CSVData,$.records[{var_Count}].accidentEliminationPeriod,TGTYPESCREENREG");
		String var_sicknessBenefitMode;
                 LOGGER.info("Executed Step = VAR,String,var_sicknessBenefitMode,TGTYPESCREENREG");
		var_sicknessBenefitMode = getJsonData(var_CSVData , "$.records["+var_Count+"].sicknessBenefitMode");
                 LOGGER.info("Executed Step = STORE,var_sicknessBenefitMode,var_CSVData,$.records[{var_Count}].sicknessBenefitMode,TGTYPESCREENREG");
		String var_sicknessBenefitPeriod;
                 LOGGER.info("Executed Step = VAR,String,var_sicknessBenefitPeriod,TGTYPESCREENREG");
		var_sicknessBenefitPeriod = getJsonData(var_CSVData , "$.records["+var_Count+"].sicknessBenefitPeriod");
                 LOGGER.info("Executed Step = STORE,var_sicknessBenefitPeriod,var_CSVData,$.records[{var_Count}].sicknessBenefitPeriod,TGTYPESCREENREG");
		String var_sicknessEliminationPeriod;
                 LOGGER.info("Executed Step = VAR,String,var_sicknessEliminationPeriod,TGTYPESCREENREG");
		var_sicknessEliminationPeriod = getJsonData(var_CSVData , "$.records["+var_Count+"].sicknessEliminationPeriod");
                 LOGGER.info("Executed Step = STORE,var_sicknessEliminationPeriod,var_CSVData,$.records[{var_Count}].sicknessEliminationPeriod,TGTYPESCREENREG");
		String var_diBenefit;
                 LOGGER.info("Executed Step = VAR,String,var_diBenefit,TGTYPESCREENREG");
		var_diBenefit = getJsonData(var_CSVData , "$.records["+var_Count+"].diBenefit");
                 LOGGER.info("Executed Step = STORE,var_diBenefit,var_CSVData,$.records[{var_Count}].diBenefit,TGTYPESCREENREG");
		String var_filler2;
                 LOGGER.info("Executed Step = VAR,String,var_filler2,TGTYPESCREENREG");
		var_filler2 = getJsonData(var_CSVData , "$.records["+var_Count+"].filler2");
                 LOGGER.info("Executed Step = STORE,var_filler2,var_CSVData,$.records[{var_Count}].filler2,TGTYPESCREENREG");
		String var_ltcBeginningDate;
                 LOGGER.info("Executed Step = VAR,String,var_ltcBeginningDate,TGTYPESCREENREG");
		var_ltcBeginningDate = getJsonData(var_CSVData , "$.records["+var_Count+"].ltcBeginningDate");
                 LOGGER.info("Executed Step = STORE,var_ltcBeginningDate,var_CSVData,$.records[{var_Count}].ltcBeginningDate,TGTYPESCREENREG");
		String var_ltcBenefitAccount;
                 LOGGER.info("Executed Step = VAR,String,var_ltcBenefitAccount,TGTYPESCREENREG");
		var_ltcBenefitAccount = getJsonData(var_CSVData , "$.records["+var_Count+"].ltcBenefitAccount");
                 LOGGER.info("Executed Step = STORE,var_ltcBenefitAccount,var_CSVData,$.records[{var_Count}].ltcBenefitAccount,TGTYPESCREENREG");
		String var_ltcIncreaseOption;
                 LOGGER.info("Executed Step = VAR,String,var_ltcIncreaseOption,TGTYPESCREENREG");
		var_ltcIncreaseOption = getJsonData(var_CSVData , "$.records["+var_Count+"].ltcIncreaseOption");
                 LOGGER.info("Executed Step = STORE,var_ltcIncreaseOption,var_CSVData,$.records[{var_Count}].ltcIncreaseOption,TGTYPESCREENREG");
		String var_ltcIncreasePercentage;
                 LOGGER.info("Executed Step = VAR,String,var_ltcIncreasePercentage,TGTYPESCREENREG");
		var_ltcIncreasePercentage = getJsonData(var_CSVData , "$.records["+var_Count+"].ltcIncreasePercentage");
                 LOGGER.info("Executed Step = STORE,var_ltcIncreasePercentage,var_CSVData,$.records[{var_Count}].ltcIncreasePercentage,TGTYPESCREENREG");
		String var_ltcBenefitPeriodMode;
                 LOGGER.info("Executed Step = VAR,String,var_ltcBenefitPeriodMode,TGTYPESCREENREG");
		var_ltcBenefitPeriodMode = getJsonData(var_CSVData , "$.records["+var_Count+"].ltcBenefitPeriodMode");
                 LOGGER.info("Executed Step = STORE,var_ltcBenefitPeriodMode,var_CSVData,$.records[{var_Count}].ltcBenefitPeriodMode,TGTYPESCREENREG");
		String var_ltcBenefitPeriod;
                 LOGGER.info("Executed Step = VAR,String,var_ltcBenefitPeriod,TGTYPESCREENREG");
		var_ltcBenefitPeriod = getJsonData(var_CSVData , "$.records["+var_Count+"].ltcBenefitPeriod");
                 LOGGER.info("Executed Step = STORE,var_ltcBenefitPeriod,var_CSVData,$.records[{var_Count}].ltcBenefitPeriod,TGTYPESCREENREG");
		String var_filler3;
                 LOGGER.info("Executed Step = VAR,String,var_filler3,TGTYPESCREENREG");
		var_filler3 = getJsonData(var_CSVData , "$.records["+var_Count+"].filler3");
                 LOGGER.info("Executed Step = STORE,var_filler3,var_CSVData,$.records[{var_Count}].filler3,TGTYPESCREENREG");
		String var_suspendedCoverPercentage;
                 LOGGER.info("Executed Step = VAR,String,var_suspendedCoverPercentage,TGTYPESCREENREG");
		var_suspendedCoverPercentage = getJsonData(var_CSVData , "$.records["+var_Count+"].suspendedCoverPercentage");
                 LOGGER.info("Executed Step = STORE,var_suspendedCoverPercentage,var_CSVData,$.records[{var_Count}].suspendedCoverPercentage,TGTYPESCREENREG");
		String var_pmlCvd100Adds;
                 LOGGER.info("Executed Step = VAR,String,var_pmlCvd100Adds,TGTYPESCREENREG");
		var_pmlCvd100Adds = getJsonData(var_CSVData , "$.records["+var_Count+"].pml_CV_D100_ADDS");
                 LOGGER.info("Executed Step = STORE,var_pmlCvd100Adds,var_CSVData,$.records[{var_Count}].pml_CV_D100_ADDS,TGTYPESCREENREG");
		String var_pmlLastAcctngStored;
                 LOGGER.info("Executed Step = VAR,String,var_pmlLastAcctngStored,TGTYPESCREENREG");
		var_pmlLastAcctngStored = getJsonData(var_CSVData , "$.records["+var_Count+"].pml_LAST_ACCTNG_STORED");
                 LOGGER.info("Executed Step = STORE,var_pmlLastAcctngStored,var_CSVData,$.records[{var_Count}].pml_LAST_ACCTNG_STORED,TGTYPESCREENREG");
		String var_pmlGmdbrType;
                 LOGGER.info("Executed Step = VAR,String,var_pmlGmdbrType,TGTYPESCREENREG");
		var_pmlGmdbrType = getJsonData(var_CSVData , "$.records["+var_Count+"].pml_GMDBR_TYPE");
                 LOGGER.info("Executed Step = STORE,var_pmlGmdbrType,var_CSVData,$.records[{var_Count}].pml_GMDBR_TYPE,TGTYPESCREENREG");
		String var_pmlRdrProcess;
                 LOGGER.info("Executed Step = VAR,String,var_pmlRdrProcess,TGTYPESCREENREG");
		var_pmlRdrProcess = getJsonData(var_CSVData , "$.records["+var_Count+"].pml_RDR_PROCESS");
                 LOGGER.info("Executed Step = STORE,var_pmlRdrProcess,var_CSVData,$.records[{var_Count}].pml_RDR_PROCESS,TGTYPESCREENREG");
		String var_pmlGdbrOvedLapseSw;
                 LOGGER.info("Executed Step = VAR,String,var_pmlGdbrOvedLapseSw,TGTYPESCREENREG");
		var_pmlGdbrOvedLapseSw = getJsonData(var_CSVData , "$.records["+var_Count+"].pml_GDBR_OVRD_LAPSE_SW");
                 LOGGER.info("Executed Step = STORE,var_pmlGdbrOvedLapseSw,var_CSVData,$.records[{var_Count}].pml_GDBR_OVRD_LAPSE_SW,TGTYPESCREENREG");
		String var_pmlGdbrGraceInd;
                 LOGGER.info("Executed Step = VAR,String,var_pmlGdbrGraceInd,TGTYPESCREENREG");
		var_pmlGdbrGraceInd = getJsonData(var_CSVData , "$.records["+var_Count+"].pml_GDBR_GRACE_IND");
                 LOGGER.info("Executed Step = STORE,var_pmlGdbrGraceInd,var_CSVData,$.records[{var_Count}].pml_GDBR_GRACE_IND,TGTYPESCREENREG");
		String var_pmlGdbrLapseAcctValue;
                 LOGGER.info("Executed Step = VAR,String,var_pmlGdbrLapseAcctValue,TGTYPESCREENREG");
		var_pmlGdbrLapseAcctValue = getJsonData(var_CSVData , "$.records["+var_Count+"].pml_GDBR_LAPSE_ACCT_VALUE");
                 LOGGER.info("Executed Step = STORE,var_pmlGdbrLapseAcctValue,var_CSVData,$.records[{var_Count}].pml_GDBR_LAPSE_ACCT_VALUE,TGTYPESCREENREG");
		String var_pmlGdbarAccumLapsePrem;
                 LOGGER.info("Executed Step = VAR,String,var_pmlGdbarAccumLapsePrem,TGTYPESCREENREG");
		var_pmlGdbarAccumLapsePrem = getJsonData(var_CSVData , "$.records["+var_Count+"].pml_GDBR_ACCUM_LAPSE_PREM");
                 LOGGER.info("Executed Step = STORE,var_pmlGdbarAccumLapsePrem,var_CSVData,$.records[{var_Count}].pml_GDBR_ACCUM_LAPSE_PREM,TGTYPESCREENREG");
		String var_pmlGdbrAccumPrem;
                 LOGGER.info("Executed Step = VAR,String,var_pmlGdbrAccumPrem,TGTYPESCREENREG");
		var_pmlGdbrAccumPrem = getJsonData(var_CSVData , "$.records["+var_Count+"].pml_GDBR_ACCUM_PREM");
                 LOGGER.info("Executed Step = STORE,var_pmlGdbrAccumPrem,var_CSVData,$.records[{var_Count}].pml_GDBR_ACCUM_PREM,TGTYPESCREENREG");
		String var_pmlGdbrCommPrem;
                 LOGGER.info("Executed Step = VAR,String,var_pmlGdbrCommPrem,TGTYPESCREENREG");
		var_pmlGdbrCommPrem = getJsonData(var_CSVData , "$.records["+var_Count+"].pml_GDBR_COMM_PREM");
                 LOGGER.info("Executed Step = STORE,var_pmlGdbrCommPrem,var_CSVData,$.records[{var_Count}].pml_GDBR_COMM_PREM,TGTYPESCREENREG");
		String var_pmlGdbrMinPrem;
                 LOGGER.info("Executed Step = VAR,String,var_pmlGdbrMinPrem,TGTYPESCREENREG");
		var_pmlGdbrMinPrem = getJsonData(var_CSVData , "$.records["+var_Count+"].pml_GDBR_MIN_PREM");
                 LOGGER.info("Executed Step = STORE,var_pmlGdbrMinPrem,var_CSVData,$.records[{var_Count}].pml_GDBR_MIN_PREM,TGTYPESCREENREG");
		String var_pmlCvoverDivover;
                 LOGGER.info("Executed Step = VAR,String,var_pmlCvoverDivover,TGTYPESCREENREG");
		var_pmlCvoverDivover = getJsonData(var_CSVData , "$.records["+var_Count+"].pml_CVOVER_DIVOVER");
                 LOGGER.info("Executed Step = STORE,var_pmlCvoverDivover,var_CSVData,$.records[{var_Count}].pml_CVOVER_DIVOVER,TGTYPESCREENREG");
		String var_pmlAtoTdoPrem;
                 LOGGER.info("Executed Step = VAR,String,var_pmlAtoTdoPrem,TGTYPESCREENREG");
		var_pmlAtoTdoPrem = getJsonData(var_CSVData , "$.records["+var_Count+"].pml_ATP_TDO_PREM");
                 LOGGER.info("Executed Step = STORE,var_pmlAtoTdoPrem,var_CSVData,$.records[{var_Count}].pml_ATP_TDO_PREM,TGTYPESCREENREG");
		String var_pmlClosedPlanSwsav;
                 LOGGER.info("Executed Step = VAR,String,var_pmlClosedPlanSwsav,TGTYPESCREENREG");
		var_pmlClosedPlanSwsav = getJsonData(var_CSVData , "$.records["+var_Count+"].pml_CLOSED_PLAN_SW");
                 LOGGER.info("Executed Step = STORE,var_pmlClosedPlanSwsav,var_CSVData,$.records[{var_Count}].pml_CLOSED_PLAN_SW,TGTYPESCREENREG");
		String var_pmlImacCovForm;
                 LOGGER.info("Executed Step = VAR,String,var_pmlImacCovForm,TGTYPESCREENREG");
		var_pmlImacCovForm = getJsonData(var_CSVData , "$.records["+var_Count+"].pml_IMAC_COV_FORM");
                 LOGGER.info("Executed Step = STORE,var_pmlImacCovForm,var_CSVData,$.records[{var_Count}].pml_IMAC_COV_FORM,TGTYPESCREENREG");
		String var_pmlImacCovB4m;
                 LOGGER.info("Executed Step = VAR,String,var_pmlImacCovB4m,TGTYPESCREENREG");
		var_pmlImacCovB4m = getJsonData(var_CSVData , "$.records["+var_Count+"].pml_IMAC_COV_B4M");
                 LOGGER.info("Executed Step = STORE,var_pmlImacCovB4m,var_CSVData,$.records[{var_Count}].pml_IMAC_COV_B4M,TGTYPESCREENREG");
		String var_pmlNextRider;
                 LOGGER.info("Executed Step = VAR,String,var_pmlNextRider,TGTYPESCREENREG");
		var_pmlNextRider = getJsonData(var_CSVData , "$.records["+var_Count+"].pml_NEXT_RIDER");
                 LOGGER.info("Executed Step = STORE,var_pmlNextRider,var_CSVData,$.records[{var_Count}].pml_NEXT_RIDER,TGTYPESCREENREG");
		String var_pmlReinsAgreement;
                 LOGGER.info("Executed Step = VAR,String,var_pmlReinsAgreement,TGTYPESCREENREG");
		var_pmlReinsAgreement = getJsonData(var_CSVData , "$.records["+var_Count+"].pml_REINS_AGREEMENT");
                 LOGGER.info("Executed Step = STORE,var_pmlReinsAgreement,var_CSVData,$.records[{var_Count}].pml_REINS_AGREEMENT,TGTYPESCREENREG");
		String var_pmlCovRels;
                 LOGGER.info("Executed Step = VAR,String,var_pmlCovRels,TGTYPESCREENREG");
		var_pmlCovRels = getJsonData(var_CSVData , "$.records["+var_Count+"].pml_COV_REL");
                 LOGGER.info("Executed Step = STORE,var_pmlCovRels,var_CSVData,$.records[{var_Count}].pml_COV_REL,TGTYPESCREENREG");
		String var_pmlPolFee;
                 LOGGER.info("Executed Step = VAR,String,var_pmlPolFee,TGTYPESCREENREG");
		var_pmlPolFee = getJsonData(var_CSVData , "$.records["+var_Count+"].pml_POL_FEE");
                 LOGGER.info("Executed Step = STORE,var_pmlPolFee,var_CSVData,$.records[{var_Count}].pml_POL_FEE,TGTYPESCREENREG");
		String var_pmlUnitPrem;
                 LOGGER.info("Executed Step = VAR,String,var_pmlUnitPrem,TGTYPESCREENREG");
		var_pmlUnitPrem = getJsonData(var_CSVData , "$.records["+var_Count+"].pml_UNIT_PREM");
                 LOGGER.info("Executed Step = STORE,var_pmlUnitPrem,var_CSVData,$.records[{var_Count}].pml_UNIT_PREM,TGTYPESCREENREG");
		String var_pmlAddsCvSwsa;
                 LOGGER.info("Executed Step = VAR,String,var_pmlAddsCvSwsa,TGTYPESCREENREG");
		var_pmlAddsCvSwsa = getJsonData(var_CSVData , "$.records["+var_Count+"].pml_ADDS_CV_SW");
                 LOGGER.info("Executed Step = STORE,var_pmlAddsCvSwsa,var_CSVData,$.records[{var_Count}].pml_ADDS_CV_SW,TGTYPESCREENREG");
		String var_pmlLfpAmt;
                 LOGGER.info("Executed Step = VAR,String,var_pmlLfpAmt,TGTYPESCREENREG");
		var_pmlLfpAmt = getJsonData(var_CSVData , "$.records["+var_Count+"].pml_LFP_AMT");
                 LOGGER.info("Executed Step = STORE,var_pmlLfpAmt,var_CSVData,$.records[{var_Count}].pml_LFP_AMT,TGTYPESCREENREG");
		String var_pmlAtpTdoAmtsa;
                 LOGGER.info("Executed Step = VAR,String,var_pmlAtpTdoAmtsa,TGTYPESCREENREG");
		var_pmlAtpTdoAmtsa = getJsonData(var_CSVData , "$.records["+var_Count+"].pml_ATP_TDO_AMT");
                 LOGGER.info("Executed Step = STORE,var_pmlAtpTdoAmtsa,var_CSVData,$.records[{var_Count}].pml_ATP_TDO_AMT,TGTYPESCREENREG");
		String var_pmlPuiAmt;
                 LOGGER.info("Executed Step = VAR,String,var_pmlPuiAmt,TGTYPESCREENREG");
		var_pmlPuiAmt = getJsonData(var_CSVData , "$.records["+var_Count+"].pml_PUI_AMT");
                 LOGGER.info("Executed Step = STORE,var_pmlPuiAmt,var_CSVData,$.records[{var_Count}].pml_PUI_AMT,TGTYPESCREENREG");
		String var_pmlPuoAmt;
                 LOGGER.info("Executed Step = VAR,String,var_pmlPuoAmt,TGTYPESCREENREG");
		var_pmlPuoAmt = getJsonData(var_CSVData , "$.records["+var_Count+"].pml_PUO_AMT");
                 LOGGER.info("Executed Step = STORE,var_pmlPuoAmt,var_CSVData,$.records[{var_Count}].pml_PUO_AMT,TGTYPESCREENREG");
		String var_pmlPermAddssa;
                 LOGGER.info("Executed Step = VAR,String,var_pmlPermAddssa,TGTYPESCREENREG");
		var_pmlPermAddssa = getJsonData(var_CSVData , "$.records["+var_Count+"].pml_PERM_ADDS");
                 LOGGER.info("Executed Step = STORE,var_pmlPermAddssa,var_CSVData,$.records[{var_Count}].pml_PERM_ADDS,TGTYPESCREENREG");
		String var_pmlOldCo;
                 LOGGER.info("Executed Step = VAR,String,var_pmlOldCo,TGTYPESCREENREG");
		var_pmlOldCo = getJsonData(var_CSVData , "$.records["+var_Count+"].pml_OLD_CO");
                 LOGGER.info("Executed Step = STORE,var_pmlOldCo,var_CSVData,$.records[{var_Count}].pml_OLD_CO,TGTYPESCREENREG");
		String var_pmlValExtraCd;
                 LOGGER.info("Executed Step = VAR,String,var_pmlValExtraCd,TGTYPESCREENREG");
		var_pmlValExtraCd = getJsonData(var_CSVData , "$.records["+var_Count+"].pml_VAL_EXTRA_CD");
                 LOGGER.info("Executed Step = STORE,var_pmlValExtraCd,var_CSVData,$.records[{var_Count}].pml_VAL_EXTRA_CD,TGTYPESCREENREG");
		String var_groupId2;
                 LOGGER.info("Executed Step = VAR,String,var_groupId2,TGTYPESCREENREG");
		var_groupId2 = getJsonData(var_CSVData , "$.records["+var_Count+"].group_Id");
                 LOGGER.info("Executed Step = STORE,var_groupId2,var_CSVData,$.records[{var_Count}].group_Id,TGTYPESCREENREG");
		String var_filter4;
                 LOGGER.info("Executed Step = VAR,String,var_filter4,TGTYPESCREENREG");
		var_filter4 = getJsonData(var_CSVData , "$.records["+var_Count+"].filter_4");
                 LOGGER.info("Executed Step = STORE,var_filter4,var_CSVData,$.records[{var_Count}].filter_4,TGTYPESCREENREG");
        CALL.$("TC001VariableForDatabse","TGTYPESCREENREG");

		int var_costBasisBaseRiderCodeFromDB = 0;
                 LOGGER.info("Executed Step = VAR,Integer,var_costBasisBaseRiderCodeFromDB,0,TGTYPESCREENREG");
		String var_clientNumberFromDB = "null";
                 LOGGER.info("Executed Step = VAR,String,var_clientNumberFromDB,null,TGTYPESCREENREG");
		String var_FundedBenefitFromDB = "null";
                 LOGGER.info("Executed Step = VAR,String,var_FundedBenefitFromDB,null,TGTYPESCREENREG");
		String var_PaidToDateFromDB = "null";
                 LOGGER.info("Executed Step = VAR,String,var_PaidToDateFromDB,null,TGTYPESCREENREG");
		String var_faceAmountForDeathBenefit = "nul";
                 LOGGER.info("Executed Step = VAR,String,var_faceAmountForDeathBenefit,nul,TGTYPESCREENREG");
		String var_DeathBenefitOptionDeathBenefit = "null";
                 LOGGER.info("Executed Step = VAR,String,var_DeathBenefitOptionDeathBenefit,null,TGTYPESCREENREG");
		int var_uniqueIDFrompos224s1 = 0;
                 LOGGER.info("Executed Step = VAR,Integer,var_uniqueIDFrompos224s1,0,TGTYPESCREENREG");
		int var_uniqueIDFrompos210s1 = 0;
                 LOGGER.info("Executed Step = VAR,Integer,var_uniqueIDFrompos210s1,0,TGTYPESCREENREG");
		String var_specialClassUnisexRatesAllowed = "null";
                 LOGGER.info("Executed Step = VAR,String,var_specialClassUnisexRatesAllowed,null,TGTYPESCREENREG");
		try { 
		 		 			var_policyResult = "PASS";
							var_failedResults = "Fields = ";
							var_apiResult = "FAIL";

							try {
								URL url = new URL("http://cis-coreapd1.btoins.ibm.com:17000/rest/pageapi/pos200s1/loadData");
								HttpURLConnection con = (HttpURLConnection) url.openConnection();
								con.setRequestMethod("POST");
								con.setRequestProperty("Content-Type", "application/json; utf-8");
								con.setRequestProperty("Accept", "application/json");
								String loginPassword = "autoapi" + ":" + "Autoapi!23";
								String encoded = new sun.misc.BASE64Encoder().encode(loginPassword.getBytes());
								con.setRequestProperty("Authorization", "Basic " + encoded);
								con.setDoOutput(true);
								JSONObject param = new JSONObject();
								param.put("policyNumber", var_policyNumber);
								JSONObject session = new JSONObject();
								session.put("currentCompany", "CO1");

								String jsonInputString = new JSONObject()
										.put("param", param)
										.put("session", session)
										.toString();

								System.out.println(jsonInputString);
								try (OutputStream os = con.getOutputStream()) {
									byte[] input = jsonInputString.getBytes("utf-8");
									os.write(input, 0, input.length);
								}
								try (BufferedReader br = new BufferedReader(
										new InputStreamReader(con.getInputStream(), "utf-8"))) {
									StringBuilder response = new StringBuilder();
									String responseLine = null;
									while ((responseLine = br.readLine()) != null) {

										response.append(responseLine.trim());
									}
									System.out.println("pos200s1 ===== " + response.toString());

									var_apiResult = "PASS";
									JSONObject response1 = new JSONObject(response.toString());

									JSONObject objData = response1.getJSONObject("data");

									String PolicyNumber1 = objData.getString("policyNumber");
									System.out.println("PolicyNumber1 ==== : " + PolicyNumber1);
									LOGGER.info("PolicyNumber1 ==== : " + PolicyNumber1);

									if (PolicyNumber1.contains(var_policyNumber)) {

										writeToCSV("PolicyNumber", var_policyNumber);
									} else {
										var_policyResult = "FAIL";
										var_failedResults = var_failedResults + "PolicyNumber = " + PolicyNumber1 + " ";
										writeToCSV("PolicyNumber", var_policyNumber);
									}

									Integer costBasisBaseRiderCodeFromDB = objData.getInt("costBasisBaseRiderCode");
									var_costBasisBaseRiderCodeFromDB = costBasisBaseRiderCodeFromDB;

									String costBasisBaseRiderCode1 = String.valueOf(costBasisBaseRiderCodeFromDB + 1);
									System.out.println("costBasisBaseRiderCode1 ==== : " + costBasisBaseRiderCode1);
									LOGGER.info("costBasisBaseRiderCode1 ==== : " + costBasisBaseRiderCode1);

									if (var_baseriderSequence.contains(costBasisBaseRiderCode1)) {

										writeToCSV("BaseRiderSequence", var_baseriderSequence);
									} else {
										var_policyResult = "FAIL";
										var_failedResults = var_failedResults + ", BaseRiderSequence = " + costBasisBaseRiderCode1 + " ";
										writeToCSV("BaseRiderSequence", var_baseriderSequence);
									}


									String statusFromDB = objData.getString("status");
									String BillingModeFromDB = objData.getString("paymentMethod");
									String status1 = " ";

									if (statusFromDB.contains("I")) {
										status1 = "PMP";

									} else if (statusFromDB.contains("C")) {
										status1 = "NTO";
									} else if (statusFromDB.contains("I") && BillingModeFromDB.contains("WP")) {
										status1 = "WOP";
									} else if (statusFromDB.contains("E")) {
										status1 = "ETI";
									} else if (statusFromDB.contains("R")) {
										status1 = "RPU";
									} else if (statusFromDB.contains("P")) {
										status1 = "PDU";
									} else if (statusFromDB.contains("N")) {
										status1 = "NTO";
									} else if (statusFromDB.contains("L")) {
										status1 = "LAP";
									} else if (statusFromDB.contains("D")) {
										status1 = "DTH";
									} else if (statusFromDB.contains("S")) {
										status1 = "SUR";
									} else if (statusFromDB.contains("X")) {
										status1 = "EXP";
									} else if (statusFromDB.contains("M")) {
										status1 = "MAT";
									} else if (statusFromDB.contains("T")) {
										status1 = "TRM";
									}

									System.out.println("status1 ==== : " + status1);
									LOGGER.info("status1 ==== : " + status1);


									if (var_policyStatus.contains(status1)) {

										writeToCSV("PolicyStatus", var_policyStatus);
									} else {
										var_policyResult = "FAIL";
										var_failedResults = var_failedResults + " , PolicyStatus = " + status1 + " ";
										writeToCSV("PolicyStatus", var_policyStatus);
									}


									String paidToDate1 = objData.getString("paidToDate").replaceAll("-", "");
									System.out.println("paidToDate1 ==== : " + paidToDate1);
									LOGGER.info("paidToDate1 ==== : " + paidToDate1);
									if (var_paidToDate.contains(paidToDate1)) {

										writeToCSV("paidToDate", var_paidToDate);
									} else {
										var_policyResult = "FAIL";
										var_failedResults = var_failedResults + " , paidToDate = " + paidToDate1 + " ";
										writeToCSV("paidToDate", var_paidToDate);
									}

									if (var_issueType.contains("")) {

										writeToCSV("Issue Type", var_issueType);
									} else {
										var_policyResult = "FAIL";
										var_failedResults = var_failedResults + " , Issue Type  = ";
										writeToCSV("Issue Type", var_issueType);
									}


									String ownerCityStateZip1 = objData.getString("ownerCityStateZip");
									System.out.println("ownerCityStateZip1 ==== : " + ownerCityStateZip1);
									LOGGER.info("ownerCityStateZip1 ==== : " + ownerCityStateZip1);
									if (ownerCityStateZip1.contains(var_ownerState)) {

										writeToCSV("OwnerState", var_ownerState);
									} else {
										var_policyResult = "FAIL";
										var_failedResults = var_failedResults + " , OwnerState = " + ownerCityStateZip1 + " ";
										writeToCSV("OwnerState", var_ownerState);
									}


									String lastDividendDate1 = objData.getString("lastDividendDate").replaceAll("-", "");
									String Year1 = getSubStringFromString(lastDividendDate1, 1, 4);
									String Month1 = getSubStringFromString(lastDividendDate1, 5, 6);
									String Date1 = getSubStringFromString(lastDividendDate1, 7, 8);
									var_PaidToDateFromDB = lastDividendDate1;

									if (lastDividendDate1.equals("00000000")) {

										var_PaidToDateFromDB = paidToDate1;
										Year1 = getSubStringFromString(paidToDate1, 1, 4);
										Month1 = getSubStringFromString(paidToDate1, 5, 6);
										Date1 = getSubStringFromString(paidToDate1, 7, 8);
									}


								}
							}catch (Exception e){
								var_policyResult = "FAIL";
								writeToCSV("PolicyNumber", var_policyNumber);
								writeToCSV("BaseRiderSequence", var_baseriderSequence);
								writeToCSV("PolicyStatus", var_policyStatus);
								writeToCSV("paidToDate", var_paidToDate);
								writeToCSV("Issue Type", var_issueType);
								writeToCSV("OwnerState", var_ownerState);
								var_failedResults = var_failedResults  + "API is not working for this policy";

								PrintVal("Renderer Script Exception : "+e.toString());
							}


						
				 
				 
		 
		 ;} 
		 catch(Exception e) { PrintVal("Renderer Script Exception : "+e.toString()); } 
		 PrintVal("Executed Step = CALLRENDERERSCRIPT,GETPOLICYDATAAPI_POS200S1"); 
		try { 
		 		 			var_apiResult = "FAIL";

				 			try {
								URL url = new URL("http://cis-coreapd1.btoins.ibm.com:17000/rest/pageapi/pos210s1/loadData");
								HttpURLConnection con = (HttpURLConnection) url.openConnection();
								con.setRequestMethod("POST");
								con.setRequestProperty("Content-Type", "application/json; utf-8");
								con.setRequestProperty("Accept", "application/json");
								String loginPassword = "autoapi" + ":" + "Autoapi!23";
								String encoded = new sun.misc.BASE64Encoder().encode(loginPassword.getBytes());
								con.setRequestProperty("Authorization", "Basic " + encoded);
								con.setDoOutput(true);

								JSONObject param = new JSONObject();
								param.put("policyNumber", var_policyNumber);
								param.put("mode", "display");
								param.put("uniqueId", 101);

								JSONObject session = new JSONObject();
								session.put("currentCompany", "CO1");

								String jsonInputString = new JSONObject()
										.put("param", param)
										.put("session", session)
										.toString();


								System.out.println(jsonInputString);
								try (OutputStream os = con.getOutputStream()) {
									byte[] input = jsonInputString.getBytes("utf-8");
									os.write(input, 0, input.length);
								}
								try (BufferedReader br = new BufferedReader(
										new InputStreamReader(con.getInputStream(), "utf-8"))) {
									StringBuilder response = new StringBuilder();
									String responseLine = null;
									while ((responseLine = br.readLine()) != null) {

										response.append(responseLine.trim());
									}
									LOGGER.info("pos210s1 =====" + response.toString());

									JSONObject response1 = new JSONObject(response.toString());
									JSONObject objData = response1.getJSONObject("data");

									var_apiResult = "PASS";

									String modalPremium1 = String.valueOf((int) objData.getDouble("modalPremium"));
									System.out.println("lastPaymentAmount1 ==== : " + modalPremium1);
									LOGGER.info("lastPaymentAmount1 ==== : " + modalPremium1);
									if (var_premiumWaived.contains(modalPremium1)) {
										writeToCSV("PremiumWaived", var_premiumWaived);
									} else {
										var_policyResult = "FAIL";
										var_failedResults = var_failedResults + " , PremiumWaived = " + modalPremium1 + " ";
										writeToCSV("PremiumWaived", var_premiumWaived);
									}

									String settleDate1 = objData.getString("settleDate").replaceAll("-", "");
									System.out.println("settleDate1 ==== : " + settleDate1);
									LOGGER.info("settleDate1 ==== : " + settleDate1);
									if (settleDate1.contains(var_addedToFileDate)) {
										writeToCSV("SettleDate", var_addedToFileDate);
									} else {
										var_policyResult = "FAIL";
										var_failedResults = var_failedResults + " , SettleDate = " + settleDate1 + " ";
										writeToCSV("SettleDate", var_addedToFileDate);
									}

									String nextMAVDate1 = objData.getString("nextMAVDate").replaceAll("-", "");
									var_PaidToDateFromDB = nextMAVDate1;


									String effectiveDate1 = "";

									String terminationDate1 = objData.getString("terminationDate").replaceAll("-", "");
									if (terminationDate1.contains("00000000")) {
										effectiveDate1 = objData.getString("effectiveDate").replaceAll("-", "");
									} else {
										effectiveDate1 = terminationDate1;
									}

									System.out.println("effectiveDate1 ==== : " + effectiveDate1);
									LOGGER.info("effectiveDate1 ==== : " + effectiveDate1);
									System.out.println("effectiveDate1 From Json ==== : " + var_statusChangeDate);
									LOGGER.info("effectiveDate1 From Json ==== : " + var_statusChangeDate);

									if (effectiveDate1.contains(var_statusChangeDate)) {
										writeToCSV("EffectiveDate", var_statusChangeDate);
									} else {
										var_policyResult = "FAIL";
										var_failedResults = var_failedResults + " , EffectiveDate =" + effectiveDate1 + " ";
										writeToCSV("EffectiveDate", var_statusChangeDate);
									}


									String issueState11 = objData.getString("stateOfIssue");
									System.out.println("issueState11 ==== : " + issueState11);
									LOGGER.info("issueState11 ==== : " + issueState11);
									if (issueState11.contains(var_issueState)) {
										writeToCSV("IssueState", var_issueState);
									} else {
										var_policyResult = "FAIL";
										var_failedResults = var_failedResults + " , IssueState = " + issueState11 + "";
										writeToCSV("IssueState", var_issueState);
									}


									String PolicyFees11 = String.valueOf((int) objData.getDouble("annualPolicyFee"));
									System.out.println("PolicyFees11 ==== : " + PolicyFees11);
									LOGGER.info("PolicyFees11 ==== : " + PolicyFees11);
									if (var_policyFee.contains(PolicyFees11)) {
										writeToCSV("Policy Fees", var_policyFee);
									} else {
										var_policyResult = "FAIL";
										var_failedResults = var_failedResults + " , Policy Fees = " + PolicyFees11 + "";
										writeToCSV("Policy Fees", var_policyFee);
									}


									var_uniqueIDFrompos210s1 = objData.getInt("uniqueID");


								}
							}catch(Exception e){

								writeToCSV("PremiumWaived", var_premiumWaived);
								writeToCSV("SettleDate", var_addedToFileDate);
								writeToCSV("EffectiveDate", var_statusChangeDate);
								writeToCSV("IssueState", var_issueState);
								writeToCSV("Policy Fees", var_policyFee);
								// var_failedResults = var_failedResults  + " , API POS210S1 is not working";

								PrintVal("Renderer Script Exception : "+e.toString());
							}


						
				 
				 
		 
		 ;} 
		 catch(Exception e) { PrintVal("Renderer Script Exception : "+e.toString()); } 
		 PrintVal("Executed Step = CALLRENDERERSCRIPT,GETPOLICYDATAAPI_POS210S1"); 
		try { 
		 

					try{
				 			var_apiResult = "FAIL";
							URL url = new URL ("http://cis-coreapd1.btoins.ibm.com:17000/rest/pageapi/pos230s1/loadData");
							HttpURLConnection con = (HttpURLConnection)url.openConnection();
							con.setRequestMethod("POST");
							con.setRequestProperty("Content-Type", "application/json; utf-8");
							con.setRequestProperty("Accept", "application/json");
							String loginPassword = "autoapi"+ ":" + "Autoapi!23";
							String encoded = new sun.misc.BASE64Encoder().encode (loginPassword.getBytes());
							con.setRequestProperty ("Authorization", "Basic " + encoded);
							con.setDoOutput(true);
							JSONObject param = new JSONObject();
							param.put("policyNumber",var_policyNumber);
							param.put("uniqueId",111);
							JSONObject session = new JSONObject();
							session.put("currentCompany","CO1");
							session.put("sessionDate","2021-04-16");
							String jsonInputString = new JSONObject()
									.put("param",param )
									.put("session",session)
									.toString();



							System.out.println(jsonInputString);
							try(OutputStream os = con.getOutputStream()) {
								byte[] input = jsonInputString.getBytes("utf-8");
								os.write(input, 0, input.length);
							}
							try(BufferedReader br = new BufferedReader(
									new InputStreamReader(con.getInputStream(), "utf-8"))) {
								StringBuilder response = new StringBuilder();
								String responseLine = null;
								while ((responseLine = br.readLine()) != null) {

									response.append(responseLine.trim());
								}
								System.out.println("pos230s1 ===== "+response.toString());
								//LOGGER.info("BBBB=====" +response.toString());

								var_apiResult = "PASS";
								JSONObject response1 = new JSONObject(response.toString());

								JSONObject objData =  response1.getJSONObject("data");




							}

					}catch(Exception e){


						PrintVal("Renderer Script Exception : "+e.toString());
					}

						
				 
				 
		 
		 ;} 
		 catch(Exception e) { PrintVal("Renderer Script Exception : "+e.toString()); } 
		 PrintVal("Executed Step = CALLRENDERERSCRIPT,GETPOLICYDATAAPI_POS230S1"); 
		try { 
		 		 			var_apiResult = "FAIL";
				 			try {
								URL url = new URL("http://cis-coreapd1.btoins.ibm.com:17000/rest/pageapi/pdf100s02/loadData");
								HttpURLConnection con = (HttpURLConnection) url.openConnection();
								con.setRequestMethod("POST");
								con.setRequestProperty("Content-Type", "application/json; utf-8");
								con.setRequestProperty("Accept", "application/json");
								String loginPassword = "autoapi" + ":" + "Autoapi!23";
								String encoded = new sun.misc.BASE64Encoder().encode(loginPassword.getBytes());
								con.setRequestProperty("Authorization", "Basic " + encoded);
								con.setDoOutput(true);
								JSONObject param = new JSONObject();
								param.put("policyNumber", var_policyNumber);
								param.put("addProgress", "");
								param.put("pendingChanges", false);
								param.put("uniqueId", 999);
								param.put("planCode", var_planCode);
								String jsonInputString = new JSONObject()
										.put("param", param)
										.toString();


								System.out.println(jsonInputString);
								try (OutputStream os = con.getOutputStream()) {
									byte[] input = jsonInputString.getBytes("utf-8");
									os.write(input, 0, input.length);
								}
								try (BufferedReader br = new BufferedReader(
										new InputStreamReader(con.getInputStream(), "utf-8"))) {
									StringBuilder response = new StringBuilder();
									String responseLine = null;
									while ((responseLine = br.readLine()) != null) {

										response.append(responseLine.trim());
									}
									// System.out.println("pdf100s02 ===== "+response.toString());
									LOGGER.info("pdf100s02 =====" + response.toString());
									var_apiResult = "PASS";
									JSONObject response1 = new JSONObject(response.toString());
									JSONObject objData = response1.getJSONObject("data");

									String planCode1 = objData.getString("planCode");
									System.out.println("planCode1 ==== : " + planCode1);
									LOGGER.info("planCode1 ==== : " + planCode1);
									if (var_planCode.contains(planCode1)) {
										writeToCSV("PlanCode", var_planCode);
									} else {
										var_policyResult = "FAIL";
										var_failedResults = var_failedResults + " , PlanCode = " + planCode1 + " ";
										writeToCSV("PlanCode", var_planCode);
									}

									String participatingPlan1 = objData.getString("participatingPlan");
									System.out.println("participating Plan1 ==== : " + participatingPlan1);
									LOGGER.info("participating Plan1 ==== : " + participatingPlan1);
									if (participatingPlan1.equals("N")) {
										participatingPlan1 = "N";
									} else {
										participatingPlan1 = "P";
									}
									if (var_participatingIndicator.contains(participatingPlan1)) {
										writeToCSV("Participating Indicator", var_participatingIndicator);
									} else {
										var_policyResult = "FAIL";
										var_failedResults = var_failedResults + " , Participating Indicator = " + participatingPlan1 + " ";
										writeToCSV("Participating Indicator", var_participatingIndicator);
									}


									String currency1 = objData.getString("currencyCode");
									System.out.println("currency1 ==== : " + currency1);
									LOGGER.info("currency1 ==== : " + currency1);
									if (currency1.contains(var_currencyCode)) {
										writeToCSV("Currency Code", var_currencyCode);
									} else {
										var_policyResult = "FAIL";
										var_failedResults = var_failedResults + " , Currency Code = " + currency1 + " ";
										writeToCSV("Currency Code", var_currencyCode);
									}

									String insuranceType1 = objData.getString("insuranceType");
									System.out.println("insuranceType1 ==== : " + insuranceType1);
									LOGGER.info("insuranceType1 ==== : " + insuranceType1);

									if (insuranceType1.equals("L")) {
										insuranceType1 = "W";
									}

									if (var_productCode.contains(insuranceType1)) {
										writeToCSV("Product Code", var_productCode);
									} else {
										var_policyResult = "FAIL";
										var_failedResults = var_failedResults + " , Product Code = " + insuranceType1 + " ";
										writeToCSV("Product Code", var_productCode);
									}

									var_specialClassUnisexRatesAllowed = objData.getString("specialClassUnisexRatesAllowed");


								}

							}catch(Exception e){
								writeToCSV("PlanCode", var_planCode);
								writeToCSV("Participating Indicator", var_participatingIndicator);
								writeToCSV("Currency Code", var_currencyCode);
								writeToCSV("Product Code", var_productCode);
								// var_failedResults = var_failedResults  + " , API PDF100S02 is not working";


								PrintVal("Renderer Script Exception : "+e.toString());
							}



						
				 
				 
		 
		 ;} 
		 catch(Exception e) { PrintVal("Renderer Script Exception : "+e.toString()); } 
		 PrintVal("Executed Step = CALLRENDERERSCRIPT,GETPOLICYDATAAPI_PDF100S02"); 
		try { 
		 		 			var_apiResult = "FAIL";

				 			try{
							URL url = new URL ("http://cis-coreapd1.btoins.ibm.com:17000/rest/dataapi/clientRelationship/loadRows");
							HttpURLConnection con = (HttpURLConnection)url.openConnection();
							con.setRequestMethod("POST");
							con.setRequestProperty("Content-Type", "application/json; utf-8");
							con.setRequestProperty("Accept", "application/json");
							String loginPassword = "autoapi"+ ":" + "Autoapi!23";
							String encoded = new sun.misc.BASE64Encoder().encode (loginPassword.getBytes());
							con.setRequestProperty ("Authorization", "Basic " + encoded);
							con.setDoOutput(true);
							JSONArray arrayofsearchCriteria = new JSONArray();
							//JSONObject searchCriteria = new JSONObject();

							JSONObject searchCriteria1 = new JSONObject();
							searchCriteria1.put("field","controlNumberOne");
							searchCriteria1.put("operator","EQ");
							searchCriteria1.put("value",var_policyNumber);

							JSONObject searchCriteria2 = new JSONObject();
							searchCriteria2.put("field","controlNumberTwo");
							searchCriteria2.put("operator","EQ");
							searchCriteria2.put("value",var_costBasisBaseRiderCodeFromDB);

							arrayofsearchCriteria.put(searchCriteria1);
							arrayofsearchCriteria.put(searchCriteria2);
							
							String jsonInputString = new JSONObject()
									.put("searchCriteria",arrayofsearchCriteria)
									.toString();



							System.out.println(jsonInputString);
							try(OutputStream os = con.getOutputStream()) {
								byte[] input = jsonInputString.getBytes("utf-8");
								os.write(input, 0, input.length);
							}
							try(BufferedReader br = new BufferedReader(
									new InputStreamReader(con.getInputStream(), "utf-8"))) {
								StringBuilder response = new StringBuilder();
								String responseLine = null;
								while ((responseLine = br.readLine()) != null) {

									response.append(responseLine.trim());
								}
								System.out.println("clientRelationship ===== "+response.toString());
								//LOGGER.info("clientRelationship=====" +response.toString());
				 
								var_apiResult = "PASS";
								JSONObject response1 = new JSONObject(response.toString());

								//JSONObject objData =  response1.getJSONObject("data");
								//System.out.println("objData ===== "+objData.toString());

								JSONArray jsonarrayobject = response1.getJSONArray("data");
								System.out.println("jsonarrayobject ===== "+jsonarrayobject.toString());
								JSONObject firstObjectOfData = jsonarrayobject.getJSONObject(0);

								System.out.println("firstObjectOfData 1 ===== "+firstObjectOfData.toString());

								var_clientNumberFromDB = firstObjectOfData.getString("clientNumber");



							}


							}catch(Exception e){

								// var_failedResults = var_failedResults  + " , API CLIENTRELATIONSHIP is not working";


								PrintVal("Renderer Script Exception : "+e.toString());
							}


				 
				 
		 
		 ;} 
		 catch(Exception e) { PrintVal("Renderer Script Exception : "+e.toString()); } 
		 PrintVal("Executed Step = CALLRENDERERSCRIPT,GETPOLICYDATAAPI_CLIENTRELATIONSHIP"); 
		try { 
		 		 			var_apiResult = "FAIL";

				 			try {
								URL url = new URL("http://cis-coreapd1.btoins.ibm.com:17000/rest/pageapi/cms005s1/loadData");
								HttpURLConnection con = (HttpURLConnection) url.openConnection();
								con.setRequestMethod("POST");
								con.setRequestProperty("Content-Type", "application/json; utf-8");
								con.setRequestProperty("Accept", "application/json");
								String loginPassword = "autoapi" + ":" + "Autoapi!23";
								String encoded = new sun.misc.BASE64Encoder().encode(loginPassword.getBytes());
								con.setRequestProperty("Authorization", "Basic " + encoded);
								con.setDoOutput(true);
								JSONObject param = new JSONObject();

								JSONObject clientMaint = new JSONObject();
								clientMaint.put("clientNumber", var_clientNumberFromDB);

								param.put("clientMaint", clientMaint);
								param.put("mode", "display");
								param.put("uniqueId", 111);
								JSONObject session = new JSONObject();
								session.put("currentCompany", "CO1");
								String jsonInputString = new JSONObject()
										.put("param", param)
										.put("session", session)
										.toString();


								System.out.println(jsonInputString);
								try (OutputStream os = con.getOutputStream()) {
									byte[] input = jsonInputString.getBytes("utf-8");
									os.write(input, 0, input.length);
								}
								try (BufferedReader br = new BufferedReader(
										new InputStreamReader(con.getInputStream(), "utf-8"))) {
									StringBuilder response = new StringBuilder();
									String responseLine = null;
									while ((responseLine = br.readLine()) != null) {

										response.append(responseLine.trim());
									}
									System.out.println("cms005s1===== " + response.toString());


									var_apiResult = "PASS";
									JSONObject response1 = new JSONObject(response.toString());

									JSONObject objData = response1.getJSONObject("data");
									System.out.println("objData_cms005s1 ===== " + objData.toString());


									String pricingsexCode1 = objData.getString("sexCode");
									System.out.println("sexCode1 ==== : " + pricingsexCode1);
									if (var_specialClassUnisexRatesAllowed.equals("Y")) {
										pricingsexCode1 = "M";
									}
									LOGGER.info("sexCode1 ==== : " + pricingsexCode1);
									if (pricingsexCode1.contains(var_pricingSex)) {
										writeToCSV("Pricing Sex", var_pricingSex);
									} else {
										var_policyResult = "FAIL";
										var_failedResults = var_failedResults + " , Pricing Sex = " + pricingsexCode1 + " ";
										writeToCSV("Pricing Sex", var_pricingSex);
									}


									String sexCode1 = objData.getString("sexCode");
									System.out.println("sexCode1 ==== : " + sexCode1);
									LOGGER.info("sexCode1 ==== : " + sexCode1);
									if (sexCode1.contains(var_sex)) {
										writeToCSV("Gender", var_sex);
									} else {
										var_policyResult = "FAIL";
										var_failedResults = var_failedResults + " , Gender = " + sexCode1 + " ";
										writeToCSV("Gender", var_sex);
									}


								}

							}catch(Exception e){

								writeToCSV("Pricing Sex", var_pricingSex);
								writeToCSV("Gender", var_sex);
					// var_failedResults = var_failedResults  + " , API CMS005S1 is not working";


					PrintVal("Renderer Script Exception : "+e.toString());
				}



					
		 
		 ;} 
		 catch(Exception e) { PrintVal("Renderer Script Exception : "+e.toString()); } 
		 PrintVal("Executed Step = CALLRENDERERSCRIPT,GETPOLICYDATAAPI_CMS005S1"); 
		try { 
		 		 

					try {
						URL url = new URL("http://cis-coreapd1.btoins.ibm.com:17000/rest/pageapi/pos224s1/loadRows");
						HttpURLConnection con = (HttpURLConnection) url.openConnection();
						con.setRequestMethod("POST");
						con.setRequestProperty("Content-Type", "application/json; utf-8");
						con.setRequestProperty("Accept", "application/json");
						String loginPassword = "autoapi" + ":" + "Autoapi!23";
						String encoded = new sun.misc.BASE64Encoder().encode(loginPassword.getBytes());
						con.setRequestProperty("Authorization", "Basic " + encoded);
						con.setDoOutput(true);


						JSONObject data = new JSONObject();
						data.put("baseRiderCode", var_costBasisBaseRiderCodeFromDB);
						data.put("policyNumber", var_policyNumber);
						data.put("pageNumber", 1);
						data.put("pageSize", 25);

						JSONObject session = new JSONObject();
						String jsonInputString = new JSONObject()
								.put("param", data)
								.put("session", session)
								.toString();


						System.out.println(jsonInputString);
						try (OutputStream os = con.getOutputStream()) {
							byte[] input = jsonInputString.getBytes("utf-8");
							os.write(input, 0, input.length);
						}
						try (BufferedReader br = new BufferedReader(
								new InputStreamReader(con.getInputStream(), "utf-8"))) {
							StringBuilder response = new StringBuilder();
							String responseLine = null;
							while ((responseLine = br.readLine()) != null) {

								response.append(responseLine.trim());
							}
							System.out.println("pos224s1 ===== " + response.toString());

							JSONObject response1 = new JSONObject(response.toString());

							JSONArray jsonarrayobject = response1.getJSONArray("rows");
							System.out.println("jsonarrayobject ===== " + jsonarrayobject.toString());
							JSONObject firstObjectOfData = jsonarrayobject.getJSONObject(0);

							System.out.println("firstObjectOfData 1 ===== " + firstObjectOfData.toString());

							var_uniqueIDFrompos224s1 = firstObjectOfData.getInt("uniqueID");

							System.out.println("var_uniqueIDFrompos224s1 1 ===== " + var_uniqueIDFrompos224s1);


						}
					}catch(Exception e){

					// var_failedResults = var_failedResults  + " , API POS224S1 is not working";


					PrintVal("Renderer Script Exception : "+e.toString());
				}
				 
				 
		 
		 ;} 
		 catch(Exception e) { PrintVal("Renderer Script Exception : "+e.toString()); } 
		 PrintVal("Executed Step = CALLRENDERERSCRIPT,GETPOLICYDATAAPI_POS224S1"); 
		try { 
		 		 
							//var_apiResult = "FAIL";
					try{
							URL url = new URL ("http://cis-coreapd1.btoins.ibm.com:17000/rest/pageapi/pos224s2/loadData");
							HttpURLConnection con = (HttpURLConnection)url.openConnection();
							con.setRequestMethod("POST");
							con.setRequestProperty("Content-Type", "application/json; utf-8");
							con.setRequestProperty("Accept", "application/json");
							String loginPassword = "autoapi"+ ":" + "Autoapi!23";
							String encoded = new sun.misc.BASE64Encoder().encode (loginPassword.getBytes());
							con.setRequestProperty ("Authorization", "Basic " + encoded);
							con.setDoOutput(true);


							JSONObject param = new JSONObject();
							param.put("baseRiderCode",var_costBasisBaseRiderCodeFromDB);
							param.put("policyNumber",var_policyNumber);
							param.put("mode","display");
							param.put("uniqueId",var_uniqueIDFrompos224s1);
							param.put("planCode","");
							param.put("insuredName","");
							param.put("jointInsuredName","");
							JSONObject session = new JSONObject();
							session.put("currentCompany","CO1");

							String jsonInputString = new JSONObject()
									.put("param",param )
									.put("session",session)
									.toString();


							System.out.println(jsonInputString);
							try(OutputStream os = con.getOutputStream()) {
								byte[] input = jsonInputString.getBytes("utf-8");
								os.write(input, 0, input.length);
							}
							try(BufferedReader br = new BufferedReader(
									new InputStreamReader(con.getInputStream(), "utf-8"))) {
								StringBuilder response = new StringBuilder();
								String responseLine = null;
								while ((responseLine = br.readLine()) != null) {

									response.append(responseLine.trim());
								}
								System.out.println("pos224s2 ===== "+response.toString());


								var_apiResult = "PASS";
								JSONObject response1 = new JSONObject(response.toString());

								JSONObject objData =  response1.getJSONObject("data");




								String AnnualPremium1 = String.valueOf((int)objData.getDouble("annualPremium"));
								System.out.println("AnnualPremium1 ==== : "+ AnnualPremium1);
								LOGGER.info("AnnualPremium1 ==== : "+ AnnualPremium1);

								String modalPremium1 = String.valueOf((int)objData.getDouble("modalPremium"));

								System.out.println("modalPremium1 ==== : "+ modalPremium1);
								LOGGER.info("modalPremium1 ==== : "+ modalPremium1);

								if(var_premiumForAccidentalDeathBenefit.contains(AnnualPremium1)){

									writeToCSV("Premium for Accidental Death Benefit",var_premiumForAccidentalDeathBenefit);

								} else if(var_premiumForAccidentalDeathBenefit.contains(modalPremium1)){

									writeToCSV("Premium for Accidental Death Benefit",var_premiumForAccidentalDeathBenefit);

								}else{
									var_policyResult = "FAIL";
									var_failedResults = var_failedResults + " , Premium for Accidental Death Benefit ="+AnnualPremium1+" ";
									writeToCSV("Premium for Accidental Death Benefit",var_premiumForAccidentalDeathBenefit);
								}







							}


					}catch(Exception e){

						writeToCSV("Premium for Accidental Death Benefit",var_premiumForAccidentalDeathBenefit);
						// var_failedResults = var_failedResults  + " , API POS224S2 is not working";


						PrintVal("Renderer Script Exception : "+e.toString());
					}

						
				 
				 
		 
		 ;} 
		 catch(Exception e) { PrintVal("Renderer Script Exception : "+e.toString()); } 
		 PrintVal("Executed Step = CALLRENDERERSCRIPT,GETPOLICYDATAAPI_POS224S2"); 
		try { 
		 		 			//var_apiResult = "FAIL";

					try {
						URL url = new URL("http://cis-coreapd1.btoins.ibm.com:17000/rest/dataapi/DUFFieldData/loadRows");
						HttpURLConnection con = (HttpURLConnection) url.openConnection();
						con.setRequestMethod("POST");
						con.setRequestProperty("Content-Type", "application/json; utf-8");
						con.setRequestProperty("Accept", "application/json");
						String loginPassword = "autoapi" + ":" + "Autoapi!23";
						String encoded = new sun.misc.BASE64Encoder().encode(loginPassword.getBytes());
						con.setRequestProperty("Authorization", "Basic " + encoded);
						con.setDoOutput(true);


						JSONArray arrayofsearchCriteria = new JSONArray();

						JSONObject searchCriteria1 = new JSONObject();
						searchCriteria1.put("field", "parentId");
						searchCriteria1.put("operator", "EQ");
						searchCriteria1.put("value", var_uniqueIDFrompos210s1);

						JSONObject searchCriteria2 = new JSONObject();
						searchCriteria2.put("field", "sourceFile");
						searchCriteria2.put("operator", "EQ");
						searchCriteria2.put("value", "Contract");

						JSONObject searchCriteria3 = new JSONObject();
						searchCriteria3.put("field", "userField");
						searchCriteria3.put("operator", "EQ");
						searchCriteria3.put("value", "REPORTCODE");

						arrayofsearchCriteria.put(searchCriteria1);
						arrayofsearchCriteria.put(searchCriteria2);
						arrayofsearchCriteria.put(searchCriteria3);

						JSONArray searchOrder = new JSONArray();

						String jsonInputString = new JSONObject()
								.put("searchCriteria", arrayofsearchCriteria)
								.put("searchOrder", searchOrder)
								.toString();


						System.out.println(jsonInputString);
						try (OutputStream os = con.getOutputStream()) {
							byte[] input = jsonInputString.getBytes("utf-8");
							os.write(input, 0, input.length);
						}
						try (BufferedReader br = new BufferedReader(
								new InputStreamReader(con.getInputStream(), "utf-8"))) {
							StringBuilder response = new StringBuilder();
							String responseLine = null;
							while ((responseLine = br.readLine()) != null) {

								response.append(responseLine.trim());
							}
							System.out.println("DUFFieldData ===== " + response.toString());
							//LOGGER.info("DUFFieldData =====" +response.toString());

							var_apiResult = "PASS";
							JSONObject response1 = new JSONObject(response.toString());

							JSONArray jsonarrayobject = response1.getJSONArray("data");
							System.out.println("jsonarrayobject ===== " + jsonarrayobject.toString());
							JSONObject firstObjectOfData = jsonarrayobject.getJSONObject(0);

							System.out.println("firstObjectOfData 1 ===== " + firstObjectOfData.toString());

							String LocationCode11 = firstObjectOfData.getString("stringValue");

							//String LocationCode11 = objData.getString("variableClassCode");
							System.out.println("LocationCode11 ==== : " + LocationCode11);
							LOGGER.info("LocationCode11 ==== : " + LocationCode11);
							if (var_locationCode.contains("")) {
								writeToCSV("Location Code", var_locationCode);
							} else {
								var_policyResult = "FAIL";
								var_failedResults = var_failedResults + " , Location Code =  ";
								writeToCSV("Location Code", var_locationCode);
							}


						}
					}catch(Exception e){

						writeToCSV("Location Code", var_locationCode);
						//var_failedResults = var_failedResults  + " , API LOCATIONCODE is not working";


						PrintVal("Renderer Script Exception : "+e.toString());
					}


						
				 
				 
		 
		 ;} 
		 catch(Exception e) { PrintVal("Renderer Script Exception : "+e.toString()); } 
		 PrintVal("Executed Step = CALLRENDERERSCRIPT,GETPOLICYDATAAPI_LOCATIONCODE"); 
		try { 
		 			try {
						String wsURL = "http://cis-coreapd1.btoins.ibm.com:17000/ws";
						URL url = null;
						URLConnection connection = null;
						HttpURLConnection httpConn = null;
						String responseString = null;
						String outputString = "";
						OutputStream out = null;
						InputStreamReader isr = null;
						BufferedReader in = null;

						String xmlInput = "<SOAP-ENV:Envelope xmlns:SOAP-ENV=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:q0=\"http://policyvalues.ws.gias.concentrix.com\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"> <SOAP-ENV:Header> <wsse:Security SOAP-ENV:mustUnderstand=\"1\" xmlns:wsse=\"http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd\"> <wsse:UsernameToken xmlns:wsu=\"http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd\" wsu:ID=\"UsernameToken-400627681\"> <wsse:Username>autoapi</wsse:Username> <wsse:Password Type=\"http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-username-token-profile-1.0#PasswordText\">Autoapi!23</wsse:Password> </wsse:UsernameToken> </wsse:Security> </SOAP-ENV:Header> <SOAP-ENV:Body> <q0:PolicyCashValueRequest> <calculationDate>2021-05-22</calculationDate> <companyCode>CO1</companyCode> <policyNumber>" + var_policyNumber + "</policyNumber> </q0:PolicyCashValueRequest> </SOAP-ENV:Body> </SOAP-ENV:Envelope>";

				
							url = new URL(wsURL);
							connection = url.openConnection();
							httpConn = (HttpURLConnection) connection;

							byte[] buffer = new byte[xmlInput.length()];
							buffer = xmlInput.getBytes();

							String SOAPAction = "";
							httpConn.setRequestProperty("Content-Type",
									"text/xml; charset=utf-8");


							httpConn.setRequestProperty("SOAPAction", SOAPAction);
							httpConn.setRequestMethod("POST");
							httpConn.setDoOutput(true);
							httpConn.setDoInput(true);
							out = httpConn.getOutputStream();
							out.write(buffer);
							out.close();

							// Read the response and write it to standard out.
							isr = new InputStreamReader(httpConn.getInputStream());
							in = new BufferedReader(isr);

							while ((responseString = in.readLine()) != null) {
								outputString = outputString + responseString;
							}
							System.out.println(outputString);
							System.out.println("");


							javax.xml.parsers.DocumentBuilderFactory dbf = javax.xml.parsers.DocumentBuilderFactory.newInstance();
							javax.xml.parsers.DocumentBuilder db = dbf.newDocumentBuilder();
							org.xml.sax.InputSource is = new org.xml.sax.InputSource(new StringReader(outputString));
							org.w3c.dom.Document document = db.parse(is);
							org.w3c.dom.NodeList nodeLst = document.getElementsByTagName("ns3:PolicyCashValueResponse");
							String webServiceResponse = nodeLst.item(0).getTextContent();
							System.out.println("The response from the web service call is : " + webServiceResponse);
							org.w3c.dom.Node node = nodeLst.item(0);

							if (node.getNodeType() == org.w3c.dom.Node.ELEMENT_NODE) {
								org.w3c.dom.Element eElement = (org.w3c.dom.Element) node.getChildNodes();

								String netCashValue11 = eElement.getElementsByTagName("netCashValue").item(0).getTextContent();
								System.out.println("netCashValue : " + netCashValue11);
								netCashValue11 = String.valueOf((int) Double.parseDouble(netCashValue11));
								System.out.println("netCashValue : " + netCashValue11);


								//String netCashValue1 = String.valueOf((int)objData.getDouble("netCashValue"));
								System.out.println("netCashValue1 ==== : " + netCashValue11);
								LOGGER.info("netCashValue1 ==== : " + netCashValue11);
								if (var_cashValue.contains(netCashValue11)) {
									writeToCSV("Net Cash Value", var_cashValue);
								} else {
									var_policyResult = "FAIL";
									var_failedResults = var_failedResults + " , Net Cash Value =" + netCashValue11 + "";
									writeToCSV("Net Cash Value", var_cashValue);
								}


								String dividendAccum = eElement.getElementsByTagName("dividendAccum").item(0).getTextContent();
								System.out.println("dividendAccum : " + dividendAccum);
								dividendAccum = String.valueOf((int) Double.parseDouble(dividendAccum));
								System.out.println("dividendAccum : " + dividendAccum);


								LOGGER.info("lastDividendAmount1 ==== : " + dividendAccum);
								if (var_dividend.contains(dividendAccum)) {
									writeToCSV("Dividend", var_dividend);
								} else {
									var_policyResult = "FAIL";
									var_failedResults = var_failedResults + " , Dividend = " + dividendAccum + " ";
									writeToCSV("Dividend", var_dividend);
								}


							}


					} catch (Exception e){
						writeToCSV("Net Cash Value", var_cashValue);
						writeToCSV("Dividend", var_dividend);
					//	var_failedResults = var_failedResults  + " , API net_cash_value and Dividend is not working";


						PrintVal("Renderer Script Exception : "+e.toString());
					}
				 
				 
		 
		 ;} 
		 catch(Exception e) { PrintVal("Renderer Script Exception : "+e.toString()); } 
		 PrintVal("Executed Step = CALLRENDERERSCRIPT,GETPOLICYSOAPAPI_CASHVALUE"); 
		try { 
		 			try {
						var_apiResult = "FAIL";
						URL url = new URL("http://cis-coreapd1.btoins.ibm.com:17000/rest/pageapi/pos220s2/loadData");
						HttpURLConnection con = (HttpURLConnection) url.openConnection();
						con.setRequestMethod("POST");
						con.setRequestProperty("Content-Type", "application/json; utf-8");
						con.setRequestProperty("Accept", "application/json");
						String loginPassword = "autoapi" + ":" + "Autoapi!23";
						String encoded = new sun.misc.BASE64Encoder().encode(loginPassword.getBytes());
						con.setRequestProperty("Authorization", "Basic " + encoded);
						con.setDoOutput(true);
						JSONObject param = new JSONObject();
						param.put("baseRiderCode", 0);
						param.put("policyNumber", var_policyNumber);
						param.put("mode", "display");
						param.put("uniqueId", 999);
						JSONObject session = new JSONObject();
						session.put("currentCompany", "CO1");
						session.put("sessionDate", "2021-04-16");
						String jsonInputString = new JSONObject()
								.put("param", param)
								.put("session", session)
								.toString();


						System.out.println(jsonInputString);
						try (OutputStream os = con.getOutputStream()) {
							byte[] input = jsonInputString.getBytes("utf-8");
							os.write(input, 0, input.length);
						}
						try (BufferedReader br = new BufferedReader(
								new InputStreamReader(con.getInputStream(), "utf-8"))) {
							StringBuilder response = new StringBuilder();
							String responseLine = null;
							while ((responseLine = br.readLine()) != null) {

								response.append(responseLine.trim());
							}
							System.out.println("BBBBB===== " + response.toString());
							//LOGGER.info("BBBB=====" +response.toString());

							var_apiResult = "PASS";
							JSONObject response1 = new JSONObject(response.toString());

							JSONObject objData = response1.getJSONObject("data");

							String levelDeathBenefit1 = objData.getString("deathBenefitOption");
							System.out.println("levelDeathBenefit1 ==== : " + levelDeathBenefit1);

							if (levelDeathBenefit1.equals("A")) {
								levelDeathBenefit1 = "1";

							} else if (levelDeathBenefit1.equals("B")) {
								levelDeathBenefit1 = "2";
							}

							LOGGER.info("levelDeathBenefit1 ==== : " + levelDeathBenefit1);
							if (var_deathBenefitOption.contains(levelDeathBenefit1)) {
								writeToCSV("Death Benefit Option", var_deathBenefitOption);
							} else {
								var_policyResult = "FAIL";
								var_failedResults = var_failedResults + " , Death Benefit Option = " + levelDeathBenefit1 + " ";
								writeToCSV("Death Benefit Option", var_deathBenefitOption);
							}

							String insuredName1 = objData.getString("insuredName");
							System.out.println("insuredName ==== : " + insuredName1);
							LOGGER.info("insuredName ==== : " + insuredName1);

							String[] Name = insuredName1.split(" ");

							String firstName1 = "";
							String lastName1 = "";
							String middleName1 = "";


							if (Name.length == 3) {
								firstName1 = Name[0];
								System.out.println("firstName1 ==== : " + firstName1);
								LOGGER.info("firstName1 ==== : " + firstName1);
								if (firstName1.toUpperCase().contains(var_firstName.toUpperCase())) {
									writeToCSV("FirstName", var_firstName);
								} else {
									var_policyResult = "FAIL";
									var_failedResults = var_failedResults + " , FirstName = " + firstName1 + " ";
									writeToCSV("FirstName", var_firstName);
								}

								middleName1 = Name[1];
								System.out.println("middleName1 ==== : " + middleName1);
								LOGGER.info("middleName1 ==== : " + middleName1);
								if (middleName1.toUpperCase().contains(var_middleInitials.toUpperCase())) {
									writeToCSV("MiddleName", var_middleInitials);
								} else {
									var_policyResult = "FAIL";
									var_failedResults = var_failedResults + " , MiddleName = " + middleName1 + " ";
									writeToCSV("MiddleName", var_middleInitials);
								}


								lastName1 = Name[2];
								System.out.println("lastName1 ==== : " + lastName1);
								LOGGER.info("lastName1 ==== : " + lastName1);
								if (lastName1.toUpperCase().contains(var_lastName.toUpperCase())) {
									writeToCSV("LastName", var_lastName);
								} else {
									var_policyResult = "FAIL";
									var_failedResults = var_failedResults + " , LastName = " + lastName1 + "";
									writeToCSV("LastName", var_lastName);
								}


							} else {
								firstName1 = Name[0];
								System.out.println("firstName1 ==== : " + firstName1);
								LOGGER.info("firstName1 ==== : " + firstName1);
								if (firstName1.toUpperCase().contains(var_firstName.toUpperCase())) {
									writeToCSV("FirstName", var_firstName);
								} else {
									var_policyResult = "FAIL";
									var_failedResults = var_failedResults + " , FirstName = " + firstName1 + " ";
									writeToCSV("FirstName", var_firstName);
								}

								writeToCSV("MiddleName", middleName1);

								lastName1 = Name[1];
								System.out.println("lastName1 ==== : " + lastName1);
								LOGGER.info("lastName1 ==== : " + lastName1);
								if (lastName1.toUpperCase().contains(var_lastName.toUpperCase())) {
									writeToCSV("LastName", var_lastName);
								} else {
									var_policyResult = "FAIL";
									var_failedResults = var_failedResults + " , LastName = " + lastName1 + "";
									writeToCSV("LastName", var_lastName);
								}
							}


							String insuredBirthDate1 = objData.getString("insuredBirthDate").replaceAll("-", "");
							System.out.println("insuredBirthDate1 ==== : " + insuredBirthDate1);
							LOGGER.info("insuredBirthDate1 ==== : " + insuredBirthDate1);
							if (var_dateOfBirth.contains(insuredBirthDate1)) {
								writeToCSV("DateofBirth", var_dateOfBirth);
							} else {
								var_policyResult = "FAIL";
								var_failedResults = var_failedResults + " , DateofBirth = " + insuredBirthDate1 + " ";
								writeToCSV("DateofBirth", var_dateOfBirth);
							}


							String benefitStatus1 = objData.getString("benefitStatus");
							System.out.println("benefitStatus1 ==== : " + benefitStatus1);
							LOGGER.info("benefitStatus1 ==== : " + benefitStatus1);
							if (var_insuredStatus.contains("A")) {
								writeToCSV("Status", var_insuredStatus);
							} else {
								var_policyResult = "FAIL";
								var_failedResults = var_failedResults + " , Status = A ";
								writeToCSV("Status", var_insuredStatus);
							}


							String issueAge1 = String.valueOf(objData.getInt("issueAge"));
							System.out.println("issueAge1 ==== : " + issueAge1);
							LOGGER.info("issueAge1 ==== : " + issueAge1);
							if (issueAge1.contains(var_issueAge)) {
								writeToCSV("IssueAge", var_issueAge);
							} else {
								var_policyResult = "FAIL";
								var_failedResults = var_failedResults + " , IssueAge =" + issueAge1 + " ";
								writeToCSV("IssueAge", var_issueAge);
							}


							String smokerCode1 = objData.getString("smokerCode");
							System.out.println("smokerCode1 ==== : " + smokerCode1);
							LOGGER.info("smokerCode1 ==== : " + smokerCode1);
							if (var_premiumClass.contains(smokerCode1)) {
								writeToCSV("PremiumClass", var_premiumClass);
							} else {
								var_policyResult = "FAIL";
								var_failedResults = var_failedResults + " , PremiumClass =" + smokerCode1 + "";
								writeToCSV("PremiumClass", var_premiumClass);
							}


							String faceAmount1 = String.valueOf(objData.getInt("faceAmount"));
							System.out.println("faceAmount1 ==== : " + faceAmount1);
							LOGGER.info("faceAmount1 ==== : " + faceAmount1);

							System.out.println("faceAmount1 From Json ==== : " + var_faceAmount);
							LOGGER.info("faceAmount1 From Json ==== : " + var_faceAmount);
							if (var_faceAmount.contains(faceAmount1)) {
								writeToCSV("FaceAmount", var_faceAmount);
							} else {
								var_policyResult = "FAIL";
								var_failedResults = var_failedResults + " , FaceAmount = " + faceAmount1 + "";
								writeToCSV("FaceAmount", var_faceAmount);
							}


							String totalAnnualPremium1 = String.valueOf((int) objData.getDouble("annualPremium"));
							System.out.println("totalAnnualPremium1 ==== : " + totalAnnualPremium1);
							LOGGER.info("totalAnnualPremium1 ==== : " + totalAnnualPremium1);
							System.out.println("totalAnnualPremium1 From Json ==== : " + var_premiumForBaseCoverage);
							LOGGER.info("totalAnnualPremium1 From Json ==== : " + var_premiumForBaseCoverage);

							if (var_premiumForBaseCoverage.contains(totalAnnualPremium1)) {
								writeToCSV("AnnualPremium", var_premiumForBaseCoverage);
							} else {
								var_policyResult = "FAIL";
								var_failedResults = var_failedResults + " , AnnualPremium =" + totalAnnualPremium1 + " ";
								writeToCSV("AnnualPremium", var_premiumForBaseCoverage);
							}


							String planCodeN1 = objData.getString("planCode");
							System.out.println("planCodeN1 ==== : " + planCodeN1);
							LOGGER.info("planCodeN1 ==== : " + planCodeN1);

							String fundedBenefit1 = objData.getString("fundedBenefit");

							String Year1 = getSubStringFromString(var_PaidToDateFromDB, 1, 4);
							String Month1 = getSubStringFromString(var_PaidToDateFromDB, 5, 6);

							if (fundedBenefit1.equals("N")) {
								Year1 = String.valueOf((Integer.parseInt(Year1) - 1));

							} else if (fundedBenefit1.equals("Y")) {

								if (Month1.equals("1") || Month1.contains("01")) {
									Month1 = "12";
									Year1 = String.valueOf((Integer.parseInt(Year1) - 1));
								} else {

									Month1 = String.valueOf((Integer.parseInt(Month1) - 1));

								}

							}


							if (Year1.contains(var_valuesYear)) {
								writeToCSV("Value Year", var_valuesYear);
							} else {
								var_policyResult = "FAIL";
								var_failedResults = var_failedResults + " , Value Year = " + Year1 + " ";
								writeToCSV("Value Year", var_valuesYear);
							}

							if (var_valuesMonth.contains(Month1)) {
								writeToCSV("Value Month", var_valuesMonth);
							} else {
								var_policyResult = "FAIL";
								var_failedResults = var_failedResults + " , Value Month = " + Month1 + " ";
								writeToCSV("Value Month", var_valuesMonth);
							}

							String faceAmountForDeathBenefit = String.valueOf((int) (objData.getDouble("faceAmount")));
							System.out.println("faceAmountForDeathBenefit ==== : " + faceAmountForDeathBenefit);
							LOGGER.info("faceAmountForDeathBenefit ==== : " + faceAmountForDeathBenefit);

							if (fundedBenefit1.equals("N")) {

								if (var_deathBenefit.contains(faceAmountForDeathBenefit)) {

									writeToCSV("Death Benefit", var_deathBenefit);
								} else {
									var_policyResult = "FAIL";
									var_failedResults = var_failedResults + " , Death Benefit =" + faceAmountForDeathBenefit + " ";
									writeToCSV("Death Benefit", var_deathBenefit);
								}


							} else if (fundedBenefit1.equals("Y")) {

								if (levelDeathBenefit1.equals("A")) {

									if (var_deathBenefit.contains(faceAmountForDeathBenefit)) {

										writeToCSV("Death Benefit", var_deathBenefit);
									} else {
										var_policyResult = "FAIL";
										var_failedResults = var_failedResults + " , Death Benefit =" + faceAmountForDeathBenefit + " ";
										writeToCSV("Death Benefit", var_deathBenefit);
									}


								} else if (levelDeathBenefit1.equals("B")) {
									var_DeathBenefitOptionDeathBenefit = "B";
									var_faceAmountForDeathBenefit = faceAmountForDeathBenefit;


								} else {

									if (var_deathBenefit.contains(faceAmountForDeathBenefit)) {

										writeToCSV("Death Benefit", var_deathBenefit);
									} else {
										var_policyResult = "FAIL";
										var_failedResults = var_failedResults + " , Death Benefit =" + faceAmountForDeathBenefit + " ";
										writeToCSV("Death Benefit", var_deathBenefit);
									}


								}


							}


						}

					}catch (Exception e){
						writeToCSV("Death Benefit Option", var_deathBenefitOption);
						writeToCSV("FirstName", var_firstName);
						writeToCSV("MiddleName", var_middleInitials);
						writeToCSV("LastName", var_lastName);
						writeToCSV("DateofBirth", var_dateOfBirth);
						writeToCSV("Status", var_insuredStatus);
						writeToCSV("IssueAge", var_issueAge);
						writeToCSV("PremiumClass", var_premiumClass);
						writeToCSV("FaceAmount", var_faceAmount);
						writeToCSV("AnnualPremium", var_premiumForBaseCoverage);
						writeToCSV("Value Year",var_valuesYear);
						writeToCSV("Value Month",var_valuesMonth);
						writeToCSV("Death Benefit",var_deathBenefit);

						// var_failedResults = var_failedResults  + " , API POS220S2 is not working";


										PrintVal("Renderer Script Exception : "+e.toString());
									}



								
						 
						 
				 
				 
		 
		 ;} 
		 catch(Exception e) { PrintVal("Renderer Script Exception : "+e.toString()); } 
		 PrintVal("Executed Step = CALLRENDERERSCRIPT,GETPOLICYDATAAPI_POS220S2"); 
        LOGGER.info("Executed Step = START IF");
        if (check(var_DeathBenefitOptionDeathBenefit,"=","B","TGTYPESCREENREG")) {
         LOGGER.info("Executed Step = IF,var_DeathBenefitOptionDeathBenefit,=,B,TGTYPESCREENREG");
		try { 
		 
				 

							//var_apiResult = "FAIL";
					try{
							URL url = new URL ("http://cis-coreapd1.btoins.ibm.com:17000/rest/pageapi/pos252s1/loadRows");
							HttpURLConnection con = (HttpURLConnection)url.openConnection();
							con.setRequestMethod("POST");
							con.setRequestProperty("Content-Type", "application/json; utf-8");
							con.setRequestProperty("Accept", "application/json");
							String loginPassword = "autoapi"+ ":" + "Autoapi!23";
							String encoded = new sun.misc.BASE64Encoder().encode (loginPassword.getBytes());
							con.setRequestProperty ("Authorization", "Basic " + encoded);
							con.setDoOutput(true);


							JSONObject	displayFund = new JSONObject();
							displayFund.put("baseRiderCode",var_costBasisBaseRiderCodeFromDB);
							displayFund.put("displayFundSources","N");
							displayFund.put("displayShadowFunds","N");
							displayFund.put("policyNumber",var_policyNumber);
							displayFund.put("returnCode","");

							JSONObject	displayFund1 = new JSONObject();
							displayFund1.put("displayFund",displayFund);
							displayFund1.put("filterMatch","start");
							displayFund1.put("pageNumber",1);
							displayFund1.put("pageSize",25);

							JSONObject session = new JSONObject();


							String jsonInputString = new JSONObject()
									.put("param",displayFund1)
									.put("session",session)
									.toString();



							System.out.println(jsonInputString);
							try(OutputStream os = con.getOutputStream()) {
								byte[] input = jsonInputString.getBytes("utf-8");
								os.write(input, 0, input.length);
							}
							try(BufferedReader br = new BufferedReader(
									new InputStreamReader(con.getInputStream(), "utf-8"))) {
								StringBuilder response = new StringBuilder();
								String responseLine = null;
								while ((responseLine = br.readLine()) != null) {

									response.append(responseLine.trim());
								}
								System.out.println("pos252s1 ===== "+response.toString());


								JSONObject response1 = new JSONObject(response.toString());

								JSONArray jsonarrayobject = response1.getJSONArray("rows");
								System.out.println("jsonarrayobject ===== "+jsonarrayobject.toString());

								Double currentFundBalance = 0.00;
								for (int i = 0; i < jsonarrayobject.length(); i++) {
									JSONObject firstObjectOfData = jsonarrayobject.getJSONObject(i);
									System.out.println("firstObjectOfData 1 ===== "+firstObjectOfData.toString());

									currentFundBalance = currentFundBalance + firstObjectOfData.getDouble("currentFundBalance");

								}

								String currentFundBalanceN = String.valueOf(currentFundBalance);

								if (currentFundBalanceN.contains(var_deathBenefit)){

									writeToCSV("Death Benefit",var_deathBenefit);
								}else{
									var_policyResult = "FAIL";
									var_failedResults = var_failedResults + " , Death Benefit =" +currentFundBalanceN+" ";
									writeToCSV("Death Benefit",var_deathBenefit);
								}



							}

					}catch (Exception e){

						writeToCSV("Death Benefit",var_deathBenefit);
						var_failedResults = var_failedResults  + " , API OS252S1 is not working";


						PrintVal("Renderer Script Exception : "+e.toString());
					}






						
				 
				 
		 
		 ;} 
		 catch(Exception e) { PrintVal("Renderer Script Exception : "+e.toString()); } 
		 PrintVal("Executed Step = CALLRENDERERSCRIPT,GETPOLICYDATAAPI_POS252S1"); 
        }
          LOGGER.info("Executed Step = ENDIF,TGTYPESCREENREG");
		try { 
		 				writeToCSV("PolicyResult",var_policyResult);

						if (var_failedResults.equals("Fields = ")){
							var_failedResults = "";
						}
						writeToCSV("FailedField",var_failedResults);
		 
		 ;} 
		 catch(Exception e) { PrintVal("Renderer Script Exception : "+e.toString()); } 
		 PrintVal("Executed Step = CALLRENDERERSCRIPT,FINALPOLICYRESULTS"); 
        WAIT.$(5,"TGTYPESCREENREG");

		var_Count = var_Count + ( 1 );
                 LOGGER.info("Executed Step = INCREASEVAR,var_Count,1,TGTYPESCREENREG");
    	}
          LOGGER.info("Executed Step = WEND,TGTYPESCREENREG");
    	resetAttemps();

    }


    @Test
    public void tempapitesting() {

        // No need to declare / print method name here, handled by TestNg listener
        // declare("tempapitesting");

		try { 
		 String wsURL = "http://cis-coreapd1.btoins.ibm.com:17000/ws";
			URL url = null;
			URLConnection connection = null;
			HttpURLConnection httpConn = null;
			String responseString = null;
			String outputString="";
			OutputStream out = null;
			InputStreamReader isr = null;
			BufferedReader in = null;

			String xmlInput = "<SOAP-ENV:Envelope xmlns:SOAP-ENV=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:q0=\"http://policyvalues.ws.gias.concentrix.com\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"> <SOAP-ENV:Header> <wsse:Security SOAP-ENV:mustUnderstand=\"1\" xmlns:wsse=\"http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd\"> <wsse:UsernameToken xmlns:wsu=\"http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd\" wsu:ID=\"UsernameToken-400627681\"> <wsse:Username>autoapi</wsse:Username> <wsse:Password Type=\"http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-username-token-profile-1.0#PasswordText\">Autoapi!23</wsse:Password> </wsse:UsernameToken> </wsse:Security> </SOAP-ENV:Header> <SOAP-ENV:Body> <q0:PolicyCashValueRequest> <calculationDate>2021-05-18</calculationDate> <companyCode>CO1</companyCode> <policyNumber>1000929</policyNumber> </q0:PolicyCashValueRequest> </SOAP-ENV:Body> </SOAP-ENV:Envelope>";
			
			try
			{
				url = new URL(wsURL);
				connection = url.openConnection();
				httpConn = (HttpURLConnection) connection;

				byte[] buffer = new byte[xmlInput.length()];
				buffer = xmlInput.getBytes();

				String SOAPAction = "";
				// Set the appropriate HTTP parameters.
		//		httpConn.setRequestProperty("Content-Length", String
		//				.valueOf(buffer.length));
				httpConn.setRequestProperty("Content-Type",
						"text/xml; charset=utf-8");


				httpConn.setRequestProperty("SOAPAction", SOAPAction);
				httpConn.setRequestMethod("POST");
				httpConn.setDoOutput(true);
				httpConn.setDoInput(true);
				out = httpConn.getOutputStream();
				out.write(buffer);
				out.close();

				// Read the response and write it to standard out.
				isr = new InputStreamReader(httpConn.getInputStream());
				in = new BufferedReader(isr);

				while ((responseString = in.readLine()) != null)
				{
					outputString = outputString + responseString;
				}
				System.out.println(outputString);
				System.out.println("");

		javax.xml.parsers.DocumentBuilderFactory dbf = javax.xml.parsers.DocumentBuilderFactory.newInstance();
					javax.xml.parsers.DocumentBuilder db = dbf.newDocumentBuilder();
					org.xml.sax.InputSource is = new org.xml.sax.InputSource(new StringReader(outputString));
					org.w3c.dom.Document document = db.parse(is);
				    org.w3c.dom.NodeList nodeLst = document.getElementsByTagName("ns3:PolicyCashValueResponse");
				    String webServiceResponse = nodeLst.item(0).getTextContent();
				System.out.println("The response from the web service call is : " + webServiceResponse);
			     	org.w3c.dom.Node node = nodeLst.item(0);

				if (node.getNodeType() == org.w3c.dom.Node.ELEMENT_NODE) {
					org.w3c.dom.Element  eElement = (org.w3c.dom.Element) node.getChildNodes();

					String netCashValue11 = eElement.getElementsByTagName("netCashValue").item(0).getTextContent();
					System.out.println("netCashValue : " + netCashValue11);
					netCashValue11 = String.valueOf((int) Double.parseDouble(netCashValue11));
					System.out.println("netCashValue : " + netCashValue11);


					String dividendAccum = eElement.getElementsByTagName("dividendAccum").item(0).getTextContent();
					System.out.println("dividendAccum : " + dividendAccum);
					dividendAccum = String.valueOf((int) Double.parseDouble(dividendAccum));
					System.out.println("dividendAccum : " + dividendAccum);
				}
				     System.out.println("The response from the web service call is : " + webServiceResponse);


					
			}
			catch (Exception e)
			{
				e.printStackTrace();
			}
		 
		 ;} 
		 catch(Exception e) { PrintVal("Renderer Script Exception : "+e.toString()); } 
		 PrintVal("Executed Step = CALLRENDERERSCRIPT,SOAP_API_NEW"); 

    }


    @Test
    public void tempreinsurance() {

        // No need to declare / print method name here, handled by TestNg listener
        // declare("tempreinsurance");

		int var_CountPolicy = 0;
                 LOGGER.info("Executed Step = VAR,Integer,var_CountPolicy,0,TGTYPESCREENREG");
		try { 
		 		         ArrayList<String> list = getTextFileToArrayList("//cis-coreapd1/CORE/NWITG1/TRANSFER/NW/Outbound/NationWide/Reinsurance/NWReinsurance.txt");
				        ArrayList<NWReinsurance> NWReinsurance = new ArrayList<NWReinsurance>();

				        for (int i = 0; i < list.size(); i++) {
				          NWReinsurance Reinsurance = new NWReinsurance();
						            Reinsurance.processingCompanyNumber = getSubStringFromString(list.get(i), 1, 3);
						            Reinsurance.policyNumber = getSubStringFromString(list.get(i), 4, 13);
						            Reinsurance.base_riderSequence = getSubStringFromString(list.get(i),14,15);
						            Reinsurance.policyStatus = getSubStringFromString(list.get(i), 16, 18);
						           // Reinsurance.addedToFileDate = getSubStringFromString(list.get(i),19,26);
								     Reinsurance.addedToFileDate = getSubStringFromString(list.get(i),19,26);
		//				            String YearFromString = getSubStringFromString(DateString,1,4);
		//							String MonthFromString = getSubStringFromString(DateString,5,6);
		////							String DateFromString = getSubStringFromString(DateString,7,8);
		//							String DateFinal = DateString;
		//							Reinsurance.addedToFileDate = DateFinal;


									Reinsurance.paidToDate = getSubStringFromString(list.get(i),27,34);
		//							 YearFromString = getSubStringFromString(DateString,1,4);
		//							 MonthFromString = getSubStringFromString(DateString,5,6);
		//							 DateFromString = getSubStringFromString(DateString,7,8);
		//							 DateFinal = MonthFromString +"/"+ DateFromString + "/" + YearFromString;
		//							 Reinsurance.paidToDate = DateFinal;


							//Reinsurance.statusChangeDate  = getSubStringFromString(list.get(i), 35, 42);
		//							YearFromString = getSubStringFromString(DateString,1,4);
		//							MonthFromString = getSubStringFromString(DateString,5,6);
		//							DateFromString = getSubStringFromString(DateString,7,8);
		//							DateFinal = MonthFromString +"/"+ DateFromString + "/" + YearFromString;
		//							Reinsurance.statusChangeDate = DateFinal;

						            Reinsurance.statusChangeDate = getSubStringFromString(list.get(i), 35, 42);
						            Reinsurance.issueType = getSubStringFromString(list.get(i), 43, 43);
						            Reinsurance.planCode = getSubStringFromString(list.get(i),44,51);
									Reinsurance.productCode = getSubStringFromString(list.get(i), 52, 52);
						            Reinsurance.productCode1 = getSubStringFromString(list.get(i), 53, 53);
						            Reinsurance.productCode2 = getSubStringFromString(list.get(i), 54, 54);
						            Reinsurance.administrationReinsuranceSwitch = getSubStringFromString(list.get(i), 55, 55);
						            Reinsurance.issueState = getSubStringFromString(list.get(i),56,57);
						            Reinsurance.ownerState = getSubStringFromString(list.get(i),58,59);
						            Reinsurance.underwritingMethod = getSubStringFromString(list.get(i), 60, 60);
						            Reinsurance.participatingIndicator = getSubStringFromString(list.get(i),61,61);
						            Reinsurance.deathBenefitOption = getSubStringFromString(list.get(i), 62, 62);
						            Reinsurance.faceAmount = getSubStringFromString(list.get(i),63,71);
						            Reinsurance.mortalityRating = getSubStringFromString(list.get(i), 72, 75);
						            Reinsurance.faceAmount2ADB = getSubStringFromString(list.get(i), 76, 84);
						            Reinsurance.adbTableRating = getSubStringFromString(list.get(i), 85, 88);
						            Reinsurance.faceAmount3WP = getSubStringFromString(list.get(i), 89, 97);
						            Reinsurance.wpTableRating = getSubStringFromString(list.get(i), 98, 101);
						            Reinsurance.ultimateFaceAmount = getSubStringFromString(list.get(i), 102, 110);
						            Reinsurance.jointType = getSubStringFromString(list.get(i), 111, 111);
						            Reinsurance.life = getSubStringFromString(list.get(i),112,113);
						            Reinsurance.jointAge = getSubStringFromString(list.get(i), 114, 115);
						            Reinsurance.premiumWaived = getSubStringFromString(list.get(i), 116, 124);
						            Reinsurance.specialPremium = getSubStringFromString(list.get(i), 125, 133);
						            Reinsurance.specialPremiumType = getSubStringFromString(list.get(i), 134, 134);
						            Reinsurance.dividend = getSubStringFromString(list.get(i), 135, 143);
						            Reinsurance.oldPolicyNumber = getSubStringFromString(list.get(i),144,153);
						            Reinsurance.administrationCode = getSubStringFromString(list.get(i), 154, 154);
						            Reinsurance.policyFee = getSubStringFromString(list.get(i), 155, 159);
						            Reinsurance.groupId = getSubStringFromString(list.get(i), 160, 174);
						            Reinsurance.locationCode = getSubStringFromString(list.get(i), 175, 184);
						            Reinsurance.currencyCode = getSubStringFromString(list.get(i), 185, 187);
						            Reinsurance.valuesYear = getSubStringFromString(list.get(i), 188, 191);
						            Reinsurance.valuesMonth = getSubStringFromString(list.get(i), 192, 193);
						            Reinsurance.deathBenefit = getSubStringFromString(list.get(i), 194, 202);
						            Reinsurance.cashValue = getSubStringFromString(list.get(i), 203, 211);
						            Reinsurance.nextCashValue = getSubStringFromString(list.get(i), 212, 220);
						            Reinsurance.premiumForBaseCoverage = getSubStringFromString(list.get(i), 221, 229);
						            Reinsurance.premiumForAccidentalDeathBenefit = getSubStringFromString(list.get(i), 230, 238);
						            Reinsurance.premiumForWaiver = getSubStringFromString(list.get(i), 239, 247);
						            Reinsurance.filler = getSubStringFromString(list.get(i), 248, 278);
						            Reinsurance.clientId = getSubStringFromString(list.get(i), 279, 293);
						            Reinsurance.lastName = getSubStringFromString(list.get(i), 294, 313);
						            Reinsurance.firstName = getSubStringFromString(list.get(i), 314, 328);
						            Reinsurance.middleInitials = getSubStringFromString(list.get(i), 329, 329);
						            Reinsurance.pricingSex = getSubStringFromString(list.get(i), 330, 330);
						            Reinsurance.sex = getSubStringFromString(list.get(i), 331, 331);

		//							DateString = getSubStringFromString(list.get(i), 332, 339);
		//							YearFromString = getSubStringFromString(DateString,1,4);
		//							MonthFromString = getSubStringFromString(DateString,5,6);
		//							DateFromString = getSubStringFromString(DateString,7,8);
		//							DateFinal = MonthFromString +"/"+ DateFromString + "/" + YearFromString;
		//							Reinsurance.dateOfBirth = DateFinal;

						            Reinsurance.dateOfBirth = getSubStringFromString(list.get(i), 332, 339);
						            Reinsurance.insuredStatus = getSubStringFromString(list.get(i), 340, 340);
						            Reinsurance.issueAge = getSubStringFromString(list.get(i), 341, 342);
						            Reinsurance.premiumClass = getSubStringFromString(list.get(i), 343, 344);
						            Reinsurance.mortalityRating1 = getSubStringFromString(list.get(i), 345, 348);
						            Reinsurance.mortalityDuration = getSubStringFromString(list.get(i), 349, 350);
						            Reinsurance.permanentFlatExtraRate = getSubStringFromString(list.get(i), 351, 355);
						            Reinsurance.permanentFlatExtraPremiumRateDuration = getSubStringFromString(list.get(i), 356, 357);
						            Reinsurance.temporaryFlatExtraRate = getSubStringFromString(list.get(i), 358, 362);
						            Reinsurance.temporaryFlatExtraPremiumRateDuration = getSubStringFromString(list.get(i), 363, 364);
						            Reinsurance.clientIdSecondaryInsured = getSubStringFromString(list.get(i), 365, 379);
						            Reinsurance.lastNameSecondaryInsured = getSubStringFromString(list.get(i), 380, 399);
						            Reinsurance.firstNameSecondaryInsured = getSubStringFromString(list.get(i), 400, 414);
						            Reinsurance.middleInitialsSecondaryInsured = getSubStringFromString(list.get(i), 415, 415);
						            Reinsurance.pricingSexSecondaryInsured = getSubStringFromString(list.get(i), 416, 416);
						            Reinsurance.sexSecondaryInsured = getSubStringFromString(list.get(i), 417, 417);



		//							DateString = getSubStringFromString(list.get(i), 418, 425);
		//							YearFromString = getSubStringFromString(DateString,1,4);
		//							MonthFromString = getSubStringFromString(DateString,5,6);
		//							DateFromString = getSubStringFromString(DateString,7,8);
		//							DateFinal = MonthFromString +"/"+ DateFromString + "/" + YearFromString;
		//							Reinsurance.dateOfBirthSecondaryInsured = DateFinal;


						            Reinsurance.dateOfBirthSecondaryInsured = getSubStringFromString(list.get(i), 418, 425);
						            Reinsurance.insuredStatusSecondaryInsured = getSubStringFromString(list.get(i), 426, 426);
						            Reinsurance.issueAgeSecondaryInsured = getSubStringFromString(list.get(i), 427, 428);
						            Reinsurance.premiumClassSecondaryInsured = getSubStringFromString(list.get(i), 429, 430);
						            Reinsurance.mortalityRating1SecondaryInsured = getSubStringFromString(list.get(i), 431, 434);
						            Reinsurance.mortalityDurationSecondaryInsured = getSubStringFromString(list.get(i), 435, 436);
						            Reinsurance.permanentFlatExtraRateSecondaryInsured = getSubStringFromString(list.get(i), 437, 441);
						            Reinsurance.permanentFlatExtraPremiumRateDurationSecondaryInsured = getSubStringFromString(list.get(i), 442, 443);
						            Reinsurance.temporaryFlatExtraRateSecondaryInsured = getSubStringFromString(list.get(i), 444, 448);
						            Reinsurance.temporaryFlatExtraPremiumRateDurationSecondaryInsured = getSubStringFromString(list.get(i), 449, 450);
						            Reinsurance.occupationClass = getSubStringFromString(list.get(i), 365, 366);
						            Reinsurance.colaPercentage = getSubStringFromString(list.get(i), 367, 369);
						            Reinsurance.accidentBenefitMode = getSubStringFromString(list.get(i), 370, 370);
						            Reinsurance.accidentBenefitPeriod = getSubStringFromString(list.get(i), 371, 373);
						            Reinsurance.accidentEliminationPeriod = getSubStringFromString(list.get(i), 374, 376);
						            Reinsurance.sicknessBenefitMode = getSubStringFromString(list.get(i), 377, 377);
						            Reinsurance.sicknessBenefitPeriod = getSubStringFromString(list.get(i), 378, 380);
						            Reinsurance.sicknessEliminationPeriod = getSubStringFromString(list.get(i), 381, 383);
						            Reinsurance.diBenefit = getSubStringFromString(list.get(i), 384, 399);
						            Reinsurance.filler2 = getSubStringFromString(list.get(i), 400, 450);
						            Reinsurance.ltcBeginningDate = getSubStringFromString(list.get(i), 365, 367);
						            Reinsurance.ltcBenefitAccount = getSubStringFromString(list.get(i), 368, 376);
						            Reinsurance.ltcIncreaseOption = getSubStringFromString(list.get(i), 377, 377);
						            Reinsurance.ltcIncreasePercentage = getSubStringFromString(list.get(i), 378, 380);
						            Reinsurance.ltcBenefitPeriodMode = getSubStringFromString(list.get(i), 381, 381);
						            Reinsurance.ltcBenefitPeriod = getSubStringFromString(list.get(i), 382, 384);
						            Reinsurance.filler3 = getSubStringFromString(list.get(i), 385, 450);
						            Reinsurance.suspendedCoverPercentage = getSubStringFromString(list.get(i), 451, 453);
						            Reinsurance.pml_CV_D100_ADDS = getSubStringFromString(list.get(i), 454, 464);
						            Reinsurance.pml_LAST_ACCTNG_STORED = getSubStringFromString(list.get(i), 465, 472);
						            Reinsurance.pml_GMDBR_TYPE = getSubStringFromString(list.get(i), 473, 473);
						            Reinsurance.pml_RDR_PROCESS = getSubStringFromString(list.get(i), 474, 474);
						            Reinsurance.pml_GDBR_OVRD_LAPSE_SW = getSubStringFromString(list.get(i), 475, 475);
						            Reinsurance.pml_GDBR_GRACE_IND = getSubStringFromString(list.get(i), 476, 476);
						            Reinsurance.pml_GDBR_LAPSE_ACCT_VALUE = getSubStringFromString(list.get(i), 477, 485);
						            Reinsurance.pml_GDBR_ACCUM_LAPSE_PREM = getSubStringFromString(list.get(i), 486, 492);
						            Reinsurance.pml_GDBR_ACCUM_PREM = getSubStringFromString(list.get(i), 493, 503);
						            Reinsurance.pml_GDBR_COMM_PREM = getSubStringFromString(list.get(i), 504, 514);
						            Reinsurance.pml_GDBR_MIN_PREM = getSubStringFromString(list.get(i), 515, 525);
						            Reinsurance.pml_CVOVER_DIVOVER = getSubStringFromString(list.get(i), 526, 526);
						            Reinsurance.pml_ATP_TDO_PREM = getSubStringFromString(list.get(i), 527, 533);
						            Reinsurance.pml_CLOSED_PLAN_SW = getSubStringFromString(list.get(i), 534, 534);
						            Reinsurance.pml_IMAC_COV_FORM = getSubStringFromString(list.get(i), 535, 538);
						            Reinsurance.pml_IMAC_COV_B4M = getSubStringFromString(list.get(i), 539, 543);
						            Reinsurance.pml_NEXT_RIDER = getSubStringFromString(list.get(i), 544, 545);
						            Reinsurance.pml_REINS_AGREEMENT = getSubStringFromString(list.get(i), 546, 554);
						            Reinsurance.pml_COV_REL = getSubStringFromString(list.get(i), 555, 555);
						            Reinsurance.pml_POL_FEE = getSubStringFromString(list.get(i), 556, 560);
						            Reinsurance.pml_UNIT_PREM = getSubStringFromString(list.get(i), 561, 569);
						            Reinsurance.pml_ADDS_CV_SW = getSubStringFromString(list.get(i), 570, 570);
						            Reinsurance.pml_LFP_AMT = getSubStringFromString(list.get(i), 571, 579);
						            Reinsurance.pml_ATP_TDO_AMT = getSubStringFromString(list.get(i), 580, 588);
						            Reinsurance.pml_PUI_AMT = getSubStringFromString(list.get(i), 589, 597);
						            Reinsurance.pml_PUO_AMT = getSubStringFromString(list.get(i), 598, 606);
						            Reinsurance.pml_PERM_ADDS = getSubStringFromString(list.get(i), 607, 615);
						            Reinsurance.pml_OLD_CO = getSubStringFromString(list.get(i), 616, 618);
						            Reinsurance.pml_VAL_EXTRA_CD = getSubStringFromString(list.get(i), 619, 619);
						            Reinsurance.group_Id = getSubStringFromString(list.get(i), 620, 629);
						            Reinsurance.filter_4 = getSubStringFromString(list.get(i), 630, 650);

						            NWReinsurance.add(Reinsurance);
				        }

				              var_CountPolicy = NWReinsurance.size();
						System.out.println("Model Completed");
							LOGGER.info("Model Completed");

				        try (Writer writer = new FileWriter("C:/xampp/htdocs/testgrid/NWReinsurance/Reinsurance.json")) {

				       Gson gson = new GsonBuilder().create();

				            com.google.gson.JsonObject main = new com.google.gson.JsonObject();


				            com.google.gson.JsonArray records = new Gson().toJsonTree(NWReinsurance).getAsJsonArray();
				            main.add("records",records);
				            gson.toJson(main,writer);
											System.out.println("JSON Completed");
											LOGGER.info("JSON Completed");
				        } catch (IOException e) {
											System.out.println("JSON error");
											LOGGER.info("JSON error");
				            e.printStackTrace();
				        }
							System.out.println("Completed");
							LOGGER.info("Completed");


				    
				 
				 
		 
		 ;} 
		 catch(Exception e) { PrintVal("Renderer Script Exception : "+e.toString()); } 
		 PrintVal("Executed Step = CALLRENDERERSCRIPT,TC01REINSURENCECONVERTTXTTOJSON"); 
		int var_Count = 0;
                 LOGGER.info("Executed Step = VAR,Integer,var_Count,0,TGTYPESCREENREG");
		com.jayway.jsonpath.ReadContext var_CSVData = getJsonData("C:/xampp/htdocs/testgrid/NWReinsurance/Reinsurance.json");
                 LOGGER.info("Executed Step = VAR,Json,var_CSVData,C:/xampp/htdocs/testgrid/NWReinsurance/Reinsurance.json,TGTYPESCREENREG");
		String var_policyResult = "PASS";
                 LOGGER.info("Executed Step = VAR,String,var_policyResult,PASS,TGTYPESCREENREG");
		String var_failedResults = "Fields = ";
                 LOGGER.info("Executed Step = VAR,String,var_failedResults,Fields = ,TGTYPESCREENREG");
		String var_apiResult = "FAIL";
                 LOGGER.info("Executed Step = VAR,String,var_apiResult,FAIL,TGTYPESCREENREG");
        LOGGER.info("Executed Step = REPEAT IF");
    	while (check(var_Count,"<",var_CountPolicy,"TGTYPESCREENREG")) {
          LOGGER.info("Executed Step = WHILE,var_Count,<,var_CountPolicy,TGTYPESCREENREG");
        CALL.$("TC01ReinsuranceVariableDeclarationFirst","TGTYPESCREENREG");

		String var_processingCompanyNumber;
                 LOGGER.info("Executed Step = VAR,String,var_processingCompanyNumber,TGTYPESCREENREG");
		var_processingCompanyNumber = getJsonData(var_CSVData , "$.records["+var_Count+"].processingCompanyNumber");
                 LOGGER.info("Executed Step = STORE,var_processingCompanyNumber,var_CSVData,$.records[{var_Count}].processingCompanyNumber,TGTYPESCREENREG");
		String var_policyNumber;
                 LOGGER.info("Executed Step = VAR,String,var_policyNumber,TGTYPESCREENREG");
		var_policyNumber = getJsonData(var_CSVData , "$.records["+var_Count+"].policyNumber");
                 LOGGER.info("Executed Step = STORE,var_policyNumber,var_CSVData,$.records[{var_Count}].policyNumber,TGTYPESCREENREG");
		String var_baseriderSequence;
                 LOGGER.info("Executed Step = VAR,String,var_baseriderSequence,TGTYPESCREENREG");
		var_baseriderSequence = getJsonData(var_CSVData , "$.records["+var_Count+"].base_riderSequence");
                 LOGGER.info("Executed Step = STORE,var_baseriderSequence,var_CSVData,$.records[{var_Count}].base_riderSequence,TGTYPESCREENREG");
		String var_policyStatus;
                 LOGGER.info("Executed Step = VAR,String,var_policyStatus,TGTYPESCREENREG");
		var_policyStatus = getJsonData(var_CSVData , "$.records["+var_Count+"].policyStatus");
                 LOGGER.info("Executed Step = STORE,var_policyStatus,var_CSVData,$.records[{var_Count}].policyStatus,TGTYPESCREENREG");
		String var_addedToFileDate;
                 LOGGER.info("Executed Step = VAR,String,var_addedToFileDate,TGTYPESCREENREG");
		var_addedToFileDate = getJsonData(var_CSVData , "$.records["+var_Count+"].addedToFileDate");
                 LOGGER.info("Executed Step = STORE,var_addedToFileDate,var_CSVData,$.records[{var_Count}].addedToFileDate,TGTYPESCREENREG");
		String var_paidToDate;
                 LOGGER.info("Executed Step = VAR,String,var_paidToDate,TGTYPESCREENREG");
		var_paidToDate = getJsonData(var_CSVData , "$.records["+var_Count+"].paidToDate");
                 LOGGER.info("Executed Step = STORE,var_paidToDate,var_CSVData,$.records[{var_Count}].paidToDate,TGTYPESCREENREG");
		String var_statusChangeDate;
                 LOGGER.info("Executed Step = VAR,String,var_statusChangeDate,TGTYPESCREENREG");
		var_statusChangeDate = getJsonData(var_CSVData , "$.records["+var_Count+"].statusChangeDate");
                 LOGGER.info("Executed Step = STORE,var_statusChangeDate,var_CSVData,$.records[{var_Count}].statusChangeDate,TGTYPESCREENREG");
		String var_issueType;
                 LOGGER.info("Executed Step = VAR,String,var_issueType,TGTYPESCREENREG");
		var_issueType = getJsonData(var_CSVData , "$.records["+var_Count+"].issueType");
                 LOGGER.info("Executed Step = STORE,var_issueType,var_CSVData,$.records[{var_Count}].issueType,TGTYPESCREENREG");
		String var_planCode;
                 LOGGER.info("Executed Step = VAR,String,var_planCode,TGTYPESCREENREG");
		var_planCode = getJsonData(var_CSVData , "$.records["+var_Count+"].planCode");
                 LOGGER.info("Executed Step = STORE,var_planCode,var_CSVData,$.records[{var_Count}].planCode,TGTYPESCREENREG");
		String var_productCode;
                 LOGGER.info("Executed Step = VAR,String,var_productCode,TGTYPESCREENREG");
		var_productCode = getJsonData(var_CSVData , "$.records["+var_Count+"].productCode");
                 LOGGER.info("Executed Step = STORE,var_productCode,var_CSVData,$.records[{var_Count}].productCode,TGTYPESCREENREG");
		String var_productCode1;
                 LOGGER.info("Executed Step = VAR,String,var_productCode1,TGTYPESCREENREG");
		var_productCode1 = getJsonData(var_CSVData , "$.records["+var_Count+"].productCode1");
                 LOGGER.info("Executed Step = STORE,var_productCode1,var_CSVData,$.records[{var_Count}].productCode1,TGTYPESCREENREG");
		String var_productCode2;
                 LOGGER.info("Executed Step = VAR,String,var_productCode2,TGTYPESCREENREG");
		var_productCode2 = getJsonData(var_CSVData , "$.records["+var_Count+"].productCode2");
                 LOGGER.info("Executed Step = STORE,var_productCode2,var_CSVData,$.records[{var_Count}].productCode2,TGTYPESCREENREG");
		String var_administrationReinsuranceSwitch;
                 LOGGER.info("Executed Step = VAR,String,var_administrationReinsuranceSwitch,TGTYPESCREENREG");
		var_administrationReinsuranceSwitch = getJsonData(var_CSVData , "$.records["+var_Count+"].administrationReinsuranceSwitch");
                 LOGGER.info("Executed Step = STORE,var_administrationReinsuranceSwitch,var_CSVData,$.records[{var_Count}].administrationReinsuranceSwitch,TGTYPESCREENREG");
		String var_issueState;
                 LOGGER.info("Executed Step = VAR,String,var_issueState,TGTYPESCREENREG");
		var_issueState = getJsonData(var_CSVData , "$.records["+var_Count+"].issueState");
                 LOGGER.info("Executed Step = STORE,var_issueState,var_CSVData,$.records[{var_Count}].issueState,TGTYPESCREENREG");
		String var_ownerState;
                 LOGGER.info("Executed Step = VAR,String,var_ownerState,TGTYPESCREENREG");
		var_ownerState = getJsonData(var_CSVData , "$.records["+var_Count+"].ownerState");
                 LOGGER.info("Executed Step = STORE,var_ownerState,var_CSVData,$.records[{var_Count}].ownerState,TGTYPESCREENREG");
		String var_underwritingMethod;
                 LOGGER.info("Executed Step = VAR,String,var_underwritingMethod,TGTYPESCREENREG");
		var_underwritingMethod = getJsonData(var_CSVData , "$.records["+var_Count+"].underwritingMethod");
                 LOGGER.info("Executed Step = STORE,var_underwritingMethod,var_CSVData,$.records[{var_Count}].underwritingMethod,TGTYPESCREENREG");
		String var_participatingIndicator;
                 LOGGER.info("Executed Step = VAR,String,var_participatingIndicator,TGTYPESCREENREG");
		var_participatingIndicator = getJsonData(var_CSVData , "$.records["+var_Count+"].participatingIndicator");
                 LOGGER.info("Executed Step = STORE,var_participatingIndicator,var_CSVData,$.records[{var_Count}].participatingIndicator,TGTYPESCREENREG");
		String var_deathBenefitOption;
                 LOGGER.info("Executed Step = VAR,String,var_deathBenefitOption,TGTYPESCREENREG");
		var_deathBenefitOption = getJsonData(var_CSVData , "$.records["+var_Count+"].deathBenefitOption");
                 LOGGER.info("Executed Step = STORE,var_deathBenefitOption,var_CSVData,$.records[{var_Count}].deathBenefitOption,TGTYPESCREENREG");
		String var_faceAmount;
                 LOGGER.info("Executed Step = VAR,String,var_faceAmount,TGTYPESCREENREG");
		var_faceAmount = getJsonData(var_CSVData , "$.records["+var_Count+"].faceAmount");
                 LOGGER.info("Executed Step = STORE,var_faceAmount,var_CSVData,$.records[{var_Count}].faceAmount,TGTYPESCREENREG");
		String var_mortalityRating;
                 LOGGER.info("Executed Step = VAR,String,var_mortalityRating,TGTYPESCREENREG");
		var_mortalityRating = getJsonData(var_CSVData , "$.records["+var_Count+"].mortalityRating");
                 LOGGER.info("Executed Step = STORE,var_mortalityRating,var_CSVData,$.records[{var_Count}].mortalityRating,TGTYPESCREENREG");
		String var_faceAmount2ADB;
                 LOGGER.info("Executed Step = VAR,String,var_faceAmount2ADB,TGTYPESCREENREG");
		var_faceAmount2ADB = getJsonData(var_CSVData , "$.records["+var_Count+"].faceAmount2ADB");
                 LOGGER.info("Executed Step = STORE,var_faceAmount2ADB,var_CSVData,$.records[{var_Count}].faceAmount2ADB,TGTYPESCREENREG");
		String var_adbTableRating;
                 LOGGER.info("Executed Step = VAR,String,var_adbTableRating,TGTYPESCREENREG");
		var_adbTableRating = getJsonData(var_CSVData , "$.records["+var_Count+"].adbTableRating");
                 LOGGER.info("Executed Step = STORE,var_adbTableRating,var_CSVData,$.records[{var_Count}].adbTableRating,TGTYPESCREENREG");
		String var_faceAmount3WP;
                 LOGGER.info("Executed Step = VAR,String,var_faceAmount3WP,TGTYPESCREENREG");
		var_faceAmount3WP = getJsonData(var_CSVData , "$.records["+var_Count+"].faceAmount3WP");
                 LOGGER.info("Executed Step = STORE,var_faceAmount3WP,var_CSVData,$.records[{var_Count}].faceAmount3WP,TGTYPESCREENREG");
		String var_wpTableRating;
                 LOGGER.info("Executed Step = VAR,String,var_wpTableRating,TGTYPESCREENREG");
		var_wpTableRating = getJsonData(var_CSVData , "$.records["+var_Count+"].wpTableRating");
                 LOGGER.info("Executed Step = STORE,var_wpTableRating,var_CSVData,$.records[{var_Count}].wpTableRating,TGTYPESCREENREG");
		String var_ultimateFaceAmount;
                 LOGGER.info("Executed Step = VAR,String,var_ultimateFaceAmount,TGTYPESCREENREG");
		var_ultimateFaceAmount = getJsonData(var_CSVData , "$.records["+var_Count+"].ultimateFaceAmount");
                 LOGGER.info("Executed Step = STORE,var_ultimateFaceAmount,var_CSVData,$.records[{var_Count}].ultimateFaceAmount,TGTYPESCREENREG");
		String var_jointType;
                 LOGGER.info("Executed Step = VAR,String,var_jointType,TGTYPESCREENREG");
		var_jointType = getJsonData(var_CSVData , "$.records["+var_Count+"].jointType");
                 LOGGER.info("Executed Step = STORE,var_jointType,var_CSVData,$.records[{var_Count}].jointType,TGTYPESCREENREG");
		String var_life;
                 LOGGER.info("Executed Step = VAR,String,var_life,TGTYPESCREENREG");
		var_life = getJsonData(var_CSVData , "$.records["+var_Count+"].life");
                 LOGGER.info("Executed Step = STORE,var_life,var_CSVData,$.records[{var_Count}].life,TGTYPESCREENREG");
		String var_jointAge;
                 LOGGER.info("Executed Step = VAR,String,var_jointAge,TGTYPESCREENREG");
		var_jointAge = getJsonData(var_CSVData , "$.records["+var_Count+"].jointAge");
                 LOGGER.info("Executed Step = STORE,var_jointAge,var_CSVData,$.records[{var_Count}].jointAge,TGTYPESCREENREG");
		String var_premiumWaived;
                 LOGGER.info("Executed Step = VAR,String,var_premiumWaived,TGTYPESCREENREG");
		var_premiumWaived = getJsonData(var_CSVData , "$.records["+var_Count+"].premiumWaived");
                 LOGGER.info("Executed Step = STORE,var_premiumWaived,var_CSVData,$.records[{var_Count}].premiumWaived,TGTYPESCREENREG");
		String var_specialPremium;
                 LOGGER.info("Executed Step = VAR,String,var_specialPremium,TGTYPESCREENREG");
		var_specialPremium = getJsonData(var_CSVData , "$.records["+var_Count+"].specialPremium");
                 LOGGER.info("Executed Step = STORE,var_specialPremium,var_CSVData,$.records[{var_Count}].specialPremium,TGTYPESCREENREG");
		String var_specialPremiumType;
                 LOGGER.info("Executed Step = VAR,String,var_specialPremiumType,TGTYPESCREENREG");
		var_specialPremiumType = getJsonData(var_CSVData , "$.records["+var_Count+"].specialPremiumType");
                 LOGGER.info("Executed Step = STORE,var_specialPremiumType,var_CSVData,$.records[{var_Count}].specialPremiumType,TGTYPESCREENREG");
		String var_dividend;
                 LOGGER.info("Executed Step = VAR,String,var_dividend,TGTYPESCREENREG");
		var_dividend = getJsonData(var_CSVData , "$.records["+var_Count+"].dividend");
                 LOGGER.info("Executed Step = STORE,var_dividend,var_CSVData,$.records[{var_Count}].dividend,TGTYPESCREENREG");
		String var_oldPolicyNumber;
                 LOGGER.info("Executed Step = VAR,String,var_oldPolicyNumber,TGTYPESCREENREG");
		var_oldPolicyNumber = getJsonData(var_CSVData , "$.records["+var_Count+"].oldPolicyNumber");
                 LOGGER.info("Executed Step = STORE,var_oldPolicyNumber,var_CSVData,$.records[{var_Count}].oldPolicyNumber,TGTYPESCREENREG");
		String var_administrationCode;
                 LOGGER.info("Executed Step = VAR,String,var_administrationCode,TGTYPESCREENREG");
		var_administrationCode = getJsonData(var_CSVData , "$.records["+var_Count+"].administrationCode");
                 LOGGER.info("Executed Step = STORE,var_administrationCode,var_CSVData,$.records[{var_Count}].administrationCode,TGTYPESCREENREG");
		String var_policyFee;
                 LOGGER.info("Executed Step = VAR,String,var_policyFee,TGTYPESCREENREG");
		var_policyFee = getJsonData(var_CSVData , "$.records["+var_Count+"].policyFee");
                 LOGGER.info("Executed Step = STORE,var_policyFee,var_CSVData,$.records[{var_Count}].policyFee,TGTYPESCREENREG");
		String var_groupId;
                 LOGGER.info("Executed Step = VAR,String,var_groupId,TGTYPESCREENREG");
		var_groupId = getJsonData(var_CSVData , "$.records["+var_Count+"].groupId");
                 LOGGER.info("Executed Step = STORE,var_groupId,var_CSVData,$.records[{var_Count}].groupId,TGTYPESCREENREG");
		String var_locationCode;
                 LOGGER.info("Executed Step = VAR,String,var_locationCode,TGTYPESCREENREG");
		var_locationCode = getJsonData(var_CSVData , "$.records["+var_Count+"].locationCode");
                 LOGGER.info("Executed Step = STORE,var_locationCode,var_CSVData,$.records[{var_Count}].locationCode,TGTYPESCREENREG");
		String var_currencyCode;
                 LOGGER.info("Executed Step = VAR,String,var_currencyCode,TGTYPESCREENREG");
		var_currencyCode = getJsonData(var_CSVData , "$.records["+var_Count+"].currencyCode");
                 LOGGER.info("Executed Step = STORE,var_currencyCode,var_CSVData,$.records[{var_Count}].currencyCode,TGTYPESCREENREG");
		String var_valuesYear;
                 LOGGER.info("Executed Step = VAR,String,var_valuesYear,TGTYPESCREENREG");
		var_valuesYear = getJsonData(var_CSVData , "$.records["+var_Count+"].valuesYear");
                 LOGGER.info("Executed Step = STORE,var_valuesYear,var_CSVData,$.records[{var_Count}].valuesYear,TGTYPESCREENREG");
		String var_valuesMonth;
                 LOGGER.info("Executed Step = VAR,String,var_valuesMonth,TGTYPESCREENREG");
		var_valuesMonth = getJsonData(var_CSVData , "$.records["+var_Count+"].valuesMonth");
                 LOGGER.info("Executed Step = STORE,var_valuesMonth,var_CSVData,$.records[{var_Count}].valuesMonth,TGTYPESCREENREG");
		String var_deathBenefit;
                 LOGGER.info("Executed Step = VAR,String,var_deathBenefit,TGTYPESCREENREG");
		var_deathBenefit = getJsonData(var_CSVData , "$.records["+var_Count+"].deathBenefit");
                 LOGGER.info("Executed Step = STORE,var_deathBenefit,var_CSVData,$.records[{var_Count}].deathBenefit,TGTYPESCREENREG");
		String var_cashValue;
                 LOGGER.info("Executed Step = VAR,String,var_cashValue,TGTYPESCREENREG");
		var_cashValue = getJsonData(var_CSVData , "$.records["+var_Count+"].cashValue");
                 LOGGER.info("Executed Step = STORE,var_cashValue,var_CSVData,$.records[{var_Count}].cashValue,TGTYPESCREENREG");
		String var_nextCashValue;
                 LOGGER.info("Executed Step = VAR,String,var_nextCashValue,TGTYPESCREENREG");
		var_nextCashValue = getJsonData(var_CSVData , "$.records["+var_Count+"].nextCashValue");
                 LOGGER.info("Executed Step = STORE,var_nextCashValue,var_CSVData,$.records[{var_Count}].nextCashValue,TGTYPESCREENREG");
		String var_premiumForBaseCoverage;
                 LOGGER.info("Executed Step = VAR,String,var_premiumForBaseCoverage,TGTYPESCREENREG");
		var_premiumForBaseCoverage = getJsonData(var_CSVData , "$.records["+var_Count+"].premiumForBaseCoverage");
                 LOGGER.info("Executed Step = STORE,var_premiumForBaseCoverage,var_CSVData,$.records[{var_Count}].premiumForBaseCoverage,TGTYPESCREENREG");
		String var_premiumForAccidentalDeathBenefit;
                 LOGGER.info("Executed Step = VAR,String,var_premiumForAccidentalDeathBenefit,TGTYPESCREENREG");
		var_premiumForAccidentalDeathBenefit = getJsonData(var_CSVData , "$.records["+var_Count+"].premiumForAccidentalDeathBenefit");
                 LOGGER.info("Executed Step = STORE,var_premiumForAccidentalDeathBenefit,var_CSVData,$.records[{var_Count}].premiumForAccidentalDeathBenefit,TGTYPESCREENREG");
		String var_premiumForWaiver;
                 LOGGER.info("Executed Step = VAR,String,var_premiumForWaiver,TGTYPESCREENREG");
		var_premiumForWaiver = getJsonData(var_CSVData , "$.records["+var_Count+"].premiumForWaiver");
                 LOGGER.info("Executed Step = STORE,var_premiumForWaiver,var_CSVData,$.records[{var_Count}].premiumForWaiver,TGTYPESCREENREG");
		String var_filler;
                 LOGGER.info("Executed Step = VAR,String,var_filler,TGTYPESCREENREG");
		var_filler = getJsonData(var_CSVData , "$.records["+var_Count+"].filler");
                 LOGGER.info("Executed Step = STORE,var_filler,var_CSVData,$.records[{var_Count}].filler,TGTYPESCREENREG");
		String var_clientId;
                 LOGGER.info("Executed Step = VAR,String,var_clientId,TGTYPESCREENREG");
		var_clientId = getJsonData(var_CSVData , "$.records["+var_Count+"].clientId");
                 LOGGER.info("Executed Step = STORE,var_clientId,var_CSVData,$.records[{var_Count}].clientId,TGTYPESCREENREG");
		String var_lastName;
                 LOGGER.info("Executed Step = VAR,String,var_lastName,TGTYPESCREENREG");
		var_lastName = getJsonData(var_CSVData , "$.records["+var_Count+"].lastName");
                 LOGGER.info("Executed Step = STORE,var_lastName,var_CSVData,$.records[{var_Count}].lastName,TGTYPESCREENREG");
		String var_firstName;
                 LOGGER.info("Executed Step = VAR,String,var_firstName,TGTYPESCREENREG");
		var_firstName = getJsonData(var_CSVData , "$.records["+var_Count+"].firstName");
                 LOGGER.info("Executed Step = STORE,var_firstName,var_CSVData,$.records[{var_Count}].firstName,TGTYPESCREENREG");
		String var_middleInitials;
                 LOGGER.info("Executed Step = VAR,String,var_middleInitials,TGTYPESCREENREG");
		var_middleInitials = getJsonData(var_CSVData , "$.records["+var_Count+"].middleInitials");
                 LOGGER.info("Executed Step = STORE,var_middleInitials,var_CSVData,$.records[{var_Count}].middleInitials,TGTYPESCREENREG");
		String var_pricingSex;
                 LOGGER.info("Executed Step = VAR,String,var_pricingSex,TGTYPESCREENREG");
		var_pricingSex = getJsonData(var_CSVData , "$.records["+var_Count+"].pricingSex");
                 LOGGER.info("Executed Step = STORE,var_pricingSex,var_CSVData,$.records[{var_Count}].pricingSex,TGTYPESCREENREG");
		String var_sex;
                 LOGGER.info("Executed Step = VAR,String,var_sex,TGTYPESCREENREG");
		var_sex = getJsonData(var_CSVData , "$.records["+var_Count+"].sex");
                 LOGGER.info("Executed Step = STORE,var_sex,var_CSVData,$.records[{var_Count}].sex,TGTYPESCREENREG");
		String var_dateOfBirth;
                 LOGGER.info("Executed Step = VAR,String,var_dateOfBirth,TGTYPESCREENREG");
		var_dateOfBirth = getJsonData(var_CSVData , "$.records["+var_Count+"].dateOfBirth");
                 LOGGER.info("Executed Step = STORE,var_dateOfBirth,var_CSVData,$.records[{var_Count}].dateOfBirth,TGTYPESCREENREG");
		String var_insuredStatus;
                 LOGGER.info("Executed Step = VAR,String,var_insuredStatus,TGTYPESCREENREG");
		var_insuredStatus = getJsonData(var_CSVData , "$.records["+var_Count+"].insuredStatus");
                 LOGGER.info("Executed Step = STORE,var_insuredStatus,var_CSVData,$.records[{var_Count}].insuredStatus,TGTYPESCREENREG");
		String var_issueAge;
                 LOGGER.info("Executed Step = VAR,String,var_issueAge,TGTYPESCREENREG");
		var_issueAge = getJsonData(var_CSVData , "$.records["+var_Count+"].issueAge");
                 LOGGER.info("Executed Step = STORE,var_issueAge,var_CSVData,$.records[{var_Count}].issueAge,TGTYPESCREENREG");
		String var_premiumClass;
                 LOGGER.info("Executed Step = VAR,String,var_premiumClass,TGTYPESCREENREG");
		var_premiumClass = getJsonData(var_CSVData , "$.records["+var_Count+"].premiumClass");
                 LOGGER.info("Executed Step = STORE,var_premiumClass,var_CSVData,$.records[{var_Count}].premiumClass,TGTYPESCREENREG");
		String var_mortalityRating1;
                 LOGGER.info("Executed Step = VAR,String,var_mortalityRating1,TGTYPESCREENREG");
		var_mortalityRating1 = getJsonData(var_CSVData , "$.records["+var_Count+"].mortalityRating1");
                 LOGGER.info("Executed Step = STORE,var_mortalityRating1,var_CSVData,$.records[{var_Count}].mortalityRating1,TGTYPESCREENREG");
		String var_mortalityDuration;
                 LOGGER.info("Executed Step = VAR,String,var_mortalityDuration,TGTYPESCREENREG");
		var_mortalityDuration = getJsonData(var_CSVData , "$.records["+var_Count+"].mortalityDuration");
                 LOGGER.info("Executed Step = STORE,var_mortalityDuration,var_CSVData,$.records[{var_Count}].mortalityDuration,TGTYPESCREENREG");
		String var_permanentFlatExtraRate;
                 LOGGER.info("Executed Step = VAR,String,var_permanentFlatExtraRate,TGTYPESCREENREG");
		var_permanentFlatExtraRate = getJsonData(var_CSVData , "$.records["+var_Count+"].permanentFlatExtraRate");
                 LOGGER.info("Executed Step = STORE,var_permanentFlatExtraRate,var_CSVData,$.records[{var_Count}].permanentFlatExtraRate,TGTYPESCREENREG");
		String var_permanentFlatExtraPremiumRateDuration;
                 LOGGER.info("Executed Step = VAR,String,var_permanentFlatExtraPremiumRateDuration,TGTYPESCREENREG");
		var_permanentFlatExtraPremiumRateDuration = getJsonData(var_CSVData , "$.records["+var_Count+"].permanentFlatExtraPremiumRateDuration");
                 LOGGER.info("Executed Step = STORE,var_permanentFlatExtraPremiumRateDuration,var_CSVData,$.records[{var_Count}].permanentFlatExtraPremiumRateDuration,TGTYPESCREENREG");
		String var_temporaryFlatExtraRate;
                 LOGGER.info("Executed Step = VAR,String,var_temporaryFlatExtraRate,TGTYPESCREENREG");
		var_temporaryFlatExtraRate = getJsonData(var_CSVData , "$.records["+var_Count+"].temporaryFlatExtraRate");
                 LOGGER.info("Executed Step = STORE,var_temporaryFlatExtraRate,var_CSVData,$.records[{var_Count}].temporaryFlatExtraRate,TGTYPESCREENREG");
		String var_temporaryFlatExtraPremiumRateDuration;
                 LOGGER.info("Executed Step = VAR,String,var_temporaryFlatExtraPremiumRateDuration,TGTYPESCREENREG");
		var_temporaryFlatExtraPremiumRateDuration = getJsonData(var_CSVData , "$.records["+var_Count+"].temporaryFlatExtraPremiumRateDuration");
                 LOGGER.info("Executed Step = STORE,var_temporaryFlatExtraPremiumRateDuration,var_CSVData,$.records[{var_Count}].temporaryFlatExtraPremiumRateDuration,TGTYPESCREENREG");
        CALL.$("TC01ReinsuranceVariableDeclarationSecond","TGTYPESCREENREG");

		String var_clientIdSecondaryInsured;
                 LOGGER.info("Executed Step = VAR,String,var_clientIdSecondaryInsured,TGTYPESCREENREG");
		var_clientIdSecondaryInsured = getJsonData(var_CSVData , "$.records["+var_Count+"].clientIdSecondaryInsured");
                 LOGGER.info("Executed Step = STORE,var_clientIdSecondaryInsured,var_CSVData,$.records[{var_Count}].clientIdSecondaryInsured,TGTYPESCREENREG");
		String var_lastNameSecondaryInsured;
                 LOGGER.info("Executed Step = VAR,String,var_lastNameSecondaryInsured,TGTYPESCREENREG");
		var_lastNameSecondaryInsured = getJsonData(var_CSVData , "$.records["+var_Count+"].lastNameSecondaryInsured");
                 LOGGER.info("Executed Step = STORE,var_lastNameSecondaryInsured,var_CSVData,$.records[{var_Count}].lastNameSecondaryInsured,TGTYPESCREENREG");
		String var_firstNameSecondaryInsured;
                 LOGGER.info("Executed Step = VAR,String,var_firstNameSecondaryInsured,TGTYPESCREENREG");
		var_firstNameSecondaryInsured = getJsonData(var_CSVData , "$.records["+var_Count+"].firstNameSecondaryInsured");
                 LOGGER.info("Executed Step = STORE,var_firstNameSecondaryInsured,var_CSVData,$.records[{var_Count}].firstNameSecondaryInsured,TGTYPESCREENREG");
		String var_middleInitialsSecondaryInsured;
                 LOGGER.info("Executed Step = VAR,String,var_middleInitialsSecondaryInsured,TGTYPESCREENREG");
		var_middleInitialsSecondaryInsured = getJsonData(var_CSVData , "$.records["+var_Count+"].middleInitialsSecondaryInsured");
                 LOGGER.info("Executed Step = STORE,var_middleInitialsSecondaryInsured,var_CSVData,$.records[{var_Count}].middleInitialsSecondaryInsured,TGTYPESCREENREG");
		String var_pricingSexSecondaryInsured;
                 LOGGER.info("Executed Step = VAR,String,var_pricingSexSecondaryInsured,TGTYPESCREENREG");
		var_pricingSexSecondaryInsured = getJsonData(var_CSVData , "$.records["+var_Count+"].pricingSexSecondaryInsured");
                 LOGGER.info("Executed Step = STORE,var_pricingSexSecondaryInsured,var_CSVData,$.records[{var_Count}].pricingSexSecondaryInsured,TGTYPESCREENREG");
		String var_sexSecondaryInsured;
                 LOGGER.info("Executed Step = VAR,String,var_sexSecondaryInsured,TGTYPESCREENREG");
		var_sexSecondaryInsured = getJsonData(var_CSVData , "$.records["+var_Count+"].sexSecondaryInsured");
                 LOGGER.info("Executed Step = STORE,var_sexSecondaryInsured,var_CSVData,$.records[{var_Count}].sexSecondaryInsured,TGTYPESCREENREG");
		String var_dateOfBirthSecondaryInsured;
                 LOGGER.info("Executed Step = VAR,String,var_dateOfBirthSecondaryInsured,TGTYPESCREENREG");
		var_dateOfBirthSecondaryInsured = getJsonData(var_CSVData , "$.records["+var_Count+"].dateOfBirthSecondaryInsured");
                 LOGGER.info("Executed Step = STORE,var_dateOfBirthSecondaryInsured,var_CSVData,$.records[{var_Count}].dateOfBirthSecondaryInsured,TGTYPESCREENREG");
		String var_insuredStatusSecondaryInsured;
                 LOGGER.info("Executed Step = VAR,String,var_insuredStatusSecondaryInsured,TGTYPESCREENREG");
		var_insuredStatusSecondaryInsured = getJsonData(var_CSVData , "$.records["+var_Count+"].insuredStatusSecondaryInsured");
                 LOGGER.info("Executed Step = STORE,var_insuredStatusSecondaryInsured,var_CSVData,$.records[{var_Count}].insuredStatusSecondaryInsured,TGTYPESCREENREG");
		String var_issueAgeSecondaryInsured;
                 LOGGER.info("Executed Step = VAR,String,var_issueAgeSecondaryInsured,TGTYPESCREENREG");
		var_issueAgeSecondaryInsured = getJsonData(var_CSVData , "$.records["+var_Count+"].issueAgeSecondaryInsured");
                 LOGGER.info("Executed Step = STORE,var_issueAgeSecondaryInsured,var_CSVData,$.records[{var_Count}].issueAgeSecondaryInsured,TGTYPESCREENREG");
		String var_premiumClassSecondaryInsured;
                 LOGGER.info("Executed Step = VAR,String,var_premiumClassSecondaryInsured,TGTYPESCREENREG");
		var_premiumClassSecondaryInsured = getJsonData(var_CSVData , "$.records["+var_Count+"].premiumClassSecondaryInsured");
                 LOGGER.info("Executed Step = STORE,var_premiumClassSecondaryInsured,var_CSVData,$.records[{var_Count}].premiumClassSecondaryInsured,TGTYPESCREENREG");
		String var_mortalityRating1SecondaryInsured;
                 LOGGER.info("Executed Step = VAR,String,var_mortalityRating1SecondaryInsured,TGTYPESCREENREG");
		var_mortalityRating1SecondaryInsured = getJsonData(var_CSVData , "$.records["+var_Count+"].mortalityRating1SecondaryInsured");
                 LOGGER.info("Executed Step = STORE,var_mortalityRating1SecondaryInsured,var_CSVData,$.records[{var_Count}].mortalityRating1SecondaryInsured,TGTYPESCREENREG");
		String var_mortalityDurationSecondaryInsured;
                 LOGGER.info("Executed Step = VAR,String,var_mortalityDurationSecondaryInsured,TGTYPESCREENREG");
		var_mortalityDurationSecondaryInsured = getJsonData(var_CSVData , "$.records["+var_Count+"].mortalityDurationSecondaryInsured");
                 LOGGER.info("Executed Step = STORE,var_mortalityDurationSecondaryInsured,var_CSVData,$.records[{var_Count}].mortalityDurationSecondaryInsured,TGTYPESCREENREG");
		String var_permanentFlatExtraRateSecondaryInsured;
                 LOGGER.info("Executed Step = VAR,String,var_permanentFlatExtraRateSecondaryInsured,TGTYPESCREENREG");
		var_permanentFlatExtraRateSecondaryInsured = getJsonData(var_CSVData , "$.records["+var_Count+"].permanentFlatExtraRateSecondaryInsured");
                 LOGGER.info("Executed Step = STORE,var_permanentFlatExtraRateSecondaryInsured,var_CSVData,$.records[{var_Count}].permanentFlatExtraRateSecondaryInsured,TGTYPESCREENREG");
		String var_permanentFlatExtraPremiumRateDurationSecondaryInsured;
                 LOGGER.info("Executed Step = VAR,String,var_permanentFlatExtraPremiumRateDurationSecondaryInsured,TGTYPESCREENREG");
		var_permanentFlatExtraPremiumRateDurationSecondaryInsured = getJsonData(var_CSVData , "$.records["+var_Count+"].permanentFlatExtraPremiumRateDurationSecondaryInsured");
                 LOGGER.info("Executed Step = STORE,var_permanentFlatExtraPremiumRateDurationSecondaryInsured,var_CSVData,$.records[{var_Count}].permanentFlatExtraPremiumRateDurationSecondaryInsured,TGTYPESCREENREG");
		String var_temporaryFlatExtraRateSecondaryInsured;
                 LOGGER.info("Executed Step = VAR,String,var_temporaryFlatExtraRateSecondaryInsured,TGTYPESCREENREG");
		var_temporaryFlatExtraRateSecondaryInsured = getJsonData(var_CSVData , "$.records["+var_Count+"].temporaryFlatExtraRateSecondaryInsured");
                 LOGGER.info("Executed Step = STORE,var_temporaryFlatExtraRateSecondaryInsured,var_CSVData,$.records[{var_Count}].temporaryFlatExtraRateSecondaryInsured,TGTYPESCREENREG");
		String var_temporaryFlatExtraPremiumRateDurationSecondaryInsured;
                 LOGGER.info("Executed Step = VAR,String,var_temporaryFlatExtraPremiumRateDurationSecondaryInsured,TGTYPESCREENREG");
		var_temporaryFlatExtraPremiumRateDurationSecondaryInsured = getJsonData(var_CSVData , "$.records["+var_Count+"].temporaryFlatExtraPremiumRateDurationSecondaryInsured");
                 LOGGER.info("Executed Step = STORE,var_temporaryFlatExtraPremiumRateDurationSecondaryInsured,var_CSVData,$.records[{var_Count}].temporaryFlatExtraPremiumRateDurationSecondaryInsured,TGTYPESCREENREG");
		String var_occupationClass;
                 LOGGER.info("Executed Step = VAR,String,var_occupationClass,TGTYPESCREENREG");
		var_occupationClass = getJsonData(var_CSVData , "$.records["+var_Count+"].occupationClass");
                 LOGGER.info("Executed Step = STORE,var_occupationClass,var_CSVData,$.records[{var_Count}].occupationClass,TGTYPESCREENREG");
		String var_colaPercentage;
                 LOGGER.info("Executed Step = VAR,String,var_colaPercentage,TGTYPESCREENREG");
		var_colaPercentage = getJsonData(var_CSVData , "$.records["+var_Count+"].colaPercentage");
                 LOGGER.info("Executed Step = STORE,var_colaPercentage,var_CSVData,$.records[{var_Count}].colaPercentage,TGTYPESCREENREG");
		String var_accidentBenefitMode;
                 LOGGER.info("Executed Step = VAR,String,var_accidentBenefitMode,TGTYPESCREENREG");
		var_accidentBenefitMode = getJsonData(var_CSVData , "$.records["+var_Count+"].accidentBenefitMode");
                 LOGGER.info("Executed Step = STORE,var_accidentBenefitMode,var_CSVData,$.records[{var_Count}].accidentBenefitMode,TGTYPESCREENREG");
		String var_accidentBenefitPeriod;
                 LOGGER.info("Executed Step = VAR,String,var_accidentBenefitPeriod,TGTYPESCREENREG");
		var_accidentBenefitPeriod = getJsonData(var_CSVData , "$.records["+var_Count+"].accidentBenefitPeriod");
                 LOGGER.info("Executed Step = STORE,var_accidentBenefitPeriod,var_CSVData,$.records[{var_Count}].accidentBenefitPeriod,TGTYPESCREENREG");
		String var_accidentEliminationPeriod;
                 LOGGER.info("Executed Step = VAR,String,var_accidentEliminationPeriod,TGTYPESCREENREG");
		var_accidentEliminationPeriod = getJsonData(var_CSVData , "$.records["+var_Count+"].accidentEliminationPeriod");
                 LOGGER.info("Executed Step = STORE,var_accidentEliminationPeriod,var_CSVData,$.records[{var_Count}].accidentEliminationPeriod,TGTYPESCREENREG");
		String var_sicknessBenefitMode;
                 LOGGER.info("Executed Step = VAR,String,var_sicknessBenefitMode,TGTYPESCREENREG");
		var_sicknessBenefitMode = getJsonData(var_CSVData , "$.records["+var_Count+"].sicknessBenefitMode");
                 LOGGER.info("Executed Step = STORE,var_sicknessBenefitMode,var_CSVData,$.records[{var_Count}].sicknessBenefitMode,TGTYPESCREENREG");
		String var_sicknessBenefitPeriod;
                 LOGGER.info("Executed Step = VAR,String,var_sicknessBenefitPeriod,TGTYPESCREENREG");
		var_sicknessBenefitPeriod = getJsonData(var_CSVData , "$.records["+var_Count+"].sicknessBenefitPeriod");
                 LOGGER.info("Executed Step = STORE,var_sicknessBenefitPeriod,var_CSVData,$.records[{var_Count}].sicknessBenefitPeriod,TGTYPESCREENREG");
		String var_sicknessEliminationPeriod;
                 LOGGER.info("Executed Step = VAR,String,var_sicknessEliminationPeriod,TGTYPESCREENREG");
		var_sicknessEliminationPeriod = getJsonData(var_CSVData , "$.records["+var_Count+"].sicknessEliminationPeriod");
                 LOGGER.info("Executed Step = STORE,var_sicknessEliminationPeriod,var_CSVData,$.records[{var_Count}].sicknessEliminationPeriod,TGTYPESCREENREG");
		String var_diBenefit;
                 LOGGER.info("Executed Step = VAR,String,var_diBenefit,TGTYPESCREENREG");
		var_diBenefit = getJsonData(var_CSVData , "$.records["+var_Count+"].diBenefit");
                 LOGGER.info("Executed Step = STORE,var_diBenefit,var_CSVData,$.records[{var_Count}].diBenefit,TGTYPESCREENREG");
		String var_filler2;
                 LOGGER.info("Executed Step = VAR,String,var_filler2,TGTYPESCREENREG");
		var_filler2 = getJsonData(var_CSVData , "$.records["+var_Count+"].filler2");
                 LOGGER.info("Executed Step = STORE,var_filler2,var_CSVData,$.records[{var_Count}].filler2,TGTYPESCREENREG");
		String var_ltcBeginningDate;
                 LOGGER.info("Executed Step = VAR,String,var_ltcBeginningDate,TGTYPESCREENREG");
		var_ltcBeginningDate = getJsonData(var_CSVData , "$.records["+var_Count+"].ltcBeginningDate");
                 LOGGER.info("Executed Step = STORE,var_ltcBeginningDate,var_CSVData,$.records[{var_Count}].ltcBeginningDate,TGTYPESCREENREG");
		String var_ltcBenefitAccount;
                 LOGGER.info("Executed Step = VAR,String,var_ltcBenefitAccount,TGTYPESCREENREG");
		var_ltcBenefitAccount = getJsonData(var_CSVData , "$.records["+var_Count+"].ltcBenefitAccount");
                 LOGGER.info("Executed Step = STORE,var_ltcBenefitAccount,var_CSVData,$.records[{var_Count}].ltcBenefitAccount,TGTYPESCREENREG");
		String var_ltcIncreaseOption;
                 LOGGER.info("Executed Step = VAR,String,var_ltcIncreaseOption,TGTYPESCREENREG");
		var_ltcIncreaseOption = getJsonData(var_CSVData , "$.records["+var_Count+"].ltcIncreaseOption");
                 LOGGER.info("Executed Step = STORE,var_ltcIncreaseOption,var_CSVData,$.records[{var_Count}].ltcIncreaseOption,TGTYPESCREENREG");
		String var_ltcIncreasePercentage;
                 LOGGER.info("Executed Step = VAR,String,var_ltcIncreasePercentage,TGTYPESCREENREG");
		var_ltcIncreasePercentage = getJsonData(var_CSVData , "$.records["+var_Count+"].ltcIncreasePercentage");
                 LOGGER.info("Executed Step = STORE,var_ltcIncreasePercentage,var_CSVData,$.records[{var_Count}].ltcIncreasePercentage,TGTYPESCREENREG");
		String var_ltcBenefitPeriodMode;
                 LOGGER.info("Executed Step = VAR,String,var_ltcBenefitPeriodMode,TGTYPESCREENREG");
		var_ltcBenefitPeriodMode = getJsonData(var_CSVData , "$.records["+var_Count+"].ltcBenefitPeriodMode");
                 LOGGER.info("Executed Step = STORE,var_ltcBenefitPeriodMode,var_CSVData,$.records[{var_Count}].ltcBenefitPeriodMode,TGTYPESCREENREG");
		String var_ltcBenefitPeriod;
                 LOGGER.info("Executed Step = VAR,String,var_ltcBenefitPeriod,TGTYPESCREENREG");
		var_ltcBenefitPeriod = getJsonData(var_CSVData , "$.records["+var_Count+"].ltcBenefitPeriod");
                 LOGGER.info("Executed Step = STORE,var_ltcBenefitPeriod,var_CSVData,$.records[{var_Count}].ltcBenefitPeriod,TGTYPESCREENREG");
		String var_filler3;
                 LOGGER.info("Executed Step = VAR,String,var_filler3,TGTYPESCREENREG");
		var_filler3 = getJsonData(var_CSVData , "$.records["+var_Count+"].filler3");
                 LOGGER.info("Executed Step = STORE,var_filler3,var_CSVData,$.records[{var_Count}].filler3,TGTYPESCREENREG");
		String var_suspendedCoverPercentage;
                 LOGGER.info("Executed Step = VAR,String,var_suspendedCoverPercentage,TGTYPESCREENREG");
		var_suspendedCoverPercentage = getJsonData(var_CSVData , "$.records["+var_Count+"].suspendedCoverPercentage");
                 LOGGER.info("Executed Step = STORE,var_suspendedCoverPercentage,var_CSVData,$.records[{var_Count}].suspendedCoverPercentage,TGTYPESCREENREG");
		String var_pmlCvd100Adds;
                 LOGGER.info("Executed Step = VAR,String,var_pmlCvd100Adds,TGTYPESCREENREG");
		var_pmlCvd100Adds = getJsonData(var_CSVData , "$.records["+var_Count+"].pml_CV_D100_ADDS");
                 LOGGER.info("Executed Step = STORE,var_pmlCvd100Adds,var_CSVData,$.records[{var_Count}].pml_CV_D100_ADDS,TGTYPESCREENREG");
		String var_pmlLastAcctngStored;
                 LOGGER.info("Executed Step = VAR,String,var_pmlLastAcctngStored,TGTYPESCREENREG");
		var_pmlLastAcctngStored = getJsonData(var_CSVData , "$.records["+var_Count+"].pml_LAST_ACCTNG_STORED");
                 LOGGER.info("Executed Step = STORE,var_pmlLastAcctngStored,var_CSVData,$.records[{var_Count}].pml_LAST_ACCTNG_STORED,TGTYPESCREENREG");
		String var_pmlGmdbrType;
                 LOGGER.info("Executed Step = VAR,String,var_pmlGmdbrType,TGTYPESCREENREG");
		var_pmlGmdbrType = getJsonData(var_CSVData , "$.records["+var_Count+"].pml_GMDBR_TYPE");
                 LOGGER.info("Executed Step = STORE,var_pmlGmdbrType,var_CSVData,$.records[{var_Count}].pml_GMDBR_TYPE,TGTYPESCREENREG");
		String var_pmlRdrProcess;
                 LOGGER.info("Executed Step = VAR,String,var_pmlRdrProcess,TGTYPESCREENREG");
		var_pmlRdrProcess = getJsonData(var_CSVData , "$.records["+var_Count+"].pml_RDR_PROCESS");
                 LOGGER.info("Executed Step = STORE,var_pmlRdrProcess,var_CSVData,$.records[{var_Count}].pml_RDR_PROCESS,TGTYPESCREENREG");
		String var_pmlGdbrOvedLapseSw;
                 LOGGER.info("Executed Step = VAR,String,var_pmlGdbrOvedLapseSw,TGTYPESCREENREG");
		var_pmlGdbrOvedLapseSw = getJsonData(var_CSVData , "$.records["+var_Count+"].pml_GDBR_OVRD_LAPSE_SW");
                 LOGGER.info("Executed Step = STORE,var_pmlGdbrOvedLapseSw,var_CSVData,$.records[{var_Count}].pml_GDBR_OVRD_LAPSE_SW,TGTYPESCREENREG");
		String var_pmlGdbrGraceInd;
                 LOGGER.info("Executed Step = VAR,String,var_pmlGdbrGraceInd,TGTYPESCREENREG");
		var_pmlGdbrGraceInd = getJsonData(var_CSVData , "$.records["+var_Count+"].pml_GDBR_GRACE_IND");
                 LOGGER.info("Executed Step = STORE,var_pmlGdbrGraceInd,var_CSVData,$.records[{var_Count}].pml_GDBR_GRACE_IND,TGTYPESCREENREG");
		String var_pmlGdbrLapseAcctValue;
                 LOGGER.info("Executed Step = VAR,String,var_pmlGdbrLapseAcctValue,TGTYPESCREENREG");
		var_pmlGdbrLapseAcctValue = getJsonData(var_CSVData , "$.records["+var_Count+"].pml_GDBR_LAPSE_ACCT_VALUE");
                 LOGGER.info("Executed Step = STORE,var_pmlGdbrLapseAcctValue,var_CSVData,$.records[{var_Count}].pml_GDBR_LAPSE_ACCT_VALUE,TGTYPESCREENREG");
		String var_pmlGdbarAccumLapsePrem;
                 LOGGER.info("Executed Step = VAR,String,var_pmlGdbarAccumLapsePrem,TGTYPESCREENREG");
		var_pmlGdbarAccumLapsePrem = getJsonData(var_CSVData , "$.records["+var_Count+"].pml_GDBR_ACCUM_LAPSE_PREM");
                 LOGGER.info("Executed Step = STORE,var_pmlGdbarAccumLapsePrem,var_CSVData,$.records[{var_Count}].pml_GDBR_ACCUM_LAPSE_PREM,TGTYPESCREENREG");
		String var_pmlGdbrAccumPrem;
                 LOGGER.info("Executed Step = VAR,String,var_pmlGdbrAccumPrem,TGTYPESCREENREG");
		var_pmlGdbrAccumPrem = getJsonData(var_CSVData , "$.records["+var_Count+"].pml_GDBR_ACCUM_PREM");
                 LOGGER.info("Executed Step = STORE,var_pmlGdbrAccumPrem,var_CSVData,$.records[{var_Count}].pml_GDBR_ACCUM_PREM,TGTYPESCREENREG");
		String var_pmlGdbrCommPrem;
                 LOGGER.info("Executed Step = VAR,String,var_pmlGdbrCommPrem,TGTYPESCREENREG");
		var_pmlGdbrCommPrem = getJsonData(var_CSVData , "$.records["+var_Count+"].pml_GDBR_COMM_PREM");
                 LOGGER.info("Executed Step = STORE,var_pmlGdbrCommPrem,var_CSVData,$.records[{var_Count}].pml_GDBR_COMM_PREM,TGTYPESCREENREG");
		String var_pmlGdbrMinPrem;
                 LOGGER.info("Executed Step = VAR,String,var_pmlGdbrMinPrem,TGTYPESCREENREG");
		var_pmlGdbrMinPrem = getJsonData(var_CSVData , "$.records["+var_Count+"].pml_GDBR_MIN_PREM");
                 LOGGER.info("Executed Step = STORE,var_pmlGdbrMinPrem,var_CSVData,$.records[{var_Count}].pml_GDBR_MIN_PREM,TGTYPESCREENREG");
		String var_pmlCvoverDivover;
                 LOGGER.info("Executed Step = VAR,String,var_pmlCvoverDivover,TGTYPESCREENREG");
		var_pmlCvoverDivover = getJsonData(var_CSVData , "$.records["+var_Count+"].pml_CVOVER_DIVOVER");
                 LOGGER.info("Executed Step = STORE,var_pmlCvoverDivover,var_CSVData,$.records[{var_Count}].pml_CVOVER_DIVOVER,TGTYPESCREENREG");
		String var_pmlAtoTdoPrem;
                 LOGGER.info("Executed Step = VAR,String,var_pmlAtoTdoPrem,TGTYPESCREENREG");
		var_pmlAtoTdoPrem = getJsonData(var_CSVData , "$.records["+var_Count+"].pml_ATP_TDO_PREM");
                 LOGGER.info("Executed Step = STORE,var_pmlAtoTdoPrem,var_CSVData,$.records[{var_Count}].pml_ATP_TDO_PREM,TGTYPESCREENREG");
		String var_pmlClosedPlanSwsav;
                 LOGGER.info("Executed Step = VAR,String,var_pmlClosedPlanSwsav,TGTYPESCREENREG");
		var_pmlClosedPlanSwsav = getJsonData(var_CSVData , "$.records["+var_Count+"].pml_CLOSED_PLAN_SW");
                 LOGGER.info("Executed Step = STORE,var_pmlClosedPlanSwsav,var_CSVData,$.records[{var_Count}].pml_CLOSED_PLAN_SW,TGTYPESCREENREG");
		String var_pmlImacCovForm;
                 LOGGER.info("Executed Step = VAR,String,var_pmlImacCovForm,TGTYPESCREENREG");
		var_pmlImacCovForm = getJsonData(var_CSVData , "$.records["+var_Count+"].pml_IMAC_COV_FORM");
                 LOGGER.info("Executed Step = STORE,var_pmlImacCovForm,var_CSVData,$.records[{var_Count}].pml_IMAC_COV_FORM,TGTYPESCREENREG");
		String var_pmlImacCovB4m;
                 LOGGER.info("Executed Step = VAR,String,var_pmlImacCovB4m,TGTYPESCREENREG");
		var_pmlImacCovB4m = getJsonData(var_CSVData , "$.records["+var_Count+"].pml_IMAC_COV_B4M");
                 LOGGER.info("Executed Step = STORE,var_pmlImacCovB4m,var_CSVData,$.records[{var_Count}].pml_IMAC_COV_B4M,TGTYPESCREENREG");
		String var_pmlNextRider;
                 LOGGER.info("Executed Step = VAR,String,var_pmlNextRider,TGTYPESCREENREG");
		var_pmlNextRider = getJsonData(var_CSVData , "$.records["+var_Count+"].pml_NEXT_RIDER");
                 LOGGER.info("Executed Step = STORE,var_pmlNextRider,var_CSVData,$.records[{var_Count}].pml_NEXT_RIDER,TGTYPESCREENREG");
		String var_pmlReinsAgreement;
                 LOGGER.info("Executed Step = VAR,String,var_pmlReinsAgreement,TGTYPESCREENREG");
		var_pmlReinsAgreement = getJsonData(var_CSVData , "$.records["+var_Count+"].pml_REINS_AGREEMENT");
                 LOGGER.info("Executed Step = STORE,var_pmlReinsAgreement,var_CSVData,$.records[{var_Count}].pml_REINS_AGREEMENT,TGTYPESCREENREG");
		String var_pmlCovRels;
                 LOGGER.info("Executed Step = VAR,String,var_pmlCovRels,TGTYPESCREENREG");
		var_pmlCovRels = getJsonData(var_CSVData , "$.records["+var_Count+"].pml_COV_REL");
                 LOGGER.info("Executed Step = STORE,var_pmlCovRels,var_CSVData,$.records[{var_Count}].pml_COV_REL,TGTYPESCREENREG");
		String var_pmlPolFee;
                 LOGGER.info("Executed Step = VAR,String,var_pmlPolFee,TGTYPESCREENREG");
		var_pmlPolFee = getJsonData(var_CSVData , "$.records["+var_Count+"].pml_POL_FEE");
                 LOGGER.info("Executed Step = STORE,var_pmlPolFee,var_CSVData,$.records[{var_Count}].pml_POL_FEE,TGTYPESCREENREG");
		String var_pmlUnitPrem;
                 LOGGER.info("Executed Step = VAR,String,var_pmlUnitPrem,TGTYPESCREENREG");
		var_pmlUnitPrem = getJsonData(var_CSVData , "$.records["+var_Count+"].pml_UNIT_PREM");
                 LOGGER.info("Executed Step = STORE,var_pmlUnitPrem,var_CSVData,$.records[{var_Count}].pml_UNIT_PREM,TGTYPESCREENREG");
		String var_pmlAddsCvSwsa;
                 LOGGER.info("Executed Step = VAR,String,var_pmlAddsCvSwsa,TGTYPESCREENREG");
		var_pmlAddsCvSwsa = getJsonData(var_CSVData , "$.records["+var_Count+"].pml_ADDS_CV_SW");
                 LOGGER.info("Executed Step = STORE,var_pmlAddsCvSwsa,var_CSVData,$.records[{var_Count}].pml_ADDS_CV_SW,TGTYPESCREENREG");
		String var_pmlLfpAmt;
                 LOGGER.info("Executed Step = VAR,String,var_pmlLfpAmt,TGTYPESCREENREG");
		var_pmlLfpAmt = getJsonData(var_CSVData , "$.records["+var_Count+"].pml_LFP_AMT");
                 LOGGER.info("Executed Step = STORE,var_pmlLfpAmt,var_CSVData,$.records[{var_Count}].pml_LFP_AMT,TGTYPESCREENREG");
		String var_pmlAtpTdoAmtsa;
                 LOGGER.info("Executed Step = VAR,String,var_pmlAtpTdoAmtsa,TGTYPESCREENREG");
		var_pmlAtpTdoAmtsa = getJsonData(var_CSVData , "$.records["+var_Count+"].pml_ATP_TDO_AMT");
                 LOGGER.info("Executed Step = STORE,var_pmlAtpTdoAmtsa,var_CSVData,$.records[{var_Count}].pml_ATP_TDO_AMT,TGTYPESCREENREG");
		String var_pmlPuiAmt;
                 LOGGER.info("Executed Step = VAR,String,var_pmlPuiAmt,TGTYPESCREENREG");
		var_pmlPuiAmt = getJsonData(var_CSVData , "$.records["+var_Count+"].pml_PUI_AMT");
                 LOGGER.info("Executed Step = STORE,var_pmlPuiAmt,var_CSVData,$.records[{var_Count}].pml_PUI_AMT,TGTYPESCREENREG");
		String var_pmlPuoAmt;
                 LOGGER.info("Executed Step = VAR,String,var_pmlPuoAmt,TGTYPESCREENREG");
		var_pmlPuoAmt = getJsonData(var_CSVData , "$.records["+var_Count+"].pml_PUO_AMT");
                 LOGGER.info("Executed Step = STORE,var_pmlPuoAmt,var_CSVData,$.records[{var_Count}].pml_PUO_AMT,TGTYPESCREENREG");
		String var_pmlPermAddssa;
                 LOGGER.info("Executed Step = VAR,String,var_pmlPermAddssa,TGTYPESCREENREG");
		var_pmlPermAddssa = getJsonData(var_CSVData , "$.records["+var_Count+"].pml_PERM_ADDS");
                 LOGGER.info("Executed Step = STORE,var_pmlPermAddssa,var_CSVData,$.records[{var_Count}].pml_PERM_ADDS,TGTYPESCREENREG");
		String var_pmlOldCo;
                 LOGGER.info("Executed Step = VAR,String,var_pmlOldCo,TGTYPESCREENREG");
		var_pmlOldCo = getJsonData(var_CSVData , "$.records["+var_Count+"].pml_OLD_CO");
                 LOGGER.info("Executed Step = STORE,var_pmlOldCo,var_CSVData,$.records[{var_Count}].pml_OLD_CO,TGTYPESCREENREG");
		String var_pmlValExtraCd;
                 LOGGER.info("Executed Step = VAR,String,var_pmlValExtraCd,TGTYPESCREENREG");
		var_pmlValExtraCd = getJsonData(var_CSVData , "$.records["+var_Count+"].pml_VAL_EXTRA_CD");
                 LOGGER.info("Executed Step = STORE,var_pmlValExtraCd,var_CSVData,$.records[{var_Count}].pml_VAL_EXTRA_CD,TGTYPESCREENREG");
		String var_groupId2;
                 LOGGER.info("Executed Step = VAR,String,var_groupId2,TGTYPESCREENREG");
		var_groupId2 = getJsonData(var_CSVData , "$.records["+var_Count+"].group_Id");
                 LOGGER.info("Executed Step = STORE,var_groupId2,var_CSVData,$.records[{var_Count}].group_Id,TGTYPESCREENREG");
		String var_filter4;
                 LOGGER.info("Executed Step = VAR,String,var_filter4,TGTYPESCREENREG");
		var_filter4 = getJsonData(var_CSVData , "$.records["+var_Count+"].filter_4");
                 LOGGER.info("Executed Step = STORE,var_filter4,var_CSVData,$.records[{var_Count}].filter_4,TGTYPESCREENREG");
		try { 
		 		 			var_policyResult = "PASS";
							var_failedResults = "Fields = ";
							var_apiResult = "FAIL";

							try {
								URL url = new URL("http://cis-coreapd1.btoins.ibm.com:17000/rest/pageapi/pos200s1/loadData");
								HttpURLConnection con = (HttpURLConnection) url.openConnection();
								con.setRequestMethod("POST");
								con.setRequestProperty("Content-Type", "application/json; utf-8");
								con.setRequestProperty("Accept", "application/json");
								String loginPassword = "autoapi" + ":" + "Autoapi!23";
								String encoded = new sun.misc.BASE64Encoder().encode(loginPassword.getBytes());
								con.setRequestProperty("Authorization", "Basic " + encoded);
								con.setDoOutput(true);
								JSONObject param = new JSONObject();
								param.put("policyNumber", var_policyNumber);
								JSONObject session = new JSONObject();
								session.put("currentCompany", "CO1");

								String jsonInputString = new JSONObject()
										.put("param", param)
										.put("session", session)
										.toString();

								System.out.println(jsonInputString);
								try (OutputStream os = con.getOutputStream()) {
									byte[] input = jsonInputString.getBytes("utf-8");
									os.write(input, 0, input.length);
								}
								try (BufferedReader br = new BufferedReader(
										new InputStreamReader(con.getInputStream(), "utf-8"))) {
									StringBuilder response = new StringBuilder();
									String responseLine = null;
									while ((responseLine = br.readLine()) != null) {

										response.append(responseLine.trim());
									}
									System.out.println("pos200s1 ===== " + response.toString());

									var_apiResult = "PASS";
									JSONObject response1 = new JSONObject(response.toString());

									JSONObject objData = response1.getJSONObject("data");

									String PolicyNumber1 = objData.getString("policyNumber");
									System.out.println("PolicyNumber1 ==== : " + PolicyNumber1);
									LOGGER.info("PolicyNumber1 ==== : " + PolicyNumber1);

									if (PolicyNumber1.contains(var_policyNumber)) {

										writeToCSV("PolicyNumber", var_policyNumber);
									} else {
										var_policyResult = "FAIL";
										var_failedResults = var_failedResults + "PolicyNumber = " + PolicyNumber1 + " ";
										writeToCSV("PolicyNumber", var_policyNumber);
									}

									Integer costBasisBaseRiderCodeFromDB = objData.getInt("costBasisBaseRiderCode");
									var_costBasisBaseRiderCodeFromDB = costBasisBaseRiderCodeFromDB;

									String costBasisBaseRiderCode1 = String.valueOf(costBasisBaseRiderCodeFromDB + 1);
									System.out.println("costBasisBaseRiderCode1 ==== : " + costBasisBaseRiderCode1);
									LOGGER.info("costBasisBaseRiderCode1 ==== : " + costBasisBaseRiderCode1);

									if (var_baseriderSequence.contains(costBasisBaseRiderCode1)) {

										writeToCSV("BaseRiderSequence", var_baseriderSequence);
									} else {
										var_policyResult = "FAIL";
										var_failedResults = var_failedResults + ", BaseRiderSequence = " + costBasisBaseRiderCode1 + " ";
										writeToCSV("BaseRiderSequence", var_baseriderSequence);
									}


									String statusFromDB = objData.getString("status");
									String BillingModeFromDB = objData.getString("paymentMethod");
									String status1 = " ";

									if (statusFromDB.contains("I")) {
										status1 = "PMP";

									} else if (statusFromDB.contains("C")) {
										status1 = "NTO";
									} else if (statusFromDB.contains("I") && BillingModeFromDB.contains("WP")) {
										status1 = "WOP";
									} else if (statusFromDB.contains("E")) {
										status1 = "ETI";
									} else if (statusFromDB.contains("R")) {
										status1 = "RPU";
									} else if (statusFromDB.contains("P")) {
										status1 = "PDU";
									} else if (statusFromDB.contains("N")) {
										status1 = "NTO";
									} else if (statusFromDB.contains("L")) {
										status1 = "LAP";
									} else if (statusFromDB.contains("D")) {
										status1 = "DTH";
									} else if (statusFromDB.contains("S")) {
										status1 = "SUR";
									} else if (statusFromDB.contains("X")) {
										status1 = "EXP";
									} else if (statusFromDB.contains("M")) {
										status1 = "MAT";
									} else if (statusFromDB.contains("T")) {
										status1 = "TRM";
									}

									System.out.println("status1 ==== : " + status1);
									LOGGER.info("status1 ==== : " + status1);


									if (var_policyStatus.contains(status1)) {

										writeToCSV("PolicyStatus", var_policyStatus);
									} else {
										var_policyResult = "FAIL";
										var_failedResults = var_failedResults + " , PolicyStatus = " + status1 + " ";
										writeToCSV("PolicyStatus", var_policyStatus);
									}


									String paidToDate1 = objData.getString("paidToDate").replaceAll("-", "");
									System.out.println("paidToDate1 ==== : " + paidToDate1);
									LOGGER.info("paidToDate1 ==== : " + paidToDate1);
									if (var_paidToDate.contains(paidToDate1)) {

										writeToCSV("paidToDate", var_paidToDate);
									} else {
										var_policyResult = "FAIL";
										var_failedResults = var_failedResults + " , paidToDate = " + paidToDate1 + " ";
										writeToCSV("paidToDate", var_paidToDate);
									}

									if (var_issueType.contains("")) {

										writeToCSV("Issue Type", var_issueType);
									} else {
										var_policyResult = "FAIL";
										var_failedResults = var_failedResults + " , Issue Type  = ";
										writeToCSV("Issue Type", var_issueType);
									}


									String ownerCityStateZip1 = objData.getString("ownerCityStateZip");
									System.out.println("ownerCityStateZip1 ==== : " + ownerCityStateZip1);
									LOGGER.info("ownerCityStateZip1 ==== : " + ownerCityStateZip1);
									if (ownerCityStateZip1.contains(var_ownerState)) {

										writeToCSV("OwnerState", var_ownerState);
									} else {
										var_policyResult = "FAIL";
										var_failedResults = var_failedResults + " , OwnerState = " + ownerCityStateZip1 + " ";
										writeToCSV("OwnerState", var_ownerState);
									}


									String lastDividendDate1 = objData.getString("lastDividendDate").replaceAll("-", "");
									String Year1 = getSubStringFromString(lastDividendDate1, 1, 4);
									String Month1 = getSubStringFromString(lastDividendDate1, 5, 6);
									String Date1 = getSubStringFromString(lastDividendDate1, 7, 8);
									var_PaidToDateFromDB = lastDividendDate1;

									if (lastDividendDate1.equals("00000000")) {

										var_PaidToDateFromDB = paidToDate1;
										Year1 = getSubStringFromString(paidToDate1, 1, 4);
										Month1 = getSubStringFromString(paidToDate1, 5, 6);
										Date1 = getSubStringFromString(paidToDate1, 7, 8);
									}


								}
							}catch (Exception e){
								var_policyResult = "FAIL";
								writeToCSV("PolicyNumber", var_policyNumber);
								writeToCSV("BaseRiderSequence", var_baseriderSequence);
								writeToCSV("PolicyStatus", var_policyStatus);
								writeToCSV("paidToDate", var_paidToDate);
								writeToCSV("Issue Type", var_issueType);
								writeToCSV("OwnerState", var_ownerState);
								var_failedResults = var_failedResults  + "API is not working for this policy";

								PrintVal("Renderer Script Exception : "+e.toString());
							}


						
				 
				 
		 
		 ;} 
		 catch(Exception e) { PrintVal("Renderer Script Exception : "+e.toString()); } 
		 PrintVal("Executed Step = CALLRENDERERSCRIPT,GETPOLICYDATAAPI_POS200S1"); 
        LOGGER.info("Executed Step = START IF");
        if (check(var_apiResult,"=","PASS","TGTYPESCREENREG")) {
         LOGGER.info("Executed Step = IF,var_apiResult,=,PASS,TGTYPESCREENREG");
		try { 
		 
					var_apiResult = "FAIL";
					URL url = new URL ("http://cis-coreapa1.btoins.ibm.com:17000/rest/pageapi/pos220s2/loadData");
					HttpURLConnection con = (HttpURLConnection)url.openConnection();
					con.setRequestMethod("POST");
					con.setRequestProperty("Content-Type", "application/json; utf-8");
					con.setRequestProperty("Accept", "application/json");
					String loginPassword = "autoapi"+ ":" + "Autoapi!23";
					String encoded = new sun.misc.BASE64Encoder().encode (loginPassword.getBytes());
					con.setRequestProperty ("Authorization", "Basic " + encoded);
					con.setDoOutput(true);
					JSONObject param = new JSONObject();
					param.put("baseRiderCode",0);
					param.put("policyNumber",var_policyNumber);
					param.put("mode","display");
					param.put("uniqueId",999);
					JSONObject session = new JSONObject();
					session.put("currentCompany","CO1");
					session.put("sessionDate","2021-04-16");
					String jsonInputString = new JSONObject()
							.put("param",param )
							.put("session",session)
							.toString();



					System.out.println(jsonInputString);
					try(OutputStream os = con.getOutputStream()) {
						byte[] input = jsonInputString.getBytes("utf-8");
						os.write(input, 0, input.length);
					}
					try(BufferedReader br = new BufferedReader(
							new InputStreamReader(con.getInputStream(), "utf-8"))) {
						StringBuilder response = new StringBuilder();
						String responseLine = null;
						while ((responseLine = br.readLine()) != null) {

							response.append(responseLine.trim());
						}
						System.out.println("BBBBB===== "+response.toString());
						//LOGGER.info("BBBB=====" +response.toString());

						var_apiResult = "PASS";
						JSONObject response1 = new JSONObject(response.toString());

						JSONObject objData =  response1.getJSONObject("data");

						String pricingsexCode1 = objData.getString("sexCode");
						System.out.println("sexCode1 ==== : "+ pricingsexCode1);
						LOGGER.info("sexCode1 ==== : "+ pricingsexCode1);
						if (pricingsexCode1.contains(var_pricingSex)){
							writeToCSV("Pricing Sex",var_pricingSex);
						}else{
							var_policyResult = "FAIL";
							var_failedResults = var_failedResults + " , Pricing Sex = "+pricingsexCode1+" ";
							writeToCSV("Pricing Sex",var_pricingSex);
						}



						String sexCode1 = objData.getString("sexCode");
						System.out.println("sexCode1 ==== : "+ sexCode1);
						LOGGER.info("sexCode1 ==== : "+ sexCode1);
						if (sexCode1.contains(var_sex)){
							writeToCSV("Gender",var_sex);
						}else{
							var_policyResult = "FAIL";
							var_failedResults = var_failedResults + " , Gender = "+sexCode1+" ";
							writeToCSV("Gender",var_sex);
						}



						String insuredBirthDate1 = objData.getString("insuredBirthDate").replaceAll("-","");
						System.out.println("insuredBirthDate1 ==== : "+ insuredBirthDate1);
						LOGGER.info("insuredBirthDate1 ==== : "+ insuredBirthDate1);
						if (var_dateOfBirth.contains(insuredBirthDate1)){
							writeToCSV("DateofBirth",var_dateOfBirth);
						}else{
							var_policyResult = "FAIL";
							var_failedResults = var_failedResults + " , DateofBirth = "+insuredBirthDate1+" ";
							writeToCSV("DateofBirth",var_dateOfBirth);
						}



						String benefitStatus1 = objData.getString("benefitStatus");
						System.out.println("benefitStatus1 ==== : "+ benefitStatus1);
						LOGGER.info("benefitStatus1 ==== : "+ benefitStatus1);
						if (var_insuredStatus.contains("A")){
							writeToCSV("Status",var_insuredStatus);
						}else{
							var_policyResult = "FAIL";
							var_failedResults = var_failedResults + " , Status = A ";
							writeToCSV("Status",var_insuredStatus);
						}



						String issueAge1 = String.valueOf(objData.getInt("issueAge"));
						System.out.println("issueAge1 ==== : "+ issueAge1);
						LOGGER.info("issueAge1 ==== : "+ issueAge1);
						if (issueAge1.contains(var_issueAge)){
							writeToCSV("IssueAge",var_issueAge);
						}else{
							var_policyResult = "FAIL";
							var_failedResults = var_failedResults + " , IssueAge ="+issueAge1+" ";
							writeToCSV("IssueAge",var_issueAge);
						}




						String smokerCode1 = objData.getString("smokerCode");
						System.out.println("smokerCode1 ==== : "+ smokerCode1);
						LOGGER.info("smokerCode1 ==== : "+ smokerCode1);
						if (var_premiumClass.contains(smokerCode1)){
							writeToCSV("PremiumClass",var_premiumClass);
						}else{
							var_policyResult = "FAIL";
							var_failedResults = var_failedResults + " , PremiumClass ="+smokerCode1+"";
							writeToCSV("PremiumClass",var_premiumClass);
						}


						String faceAmount1 = String.valueOf(objData.getInt("faceAmount"));
						System.out.println("faceAmount1 ==== : "+ faceAmount1);
						LOGGER.info("faceAmount1 ==== : "+ faceAmount1);

						System.out.println("faceAmount1 From Json ==== : "+ var_faceAmount);
						LOGGER.info("faceAmount1 From Json ==== : "+ var_faceAmount);
						if (var_faceAmount.contains(faceAmount1)){
							writeToCSV("FaceAmount",var_faceAmount);
						}else{
							var_policyResult = "FAIL";
							var_failedResults = var_failedResults + " , FaceAmount = "+faceAmount1+"";
							writeToCSV("FaceAmount",var_faceAmount);
						}



		//				String effectiveDate1 = objData.getString("effectiveDate").replaceAll("-","");
		//				System.out.println("effectiveDate1 ==== : "+ effectiveDate1);
		//				LOGGER.info("effectiveDate1 ==== : "+ effectiveDate1);
		//				System.out.println("effectiveDate1 From Json ==== : "+ var_statusChangeDate);
		//				LOGGER.info("effectiveDate1 From Json ==== : "+ var_statusChangeDate);
		//
		//				if (effectiveDate1.contains(var_statusChangeDate)){
		//					writeToCSV("EffectiveDate",var_statusChangeDate);
		//				}else{
		//					var_policyResult = "FAIL";
		//					var_failedResults = var_failedResults + " , EffectiveDate ="+effectiveDate1+" ";
		//					writeToCSV("EffectiveDate",var_statusChangeDate);
		//				}


						String totalAnnualPremium1 = String.valueOf((int)objData.getDouble("annualPremium"));
						System.out.println("totalAnnualPremium1 ==== : "+ totalAnnualPremium1);
						LOGGER.info("totalAnnualPremium1 ==== : "+ totalAnnualPremium1);
						System.out.println("totalAnnualPremium1 From Json ==== : "+ var_premiumForBaseCoverage);
						LOGGER.info("totalAnnualPremium1 From Json ==== : "+ var_premiumForBaseCoverage);

						if(var_premiumForBaseCoverage.contains(totalAnnualPremium1)){
							writeToCSV("AnnualPremium",var_premiumForBaseCoverage);
						}else{
							var_policyResult = "FAIL";
							var_failedResults = var_failedResults + " , AnnualPremium ="+totalAnnualPremium1+" ";
							writeToCSV("AnnualPremium",var_premiumForBaseCoverage);
						}


						String planCodeN1 = objData.getString("planCode");
						System.out.println("planCodeN1 ==== : "+ planCodeN1);
						LOGGER.info("planCodeN1 ==== : "+ planCodeN1);


					}

		//			if (var_apiResult.equals("FAIL")){
		//				var_failedResults = var_failedResults + " , Pricing Sex = ";
		//				writeToCSV("Pricing Sex","");
		//				var_failedResults = var_failedResults + " , Gender = ";
		//				writeToCSV("Gender","");
		//				var_failedResults = var_failedResults + " , DateofBirth = ";
		//				writeToCSV("DateofBirth","");
		//				var_failedResults = var_failedResults + " , Status = ";
		//				writeToCSV("Status","");
		//				var_failedResults = var_failedResults + " , IssueAge = ";
		//				writeToCSV("IssueAge","");
		//				var_failedResults = var_failedResults + " , PremiumClass = ";
		//				writeToCSV("PremiumClass","");
		//				var_failedResults = var_failedResults + " , FaceAmount = ";
		//				writeToCSV("FaceAmount","");
		////				var_failedResults = var_failedResults + " , EffectiveDate = ";
		////				writeToCSV("EffectiveDate","");
		//				var_failedResults = var_failedResults + " , AnnualPremium = ";
		//				writeToCSV("AnnualPremium","");
		//			}
		//
		//			writeToCSV("PolicyResult",var_policyResult);
		//
		//			if (var_failedResults.equals("Fields = ")){
		//				var_failedResults = "";
		//			}
		//			writeToCSV("FailedField",var_failedResults);











				
		 
		 ;} 
		 catch(Exception e) { PrintVal("Renderer Script Exception : "+e.toString()); } 
		 PrintVal("Executed Step = CALLRENDERERSCRIPT,TEMPGETPOLICYDATAAPI_POS220S2"); 
		try { 
		 		 			var_policyResult = "PASS";
							var_failedResults = "Fields = ";
							var_apiResult = "FAIL";

							try {
								URL url = new URL("http://cis-coreapd1.btoins.ibm.com:17000/rest/pageapi/pos200s1/loadData");
								HttpURLConnection con = (HttpURLConnection) url.openConnection();
								con.setRequestMethod("POST");
								con.setRequestProperty("Content-Type", "application/json; utf-8");
								con.setRequestProperty("Accept", "application/json");
								String loginPassword = "autoapi" + ":" + "Autoapi!23";
								String encoded = new sun.misc.BASE64Encoder().encode(loginPassword.getBytes());
								con.setRequestProperty("Authorization", "Basic " + encoded);
								con.setDoOutput(true);
								JSONObject param = new JSONObject();
								param.put("policyNumber", var_policyNumber);
								JSONObject session = new JSONObject();
								session.put("currentCompany", "CO1");

								String jsonInputString = new JSONObject()
										.put("param", param)
										.put("session", session)
										.toString();

								System.out.println(jsonInputString);
								try (OutputStream os = con.getOutputStream()) {
									byte[] input = jsonInputString.getBytes("utf-8");
									os.write(input, 0, input.length);
								}
								try (BufferedReader br = new BufferedReader(
										new InputStreamReader(con.getInputStream(), "utf-8"))) {
									StringBuilder response = new StringBuilder();
									String responseLine = null;
									while ((responseLine = br.readLine()) != null) {

										response.append(responseLine.trim());
									}
									System.out.println("pos200s1 ===== " + response.toString());

									var_apiResult = "PASS";
									JSONObject response1 = new JSONObject(response.toString());

									JSONObject objData = response1.getJSONObject("data");

									String PolicyNumber1 = objData.getString("policyNumber");
									System.out.println("PolicyNumber1 ==== : " + PolicyNumber1);
									LOGGER.info("PolicyNumber1 ==== : " + PolicyNumber1);

									if (PolicyNumber1.contains(var_policyNumber)) {

										writeToCSV("PolicyNumber", var_policyNumber);
									} else {
										var_policyResult = "FAIL";
										var_failedResults = var_failedResults + "PolicyNumber = " + PolicyNumber1 + " ";
										writeToCSV("PolicyNumber", var_policyNumber);
									}

									Integer costBasisBaseRiderCodeFromDB = objData.getInt("costBasisBaseRiderCode");
									var_costBasisBaseRiderCodeFromDB = costBasisBaseRiderCodeFromDB;

									String costBasisBaseRiderCode1 = String.valueOf(costBasisBaseRiderCodeFromDB + 1);
									System.out.println("costBasisBaseRiderCode1 ==== : " + costBasisBaseRiderCode1);
									LOGGER.info("costBasisBaseRiderCode1 ==== : " + costBasisBaseRiderCode1);

									if (var_baseriderSequence.contains(costBasisBaseRiderCode1)) {

										writeToCSV("BaseRiderSequence", var_baseriderSequence);
									} else {
										var_policyResult = "FAIL";
										var_failedResults = var_failedResults + ", BaseRiderSequence = " + costBasisBaseRiderCode1 + " ";
										writeToCSV("BaseRiderSequence", var_baseriderSequence);
									}


									String statusFromDB = objData.getString("status");
									String BillingModeFromDB = objData.getString("paymentMethod");
									String status1 = " ";

									if (statusFromDB.contains("I")) {
										status1 = "PMP";

									} else if (statusFromDB.contains("C")) {
										status1 = "NTO";
									} else if (statusFromDB.contains("I") && BillingModeFromDB.contains("WP")) {
										status1 = "WOP";
									} else if (statusFromDB.contains("E")) {
										status1 = "ETI";
									} else if (statusFromDB.contains("R")) {
										status1 = "RPU";
									} else if (statusFromDB.contains("P")) {
										status1 = "PDU";
									} else if (statusFromDB.contains("N")) {
										status1 = "NTO";
									} else if (statusFromDB.contains("L")) {
										status1 = "LAP";
									} else if (statusFromDB.contains("D")) {
										status1 = "DTH";
									} else if (statusFromDB.contains("S")) {
										status1 = "SUR";
									} else if (statusFromDB.contains("X")) {
										status1 = "EXP";
									} else if (statusFromDB.contains("M")) {
										status1 = "MAT";
									} else if (statusFromDB.contains("T")) {
										status1 = "TRM";
									}

									System.out.println("status1 ==== : " + status1);
									LOGGER.info("status1 ==== : " + status1);


									if (var_policyStatus.contains(status1)) {

										writeToCSV("PolicyStatus", var_policyStatus);
									} else {
										var_policyResult = "FAIL";
										var_failedResults = var_failedResults + " , PolicyStatus = " + status1 + " ";
										writeToCSV("PolicyStatus", var_policyStatus);
									}


									String paidToDate1 = objData.getString("paidToDate").replaceAll("-", "");
									System.out.println("paidToDate1 ==== : " + paidToDate1);
									LOGGER.info("paidToDate1 ==== : " + paidToDate1);
									if (var_paidToDate.contains(paidToDate1)) {

										writeToCSV("paidToDate", var_paidToDate);
									} else {
										var_policyResult = "FAIL";
										var_failedResults = var_failedResults + " , paidToDate = " + paidToDate1 + " ";
										writeToCSV("paidToDate", var_paidToDate);
									}

									if (var_issueType.contains("")) {

										writeToCSV("Issue Type", var_issueType);
									} else {
										var_policyResult = "FAIL";
										var_failedResults = var_failedResults + " , Issue Type  = ";
										writeToCSV("Issue Type", var_issueType);
									}


									String ownerCityStateZip1 = objData.getString("ownerCityStateZip");
									System.out.println("ownerCityStateZip1 ==== : " + ownerCityStateZip1);
									LOGGER.info("ownerCityStateZip1 ==== : " + ownerCityStateZip1);
									if (ownerCityStateZip1.contains(var_ownerState)) {

										writeToCSV("OwnerState", var_ownerState);
									} else {
										var_policyResult = "FAIL";
										var_failedResults = var_failedResults + " , OwnerState = " + ownerCityStateZip1 + " ";
										writeToCSV("OwnerState", var_ownerState);
									}


									String lastDividendDate1 = objData.getString("lastDividendDate").replaceAll("-", "");
									String Year1 = getSubStringFromString(lastDividendDate1, 1, 4);
									String Month1 = getSubStringFromString(lastDividendDate1, 5, 6);
									String Date1 = getSubStringFromString(lastDividendDate1, 7, 8);
									var_PaidToDateFromDB = lastDividendDate1;

									if (lastDividendDate1.equals("00000000")) {

										var_PaidToDateFromDB = paidToDate1;
										Year1 = getSubStringFromString(paidToDate1, 1, 4);
										Month1 = getSubStringFromString(paidToDate1, 5, 6);
										Date1 = getSubStringFromString(paidToDate1, 7, 8);
									}


								}
							}catch (Exception e){
								var_policyResult = "FAIL";
								writeToCSV("PolicyNumber", var_policyNumber);
								writeToCSV("BaseRiderSequence", var_baseriderSequence);
								writeToCSV("PolicyStatus", var_policyStatus);
								writeToCSV("paidToDate", var_paidToDate);
								writeToCSV("Issue Type", var_issueType);
								writeToCSV("OwnerState", var_ownerState);
								var_failedResults = var_failedResults  + "API is not working for this policy";

								PrintVal("Renderer Script Exception : "+e.toString());
							}


						
				 
				 
		 
		 ;} 
		 catch(Exception e) { PrintVal("Renderer Script Exception : "+e.toString()); } 
		 PrintVal("Executed Step = CALLRENDERERSCRIPT,GETPOLICYDATAAPI_POS200S1"); 
        }
          LOGGER.info("Executed Step = ENDIF,TGTYPESCREENREG");
		var_Count = var_Count + ( 1 );
                 LOGGER.info("Executed Step = INCREASEVAR,var_Count,1,TGTYPESCREENREG");
    	}
          LOGGER.info("Executed Step = WEND,TGTYPESCREENREG");
    	resetAttemps();

    }


}
