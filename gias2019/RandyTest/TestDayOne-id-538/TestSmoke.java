package appium.tgo;

import com.tg.octopus.base.TGOWrapper;
import com.tg.octopus.libs.testng.listener.TestListener;
import static com.tg.octopus.base.TGOWrapper.Action.*;
import static com.tg.octopus.base.TGOWrapper.LOGGER;
import com.tg.octopus.wrapper.ActionWrapper;

import com.jayway.jsonpath.JsonPath;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.SessionNotCreatedException;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.Augmenter;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.remote.UnreachableBrowserException;

import org.testng.Assert;
import org.testng.ITestContext;
import org.testng.ITestResult;
import org.testng.Reporter;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;
import java.net.HttpURLConnection;
import java.net.URL;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import org.json.JSONArray;
import org.json.JSONObject;
import helper.*;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import static helper.WebTestUtility.getSubStringFromString;
import static helper.WebTestUtility.getTextFileToArrayList;
import java.sql.*;
import com.testautomationguru.utility.PDFUtil;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.multipdf.Splitter;

import java.util.*;
import org.openqa.selenium.*;
import java.io.*;
import java.net.*;

import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;


@Listeners(TestListener.class)
public class TestSmoke extends TGOWrapper {



    static {
       needsresetoncebeforetest ="false";
       needsresetbeforeeverymethod ="true";
       captureverystepsnap ="true";
       appendlastsnap ="false";
       ishybridapp ="false";

    }


    @Test
    public void test1() {

        // No need to declare / print method name here, handled by TestNg listener
        // declare("test1");

        CALL.$("Login","TGTYPESCREENREG");

        TYPE.$("ELE_TEXTFIELD_USERNAME","//input[@id='outerForm:username']","x4725","FALSE","TGTYPESCREENREG");

        TYPE.$("ELE_TXTFELD_PASSWORD","//input[@id='outerForm:password']","T3r3saRW41","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_LOGIN","//input[@id='outerForm:loginButton']","TGTYPESCREENREG");


    }


    @Test
    public void fullsurrender() {

        // No need to declare / print method name here, handled by TestNg listener
        // declare("fullsurrender");

		// http://stage.testgrid.io/json/20210427/7AoaHI.json;
		com.jayway.jsonpath.ReadContext var_CSVData = getJsonData("http://stage.testgrid.io/json/20210427/7AoaHI.json");
                 LOGGER.info("Executed Step = VAR,Json,var_CSVData,http://stage.testgrid.io/json/20210427/7AoaHI.json,TGTYPESCREENREG");
		int var_Count = 0;
                 LOGGER.info("Executed Step = VAR,Integer,var_Count,0,TGTYPESCREENREG");
        CALL.$("Login","TGTYPESCREENREG");

        TYPE.$("ELE_TEXTFIELD_USERNAME","//input[@id='outerForm:username']","x4725","FALSE","TGTYPESCREENREG");

        TYPE.$("ELE_TXTFELD_PASSWORD","//input[@id='outerForm:password']","T3r3saRW41","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_LOGIN","//input[@id='outerForm:loginButton']","TGTYPESCREENREG");

        LOGGER.info("Executed Step = REPEAT IF");
    	while (check(var_Count,"<",1,"TGTYPESCREENREG")) {
          LOGGER.info("Executed Step = WHILE,var_Count,<,1,TGTYPESCREENREG");
        CALL.$("FullSurrender","TGTYPESCREENREG");

		String var_PolicyNumber;
                 LOGGER.info("Executed Step = VAR,String,var_PolicyNumber,TGTYPESCREENREG");
		var_PolicyNumber = getJsonData(var_CSVData , "$.records["+var_Count+"].PolicyNumber");
                 LOGGER.info("Executed Step = STORE,var_PolicyNumber,var_CSVData,$.records[{var_Count}].PolicyNumber,TGTYPESCREENREG");
        HOVER.$("ELE_MENU_POLICYOWNERSERVICE","//*[@class=\"ThemePanelMainItem\"]/td[contains(text(),'Policyowner Services')]","TGTYPESCREENREG");

        HOVER.$("ELE_SUBMENU_SURRENDER","//td[@class='ThemePanelMenuFolderText'][contains(text(),'Surrender')]","TGTYPESCREENREG");

        TAP.$("ELE_SUBMENU_FULLPARTIALSURRENDER","//td[contains(text(),'Full/Partial Surrender')]","TGTYPESCREENREG");

        TYPE.$("ELE_TEXTFIELD_POLICYNUMBER","//input[@type='text'][@name='outerForm:policyNumber']",var_PolicyNumber,"FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_FULLSURRENDER","//input[@id='outerForm:FullSurrender']","TGTYPESCREENREG");

        TAP.$("ELE_LINK_BASERIDER","//span[@id='outerForm:grid:0:baseRiderCodeOut1']","TGTYPESCREENREG");

        SELECT.$("ELE_DROPDOWN_DISTRIBUTIONREASONCODEFULL","//select[@id='outerForm:distributionReasonCode_full']","7 - Normal Distribution","TGTYPESCREENREG");

        SELECT.$("ELE_DROPDOWN_REPLACEMENT","//select[@id='outerForm:replacementQuestionA']","No","TGTYPESCREENREG");

        TAP.$("ELE_LINK_DISTRIBUTIONTEXT","//span[@id='outerForm:DistributionInformationText']","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_CONTINUE","//input[@id='outerForm:Continue']","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SUBMIT","//input[@id='outerForm:Submit']","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_CONFIRMSUBMIT","//input[@id='outerForm:ConfirmSubmit']","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_CANCEL","//input[@id='outerForm:Cancel']","TGTYPESCREENREG");

		var_Count = var_Count + ( 1 );
                 LOGGER.info("Executed Step = INCREASEVAR,var_Count,1,TGTYPESCREENREG");
    	}
          LOGGER.info("Executed Step = WEND,TGTYPESCREENREG");
    	resetAttemps();

    }


}
