package appium.tgo;

import com.tg.octopus.base.TGOWrapper;
import com.tg.octopus.libs.testng.listener.TestListener;
import static com.tg.octopus.base.TGOWrapper.Action.*;
import static com.tg.octopus.base.TGOWrapper.LOGGER;
import com.tg.octopus.wrapper.ActionWrapper;

import com.jayway.jsonpath.JsonPath;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.SessionNotCreatedException;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.Augmenter;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.remote.UnreachableBrowserException;

import org.testng.Assert;
import org.testng.ITestContext;
import org.testng.ITestResult;
import org.testng.Reporter;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;
import java.net.HttpURLConnection;
import java.net.URL;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import org.json.JSONArray;
import org.json.JSONObject;
import helper.WebTestUtility;
import java.sql.*;
import com.testautomationguru.utility.PDFUtil;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.multipdf.Splitter;

import java.util.*;
import org.openqa.selenium.*;
import java.io.*;
import java.net.*;

import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;


@Listeners(TestListener.class)
public class Test extends TGOWrapper {



    static {
       needsresetoncebeforetest ="false";
       needsresetbeforeeverymethod ="true";
       captureverystepsnap ="true";
       appendlastsnap ="false";
       ishybridapp ="true";

    }


    @Test
    public void tc04addabeneficiary() {

        // No need to declare / print method name here, handled by TestNg listener
        // declare("tc04addabeneficiary");

		int var_Count;
                 LOGGER.info("Executed Step = VAR,Integer,var_Count,TGTYPESCREENREG");
		var_Count = 0;
                 LOGGER.info("Executed Step = STORE,var_Count,0,TGTYPESCREENREG");
        LOGGER.info("Executed Step = REPEAT IF");
    	while (check(var_Count,"<",1,"TGTYPESCREENREG")) {
          LOGGER.info("Executed Step = WHILE,var_Count,<,1,TGTYPESCREENREG");
        CALL.$("LoginToGIAS36","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_USERNAME","outerForm:usernameTGWEBCOMMAIdTGWEBCOMMA0","GIAS36ILC","FALSE","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_PASSWORD","outerForm:passwordTGWEBCOMMAIdTGWEBCOMMA0","Automation","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_LOGIN","outerForm:loginButtonTGWEBCOMMAIdTGWEBCOMMA0","TGTYPESCREENREG");

        CALL.$("AddABeneficiary","TGTYPESCREENREG");

        HOVER.$("ELE_MENU_CLIENT","//td[text()='Client']","TGTYPESCREENREG");

        TAP.$("ELE_SUBMEN_POLICYRELATIONSHIPS","//td[text()='Policy Relationships']","TGTYPESCREENREG");

		com.jayway.jsonpath.ReadContext var_CSVData = getJsonData("C:/GIAS_Automation/3.6 BaseRegression/InputData/36Reg_AddBeneficiary.csv");
                 LOGGER.info("Executed Step = VAR,Json,var_CSVData,C:/GIAS_Automation/3.6 BaseRegression/InputData/36Reg_AddBeneficiary.csv,TGTYPESCREENREG");
		String var_PolicyNumber = "null";
                 LOGGER.info("Executed Step = VAR,String,var_PolicyNumber,null,TGTYPESCREENREG");
		var_PolicyNumber = getJsonData(var_CSVData , "$.records["+var_Count+"].PolicyNumber");
                 LOGGER.info("Executed Step = STORE,var_PolicyNumber,var_CSVData,$.records[{var_Count}].PolicyNumber,TGTYPESCREENREG");
        TYPE.$("ELE_TXTBOX_POLICYNUMBER","//input[@type='text'][@name='outerForm:policyNumber']",var_PolicyNumber,"FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_CONTINUE","//input[@type='submit'][@name='outerForm:Continue']","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_ADDCLIENTRELATIONSHIP","//input[@type='submit'][@name='outerForm:AddRelationship']","TGTYPESCREENREG");

        SELECT.$("ELE_DRPDWN_RELATIONSHIPTYPE","*[name='outerForm:userDefnRelaType']TGWEBCOMMACssSelectorTGWEBCOMMA0","Beneficiary (Contract)","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_SHAREPERCENTAGE","//input[@type='text'][@name='outerForm:sharePercentage']","100.00","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_LINK_SELECTCLIENT","//span[text()='Select Client']","TGTYPESCREENREG");

        TAP.$("ELE_RECBTN_ADD","//input[@type='submit'][@name='outerForm:Add']","TGTYPESCREENREG");

		String var_BeneficiaryFirstName = "null";
                 LOGGER.info("Executed Step = VAR,String,var_BeneficiaryFirstName,null,TGTYPESCREENREG");
		var_BeneficiaryFirstName = getJsonData(var_CSVData , "$.records["+var_Count+"].BeneficiaryFirstName");
                 LOGGER.info("Executed Step = STORE,var_BeneficiaryFirstName,var_CSVData,$.records[{var_Count}].BeneficiaryFirstName,TGTYPESCREENREG");
		String var_BeneficiaryLastName;
                 LOGGER.info("Executed Step = VAR,String,var_BeneficiaryLastName,TGTYPESCREENREG");
		var_BeneficiaryLastName = getJsonData(var_CSVData , "$.records["+var_Count+"].BeneficiaryLastName");
                 LOGGER.info("Executed Step = STORE,var_BeneficiaryLastName,var_CSVData,$.records[{var_Count}].BeneficiaryLastName,TGTYPESCREENREG");
		String var_BeneficiaryBirthState;
                 LOGGER.info("Executed Step = VAR,String,var_BeneficiaryBirthState,TGTYPESCREENREG");
		var_BeneficiaryBirthState = getJsonData(var_CSVData , "$.records["+var_Count+"].BeneficiaryBirthState");
                 LOGGER.info("Executed Step = STORE,var_BeneficiaryBirthState,var_CSVData,$.records[{var_Count}].BeneficiaryBirthState,TGTYPESCREENREG");
		String var_BeneficiaryDOB;
                 LOGGER.info("Executed Step = VAR,String,var_BeneficiaryDOB,TGTYPESCREENREG");
		var_BeneficiaryDOB = getJsonData(var_CSVData , "$.records["+var_Count+"].BeneficiaryDOB");
                 LOGGER.info("Executed Step = STORE,var_BeneficiaryDOB,var_CSVData,$.records[{var_Count}].BeneficiaryDOB,TGTYPESCREENREG");
		String var_BeneficiaryGender;
                 LOGGER.info("Executed Step = VAR,String,var_BeneficiaryGender,TGTYPESCREENREG");
		var_BeneficiaryGender = getJsonData(var_CSVData , "$.records["+var_Count+"].BeneficiaryGender");
                 LOGGER.info("Executed Step = STORE,var_BeneficiaryGender,var_CSVData,$.records[{var_Count}].BeneficiaryGender,TGTYPESCREENREG");
		String var_BeneficiaryIDType;
                 LOGGER.info("Executed Step = VAR,String,var_BeneficiaryIDType,TGTYPESCREENREG");
		var_BeneficiaryIDType = getJsonData(var_CSVData , "$.records["+var_Count+"].BeneficiaryIDType");
                 LOGGER.info("Executed Step = STORE,var_BeneficiaryIDType,var_CSVData,$.records[{var_Count}].BeneficiaryIDType,TGTYPESCREENREG");
		String var_BeneficiaryIDNumber;
                 LOGGER.info("Executed Step = VAR,String,var_BeneficiaryIDNumber,TGTYPESCREENREG");
		var_BeneficiaryIDNumber = getJsonData(var_CSVData , "$.records["+var_Count+"].BeneficiaryIDNumber");
                 LOGGER.info("Executed Step = STORE,var_BeneficiaryIDNumber,var_CSVData,$.records[{var_Count}].BeneficiaryIDNumber,TGTYPESCREENREG");
        TYPE.$("ELE_TXTBOX_BENEFICIARYFIRSTNAME","input[type=text][name='outerForm:nameFirst']TGWEBCOMMACssSelectorTGWEBCOMMA0",var_BeneficiaryFirstName,"FALSE","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_BENEFICIARYLASTNAME","input[type=text][name='outerForm:nameLast']TGWEBCOMMACssSelectorTGWEBCOMMA0",var_BeneficiaryLastName,"FALSE","TGTYPESCREENREG");

        SELECT.$("ELE_DRPDWN_BENEFICIARYSTATE","select[name='outerForm:birthCountryAndState2']TGWEBCOMMACssSelectorTGWEBCOMMA0",var_BeneficiaryBirthState,"TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_BENEFICIARYDOB","input[type=text][name='outerForm:dateOfBirth2']TGWEBCOMMACssSelectorTGWEBCOMMA0",var_BeneficiaryDOB,"FALSE","TGTYPESCREENREG");

        SELECT.$("ELE_DRPBWN_BENEFICIARYGENDER","select[name='outerForm:sexCode2']TGWEBCOMMACssSelectorTGWEBCOMMA0",var_BeneficiaryGender,"TGTYPESCREENREG");

        SELECT.$("ELE_DRPDWN_BENEFICIARYID","select[name='outerForm:taxIdenUsag2']TGWEBCOMMACssSelectorTGWEBCOMMA0",var_BeneficiaryIDType,"TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_BENEFICIARYIDNUMBER","input[type=text][name='outerForm:identificationNumber2']TGWEBCOMMACssSelectorTGWEBCOMMA0",var_BeneficiaryIDNumber,"FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SUBMIT","//input[@type='submit'][@name='outerForm:Submit']","TGTYPESCREENREG");

        ASSERT.$("ELE_GETMSG_SUCCESS","//li[@class='MessageInfo']","CONTAINS","successfully","TGTYPESCREENREG");

		String var_BeneficiaryStreetAddress = "null";
                 LOGGER.info("Executed Step = VAR,String,var_BeneficiaryStreetAddress,null,TGTYPESCREENREG");
		var_BeneficiaryStreetAddress = getJsonData(var_CSVData , "$.records["+var_Count+"].BeneficiaryStreetAddress");
                 LOGGER.info("Executed Step = STORE,var_BeneficiaryStreetAddress,var_CSVData,$.records[{var_Count}].BeneficiaryStreetAddress,TGTYPESCREENREG");
        TYPE.$("ELE_TXTBOX_BENEFICIARYSTREETADDRESS","input[type=text][name='outerForm:addressLineOne']TGWEBCOMMACssSelectorTGWEBCOMMA0",var_BeneficiaryStreetAddress,"FALSE","TGTYPESCREENREG");

		String var_BeneficiaryCity;
                 LOGGER.info("Executed Step = VAR,String,var_BeneficiaryCity,TGTYPESCREENREG");
		var_BeneficiaryCity = getJsonData(var_CSVData , "$.records["+var_Count+"].BeneficiaryCity");
                 LOGGER.info("Executed Step = STORE,var_BeneficiaryCity,var_CSVData,$.records[{var_Count}].BeneficiaryCity,TGTYPESCREENREG");
        TYPE.$("ELE_TXTBOX_BENEFICIARYCITY","input[type=text][name='outerForm:addressCity']TGWEBCOMMACssSelectorTGWEBCOMMA0",var_BeneficiaryCity,"FALSE","TGTYPESCREENREG");

        SELECT.$("ELE_DRPDWN_BENEFICIARYSTATEANDCOUNTRY","select[name='outerForm:countryAndStateCode']TGWEBCOMMACssSelectorTGWEBCOMMA0",var_BeneficiaryBirthState,"TGTYPESCREENREG");

		String var_BeneficairyZip;
                 LOGGER.info("Executed Step = VAR,String,var_BeneficairyZip,TGTYPESCREENREG");
		var_BeneficairyZip = getJsonData(var_CSVData , "$.records["+var_Count+"].BeneficairyZip");
                 LOGGER.info("Executed Step = STORE,var_BeneficairyZip,var_CSVData,$.records[{var_Count}].BeneficairyZip,TGTYPESCREENREG");
        TYPE.$("ELE_TXTBOX_BENEFICIARYZIP","input[type=text][name='outerForm:zipCode']TGWEBCOMMACssSelectorTGWEBCOMMA0",var_BeneficairyZip,"FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SUBMIT","//input[@type='submit'][@name='outerForm:Submit']","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_CANCEL","//input[@type='submit'][@name='outerForm:Cancel']","TGTYPESCREENREG");

        SELECT.$("ELE_DRPDWN_BENEFICIARYRELATIONSHIP","select[name='outerForm:grid:0:relationship']TGWEBCOMMACssSelectorTGWEBCOMMA0","Beneficiary (Contract)","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SUBMIT","//input[@type='submit'][@name='outerForm:Submit']","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SUBMIT","//input[@type='submit'][@name='outerForm:Submit']","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_CANCEL","//input[@type='submit'][@name='outerForm:Cancel']","TGTYPESCREENREG");

        TAP.$("ELE_LINK_BENEFICIARYCONTRACT","//span[text()='Beneficiary (Contract)']","TGTYPESCREENREG");

		writeToCSV("Beneficiary Name " , ActionWrapper.getWebElementValueForVariable("span[id='outerForm:clientName']", "CssSelector", 0, "span[id='outerForm:clientName']TGWEBCOMMACssSelectorTGWEBCOMMA0"));
                 LOGGER.info("Executed Step = WRITETOCSV,ELE_GETVAL_CLIENTBENEFICIARYNAME,Beneficiary Name,TGTYPESCREENREG");
		writeToCSV("Beneficiary RelationType " , ActionWrapper.getWebElementValueForVariable("span[id='outerForm:userDefnRelaType']", "CssSelector", 0, "span[id='outerForm:userDefnRelaType']TGWEBCOMMACssSelectorTGWEBCOMMA0"));
                 LOGGER.info("Executed Step = WRITETOCSV,ELE_GETVAL_BENEFIICIARYRELATIONTYPE,Beneficiary RelationType,TGTYPESCREENREG");
		writeToCSV("Beneficiary Percentage " , ActionWrapper.getWebElementValueForVariable("span[id='outerForm:sharePercentageText']", "CssSelector", 0, "span[id='outerForm:sharePercentageText']TGWEBCOMMACssSelectorTGWEBCOMMA0"));
                 LOGGER.info("Executed Step = WRITETOCSV,ELE_GETVAL_BENEFICIARYPERCENTAGE,Beneficiary Percentage,TGTYPESCREENREG");
		var_Count = var_Count + ( 1 );
                 LOGGER.info("Executed Step = INCREASEVAR,var_Count,1,TGTYPESCREENREG");
    	}
          LOGGER.info("Executed Step = WEND,TGTYPESCREENREG");
    	resetAttemps();

    }


}
