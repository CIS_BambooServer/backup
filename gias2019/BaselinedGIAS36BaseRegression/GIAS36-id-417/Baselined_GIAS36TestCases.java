package appium.tgo;

import com.tg.octopus.base.TGOWrapper;
import com.tg.octopus.libs.testng.listener.TestListener;
import static com.tg.octopus.base.TGOWrapper.Action.*;
import static com.tg.octopus.base.TGOWrapper.LOGGER;
import com.tg.octopus.wrapper.ActionWrapper;

import com.jayway.jsonpath.JsonPath;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.SessionNotCreatedException;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.Augmenter;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.remote.UnreachableBrowserException;

import org.testng.Assert;
import org.testng.ITestContext;
import org.testng.ITestResult;
import org.testng.Reporter;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;
import java.net.HttpURLConnection;
import java.net.URL;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import org.json.JSONArray;
import org.json.JSONObject;
import helper.WebTestUtility;
import java.sql.*;
import com.testautomationguru.utility.PDFUtil;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.multipdf.Splitter;

import java.util.*;
import org.openqa.selenium.*;
import java.io.*;
import java.net.*;

import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;


@Listeners(TestListener.class)
public class Baselined_GIAS36TestCases extends TGOWrapper {



    static {
       needsresetoncebeforetest ="false";
       needsresetbeforeeverymethod ="true";
       captureverystepsnap ="true";
       appendlastsnap ="false";
       ishybridapp ="true";

    }


    @Test
    public void tc03fullsurrenderpolicy() {

        // No need to declare / print method name here, handled by TestNg listener
        // declare("tc03fullsurrenderpolicy");

        CALL.$("LoginToGIAS36","TGTYPESCREENREG");

        TYPE.$("ELE_USERNAME","//input[@name='outerForm:username']","GIAS36ILC","FALSE","TGTYPESCREENREG");

        TYPE.$("ELE_PASSWORD","//input[@id='outerForm:password']","Automation","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_LOGINBUTTON","outerForm:loginButtonTGWEBCOMMAIdTGWEBCOMMA0","TGTYPESCREENREG");

		int var_Count;
                 LOGGER.info("Executed Step = VAR,Integer,var_Count,TGTYPESCREENREG");
		var_Count = 0;
                 LOGGER.info("Executed Step = STORE,var_Count,0,TGTYPESCREENREG");
        LOGGER.info("Executed Step = REPEAT IF");
    	while (check(var_Count,"<",1,"TGTYPESCREENREG")) {
          LOGGER.info("Executed Step = WHILE,var_Count,<,1,TGTYPESCREENREG");
        CALL.$("NavigateToSurrenderPolicy","TGTYPESCREENREG");

        HOVER.$("ELE_SURRENDER_POLICYOWNERSERVICESMENU","//td[@class='ThemePanelMainFolderText'][contains(text(),'Policyowner Services')]","TGTYPESCREENREG");

        HOVER.$("ELE_SURRENDERSUBMENU","//td[@class='ThemePanelMenuFolderText'][contains(text(),'Surrender')]","TGTYPESCREENREG");

        TAP.$("ELE_FULLPARTIALSURRENDERREQUEST","//td[contains(text(),'Full/Partial Surrender')]","TGTYPESCREENREG");

		com.jayway.jsonpath.ReadContext var_CSVData = getJsonData("C:/GIAS Automation/3.6 BaseRegression/InputData/36Reg_FullSurrender.csv");
                 LOGGER.info("Executed Step = VAR,Json,var_CSVData,C:/GIAS Automation/3.6 BaseRegression/InputData/36Reg_FullSurrender.csv,TGTYPESCREENREG");
		String var_policyNumber;
                 LOGGER.info("Executed Step = VAR,String,var_policyNumber,TGTYPESCREENREG");
		var_policyNumber = getJsonData(var_CSVData , "$.records.["+var_Count+"].SurrenderPolicyNumber");
                 LOGGER.info("Executed Step = STORE,var_policyNumber,var_CSVData,$.records.[{var_Count}].SurrenderPolicyNumber,TGTYPESCREENREG");
        TYPE.$("ELE_SURRENDER_POLICYNUMBER","//input[@id='outerForm:policyNumber']",var_policyNumber,"FALSE","TGTYPESCREENREG");

        TYPE.$("ELE_SURRENDER_SURRENDERDATE","//input[@id='outerForm:promptDate_input']","03/20/2019","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_SURRENDER_FULLSURRENDERBUTTON","//input[@id='outerForm:FullSurrender']",1,0,"TGTYPESCREENREG");

        TAP.$("ELE_SURRENDER_BASELINK","//span[contains(text(),'Base')]",1,0,"TGTYPESCREENREG");

        TAP.$("ELE_SURRENDER_DSTRIBUTIONINFORMATIONLINK","//span[@id='outerForm:DistributionInformationText']",1,0,"TGTYPESCREENREG");

        SCROLL.$("ELE_SURRENDER_CONTINUEBUTTON","//input[@id='outerForm:Continue']","DOWN","TGTYPESCREENREG");

        TAP.$("ELE_SURRENDER_CONTINUEBUTTON","//input[@id='outerForm:Continue']",1,0,"TGTYPESCREENREG");

        SELECT.$("ELE_SURRENDER_DISTRIBUTIONREASONCODE_FULL1","//select[@name='outerForm:distributionReasonCode_full']","7 - Normal Distribution","TGTYPESCREENREG");

        SCROLL.$("ELE_SUBMITBUTTON","//input[@id='outerForm:Submit']","DOWN","TGTYPESCREENREG");

        TAP.$("ELE_SUBMITBUTTON","//input[@id='outerForm:Submit']",1,0,"TGTYPESCREENREG");

        CALL.$("InquireSurrenderPolicy","TGTYPESCREENREG");

        HOVER.$("ELE_INQUIRY","//td[@class='ThemePanelMainFolderText'][contains(text(),'Inquiry')]","TGTYPESCREENREG");

        TAP.$("ELE_POLICYDETAILSUBMENU","//td[contains(text(),'Policy Detail')]","TGTYPESCREENREG");

        TYPE.$("ELE_SURRENDER_POLICYNUMBER","//input[@id='outerForm:policyNumber']",var_policyNumber,"FALSE","TGTYPESCREENREG");

        TAP.$("ELE_SURRENDER_CONTINUEBUTTON","//input[@id='outerForm:Continue']","TGTYPESCREENREG");

		writeToCSV ("ELE_GETPOLICYNUMBER " , ActionWrapper.getElementValue("ELE_GETPOLICYNUMBER", "//span[@id='outerForm:policyNumber']"));
                 LOGGER.info("Executed Step = WRITETOCSV,ELE_GETPOLICYNUMBER,TGTYPESCREENREG");
		writeToCSV ("ELE_STATUS " , ActionWrapper.getElementValue("ELE_STATUS", "//span[@id='outerForm:statusText']"));
                 LOGGER.info("Executed Step = WRITETOCSV,ELE_STATUS,TGTYPESCREENREG");
		writeToCSV ("ELE_GETEFFECTIVEDATETEXT " , ActionWrapper.getElementValue("ELE_GETEFFECTIVEDATETEXT", "//span[@id='outerForm:effectiveDate_input']"));
                 LOGGER.info("Executed Step = WRITETOCSV,ELE_GETEFFECTIVEDATETEXT,TGTYPESCREENREG");
		writeToCSV ("ELE_ANNUALPREMIUM " , ActionWrapper.getElementValue("ELE_ANNUALPREMIUM", "//span[@id='outerForm:annualPremium']"));
                 LOGGER.info("Executed Step = WRITETOCSV,ELE_ANNUALPREMIUM,TGTYPESCREENREG");
        TAP.$("ELE_POLICYVALUESLINK","//span[contains(text(),'Policy Values')]","TGTYPESCREENREG");

		writeToCSV ("ELE_NETPOLICYVALUE " , ActionWrapper.getElementValue("ELE_NETPOLICYVALUE", "//span[@id='outerForm:netCashValue']"));
                 LOGGER.info("Executed Step = WRITETOCSV,ELE_NETPOLICYVALUE,TGTYPESCREENREG");
		var_Count = var_Count + ( 1 );
                 LOGGER.info("Executed Step = INCREASEVAR,var_Count,1,TGTYPESCREENREG");
    	}
          LOGGER.info("Executed Step = WEND,TGTYPESCREENREG");
    	resetAttemps();

    }


    @Test
    public void tc02suspenseaccount() {

        // No need to declare / print method name here, handled by TestNg listener
        // declare("tc02suspenseaccount");

        CALL.$("LoginToGIAS36","TGTYPESCREENREG");

        TYPE.$("ELE_USERNAME","//input[@name='outerForm:username']","GIAS36ILC","FALSE","TGTYPESCREENREG");

        TYPE.$("ELE_PASSWORD","//input[@id='outerForm:password']","Automation","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_LOGINBUTTON","outerForm:loginButtonTGWEBCOMMAIdTGWEBCOMMA0","TGTYPESCREENREG");

		int var_Count;
                 LOGGER.info("Executed Step = VAR,Integer,var_Count,TGTYPESCREENREG");
		var_Count = 0;
                 LOGGER.info("Executed Step = STORE,var_Count,0,TGTYPESCREENREG");
		com.jayway.jsonpath.ReadContext var_CSVData = getJsonData("C:/GIAS_Automation/3.6 BaseRegression/InputData/36Reg_SuspenseAccount.csv");
                 LOGGER.info("Executed Step = VAR,Json,var_CSVData,C:/GIAS_Automation/3.6 BaseRegression/InputData/36Reg_SuspenseAccount.csv,TGTYPESCREENREG");
        LOGGER.info("Executed Step = REPEAT IF");
    	while (check(var_Count,"<",1,"TGTYPESCREENREG")) {
          LOGGER.info("Executed Step = WHILE,var_Count,<,1,TGTYPESCREENREG");
		String var_PolicyNumber;
                 LOGGER.info("Executed Step = VAR,String,var_PolicyNumber,TGTYPESCREENREG");
		var_PolicyNumber = getJsonData(var_CSVData , "$.records["+var_Count+"].SuspensePolicyNumber");
                 LOGGER.info("Executed Step = STORE,var_PolicyNumber,var_CSVData,$.records[{var_Count}].SuspensePolicyNumber,TGTYPESCREENREG");
        CALL.$("CheckMinimumIsLessOrEqualToSuspenseBalance","TGTYPESCREENREG");

		int var_SuspenseBalance;
                 LOGGER.info("Executed Step = VAR,Integer,var_SuspenseBalance,TGTYPESCREENREG");
		int var_getMinimumBalance;
                 LOGGER.info("Executed Step = VAR,Integer,var_getMinimumBalance,TGTYPESCREENREG");
		String var_MinimumBalance;
                 LOGGER.info("Executed Step = VAR,String,var_MinimumBalance,TGTYPESCREENREG");
        HOVER.$("ELE_INQUIRYMENU","//td[@class='ThemePanelMainFolderText'][contains(text(),'Inquiry')]","TGTYPESCREENREG");

        TAP.$("ELE_POLICYDETAILINQUIRYSUBMENU","//td[contains(text(),'Policy Detail')]","TGTYPESCREENREG");

        TYPE.$("ELE_SUSPENSEQUICKINQUIRYPOLICYNUMBER","//input[@id='outerForm:policyNumber']",var_PolicyNumber,"FALSE","TGTYPESCREENREG");

        TAP.$("ELE_CONTINUEBUTTON","//input[@id='outerForm:Continue']","TGTYPESCREENREG");

		var_getMinimumBalance = ActionWrapper.getElementValueForVariableWithInt("ELE_GETMINIMUMBALANCEFROMINQUIRY", "//span[@id='outerForm:minimumRequiredPrem']");
                 LOGGER.info("Executed Step = STORE,var_getMinimumBalance,ELE_GETMINIMUMBALANCEFROMINQUIRY,TGTYPESCREENREG");
		var_SuspenseBalance = ActionWrapper.getElementValueForVariableWithInt("ELE_GETSUSPENSEBALANCEFROMINQUIRY", "//span[@id='outerForm:suspenseBalance']");
                 LOGGER.info("Executed Step = STORE,var_SuspenseBalance,ELE_GETSUSPENSEBALANCEFROMINQUIRY,TGTYPESCREENREG");
		var_MinimumBalance = ActionWrapper.getElementValue("ELE_GETMINIMUMBALANCEFROMINQUIRY", "//span[@id='outerForm:minimumRequiredPrem']");
                 LOGGER.info("Executed Step = STORE,var_MinimumBalance,ELE_GETMINIMUMBALANCEFROMINQUIRY,TGTYPESCREENREG");
        LOGGER.info("Executed Step = START IF");
        if (check(var_getMinimumBalance,"<=",var_SuspenseBalance,"TGTYPESCREENREG")) {
         LOGGER.info("Executed Step = IF,var_getMinimumBalance,<=,var_SuspenseBalance,TGTYPESCREENREG");
        CALL.$("SearchSuspenseAccount","TGTYPESCREENREG");

        HOVER.$("ELE_PAYMENTSMENU","//td[@class='ThemePanelMainFolderText'][contains(text(),'Payments')]","TGTYPESCREENREG");

        TAP.$("ELE_SUSPENSEPAYMENTSUBMENU","//div[@id='cmSubMenuID24']//td[@class='ThemePanelMenuItemText'][contains(text(),'Suspense')]","TGTYPESCREENREG");

        TAP.$("ELE_SUSPENSE_SEARECHROUNDBUTTON","//img[@id='outerForm:gnSearchImage']","TGTYPESCREENREG");

        SELECT.$("ELE_SEARCHCONTROLNUMBERDRPDOWN","//select[@name='outerForm:grid:3:FullOperator']","Equals","TGTYPESCREENREG");

        TYPE.$("ELE_SEARCHCONTROLNUMBERVALUE","//input[@id='outerForm:grid:3:fieldUpper']",var_PolicyNumber,"FALSE","TGTYPESCREENREG");

        TAP.$("ELE_SUSPENSE_SEARCBUTTON","//input[@id='outerForm:Search']","TGTYPESCREENREG");

        WAIT.$("ELE_SUSPENSE_SEARECHROUNDBUTTON","//img[@id='outerForm:gnSearchImage']","VISIBLE","TGTYPESCREENREG");

        LOGGER.info("Executed Step = START IF");
        if (check("ELE_GETPREMIUMSUSPENSEFROMSEARCHRESULT","//span[contains(text(),'Premium suspense')]","CONTAINS","Premium suspense","TGTYPESCREENREG")) {
         LOGGER.info("Executed Step = IF,ELE_GETPREMIUMSUSPENSEFROMSEARCHRESULT,CONTAINS,Premium suspense,TGTYPESCREENREG");
        CALL.$("MakePayment","TGTYPESCREENREG");

        HOVER.$("ELE_PAYMENTSMENU","//td[@class='ThemePanelMainFolderText'][contains(text(),'Payments')]","TGTYPESCREENREG");

        TAP.$("ELE_POLICYPAYMENTSSUBMENU","//div[@id='cmSubMenuID24']//td[@class='ThemePanelMenuItemText'][contains(text(),'Policy')]","TGTYPESCREENREG");

        TYPE.$("ELE_SUSPENSEQUICKINQUIRYPOLICYNUMBER","//input[@id='outerForm:policyNumber']",var_PolicyNumber,"FALSE","TGTYPESCREENREG");

        TAP.$("ELE_CONTINUEBUTTON","//input[@id='outerForm:Continue']","TGTYPESCREENREG");

        TAP.$("ELE_SUSPENSE_SUBMITRECTBUTTON","//input[@id='outerForm:Submit']","TGTYPESCREENREG");

        }
          LOGGER.info("Executed Step = ENDIF,TGTYPESCREENREG");
        }
         else {
          LOGGER.info("Executed Step = ELSE,TGTYPESCREENREG");
        CALL.$("fillAddSuspenseDetailsForm","TGTYPESCREENREG");

        HOVER.$("ELE_PAYMENTSMENU","//td[@class='ThemePanelMainFolderText'][contains(text(),'Payments')]","TGTYPESCREENREG");

        TAP.$("ELE_SUSPENSEPAYMENTSUBMENU","//div[@id='cmSubMenuID24']//td[@class='ThemePanelMenuItemText'][contains(text(),'Suspense')]","TGTYPESCREENREG");

        TAP.$("ELE_SUSPENSE_ADDROUNDBUTTON","//img[@id='outerForm:gnAddImage']","TGTYPESCREENREG");

        SELECT.$("ELE_ADDSUSPENSECURRENCYCODEDRPDOWN","//select[@id='outerForm:currencyCode']","Dollars [US]","TGTYPESCREENREG");

        TYPE.$("ELE_ADDSUSPENSECONTROLNUMBER","//input[@id='outerForm:suspenseControlNumb']",var_PolicyNumber,"FALSE","TGTYPESCREENREG");

        SELECT.$("ELE_ADDSUSPENSETYPEDRPDOWN","//select[@id='outerForm:suspenseType']","Premium suspense","TGTYPESCREENREG");

        TYPE.$("ELE_ADDSUSPENSEAMOUNTOFCHANGE","//input[@id='outerForm:amountOfChange']",var_MinimumBalance,"TRUE","TGTYPESCREENREG");

        TYPE.$("ELE_ADDSUSPENSECOMMENT","//input[@id='outerForm:suspenseComment']","Premium suspense","FALSE","TGTYPESCREENREG");

        SELECT.$("ELE_ADDSUSPENSEACCOUNTNUMBERDRPDWN","//select[@id='outerForm:suspenseAccountNumb']","153000 - CASH","TGTYPESCREENREG");

        TAP.$("ELE_SUSPENSE_SUBMITRECTBUTTON","//input[@id='outerForm:Submit']","TGTYPESCREENREG");

        LOGGER.info("Executed Step = START IF");
        if (check("ELE_CONFIRMSUBMITBUTTON","//input[@id='outerForm:ConfirmSubmit']","VISIBLE","TGTYPESCREENREG")) {
         LOGGER.info("Executed Step = IF,ELE_CONFIRMSUBMITBUTTON,VISIBLE,TGTYPESCREENREG");
        TAP.$("ELE_CONFIRMSUBMITBUTTON","//input[@id='outerForm:ConfirmSubmit']","TGTYPESCREENREG");

        }
          LOGGER.info("Executed Step = ENDIF,TGTYPESCREENREG");
        CALL.$("MakePayment","TGTYPESCREENREG");

        HOVER.$("ELE_PAYMENTSMENU","//td[@class='ThemePanelMainFolderText'][contains(text(),'Payments')]","TGTYPESCREENREG");

        TAP.$("ELE_POLICYPAYMENTSSUBMENU","//div[@id='cmSubMenuID24']//td[@class='ThemePanelMenuItemText'][contains(text(),'Policy')]","TGTYPESCREENREG");

        TYPE.$("ELE_SUSPENSEQUICKINQUIRYPOLICYNUMBER","//input[@id='outerForm:policyNumber']",var_PolicyNumber,"FALSE","TGTYPESCREENREG");

        TAP.$("ELE_CONTINUEBUTTON","//input[@id='outerForm:Continue']","TGTYPESCREENREG");

        TAP.$("ELE_SUSPENSE_SUBMITRECTBUTTON","//input[@id='outerForm:Submit']","TGTYPESCREENREG");

        }
          LOGGER.info("Executed Step = ENDIF,TGTYPESCREENREG");
        CALL.$("ToCheckSuspenseAccountBalance","TGTYPESCREENREG");

        HOVER.$("ELE_INQUIRYMENU","//td[@class='ThemePanelMainFolderText'][contains(text(),'Inquiry')]","TGTYPESCREENREG");

        TAP.$("ELE_QUICKINQUIRYSUBMENU","//td[contains(text(),'Quick Inquiry')]","TGTYPESCREENREG");

        TYPE.$("ELE_SUSPENSEQUICKINQUIRYPOLICYNUMBER","//input[@id='outerForm:policyNumber']",var_PolicyNumber,"FALSE","TGTYPESCREENREG");

        TAP.$("ELE_CONTINUEBUTTON","//input[@id='outerForm:Continue']","TGTYPESCREENREG");

		writeToCSV ("ELE_GETPOLICYNUMBER " , ActionWrapper.getElementValue("ELE_GETPOLICYNUMBER", "//span[@id='outerForm:policyNumber']"));
                 LOGGER.info("Executed Step = WRITETOCSV,ELE_GETPOLICYNUMBER,TGTYPESCREENREG");
		writeToCSV ("ELE_GETEFFECTIVEDATETEXTFROMGRID " , ActionWrapper.getElementValue("ELE_GETEFFECTIVEDATETEXTFROMGRID", "//span[@id='outerForm:grid:0:effectiveDateOut2']"));
                 LOGGER.info("Executed Step = WRITETOCSV,ELE_GETEFFECTIVEDATETEXTFROMGRID,TGTYPESCREENREG");
		writeToCSV ("ELE_GETSTATUSFROMINQUIRY " , ActionWrapper.getElementValue("ELE_GETSTATUSFROMINQUIRY", "//span[@id='outerForm:statusText']"));
                 LOGGER.info("Executed Step = WRITETOCSV,ELE_GETSTATUSFROMINQUIRY,TGTYPESCREENREG");
		writeToCSV ("ELE_SUSPENSEBALANCECHECK " , ActionWrapper.getElementValue("ELE_SUSPENSEBALANCECHECK", "//span[@id='outerForm:suspenseBalance']"));
                 LOGGER.info("Executed Step = WRITETOCSV,ELE_SUSPENSEBALANCECHECK,TGTYPESCREENREG");
		var_Count = var_Count + ( 1 );
                 LOGGER.info("Executed Step = INCREASEVAR,var_Count,1,TGTYPESCREENREG");
    	}
          LOGGER.info("Executed Step = WEND,TGTYPESCREENREG");
    	resetAttemps();

    }


    @Test
    public void tc01createnewbusinesspolicyold() {

        // No need to declare / print method name here, handled by TestNg listener
        // declare("tc01createnewbusinesspolicyold");

        CALL.$("LoginToGIAS36","TGTYPESCREENREG");

        TYPE.$("ELE_USERNAME","//input[@name='outerForm:username']","GIAS36ILC","FALSE","TGTYPESCREENREG");

        TYPE.$("ELE_PASSWORD","//input[@id='outerForm:password']","Automation","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_LOGINBUTTON","outerForm:loginButtonTGWEBCOMMAIdTGWEBCOMMA0","TGTYPESCREENREG");

		int var_Count;
                 LOGGER.info("Executed Step = VAR,Integer,var_Count,TGTYPESCREENREG");
		var_Count = 0;
                 LOGGER.info("Executed Step = STORE,var_Count,0,TGTYPESCREENREG");
        LOGGER.info("Executed Step = REPEAT IF");
    	while (check(var_Count,"<",1,"TGTYPESCREENREG")) {
          LOGGER.info("Executed Step = WHILE,var_Count,<,1,TGTYPESCREENREG");
        CALL.$("AddDistributionChannel","TGTYPESCREENREG");

		com.jayway.jsonpath.ReadContext var_CSVData = getJsonData("C:/GIAS_Automation/3.6 BaseRegression/InputData/36Reg_CreateAPolicy.csv");
                 LOGGER.info("Executed Step = VAR,Json,var_CSVData,C:/GIAS_Automation/3.6 BaseRegression/InputData/36Reg_CreateAPolicy.csv,TGTYPESCREENREG");
		String var_DistributionChannelName;
                 LOGGER.info("Executed Step = VAR,String,var_DistributionChannelName,TGTYPESCREENREG");
		var_DistributionChannelName = getJsonData(var_CSVData , "$.records["+var_Count+"].DistributionChannelName");
                 LOGGER.info("Executed Step = STORE,var_DistributionChannelName,var_CSVData,$.records[{var_Count}].DistributionChannelName,TGTYPESCREENREG");
		String var_DistributionChannelDescription;
                 LOGGER.info("Executed Step = VAR,String,var_DistributionChannelDescription,TGTYPESCREENREG");
		var_DistributionChannelDescription = getJsonData(var_CSVData , "$.records["+var_Count+"].DistributionChannelDescription");
                 LOGGER.info("Executed Step = STORE,var_DistributionChannelDescription,var_CSVData,$.records[{var_Count}].DistributionChannelDescription,TGTYPESCREENREG");
        HOVER.$("ELE_AGENCYTAB","//td[text()='Agency']","TGTYPESCREENREG");

        TAP.$("ELE_DISTRIBUTIONCHANNELLINK","//td[text()='Distribution Channel']","TGTYPESCREENREG");

        TAP.$("ELE_ADDBUTTONDISTRIBUTIONCHANNEL","input[type=submit][name='outerForm:Add']TGWEBCOMMACssSelectorTGWEBCOMMA0","TGTYPESCREENREG");

        TYPE.$("ELE_DISTRIBUTIONCHANNELTEXTFIELD","//input[@type='text'][@name='outerForm:distributionChanName']",var_DistributionChannelName,"FALSE","TGTYPESCREENREG");

        TYPE.$("ELE_DESCRIPTIONTEXTFIELDFORDISTRICHANNAL","input[type=text][name='outerForm:channelDescription']TGWEBCOMMACssSelectorTGWEBCOMMA0",var_DistributionChannelDescription,"FALSE","TGTYPESCREENREG");

        TYPE.$("ELE_DISTRIBUTIONCHANNELCODETEXTFIELD","input[type=text][name='outerForm:duf_dist_code']TGWEBCOMMACssSelectorTGWEBCOMMA0",var_DistributionChannelName,"FALSE","TGTYPESCREENREG");

        TAP.$("ELE_SUBMITBUTTONDISTCHANNEL","input[type=submit][name='outerForm:Submit']TGWEBCOMMACssSelectorTGWEBCOMMA0","TGTYPESCREENREG");

        CALL.$("AddAgentRankCode","TGTYPESCREENREG");

        HOVER.$("ELE_AGENCYTAB","//td[text()='Agency']","TGTYPESCREENREG");

		String var_DistributionChannelDescription2;
                 LOGGER.info("Executed Step = VAR,String,var_DistributionChannelDescription2,TGTYPESCREENREG");
		var_DistributionChannelDescription2 = getJsonData(var_CSVData , "$.records["+var_Count+"].DistributionChannelDescription");
                 LOGGER.info("Executed Step = STORE,var_DistributionChannelDescription2,var_CSVData,$.records[{var_Count}].DistributionChannelDescription,TGTYPESCREENREG");
		String var_AgencyRankDescription;
                 LOGGER.info("Executed Step = VAR,String,var_AgencyRankDescription,TGTYPESCREENREG");
		var_AgencyRankDescription = getJsonData(var_CSVData , "$.records["+var_Count+"].AgencyRankDescription");
                 LOGGER.info("Executed Step = STORE,var_AgencyRankDescription,var_CSVData,$.records[{var_Count}].AgencyRankDescription,TGTYPESCREENREG");
		String var_AgencyRankCode;
                 LOGGER.info("Executed Step = VAR,String,var_AgencyRankCode,TGTYPESCREENREG");
		var_AgencyRankCode = getJsonData(var_CSVData , "$.records["+var_Count+"].AgencyRankCode");
                 LOGGER.info("Executed Step = STORE,var_AgencyRankCode,var_CSVData,$.records[{var_Count}].AgencyRankCode,TGTYPESCREENREG");
        TAP.$("ELE_AGENRANKCODESLINK","//td[text()='Agent Rank Codes']","TGTYPESCREENREG");

        TAP.$("ELE_ADDBUTTONAGENTCODES","//input[@type='submit'][@name='outerForm:Add']","TGTYPESCREENREG");

        SELECT.$("ELE_DROPDOWNDISTRIBUTIONCHANNELONAGENTCODES","//select[@name='outerForm:distributionChannel']",var_DistributionChannelDescription2,"TGTYPESCREENREG");

        TYPE.$("ELE_AGENCYRANKLAVELTEXTFIELD","//input[@type='text'][@name='outerForm:agencyRankLevel']","1","FALSE","TGTYPESCREENREG");

        TYPE.$("ELE_AGENCYRANKCODETEXTFIELD","//input[@type='text'][@name='outerForm:agencyRankCode']",var_AgencyRankCode,"FALSE","TGTYPESCREENREG");

        TYPE.$("ELE_AGENCYRANKDESCRIPTION","//input[@type='text'][@name='outerForm:description']",var_AgencyRankDescription,"FALSE","TGTYPESCREENREG");

        TAP.$("ELE_SUBMITBUTTONAGENTRANKCODESSCREEN","//input[@type='submit'][@name='outerForm:Submit']","TGTYPESCREENREG");

        CALL.$("AddCommissionScheduleRates","TGTYPESCREENREG");

        HOVER.$("ELE_AGENCYTAB","//td[text()='Agency']","TGTYPESCREENREG");

		String var_TableName;
                 LOGGER.info("Executed Step = VAR,String,var_TableName,TGTYPESCREENREG");
		var_TableName = getJsonData(var_CSVData , "$.records["+var_Count+"].TableName");
                 LOGGER.info("Executed Step = STORE,var_TableName,var_CSVData,$.records[{var_Count}].TableName,TGTYPESCREENREG");
		String var_TableDescription;
                 LOGGER.info("Executed Step = VAR,String,var_TableDescription,TGTYPESCREENREG");
		var_TableDescription = getJsonData(var_CSVData , "$.records["+var_Count+"].TableDescription");
                 LOGGER.info("Executed Step = STORE,var_TableDescription,var_CSVData,$.records[{var_Count}].TableDescription,TGTYPESCREENREG");
		String var_DistributionChannelDescription3;
                 LOGGER.info("Executed Step = VAR,String,var_DistributionChannelDescription3,TGTYPESCREENREG");
		var_DistributionChannelDescription3 = getJsonData(var_CSVData , "$.records["+var_Count+"].DistributionChannelDescription");
                 LOGGER.info("Executed Step = STORE,var_DistributionChannelDescription3,var_CSVData,$.records[{var_Count}].DistributionChannelDescription,TGTYPESCREENREG");
		String var_AgencyRankDescription3;
                 LOGGER.info("Executed Step = VAR,String,var_AgencyRankDescription3,TGTYPESCREENREG");
		var_AgencyRankDescription3 = getJsonData(var_CSVData , "$.records["+var_Count+"].AgencyRankDescription");
                 LOGGER.info("Executed Step = STORE,var_AgencyRankDescription3,var_CSVData,$.records[{var_Count}].AgencyRankDescription,TGTYPESCREENREG");
        TAP.$("ELE_COMMISSIONSCHEDULERATESLINK","//td[text()='Commission Schedule Rates']","TGTYPESCREENREG");

        TAP.$("ELE_ADDBUTTONCOMMISSIONSCHEDULE","//input[@type='submit'][@name='outerForm:Add']","TGTYPESCREENREG");

        TYPE.$("ELE_TABLENAMETEXTFIELD","//input[@type='text'][@name='outerForm:tableName']",var_TableName,"FALSE","TGTYPESCREENREG");

        TYPE.$("ELE_TABLEDESCRIPTIONTEXTFIELD","//input[@type='text'][@name='outerForm:description']",var_TableDescription,"FALSE","TGTYPESCREENREG");

        SELECT.$("ELE_DROPDOWNDISTRIBUTIONCHANNELCOMMISSIONSCHEDULE","//select[@name='outerForm:distributionChannel']",var_DistributionChannelDescription3,"TGTYPESCREENREG");

        TAP.$("ELE_CONTINUEBUTTONCOMMISSIONSCHEDULE","//input[@type='submit'][@name='outerForm:Continue']","TGTYPESCREENREG");

        SELECT.$("ELE_DROPPDOWNAGENCYRANKCODE","//select[@name='outerForm:grid:0:agencyRankCode']",var_AgencyRankDescription,"TGTYPESCREENREG");

        TYPE.$("ELE_ENDDURATION0TEXTFIELD","//input[@type='text'][@name='outerForm:grid:endingDuration1']","1","FALSE","TGTYPESCREENREG");

        TYPE.$("ELE_ENDDURATION1TEXTFIELD","//input[@type='text'][@name='outerForm:grid:endingDuration2']","5","FALSE","TGTYPESCREENREG");

        TYPE.$("ELE_ENDDURATION2TEXTFIELD","//input[@type='text'][@name='outerForm:grid:endingDuration3']","120","FALSE","TGTYPESCREENREG");

        TYPE.$("ELE_COMMISSIONRATE0TEXTFIELD","//input[@type='text'][@name='outerForm:grid:0:commissionRate1']","20","FALSE","TGTYPESCREENREG");

        TYPE.$("ELE_COMMISSIONRATE1TEXTFIELD","//input[@type='text'][@name='outerForm:grid:0:commissionRate2']","15","FALSE","TGTYPESCREENREG");

        TYPE.$("ELE_COMMISSIONRATE2TEXTFIELD","//input[@type='text'][@name='outerForm:grid:0:commissionRate3']","10","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_SUBMITBUTTONCOMMISSIONSCHEDULE","//input[@type='submit'][@name='outerForm:Submit']","TGTYPESCREENREG");

        CALL.$("AddCommissionContract","TGTYPESCREENREG");

        HOVER.$("ELE_AGENCYTAB","//td[text()='Agency']","TGTYPESCREENREG");

		String var_CommissionContractNumber;
                 LOGGER.info("Executed Step = VAR,String,var_CommissionContractNumber,TGTYPESCREENREG");
		var_CommissionContractNumber = getJsonData(var_CSVData , "$.records["+var_Count+"].CommissionContractNumber");
                 LOGGER.info("Executed Step = STORE,var_CommissionContractNumber,var_CSVData,$.records[{var_Count}].CommissionContractNumber,TGTYPESCREENREG");
		String var_CommissionContractDescription;
                 LOGGER.info("Executed Step = VAR,String,var_CommissionContractDescription,TGTYPESCREENREG");
		var_CommissionContractDescription = getJsonData(var_CSVData , "$.records["+var_Count+"].CommissionContractDescription");
                 LOGGER.info("Executed Step = STORE,var_CommissionContractDescription,var_CSVData,$.records[{var_Count}].CommissionContractDescription,TGTYPESCREENREG");
		String var_DistributionChannelDescription4;
                 LOGGER.info("Executed Step = VAR,String,var_DistributionChannelDescription4,TGTYPESCREENREG");
		var_DistributionChannelDescription4 = getJsonData(var_CSVData , "$.records["+var_Count+"].DistributionChannelDescription");
                 LOGGER.info("Executed Step = STORE,var_DistributionChannelDescription4,var_CSVData,$.records[{var_Count}].DistributionChannelDescription,TGTYPESCREENREG");
		String var_AgencyRankDescription4;
                 LOGGER.info("Executed Step = VAR,String,var_AgencyRankDescription4,TGTYPESCREENREG");
		var_AgencyRankDescription4 = getJsonData(var_CSVData , "$.records["+var_Count+"].AgencyRankDescription");
                 LOGGER.info("Executed Step = STORE,var_AgencyRankDescription4,var_CSVData,$.records[{var_Count}].AgencyRankDescription,TGTYPESCREENREG");
        TAP.$("ELE_COMMISSIONCONTRACTSLINK","//td[text()='Commission Contracts']","TGTYPESCREENREG");

        TAP.$("ELE_ADDBUTTONCOMMISSIONCONTRACT","//input[@type='submit'][@name='outerForm:Add']","TGTYPESCREENREG");

        TYPE.$("ELE_COMMSSIONCONTRACTNOTEXTFIELD","//input[@type='text'][@name='outerForm:commissionContNumb']",var_CommissionContractNumber,"FALSE","TGTYPESCREENREG");

        TYPE.$("ELE_COMMISSIONCONTRACTDESCTEXTFIELD","//input[@type='text'][@name='outerForm:longDescription']",var_CommissionContractDescription,"FALSE","TGTYPESCREENREG");

        SELECT.$("ELE_DISTRIBUTIONCHANNELLISTCOMMISSIONCONTRACT","//select[@name='outerForm:distributionChannel']",var_DistributionChannelDescription4,"TGTYPESCREENREG");

		try { 
		 java.time.LocalDate date = java.time.LocalDate.now();
		java.time.format.DateTimeFormatter formatter = java.time.format.DateTimeFormatter.ofPattern("MM/dd/yyyy");
		String formattedDate = date.format(formatter);
		driver.findElement(By.xpath("//input[@type='text'][@name='outerForm:effectiveDate']")).sendKeys(formattedDate);
		 
		 ;} 
		 catch(Exception e) { PrintVal("Renderer Script Exception : "+e.toString()); } 
		 PrintVal("Executed Step = CALLRENDERERSCRIPT,CURRENTDATEASEFFECTIVEDATE"); 
        SELECT.$("ELE_PAYBACKMETHODLIST","//select[@name='outerForm:advancePaybackMethod']","100% of Commission Earned","TGTYPESCREENREG");

        SELECT.$("ELE_ADVANCETHROUGHAGENTRANKLIST","//select[@name='outerForm:topLevelForAdvance']",var_AgencyRankDescription4,"TGTYPESCREENREG");

        TYPE.$("ELE_DAYSPASTDUETEXTFIELD","//input[@type='text'][@name='outerForm:daysLateBeforeChar']","3","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_SUBMITBUTTONCOMMISSIONCONTRACT","//input[@type='submit'][@name='outerForm:Submit']","TGTYPESCREENREG");

        CALL.$("AddCommissionSchedule","TGTYPESCREENREG");

        HOVER.$("ELE_AGENCYTAB","//td[text()='Agency']","TGTYPESCREENREG");

		String var_CommissionContractNumber5;
                 LOGGER.info("Executed Step = VAR,String,var_CommissionContractNumber5,TGTYPESCREENREG");
		var_CommissionContractNumber5 = getJsonData(var_CSVData , "$.records["+var_Count+"].CommissionContractNumber");
                 LOGGER.info("Executed Step = STORE,var_CommissionContractNumber5,var_CSVData,$.records[{var_Count}].CommissionContractNumber,TGTYPESCREENREG");
		String var_TableDescription5;
                 LOGGER.info("Executed Step = VAR,String,var_TableDescription5,TGTYPESCREENREG");
		var_TableDescription5 = getJsonData(var_CSVData , "$.records["+var_Count+"].TableDescription");
                 LOGGER.info("Executed Step = STORE,var_TableDescription5,var_CSVData,$.records[{var_Count}].TableDescription,TGTYPESCREENREG");
		String var_LineDescription;
                 LOGGER.info("Executed Step = VAR,String,var_LineDescription,TGTYPESCREENREG");
		var_LineDescription = getJsonData(var_CSVData , "$.records["+var_Count+"].LineDescription");
                 LOGGER.info("Executed Step = STORE,var_LineDescription,var_CSVData,$.records[{var_Count}].LineDescription,TGTYPESCREENREG");
        TAP.$("ELE_COMMISSIONCONTRACTSLINK","//td[text()='Commission Contracts']","TGTYPESCREENREG");

        TYPE.$("ELE_COMMISSIONCONTRACTNOSEARCHTEXTFIELD","//input[@type='text'][@name='outerForm:grid:commissionContNumb']",var_CommissionContractNumber5,"FALSE","TGTYPESCREENREG");

        TAP.$("ELE_FINDPOSITIONTOBUTTON","//img[@id='outerForm:gnFindImage']","TGTYPESCREENREG");

        TAP.$("ELE_COMMISSIONCONTRACT2224","span[id='outerForm:grid:0:commissionContNumbOut1']TGWEBCOMMACssSelectorTGWEBCOMMA0","TGTYPESCREENREG");

        TAP.$("ELE_COMMISSIONSCHEDULELINK","//span[text()='Commission Schedule']","TGTYPESCREENREG");

        TAP.$("ELE_ADDBUTTONCOMMISSIONSCHEDULE","//input[@type='submit'][@name='outerForm:Add']","TGTYPESCREENREG");

        SELECT.$("ELE_CONTRACTTYPELIST","//select[@name='outerForm:commissionContType']","Health","TGTYPESCREENREG");

		try { 
		 java.time.LocalDate date = java.time.LocalDate.now();
		java.time.format.DateTimeFormatter formatter = java.time.format.DateTimeFormatter.ofPattern("MM/dd/yyyy");
		String formattedDate = date.format(formatter);
		driver.findElement(By.xpath("//input[@type='text'][@name='outerForm:effectiveDate']")).sendKeys(formattedDate);
		 
		 ;} 
		 catch(Exception e) { PrintVal("Renderer Script Exception : "+e.toString()); } 
		 PrintVal("Executed Step = CALLRENDERERSCRIPT,CURRENTDATEASEFFECTIVEDATE"); 
        SELECT.$("ELE_COMMISSIONRATETABLELIST","//select[@name='outerForm:commissionRateTableName']",var_TableDescription5,"TGTYPESCREENREG");

        TYPE.$("ELE_LINEDESCRIPTIONTEXTFIELD","//input[@type='text'][@name='outerForm:shortDescription']",var_LineDescription,"FALSE","TGTYPESCREENREG");

        TAP.$("ELE_SUBMITTBUTTONADDCOMMSCHE","//input[@type='submit'][@name='outerForm:Submit']","TGTYPESCREENREG");

        CALL.$("AddNewBusinessPolicy","TGTYPESCREENREG");

        HOVER.$("ELE_NEWBUSINESSTAB","//td[text()='New Business']","TGTYPESCREENREG");

        TAP.$("ELE_APPLICATIONENTRYUPDATELINK","//td[text()='Application Entry/Update']","TGTYPESCREENREG");

        TAP.$("ELE_ADDBUTTONAPPLICATIONENTRY","//input[@type='submit'][@name='outerForm:Add']","TGTYPESCREENREG");

		String var_PolicyNumber;
                 LOGGER.info("Executed Step = VAR,String,var_PolicyNumber,TGTYPESCREENREG");
		var_PolicyNumber = getJsonData(var_CSVData , "$.records["+var_Count+"].PolicyNumber");
                 LOGGER.info("Executed Step = STORE,var_PolicyNumber,var_CSVData,$.records[{var_Count}].PolicyNumber,TGTYPESCREENREG");
		String var_FirstName;
                 LOGGER.info("Executed Step = VAR,String,var_FirstName,TGTYPESCREENREG");
		var_FirstName = getJsonData(var_CSVData , "$.records["+var_Count+"].FirstName");
                 LOGGER.info("Executed Step = STORE,var_FirstName,var_CSVData,$.records[{var_Count}].FirstName,TGTYPESCREENREG");
		String var_LastName;
                 LOGGER.info("Executed Step = VAR,String,var_LastName,TGTYPESCREENREG");
		var_LastName = getJsonData(var_CSVData , "$.records["+var_Count+"].LastName");
                 LOGGER.info("Executed Step = STORE,var_LastName,var_CSVData,$.records[{var_Count}].LastName,TGTYPESCREENREG");
		String var_IDNumber;
                 LOGGER.info("Executed Step = VAR,String,var_IDNumber,TGTYPESCREENREG");
		var_IDNumber = getJsonData(var_CSVData , "$.records["+var_Count+"].IDNumber");
                 LOGGER.info("Executed Step = STORE,var_IDNumber,var_CSVData,$.records[{var_Count}].IDNumber,TGTYPESCREENREG");
		String var_ClientSearchName;
                 LOGGER.info("Executed Step = VAR,String,var_ClientSearchName,TGTYPESCREENREG");
		var_ClientSearchName = getJsonData(var_CSVData , "$.records["+var_Count+"].ClientSearchName");
                 LOGGER.info("Executed Step = STORE,var_ClientSearchName,var_CSVData,$.records[{var_Count}].ClientSearchName,TGTYPESCREENREG");
		String var_AgentNumber;
                 LOGGER.info("Executed Step = VAR,String,var_AgentNumber,TGTYPESCREENREG");
		var_AgentNumber = getJsonData(var_CSVData , "$.records["+var_Count+"].AgentNumber");
                 LOGGER.info("Executed Step = STORE,var_AgentNumber,var_CSVData,$.records[{var_Count}].AgentNumber,TGTYPESCREENREG");
        TYPE.$("ELE_POLICYNUMBERTEXTFIELD","//input[@type='text'][@name='outerForm:policyNumber']",var_PolicyNumber,"FALSE","TGTYPESCREENREG");

        SELECT.$("ELE_CURRENCYCODELIST","//select[@name='outerForm:currencyCode']","Dollars [US]","TGTYPESCREENREG");

        SELECT.$("ELE_COUNTRYANDSTATECODELIST","//select[@name='outerForm:countryAndStateCode']","Missouri US","TGTYPESCREENREG");

        TYPE.$("ELE_CASHWITHAPPLICATIONTEXTFIELD","//input[@type='text'][@name='outerForm:cashWithApplication']","25000","FALSE","TGTYPESCREENREG");

        SELECT.$("ELE_PAYMENTFREQUENCYLIST","//select[@name='outerForm:paymentMode']","Annual","TGTYPESCREENREG");

        SELECT.$("ELE_PAYMENTMETHODLIST","//select[@name='outerForm:paymentCode']","Coupon Book","TGTYPESCREENREG");

        SELECT.$("ELE_TAXQUALIFIEDDESCRIPTIONLIST","//select[@name='outerForm:taxQualifiedCode']","NON QUALIFIED","TGTYPESCREENREG");

        SELECT.$("ELE_TAXWITHHOLDINGLIST","//select[@name='outerForm:taxWithholdingCode']","No Withholding","TGTYPESCREENREG");

        TAP.$("ELE_SELECTCLIENTLINK","//span[text()='Select Client']","TGTYPESCREENREG");

        TAP.$("ELE_ADDBUTTONCLIENTRELATIONSHOPPAGE","//input[@type='submit'][@name='outerForm:Add']","TGTYPESCREENREG");

        TYPE.$("ELE_FIRSTNAMETEXTFIELD","//input[@type='text'][@name='outerForm:nameFirst']",var_FirstName,"FALSE","TGTYPESCREENREG");

        TYPE.$("ELE_LASTNAMETEXTFIELD","//input[@type='text'][@name='outerForm:nameLast']",var_LastName,"FALSE","TGTYPESCREENREG");

        TYPE.$("ELE_CLIENT_DOB","//input[@type='text'][@name='outerForm:dateOfBirth2']","01/01/1980","FALSE","TGTYPESCREENREG");

        SELECT.$("ELE_CLIENT_GENDER_LIST","//select[@name='outerForm:sexCode2']","Male","TGTYPESCREENREG");

        SELECT.$("ELE_IDTYPELIST","//select[@name='outerForm:taxIdenUsag2']","Social Security Number","TGTYPESCREENREG");

        TYPE.$("ELE_IDNUMBERTEXTFIELD","//input[@type='text'][@name='outerForm:identificationNumber2']",var_IDNumber,"FALSE","TGTYPESCREENREG");

        TAP.$("ELE_SUBMITBUTTONADDCLIENTPAGE","//input[@type='submit'][@name='outerForm:Submit']","TGTYPESCREENREG");

        TYPE.$("ELE_ADDRESSLINE1TEXTFIELD","//input[@type='text'][@name='outerForm:addressLineOne']","123 Main St","FALSE","TGTYPESCREENREG");

        TYPE.$("ELE_CITYTEXTFIELD","//input[@type='text'][@name='outerForm:addressCity']","Kansas City","FALSE","TGTYPESCREENREG");

        SELECT.$("ELE_COUNTRYANDSTATECODELIST","//select[@name='outerForm:countryAndStateCode']","Missouri US","TGTYPESCREENREG");

        TYPE.$("ELE_ZIPCODETEXTFIELD","//input[@type='text'][@name='outerForm:zipCode']","64153","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_SUBMITBUTTONADDADDRESSPAGE","//input[@type='submit'][@name='outerForm:Submit']","TGTYPESCREENREG");

        TAP.$("ELE_SELECTCLIENTRELATIONSHIPSLINK","//a[text()='Select Client Relationships']","TGTYPESCREENREG");

        TYPE.$("ELE_CLIENTNAMESEARCHTEXTFIELD","//input[@type='text'][@name='outerForm:grid:individualName']",var_ClientSearchName,"FALSE","TGTYPESCREENREG");

        TAP.$("ELE_FINDPOSITIONTOCLIENTNAMEBUTTON","//img[@id='outerForm:gnFindImage']","TGTYPESCREENREG");

        SELECT.$("ELE_RELATIONSHIPGRID0LIST","//select[@name='outerForm:grid:0:relationship']","Owner 1","TGTYPESCREENREG");

        TAP.$("ELE_SUBMITBUTTONSELECTCLIENTRELATIONPAGE","//input[@type='submit'][@name='outerForm:Submit']","TGTYPESCREENREG");

        TYPE.$("ELE_AGENTNUMBERTEXTFIELD","//input[@type='text'][@name='outerForm:grid:0:agentNumberOut2']",var_AgentNumber,"FALSE","TGTYPESCREENREG");

        TAP.$("ELE_SUBMITBUTTONADDNEWBUSINESS","//*[@name='outerForm:Submit']","TGTYPESCREENREG");

        CALL.$("SettlePolicy","TGTYPESCREENREG");

        TAP.$("ELE_BENEFITSLINK","//span[text()='Benefits']","TGTYPESCREENREG");

		String var_BenefitPlan = "null";
                 LOGGER.info("Executed Step = VAR,String,var_BenefitPlan,null,TGTYPESCREENREG");
		var_BenefitPlan = getJsonData(var_CSVData , "$.records["+var_Count+"].BenefitPlan");
                 LOGGER.info("Executed Step = STORE,var_BenefitPlan,var_CSVData,$.records[{var_Count}].BenefitPlan,TGTYPESCREENREG");
        SELECT.$("ELE_DROPDOWNTOADDABENEFIT","//select[@name='outerForm:initialPlanCode']",var_BenefitPlan,"TGTYPESCREENREG");

        TAP.$("ELE_CONTINUEAFTERSELECTINGBENEFIT","//input[@type='submit'][@name='outerForm:Continue']","TGTYPESCREENREG");

        TYPE.$("ELE_TEXTTOENTERUNITSAFTERADDINGBENEFIT","//input[@type='text'][@name='outerForm:unitsOfInsurance']	","25.00","FALSE","TGTYPESCREENREG");

        LOGGER.info("Executed Step = START IF");
        if (check(var_BenefitPlan,"CONTAINS","ULAT1","TGTYPESCREENREG")) {
         LOGGER.info("Executed Step = IF,var_BenefitPlan,CONTAINS,ULAT1,TGTYPESCREENREG");
        TAP.$("ELE_RDOBTN_FACEAMOUNT","//input[@id='outerForm:deathBenefitOption:0']","TGTYPESCREENREG");

        }
          LOGGER.info("Executed Step = ENDIF,TGTYPESCREENREG");
        SWIPE.$("UP","TGTYPESCREENREG");

		try { 
		  List<WebElement> checkBoxes = driver.findElements(By.xpath("//input[@type='checkbox']"));
				int numberOfBoxes =  checkBoxes.size();
				if( numberOfBoxes > 0) {
						for(int i=0;i<numberOfBoxes;i++)
						{
					 driver.findElement(By.xpath("//input[@id='outerForm:gridSupp:" + i + ":selectedCheck']")).click();

		 if(driver.findElements(By.xpath("//input[@id='outerForm:gridSupp:"+ i + ":unitsOut2']")).size()!= 0)
			 {	 
				driver.findElement(By.xpath("//input[@id='outerForm:gridSupp:" + i + ":unitsOut2']")).clear();
				 driver.findElement(By.xpath("//input[@id='outerForm:gridSupp:" + i + ":unitsOut2']")).sendKeys("25.00");
			}
			else { System.out.println("Not found"); }	
			}
		}
		 
		 ;} 
		 catch(Exception e) { PrintVal("Renderer Script Exception : "+e.toString()); } 
		 PrintVal("Executed Step = CALLRENDERERSCRIPT,COUNTSUPPLEMENTCHECKBOX"); 
        TAP.$("ELE_SUBMITBENEFITWITHUNITS","//input[@type='submit'][@name='outerForm:Submit']","TGTYPESCREENREG");

        TAP.$("ELE_CANCELAFTERADDINGBENEFITS","//input[@type='submit'][@name='outerForm:Cancel']","TGTYPESCREENREG");

        TAP.$("ELE_CLICKISSUEDECLINEWITHDRAWLINK","//span[text()='Issue/Decline/Withdraw']","TGTYPESCREENREG");

		String var_effectiveDate = "null";
                 LOGGER.info("Executed Step = VAR,String,var_effectiveDate,null,TGTYPESCREENREG");
		try { 
		 java.time.LocalDate date = java.time.LocalDate.now();
		java.time.format.DateTimeFormatter formatter = java.time.format.DateTimeFormatter.ofPattern("MM/dd/yyyy");
		String formattedDate = date.format(formatter);
		var_effectiveDate = formattedDate;
		 
		 ;} 
		 catch(Exception e) { PrintVal("Renderer Script Exception : "+e.toString()); } 
		 PrintVal("Executed Step = CALLRENDERERSCRIPT,CURRENTDATEASISSUEDATE"); 
        TYPE.$("ELE_SELECTDATEINISSUEDECLINEWITHDRAWPAGE","//input[@type='text'][@name='outerForm:effectiveDate']",var_effectiveDate,"TRUE","TGTYPESCREENREG");

        TAP.$("ELE_CLICKISSUEBUTTONTOISSUETHEPOLICY","//input[@type='submit'][@name='outerForm:Issue']	","TGTYPESCREENREG");

        TAP.$("ELE_CLICKSETTLELINKTOSETTLETHEPOLICY","//span[text()='Settle']	","TGTYPESCREENREG");

        TAP.$("ELE_CLICKSUBMITTOSETTLETHEPOLICY","//input[@type='submit'][@name='outerForm:Submit']	","TGTYPESCREENREG");

        CALL.$("WritePolicyDetailToCSV","TGTYPESCREENREG");

        HOVER.$("ELE_INQUIRYMENU","//td[@class='ThemePanelMainFolderText'][contains(text(),'Inquiry')]","TGTYPESCREENREG");

        TAP.$("ELE_QUICKINQUIRYSUBMENU","//td[contains(text(),'Quick Inquiry')]","TGTYPESCREENREG");

        TYPE.$("ELE_POLICYNUMBERTEXTFIELD","//input[@type='text'][@name='outerForm:policyNumber']",var_PolicyNumber,"FALSE","TGTYPESCREENREG");

        TAP.$("ELE_CONTINUEBUTTON","//input[@id='outerForm:Continue']","TGTYPESCREENREG");

		writeToCSV ("ELE_GETPOLICYNUMBER " , ActionWrapper.getElementValue("ELE_GETPOLICYNUMBER", "//span[@id='outerForm:policyNumber']"));
                 LOGGER.info("Executed Step = WRITETOCSV,ELE_GETPOLICYNUMBER,TGTYPESCREENREG");
		writeToCSV ("ELE_GETSTATUSFROMINQUIRY " , ActionWrapper.getElementValue("ELE_GETSTATUSFROMINQUIRY", "//span[@id='outerForm:statusText']"));
                 LOGGER.info("Executed Step = WRITETOCSV,ELE_GETSTATUSFROMINQUIRY,TGTYPESCREENREG");
		writeToCSV ("ELE_GETEFFECTIVEDATETEXTFROMGRID " , ActionWrapper.getElementValue("ELE_GETEFFECTIVEDATETEXTFROMGRID", "//span[@id='outerForm:grid:0:effectiveDateOut2']"));
                 LOGGER.info("Executed Step = WRITETOCSV,ELE_GETEFFECTIVEDATETEXTFROMGRID,TGTYPESCREENREG");
		writeToCSV ("ELE_GETSTATECOUNTRY " , ActionWrapper.getElementValue("ELE_GETSTATECOUNTRY", "//span[@id='outerForm:ownerCityStateZip']"));
                 LOGGER.info("Executed Step = WRITETOCSV,ELE_GETSTATECOUNTRY,TGTYPESCREENREG");
		writeToCSV ("ELE_GETAGENTNUMBER " , ActionWrapper.getElementValue("ELE_GETAGENTNUMBER", "//span[@id='outerForm:agentNumber']"));
                 LOGGER.info("Executed Step = WRITETOCSV,ELE_GETAGENTNUMBER,TGTYPESCREENREG");
		var_Count = var_Count + ( 1 );
                 LOGGER.info("Executed Step = INCREASEVAR,var_Count,1,TGTYPESCREENREG");
    	}
          LOGGER.info("Executed Step = WEND,TGTYPESCREENREG");
    	resetAttemps();

    }


    @Test
    public void tc04addabeneficiary() {

        // No need to declare / print method name here, handled by TestNg listener
        // declare("tc04addabeneficiary");

		int var_Count;
                 LOGGER.info("Executed Step = VAR,Integer,var_Count,TGTYPESCREENREG");
		var_Count = 0;
                 LOGGER.info("Executed Step = STORE,var_Count,0,TGTYPESCREENREG");
        LOGGER.info("Executed Step = REPEAT IF");
    	while (check(var_Count,"<",1,"TGTYPESCREENREG")) {
          LOGGER.info("Executed Step = WHILE,var_Count,<,1,TGTYPESCREENREG");
        CALL.$("LoginToGIAS36","TGTYPESCREENREG");

        TYPE.$("ELE_USERNAME","//input[@name='outerForm:username']","GIAS36ILC","FALSE","TGTYPESCREENREG");

        TYPE.$("ELE_PASSWORD","//input[@id='outerForm:password']","Automation","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_LOGINBUTTON","outerForm:loginButtonTGWEBCOMMAIdTGWEBCOMMA0","TGTYPESCREENREG");

        CALL.$("AddABeneficiary","TGTYPESCREENREG");

        HOVER.$("ELE_CLICKCLIENT","//td[text()='Client']","TGTYPESCREENREG");

        TAP.$("ELE_CLICKPOLICYRELATIONSHIPS","//td[text()='Policy Relationships']","TGTYPESCREENREG");

		com.jayway.jsonpath.ReadContext var_CSVData = getJsonData("C:/GIAS_Automation/3.6 BaseRegression/InputData/36Reg_AddBeneficiary.csv");
                 LOGGER.info("Executed Step = VAR,Json,var_CSVData,C:/GIAS_Automation/3.6 BaseRegression/InputData/36Reg_AddBeneficiary.csv,TGTYPESCREENREG");
		String var_PolicyNumber;
                 LOGGER.info("Executed Step = VAR,String,var_PolicyNumber,TGTYPESCREENREG");
		var_PolicyNumber = getJsonData(var_CSVData , "$.records["+var_Count+"].PolicyNumber");
                 LOGGER.info("Executed Step = STORE,var_PolicyNumber,var_CSVData,$.records[{var_Count}].PolicyNumber,TGTYPESCREENREG");
        TYPE.$("ELE_ENTERPOLICYNUMBER","input[type=text][name='outerForm:policyNumber']TGWEBCOMMACssSelectorTGWEBCOMMA0",var_PolicyNumber,"FALSE","TGTYPESCREENREG");

        TAP.$("ELE_CLICKCONTINUEFORRELATIONSHIPS","//input[@type='submit'][@name='outerForm:Continue']","TGTYPESCREENREG");

        TAP.$("ELE_ADDACLIENTRELATIONSHIP","//input[@type='submit'][@name='outerForm:AddRelationship']","TGTYPESCREENREG");

        SELECT.$("ELE_SELECTRELATIONSHIPTYPE","*[name='outerForm:userDefnRelaType']TGWEBCOMMACssSelectorTGWEBCOMMA0","Beneficiary (Contract)","TGTYPESCREENREG");

        TYPE.$("ELE_ENTERSHAREPERCENTAGE","//input[@type='text'][@name='outerForm:sharePercentage']","100.00","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_SELECTCLIENTLINK","//span[text()='Select Client']","TGTYPESCREENREG");

        TAP.$("ELE_CLICKADDBUTTONFORBENEFICIARY","//input[@type='submit'][@name='outerForm:Add']","TGTYPESCREENREG");

		String var_BeneficiaryFirstName;
                 LOGGER.info("Executed Step = VAR,String,var_BeneficiaryFirstName,TGTYPESCREENREG");
		var_BeneficiaryFirstName = getJsonData(var_CSVData , "$.records["+var_Count+"].BeneficiaryFirstName");
                 LOGGER.info("Executed Step = STORE,var_BeneficiaryFirstName,var_CSVData,$.records[{var_Count}].BeneficiaryFirstName,TGTYPESCREENREG");
		String var_BeneficiaryLastName;
                 LOGGER.info("Executed Step = VAR,String,var_BeneficiaryLastName,TGTYPESCREENREG");
		var_BeneficiaryLastName = getJsonData(var_CSVData , "$.records["+var_Count+"].BeneficiaryLastName");
                 LOGGER.info("Executed Step = STORE,var_BeneficiaryLastName,var_CSVData,$.records[{var_Count}].BeneficiaryLastName,TGTYPESCREENREG");
		String var_BeneficiaryBirthState;
                 LOGGER.info("Executed Step = VAR,String,var_BeneficiaryBirthState,TGTYPESCREENREG");
		var_BeneficiaryBirthState = getJsonData(var_CSVData , "$.records["+var_Count+"].BeneficiaryBirthState");
                 LOGGER.info("Executed Step = STORE,var_BeneficiaryBirthState,var_CSVData,$.records[{var_Count}].BeneficiaryBirthState,TGTYPESCREENREG");
		String var_BeneficiaryDOB;
                 LOGGER.info("Executed Step = VAR,String,var_BeneficiaryDOB,TGTYPESCREENREG");
		var_BeneficiaryDOB = getJsonData(var_CSVData , "$.records["+var_Count+"].BeneficiaryDOB");
                 LOGGER.info("Executed Step = STORE,var_BeneficiaryDOB,var_CSVData,$.records[{var_Count}].BeneficiaryDOB,TGTYPESCREENREG");
		String var_BeneficiaryGender;
                 LOGGER.info("Executed Step = VAR,String,var_BeneficiaryGender,TGTYPESCREENREG");
		var_BeneficiaryGender = getJsonData(var_CSVData , "$.records["+var_Count+"].BeneficiaryGender");
                 LOGGER.info("Executed Step = STORE,var_BeneficiaryGender,var_CSVData,$.records[{var_Count}].BeneficiaryGender,TGTYPESCREENREG");
		String var_BeneficiaryIDType;
                 LOGGER.info("Executed Step = VAR,String,var_BeneficiaryIDType,TGTYPESCREENREG");
		var_BeneficiaryIDType = getJsonData(var_CSVData , "$.records["+var_Count+"].BeneficiaryIDType");
                 LOGGER.info("Executed Step = STORE,var_BeneficiaryIDType,var_CSVData,$.records[{var_Count}].BeneficiaryIDType,TGTYPESCREENREG");
		String var_BeneficiaryIDNumber;
                 LOGGER.info("Executed Step = VAR,String,var_BeneficiaryIDNumber,TGTYPESCREENREG");
		var_BeneficiaryIDNumber = getJsonData(var_CSVData , "$.records["+var_Count+"].BeneficiaryIDNumber");
                 LOGGER.info("Executed Step = STORE,var_BeneficiaryIDNumber,var_CSVData,$.records[{var_Count}].BeneficiaryIDNumber,TGTYPESCREENREG");
        TYPE.$("ELE_ENTERBENEFICIARYFIRSTNAME","input[type=text][name='outerForm:nameFirst']TGWEBCOMMACssSelectorTGWEBCOMMA0",var_BeneficiaryFirstName,"FALSE","TGTYPESCREENREG");

        TYPE.$("ELE_ENTERBENEFICIARYLASTNAME","input[type=text][name='outerForm:nameLast']TGWEBCOMMACssSelectorTGWEBCOMMA0",var_BeneficiaryLastName,"FALSE","TGTYPESCREENREG");

        SELECT.$("ELE_SELECTBENEFICIARYSTATE","select[name='outerForm:birthCountryAndState2']TGWEBCOMMACssSelectorTGWEBCOMMA0",var_BeneficiaryBirthState,"TGTYPESCREENREG");

        TYPE.$("ELE_SELECTBENEFICIARYDOB","input[type=text][name='outerForm:dateOfBirth2']TGWEBCOMMACssSelectorTGWEBCOMMA0",var_BeneficiaryDOB,"FALSE","TGTYPESCREENREG");

        SELECT.$("ELE_SELECTBENEFICIARYGENDER","select[name='outerForm:sexCode2']TGWEBCOMMACssSelectorTGWEBCOMMA0",var_BeneficiaryGender,"TGTYPESCREENREG");

        SELECT.$("ELE_SELECTBENEFICIARYID","select[name='outerForm:taxIdenUsag2']TGWEBCOMMACssSelectorTGWEBCOMMA0",var_BeneficiaryIDType,"TGTYPESCREENREG");

        TYPE.$("ELE_BENEFICIARYIDNUMBER","input[type=text][name='outerForm:identificationNumber2']TGWEBCOMMACssSelectorTGWEBCOMMA0",var_BeneficiaryIDNumber,"FALSE","TGTYPESCREENREG");

        TAP.$("ELE_CLICKSUBMITBENEFICIARYADDRESS","//input[@type='submit'][@name='outerForm:Submit']","TGTYPESCREENREG");

		String var_BeneficiaryStreetAddress;
                 LOGGER.info("Executed Step = VAR,String,var_BeneficiaryStreetAddress,TGTYPESCREENREG");
		var_BeneficiaryStreetAddress = getJsonData(var_CSVData , "$.records["+var_Count+"].BeneficiaryStreetAddress");
                 LOGGER.info("Executed Step = STORE,var_BeneficiaryStreetAddress,var_CSVData,$.records[{var_Count}].BeneficiaryStreetAddress,TGTYPESCREENREG");
        TYPE.$("ELE_ENTERBENEFICIARYSTREETADDRESS","input[type=text][name='outerForm:addressLineOne']TGWEBCOMMACssSelectorTGWEBCOMMA0",var_BeneficiaryStreetAddress,"FALSE","TGTYPESCREENREG");

		String var_BeneficiaryCity;
                 LOGGER.info("Executed Step = VAR,String,var_BeneficiaryCity,TGTYPESCREENREG");
		var_BeneficiaryCity = getJsonData(var_CSVData , "$.records["+var_Count+"].BeneficiaryCity");
                 LOGGER.info("Executed Step = STORE,var_BeneficiaryCity,var_CSVData,$.records[{var_Count}].BeneficiaryCity,TGTYPESCREENREG");
        TYPE.$("ELE_ENTERBENEFICIARYCITY","input[type=text][name='outerForm:addressCity']TGWEBCOMMACssSelectorTGWEBCOMMA0",var_BeneficiaryCity,"FALSE","TGTYPESCREENREG");

        SELECT.$("ELE_SELECTBENEFICIARYSTATEANDCOUNTRY","select[name='outerForm:countryAndStateCode']TGWEBCOMMACssSelectorTGWEBCOMMA0",var_BeneficiaryBirthState,"TGTYPESCREENREG");

		String var_BeneficairyZip;
                 LOGGER.info("Executed Step = VAR,String,var_BeneficairyZip,TGTYPESCREENREG");
		var_BeneficairyZip = getJsonData(var_CSVData , "$.records["+var_Count+"].BeneficairyZip");
                 LOGGER.info("Executed Step = STORE,var_BeneficairyZip,var_CSVData,$.records[{var_Count}].BeneficairyZip,TGTYPESCREENREG");
        TYPE.$("ELE_SELECTBENEFICIARYZIP","input[type=text][name='outerForm:zipCode']TGWEBCOMMACssSelectorTGWEBCOMMA0",var_BeneficairyZip,"FALSE","TGTYPESCREENREG");

        TAP.$("ELE_CLICKSUBMITBENEFICIARYADDRESS","//input[@type='submit'][@name='outerForm:Submit']","TGTYPESCREENREG");

        TAP.$("ELE_CLICKCANCELAFTERBENEFICIARYADDRESS","//input[@type='submit'][@name='outerForm:Cancel']","TGTYPESCREENREG");

        SELECT.$("ELE_SELECTBENEFICIARYRELATIONSHIP","select[name='outerForm:grid:0:relationship']TGWEBCOMMACssSelectorTGWEBCOMMA0","Beneficiary (Contract)","TGTYPESCREENREG");

        TAP.$("ELE_CLICKSUBMITAFTERBENEFICIARYRELATIONSHIP","//input[@type='submit'][@name='outerForm:Submit']","TGTYPESCREENREG");

        TAP.$("ELE_CLICKSUBMITTOADDBENEFICIARY","//input[@type='submit'][@name='outerForm:Submit']","TGTYPESCREENREG");

        TAP.$("ELE_CLICKCANCELAFTERBENEFICIARYADDED","//input[@type='submit'][@name='outerForm:Cancel']","TGTYPESCREENREG");

        TAP.$("ELE_CLICKBENEFICIARYLINK","//span[text()='Beneficiary (Contract)']","TGTYPESCREENREG");

		writeToCSV("ELE_NEWBENEFICIARYNAME " , ActionWrapper.getWebElementValueForVariable("span[id='outerForm:clientName']", "CssSelector", 0, "span[id='outerForm:clientName']TGWEBCOMMACssSelectorTGWEBCOMMA0"));
                 LOGGER.info("Executed Step = WRITETOCSV,ELE_NEWBENEFICIARYNAME,TGTYPESCREENREG");
		writeToCSV("ELE_NEWBENEFIICIARYRELATIONTYPE " , ActionWrapper.getWebElementValueForVariable("span[id='outerForm:userDefnRelaType']", "CssSelector", 0, "span[id='outerForm:userDefnRelaType']TGWEBCOMMACssSelectorTGWEBCOMMA0"));
                 LOGGER.info("Executed Step = WRITETOCSV,ELE_NEWBENEFIICIARYRELATIONTYPE,TGTYPESCREENREG");
		writeToCSV("ELE_NEWBENEFICIARYPERCENTAGE " , ActionWrapper.getWebElementValueForVariable("span[id='outerForm:sharePercentageText']", "CssSelector", 0, "span[id='outerForm:sharePercentageText']TGWEBCOMMACssSelectorTGWEBCOMMA0"));
                 LOGGER.info("Executed Step = WRITETOCSV,ELE_NEWBENEFICIARYPERCENTAGE,TGTYPESCREENREG");
		var_Count = var_Count + ( 1 );
                 LOGGER.info("Executed Step = INCREASEVAR,var_Count,1,TGTYPESCREENREG");
    	}
          LOGGER.info("Executed Step = WEND,TGTYPESCREENREG");
    	resetAttemps();

    }


    @Test
    public void tc05beneficiarychanges() {

        // No need to declare / print method name here, handled by TestNg listener
        // declare("tc05beneficiarychanges");

		int var_Count;
                 LOGGER.info("Executed Step = VAR,Integer,var_Count,TGTYPESCREENREG");
		var_Count = 0;
                 LOGGER.info("Executed Step = STORE,var_Count,0,TGTYPESCREENREG");
        LOGGER.info("Executed Step = REPEAT IF");
    	while (check(var_Count,"<",1,"TGTYPESCREENREG")) {
          LOGGER.info("Executed Step = WHILE,var_Count,<,1,TGTYPESCREENREG");
        CALL.$("LoginToGIAS36","TGTYPESCREENREG");

        TYPE.$("ELE_USERNAME","//input[@name='outerForm:username']","GIAS36ILC","FALSE","TGTYPESCREENREG");

        TYPE.$("ELE_PASSWORD","//input[@id='outerForm:password']","Automation","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_LOGINBUTTON","outerForm:loginButtonTGWEBCOMMAIdTGWEBCOMMA0","TGTYPESCREENREG");

        CALL.$("BeneficiaryChanges","TGTYPESCREENREG");

        HOVER.$("ELE_CLICKCLIENT","//td[text()='Client']","TGTYPESCREENREG");

        TAP.$("ELE_CLICKPOLICYRELATIONSHIPS","//td[text()='Policy Relationships']","TGTYPESCREENREG");

		com.jayway.jsonpath.ReadContext var_CSVData = getJsonData("C:/GIAS Automation/3.6 BaseRegression/InputData/36Reg_BenificiaryChanges.csv");
                 LOGGER.info("Executed Step = VAR,Json,var_CSVData,C:/GIAS Automation/3.6 BaseRegression/InputData/36Reg_BenificiaryChanges.csv,TGTYPESCREENREG");
		String var_PolicyNumber;
                 LOGGER.info("Executed Step = VAR,String,var_PolicyNumber,TGTYPESCREENREG");
		var_PolicyNumber = getJsonData(var_CSVData , "$.records["+var_Count+"].PolicyNumber");
                 LOGGER.info("Executed Step = STORE,var_PolicyNumber,var_CSVData,$.records[{var_Count}].PolicyNumber,TGTYPESCREENREG");
        TYPE.$("ELE_ENTERPOLICYNUMBER","input[type=text][name='outerForm:policyNumber']TGWEBCOMMACssSelectorTGWEBCOMMA0",var_PolicyNumber,"FALSE","TGTYPESCREENREG");

        TAP.$("ELE_CLICKCONTINUEFORRELATIONSHIPS","//input[@type='submit'][@name='outerForm:Continue']","TGTYPESCREENREG");

        TAP.$("ELE_CLICKBENEFICIARYLINK","//span[text()='Beneficiary (Contract)']","TGTYPESCREENREG");

		writeToCSV("ELE_CLIENTPOLICYNUMBER " , ActionWrapper.getWebElementValueForVariable("span[id='outerForm:controlNumberOne']", "CssSelector", 0, "span[id='outerForm:controlNumberOne']TGWEBCOMMACssSelectorTGWEBCOMMA0"));
                 LOGGER.info("Executed Step = WRITETOCSV,ELE_CLIENTPOLICYNUMBER,TGTYPESCREENREG");
		writeToCSV("ELE_OLDCLIENTBENEFICIARYNAME " , ActionWrapper.getWebElementValueForVariable("span[id='outerForm:clientName']", "CssSelector", 0, "span[id='outerForm:clientName']TGWEBCOMMACssSelectorTGWEBCOMMA0"));
                 LOGGER.info("Executed Step = WRITETOCSV,ELE_OLDCLIENTBENEFICIARYNAME,TGTYPESCREENREG");
		writeToCSV("ELE_OLDBENEFIICIARYRELATIONTYPE " , ActionWrapper.getWebElementValueForVariable("span[id='outerForm:userDefnRelaType']", "CssSelector", 0, "span[id='outerForm:userDefnRelaType']TGWEBCOMMACssSelectorTGWEBCOMMA0"));
                 LOGGER.info("Executed Step = WRITETOCSV,ELE_OLDBENEFIICIARYRELATIONTYPE,TGTYPESCREENREG");
		writeToCSV("ELE_OLDBENEFICIARYPERCENTAGE " , ActionWrapper.getWebElementValueForVariable("span[id='outerForm:sharePercentageText']", "CssSelector", 0, "span[id='outerForm:sharePercentageText']TGWEBCOMMACssSelectorTGWEBCOMMA0"));
                 LOGGER.info("Executed Step = WRITETOCSV,ELE_OLDBENEFICIARYPERCENTAGE,TGTYPESCREENREG");
        TAP.$("ELE_CLICKUPDATEBUTTONFORBENEFICIARY","//input[@type='submit'][@name='outerForm:Update']","TGTYPESCREENREG");

        TAP.$("ELE_SELECTCLIENTLINK","//span[text()='Select Client']","TGTYPESCREENREG");

        TAP.$("ELE_CLICKADDBUTTONFORBENEFICIARY","//input[@type='submit'][@name='outerForm:Add']","TGTYPESCREENREG");

		String var_BeneficiaryFirstName;
                 LOGGER.info("Executed Step = VAR,String,var_BeneficiaryFirstName,TGTYPESCREENREG");
		var_BeneficiaryFirstName = getJsonData(var_CSVData , "$.records["+var_Count+"].BeneficiaryFirstName");
                 LOGGER.info("Executed Step = STORE,var_BeneficiaryFirstName,var_CSVData,$.records[{var_Count}].BeneficiaryFirstName,TGTYPESCREENREG");
		String var_BeneficiaryLastName;
                 LOGGER.info("Executed Step = VAR,String,var_BeneficiaryLastName,TGTYPESCREENREG");
		var_BeneficiaryLastName = getJsonData(var_CSVData , "$.records["+var_Count+"].BeneficiaryLastName");
                 LOGGER.info("Executed Step = STORE,var_BeneficiaryLastName,var_CSVData,$.records[{var_Count}].BeneficiaryLastName,TGTYPESCREENREG");
		String var_BeneficiaryBirthState;
                 LOGGER.info("Executed Step = VAR,String,var_BeneficiaryBirthState,TGTYPESCREENREG");
		var_BeneficiaryBirthState = getJsonData(var_CSVData , "$.records["+var_Count+"].BeneficiaryBirthState");
                 LOGGER.info("Executed Step = STORE,var_BeneficiaryBirthState,var_CSVData,$.records[{var_Count}].BeneficiaryBirthState,TGTYPESCREENREG");
		String var_BeneficiaryDOB;
                 LOGGER.info("Executed Step = VAR,String,var_BeneficiaryDOB,TGTYPESCREENREG");
		var_BeneficiaryDOB = getJsonData(var_CSVData , "$.records["+var_Count+"].BeneficiaryDOB");
                 LOGGER.info("Executed Step = STORE,var_BeneficiaryDOB,var_CSVData,$.records[{var_Count}].BeneficiaryDOB,TGTYPESCREENREG");
		String var_BeneficiaryGender;
                 LOGGER.info("Executed Step = VAR,String,var_BeneficiaryGender,TGTYPESCREENREG");
		var_BeneficiaryGender = getJsonData(var_CSVData , "$.records["+var_Count+"].BeneficiaryGender");
                 LOGGER.info("Executed Step = STORE,var_BeneficiaryGender,var_CSVData,$.records[{var_Count}].BeneficiaryGender,TGTYPESCREENREG");
		String var_BeneficiaryIDType;
                 LOGGER.info("Executed Step = VAR,String,var_BeneficiaryIDType,TGTYPESCREENREG");
		var_BeneficiaryIDType = getJsonData(var_CSVData , "$.records["+var_Count+"].BeneficiaryIDType");
                 LOGGER.info("Executed Step = STORE,var_BeneficiaryIDType,var_CSVData,$.records[{var_Count}].BeneficiaryIDType,TGTYPESCREENREG");
		String var_BeneficiaryIDNumber;
                 LOGGER.info("Executed Step = VAR,String,var_BeneficiaryIDNumber,TGTYPESCREENREG");
		var_BeneficiaryIDNumber = getJsonData(var_CSVData , "$.records["+var_Count+"].BeneficiaryIDNumber");
                 LOGGER.info("Executed Step = STORE,var_BeneficiaryIDNumber,var_CSVData,$.records[{var_Count}].BeneficiaryIDNumber,TGTYPESCREENREG");
        TYPE.$("ELE_ENTERBENEFICIARYFIRSTNAME","input[type=text][name='outerForm:nameFirst']TGWEBCOMMACssSelectorTGWEBCOMMA0",var_BeneficiaryFirstName,"FALSE","TGTYPESCREENREG");

        TYPE.$("ELE_ENTERBENEFICIARYLASTNAME","input[type=text][name='outerForm:nameLast']TGWEBCOMMACssSelectorTGWEBCOMMA0",var_BeneficiaryLastName,"FALSE","TGTYPESCREENREG");

        SELECT.$("ELE_SELECTBENEFICIARYSTATE","select[name='outerForm:birthCountryAndState2']TGWEBCOMMACssSelectorTGWEBCOMMA0",var_BeneficiaryBirthState,"TGTYPESCREENREG");

        TYPE.$("ELE_SELECTBENEFICIARYDOB","input[type=text][name='outerForm:dateOfBirth2']TGWEBCOMMACssSelectorTGWEBCOMMA0",var_BeneficiaryDOB,"FALSE","TGTYPESCREENREG");

        SELECT.$("ELE_SELECTBENEFICIARYGENDER","select[name='outerForm:sexCode2']TGWEBCOMMACssSelectorTGWEBCOMMA0",var_BeneficiaryGender,"TGTYPESCREENREG");

        SELECT.$("ELE_SELECTBENEFICIARYID","select[name='outerForm:taxIdenUsag2']TGWEBCOMMACssSelectorTGWEBCOMMA0",var_BeneficiaryIDType,"TGTYPESCREENREG");

        TYPE.$("ELE_BENEFICIARYIDNUMBER","input[type=text][name='outerForm:identificationNumber2']TGWEBCOMMACssSelectorTGWEBCOMMA0",var_BeneficiaryIDNumber,"FALSE","TGTYPESCREENREG");

        TAP.$("ELE_CLICKSUBMITBENEFICIARYADDRESS","//input[@type='submit'][@name='outerForm:Submit']","TGTYPESCREENREG");

		String var_BeneficiaryStreetAddress;
                 LOGGER.info("Executed Step = VAR,String,var_BeneficiaryStreetAddress,TGTYPESCREENREG");
		var_BeneficiaryStreetAddress = getJsonData(var_CSVData , "$.records["+var_Count+"].BeneficiaryStreetAddress");
                 LOGGER.info("Executed Step = STORE,var_BeneficiaryStreetAddress,var_CSVData,$.records[{var_Count}].BeneficiaryStreetAddress,TGTYPESCREENREG");
        TYPE.$("ELE_ENTERBENEFICIARYSTREETADDRESS","input[type=text][name='outerForm:addressLineOne']TGWEBCOMMACssSelectorTGWEBCOMMA0",var_BeneficiaryStreetAddress,"FALSE","TGTYPESCREENREG");

		String var_BeneficiaryCity;
                 LOGGER.info("Executed Step = VAR,String,var_BeneficiaryCity,TGTYPESCREENREG");
		var_BeneficiaryCity = getJsonData(var_CSVData , "$.records["+var_Count+"].BeneficiaryCity");
                 LOGGER.info("Executed Step = STORE,var_BeneficiaryCity,var_CSVData,$.records[{var_Count}].BeneficiaryCity,TGTYPESCREENREG");
        TYPE.$("ELE_ENTERBENEFICIARYCITY","input[type=text][name='outerForm:addressCity']TGWEBCOMMACssSelectorTGWEBCOMMA0",var_BeneficiaryCity,"FALSE","TGTYPESCREENREG");

        SELECT.$("ELE_SELECTBENEFICIARYSTATEANDCOUNTRY","select[name='outerForm:countryAndStateCode']TGWEBCOMMACssSelectorTGWEBCOMMA0",var_BeneficiaryBirthState,"TGTYPESCREENREG");

		String var_BeneficairyZip;
                 LOGGER.info("Executed Step = VAR,String,var_BeneficairyZip,TGTYPESCREENREG");
		var_BeneficairyZip = getJsonData(var_CSVData , "$.records["+var_Count+"].BeneficairyZip");
                 LOGGER.info("Executed Step = STORE,var_BeneficairyZip,var_CSVData,$.records[{var_Count}].BeneficairyZip,TGTYPESCREENREG");
        TYPE.$("ELE_SELECTBENEFICIARYZIP","input[type=text][name='outerForm:zipCode']TGWEBCOMMACssSelectorTGWEBCOMMA0",var_BeneficairyZip,"FALSE","TGTYPESCREENREG");

        TAP.$("ELE_CLICKSUBMITBENEFICIARYADDRESS","//input[@type='submit'][@name='outerForm:Submit']","TGTYPESCREENREG");

        TAP.$("ELE_CLICKCANCELAFTERBENEFICIARYADDRESS","//input[@type='submit'][@name='outerForm:Cancel']","TGTYPESCREENREG");

        SELECT.$("ELE_SELECTBENEFICIARYRELATIONSHIP","select[name='outerForm:grid:0:relationship']TGWEBCOMMACssSelectorTGWEBCOMMA0","Beneficiary (Contract)","TGTYPESCREENREG");

        TAP.$("ELE_CLICKSUBMITAFTERBENEFICIARYRELATIONSHIP","//input[@type='submit'][@name='outerForm:Submit']","TGTYPESCREENREG");

        TAP.$("ELE_CLICKSUBMITTOADDBENEFICIARY","//input[@type='submit'][@name='outerForm:Submit']","TGTYPESCREENREG");

        TAP.$("ELE_CLICKCANCELAFTERBENEFICIARYADDED","//input[@type='submit'][@name='outerForm:Cancel']","TGTYPESCREENREG");

        TAP.$("ELE_CLICKBENEFICIARYLINK","//span[text()='Beneficiary (Contract)']","TGTYPESCREENREG");

		writeToCSV("ELE_NEWBENEFICIARYNAME " , ActionWrapper.getWebElementValueForVariable("span[id='outerForm:clientName']", "CssSelector", 0, "span[id='outerForm:clientName']TGWEBCOMMACssSelectorTGWEBCOMMA0"));
                 LOGGER.info("Executed Step = WRITETOCSV,ELE_NEWBENEFICIARYNAME,TGTYPESCREENREG");
		writeToCSV("ELE_NEWBENEFIICIARYRELATIONTYPE " , ActionWrapper.getWebElementValueForVariable("span[id='outerForm:userDefnRelaType']", "CssSelector", 0, "span[id='outerForm:userDefnRelaType']TGWEBCOMMACssSelectorTGWEBCOMMA0"));
                 LOGGER.info("Executed Step = WRITETOCSV,ELE_NEWBENEFIICIARYRELATIONTYPE,TGTYPESCREENREG");
		writeToCSV("ELE_NEWBENEFICIARYPERCENTAGE " , ActionWrapper.getWebElementValueForVariable("span[id='outerForm:sharePercentageText']", "CssSelector", 0, "span[id='outerForm:sharePercentageText']TGWEBCOMMACssSelectorTGWEBCOMMA0"));
                 LOGGER.info("Executed Step = WRITETOCSV,ELE_NEWBENEFICIARYPERCENTAGE,TGTYPESCREENREG");
		var_Count = var_Count + ( 1 );
                 LOGGER.info("Executed Step = INCREASEVAR,var_Count,1,TGTYPESCREENREG");
    	}
          LOGGER.info("Executed Step = WEND,TGTYPESCREENREG");
    	resetAttemps();

    }


    @Test
    public void tc06addresschanges() {

        // No need to declare / print method name here, handled by TestNg listener
        // declare("tc06addresschanges");

        CALL.$("LoginToGIAS36","TGTYPESCREENREG");

        TYPE.$("ELE_USERNAME","//input[@name='outerForm:username']","GIAS36ILC","FALSE","TGTYPESCREENREG");

        TYPE.$("ELE_PASSWORD","//input[@id='outerForm:password']","Automation","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_LOGINBUTTON","outerForm:loginButtonTGWEBCOMMAIdTGWEBCOMMA0","TGTYPESCREENREG");

		com.jayway.jsonpath.ReadContext var_CSVData = getJsonData("C:/GIAS_Automation/3.6 BaseRegression/InputData/36Reg_AddressChanges.csv");
                 LOGGER.info("Executed Step = VAR,Json,var_CSVData,C:/GIAS_Automation/3.6 BaseRegression/InputData/36Reg_AddressChanges.csv,TGTYPESCREENREG");
		int var_Count;
                 LOGGER.info("Executed Step = VAR,Integer,var_Count,TGTYPESCREENREG");
		var_Count = 0;
                 LOGGER.info("Executed Step = STORE,var_Count,0,TGTYPESCREENREG");
        HOVER.$("ELE_CLICKCLIENT","//td[text()='Client']","TGTYPESCREENREG");

        TAP.$("ELE_CLICKNAMESADDRESSES","//td[text()='Names/Addresses']",1,0,"TGTYPESCREENREG");

		String var_ClientName;
                 LOGGER.info("Executed Step = VAR,String,var_ClientName,TGTYPESCREENREG");
		var_ClientName = getJsonData(var_CSVData , "$.records["+var_Count+"].ClientLastNameFirstName");
                 LOGGER.info("Executed Step = STORE,var_ClientName,var_CSVData,$.records[{var_Count}].ClientLastNameFirstName,TGTYPESCREENREG");
		String var_NewStreetName;
                 LOGGER.info("Executed Step = VAR,String,var_NewStreetName,TGTYPESCREENREG");
		var_NewStreetName = getJsonData(var_CSVData , "$.records["+var_Count+"].NewStreetName");
                 LOGGER.info("Executed Step = STORE,var_NewStreetName,var_CSVData,$.records[{var_Count}].NewStreetName,TGTYPESCREENREG");
		String var_NewCity;
                 LOGGER.info("Executed Step = VAR,String,var_NewCity,TGTYPESCREENREG");
		var_NewCity = getJsonData(var_CSVData , "$.records["+var_Count+"].NewCity");
                 LOGGER.info("Executed Step = STORE,var_NewCity,var_CSVData,$.records[{var_Count}].NewCity,TGTYPESCREENREG");
		String var_NewStateCountry;
                 LOGGER.info("Executed Step = VAR,String,var_NewStateCountry,TGTYPESCREENREG");
		var_NewStateCountry = getJsonData(var_CSVData , "$.records["+var_Count+"].NewStateandCountry");
                 LOGGER.info("Executed Step = STORE,var_NewStateCountry,var_CSVData,$.records[{var_Count}].NewStateandCountry,TGTYPESCREENREG");
		String var_NewZip;
                 LOGGER.info("Executed Step = VAR,String,var_NewZip,TGTYPESCREENREG");
		var_NewZip = getJsonData(var_CSVData , "$.records["+var_Count+"].NewZip");
                 LOGGER.info("Executed Step = STORE,var_NewZip,var_CSVData,$.records[{var_Count}].NewZip,TGTYPESCREENREG");
        LOGGER.info("Executed Step = REPEAT IF");
    	while (check(var_Count,"<",1,"TGTYPESCREENREG")) {
          LOGGER.info("Executed Step = WHILE,var_Count,<,1,TGTYPESCREENREG");
        TYPE.$("ELE_CLIENTNAMESEARCHTEXTFIELD","//input[@type='text'][@name='outerForm:grid:individualName']",var_ClientName,"FALSE","TGTYPESCREENREG");

        TAP.$("ELE_FINDPOSITIONTOCLIENTNAMEBUTTON","//img[@id='outerForm:gnFindImage']",1,0,"TGTYPESCREENREG");

        TAP.$("ELE_CLIENTLASTNAMEFIRSTNAME","span[id='outerForm:individualName']TGWEBCOMMACssSelectorTGWEBCOMMA0",1,0,"TGTYPESCREENREG");

        TAP.$("ELE_CLICKONADDRESSINFORMATION1","//span[text()='Address Information']",1,0,"TGTYPESCREENREG");

        TAP.$("ELE_SELECTDISPLAYEDADDRESS","span[id='outerForm:grid:0:addressOut1']TGWEBCOMMACssSelectorTGWEBCOMMA0",1,0,"TGTYPESCREENREG");

        TAP.$("ELE_CLICKUPDATETOCHANGEADDRESS","//input[@type='submit'][@name='outerForm:Update']",1,0,"TGTYPESCREENREG");

        TYPE.$("ELE_ADDRESSLINE1TEXTFIELD","//input[@type='text'][@name='outerForm:addressLineOne']",var_NewStreetName,"FALSE","TGTYPESCREENREG");

        TYPE.$("ELE_CITYTEXTFIELD","//input[@type='text'][@name='outerForm:addressCity']",var_NewCity,"FALSE","TGTYPESCREENREG");

        SELECT.$("ELE_CLIENTNEWSTATE","span[id='outerForm:countryAndStateCode']TGWEBCOMMACssSelectorTGWEBCOMMA0","South Carolina US","TGTYPESCREENREG");

        TYPE.$("ELE_ZIPCODETEXTFIELD","//input[@type='text'][@name='outerForm:zipCode']",var_NewZip,"FALSE","TGTYPESCREENREG");

        TAP.$("ELE_SUBMITBUTTON","//input[@id='outerForm:Submit']",1,0,"TGTYPESCREENREG");

        ASSERT.$("ELE_SUCCESSMESSAGE","//li[@class='MessageInfo']","CONTAINS","Item successfully updated","TGTYPESCREENREG");

		var_Count = var_Count + ( 1 );
                 LOGGER.info("Executed Step = INCREASEVAR,var_Count,1,TGTYPESCREENREG");
    	}
          LOGGER.info("Executed Step = WEND,TGTYPESCREENREG");
    	resetAttemps();

    }


    @Test
    public void tc01createnewbusinesspolicy() {

        // No need to declare / print method name here, handled by TestNg listener
        // declare("tc01createnewbusinesspolicy");

        CALL.$("LoginToGIAS36","TGTYPESCREENREG");

        TYPE.$("ELE_USERNAME","//input[@name='outerForm:username']","GIAS36ILC","FALSE","TGTYPESCREENREG");

        TYPE.$("ELE_PASSWORD","//input[@id='outerForm:password']","Automation","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_LOGINBUTTON","outerForm:loginButtonTGWEBCOMMAIdTGWEBCOMMA0","TGTYPESCREENREG");

		int var_Count;
                 LOGGER.info("Executed Step = VAR,Integer,var_Count,TGTYPESCREENREG");
		var_Count = 0;
                 LOGGER.info("Executed Step = STORE,var_Count,0,TGTYPESCREENREG");
        LOGGER.info("Executed Step = REPEAT IF");
    	while (check(var_Count,"<",1,"TGTYPESCREENREG")) {
          LOGGER.info("Executed Step = WHILE,var_Count,<,1,TGTYPESCREENREG");
        CALL.$("AddDistributionChannel","TGTYPESCREENREG");

		com.jayway.jsonpath.ReadContext var_CSVData = getJsonData("C:/GIAS_Automation/3.6 BaseRegression/InputData/36Reg_CreateAPolicy.csv");
                 LOGGER.info("Executed Step = VAR,Json,var_CSVData,C:/GIAS_Automation/3.6 BaseRegression/InputData/36Reg_CreateAPolicy.csv,TGTYPESCREENREG");
		String var_DistributionChannelName;
                 LOGGER.info("Executed Step = VAR,String,var_DistributionChannelName,TGTYPESCREENREG");
		var_DistributionChannelName = getJsonData(var_CSVData , "$.records["+var_Count+"].DistributionChannelName");
                 LOGGER.info("Executed Step = STORE,var_DistributionChannelName,var_CSVData,$.records[{var_Count}].DistributionChannelName,TGTYPESCREENREG");
		String var_DistributionChannelDescription;
                 LOGGER.info("Executed Step = VAR,String,var_DistributionChannelDescription,TGTYPESCREENREG");
		var_DistributionChannelDescription = getJsonData(var_CSVData , "$.records["+var_Count+"].DistributionChannelDescription");
                 LOGGER.info("Executed Step = STORE,var_DistributionChannelDescription,var_CSVData,$.records[{var_Count}].DistributionChannelDescription,TGTYPESCREENREG");
        HOVER.$("ELE_AGENCYTAB","//td[text()='Agency']","TGTYPESCREENREG");

        TAP.$("ELE_DISTRIBUTIONCHANNELLINK","//td[text()='Distribution Channel']","TGTYPESCREENREG");

        TAP.$("ELE_ADDBUTTONDISTRIBUTIONCHANNEL","input[type=submit][name='outerForm:Add']TGWEBCOMMACssSelectorTGWEBCOMMA0","TGTYPESCREENREG");

        TYPE.$("ELE_DISTRIBUTIONCHANNELTEXTFIELD","//input[@type='text'][@name='outerForm:distributionChanName']",var_DistributionChannelName,"FALSE","TGTYPESCREENREG");

        TYPE.$("ELE_DESCRIPTIONTEXTFIELDFORDISTRICHANNAL","input[type=text][name='outerForm:channelDescription']TGWEBCOMMACssSelectorTGWEBCOMMA0",var_DistributionChannelDescription,"FALSE","TGTYPESCREENREG");

        TYPE.$("ELE_DISTRIBUTIONCHANNELCODETEXTFIELD","input[type=text][name='outerForm:duf_dist_code']TGWEBCOMMACssSelectorTGWEBCOMMA0",var_DistributionChannelName,"FALSE","TGTYPESCREENREG");

        TAP.$("ELE_SUBMITBUTTONDISTCHANNEL","input[type=submit][name='outerForm:Submit']TGWEBCOMMACssSelectorTGWEBCOMMA0","TGTYPESCREENREG");

        CALL.$("AddAgentRankCode","TGTYPESCREENREG");

        HOVER.$("ELE_AGENCYTAB","//td[text()='Agency']","TGTYPESCREENREG");

		String var_DistributionChannelDescription2;
                 LOGGER.info("Executed Step = VAR,String,var_DistributionChannelDescription2,TGTYPESCREENREG");
		var_DistributionChannelDescription2 = getJsonData(var_CSVData , "$.records["+var_Count+"].DistributionChannelDescription");
                 LOGGER.info("Executed Step = STORE,var_DistributionChannelDescription2,var_CSVData,$.records[{var_Count}].DistributionChannelDescription,TGTYPESCREENREG");
		String var_AgencyRankDescription;
                 LOGGER.info("Executed Step = VAR,String,var_AgencyRankDescription,TGTYPESCREENREG");
		var_AgencyRankDescription = getJsonData(var_CSVData , "$.records["+var_Count+"].AgencyRankDescription");
                 LOGGER.info("Executed Step = STORE,var_AgencyRankDescription,var_CSVData,$.records[{var_Count}].AgencyRankDescription,TGTYPESCREENREG");
		String var_AgencyRankCode;
                 LOGGER.info("Executed Step = VAR,String,var_AgencyRankCode,TGTYPESCREENREG");
		var_AgencyRankCode = getJsonData(var_CSVData , "$.records["+var_Count+"].AgencyRankCode");
                 LOGGER.info("Executed Step = STORE,var_AgencyRankCode,var_CSVData,$.records[{var_Count}].AgencyRankCode,TGTYPESCREENREG");
        TAP.$("ELE_AGENRANKCODESLINK","//td[text()='Agent Rank Codes']","TGTYPESCREENREG");

        TAP.$("ELE_ADDBUTTONAGENTCODES","//input[@type='submit'][@name='outerForm:Add']","TGTYPESCREENREG");

        SELECT.$("ELE_DROPDOWNDISTRIBUTIONCHANNELONAGENTCODES","//select[@name='outerForm:distributionChannel']",var_DistributionChannelDescription2,"TGTYPESCREENREG");

        TYPE.$("ELE_AGENCYRANKLAVELTEXTFIELD","//input[@type='text'][@name='outerForm:agencyRankLevel']","1","FALSE","TGTYPESCREENREG");

        TYPE.$("ELE_AGENCYRANKCODETEXTFIELD","//input[@type='text'][@name='outerForm:agencyRankCode']",var_AgencyRankCode,"FALSE","TGTYPESCREENREG");

        TYPE.$("ELE_AGENCYRANKDESCRIPTION","//input[@type='text'][@name='outerForm:description']",var_AgencyRankDescription,"FALSE","TGTYPESCREENREG");

        TAP.$("ELE_SUBMITBUTTONAGENTRANKCODESSCREEN","//input[@type='submit'][@name='outerForm:Submit']","TGTYPESCREENREG");

        CALL.$("AddCommissionScheduleRates","TGTYPESCREENREG");

        HOVER.$("ELE_AGENCYTAB","//td[text()='Agency']","TGTYPESCREENREG");

		String var_TableName;
                 LOGGER.info("Executed Step = VAR,String,var_TableName,TGTYPESCREENREG");
		var_TableName = getJsonData(var_CSVData , "$.records["+var_Count+"].TableName");
                 LOGGER.info("Executed Step = STORE,var_TableName,var_CSVData,$.records[{var_Count}].TableName,TGTYPESCREENREG");
		String var_TableDescription;
                 LOGGER.info("Executed Step = VAR,String,var_TableDescription,TGTYPESCREENREG");
		var_TableDescription = getJsonData(var_CSVData , "$.records["+var_Count+"].TableDescription");
                 LOGGER.info("Executed Step = STORE,var_TableDescription,var_CSVData,$.records[{var_Count}].TableDescription,TGTYPESCREENREG");
		String var_DistributionChannelDescription3;
                 LOGGER.info("Executed Step = VAR,String,var_DistributionChannelDescription3,TGTYPESCREENREG");
		var_DistributionChannelDescription3 = getJsonData(var_CSVData , "$.records["+var_Count+"].DistributionChannelDescription");
                 LOGGER.info("Executed Step = STORE,var_DistributionChannelDescription3,var_CSVData,$.records[{var_Count}].DistributionChannelDescription,TGTYPESCREENREG");
		String var_AgencyRankDescription3;
                 LOGGER.info("Executed Step = VAR,String,var_AgencyRankDescription3,TGTYPESCREENREG");
		var_AgencyRankDescription3 = getJsonData(var_CSVData , "$.records["+var_Count+"].AgencyRankDescription");
                 LOGGER.info("Executed Step = STORE,var_AgencyRankDescription3,var_CSVData,$.records[{var_Count}].AgencyRankDescription,TGTYPESCREENREG");
        TAP.$("ELE_COMMISSIONSCHEDULERATESLINK","//td[text()='Commission Schedule Rates']","TGTYPESCREENREG");

        TAP.$("ELE_ADDBUTTONCOMMISSIONSCHEDULE","//input[@type='submit'][@name='outerForm:Add']","TGTYPESCREENREG");

        TYPE.$("ELE_TABLENAMETEXTFIELD","//input[@type='text'][@name='outerForm:tableName']",var_TableName,"FALSE","TGTYPESCREENREG");

        TYPE.$("ELE_TABLEDESCRIPTIONTEXTFIELD","//input[@type='text'][@name='outerForm:description']",var_TableDescription,"FALSE","TGTYPESCREENREG");

        SELECT.$("ELE_DROPDOWNDISTRIBUTIONCHANNELCOMMISSIONSCHEDULE","//select[@name='outerForm:distributionChannel']",var_DistributionChannelDescription3,"TGTYPESCREENREG");

        TAP.$("ELE_CONTINUEBUTTONCOMMISSIONSCHEDULE","//input[@type='submit'][@name='outerForm:Continue']","TGTYPESCREENREG");

        SELECT.$("ELE_DROPPDOWNAGENCYRANKCODE","//select[@name='outerForm:grid:0:agencyRankCode']",var_AgencyRankDescription,"TGTYPESCREENREG");

        TYPE.$("ELE_ENDDURATION0TEXTFIELD","//input[@type='text'][@name='outerForm:grid:endingDuration1']","1","FALSE","TGTYPESCREENREG");

        TYPE.$("ELE_ENDDURATION1TEXTFIELD","//input[@type='text'][@name='outerForm:grid:endingDuration2']","5","FALSE","TGTYPESCREENREG");

        TYPE.$("ELE_ENDDURATION2TEXTFIELD","//input[@type='text'][@name='outerForm:grid:endingDuration3']","120","FALSE","TGTYPESCREENREG");

        TYPE.$("ELE_COMMISSIONRATE0TEXTFIELD","//input[@type='text'][@name='outerForm:grid:0:commissionRate1']","20","FALSE","TGTYPESCREENREG");

        TYPE.$("ELE_COMMISSIONRATE1TEXTFIELD","//input[@type='text'][@name='outerForm:grid:0:commissionRate2']","15","FALSE","TGTYPESCREENREG");

        TYPE.$("ELE_COMMISSIONRATE2TEXTFIELD","//input[@type='text'][@name='outerForm:grid:0:commissionRate3']","10","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_SUBMITBUTTONCOMMISSIONSCHEDULE","//input[@type='submit'][@name='outerForm:Submit']","TGTYPESCREENREG");

        CALL.$("AddCommissionContract","TGTYPESCREENREG");

        HOVER.$("ELE_AGENCYTAB","//td[text()='Agency']","TGTYPESCREENREG");

		String var_CommissionContractNumber;
                 LOGGER.info("Executed Step = VAR,String,var_CommissionContractNumber,TGTYPESCREENREG");
		var_CommissionContractNumber = getJsonData(var_CSVData , "$.records["+var_Count+"].CommissionContractNumber");
                 LOGGER.info("Executed Step = STORE,var_CommissionContractNumber,var_CSVData,$.records[{var_Count}].CommissionContractNumber,TGTYPESCREENREG");
		String var_CommissionContractDescription;
                 LOGGER.info("Executed Step = VAR,String,var_CommissionContractDescription,TGTYPESCREENREG");
		var_CommissionContractDescription = getJsonData(var_CSVData , "$.records["+var_Count+"].CommissionContractDescription");
                 LOGGER.info("Executed Step = STORE,var_CommissionContractDescription,var_CSVData,$.records[{var_Count}].CommissionContractDescription,TGTYPESCREENREG");
		String var_DistributionChannelDescription4;
                 LOGGER.info("Executed Step = VAR,String,var_DistributionChannelDescription4,TGTYPESCREENREG");
		var_DistributionChannelDescription4 = getJsonData(var_CSVData , "$.records["+var_Count+"].DistributionChannelDescription");
                 LOGGER.info("Executed Step = STORE,var_DistributionChannelDescription4,var_CSVData,$.records[{var_Count}].DistributionChannelDescription,TGTYPESCREENREG");
		String var_AgencyRankDescription4;
                 LOGGER.info("Executed Step = VAR,String,var_AgencyRankDescription4,TGTYPESCREENREG");
		var_AgencyRankDescription4 = getJsonData(var_CSVData , "$.records["+var_Count+"].AgencyRankDescription");
                 LOGGER.info("Executed Step = STORE,var_AgencyRankDescription4,var_CSVData,$.records[{var_Count}].AgencyRankDescription,TGTYPESCREENREG");
        TAP.$("ELE_COMMISSIONCONTRACTSLINK","//td[text()='Commission Contracts']","TGTYPESCREENREG");

        TAP.$("ELE_ADDBUTTONCOMMISSIONCONTRACT","//input[@type='submit'][@name='outerForm:Add']","TGTYPESCREENREG");

        TYPE.$("ELE_COMMSSIONCONTRACTNOTEXTFIELD","//input[@type='text'][@name='outerForm:commissionContNumb']",var_CommissionContractNumber,"FALSE","TGTYPESCREENREG");

        TYPE.$("ELE_COMMISSIONCONTRACTDESCTEXTFIELD","//input[@type='text'][@name='outerForm:longDescription']",var_CommissionContractDescription,"FALSE","TGTYPESCREENREG");

        SELECT.$("ELE_DISTRIBUTIONCHANNELLISTCOMMISSIONCONTRACT","//select[@name='outerForm:distributionChannel']",var_DistributionChannelDescription4,"TGTYPESCREENREG");

		try { 
		 java.time.LocalDate date = java.time.LocalDate.now();
		java.time.format.DateTimeFormatter formatter = java.time.format.DateTimeFormatter.ofPattern("MM/dd/yyyy");
		String formattedDate = date.format(formatter);
		driver.findElement(By.xpath("//input[@type='text'][@name='outerForm:effectiveDate']")).sendKeys(formattedDate);
		 
		 ;} 
		 catch(Exception e) { PrintVal("Renderer Script Exception : "+e.toString()); } 
		 PrintVal("Executed Step = CALLRENDERERSCRIPT,CURRENTDATEASEFFECTIVEDATE"); 
        SELECT.$("ELE_PAYBACKMETHODLIST","//select[@name='outerForm:advancePaybackMethod']","100% of Commission Earned","TGTYPESCREENREG");

        SELECT.$("ELE_ADVANCETHROUGHAGENTRANKLIST","//select[@name='outerForm:topLevelForAdvance']",var_AgencyRankDescription4,"TGTYPESCREENREG");

        TYPE.$("ELE_DAYSPASTDUETEXTFIELD","//input[@type='text'][@name='outerForm:daysLateBeforeChar']","3","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_SUBMITBUTTONCOMMISSIONCONTRACT","//input[@type='submit'][@name='outerForm:Submit']","TGTYPESCREENREG");

        CALL.$("AddCommissionSchedule","TGTYPESCREENREG");

        HOVER.$("ELE_AGENCYTAB","//td[text()='Agency']","TGTYPESCREENREG");

		String var_CommissionContractNumber5;
                 LOGGER.info("Executed Step = VAR,String,var_CommissionContractNumber5,TGTYPESCREENREG");
		var_CommissionContractNumber5 = getJsonData(var_CSVData , "$.records["+var_Count+"].CommissionContractNumber");
                 LOGGER.info("Executed Step = STORE,var_CommissionContractNumber5,var_CSVData,$.records[{var_Count}].CommissionContractNumber,TGTYPESCREENREG");
		String var_TableDescription5;
                 LOGGER.info("Executed Step = VAR,String,var_TableDescription5,TGTYPESCREENREG");
		var_TableDescription5 = getJsonData(var_CSVData , "$.records["+var_Count+"].TableDescription");
                 LOGGER.info("Executed Step = STORE,var_TableDescription5,var_CSVData,$.records[{var_Count}].TableDescription,TGTYPESCREENREG");
		String var_LineDescription;
                 LOGGER.info("Executed Step = VAR,String,var_LineDescription,TGTYPESCREENREG");
		var_LineDescription = getJsonData(var_CSVData , "$.records["+var_Count+"].LineDescription");
                 LOGGER.info("Executed Step = STORE,var_LineDescription,var_CSVData,$.records[{var_Count}].LineDescription,TGTYPESCREENREG");
        TAP.$("ELE_COMMISSIONCONTRACTSLINK","//td[text()='Commission Contracts']","TGTYPESCREENREG");

        TYPE.$("ELE_COMMISSIONCONTRACTNOSEARCHTEXTFIELD","//input[@type='text'][@name='outerForm:grid:commissionContNumb']",var_CommissionContractNumber5,"FALSE","TGTYPESCREENREG");

        TAP.$("ELE_FINDPOSITIONTOBUTTON","//img[@id='outerForm:gnFindImage']","TGTYPESCREENREG");

        TAP.$("ELE_COMMISSIONCONTRACT2224","span[id='outerForm:grid:0:commissionContNumbOut1']TGWEBCOMMACssSelectorTGWEBCOMMA0","TGTYPESCREENREG");

        TAP.$("ELE_COMMISSIONSCHEDULELINK","//span[text()='Commission Schedule']","TGTYPESCREENREG");

        TAP.$("ELE_ADDBUTTONCOMMISSIONSCHEDULE","//input[@type='submit'][@name='outerForm:Add']","TGTYPESCREENREG");

        SELECT.$("ELE_CONTRACTTYPELIST","//select[@name='outerForm:commissionContType']","Health","TGTYPESCREENREG");

		try { 
		 java.time.LocalDate date = java.time.LocalDate.now();
		java.time.format.DateTimeFormatter formatter = java.time.format.DateTimeFormatter.ofPattern("MM/dd/yyyy");
		String formattedDate = date.format(formatter);
		driver.findElement(By.xpath("//input[@type='text'][@name='outerForm:effectiveDate']")).sendKeys(formattedDate);
		 
		 ;} 
		 catch(Exception e) { PrintVal("Renderer Script Exception : "+e.toString()); } 
		 PrintVal("Executed Step = CALLRENDERERSCRIPT,CURRENTDATEASEFFECTIVEDATE"); 
        SELECT.$("ELE_COMMISSIONRATETABLELIST","//select[@name='outerForm:commissionRateTableName']",var_TableDescription5,"TGTYPESCREENREG");

        TYPE.$("ELE_LINEDESCRIPTIONTEXTFIELD","//input[@type='text'][@name='outerForm:shortDescription']",var_LineDescription,"FALSE","TGTYPESCREENREG");

        TAP.$("ELE_SUBMITTBUTTONADDCOMMSCHE","//input[@type='submit'][@name='outerForm:Submit']","TGTYPESCREENREG");

        CALL.$("AddNewBusinessPolicy","TGTYPESCREENREG");

        HOVER.$("ELE_NEWBUSINESSTAB","//td[text()='New Business']","TGTYPESCREENREG");

        TAP.$("ELE_APPLICATIONENTRYUPDATELINK","//td[text()='Application Entry/Update']","TGTYPESCREENREG");

        TAP.$("ELE_ADDBUTTONAPPLICATIONENTRY","//input[@type='submit'][@name='outerForm:Add']","TGTYPESCREENREG");

		String var_PolicyNumber;
                 LOGGER.info("Executed Step = VAR,String,var_PolicyNumber,TGTYPESCREENREG");
		var_PolicyNumber = getJsonData(var_CSVData , "$.records["+var_Count+"].PolicyNumber");
                 LOGGER.info("Executed Step = STORE,var_PolicyNumber,var_CSVData,$.records[{var_Count}].PolicyNumber,TGTYPESCREENREG");
		String var_FirstName;
                 LOGGER.info("Executed Step = VAR,String,var_FirstName,TGTYPESCREENREG");
		var_FirstName = getJsonData(var_CSVData , "$.records["+var_Count+"].FirstName");
                 LOGGER.info("Executed Step = STORE,var_FirstName,var_CSVData,$.records[{var_Count}].FirstName,TGTYPESCREENREG");
		String var_LastName;
                 LOGGER.info("Executed Step = VAR,String,var_LastName,TGTYPESCREENREG");
		var_LastName = getJsonData(var_CSVData , "$.records["+var_Count+"].LastName");
                 LOGGER.info("Executed Step = STORE,var_LastName,var_CSVData,$.records[{var_Count}].LastName,TGTYPESCREENREG");
		String var_IDNumber;
                 LOGGER.info("Executed Step = VAR,String,var_IDNumber,TGTYPESCREENREG");
		var_IDNumber = getJsonData(var_CSVData , "$.records["+var_Count+"].IDNumber");
                 LOGGER.info("Executed Step = STORE,var_IDNumber,var_CSVData,$.records[{var_Count}].IDNumber,TGTYPESCREENREG");
		String var_ClientSearchName;
                 LOGGER.info("Executed Step = VAR,String,var_ClientSearchName,TGTYPESCREENREG");
		var_ClientSearchName = getJsonData(var_CSVData , "$.records["+var_Count+"].ClientSearchName");
                 LOGGER.info("Executed Step = STORE,var_ClientSearchName,var_CSVData,$.records[{var_Count}].ClientSearchName,TGTYPESCREENREG");
		String var_AgentNumber;
                 LOGGER.info("Executed Step = VAR,String,var_AgentNumber,TGTYPESCREENREG");
		var_AgentNumber = getJsonData(var_CSVData , "$.records["+var_Count+"].AgentNumber");
                 LOGGER.info("Executed Step = STORE,var_AgentNumber,var_CSVData,$.records[{var_Count}].AgentNumber,TGTYPESCREENREG");
        TYPE.$("ELE_POLICYNUMBERTEXTFIELD","//input[@type='text'][@name='outerForm:policyNumber']",var_PolicyNumber,"FALSE","TGTYPESCREENREG");

        SELECT.$("ELE_CURRENCYCODELIST","//select[@name='outerForm:currencyCode']","Dollars [US]","TGTYPESCREENREG");

        SELECT.$("ELE_COUNTRYANDSTATECODELIST","//select[@name='outerForm:countryAndStateCode']","Missouri US","TGTYPESCREENREG");

        TYPE.$("ELE_CASHWITHAPPLICATIONTEXTFIELD","//input[@type='text'][@name='outerForm:cashWithApplication']","25000","FALSE","TGTYPESCREENREG");

        SELECT.$("ELE_PAYMENTFREQUENCYLIST","//select[@name='outerForm:paymentMode']","Annual","TGTYPESCREENREG");

        SELECT.$("ELE_PAYMENTMETHODLIST","//select[@name='outerForm:paymentCode']","Coupon Book","TGTYPESCREENREG");

        SELECT.$("ELE_TAXQUALIFIEDDESCRIPTIONLIST","//select[@name='outerForm:taxQualifiedCode']","NON QUALIFIED","TGTYPESCREENREG");

        SELECT.$("ELE_TAXWITHHOLDINGLIST","//select[@name='outerForm:taxWithholdingCode']","No Withholding","TGTYPESCREENREG");

        TAP.$("ELE_SELECTCLIENTLINK","//span[text()='Select Client']","TGTYPESCREENREG");

        TAP.$("ELE_ADDBUTTONCLIENTRELATIONSHOPPAGE","//input[@type='submit'][@name='outerForm:Add']","TGTYPESCREENREG");

        TYPE.$("ELE_FIRSTNAMETEXTFIELD","//input[@type='text'][@name='outerForm:nameFirst']",var_FirstName,"FALSE","TGTYPESCREENREG");

        TYPE.$("ELE_LASTNAMETEXTFIELD","//input[@type='text'][@name='outerForm:nameLast']",var_LastName,"FALSE","TGTYPESCREENREG");

        TYPE.$("ELE_CLIENT_DOB","//input[@type='text'][@name='outerForm:dateOfBirth2']","01/01/1980","FALSE","TGTYPESCREENREG");

        SELECT.$("ELE_CLIENT_GENDER_LIST","//select[@name='outerForm:sexCode2']","Male","TGTYPESCREENREG");

        SELECT.$("ELE_IDTYPELIST","//select[@name='outerForm:taxIdenUsag2']","Social Security Number","TGTYPESCREENREG");

        TYPE.$("ELE_IDNUMBERTEXTFIELD","//input[@type='text'][@name='outerForm:identificationNumber2']",var_IDNumber,"FALSE","TGTYPESCREENREG");

        TAP.$("ELE_SUBMITBUTTONADDCLIENTPAGE","//input[@type='submit'][@name='outerForm:Submit']","TGTYPESCREENREG");

        TYPE.$("ELE_ADDRESSLINE1TEXTFIELD","//input[@type='text'][@name='outerForm:addressLineOne']","123 Main St","FALSE","TGTYPESCREENREG");

        TYPE.$("ELE_CITYTEXTFIELD","//input[@type='text'][@name='outerForm:addressCity']","Kansas City","FALSE","TGTYPESCREENREG");

        SELECT.$("ELE_COUNTRYANDSTATECODELIST","//select[@name='outerForm:countryAndStateCode']","Missouri US","TGTYPESCREENREG");

        TYPE.$("ELE_ZIPCODETEXTFIELD","//input[@type='text'][@name='outerForm:zipCode']","64153","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_SUBMITBUTTONADDADDRESSPAGE","//input[@type='submit'][@name='outerForm:Submit']","TGTYPESCREENREG");

        TAP.$("ELE_SELECTCLIENTRELATIONSHIPSLINK","//a[text()='Select Client Relationships']","TGTYPESCREENREG");

        TYPE.$("ELE_CLIENTNAMESEARCHTEXTFIELD","//input[@type='text'][@name='outerForm:grid:individualName']",var_ClientSearchName,"FALSE","TGTYPESCREENREG");

        TAP.$("ELE_FINDPOSITIONTOCLIENTNAMEBUTTON","//img[@id='outerForm:gnFindImage']","TGTYPESCREENREG");

        SELECT.$("ELE_RELATIONSHIPGRID0LIST","//select[@name='outerForm:grid:0:relationship']","Owner 1","TGTYPESCREENREG");

        TAP.$("ELE_SUBMITBUTTONSELECTCLIENTRELATIONPAGE","//input[@type='submit'][@name='outerForm:Submit']","TGTYPESCREENREG");

        TYPE.$("ELE_AGENTNUMBERTEXTFIELD","//input[@type='text'][@name='outerForm:grid:0:agentNumberOut2']",var_AgentNumber,"FALSE","TGTYPESCREENREG");

        TAP.$("ELE_SUBMITBUTTONADDNEWBUSINESS","//*[@name='outerForm:Submit']","TGTYPESCREENREG");

        CALL.$("SettlePolicy","TGTYPESCREENREG");

        TAP.$("ELE_BENEFITSLINK","//span[text()='Benefits']","TGTYPESCREENREG");

		String var_BenefitPlan = "null";
                 LOGGER.info("Executed Step = VAR,String,var_BenefitPlan,null,TGTYPESCREENREG");
		var_BenefitPlan = getJsonData(var_CSVData , "$.records["+var_Count+"].BenefitPlan");
                 LOGGER.info("Executed Step = STORE,var_BenefitPlan,var_CSVData,$.records[{var_Count}].BenefitPlan,TGTYPESCREENREG");
        SELECT.$("ELE_DROPDOWNTOADDABENEFIT","//select[@name='outerForm:initialPlanCode']",var_BenefitPlan,"TGTYPESCREENREG");

        TAP.$("ELE_CONTINUEAFTERSELECTINGBENEFIT","//input[@type='submit'][@name='outerForm:Continue']","TGTYPESCREENREG");

        TYPE.$("ELE_TEXTTOENTERUNITSAFTERADDINGBENEFIT","//input[@type='text'][@name='outerForm:unitsOfInsurance']	","25.00","FALSE","TGTYPESCREENREG");

        LOGGER.info("Executed Step = START IF");
        if (check(var_BenefitPlan,"CONTAINS","ULAT1","TGTYPESCREENREG")) {
         LOGGER.info("Executed Step = IF,var_BenefitPlan,CONTAINS,ULAT1,TGTYPESCREENREG");
        TAP.$("ELE_RDOBTN_FACEAMOUNT","//input[@id='outerForm:deathBenefitOption:0']","TGTYPESCREENREG");

        }
          LOGGER.info("Executed Step = ENDIF,TGTYPESCREENREG");
        SWIPE.$("UP","TGTYPESCREENREG");

		try { 
		  List<WebElement> checkBoxes = driver.findElements(By.xpath("//input[@type='checkbox']"));
				int numberOfBoxes =  checkBoxes.size();
				if( numberOfBoxes > 0) {
						for(int i=0;i<numberOfBoxes;i++)
						{
					 driver.findElement(By.xpath("//input[@id='outerForm:gridSupp:" + i + ":selectedCheck']")).click();

		 if(driver.findElements(By.xpath("//input[@id='outerForm:gridSupp:"+ i + ":unitsOut2']")).size()!= 0)
			 {	 
				driver.findElement(By.xpath("//input[@id='outerForm:gridSupp:" + i + ":unitsOut2']")).clear();
				 driver.findElement(By.xpath("//input[@id='outerForm:gridSupp:" + i + ":unitsOut2']")).sendKeys("25.00");
			}
			else { System.out.println("Not found"); }	
			}
		}
		 
		 ;} 
		 catch(Exception e) { PrintVal("Renderer Script Exception : "+e.toString()); } 
		 PrintVal("Executed Step = CALLRENDERERSCRIPT,COUNTSUPPLEMENTCHECKBOX"); 
        TAP.$("ELE_SUBMITBENEFITWITHUNITS","//input[@type='submit'][@name='outerForm:Submit']","TGTYPESCREENREG");

        TAP.$("ELE_CANCELAFTERADDINGBENEFITS","//input[@type='submit'][@name='outerForm:Cancel']","TGTYPESCREENREG");

        TAP.$("ELE_CLICKISSUEDECLINEWITHDRAWLINK","//span[text()='Issue/Decline/Withdraw']","TGTYPESCREENREG");

		String var_effectiveDate = "null";
                 LOGGER.info("Executed Step = VAR,String,var_effectiveDate,null,TGTYPESCREENREG");
		try { 
		 java.time.LocalDate date = java.time.LocalDate.now();
		java.time.format.DateTimeFormatter formatter = java.time.format.DateTimeFormatter.ofPattern("MM/dd/yyyy");
		String formattedDate = date.format(formatter);
		var_effectiveDate = formattedDate;
		 
		 ;} 
		 catch(Exception e) { PrintVal("Renderer Script Exception : "+e.toString()); } 
		 PrintVal("Executed Step = CALLRENDERERSCRIPT,CURRENTDATEASISSUEDATE"); 
        TYPE.$("ELE_SELECTDATEINISSUEDECLINEWITHDRAWPAGE","//input[@type='text'][@name='outerForm:effectiveDate']",var_effectiveDate,"TRUE","TGTYPESCREENREG");

        TAP.$("ELE_CLICKISSUEBUTTONTOISSUETHEPOLICY","//input[@type='submit'][@name='outerForm:Issue']	","TGTYPESCREENREG");

        TAP.$("ELE_CLICKSETTLELINKTOSETTLETHEPOLICY","//span[text()='Settle']	","TGTYPESCREENREG");

        TAP.$("ELE_CLICKSUBMITTOSETTLETHEPOLICY","//input[@type='submit'][@name='outerForm:Submit']	","TGTYPESCREENREG");

        CALL.$("WritePolicyDetailToCSV","TGTYPESCREENREG");

        HOVER.$("ELE_INQUIRYMENU","//td[@class='ThemePanelMainFolderText'][contains(text(),'Inquiry')]","TGTYPESCREENREG");

        TAP.$("ELE_QUICKINQUIRYSUBMENU","//td[contains(text(),'Quick Inquiry')]","TGTYPESCREENREG");

        TYPE.$("ELE_POLICYNUMBERTEXTFIELD","//input[@type='text'][@name='outerForm:policyNumber']",var_PolicyNumber,"FALSE","TGTYPESCREENREG");

        TAP.$("ELE_CONTINUEBUTTON","//input[@id='outerForm:Continue']","TGTYPESCREENREG");

		writeToCSV ("ELE_GETPOLICYNUMBER " , ActionWrapper.getElementValue("ELE_GETPOLICYNUMBER", "//span[@id='outerForm:policyNumber']"));
                 LOGGER.info("Executed Step = WRITETOCSV,ELE_GETPOLICYNUMBER,TGTYPESCREENREG");
		writeToCSV ("ELE_GETSTATUSFROMINQUIRY " , ActionWrapper.getElementValue("ELE_GETSTATUSFROMINQUIRY", "//span[@id='outerForm:statusText']"));
                 LOGGER.info("Executed Step = WRITETOCSV,ELE_GETSTATUSFROMINQUIRY,TGTYPESCREENREG");
		writeToCSV ("ELE_GETEFFECTIVEDATETEXTFROMGRID " , ActionWrapper.getElementValue("ELE_GETEFFECTIVEDATETEXTFROMGRID", "//span[@id='outerForm:grid:0:effectiveDateOut2']"));
                 LOGGER.info("Executed Step = WRITETOCSV,ELE_GETEFFECTIVEDATETEXTFROMGRID,TGTYPESCREENREG");
		writeToCSV ("ELE_GETSTATECOUNTRY " , ActionWrapper.getElementValue("ELE_GETSTATECOUNTRY", "//span[@id='outerForm:ownerCityStateZip']"));
                 LOGGER.info("Executed Step = WRITETOCSV,ELE_GETSTATECOUNTRY,TGTYPESCREENREG");
		writeToCSV ("ELE_GETAGENTNUMBER " , ActionWrapper.getElementValue("ELE_GETAGENTNUMBER", "//span[@id='outerForm:agentNumber']"));
                 LOGGER.info("Executed Step = WRITETOCSV,ELE_GETAGENTNUMBER,TGTYPESCREENREG");
		var_Count = var_Count + ( 1 );
                 LOGGER.info("Executed Step = INCREASEVAR,var_Count,1,TGTYPESCREENREG");
    	}
          LOGGER.info("Executed Step = WEND,TGTYPESCREENREG");
    	resetAttemps();

    }


}
