package appium.tgo;

import com.tg.octopus.base.TGOWrapper;
import com.tg.octopus.libs.testng.listener.TestListener;
import static com.tg.octopus.base.TGOWrapper.Action.*;
import static com.tg.octopus.base.TGOWrapper.LOGGER;
import com.tg.octopus.wrapper.ActionWrapper;

import com.jayway.jsonpath.JsonPath;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.SessionNotCreatedException;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.Augmenter;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.remote.UnreachableBrowserException;

import org.testng.Assert;
import org.testng.ITestContext;
import org.testng.ITestResult;
import org.testng.Reporter;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;
import java.net.HttpURLConnection;
import java.net.URL;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import org.json.JSONArray;
import org.json.JSONObject;
import helper.WebTestUtility;
import java.sql.*;
import com.testautomationguru.utility.PDFUtil;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.multipdf.Splitter;

import java.util.*;
import org.openqa.selenium.*;
import java.io.*;
import java.net.*;

import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;


@Listeners(TestListener.class)
public class TestCases extends TGOWrapper {



    static {
       needsresetoncebeforetest ="false";
       needsresetbeforeeverymethod ="true";
       captureverystepsnap ="true";
       appendlastsnap ="false";
       ishybridapp ="false";

    }


    @Test
    public void tc01nonforfeiturechangingtheoption() {

        // No need to declare / print method name here, handled by TestNg listener
        // declare("tc01nonforfeiturechangingtheoption");

        CALL.$("LoginToGIAS36","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_USERNAME","//input[@id='outerForm:username']","ganesh","FALSE","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_PASSWORD","//input[@id='outerForm:password']","123456a","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_LOGIN","//input[@id='outerForm:loginButton']","TGTYPESCREENREG");

        CALL.$("ChangeNonForfeitureOptionToExtendedTerm","TGTYPESCREENREG");

        HOVER.$("ELE_MENU_BILLING","//td[@class='ThemePanelMainFolderText'][contains(text(),'Billing')]","TGTYPESCREENREG");

        TAP.$("ELE_SUBMEN_POLICYCONTRACTS","//td[contains(text(),'Policy Contracts')]","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_POLICYNUMBER","//input[@id='outerForm:policyNumber']","123455","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_CONTINUE","//input[@id='outerForm:Continue']","TGTYPESCREENREG");

        SELECT.$("ELE_DRPDWN_NONFORFEITUREOPTION","//select[@id='outerForm:nonForfeitureOption']","Extended Term","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SUBMIT","//input[@id='outerForm:Submit']","TGTYPESCREENREG");


    }


}
