package appium.tgo;

import com.tg.octopus.base.TGOWrapper;
import com.tg.octopus.libs.testng.listener.TestListener;
import static com.tg.octopus.base.TGOWrapper.Action.*;
import static com.tg.octopus.base.TGOWrapper.LOGGER;
import com.tg.octopus.wrapper.ActionWrapper;

import com.jayway.jsonpath.JsonPath;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.SessionNotCreatedException;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.Augmenter;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.remote.UnreachableBrowserException;

import org.testng.Assert;
import org.testng.ITestContext;
import org.testng.ITestResult;
import org.testng.Reporter;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;
import java.net.HttpURLConnection;
import java.net.URL;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import org.json.JSONArray;
import org.json.JSONObject;
import helper.WebTestUtility;
import java.sql.*;
import com.testautomationguru.utility.PDFUtil;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.multipdf.Splitter;

import java.util.*;
import org.openqa.selenium.*;
import java.io.*;
import java.net.*;

import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;


@Listeners(TestListener.class)
public class GS36_NonforfeitureOption extends TGOWrapper {



    static {
       needsresetoncebeforetest ="false";
       needsresetbeforeeverymethod ="true";
       captureverystepsnap ="true";
       appendlastsnap ="false";
       ishybridapp ="false";

    }


    @Test
    public void tc0136nonforfeiturechangingtheoption() {

        // No need to declare / print method name here, handled by TestNg listener
        // declare("tc0136nonforfeiturechangingtheoption");

        CALL.$("LoginToGIAS36","TGTYPESCREENREG");

        TYPE.$("ELE_LOGIN_USERNAME","//input[@id='outerForm:username']","ganesh","FALSE","TGTYPESCREENREG");

        TYPE.$("ELE_LOGIN_PASSWORD","//input[@id='outerForm:password']","123456a","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_LOGINBUTTON","//input[@id='outerForm:loginButton']","TGTYPESCREENREG");

		String var_policyNumber;
                 LOGGER.info("Executed Step = VAR,String,var_policyNumber,TGTYPESCREENREG");
		com.jayway.jsonpath.ReadContext var_CSVData = getJsonData("C:/GIAS Automation/3.6 BaseRegression/InputData/3_6_nonforfeiture.csv");
                 LOGGER.info("Executed Step = VAR,Json,var_CSVData,C:/GIAS Automation/3.6 BaseRegression/InputData/3_6_nonforfeiture.csv,TGTYPESCREENREG");
		int var_Count;
                 LOGGER.info("Executed Step = VAR,Integer,var_Count,TGTYPESCREENREG");
		var_Count = 0;
                 LOGGER.info("Executed Step = STORE,var_Count,0,TGTYPESCREENREG");
        LOGGER.info("Executed Step = REPEAT IF");
    	while (check(var_Count,"<",1,"TGTYPESCREENREG")) {
          LOGGER.info("Executed Step = WHILE,var_Count,<,1,TGTYPESCREENREG");
		var_policyNumber = getJsonData(var_CSVData , "$.records["+var_Count+"].PolicyNumber");
                 LOGGER.info("Executed Step = STORE,var_policyNumber,var_CSVData,$.records[{var_Count}].PolicyNumber,TGTYPESCREENREG");
        CALL.$("InquiryPolicyForfeitureStatus","TGTYPESCREENREG");

        HOVER.$("ELE_INQUIRYMENU","//td[@class='ThemePanelMainFolderText'][contains(text(),'Inquiry')]","TGTYPESCREENREG");

        TAP.$("ELE_QUICKINQUIRYSUBMENU","//td[@class='ThemePanelMenuItemText'][contains(text(),'Quick Inquiry')]","TGTYPESCREENREG");

        TYPE.$("ELE_NFO_ENTERPOLICYNUMBER","//input[@type='text'][@name='outerForm:policyNumber']",var_policyNumber,"FALSE","TGTYPESCREENREG");

        TAP.$("ELE_CONTINUEBUTTON","//input[@id='outerForm:Continue']","TGTYPESCREENREG");

        ASSERT.$("ELE_GETPOLICYNONFORFEITURESTATUS","//span[@id='outerForm:nonforfeitureOptionText']","CONTAINS","Automatic Premium Loan","TGTYPESCREENREG");

		writeToCSV("var_policyNumber " , var_policyNumber);
                 LOGGER.info("Executed Step = WRITETOCSV,var_policyNumber,TGTYPESCREENREG");
		writeToCSV ("ELE_GETPOLICYNONFORFEITURESTATUS " , ActionWrapper.getElementValue("ELE_GETPOLICYNONFORFEITURESTATUS", "//span[@id='outerForm:nonforfeitureOptionText']"));
                 LOGGER.info("Executed Step = WRITETOCSV,ELE_GETPOLICYNONFORFEITURESTATUS,TGTYPESCREENREG");
        CALL.$("ChangetheNonforfeitureOptionToExtendedTerm","TGTYPESCREENREG");

        HOVER.$("ELE_BILLINGMENU","//td[@class='ThemePanelMainFolderText'][contains(text(),'Billing')]","TGTYPESCREENREG");

        TAP.$("ELE_POLICYCONTRACTSUBMENU","//td[contains(text(),'Policy Contract')]","TGTYPESCREENREG");

        TYPE.$("ELE_NFO_ENTERPOLICYNUMBER","//input[@type='text'][@name='outerForm:policyNumber']",var_policyNumber,"FALSE","TGTYPESCREENREG");

        TAP.$("ELE_CONTINUEBUTTON","//input[@id='outerForm:Continue']","TGTYPESCREENREG");

        SELECT.$("ELE_NFO_NONFORFEITUREOPTION","//select[@name='outerForm:nonForfeitureOption']","Extended Term","TGTYPESCREENREG");

        SWIPE.$("UP","ELE_NFO_SUBMITBUTTON","//input[@type='submit'][@name='outerForm:Submit']","TGTYPESCREENREG");

        TAP.$("ELE_NFO_SUBMITBUTTON","//input[@type='submit'][@name='outerForm:Submit']","TGTYPESCREENREG");

		var_Count = var_Count + ( 1 );
                 LOGGER.info("Executed Step = INCREASEVAR,var_Count,1,TGTYPESCREENREG");
    	}
          LOGGER.info("Executed Step = WEND,TGTYPESCREENREG");
    	resetAttemps();

    }


}
