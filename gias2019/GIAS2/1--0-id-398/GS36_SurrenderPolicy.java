package appium.tgo;

import com.tg.octopus.base.TGOWrapper;
import com.tg.octopus.libs.testng.listener.TestListener;
import static com.tg.octopus.base.TGOWrapper.Action.*;
import static com.tg.octopus.base.TGOWrapper.LOGGER;
import com.tg.octopus.wrapper.ActionWrapper;

import com.jayway.jsonpath.JsonPath;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.SessionNotCreatedException;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.Augmenter;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.remote.UnreachableBrowserException;

import org.testng.Assert;
import org.testng.ITestContext;
import org.testng.ITestResult;
import org.testng.Reporter;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;
import java.net.HttpURLConnection;
import java.net.URL;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import org.json.JSONArray;
import org.json.JSONObject;
import helper.WebTestUtility;
import java.sql.*;
import com.testautomationguru.utility.PDFUtil;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.multipdf.Splitter;

import java.util.*;
import org.openqa.selenium.*;
import java.io.*;
import java.net.*;

import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;


@Listeners(TestListener.class)
public class GS36_SurrenderPolicy extends TGOWrapper {



    static {
       needsresetoncebeforetest ="false";
       needsresetbeforeeverymethod ="true";
       captureverystepsnap ="true";
       appendlastsnap ="false";
       ishybridapp ="false";

    }


    @Test
    public void gs36fullsurrenderpolicy() {

        // No need to declare / print method name here, handled by TestNg listener
        // declare("gs36fullsurrenderpolicy");

        CALL.$("LoginToGIAS36","TGTYPESCREENREG");

        TYPE.$("ELE_LOGINUSERNAME","//input[@id='outerForm:username']","ganesh","FALSE","TGTYPESCREENREG");

        TYPE.$("ELE_LOGINPASSWORD","//input[@id='outerForm:password']","123456a","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_LOGINBUTTON","//input[@id='outerForm:loginButton']","TGTYPESCREENREG");

		int var_Count;
                 LOGGER.info("Executed Step = VAR,Integer,var_Count,TGTYPESCREENREG");
		var_Count = 0;
                 LOGGER.info("Executed Step = STORE,var_Count,0,TGTYPESCREENREG");
        LOGGER.info("Executed Step = REPEAT IF");
    	while (check(var_Count,"<",1,"TGTYPESCREENREG")) {
          LOGGER.info("Executed Step = WHILE,var_Count,<,1,TGTYPESCREENREG");
        CALL.$("NavigateToSurrenderPolicy","TGTYPESCREENREG");

        HOVER.$("ELE_SURRENDER_POLICYOWNERSERVICESMENU","//td[@class='ThemePanelMainFolderText'][contains(text(),'Policyowner Services')]","TGTYPESCREENREG");

        HOVER.$("ELE_SURRENDERSUBMENU","//td[@class='ThemePanelMenuFolderText'][contains(text(),'Surrender')]","TGTYPESCREENREG");

        TAP.$("ELE_FULLPARTIALSURRENDERREQUEST","//td[contains(text(),'Full/Partial Surrender')]","TGTYPESCREENREG");

		com.jayway.jsonpath.ReadContext var_CSVData = getJsonData("C:/GIAS Automation/3.6 BaseRegression/InputData/3_6_surrender.csv");
                 LOGGER.info("Executed Step = VAR,Json,var_CSVData,C:/GIAS Automation/3.6 BaseRegression/InputData/3_6_surrender.csv,TGTYPESCREENREG");
		String var_policyNumber;
                 LOGGER.info("Executed Step = VAR,String,var_policyNumber,TGTYPESCREENREG");
		var_policyNumber = getJsonData(var_CSVData , "$.records.["+var_Count+"].SurrenderPolicyNumber");
                 LOGGER.info("Executed Step = STORE,var_policyNumber,var_CSVData,$.records.[{var_Count}].SurrenderPolicyNumber,TGTYPESCREENREG");
        TYPE.$("ELE_SURRENDER_POLICYNUMBER","//input[@id='outerForm:policyNumber']",var_policyNumber,"FALSE","TGTYPESCREENREG");

        TYPE.$("ELE_SURRENDER_SURRENDERDATE","//input[@id='outerForm:promptDate_input']","03/20/2019","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_SURRENDER_FULLSURRENDERBUTTON","//input[@id='outerForm:FullSurrender']",1,0,"TGTYPESCREENREG");

        TAP.$("ELE_SURRENDER_BASELINK","//span[contains(text(),'Base')]",1,0,"TGTYPESCREENREG");

        TAP.$("ELE_SURRENDER_DSTRIBUTIONINFORMATIONLINK","//span[@id='outerForm:DistributionInformationText']",1,0,"TGTYPESCREENREG");

        SCROLL.$("ELE_SURRENDER_CONTINUEBUTTON","//input[@id='outerForm:Continue']","DOWN","TGTYPESCREENREG");

        TAP.$("ELE_SURRENDER_CONTINUEBUTTON","//input[@id='outerForm:Continue']",1,0,"TGTYPESCREENREG");

        SELECT.$("ELE_SURRENDER_DISTRIBUTIONREASONCODE_FULL1","//select[@name='outerForm:distributionReasonCode_full']","7 - Normal Distribution","TGTYPESCREENREG");

        SCROLL.$("ELE_SUBMITBUTTON","//input[@id='outerForm:Submit']","DOWN","TGTYPESCREENREG");

        TAP.$("ELE_SUBMITBUTTON","//input[@id='outerForm:Submit']",1,0,"TGTYPESCREENREG");

        CALL.$("InquireSurrenderPolicy","TGTYPESCREENREG");

        HOVER.$("ELE_INQUIRY","//td[@class='ThemePanelMainFolderText'][contains(text(),'Inquiry')]","TGTYPESCREENREG");

        TAP.$("ELE_POLICYDETAIL","//td[contains(text(),'Policy Detail')]","TGTYPESCREENREG");

        TYPE.$("ELE_SURRENDER_POLICYNUMBER","//input[@id='outerForm:policyNumber']",var_policyNumber,"FALSE","TGTYPESCREENREG");

        TAP.$("ELE_SURRENDER_CONTINUEBUTTON","//input[@id='outerForm:Continue']","TGTYPESCREENREG");

		writeToCSV ("ELE_GETPOLICYNUMBER " , ActionWrapper.getElementValue("ELE_GETPOLICYNUMBER", "//span[@id='outerForm:policyNumber']"));
                 LOGGER.info("Executed Step = WRITETOCSV,ELE_GETPOLICYNUMBER,TGTYPESCREENREG");
		writeToCSV ("ELE_STATUS " , ActionWrapper.getElementValue("ELE_STATUS", "//span[@id='outerForm:statusText']"));
                 LOGGER.info("Executed Step = WRITETOCSV,ELE_STATUS,TGTYPESCREENREG");
		writeToCSV ("ELE_EFFECTIVEDATE " , ActionWrapper.getElementValue("ELE_EFFECTIVEDATE", "//span[@id='outerForm:effectiveDate_input']"));
                 LOGGER.info("Executed Step = WRITETOCSV,ELE_EFFECTIVEDATE,TGTYPESCREENREG");
		writeToCSV ("ELE_ANNUALPREMIUM " , ActionWrapper.getElementValue("ELE_ANNUALPREMIUM", "//span[@id='outerForm:annualPremium']"));
                 LOGGER.info("Executed Step = WRITETOCSV,ELE_ANNUALPREMIUM,TGTYPESCREENREG");
        TAP.$("ELE_POLICYVALUESLINK","//span[contains(text(),'Policy Values')]","TGTYPESCREENREG");

		writeToCSV ("ELE_NETPOLICYVALUE " , ActionWrapper.getElementValue("ELE_NETPOLICYVALUE", "//span[@id='outerForm:netCashValue']"));
                 LOGGER.info("Executed Step = WRITETOCSV,ELE_NETPOLICYVALUE,TGTYPESCREENREG");
		var_Count = var_Count + ( 1 );
                 LOGGER.info("Executed Step = INCREASEVAR,var_Count,1,TGTYPESCREENREG");
    	}
          LOGGER.info("Executed Step = WEND,TGTYPESCREENREG");
    	resetAttemps();

    }


}
