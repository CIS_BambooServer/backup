package appium.tgo;

import com.tg.octopus.base.TGOWrapper;
import com.tg.octopus.libs.testng.listener.TestListener;
import static com.tg.octopus.base.TGOWrapper.Action.*;
import static com.tg.octopus.base.TGOWrapper.LOGGER;
import com.tg.octopus.wrapper.ActionWrapper;

import com.jayway.jsonpath.JsonPath;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.SessionNotCreatedException;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.Augmenter;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.remote.UnreachableBrowserException;

import org.testng.Assert;
import org.testng.ITestContext;
import org.testng.ITestResult;
import org.testng.Reporter;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;
import java.net.HttpURLConnection;
import java.net.URL;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import org.json.JSONArray;
import org.json.JSONObject;
import helper.WebTestUtility;
import java.sql.*;
import com.testautomationguru.utility.PDFUtil;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.multipdf.Splitter;

import java.util.*;
import org.openqa.selenium.*;
import java.io.*;
import java.net.*;

import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;


@Listeners(TestListener.class)
public class GS36_LoanProcessing extends TGOWrapper {



    static {
       needsresetoncebeforetest ="false";
       needsresetbeforeeverymethod ="true";
       captureverystepsnap ="true";
       appendlastsnap ="false";
       ishybridapp ="false";

    }


    @Test
    public void tc0136loanprocessingapplyforloan() {

        // No need to declare / print method name here, handled by TestNg listener
        // declare("tc0136loanprocessingapplyforloan");

        TYPE.$("ELE_USERNAME","//input[@id='outerForm:username']","ganesh","FALSE","TGTYPESCREENREG");

        TYPE.$("ELE_PASSWORD","//input[@id='outerForm:password']","123456a","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_LOGINBUTTON","//input[@id='outerForm:loginButton']",1,1,"TGTYPESCREENREG");

		int var_Count;
                 LOGGER.info("Executed Step = VAR,Integer,var_Count,TGTYPESCREENREG");
		var_Count = 0;
                 LOGGER.info("Executed Step = STORE,var_Count,0,TGTYPESCREENREG");
		com.jayway.jsonpath.ReadContext var_CSVData = getJsonData("C:/GIAS Automation/3.6 BaseRegression/InputData/3_6_loanprocessing.csv");
                 LOGGER.info("Executed Step = VAR,Json,var_CSVData,C:/GIAS Automation/3.6 BaseRegression/InputData/3_6_loanprocessing.csv,TGTYPESCREENREG");
        LOGGER.info("Executed Step = REPEAT IF");
    	while (check(var_Count,"<",1,"TGTYPESCREENREG")) {
          LOGGER.info("Executed Step = WHILE,var_Count,<,1,TGTYPESCREENREG");
		String var_PolicyNumber;
                 LOGGER.info("Executed Step = VAR,String,var_PolicyNumber,TGTYPESCREENREG");
		var_PolicyNumber = getJsonData(var_CSVData , "$.records["+var_Count+"].PolicyNumber");
                 LOGGER.info("Executed Step = STORE,var_PolicyNumber,var_CSVData,$.records[{var_Count}].PolicyNumber,TGTYPESCREENREG");
        CALL.$("ProcessTheLoanTransaction","TGTYPESCREENREG");

        HOVER.$("ELE_POLICYOWNERSERVICESMENU","//div[@id='outerForm_menu_menu']//tbody//tr[11]//td[@class='ThemePanelMainFolderText']","TGTYPESCREENREG");

        HOVER.$("ELE_LOANLIENSUBMENU","//div[@id='cmSubMenuID38']//tbody//tr[4]//td[2]","TGTYPESCREENREG");

        TAP.$("ELE_LOANSUBMENU","//div[@id='cmSubMenuID41']//tbody//tr[2]//td[2]","TGTYPESCREENREG");

        TYPE.$("ELE_LOANPROCESSING_ENTERPOLICYNUM","//input[@type='text'][@name='outerForm:policyNumber']",var_PolicyNumber,"FALSE","TGTYPESCREENREG");

        TAP.$("ELE_LOANPROCESSING_CONTINUEBUTTON","//input[@type='submit'][@name='outerForm:Continue']","TGTYPESCREENREG");

		int var_RequestedLoanAmount;
                 LOGGER.info("Executed Step = VAR,Integer,var_RequestedLoanAmount,TGTYPESCREENREG");
		String var_RequestLoanAmt;
                 LOGGER.info("Executed Step = VAR,String,var_RequestLoanAmt,TGTYPESCREENREG");
		var_RequestedLoanAmount = ActionWrapper.getElementValueForVariableWithInt("ELE_MAXIMUMAVAILABLENETLOAN", "//span[@id='outerForm:maximumAvailableNetLoan']");
                 LOGGER.info("Executed Step = STORE,var_RequestedLoanAmount,ELE_MAXIMUMAVAILABLENETLOAN,TGTYPESCREENREG");
        TAP.$("ELE_MAXLOANREQUESTRADIOBTTNNO","//*[@id='outerForm:maximumLoanRequest:0']","TGTYPESCREENREG");

        TAP.$("ELE_GROSSORNETRADIOBTTNNET","//input[@id='outerForm:grossOrNetLoan:1']","TGTYPESCREENREG");

		try { 
		 var_RequestedLoanAmount =var_RequestedLoanAmount/2;
		 
		 ;} 
		 catch(Exception e) { PrintVal("Renderer Script Exception : "+e.toString()); } 
		 PrintVal("Executed Step = CALLRENDERERSCRIPT,REQUESTLOANAMOUNT"); 
        TYPE.$("ELE_LOANPROCESSING_REQUESTEDLOANAMT","//input[@type='text'][@name='outerForm:loanRequest']",var_RequestedLoanAmount,"FALSE","TGTYPESCREENREG");

        TAP.$("ELE_LOANPROCESSING_QUOTEBUTTON","//input[@type='submit'][@name='outerForm:Quote']","TGTYPESCREENREG");

        ASSERT.$("ELE_DISTRIBUTIONCONFIRMATIONWARNING","//li[@class='MessageWarn']","CONTAINS","DISTRIBUTION_INFO_MUST_BE_CONFIRMED","TGTYPESCREENREG");

        CALL.$("CompleteDistributionRequest","TGTYPESCREENREG");

        TAP.$("ELE_LOANPROCESSING_DISTRIBUTIONINFO","//span[text()='Distribution Information']","TGTYPESCREENREG");

        SELECT.$("ELE_LOANPROCESSING_CURRENCY","//select[@name='outerForm:disbursementCurrency']	","Dollars [US]","TGTYPESCREENREG");

        TAP.$("ELE_LOANPROCESSING_DISTINFOCONTINUEBUTTON","//input[@type='submit'][@name='outerForm:Continue']","TGTYPESCREENREG");

        TAP.$("ELE_LOANPROCESSING_SUBMITBUTTON","//input[@type='submit'][@name='outerForm:Submit']","TGTYPESCREENREG");

        ASSERT.$("ELE_SUCCESSMESSAGE","//li[@class='MessageInfo']","CONTAINS","Request completed successfully","TGTYPESCREENREG");

		var_Count = var_Count + ( 1 );
                 LOGGER.info("Executed Step = INCREASEVAR,var_Count,1,TGTYPESCREENREG");
    	}
          LOGGER.info("Executed Step = WEND,TGTYPESCREENREG");
    	resetAttemps();

    }


}
