package appium.tgo;

import com.tg.octopus.base.TGOWrapper;
import com.tg.octopus.libs.testng.listener.TestListener;
import static com.tg.octopus.base.TGOWrapper.Action.*;
import static com.tg.octopus.base.TGOWrapper.LOGGER;
import com.tg.octopus.wrapper.ActionWrapper;

import com.jayway.jsonpath.JsonPath;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.SessionNotCreatedException;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.Augmenter;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.remote.UnreachableBrowserException;

import org.testng.Assert;
import org.testng.ITestContext;
import org.testng.ITestResult;
import org.testng.Reporter;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;
import java.net.HttpURLConnection;
import java.net.URL;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import org.json.JSONArray;
import org.json.JSONObject;
import helper.WebTestUtility;
import java.sql.*;
import com.testautomationguru.utility.PDFUtil;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.multipdf.Splitter;

import java.util.*;
import org.openqa.selenium.*;
import java.io.*;
import java.net.*;

import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;


@Listeners(TestListener.class)
public class GS36_SuspenseMaintenance extends TGOWrapper {



    static {
       needsresetoncebeforetest ="false";
       needsresetbeforeeverymethod ="true";
       captureverystepsnap ="true";
       appendlastsnap ="false";
       ishybridapp ="false";

    }


    @Test
    public void tc01gs36suspenseaccount() {

        // No need to declare / print method name here, handled by TestNg listener
        // declare("tc01gs36suspenseaccount");

        CALL.$("LoginToGIAS36","TGTYPESCREENREG");

        TYPE.$("ELE_LOGINUSERNAME","//input[@name='outerForm:username']","ganesh","TRUE","TGTYPESCREENREG");

        TYPE.$("ELE_LOGINPASSWORD","//input[@id='outerForm:password']","123456a","TRUE","TGTYPESCREENREG");

        TAP.$("ELE_LOGINBUTTON","//input[@id='outerForm:loginButton']","TGTYPESCREENREG");

		int var_Count;
                 LOGGER.info("Executed Step = VAR,Integer,var_Count,TGTYPESCREENREG");
		var_Count = 0;
                 LOGGER.info("Executed Step = STORE,var_Count,0,TGTYPESCREENREG");
		com.jayway.jsonpath.ReadContext var_CSVData = getJsonData("C:/GIAS Automation/3.6 BaseRegression/InputData/3_6_suspense.csv");
                 LOGGER.info("Executed Step = VAR,Json,var_CSVData,C:/GIAS Automation/3.6 BaseRegression/InputData/3_6_suspense.csv,TGTYPESCREENREG");
        LOGGER.info("Executed Step = REPEAT IF");
    	while (check(var_Count,"<",1,"TGTYPESCREENREG")) {
          LOGGER.info("Executed Step = WHILE,var_Count,<,1,TGTYPESCREENREG");
		String var_SuspensePolicyNumber;
                 LOGGER.info("Executed Step = VAR,String,var_SuspensePolicyNumber,TGTYPESCREENREG");
		var_SuspensePolicyNumber = getJsonData(var_CSVData , "$.records["+var_Count+"].SuspensePolicyNumber");
                 LOGGER.info("Executed Step = STORE,var_SuspensePolicyNumber,var_CSVData,$.records[{var_Count}].SuspensePolicyNumber,TGTYPESCREENREG");
        CALL.$("CheckMinimumIsLessOrEqualToSuspenseBalance","TGTYPESCREENREG");

		int var_SuspenseBalance;
                 LOGGER.info("Executed Step = VAR,Integer,var_SuspenseBalance,TGTYPESCREENREG");
		int var_getMinimumBalance;
                 LOGGER.info("Executed Step = VAR,Integer,var_getMinimumBalance,TGTYPESCREENREG");
		String var_MinimumBalance;
                 LOGGER.info("Executed Step = VAR,String,var_MinimumBalance,TGTYPESCREENREG");
        HOVER.$("ELE_INQUIRYMENU","//td[@class='ThemePanelMainFolderText'][contains(text(),'Inquiry')]","TGTYPESCREENREG");

        TAP.$("ELE_POLICYDETAILINQUIRYSUBMENU","//td[contains(text(),'Policy Detail')]","TGTYPESCREENREG");

        TYPE.$("ELE_SUSPENSEQUICKINQUIRYPOLICYNUMBER","//input[@id='outerForm:policyNumber']",var_SuspensePolicyNumber,"FALSE","TGTYPESCREENREG");

        TAP.$("ELE_CONTINUEBUTTON","//input[@id='outerForm:Continue']","TGTYPESCREENREG");

		var_getMinimumBalance = ActionWrapper.getElementValueForVariableWithInt("ELE_GETMINIMUMBALANCEFROMINQUIRY", "//span[@id='outerForm:minimumRequiredPrem']");
                 LOGGER.info("Executed Step = STORE,var_getMinimumBalance,ELE_GETMINIMUMBALANCEFROMINQUIRY,TGTYPESCREENREG");
		var_SuspenseBalance = ActionWrapper.getElementValueForVariableWithInt("ELE_GETSUSPENSEBALANCEFROMINQUIRY", "//span[@id='outerForm:suspenseBalance']");
                 LOGGER.info("Executed Step = STORE,var_SuspenseBalance,ELE_GETSUSPENSEBALANCEFROMINQUIRY,TGTYPESCREENREG");
		var_MinimumBalance = ActionWrapper.getElementValue("ELE_GETMINIMUMBALANCEFROMINQUIRY", "//span[@id='outerForm:minimumRequiredPrem']");
                 LOGGER.info("Executed Step = STORE,var_MinimumBalance,ELE_GETMINIMUMBALANCEFROMINQUIRY,TGTYPESCREENREG");
        LOGGER.info("Executed Step = START IF");
        if (check(var_getMinimumBalance,"<=",var_SuspenseBalance,"TGTYPESCREENREG")) {
         LOGGER.info("Executed Step = IF,var_getMinimumBalance,<=,var_SuspenseBalance,TGTYPESCREENREG");
        CALL.$("SearchSuspenseAccount","TGTYPESCREENREG");

        HOVER.$("ELE_PAYMENTSMENU","//td[@class='ThemePanelMainFolderText'][contains(text(),'Payments')]","TGTYPESCREENREG");

        TAP.$("ELE_SUSPENSEPAYMENTSUBMENU","//div[@id='cmSubMenuID24']//td[@class='ThemePanelMenuItemText'][contains(text(),'Suspense')]","TGTYPESCREENREG");

        TAP.$("ELE_SUSPENSE_SEARECHROUNDBUTTON","//img[@id='outerForm:gnSearchImage']","TGTYPESCREENREG");

        SELECT.$("ELE_SEARCHCONTROLNUMBERDRPDOWN","//select[@name='outerForm:grid:3:FullOperator']","Equals","TGTYPESCREENREG");

        TYPE.$("ELE_SEARCHCONTROLNUMBERVALUE","//input[@id='outerForm:grid:3:fieldUpper']",var_SuspensePolicyNumber,"FALSE","TGTYPESCREENREG");

        TAP.$("ELE_SUSPENSE_SEARCBUTTON","//input[@id='outerForm:Search']","TGTYPESCREENREG");

        WAIT.$("ELE_SUSPENSE_SEARECHROUNDBUTTON","//img[@id='outerForm:gnSearchImage']","VISIBLE","TGTYPESCREENREG");

        LOGGER.info("Executed Step = START IF");
        if (check("ELE_GETPREMIUMSUSPENSEFROMSEARCHRESULT","//span[contains(text(),'Premium suspense')]","CONTAINS","Premium suspense","TGTYPESCREENREG")) {
         LOGGER.info("Executed Step = IF,ELE_GETPREMIUMSUSPENSEFROMSEARCHRESULT,CONTAINS,Premium suspense,TGTYPESCREENREG");
        CALL.$("MakePayment","TGTYPESCREENREG");

        HOVER.$("ELE_PAYMENTSMENU","//td[@class='ThemePanelMainFolderText'][contains(text(),'Payments')]","TGTYPESCREENREG");

        TAP.$("ELE_POLICYPAYMENTSSUBMENU","//div[@id='cmSubMenuID24']//td[@class='ThemePanelMenuItemText'][contains(text(),'Policy')]","TGTYPESCREENREG");

        TYPE.$("ELE_SUSPENSEQUICKINQUIRYPOLICYNUMBER","//input[@id='outerForm:policyNumber']",var_SuspensePolicyNumber,"FALSE","TGTYPESCREENREG");

        TAP.$("ELE_CONTINUEBUTTON","//input[@id='outerForm:Continue']","TGTYPESCREENREG");

        TAP.$("ELE_SUSPENSE_SUBMITRECTBUTTON","//input[@id='outerForm:Submit']","TGTYPESCREENREG");

        }
          LOGGER.info("Executed Step = ENDIF,TGTYPESCREENREG");
        }
         else {
          LOGGER.info("Executed Step = ELSE,TGTYPESCREENREG");
        CALL.$("fillAddSuspenseDetailsForm","TGTYPESCREENREG");

        HOVER.$("ELE_PAYMENTSMENU","//td[@class='ThemePanelMainFolderText'][contains(text(),'Payments')]","TGTYPESCREENREG");

        TAP.$("ELE_SUSPENSEPAYMENTSUBMENU","//div[@id='cmSubMenuID24']//td[@class='ThemePanelMenuItemText'][contains(text(),'Suspense')]","TGTYPESCREENREG");

        TAP.$("ELE_SUSPENSE_ADDROUNDBUTTON","//img[@id='outerForm:gnAddImage']","TGTYPESCREENREG");

        SELECT.$("ELE_ADDSUSPENSECURRENCYCODEDRPDOWN","//select[@id='outerForm:currencyCode']","Dollars [US]","TGTYPESCREENREG");

        TYPE.$("ELE_ADDSUSPENSECONTROLNUMBER","//input[@id='outerForm:suspenseControlNumb']",var_SuspensePolicyNumber,"FALSE","TGTYPESCREENREG");

        SELECT.$("ELE_ADDSUSPENSETYPEDRPDOWN","//select[@id='outerForm:suspenseType']","Premium suspense","TGTYPESCREENREG");

        TYPE.$("ELE_ADDSUSPENSEAMOUNTOFCHANGE","//input[@id='outerForm:amountOfChange']",var_MinimumBalance,"TRUE","TGTYPESCREENREG");

        TYPE.$("ELE_ADDSUSPENSECOMMENT","//input[@id='outerForm:suspenseComment']","Premium suspense","FALSE","TGTYPESCREENREG");

        SELECT.$("ELE_ADDSUSPENSEACCOUNTNUMBERDRPDWN","//select[@id='outerForm:suspenseAccountNumb']","153000 - CASH","TGTYPESCREENREG");

        TAP.$("ELE_SUSPENSE_SUBMITRECTBUTTON","//input[@id='outerForm:Submit']","TGTYPESCREENREG");

        LOGGER.info("Executed Step = START IF");
        if (check("ELE_CONFIRMSUBMITBUTTON","//input[@id='outerForm:ConfirmSubmit']","VISIBLE","TGTYPESCREENREG")) {
         LOGGER.info("Executed Step = IF,ELE_CONFIRMSUBMITBUTTON,VISIBLE,TGTYPESCREENREG");
        TAP.$("ELE_CONFIRMSUBMITBUTTON","//input[@id='outerForm:ConfirmSubmit']","TGTYPESCREENREG");

        }
          LOGGER.info("Executed Step = ENDIF,TGTYPESCREENREG");
        CALL.$("MakePayment","TGTYPESCREENREG");

        HOVER.$("ELE_PAYMENTSMENU","//td[@class='ThemePanelMainFolderText'][contains(text(),'Payments')]","TGTYPESCREENREG");

        TAP.$("ELE_POLICYPAYMENTSSUBMENU","//div[@id='cmSubMenuID24']//td[@class='ThemePanelMenuItemText'][contains(text(),'Policy')]","TGTYPESCREENREG");

        TYPE.$("ELE_SUSPENSEQUICKINQUIRYPOLICYNUMBER","//input[@id='outerForm:policyNumber']",var_SuspensePolicyNumber,"FALSE","TGTYPESCREENREG");

        TAP.$("ELE_CONTINUEBUTTON","//input[@id='outerForm:Continue']","TGTYPESCREENREG");

        TAP.$("ELE_SUSPENSE_SUBMITRECTBUTTON","//input[@id='outerForm:Submit']","TGTYPESCREENREG");

        }
          LOGGER.info("Executed Step = ENDIF,TGTYPESCREENREG");
        CALL.$("ToCheckSuspenseAccountBalance","TGTYPESCREENREG");

        HOVER.$("ELE_INQUIRYMENU","//td[@class='ThemePanelMainFolderText'][contains(text(),'Inquiry')]","TGTYPESCREENREG");

        TAP.$("ELE_QUICKINQUIRYSUBMENU","//td[contains(text(),'Quick Inquiry')]","TGTYPESCREENREG");

        TYPE.$("ELE_SUSPENSEQUICKINQUIRYPOLICYNUMBER","//input[@id='outerForm:policyNumber']",var_SuspensePolicyNumber,"FALSE","TGTYPESCREENREG");

        TAP.$("ELE_CONTINUEBUTTON","//input[@id='outerForm:Continue']","TGTYPESCREENREG");

		writeToCSV ("ELE_GETPOLICYNUMBER " , ActionWrapper.getElementValue("ELE_GETPOLICYNUMBER", "//span[@id='outerForm:policyNumber']"));
                 LOGGER.info("Executed Step = WRITETOCSV,ELE_GETPOLICYNUMBER,TGTYPESCREENREG");
		writeToCSV ("ELE_GETEFFECTIVEDATE " , ActionWrapper.getElementValue("ELE_GETEFFECTIVEDATE", "//span[@id='outerForm:grid:0:effectiveDateOut2']"));
                 LOGGER.info("Executed Step = WRITETOCSV,ELE_GETEFFECTIVEDATE,TGTYPESCREENREG");
		writeToCSV ("ELE_GETSTATUSFROMINQUIRY " , ActionWrapper.getElementValue("ELE_GETSTATUSFROMINQUIRY", "//span[@id='outerForm:statusText']"));
                 LOGGER.info("Executed Step = WRITETOCSV,ELE_GETSTATUSFROMINQUIRY,TGTYPESCREENREG");
		writeToCSV ("ELE_SUSPENSEBALANCECHECK " , ActionWrapper.getElementValue("ELE_SUSPENSEBALANCECHECK", "//span[@id='outerForm:suspenseBalance']"));
                 LOGGER.info("Executed Step = WRITETOCSV,ELE_SUSPENSEBALANCECHECK,TGTYPESCREENREG");
		var_Count = var_Count + ( 1 );
                 LOGGER.info("Executed Step = INCREASEVAR,var_Count,1,TGTYPESCREENREG");
    	}
          LOGGER.info("Executed Step = WEND,TGTYPESCREENREG");
    	resetAttemps();

    }


}
