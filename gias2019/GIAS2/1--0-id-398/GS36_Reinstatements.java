package appium.tgo;

import com.tg.octopus.base.TGOWrapper;
import com.tg.octopus.libs.testng.listener.TestListener;
import static com.tg.octopus.base.TGOWrapper.Action.*;
import static com.tg.octopus.base.TGOWrapper.LOGGER;
import com.tg.octopus.wrapper.ActionWrapper;

import com.jayway.jsonpath.JsonPath;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.SessionNotCreatedException;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.Augmenter;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.remote.UnreachableBrowserException;

import org.testng.Assert;
import org.testng.ITestContext;
import org.testng.ITestResult;
import org.testng.Reporter;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;
import java.net.HttpURLConnection;
import java.net.URL;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import org.json.JSONArray;
import org.json.JSONObject;
import helper.WebTestUtility;
import java.sql.*;
import com.testautomationguru.utility.PDFUtil;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.multipdf.Splitter;

import java.util.*;
import org.openqa.selenium.*;
import java.io.*;
import java.net.*;

import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;


@Listeners(TestListener.class)
public class GS36_Reinstatements extends TGOWrapper {



    static {
       needsresetoncebeforetest ="false";
       needsresetbeforeeverymethod ="true";
       captureverystepsnap ="true";
       appendlastsnap ="false";
       ishybridapp ="false";

    }


    @Test
    public void tc0136reinstatementofpolicy() {

        // No need to declare / print method name here, handled by TestNg listener
        // declare("tc0136reinstatementofpolicy");

        CALL.$("LoginToGIAS36","TGTYPESCREENREG");

        TYPE.$("ELE_USERNAME","//input[@name='outerForm:username']","ganesh","FALSE","TGTYPESCREENREG");

        TYPE.$("ELE_PASSWORD","//input[@id='outerForm:password']","123456a","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_LOGINBUTTON","outerForm:loginButton","TGTYPESCREENREG");

		int var_Count;
                 LOGGER.info("Executed Step = VAR,Integer,var_Count,TGTYPESCREENREG");
		var_Count = 0;
                 LOGGER.info("Executed Step = STORE,var_Count,0,TGTYPESCREENREG");
		com.jayway.jsonpath.ReadContext var_CSVData = getJsonData("C:/GIAS Automation/3.6 BaseRegression/InputData/3_6_reinstatement.csv");
                 LOGGER.info("Executed Step = VAR,Json,var_CSVData,C:/GIAS Automation/3.6 BaseRegression/InputData/3_6_reinstatement.csv,TGTYPESCREENREG");
		String var_PolicyNumber;
                 LOGGER.info("Executed Step = VAR,String,var_PolicyNumber,TGTYPESCREENREG");
        LOGGER.info("Executed Step = REPEAT IF");
    	while (check(var_Count,"<",1,"TGTYPESCREENREG")) {
          LOGGER.info("Executed Step = WHILE,var_Count,<,1,TGTYPESCREENREG");
		var_PolicyNumber = getJsonData(var_CSVData , "$.records["+var_Count+"].PolicyNumber");
                 LOGGER.info("Executed Step = STORE,var_PolicyNumber,var_CSVData,$.records[{var_Count}].PolicyNumber,TGTYPESCREENREG");
        CALL.$("PolicyReinstatementEligibility","TGTYPESCREENREG");

        HOVER.$("ELE_REINSTATEMENT_POLICYINQUIRYLINK","//*[@id=\"outerForm:PolicyInquiryText\"]","TGTYPESCREENREG");

        TAP.$("ELE_QUICKINQUIRY","//td[contains(text(),'Quick Inquiry')]","TGTYPESCREENREG");

        TYPE.$("ELE_REINSTATEMENT_POLICYNUMBER","//*[@id=\"outerForm:policyNumber\"]",var_PolicyNumber,"FALSE","TGTYPESCREENREG");

        TAP.$("ELE_REINSTATEMENT_CONTINUEBUTTON","//*[@id=\"outerForm:Continue\"]","TGTYPESCREENREG");

		writeToCSV ("ELE_GETSTATUS " , ActionWrapper.getElementValue("ELE_GETSTATUS", "//span[@id='outerForm:statusText']"));
                 LOGGER.info("Executed Step = WRITETOCSV,ELE_GETSTATUS,TGTYPESCREENREG");
		writeToCSV("var_PolicyNumber " , var_PolicyNumber);
                 LOGGER.info("Executed Step = WRITETOCSV,var_PolicyNumber,TGTYPESCREENREG");
		String var_Status;
                 LOGGER.info("Executed Step = VAR,String,var_Status,TGTYPESCREENREG");
		var_Status = ActionWrapper.getElementValue("ELE_GETSTATUS", "//span[@id='outerForm:statusText']");
                 LOGGER.info("Executed Step = STORE,var_Status,ELE_GETSTATUS,TGTYPESCREENREG");
        LOGGER.info("Executed Step = START IF");
        if (check(var_Status,"CONTAINS","Lapsed","TGTYPESCREENREG")) {
         LOGGER.info("Executed Step = IF,var_Status,CONTAINS,Lapsed,TGTYPESCREENREG");
        CALL.$("CheckBaseandRriderBenefits","TGTYPESCREENREG");

        TAP.$("ELE_REINSTATEMENT_POLICYINQUIRYLINK","//*[@id=\"outerForm:PolicyInquiryText\"]","TGTYPESCREENREG");

        TAP.$("ELE_REINSTATEMENT_BENEFITS","//span[@id='outerForm:PolicyInquiryText']","TGTYPESCREENREG");

        TAP.$("ELE_REINSTATEMENT_BASE","//span[@id='outerForm:grid:0:baseRiderCodeOut1']","TGTYPESCREENREG");

        TAP.$("ELE_REINSTATEMENT_SUPPLEMENTALBENEFIT","//*[@id=\"outerForm:SupplementalBenefitText\"]","TGTYPESCREENREG");

        }
          LOGGER.info("Executed Step = ENDIF,TGTYPESCREENREG");
        CALL.$("CheckBaseandRriderBenefits","TGTYPESCREENREG");

        TAP.$("ELE_REINSTATEMENT_POLICYINQUIRYLINK","//*[@id=\\"outerForm:PolicyInquiryText\\"]","TGTYPESCREENREG");

        TAP.$("ELE_REINSTATEMENT_BENEFITS","//span[@id='outerForm:PolicyInquiryText']","TGTYPESCREENREG");

        TAP.$("ELE_REINSTATEMENT_BASE","//span[@id='outerForm:grid:0:baseRiderCodeOut1']","TGTYPESCREENREG");

        TAP.$("ELE_REINSTATEMENT_SUPPLEMENTALBENEFIT","//*[@id=\\"outerForm:SupplementalBenefitText\\"]","TGTYPESCREENREG");

        CALL.$("ReinstatingTheEligiblePolicy","TGTYPESCREENREG");

        HOVER.$("ELE_POLICYOWNERSERVICEMENU","//td[@class='ThemePanelMainFolderText'][contains(text(),'Policyowner Services')]","TGTYPESCREENREG");

        HOVER.$("ELE_BENEFITMAINTENANCESUBMENU","//td[@class='ThemePanelMenuFolderText'][contains(text(),'Benefit Maintenance')]","TGTYPESCREENREG");

        TAP.$("ELE_REINSTATEMENTSUBMENU","//td[@class='ThemePanelMenuItemText'][contains(text(),'Reinstatement')]","TGTYPESCREENREG");

        TYPE.$("ELE_REINSTATEMENT_POLICYNUMBER","//*[@id=\"outerForm:policyNumber\"]",var_PolicyNumber,"FALSE","TGTYPESCREENREG");

        TAP.$("ELE_REINSTATEMENT_CONTINUEBUTTON","//*[@id=\"outerForm:Continue\"]","TGTYPESCREENREG");

        TAP.$("ELE_REINSTATEMENT_BASE","//span[@id='outerForm:grid:0:baseRiderCodeOut1']","TGTYPESCREENREG");

        SELECT.$("ELE_REINSTATEMENT_NEWBENEFITSTATUS","//*[@id=\"outerForm:newBenefitStatus\"]","Premium Paying","TGTYPESCREENREG");

		try { 
		 WebElement terminationDate =driver.findElement(By.xpath("//input[@id='outerForm:terminationDate_input']"));
		terminationDate.clear();
		 
		 ;} 
		 catch(Exception e) { PrintVal("Renderer Script Exception : "+e.toString()); } 
		 PrintVal("Executed Step = CALLRENDERERSCRIPT,CLEARTERMINATIONDATEFIELD"); 
        TAP.$("ELE_REINSTATEMENT_SUBMITBUTTON","//*[@id=\"outerForm:Submit\"]","TGTYPESCREENREG");

        LOGGER.info("Executed Step = START IF");
        if (check("ELE_REINSTATEMENT_BASE2","//span[@id='outerForm:grid:2:baseRiderCodeOut1']","VISIBLE","TGTYPESCREENREG")) {
         LOGGER.info("Executed Step = IF,ELE_REINSTATEMENT_BASE2,VISIBLE,TGTYPESCREENREG");
        TAP.$("ELE_REINSTATEMENT_BASE1","//span[@id='outerForm:grid:1:baseRiderCodeOut1']","TGTYPESCREENREG");

        SELECT.$("ELE_REINSTATEMENT_NEWBENEFITSTATUS","//*[@id=\"outerForm:newBenefitStatus\"]","Premium Paying","TGTYPESCREENREG");

		try { 
		 WebElement terminationDate =driver.findElement(By.xpath("//input[@id='outerForm:terminationDate_input']"));
		terminationDate.clear();
		 
		 ;} 
		 catch(Exception e) { PrintVal("Renderer Script Exception : "+e.toString()); } 
		 PrintVal("Executed Step = CALLRENDERERSCRIPT,CLEARTERMINATIONDATEFIELD"); 
        }
          LOGGER.info("Executed Step = ENDIF,TGTYPESCREENREG");
        LOGGER.info("Executed Step = START IF");
        if (check("ELE_REINSTATEMENT_BASE2","//span[@id='outerForm:grid:2:baseRiderCodeOut1']","VISIBLE","TGTYPESCREENREG")) {
         LOGGER.info("Executed Step = IF,ELE_REINSTATEMENT_BASE2,VISIBLE,TGTYPESCREENREG");
        TAP.$("ELE_REINSTATEMENT_BASE2","//span[@id='outerForm:grid:2:baseRiderCodeOut1']","TGTYPESCREENREG");

        SELECT.$("ELE_REINSTATEMENT_NEWBENEFITSTATUS","//*[@id=\"outerForm:newBenefitStatus\"]","Premium Paying","TGTYPESCREENREG");

		try { 
		 WebElement terminationDate =driver.findElement(By.xpath("//input[@id='outerForm:terminationDate_input']"));
		terminationDate.clear();
		 
		 ;} 
		 catch(Exception e) { PrintVal("Renderer Script Exception : "+e.toString()); } 
		 PrintVal("Executed Step = CALLRENDERERSCRIPT,CLEARTERMINATIONDATEFIELD"); 
        }
          LOGGER.info("Executed Step = ENDIF,TGTYPESCREENREG");
        LOGGER.info("Executed Step = START IF");
        if (check("ELE_REINSTATEMENT_BASE1","//span[@id='outerForm:grid:1:baseRiderCodeOut1']","VISIBLE","TGTYPESCREENREG")) {
         LOGGER.info("Executed Step = IF,ELE_REINSTATEMENT_BASE1,VISIBLE,TGTYPESCREENREG");
        TAP.$("ELE_REINSTATEMENT_BASE3","//span[@id='outerForm:grid:3:baseRiderCodeOut1']","TGTYPESCREENREG");

        SELECT.$("ELE_REINSTATEMENT_NEWBENEFITSTATUS","//*[@id=\"outerForm:newBenefitStatus\"]","Premium Paying","TGTYPESCREENREG");

		try { 
		 WebElement terminationDate =driver.findElement(By.xpath("//input[@id='outerForm:terminationDate_input']"));
		terminationDate.clear();
		 
		 ;} 
		 catch(Exception e) { PrintVal("Renderer Script Exception : "+e.toString()); } 
		 PrintVal("Executed Step = CALLRENDERERSCRIPT,CLEARTERMINATIONDATEFIELD"); 
        TAP.$("ELE_REINSTATEMENT_SUBMITBUTTON","//*[@id=\"outerForm:Submit\"]","TGTYPESCREENREG");

        }
          LOGGER.info("Executed Step = ENDIF,TGTYPESCREENREG");
        TAP.$("ELE_REINSTATEMENT_CANCELBUTTON","//*[@id=\"outerForm:Cancel\"]","TGTYPESCREENREG");

        TAP.$("ELE_REINSTATEMENT_CONTINUEBUTTON","//*[@id=\"outerForm:Continue\"]","TGTYPESCREENREG");

		String var_paidToDate;
                 LOGGER.info("Executed Step = VAR,String,var_paidToDate,TGTYPESCREENREG");
		var_paidToDate = ActionWrapper.getElementValue("ELE_GETPAIDTODATE", "//span[@id='outerForm:paidToDate_input']");
                 LOGGER.info("Executed Step = STORE,var_paidToDate,ELE_GETPAIDTODATE,TGTYPESCREENREG");
        TYPE.$("ELE_BILLTODATE","//input[@id='outerForm:billToDate_input']",var_paidToDate,"FALSE","TGTYPESCREENREG");

        TAP.$("ELE_REINSTATEMENT_SUBMITBUTTON","//*[@id=\"outerForm:Submit\"]","TGTYPESCREENREG");

        TAP.$("ELE_REINSTATEMENT_SKIPSTEP1","//*[@id=\"outerForm:SkipStep\"]","TGTYPESCREENREG");

        TAP.$("ELE_REINSTATEMENT_SKIPSTEP1","//*[@id=\"outerForm:SkipStep\"]","TGTYPESCREENREG");

        TAP.$("ELE_REINSTATEMENT_SKIPSTEP1","//*[@id=\"outerForm:SkipStep\"]","TGTYPESCREENREG");

        TAP.$("ELE_REINSTATEMENT_SKIPSTEP1","//*[@id=\"outerForm:SkipStep\"]","TGTYPESCREENREG");

        TAP.$("ELE_REINSTATEMENT_SKIPSTEP1","//*[@id=\"outerForm:SkipStep\"]","TGTYPESCREENREG");

        TAP.$("ELE_REINSTATEMENT_SKIPSTEP1","//*[@id=\"outerForm:SkipStep\"]","TGTYPESCREENREG");

        TAP.$("ELE_REINSTATEMENT_SKIPSTEP1","//*[@id=\"outerForm:SkipStep\"]","TGTYPESCREENREG");

        TAP.$("ELE_REINSTATEMENT_SKIPSTEP1","//*[@id=\"outerForm:SkipStep\"]","TGTYPESCREENREG");

        TAP.$("ELE_REINSTATEMENT_SUBMITBUTTON","//*[@id=\"outerForm:Submit\"]","TGTYPESCREENREG");

        ASSERT.$("ELE_SUCCESSMESSAGE","//li[@class='MessageInfo']","CONTAINS","Successfully","TGTYPESCREENREG");

        CALL.$("PaymentToPreventLapse","TGTYPESCREENREG");

        HOVER.$("ELE_PAYEMENTMENU","//td[@class='ThemePanelMainFolderText'][contains(text(),'Payments')]","TGTYPESCREENREG");

        TAP.$("ELE_POLICYSUBMENU","//div[@id='cmSubMenuID24']//tbody//tr[7]//td[2]","TGTYPESCREENREG");

        TYPE.$("ELE_REINSTATEMENT_POLICYNUMBER","//*[@id=\"outerForm:policyNumber\"]",var_PolicyNumber,"FALSE","TGTYPESCREENREG");

		int var_minimumBalance;
                 LOGGER.info("Executed Step = VAR,Integer,var_minimumBalance,TGTYPESCREENREG");
		int var_suspenseBalance;
                 LOGGER.info("Executed Step = VAR,Integer,var_suspenseBalance,TGTYPESCREENREG");
		var_minimumBalance = ActionWrapper.getElementValueForVariableWithInt("ELE_MINIMUMBALANCE", "//span[@id='outerForm:minimumRequiredPrem']");
                 LOGGER.info("Executed Step = STORE,var_minimumBalance,ELE_MINIMUMBALANCE,TGTYPESCREENREG");
		var_suspenseBalance = ActionWrapper.getElementValueForVariableWithInt("ELE_SUSPENSEBALANCE", "//span[@id='outerForm:suspenseAmount']");
                 LOGGER.info("Executed Step = STORE,var_suspenseBalance,ELE_SUSPENSEBALANCE,TGTYPESCREENREG");
        LOGGER.info("Executed Step = START IF");
        if (check(var_suspenseBalance,">=",var_minimumBalance,"TGTYPESCREENREG")) {
         LOGGER.info("Executed Step = IF,var_suspenseBalance,>=,var_minimumBalance,TGTYPESCREENREG");
        TAP.$("ELE_REINSTATEMENT_SUBMITBUTTON","//*[@id=\"outerForm:Submit\"]","TGTYPESCREENREG");

        ASSERT.$("ELE_SUCCESSMESSAGE","//li[@class='MessageInfo']","CONTAINS","Request completed successfully","TGTYPESCREENREG");

        }
          LOGGER.info("Executed Step = ENDIF,TGTYPESCREENREG");
        CALL.$("CrossCheckTheReinstatedPolicy","TGTYPESCREENREG");

        HOVER.$("ELE_INQUIRYMENU","//div[@id='outerForm_menu_menu']//tbody//tr[6]//td[@class='ThemePanelMainFolderText']","TGTYPESCREENREG");

        TAP.$("ELE_QUICKINQUIRYSUBMENU","//td[contains(text(),'Quick Inquiry')]","TGTYPESCREENREG");

        TYPE.$("ELE_REINSTATEMENT_POLICYNUMBER","//*[@id=\"outerForm:policyNumber\"]",var_PolicyNumber,"FALSE","TGTYPESCREENREG");

        TAP.$("ELE_REINSTATEMENT_CONTINUEBUTTON","//*[@id=\"outerForm:Continue\"]","TGTYPESCREENREG");

		String var_PolicyStatus;
                 LOGGER.info("Executed Step = VAR,String,var_PolicyStatus,TGTYPESCREENREG");
		var_PolicyStatus = ActionWrapper.getElementValue("ELE_POLICYINQUIRYSTATUS", "//span[@id='outerForm:statusText']");
                 LOGGER.info("Executed Step = STORE,var_PolicyStatus,ELE_POLICYINQUIRYSTATUS,TGTYPESCREENREG");
        ASSERT.$(var_PolicyStatus,"CONTAINS","Premium Paying","TGTYPESCREENREG");

		var_Count = var_Count + ( 1 );
                 LOGGER.info("Executed Step = INCREASEVAR,var_Count,1,TGTYPESCREENREG");
    	}
          LOGGER.info("Executed Step = WEND,TGTYPESCREENREG");
    	resetAttemps();

    }


}
