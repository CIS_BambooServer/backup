package appium.tgo;

import com.tg.octopus.base.TGOWrapper;
import com.tg.octopus.libs.testng.listener.TestListener;
import static com.tg.octopus.base.TGOWrapper.Action.*;
import static com.tg.octopus.base.TGOWrapper.LOGGER;
import com.tg.octopus.wrapper.ActionWrapper;

import com.jayway.jsonpath.JsonPath;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.SessionNotCreatedException;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.Augmenter;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.remote.UnreachableBrowserException;

import org.testng.Assert;
import org.testng.ITestContext;
import org.testng.ITestResult;
import org.testng.Reporter;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;
import java.net.HttpURLConnection;
import java.net.URL;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import org.json.JSONArray;
import org.json.JSONObject;
import helper.WebTestUtility;
import java.sql.*;
import com.testautomationguru.utility.PDFUtil;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.multipdf.Splitter;

import java.util.*;
import org.openqa.selenium.*;
import java.io.*;
import java.net.*;

import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;


@Listeners(TestListener.class)
public class  BaseGS36_RegSuite extends TGOWrapper {



    static {
       needsresetoncebeforetest ="false";
       needsresetbeforeeverymethod ="true";
       captureverystepsnap ="true";
       appendlastsnap ="false";
       ishybridapp ="true";

    }


    @Test
    public void tc14loanprocessingapplyforloan() {

        // No need to declare / print method name here, handled by TestNg listener
        // declare("tc14loanprocessingapplyforloan");

        CALL.$("LoginToGIAS36","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_USERNAME","outerForm:usernameTGWEBCOMMAIdTGWEBCOMMA0","GIAS36ILC","FALSE","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_PASSWORD","outerForm:passwordTGWEBCOMMAIdTGWEBCOMMA0","Automation","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_LOGIN","outerForm:loginButtonTGWEBCOMMAIdTGWEBCOMMA0","TGTYPESCREENREG");

		int var_Count;
                 LOGGER.info("Executed Step = VAR,Integer,var_Count,TGTYPESCREENREG");
		var_Count = 0;
                 LOGGER.info("Executed Step = STORE,var_Count,0,TGTYPESCREENREG");
		com.jayway.jsonpath.ReadContext var_CSVData = getJsonData("C:/GIAS_Automation/3.6 BaseRegression/InputData/36Reg_Loanprocessing.csv");
                 LOGGER.info("Executed Step = VAR,Json,var_CSVData,C:/GIAS_Automation/3.6 BaseRegression/InputData/36Reg_Loanprocessing.csv,TGTYPESCREENREG");
        LOGGER.info("Executed Step = REPEAT IF");
    	while (check(var_Count,"<",1,"TGTYPESCREENREG")) {
          LOGGER.info("Executed Step = WHILE,var_Count,<,1,TGTYPESCREENREG");
		String var_PolicyNumber;
                 LOGGER.info("Executed Step = VAR,String,var_PolicyNumber,TGTYPESCREENREG");
		var_PolicyNumber = getJsonData(var_CSVData , "$.records["+var_Count+"].PolicyNumber");
                 LOGGER.info("Executed Step = STORE,var_PolicyNumber,var_CSVData,$.records[{var_Count}].PolicyNumber,TGTYPESCREENREG");
        CALL.$("ProcessTheLoanTransaction","TGTYPESCREENREG");

        HOVER.$("ELE_MENU_POLICYOWNERSERVICES","//td[@class='ThemePanelMainFolderText'][contains(text(),'Policyowner Services')]","TGTYPESCREENREG");

        HOVER.$("ELE_SUBMEN_LOANLIEN","//div[@id='cmSubMenuID38']//tbody//tr[4]//td[2]","TGTYPESCREENREG");

        TAP.$("ELE_SUBMEN_POLICYOWNERSERVICESLOAN","//div[@id='cmSubMenuID41']//tbody//tr[2]//td[2]","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_POLICYNUMBER","//input[@type='text'][@name='outerForm:policyNumber']",var_PolicyNumber,"FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_CONTINUE","//input[@type='submit'][@name='outerForm:Continue']","TGTYPESCREENREG");

		String var_RequestLoanAmt;
                 LOGGER.info("Executed Step = VAR,String,var_RequestLoanAmt,TGTYPESCREENREG");
		var_RequestLoanAmt = ActionWrapper.getElementValue("ELE_GETVAL_MAXIMUMAVAILABLENETLOAN", "//span[@id='outerForm:maximumAvailableNetLoan']");
                 LOGGER.info("Executed Step = STORE,var_RequestLoanAmt,ELE_GETVAL_MAXIMUMAVAILABLENETLOAN,TGTYPESCREENREG");
        TAP.$("ELE_RDOBTN_MAXLOANREQUESTNO","//input[@id='outerForm:maximumLoanRequest:0']","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_REQUESTEDLOANAMOUNT","//input[@type='text'][@name='outerForm:loanRequest']",var_RequestLoanAmt,"FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_QUOTE","//input[@type='submit'][@name='outerForm:Quote']","TGTYPESCREENREG");

        ASSERT.$("ELE_GETMSG_WARN","//li[@class='MessageWarn']","CONTAINS","DISTRIBUTION_INFO_MUST_BE_CONFIRMED","TGTYPESCREENREG");

        CALL.$("CompleteDistributionRequest","TGTYPESCREENREG");

        TAP.$("ELE_LINK_DISTRIBUTIONINFORMATION","//span[@id='outerForm:DistributionInformationText']","TGTYPESCREENREG");

        SELECT.$("ELE_DRPDWN_DISBURSEMENTCURRENCY","//select[@id='outerForm:disbursementCurrency']","Dollars [US]","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_CONTINUE","//input[@type='submit'][@name='outerForm:Continue']","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SUBMIT","//input[@type='submit'][@name='outerForm:Submit']","TGTYPESCREENREG");

        ASSERT.$("ELE_GETMSG_SUCCESS","//li[@class='MessageInfo']","CONTAINS","Request completed successfully","TGTYPESCREENREG");

		var_Count = var_Count + ( 1 );
                 LOGGER.info("Executed Step = INCREASEVAR,var_Count,1,TGTYPESCREENREG");
    	}
          LOGGER.info("Executed Step = WEND,TGTYPESCREENREG");
    	resetAttemps();

    }


    @Test
    public void tc17reinstatementofpolicy() {

        // No need to declare / print method name here, handled by TestNg listener
        // declare("tc17reinstatementofpolicy");

        CALL.$("LoginToGIAS36","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_USERNAME","outerForm:usernameTGWEBCOMMAIdTGWEBCOMMA0","GIAS36ILC","FALSE","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_PASSWORD","outerForm:passwordTGWEBCOMMAIdTGWEBCOMMA0","Automation","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_LOGIN","outerForm:loginButtonTGWEBCOMMAIdTGWEBCOMMA0","TGTYPESCREENREG");

		int var_Count;
                 LOGGER.info("Executed Step = VAR,Integer,var_Count,TGTYPESCREENREG");
		var_Count = 0;
                 LOGGER.info("Executed Step = STORE,var_Count,0,TGTYPESCREENREG");
		com.jayway.jsonpath.ReadContext var_CSVData = getJsonData("C:/GIAS_Automation/3.6 BaseRegression/InputData/36Reg_Reinstatement.csv");
                 LOGGER.info("Executed Step = VAR,Json,var_CSVData,C:/GIAS_Automation/3.6 BaseRegression/InputData/36Reg_Reinstatement.csv,TGTYPESCREENREG");
		String var_PolicyNumber;
                 LOGGER.info("Executed Step = VAR,String,var_PolicyNumber,TGTYPESCREENREG");
        LOGGER.info("Executed Step = REPEAT IF");
    	while (check(var_Count,"<",1,"TGTYPESCREENREG")) {
          LOGGER.info("Executed Step = WHILE,var_Count,<,1,TGTYPESCREENREG");
		var_PolicyNumber = getJsonData(var_CSVData , "$.records["+var_Count+"].PolicyNumber");
                 LOGGER.info("Executed Step = STORE,var_PolicyNumber,var_CSVData,$.records[{var_Count}].PolicyNumber,TGTYPESCREENREG");
        CALL.$("PolicyReinstatementEligibility","TGTYPESCREENREG");

        HOVER.$("ELE_MENU_INQUIRY","//td[text()='Inquiry']","TGTYPESCREENREG");

        TAP.$("ELE_SUBMEN_QUICKINQUIRY","//td[contains(text(),'Quick Inquiry')]","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_POLICYNUMBER","//input[@type='text'][@name='outerForm:policyNumber']",var_PolicyNumber,"FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_CONTINUE","//input[@type='submit'][@name='outerForm:Continue']","TGTYPESCREENREG");

		writeToCSV ("Status " , ActionWrapper.getElementValue("ELE_GETVAL_STATUS", "//span[@id='outerForm:statusText']"));
                 LOGGER.info("Executed Step = WRITETOCSV,ELE_GETVAL_STATUS,Status,TGTYPESCREENREG");
		writeToCSV ("Policy Number " , ActionWrapper.getElementValue("ELE_GETVAL_POLICYNUMBER", "//span[@id='outerForm:policyNumber']"));
                 LOGGER.info("Executed Step = WRITETOCSV,ELE_GETVAL_POLICYNUMBER,Policy Number,TGTYPESCREENREG");
		String var_Status;
                 LOGGER.info("Executed Step = VAR,String,var_Status,TGTYPESCREENREG");
		var_Status = ActionWrapper.getElementValue("ELE_GETVAL_STATUS", "//span[@id='outerForm:statusText']");
                 LOGGER.info("Executed Step = STORE,var_Status,ELE_GETVAL_STATUS,TGTYPESCREENREG");
        LOGGER.info("Executed Step = START IF");
        if (check(var_Status,"CONTAINS","lapsed","TGTYPESCREENREG")) {
         LOGGER.info("Executed Step = IF,var_Status,CONTAINS,lapsed,TGTYPESCREENREG");
        CALL.$("CheckBaseandRriderBenefits","TGTYPESCREENREG");

        TAP.$("ELE_LINK_POLICYINQUIRY","//span[@id='outerForm:PolicyInquiryText']","TGTYPESCREENREG");

        TAP.$("ELE_LINK_BENEFITS","//span[text()='Benefits']","TGTYPESCREENREG");

        TAP.$("ELE_LINK_BASE","//span[contains(text(),'Base')]","TGTYPESCREENREG");

        LOGGER.info("Executed Step = START IF");
        if (check("ELE_LINK_SUPPLEMENTALBENEFIT","//span[@id='outerForm:SupplementalBenefitText']","VISIBLE","TGTYPESCREENREG")) {
         LOGGER.info("Executed Step = IF,ELE_LINK_SUPPLEMENTALBENEFIT,VISIBLE,TGTYPESCREENREG");
        TAP.$("ELE_LINK_SUPPLEMENTALBENEFIT","//span[@id='outerForm:SupplementalBenefitText']","TGTYPESCREENREG");

        }
          LOGGER.info("Executed Step = ENDIF,TGTYPESCREENREG");
        }
          LOGGER.info("Executed Step = ENDIF,TGTYPESCREENREG");
        CALL.$("ReinstatingTheEligiblePolicy","TGTYPESCREENREG");

        HOVER.$("ELE_MENU_POLICYOWNERSERVICES","//td[@class='ThemePanelMainFolderText'][contains(text(),'Policyowner Services')]","TGTYPESCREENREG");

        HOVER.$("ELE_SUBMEN_BENEFITMAINTENANCE","//td[text()='Benefit Maintenance']","TGTYPESCREENREG");

        TAP.$("ELE_SUBMEN_REINSTATEMENT","//td[@class='ThemePanelMenuItemText'][contains(text(),'Reinstatement')]","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_POLICYNUMBER","//input[@type='text'][@name='outerForm:policyNumber']",var_PolicyNumber,"FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_CONTINUE","//input[@type='submit'][@name='outerForm:Continue']","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_CONTINUE","//input[@type='submit'][@name='outerForm:Continue']","TGTYPESCREENREG");

		try { 
		 //List<WebElement> baseRider= driver.findElements(By.xpath("//span[contains(@id,'issueAgeOut2')]"));
							//int baseRiderCount=  baseRider.size();
		java.util.List<org.openqa.selenium.WebElement> base = driver.findElements(org.openqa.selenium.By.xpath("//span[contains(text(),'Base')]"));

		java.util.List<org.openqa.selenium.WebElement> rider= driver.findElements(org.openqa.selenium.By.xpath("//span[contains(text(),'Rider')]"));
			
		int baseRiderCount =  base.size() + rider.size();
						System.out.println("BaseRiderCount is  " + baseRiderCount); 
							if( baseRiderCount> 0) {
									for(int i=0; i< baseRiderCount;i++)
									{
														System.out.println("i is  " + i); 
														Thread.sleep(4000);

					driver.findElement(By.xpath("//span[@id='outerForm:grid:" + i + ":baseRiderCodeOut1']")).click();

		// Add Screenshot will only work for Android platform

		takeScreenshot();

					org.openqa.selenium.support.ui.Select newStatusChange = new org.openqa.selenium.support.ui.Select(driver.findElement(By.xpath("//select[@id='outerForm:newBenefitStatus']")));
			newStatusChange.selectByVisibleText("Premium Paying");
		WebElement terminationDate =driver.findElement(By.xpath("//input[@id='outerForm:terminationDate_input']"));

			terminationDate.clear();
		// Add Screenshot will only work for Android platform

		takeScreenshot();


		driver.findElement(By.xpath("//input[@id='outerForm:Submit']")).click();
		// Add Screenshot will only work for Android platform

		takeScreenshot();

					//			driver.findElement(By.xpath("//input[@id='outerForm:Cancel']")).click();
						//Thread.sleep(4000);
									//driver.findElement(By.xpath("//input[@id='outerForm:']")).click();
										System.out.println("next value of i is  " + i); 

						}
							}
		 
		 ;} 
		 catch(Exception e) { PrintVal("Renderer Script Exception : "+e.toString()); } 
		 PrintVal("Executed Step = CALLRENDERERSCRIPT,COUNTREINSTATEMENTBENEFITSANDSUBMIT"); 
        TAP.$("ELE_BUTTON_CANCEL","//input[@type='submit'][@name='outerForm:Cancel']","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_CONTINUE","//input[@type='submit'][@name='outerForm:Continue']","TGTYPESCREENREG");

		String var_paidToDate;
                 LOGGER.info("Executed Step = VAR,String,var_paidToDate,TGTYPESCREENREG");
		var_paidToDate = ActionWrapper.getElementValue("ELE_GETVAL_PAIDTODATE", "//span[@id='outerForm:paidToDate_input']");
                 LOGGER.info("Executed Step = STORE,var_paidToDate,ELE_GETVAL_PAIDTODATE,TGTYPESCREENREG");
        TYPE.$("ELE_TXTBOX_BILLTODATE","//input[@id='outerForm:billToDate_input']",var_paidToDate,"FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SUBMIT","//input[@type='submit'][@name='outerForm:Submit']","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_FINISH","//input[@id='outerForm:Finish']","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SUBMIT","//input[@type='submit'][@name='outerForm:Submit']","TGTYPESCREENREG");

        ASSERT.$("ELE_GETMSG_SUCCESS","//li[@class='MessageInfo']","CONTAINS","successfully","TGTYPESCREENREG");

        CALL.$("PaymentToPreventLapse","TGTYPESCREENREG");

		int var_MinimumBalance;
                 LOGGER.info("Executed Step = VAR,Integer,var_MinimumBalance,TGTYPESCREENREG");
		int var_suspenseBalance;
                 LOGGER.info("Executed Step = VAR,Integer,var_suspenseBalance,TGTYPESCREENREG");
        HOVER.$("ELE_MENU_PAYEMENTS","//td[@class='ThemePanelMainFolderText'][contains(text(),'Payments')]","TGTYPESCREENREG");

        TAP.$("ELE_SUBMEN_PAYMENTSPOLICY","//div[@id='cmSubMenuID24']//td[@class='ThemePanelMenuItemText'][contains(text(),'Policy')]","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_POLICYNUMBER","//input[@type='text'][@name='outerForm:policyNumber']",var_PolicyNumber,"FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_CONTINUE","//input[@type='submit'][@name='outerForm:Continue']","TGTYPESCREENREG");

		var_MinimumBalance = ActionWrapper.getElementValueForVariableWithInt("ELE_GETVAL_MINIMUMBALANCE", "//span[@id='outerForm:minimumRequiredPrem']");
                 LOGGER.info("Executed Step = STORE,var_MinimumBalance,ELE_GETVAL_MINIMUMBALANCE,TGTYPESCREENREG");
		var_suspenseBalance = ActionWrapper.getElementValueForVariableWithInt("ELE_GETVAL_SUSPENSEAMOUNT", "//span[@id='outerForm:suspenseAmount']");
                 LOGGER.info("Executed Step = STORE,var_suspenseBalance,ELE_GETVAL_SUSPENSEAMOUNT,TGTYPESCREENREG");
        LOGGER.info("Executed Step = START IF");
        if (check(var_suspenseBalance,">=",var_MinimumBalance,"TGTYPESCREENREG")) {
         LOGGER.info("Executed Step = IF,var_suspenseBalance,>=,var_MinimumBalance,TGTYPESCREENREG");
        TAP.$("ELE_BUTTON_SUBMIT","//input[@type='submit'][@name='outerForm:Submit']","TGTYPESCREENREG");

        ASSERT.$("ELE_GETMSG_SUCCESS","//li[@class='MessageInfo']","CONTAINS","Request completed successfully","TGTYPESCREENREG");

        }
         else {
          LOGGER.info("Executed Step = ELSE,TGTYPESCREENREG");
        CALL.$("fillAddSuspenseDetailsForm","TGTYPESCREENREG");

        HOVER.$("ELE_MENU_PAYMENTS","//td[@class='ThemePanelMainFolderText'][contains(text(),'Payments')]","TGTYPESCREENREG");

        TAP.$("ELE_SUBMEN_SUSPENSE","//div[@id='cmSubMenuID24']//td[@class='ThemePanelMenuItemText'][contains(text(),'Suspense')]","TGTYPESCREENREG");

        TAP.$("ELE_RNDBTN_ADD","//img[@id='outerForm:gnAddImage']","TGTYPESCREENREG");

        SELECT.$("ELE_DRPDWN_CURRENCYCODE","//select[@id='outerForm:currencyCode']","Dollars [US]","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_SUSPENSECONTROLNUMBER","//input[@id='outerForm:suspenseControlNumb']",var_PolicyNumber,"FALSE","TGTYPESCREENREG");

        SELECT.$("ELE_DRPDWN_SUSPENSETYPE","//select[@id='outerForm:suspenseType']","Premium suspense","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_SUSPENSEAMOUNTOFCHANGE","//input[@id='outerForm:amountOfChange']",var_MinimumBalance,"TRUE","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_SUSPENSECOMMENT","//input[@id='outerForm:suspenseComment']","Premium suspense","FALSE","TGTYPESCREENREG");

        SELECT.$("ELE_DRPDWN_SUSPENSEACCOUNTNUMBER","//select[@id='outerForm:suspenseAccountNumb']","153000 - CASH","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SUBMIT","//input[@type='submit'][@name='outerForm:Submit']","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_CONFIRMSUBMIT2","//input[@id='outerForm:ConfirmSubmit']","TGTYPESCREENREG");

        CALL.$("MakePayment","TGTYPESCREENREG");

        HOVER.$("ELE_MENU_PAYMENTS","//td[@class='ThemePanelMainFolderText'][contains(text(),'Payments')]","TGTYPESCREENREG");

        TAP.$("ELE_SUBMEN_PAYMENTSPOLICY","//div[@id='cmSubMenuID24']//td[@class='ThemePanelMenuItemText'][contains(text(),'Policy')]","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_POLICYNUMBER","//input[@type='text'][@name='outerForm:policyNumber']",var_PolicyNumber,"FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_CONTINUE","//input[@type='submit'][@name='outerForm:Continue']","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SUBMIT","//input[@type='submit'][@name='outerForm:Submit']","TGTYPESCREENREG");

        ASSERT.$("ELE_GETMSG_SUCCESS","//li[@class='MessageInfo']","CONTAINS","successfully","TGTYPESCREENREG");

        }
          LOGGER.info("Executed Step = ENDIF,TGTYPESCREENREG");
        ASSERT.$("ELE_GETMSG_SUCCESS","//li[@class='MessageInfo']","CONTAINS","successfully","TGTYPESCREENREG");

        CALL.$("CrossCheckTheReinstatedPolicy","TGTYPESCREENREG");

        HOVER.$("ELE_MENU_INQUIRY","//td[text()='Inquiry']","TGTYPESCREENREG");

        TAP.$("ELE_SUBMEN_QUICKINQUIRY","//td[contains(text(),'Quick Inquiry')]","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_POLICYNUMBER","//input[@type='text'][@name='outerForm:policyNumber']",var_PolicyNumber,"FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_CONTINUE","//input[@type='submit'][@name='outerForm:Continue']","TGTYPESCREENREG");

        ASSERT.$("ELE_GETVAL_STATUS","//span[@id='outerForm:statusText']","CONTAINS","Premium Paying","TGTYPESCREENREG");

		writeToCSV ("Policy Number " , ActionWrapper.getElementValue("ELE_GETVAL_POLICYNUMBER", "//span[@id='outerForm:policyNumber']"));
                 LOGGER.info("Executed Step = WRITETOCSV,ELE_GETVAL_POLICYNUMBER,Policy Number,TGTYPESCREENREG");
		writeToCSV ("Status " , ActionWrapper.getElementValue("ELE_GETVAL_STATUS", "//span[@id='outerForm:statusText']"));
                 LOGGER.info("Executed Step = WRITETOCSV,ELE_GETVAL_STATUS,Status,TGTYPESCREENREG");
		var_Count = var_Count + ( 1 );
                 LOGGER.info("Executed Step = INCREASEVAR,var_Count,1,TGTYPESCREENREG");
    	}
          LOGGER.info("Executed Step = WEND,TGTYPESCREENREG");
    	resetAttemps();

    }


    @Test
    public void tc08freelook() {

        // No need to declare / print method name here, handled by TestNg listener
        // declare("tc08freelook");

        CALL.$("LoginToGIAS36","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_USERNAME","outerForm:usernameTGWEBCOMMAIdTGWEBCOMMA0","GIAS36ILC","FALSE","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_PASSWORD","outerForm:passwordTGWEBCOMMAIdTGWEBCOMMA0","Automation","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_LOGIN","outerForm:loginButtonTGWEBCOMMAIdTGWEBCOMMA0","TGTYPESCREENREG");

		int var_Count = 0;
                 LOGGER.info("Executed Step = VAR,Integer,var_Count,0,TGTYPESCREENREG");
        LOGGER.info("Executed Step = REPEAT IF");
    	while (check(var_Count,"<",1,"TGTYPESCREENREG")) {
          LOGGER.info("Executed Step = WHILE,var_Count,<,1,TGTYPESCREENREG");
		com.jayway.jsonpath.ReadContext var_CSVData = getJsonData("C:/GIAS_Automation/3.6 BaseRegression/InputData/36Reg_FreeLookPolicy.csv");
                 LOGGER.info("Executed Step = VAR,Json,var_CSVData,C:/GIAS_Automation/3.6 BaseRegression/InputData/36Reg_FreeLookPolicy.csv,TGTYPESCREENREG");
		String var_PolicyNumber;
                 LOGGER.info("Executed Step = VAR,String,var_PolicyNumber,TGTYPESCREENREG");
		var_PolicyNumber = getJsonData(var_CSVData , "$.records["+var_Count+"].PolicyNumber");
                 LOGGER.info("Executed Step = STORE,var_PolicyNumber,var_CSVData,$.records[{var_Count}].PolicyNumber,TGTYPESCREENREG");
        CALL.$("FreeLookPaymentReversed","TGTYPESCREENREG");

        HOVER.$("ELE_MENU_POLICYOWNERSERVICES","//td[@class='ThemePanelMainFolderText'][contains(text(),'Policyowner Services')]","TGTYPESCREENREG");

        HOVER.$("ELE_SUBMEN_BENEFITMAINTENANCE","//td[text()='Benefit Maintenance']","TGTYPESCREENREG");

        TAP.$("ELE_SUBMEN_BENEFITSTATUSCHANGE","//td[contains(text(),'Benefit Status Change')]","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_POLICYNUMBER","//input[@type='text'][@name='outerForm:policyNumber']",var_PolicyNumber,"TRUE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_CONTINUE","//input[@type='submit'][@name='outerForm:Continue']","TGTYPESCREENREG");

		int var_premiumPayingStatusCount = 0;
                 LOGGER.info("Executed Step = VAR,Integer,var_premiumPayingStatusCount,0,TGTYPESCREENREG");
		try { 
		 java.util.List<WebElement> premiumPayingStatus = driver.findElements(By.xpath("//span[contains(text(),'Premium Paying')]"));
		var_premiumPayingStatusCount = premiumPayingStatus.size();
		 
		 ;} 
		 catch(Exception e) { PrintVal("Renderer Script Exception : "+e.toString()); } 
		 PrintVal("Executed Step = CALLRENDERERSCRIPT,PREMIUMPAYINGSTATUSCOUNT"); 
        ASSERT.$(var_premiumPayingStatusCount,">=",1,"TGTYPESCREENREG");

        TAP.$("ELE_LINK_REINSTATEMENTBASE","//span[@id='outerForm:grid:0:baseRiderCodeOut1']","TGTYPESCREENREG");

        SELECT.$("ELE_DRPDWN_NEWBENEFITSTATUS","//select[@id='outerForm:newBenefitStatus']","Not Taken","TGTYPESCREENREG");

		String var_effectiveDate = "null";
                 LOGGER.info("Executed Step = VAR,String,var_effectiveDate,null,TGTYPESCREENREG");
		var_effectiveDate = ActionWrapper.getElementValue("ELE_GETVAL_EFFECTIVEDATE", "//span[@id='outerForm:effectiveDate_input']");
                 LOGGER.info("Executed Step = STORE,var_effectiveDate,ELE_GETVAL_EFFECTIVEDATE,TGTYPESCREENREG");
        TYPE.$("ELE_TXTBOX_TERMINATIONDATE","//input[@type='text'][@name='outerForm:terminationDate']",var_effectiveDate,"TRUE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_EDIT","//input[@id='outerForm:Edit']","TGTYPESCREENREG");

        TAP.$("ELE_LINK_DISTRIBUTIONINFORMATION","//span[@id='outerForm:DistributionInformationText']","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_CONTINUE","//input[@type='submit'][@name='outerForm:Continue']","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SUBMIT","//input[@type='submit'][@name='outerForm:Submit']","TGTYPESCREENREG");

		int var_freeLookupNotTakenCount = 0;
                 LOGGER.info("Executed Step = VAR,Integer,var_freeLookupNotTakenCount,0,TGTYPESCREENREG");
		try { 
		 java.util.List<WebElement> freeLookupBenefitStatus = driver.findElements(By.xpath("//span[contains(text(),'Not Taken')]"));
		var_freeLookupNotTakenCount = freeLookupBenefitStatus.size();
		 
		 ;} 
		 catch(Exception e) { PrintVal("Renderer Script Exception : "+e.toString()); } 
		 PrintVal("Executed Step = CALLRENDERERSCRIPT,STATUSNOTTAKENCOUNT"); 
        ASSERT.$(var_freeLookupNotTakenCount,">=",1,"TGTYPESCREENREG");

        ASSERT.$(var_freeLookupNotTakenCount,"=",var_premiumPayingStatusCount,"TGTYPESCREENREG");

        ASSERT.$("ELE_GETMSG_SUCCESS","//li[@class='MessageInfo']","CONTAINS","successfully","TGTYPESCREENREG");

        CALL.$("FreeLookInquiry","TGTYPESCREENREG");

        HOVER.$("ELE_MENU_INQUIRY","//td[text()='Inquiry']","TGTYPESCREENREG");

        TAP.$("ELE_SUBMEN_POLICYDETAIL","//td[text()='Policy Detail']","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_POLICYNUMBER","//input[@type='text'][@name='outerForm:policyNumber']",var_PolicyNumber,"TRUE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_CONTINUE","//input[@type='submit'][@name='outerForm:Continue']","TGTYPESCREENREG");

		writeToCSV ("Policy Number " , ActionWrapper.getElementValue("ELE_GETVAL_POLICYNUMBER", "//span[@id='outerForm:policyNumber']"));
                 LOGGER.info("Executed Step = WRITETOCSV,ELE_GETVAL_POLICYNUMBER,Policy Number,TGTYPESCREENREG");
		writeToCSV ("Status " , ActionWrapper.getElementValue("ELE_GETVAL_STATUS", "//span[@id='outerForm:statusText']"));
                 LOGGER.info("Executed Step = WRITETOCSV,ELE_GETVAL_STATUS,Status,TGTYPESCREENREG");
		writeToCSV ("Effective Date " , ActionWrapper.getElementValue("ELE_GETVAL_EFFECTIVEDATE", "//span[@id='outerForm:effectiveDate_input']"));
                 LOGGER.info("Executed Step = WRITETOCSV,ELE_GETVAL_EFFECTIVEDATE,Effective Date,TGTYPESCREENREG");
		var_Count = var_Count + ( 1 );
                 LOGGER.info("Executed Step = INCREASEVAR,var_Count,1,TGTYPESCREENREG");
    	}
          LOGGER.info("Executed Step = WEND,TGTYPESCREENREG");
    	resetAttemps();

    }


    @Test
    public void tc13benefitpayments() {

        // No need to declare / print method name here, handled by TestNg listener
        // declare("tc13benefitpayments");

        CALL.$("LoginToGIAS36","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_USERNAME","outerForm:usernameTGWEBCOMMAIdTGWEBCOMMA0","GIAS36ILC","FALSE","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_PASSWORD","outerForm:passwordTGWEBCOMMAIdTGWEBCOMMA0","Automation","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_LOGIN","outerForm:loginButtonTGWEBCOMMAIdTGWEBCOMMA0","TGTYPESCREENREG");

		com.jayway.jsonpath.ReadContext var_CSVData = getJsonData("C:/GIAS_Automation/3.6 BaseRegression/InputData/36Reg_BenefitPayment.csv");
                 LOGGER.info("Executed Step = VAR,Json,var_CSVData,C:/GIAS_Automation/3.6 BaseRegression/InputData/36Reg_BenefitPayment.csv,TGTYPESCREENREG");
		int var_Count = 0;
                 LOGGER.info("Executed Step = VAR,Integer,var_Count,0,TGTYPESCREENREG");
		String var_PolicyNumber;
                 LOGGER.info("Executed Step = VAR,String,var_PolicyNumber,TGTYPESCREENREG");
		int var_PayLevelPremium = 0;
                 LOGGER.info("Executed Step = VAR,Integer,var_PayLevelPremium,0,TGTYPESCREENREG");
		int var_InceptionPremiumReceived = 0;
                 LOGGER.info("Executed Step = VAR,Integer,var_InceptionPremiumReceived,0,TGTYPESCREENREG");
        LOGGER.info("Executed Step = REPEAT IF");
    	while (check(var_Count,"<",1,"TGTYPESCREENREG")) {
          LOGGER.info("Executed Step = WHILE,var_Count,<,1,TGTYPESCREENREG");
		var_PolicyNumber = getJsonData(var_CSVData , "$.records["+var_Count+"].PolicyNumber");
                 LOGGER.info("Executed Step = STORE,var_PolicyNumber,var_CSVData,$.records[{var_Count}].PolicyNumber,TGTYPESCREENREG");
        CALL.$("InquiryBenefitPayment","TGTYPESCREENREG");

        HOVER.$("ELE_MENU_INQUIRY","//td[text()='Inquiry']","TGTYPESCREENREG");

        TAP.$("ELE_SUBMEN_POLICYDETAIL","//td[text()='Policy Detail']","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_POLICYNUMBER","//input[@type='text'][@name='outerForm:policyNumber']",var_PolicyNumber,"TRUE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_CONTINUE","//input[@type='submit'][@name='outerForm:Continue']","TGTYPESCREENREG");

        TAP.$("ELE_LINK_BENEFITS","//span[text()='Benefits']","TGTYPESCREENREG");

        TAP.$("ELE_LINK_BASE","//span[contains(text(),'Base')]","TGTYPESCREENREG");

        TAP.$("ELE_LINK_COSTBASIS","//span[contains(text(),'Cost Basis')]","TGTYPESCREENREG");

		var_PayLevelPremium = ActionWrapper.getElementValueForVariableWithInt("ELE_GETVAL_7PAYPREMIUMAMOUNT", "//span[@id='outerForm:c7PayPremiumAmount']");
                 LOGGER.info("Executed Step = STORE,var_PayLevelPremium,ELE_GETVAL_7PAYPREMIUMAMOUNT,TGTYPESCREENREG");
		var_InceptionPremiumReceived = ActionWrapper.getElementValueForVariableWithInt("ELE_GETVAL_INCEPTIONTODATEPREMIUMRECEIVED", "//span[@id='outerForm:itdPremiumReceived']");
                 LOGGER.info("Executed Step = STORE,var_InceptionPremiumReceived,ELE_GETVAL_INCEPTIONTODATEPREMIUMRECEIVED,TGTYPESCREENREG");
		try { 
		 var_PayLevelPremium = (var_PayLevelPremium - var_InceptionPremiumReceived)/2;
		 
		 ;} 
		 catch(Exception e) { PrintVal("Renderer Script Exception : "+e.toString()); } 
		 PrintVal("Executed Step = CALLRENDERERSCRIPT,BENEFITPAYMENTCALCULATION"); 
        CALL.$("MakeBenefitPayment","TGTYPESCREENREG");

        HOVER.$("ELE_MENU_PAYMENTS","//td[@class='ThemePanelMainFolderText'][contains(text(),'Payments')]","TGTYPESCREENREG");

        TAP.$("ELE_SUBMEN_BENEFIT","//td[text()='Benefit']","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_POLICYNUMBER","//input[@type='text'][@name='outerForm:policyNumber']",var_PolicyNumber,"TRUE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_CONTINUE","//input[@type='submit'][@name='outerForm:Continue']","TGTYPESCREENREG");

        TAP.$("ELE_LINK_BASE","//span[contains(text(),'Base')]","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_AMOUNTTOAPPLY","//input[@id='outerForm:amountToApply']",var_PayLevelPremium,"TRUE","TGTYPESCREENREG");

		writeToCSV ("Policy Number " , ActionWrapper.getElementValue("ELE_GETVAL_POLICYNUMBER", "//span[@id='outerForm:policyNumber']"));
                 LOGGER.info("Executed Step = WRITETOCSV,ELE_GETVAL_POLICYNUMBER,Policy Number,TGTYPESCREENREG");
		writeToCSV ("Effective Date " , ActionWrapper.getElementValue("ELE_GETVAL_EFFECTIVEDATE", "//span[@id='outerForm:effectiveDate_input']"));
                 LOGGER.info("Executed Step = WRITETOCSV,ELE_GETVAL_EFFECTIVEDATE,Effective Date,TGTYPESCREENREG");
        TAP.$("ELE_BUTTON_SUBMIT","//input[@type='submit'][@name='outerForm:Submit']","TGTYPESCREENREG");

        ASSERT.$("ELE_GETMSG_SUCCESS","//li[@class='MessageInfo']","CONTAINS","successfully","TGTYPESCREENREG");

		var_Count = var_Count + ( 1 );
                 LOGGER.info("Executed Step = INCREASEVAR,var_Count,1,TGTYPESCREENREG");
    	}
          LOGGER.info("Executed Step = WEND,TGTYPESCREENREG");
    	resetAttemps();

    }


    @Test
    public void tc06facechangedecrease() {

        // No need to declare / print method name here, handled by TestNg listener
        // declare("tc06facechangedecrease");

        CALL.$("LoginToGIAS36","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_USERNAME","outerForm:usernameTGWEBCOMMAIdTGWEBCOMMA0","GIAS36ILC","FALSE","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_PASSWORD","outerForm:passwordTGWEBCOMMAIdTGWEBCOMMA0","Automation","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_LOGIN","outerForm:loginButtonTGWEBCOMMAIdTGWEBCOMMA0","TGTYPESCREENREG");

		int var_Count;
                 LOGGER.info("Executed Step = VAR,Integer,var_Count,TGTYPESCREENREG");
		var_Count = 0;
                 LOGGER.info("Executed Step = STORE,var_Count,0,TGTYPESCREENREG");
        LOGGER.info("Executed Step = REPEAT IF");
    	while (check(var_Count,"<",1,"TGTYPESCREENREG")) {
          LOGGER.info("Executed Step = WHILE,var_Count,<,1,TGTYPESCREENREG");
        CALL.$("FaceChangeDecrease","TGTYPESCREENREG");

		com.jayway.jsonpath.ReadContext var_CSVData = getJsonData("C:/GIAS_Automation/3.6 BaseRegression/InputData/36Reg_FaceChange.csv");
                 LOGGER.info("Executed Step = VAR,Json,var_CSVData,C:/GIAS_Automation/3.6 BaseRegression/InputData/36Reg_FaceChange.csv,TGTYPESCREENREG");
		String var_PolicyNumber;
                 LOGGER.info("Executed Step = VAR,String,var_PolicyNumber,TGTYPESCREENREG");
		var_PolicyNumber = getJsonData(var_CSVData , "$.records["+var_Count+"].PolicyNumber");
                 LOGGER.info("Executed Step = STORE,var_PolicyNumber,var_CSVData,$.records[{var_Count}].PolicyNumber,TGTYPESCREENREG");
		String var_ApplyDate;
                 LOGGER.info("Executed Step = VAR,String,var_ApplyDate,TGTYPESCREENREG");
		var_ApplyDate = getJsonData(var_CSVData , "$.records["+var_Count+"].ApplyDate");
                 LOGGER.info("Executed Step = STORE,var_ApplyDate,var_CSVData,$.records[{var_Count}].ApplyDate,TGTYPESCREENREG");
		String var_AmountofChange;
                 LOGGER.info("Executed Step = VAR,String,var_AmountofChange,TGTYPESCREENREG");
		var_AmountofChange = getJsonData(var_CSVData , "$.records["+var_Count+"].AmountofChange");
                 LOGGER.info("Executed Step = STORE,var_AmountofChange,var_CSVData,$.records[{var_Count}].AmountofChange,TGTYPESCREENREG");
        HOVER.$("ELE_MENU_POLICYOWNERSERVICES","//td[@class='ThemePanelMainFolderText'][contains(text(),'Policyowner Services')]","TGTYPESCREENREG");

        HOVER.$("ELE_SUBMEN_NONTRADITIONAL","//div[@id='cmSubMenuID38']//tbody//tr[7]//td[2]","TGTYPESCREENREG");

        TAP.$("ELE_SUBMEN_FACECHANGE","//td[text()='Face Change']","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_POLICYNUMBER","//input[@type='text'][@name='outerForm:policyNumber']",var_PolicyNumber,"FALSE","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_APPLYDATE","//input[@type='text'][@name='outerForm:promptDate']",var_ApplyDate,"FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_CONTINUE","//input[@type='submit'][@name='outerForm:Continue']","TGTYPESCREENREG");

        TAP.$("ELE_RDOBTN_DECREASEFACECHANGE","//input[@id='outerForm:increaseDecreaseIndicator:0']","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_FACECHANGEAMOUNT","//input[@type='text'][@name='outerForm:faceChangeAmount']",var_AmountofChange,"FALSE","TGTYPESCREENREG");

		writeToCSV("Policy Number " , var_PolicyNumber);
                 LOGGER.info("Executed Step = WRITETOCSV,var_PolicyNumber,Policy Number,TGTYPESCREENREG");
		writeToCSV("Apply Date " , var_ApplyDate);
                 LOGGER.info("Executed Step = WRITETOCSV,var_ApplyDate,Apply Date,TGTYPESCREENREG");
		writeToCSV("Amount of Change " , var_AmountofChange);
                 LOGGER.info("Executed Step = WRITETOCSV,var_AmountofChange,Amount of Change,TGTYPESCREENREG");
		writeToCSV ("Starting Face Amount " , ActionWrapper.getElementValue("ELE_GETVAL_FACECHANGECURRENTFACEAMOUNT", "//span[@id='outerForm:currentFaceAmountSpecified']"));
                 LOGGER.info("Executed Step = WRITETOCSV,ELE_GETVAL_FACECHANGECURRENTFACEAMOUNT,Starting Face Amount,TGTYPESCREENREG");
        TAP.$("ELE_BUTTON_EDIT","//input[@id='outerForm:Edit']","TGTYPESCREENREG");

        ASSERT.$("ELE_GETMSG_SUCCESS","//li[@class='MessageInfo']","=","Edit completed successfully with no errors.","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SUBMIT","//input[@type='submit'][@name='outerForm:Submit']","TGTYPESCREENREG");

        ASSERT.$("ELE_GETMSG_SUCCESS","//li[@class='MessageInfo']","=","Request completed successfully.","TGTYPESCREENREG");

        HOVER.$("ELE_MENU_INQUIRY","//td[text()='Inquiry']","TGTYPESCREENREG");

        TAP.$("ELE_SUBMEN_QUICKINQUIRY","//td[contains(text(),'Quick Inquiry')]","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_POLICYNUMBER","//input[@type='text'][@name='outerForm:policyNumber']",var_PolicyNumber,"FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_CONTINUE","//input[@type='submit'][@name='outerForm:Continue']","TGTYPESCREENREG");

        TAP.$("ELE_LINK_POLICYINQUIRY","//span[@id='outerForm:PolicyInquiryText']","TGTYPESCREENREG");

        TAP.$("ELE_LINK_TRANSACTIONHISTORY","//span[@id='outerForm:TransactionHistoryText']","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_CANCEL","//input[@type='submit'][@name='outerForm:Cancel']","TGTYPESCREENREG");

        TAP.$("ELE_LINK_BENEFITS","//span[text()='Benefits']","TGTYPESCREENREG");

        TAP.$("ELE_LINK_BASE","//span[contains(text(),'Base')]","TGTYPESCREENREG");

        TAP.$("ELE_LINK_BENEFITDISTRIBUTION","//span[text()='Benefit Distribution']","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_CANCEL","//input[@type='submit'][@name='outerForm:Cancel']","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_CANCEL","//input[@type='submit'][@name='outerForm:Cancel']","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_CANCEL","//input[@type='submit'][@name='outerForm:Cancel']","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_CANCEL","//input[@type='submit'][@name='outerForm:Cancel']","TGTYPESCREENREG");

		writeToCSV ("Ending Face Amount " , ActionWrapper.getElementValue("ELE_GETVAL_INQUIRYCURRENTFACEAMOUNT", "//span[@id='outerForm:grid:0:currentFaceAmountOut2']"));
                 LOGGER.info("Executed Step = WRITETOCSV,ELE_GETVAL_INQUIRYCURRENTFACEAMOUNT,Ending Face Amount,TGTYPESCREENREG");
		var_Count = var_Count + ( 1 );
                 LOGGER.info("Executed Step = INCREASEVAR,var_Count,1,TGTYPESCREENREG");
    	}
          LOGGER.info("Executed Step = WEND,TGTYPESCREENREG");
    	resetAttemps();

    }


    @Test
    public void tc05benefitchanges() {

        // No need to declare / print method name here, handled by TestNg listener
        // declare("tc05benefitchanges");

        CALL.$("LoginToGIAS36","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_USERNAME","outerForm:usernameTGWEBCOMMAIdTGWEBCOMMA0","GIAS36ILC","FALSE","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_PASSWORD","outerForm:passwordTGWEBCOMMAIdTGWEBCOMMA0","Automation","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_LOGIN","outerForm:loginButtonTGWEBCOMMAIdTGWEBCOMMA0","TGTYPESCREENREG");

		int var_Count;
                 LOGGER.info("Executed Step = VAR,Integer,var_Count,TGTYPESCREENREG");
		var_Count = 0;
                 LOGGER.info("Executed Step = STORE,var_Count,0,TGTYPESCREENREG");
        LOGGER.info("Executed Step = REPEAT IF");
    	while (check(var_Count,"<",1,"TGTYPESCREENREG")) {
          LOGGER.info("Executed Step = WHILE,var_Count,<,1,TGTYPESCREENREG");
		com.jayway.jsonpath.ReadContext var_CSVData = getJsonData("C:/GIAS_Automation/3.6 BaseRegression/InputData/36Reg_BenefitChanges.csv");
                 LOGGER.info("Executed Step = VAR,Json,var_CSVData,C:/GIAS_Automation/3.6 BaseRegression/InputData/36Reg_BenefitChanges.csv,TGTYPESCREENREG");
        CALL.$("BenefitChanges","TGTYPESCREENREG");

		String var_PolicyNumber;
                 LOGGER.info("Executed Step = VAR,String,var_PolicyNumber,TGTYPESCREENREG");
		var_PolicyNumber = getJsonData(var_CSVData , "$.records["+var_Count+"].PolicyNumber");
                 LOGGER.info("Executed Step = STORE,var_PolicyNumber,var_CSVData,$.records[{var_Count}].PolicyNumber,TGTYPESCREENREG");
        HOVER.$("ELE_MENU_INQUIRY","//td[text()='Inquiry']","TGTYPESCREENREG");

        TAP.$("ELE_SUBMEN_POLICYDETAIL","//td[text()='Policy Detail']","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_POLICYNUMBER","//input[@type='text'][@name='outerForm:policyNumber']",var_PolicyNumber,"FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_CONTINUE","//input[@type='submit'][@name='outerForm:Continue']","TGTYPESCREENREG");

        TAP.$("ELE_LINK_BENEFITS","//span[text()='Benefits']","TGTYPESCREENREG");

        TAP.$("ELE_LINK_BASE","//span[contains(text(),'Base')]","TGTYPESCREENREG");

		String var_BaseBenefitUnits;
                 LOGGER.info("Executed Step = VAR,String,var_BaseBenefitUnits,TGTYPESCREENREG");
		var_BaseBenefitUnits = ActionWrapper.getElementValue("ELE_GETVAL_BASEBENEFITUNITS", "//span[@id='outerForm:unitsOfInsurance']");
                 LOGGER.info("Executed Step = STORE,var_BaseBenefitUnits,ELE_GETVAL_BASEBENEFITUNITS,TGTYPESCREENREG");
		writeToCSV ("Initial Base Benefit Units " , ActionWrapper.getElementValue("ELE_GETVAL_BASEBENEFITUNITS", "//span[@id='outerForm:unitsOfInsurance']"));
                 LOGGER.info("Executed Step = WRITETOCSV,ELE_GETVAL_BASEBENEFITUNITS,Initial Base Benefit Units,TGTYPESCREENREG");
        TAP.$("ELE_LINK_SUPPLEMENTALBENEFIT","//span[@id='outerForm:SupplementalBenefitText']","TGTYPESCREENREG");

		String var_SupplementalBenefitUnits;
                 LOGGER.info("Executed Step = VAR,String,var_SupplementalBenefitUnits,TGTYPESCREENREG");
		var_SupplementalBenefitUnits = ActionWrapper.getElementValue("ELE_GETVAL_SUPPLEMENTALBENEFITUNITS", "//span[@id='outerForm:grid:0:unitsOfInsuranceOut2']");
                 LOGGER.info("Executed Step = STORE,var_SupplementalBenefitUnits,ELE_GETVAL_SUPPLEMENTALBENEFITUNITS,TGTYPESCREENREG");
		writeToCSV ("Initial Supplemental Benefit Units " , ActionWrapper.getElementValue("ELE_GETVAL_SUPPLEMENTALBENEFITUNITS", "//span[@id='outerForm:grid:0:unitsOfInsuranceOut2']"));
                 LOGGER.info("Executed Step = WRITETOCSV,ELE_GETVAL_SUPPLEMENTALBENEFITUNITS,Initial Supplemental Benefit Units,TGTYPESCREENREG");
        LOGGER.info("Executed Step = START IF");
        if (check(var_SupplementalBenefitUnits,">",var_BaseBenefitUnits,"TGTYPESCREENREG")) {
         LOGGER.info("Executed Step = IF,var_SupplementalBenefitUnits,>,var_BaseBenefitUnits,TGTYPESCREENREG");
        HOVER.$("ELE_MENU_POLICYOWNERSERVICES","//td[@class='ThemePanelMainFolderText'][contains(text(),'Policyowner Services')]","TGTYPESCREENREG");

        HOVER.$("ELE_SUBMEN_BENEFITMAINTENANCE","//td[text()='Benefit Maintenance']","TGTYPESCREENREG");

        TAP.$("ELE_SUBMEN_BENEFITPOL","//div[@id='cmSubMenuID39']//tr[2]//td[2]","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_POLICYNUMBER","//input[@type='text'][@name='outerForm:policyNumber']",var_PolicyNumber,"FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_CONTINUE","//input[@type='submit'][@name='outerForm:Continue']","TGTYPESCREENREG");

        TAP.$("ELE_LINK_BASE","//span[contains(text(),'Base')]","TGTYPESCREENREG");

        TAP.$("ELE_LINK_SUPPLEMENTALBENEFITS","//span[text()='Supplemental Benefits']","TGTYPESCREENREG");

        TAP.$("ELE_LINK_ACCIDENTALDEATHBENEFIT","//span[text()='Accidental Death Benefit']","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_UNITS","//input[@type='text'][@name='outerForm:suppUnits']",var_BaseBenefitUnits,"FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SUBMIT","//input[@type='submit'][@name='outerForm:Submit']","TGTYPESCREENREG");

        ASSERT.$("ELE_GETMSG_SUCCESS","//li[@class='MessageInfo']","=","Request completed successfully.","TGTYPESCREENREG");

        HOVER.$("ELE_MENU_INQUIRY","//td[text()='Inquiry']","TGTYPESCREENREG");

        TAP.$("ELE_SUBMEN_POLICYDETAIL","//td[text()='Policy Detail']","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_POLICYNUMBER","//input[@type='text'][@name='outerForm:policyNumber']",var_PolicyNumber,"FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_CONTINUE","//input[@type='submit'][@name='outerForm:Continue']","TGTYPESCREENREG");

        TAP.$("ELE_LINK_BENEFITS","//span[text()='Benefits']","TGTYPESCREENREG");

        TAP.$("ELE_LINK_BASE","//span[contains(text(),'Base')]","TGTYPESCREENREG");

		writeToCSV ("Ending Base Benefit Units " , ActionWrapper.getElementValue("ELE_GETVAL_BASEBENEFITUNITS", "//span[@id='outerForm:unitsOfInsurance']"));
                 LOGGER.info("Executed Step = WRITETOCSV,ELE_GETVAL_BASEBENEFITUNITS,Ending Base Benefit Units,TGTYPESCREENREG");
        TAP.$("ELE_LINK_SUPPLEMENTALBENEFIT","//span[@id='outerForm:SupplementalBenefitText']","TGTYPESCREENREG");

		writeToCSV ("Revised Supplemental Benefits " , ActionWrapper.getElementValue("ELE_GETVAL_SUPPLEMENTALBENEFITUNITS", "//span[@id='outerForm:grid:0:unitsOfInsuranceOut2']"));
                 LOGGER.info("Executed Step = WRITETOCSV,ELE_GETVAL_SUPPLEMENTALBENEFITUNITS,Revised Supplemental Benefits,TGTYPESCREENREG");
        }
          LOGGER.info("Executed Step = ENDIF,TGTYPESCREENREG");
		var_Count = var_Count + ( 1 );
                 LOGGER.info("Executed Step = INCREASEVAR,var_Count,1,TGTYPESCREENREG");
    	}
          LOGGER.info("Executed Step = WEND,TGTYPESCREENREG");
    	resetAttemps();

    }


    @Test
    public void tc16traditionallifematurity() {

        // No need to declare / print method name here, handled by TestNg listener
        // declare("tc16traditionallifematurity");

        CALL.$("LoginToGIAS36","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_USERNAME","outerForm:usernameTGWEBCOMMAIdTGWEBCOMMA0","GIAS36ILC","FALSE","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_PASSWORD","outerForm:passwordTGWEBCOMMAIdTGWEBCOMMA0","Automation","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_LOGIN","outerForm:loginButtonTGWEBCOMMAIdTGWEBCOMMA0","TGTYPESCREENREG");

		int var_Count;
                 LOGGER.info("Executed Step = VAR,Integer,var_Count,TGTYPESCREENREG");
		var_Count = 0;
                 LOGGER.info("Executed Step = STORE,var_Count,0,TGTYPESCREENREG");
        LOGGER.info("Executed Step = REPEAT IF");
    	while (check(var_Count,"<",1,"TGTYPESCREENREG")) {
          LOGGER.info("Executed Step = WHILE,var_Count,<,1,TGTYPESCREENREG");
		com.jayway.jsonpath.ReadContext var_CSVData = getJsonData("C:/GIAS_Automation/3.6 BaseRegression/InputData/36Reg_TraditionalLifeMaturity.csv");
                 LOGGER.info("Executed Step = VAR,Json,var_CSVData,C:/GIAS_Automation/3.6 BaseRegression/InputData/36Reg_TraditionalLifeMaturity.csv,TGTYPESCREENREG");
		String var_PolicyNumber;
                 LOGGER.info("Executed Step = VAR,String,var_PolicyNumber,TGTYPESCREENREG");
		var_PolicyNumber = getJsonData(var_CSVData , "$.records["+var_Count+"].PolicyNumber");
                 LOGGER.info("Executed Step = STORE,var_PolicyNumber,var_CSVData,$.records[{var_Count}].PolicyNumber,TGTYPESCREENREG");
        CALL.$("TraditionalLifeMaturity2","TGTYPESCREENREG");

        HOVER.$("ELE_MENU_INQUIRY","//td[text()='Inquiry']","TGTYPESCREENREG");

        TAP.$("ELE_SUBMEN_QUICKINQUIRY","//td[contains(text(),'Quick Inquiry')]","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_POLICYNUMBER","//input[@type='text'][@name='outerForm:policyNumber']",var_PolicyNumber,"FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_CONTINUE","//input[@type='submit'][@name='outerForm:Continue']","TGTYPESCREENREG");

        ASSERT.$("ELE_GETVAL_STATUS","//span[@id='outerForm:statusText']","=","Paid Up","TGTYPESCREENREG");

        ASSERT.$("ELE_GETVAL_INQUIRYSUSPENDANDREASON","//span[@id='outerForm:suspendText']","=","Yes/ZM","TGTYPESCREENREG");

        HOVER.$("ELE_MENU_POLICYOWNERSERVICES","//td[@class='ThemePanelMainFolderText'][contains(text(),'Policyowner Services')]","TGTYPESCREENREG");

        HOVER.$("ELE_SUBMEN_BENEFITMAINTENANCE","//td[text()='Benefit Maintenance']","TGTYPESCREENREG");

        TAP.$("ELE_SUBMEN_MATURITY","//td[contains(text(),'Maturity')]","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_POLICYNUMBER","//input[@type='text'][@name='outerForm:policyNumber']",var_PolicyNumber,"FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_CONTINUE","//input[@type='submit'][@name='outerForm:Continue']","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_CONTINUE","//input[@type='submit'][@name='outerForm:Continue']","TGTYPESCREENREG");

		writeToCSV ("Insured Name " , ActionWrapper.getElementValue("ELE_GETVAL_OWNERNAME", "//span[@id='outerForm:ownerName']"));
                 LOGGER.info("Executed Step = WRITETOCSV,ELE_GETVAL_OWNERNAME,Insured Name,TGTYPESCREENREG");
		writeToCSV ("Gross Maturity Value " , ActionWrapper.getElementValue("ELE_GETVAL_GROSSMATURITYVALUE", "//span[@id='outerForm:netMaturityValue']"));
                 LOGGER.info("Executed Step = WRITETOCSV,ELE_GETVAL_GROSSMATURITYVALUE,Gross Maturity Value,TGTYPESCREENREG");
		writeToCSV ("Available Payout Amount " , ActionWrapper.getElementValue("ELE_GETVAL_AVAILABLEPAYOUTAMOUNT", "//span[@id='outerForm:availablePayoutAmount']"));
                 LOGGER.info("Executed Step = WRITETOCSV,ELE_GETVAL_AVAILABLEPAYOUTAMOUNT,Available Payout Amount,TGTYPESCREENREG");
		String var_AvailablePayoutAmount;
                 LOGGER.info("Executed Step = VAR,String,var_AvailablePayoutAmount,TGTYPESCREENREG");
		var_AvailablePayoutAmount = ActionWrapper.getElementValue("ELE_GETVAL_AVAILABLEPAYOUTAMOUNT", "//span[@id='outerForm:availablePayoutAmount']");
                 LOGGER.info("Executed Step = STORE,var_AvailablePayoutAmount,ELE_GETVAL_AVAILABLEPAYOUTAMOUNT,TGTYPESCREENREG");
        TYPE.$("ELE_TXTBOX_DISBURSEMENTALLOCATION","//input[@id='outerForm:disbursementAllocation']",var_AvailablePayoutAmount,"FALSE","TGTYPESCREENREG");

        SELECT.$("ELE_DRPDWN_DISTRIBUTIONREASONCODE","//select[@id='outerForm:distributionReasonCode']","7 - Normal Distribution","TGTYPESCREENREG");

        TAP.$("ELE_LINK_DISTRIBUTIONINFORMATION","//span[@id='outerForm:DistributionInformationText']","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_CONTINUE","//input[@type='submit'][@name='outerForm:Continue']","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SUBMIT","//input[@type='submit'][@name='outerForm:Submit']","TGTYPESCREENREG");

        ASSERT.$("ELE_GETMSG_SUCCESS","//li[@class='MessageInfo']","=","Request completed successfully.","TGTYPESCREENREG");

        HOVER.$("ELE_MENU_INQUIRY","//td[text()='Inquiry']","TGTYPESCREENREG");

        TAP.$("ELE_SUBMEN_POLICYDETAIL","//td[text()='Policy Detail']","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_POLICYNUMBER","//input[@type='text'][@name='outerForm:policyNumber']",var_PolicyNumber,"FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_CONTINUE","//input[@type='submit'][@name='outerForm:Continue']","TGTYPESCREENREG");

        ASSERT.$("ELE_GETVAL_STATUS","//span[@id='outerForm:statusText']","=","Matured","TGTYPESCREENREG");

        ASSERT.$("ELE_GETVAL_INQUIRYSUSPENDEDREASON","//span[@id='outerForm:suspendReasonCodeText']","=","No","TGTYPESCREENREG");

        TAP.$("ELE_LINK_BENEFITS","//span[text()='Benefits']","TGTYPESCREENREG");

		String var_PlanName1 = "Whole Life";
                 LOGGER.info("Executed Step = VAR,String,var_PlanName1,Whole Life,TGTYPESCREENREG");
        LOGGER.info("Executed Step = START IF");
        if (check("ELE_GETVAL_PLANNAME","//span[@id='outerForm:grid:0:planCodeTextOut2']","CONTAINS",var_PlanName1,"TGTYPESCREENREG")) {
         LOGGER.info("Executed Step = IF,ELE_GETVAL_PLANNAME,CONTAINS,var_PlanName1,TGTYPESCREENREG");
        TAP.$("ELE_BUTTON_CANCEL","//input[@type='submit'][@name='outerForm:Cancel']","TGTYPESCREENREG");

        TAP.$("ELE_LINK_TRANSACTIONHISTORY","//span[@id='outerForm:TransactionHistoryText']","TGTYPESCREENREG");

        TAP.$("ELE_LINK_TRANSDATEHISTCOLUMN1LINE2","//span[@id='outerForm:grid:1:transactionDateOut1']","TGTYPESCREENREG");

        CALL.$("WritetoCSVWholeLife","TGTYPESCREENREG");

		writeToCSV ("Transaction Type " , ActionWrapper.getElementValue("ELE_GETVAL_TRANSACTIONTYPE", "//span[@id='outerForm:posTransactionType']"));
                 LOGGER.info("Executed Step = WRITETOCSV,ELE_GETVAL_TRANSACTIONTYPE,Transaction Type,TGTYPESCREENREG");
		writeToCSV ("Policy Number " , ActionWrapper.getElementValue("ELE_GETVAL_POLICYNUMBER", "//span[@id='outerForm:policyNumber']"));
                 LOGGER.info("Executed Step = WRITETOCSV,ELE_GETVAL_POLICYNUMBER,Policy Number,TGTYPESCREENREG");
		writeToCSV ("Plan  " , ActionWrapper.getElementValue("ELE_GETVAL_PLANTYPE", "//span[@id='outerForm:planCodeText']"));
                 LOGGER.info("Executed Step = WRITETOCSV,ELE_GETVAL_PLANTYPE,Plan ,TGTYPESCREENREG");
		writeToCSV ("Insurance Type " , ActionWrapper.getElementValue("ELE_GETVAL_INSURANCETYPE", "//span[@id='outerForm:insuranceType']"));
                 LOGGER.info("Executed Step = WRITETOCSV,ELE_GETVAL_INSURANCETYPE,Insurance Type,TGTYPESCREENREG");
		writeToCSV ("Face Amount " , ActionWrapper.getElementValue("ELE_GETVAL_FACEAMOUNT", "//span[@id='outerForm:faceAmount']"));
                 LOGGER.info("Executed Step = WRITETOCSV,ELE_GETVAL_FACEAMOUNT,Face Amount,TGTYPESCREENREG");
		writeToCSV ("Suspense Balance Released " , ActionWrapper.getElementValue("ELE_GETVAL_SUSPENDBALANCERELEASED", "//span[@id='outerForm:suspenseBalReleased']"));
                 LOGGER.info("Executed Step = WRITETOCSV,ELE_GETVAL_SUSPENDBALANCERELEASED,Suspense Balance Released,TGTYPESCREENREG");
		writeToCSV ("Distribution Reason " , ActionWrapper.getElementValue("ELE_GETVAL_DISTRIBUTIONREASON", "//span[@id='outerForm:numericDistCode']"));
                 LOGGER.info("Executed Step = WRITETOCSV,ELE_GETVAL_DISTRIBUTIONREASON,Distribution Reason,TGTYPESCREENREG");
		writeToCSV ("Dividend Accumulation " , ActionWrapper.getElementValue("ELE_GETVAL_DIVIDENDACCUMULATION", "//span[@id='outerForm:dividendAccumulated']"));
                 LOGGER.info("Executed Step = WRITETOCSV,ELE_GETVAL_DIVIDENDACCUMULATION,Dividend Accumulation,TGTYPESCREENREG");
		writeToCSV ("Net Maturity Value " , ActionWrapper.getElementValue("ELE_GETVAL_NETMATURITYVALUE", "//span[@id='outerForm:benefitNetValue']"));
                 LOGGER.info("Executed Step = WRITETOCSV,ELE_GETVAL_NETMATURITYVALUE,Net Maturity Value,TGTYPESCREENREG");
        TAP.$("ELE_BUTTON_CANCEL","//input[@type='submit'][@name='outerForm:Cancel']","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_CANCEL","//input[@type='submit'][@name='outerForm:Cancel']","TGTYPESCREENREG");

        TAP.$("ELE_LINK_DIVIDENDS","//span[@id='outerForm:DividendsText']","TGTYPESCREENREG");

        TAP.$("ELE_LINK_DIVIDENDTRANSACTIONHISTORY","//span[@id='outerForm:DividendCouponHistoryText']","TGTYPESCREENREG");

        }
         else {
          LOGGER.info("Executed Step = ELSE,TGTYPESCREENREG");
        TAP.$("ELE_BUTTON_CANCEL","//input[@type='submit'][@name='outerForm:Cancel']","TGTYPESCREENREG");

        TAP.$("ELE_LINK_TRANSACTIONHISTORY","//span[@id='outerForm:TransactionHistoryText']","TGTYPESCREENREG");

        TAP.$("ELE_LINK_TRANSHISTCOL1LINE1","//span[@id='outerForm:grid:0:transactionDateOut1']","TGTYPESCREENREG");

        CALL.$("WritetoCSVUniversalLife","TGTYPESCREENREG");

		writeToCSV ("Transaction Type " , ActionWrapper.getElementValue("ELE_GETVAL_TRANSACTIONTYPE", "//span[@id='outerForm:posTransactionType']"));
                 LOGGER.info("Executed Step = WRITETOCSV,ELE_GETVAL_TRANSACTIONTYPE,Transaction Type,TGTYPESCREENREG");
		writeToCSV ("Policy Number " , ActionWrapper.getElementValue("ELE_GETVAL_POLICYNUMBER", "//span[@id='outerForm:policyNumber']"));
                 LOGGER.info("Executed Step = WRITETOCSV,ELE_GETVAL_POLICYNUMBER,Policy Number,TGTYPESCREENREG");
		writeToCSV ("Plan  " , ActionWrapper.getElementValue("ELE_GETVAL_PLANTYPE", "//span[@id='outerForm:planCodeText']"));
                 LOGGER.info("Executed Step = WRITETOCSV,ELE_GETVAL_PLANTYPE,Plan ,TGTYPESCREENREG");
		writeToCSV ("Insurance Type " , ActionWrapper.getElementValue("ELE_GETVAL_INSURANCETYPE", "//span[@id='outerForm:insuranceType']"));
                 LOGGER.info("Executed Step = WRITETOCSV,ELE_GETVAL_INSURANCETYPE,Insurance Type,TGTYPESCREENREG");
		writeToCSV ("Face Amount " , ActionWrapper.getElementValue("ELE_GETVAL_FACEAMOUNT", "//span[@id='outerForm:faceAmount']"));
                 LOGGER.info("Executed Step = WRITETOCSV,ELE_GETVAL_FACEAMOUNT,Face Amount,TGTYPESCREENREG");
		writeToCSV ("Suspense Balance Released " , ActionWrapper.getElementValue("ELE_GETVAL_SUSPENDBALANCERELEASED", "//span[@id='outerForm:suspenseBalReleased']"));
                 LOGGER.info("Executed Step = WRITETOCSV,ELE_GETVAL_SUSPENDBALANCERELEASED,Suspense Balance Released,TGTYPESCREENREG");
		writeToCSV ("Fund Balance " , ActionWrapper.getElementValue("ELE_GETVAL_FUNDBALANCE", "//span[@id='outerForm:currentFundBalance']"));
                 LOGGER.info("Executed Step = WRITETOCSV,ELE_GETVAL_FUNDBALANCE,Fund Balance,TGTYPESCREENREG");
		writeToCSV ("Distribution Reason " , ActionWrapper.getElementValue("ELE_GETVAL_DISTRIBUTIONREASON", "//span[@id='outerForm:numericDistCode']"));
                 LOGGER.info("Executed Step = WRITETOCSV,ELE_GETVAL_DISTRIBUTIONREASON,Distribution Reason,TGTYPESCREENREG");
		writeToCSV ("Net Maturity Value " , ActionWrapper.getElementValue("ELE_GETVAL_NETMATURITYVALUE", "//span[@id='outerForm:benefitNetValue']"));
                 LOGGER.info("Executed Step = WRITETOCSV,ELE_GETVAL_NETMATURITYVALUE,Net Maturity Value,TGTYPESCREENREG");
        }
          LOGGER.info("Executed Step = ENDIF,TGTYPESCREENREG");
		var_Count = var_Count + ( 1 );
                 LOGGER.info("Executed Step = INCREASEVAR,var_Count,1,TGTYPESCREENREG");
    	}
          LOGGER.info("Executed Step = WEND,TGTYPESCREENREG");
    	resetAttemps();

    }


    @Test
    public void tc06facechangeincrease() {

        // No need to declare / print method name here, handled by TestNg listener
        // declare("tc06facechangeincrease");

        CALL.$("LoginToGIAS36","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_USERNAME","outerForm:usernameTGWEBCOMMAIdTGWEBCOMMA0","GIAS36ILC","FALSE","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_PASSWORD","outerForm:passwordTGWEBCOMMAIdTGWEBCOMMA0","Automation","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_LOGIN","outerForm:loginButtonTGWEBCOMMAIdTGWEBCOMMA0","TGTYPESCREENREG");

		int var_Count;
                 LOGGER.info("Executed Step = VAR,Integer,var_Count,TGTYPESCREENREG");
		var_Count = 0;
                 LOGGER.info("Executed Step = STORE,var_Count,0,TGTYPESCREENREG");
        LOGGER.info("Executed Step = REPEAT IF");
    	while (check(var_Count,"<",1,"TGTYPESCREENREG")) {
          LOGGER.info("Executed Step = WHILE,var_Count,<,1,TGTYPESCREENREG");
        CALL.$("FaceChangeIncrease","TGTYPESCREENREG");

		com.jayway.jsonpath.ReadContext var_CSVData = getJsonData("C:/GIAS_Automation/3.6 BaseRegression/InputData/36Reg_FaceChange.csv");
                 LOGGER.info("Executed Step = VAR,Json,var_CSVData,C:/GIAS_Automation/3.6 BaseRegression/InputData/36Reg_FaceChange.csv,TGTYPESCREENREG");
		String var_PolicyNumber;
                 LOGGER.info("Executed Step = VAR,String,var_PolicyNumber,TGTYPESCREENREG");
		var_PolicyNumber = getJsonData(var_CSVData , "$.records["+var_Count+"].PolicyNumber");
                 LOGGER.info("Executed Step = STORE,var_PolicyNumber,var_CSVData,$.records[{var_Count}].PolicyNumber,TGTYPESCREENREG");
		String var_ApplyDate;
                 LOGGER.info("Executed Step = VAR,String,var_ApplyDate,TGTYPESCREENREG");
		var_ApplyDate = getJsonData(var_CSVData , "$.records["+var_Count+"].ApplyDate");
                 LOGGER.info("Executed Step = STORE,var_ApplyDate,var_CSVData,$.records[{var_Count}].ApplyDate,TGTYPESCREENREG");
		String var_AmountofChange;
                 LOGGER.info("Executed Step = VAR,String,var_AmountofChange,TGTYPESCREENREG");
		var_AmountofChange = getJsonData(var_CSVData , "$.records["+var_Count+"].AmountofChange");
                 LOGGER.info("Executed Step = STORE,var_AmountofChange,var_CSVData,$.records[{var_Count}].AmountofChange,TGTYPESCREENREG");
        HOVER.$("ELE_MENU_POLICYOWNERSERVICES","//td[@class='ThemePanelMainFolderText'][contains(text(),'Policyowner Services')]","TGTYPESCREENREG");

        HOVER.$("ELE_SUBMEN_NONTRADITIONAL","//div[@id='cmSubMenuID38']//tbody//tr[7]//td[2]","TGTYPESCREENREG");

        TAP.$("ELE_SUBMEN_FACECHANGE","//td[text()='Face Change']","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_POLICYNUMBER","//input[@type='text'][@name='outerForm:policyNumber']",var_PolicyNumber,"FALSE","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_APPLYDATE","//input[@type='text'][@name='outerForm:promptDate']",var_ApplyDate,"FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_CONTINUE","//input[@type='submit'][@name='outerForm:Continue']","TGTYPESCREENREG");

        TAP.$("ELE_RDOBTN_INCREASEFACECHANGE","//input[@id='outerForm:increaseDecreaseIndicator:1']","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_FACECHANGEAMOUNT","//input[@type='text'][@name='outerForm:faceChangeAmount']",var_AmountofChange,"FALSE","TGTYPESCREENREG");

		writeToCSV("Policy Number " , var_PolicyNumber);
                 LOGGER.info("Executed Step = WRITETOCSV,var_PolicyNumber,Policy Number,TGTYPESCREENREG");
		writeToCSV("Apply Date " , var_ApplyDate);
                 LOGGER.info("Executed Step = WRITETOCSV,var_ApplyDate,Apply Date,TGTYPESCREENREG");
		writeToCSV("Amount of Change " , var_AmountofChange);
                 LOGGER.info("Executed Step = WRITETOCSV,var_AmountofChange,Amount of Change,TGTYPESCREENREG");
		writeToCSV ("Starting Face Amount " , ActionWrapper.getElementValue("ELE_TXTBOX_FACECHANGECURRENTFACEAMOUNT", "//span[@id='outerForm:currentFaceAmountSpecified']"));
                 LOGGER.info("Executed Step = WRITETOCSV,ELE_TXTBOX_FACECHANGECURRENTFACEAMOUNT,Starting Face Amount,TGTYPESCREENREG");
        TAP.$("ELE_BUTTON_EDIT","//input[@id='outerForm:Edit']","TGTYPESCREENREG");

        ASSERT.$("ELE_GETMSG_SUCCESS","//li[@class='MessageInfo']","=","Edit completed successfully with no errors.","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SUBMIT","//input[@type='submit'][@name='outerForm:Submit']","TGTYPESCREENREG");

        ASSERT.$("ELE_GETMSG_SUCCESS","//li[@class='MessageInfo']","=","Request completed successfully.","TGTYPESCREENREG");

        HOVER.$("ELE_MENU_INQUIRY","//td[text()='Inquiry']","TGTYPESCREENREG");

        TAP.$("ELE_SUBMEN_QUICKINQUIRY","//td[contains(text(),'Quick Inquiry')]","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_POLICYNUMBER","//input[@type='text'][@name='outerForm:policyNumber']",var_PolicyNumber,"FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_CONTINUE","//input[@type='submit'][@name='outerForm:Continue']","TGTYPESCREENREG");

        TAP.$("ELE_LINK_POLICYINQUIRY","//span[@id='outerForm:PolicyInquiryText']","TGTYPESCREENREG");

        TAP.$("ELE_LINK_TRANSACTIONHISTORY","//span[@id='outerForm:TransactionHistoryText']","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_CANCEL","//input[@type='submit'][@name='outerForm:Cancel']","TGTYPESCREENREG");

        TAP.$("ELE_LINK_BENEFITS","//span[text()='Benefits']","TGTYPESCREENREG");

        TAP.$("ELE_LINK_BASE","//span[contains(text(),'Base')]","TGTYPESCREENREG");

        TAP.$("ELE_LINK_BENEFITDISTRIBUTION","//span[text()='Benefit Distribution']","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_CANCEL","//input[@type='submit'][@name='outerForm:Cancel']","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_CANCEL","//input[@type='submit'][@name='outerForm:Cancel']","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_CANCEL","//input[@type='submit'][@name='outerForm:Cancel']","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_CANCEL","//input[@type='submit'][@name='outerForm:Cancel']","TGTYPESCREENREG");

		writeToCSV ("Ending Face Amount " , ActionWrapper.getElementValue("ELE_GETVAL_INQUIRYCURRENTFACEAMOUNT", "//span[@id='outerForm:grid:0:currentFaceAmountOut2']"));
                 LOGGER.info("Executed Step = WRITETOCSV,ELE_GETVAL_INQUIRYCURRENTFACEAMOUNT,Ending Face Amount,TGTYPESCREENREG");
		var_Count = var_Count + ( 1 );
                 LOGGER.info("Executed Step = INCREASEVAR,var_Count,1,TGTYPESCREENREG");
    	}
          LOGGER.info("Executed Step = WEND,TGTYPESCREENREG");
    	resetAttemps();

    }


    @Test
    public void tc10dividendsurrender() {

        // No need to declare / print method name here, handled by TestNg listener
        // declare("tc10dividendsurrender");

        CALL.$("LoginToGIAS36","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_USERNAME","outerForm:usernameTGWEBCOMMAIdTGWEBCOMMA0","GIAS36ILC","FALSE","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_PASSWORD","outerForm:passwordTGWEBCOMMAIdTGWEBCOMMA0","Automation","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_LOGIN","outerForm:loginButtonTGWEBCOMMAIdTGWEBCOMMA0","TGTYPESCREENREG");

		int var_Count;
                 LOGGER.info("Executed Step = VAR,Integer,var_Count,TGTYPESCREENREG");
		var_Count = 0;
                 LOGGER.info("Executed Step = STORE,var_Count,0,TGTYPESCREENREG");
        LOGGER.info("Executed Step = REPEAT IF");
    	while (check(var_Count,"<",1,"TGTYPESCREENREG")) {
          LOGGER.info("Executed Step = WHILE,var_Count,<,1,TGTYPESCREENREG");
        CALL.$("DividendSurrender","TGTYPESCREENREG");

		com.jayway.jsonpath.ReadContext var_CSVData = getJsonData("C:/GIAS_Automation/3.6 BaseRegression/InputData/36Reg_DividendSurrender.csv");
                 LOGGER.info("Executed Step = VAR,Json,var_CSVData,C:/GIAS_Automation/3.6 BaseRegression/InputData/36Reg_DividendSurrender.csv,TGTYPESCREENREG");
		String var_PolicyNumber;
                 LOGGER.info("Executed Step = VAR,String,var_PolicyNumber,TGTYPESCREENREG");
		var_PolicyNumber = getJsonData(var_CSVData , "$.records["+var_Count+"].PolicyNumber");
                 LOGGER.info("Executed Step = STORE,var_PolicyNumber,var_CSVData,$.records[{var_Count}].PolicyNumber,TGTYPESCREENREG");
		String var_SurrenderAmount;
                 LOGGER.info("Executed Step = VAR,String,var_SurrenderAmount,TGTYPESCREENREG");
		var_SurrenderAmount = getJsonData(var_CSVData , "$.records["+var_Count+"].SurrenderAmount");
                 LOGGER.info("Executed Step = STORE,var_SurrenderAmount,var_CSVData,$.records[{var_Count}].SurrenderAmount,TGTYPESCREENREG");
        HOVER.$("ELE_MENU_INQUIRY","//td[text()='Inquiry']","TGTYPESCREENREG");

        TAP.$("ELE_SUBMEN_POLICYDETAIL","//td[text()='Policy Detail']","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_POLICYNUMBER","//input[@type='text'][@name='outerForm:policyNumber']",var_PolicyNumber,"FALSE","TGTYPESCREENREG");

		writeToCSV("PolicyNumber " , var_PolicyNumber);
                 LOGGER.info("Executed Step = WRITETOCSV,var_PolicyNumber,PolicyNumber,TGTYPESCREENREG");
        TAP.$("ELE_BUTTON_CONTINUEPOLICYDETAIL","//input[@type='submit'][@name='outerForm:Continue']","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_DIVIDENDS","//span[text()='Dividends']","TGTYPESCREENREG");

		writeToCSV ("Prior Amount " , ActionWrapper.getElementValue("ELE_GETVAL_DEPACCUM", "//span[@id='outerForm:divCpnOnDepositAmt']"));
                 LOGGER.info("Executed Step = WRITETOCSV,ELE_GETVAL_DEPACCUM,Prior Amount,TGTYPESCREENREG");
        HOVER.$("ELE_MENU_POLICYOWNERSERVICES","//td[@class='ThemePanelMainFolderText'][contains(text(),'Policyowner Services')]","TGTYPESCREENREG");

        HOVER.$("ELE_SUBMEN_SURRENDER","//td[@class='ThemePanelMenuFolderText'][contains(text(),'Surrender')]","TGTYPESCREENREG");

        TAP.$("ELE_SUBMEN_DIVIDENDCOUPON","//div[@id='cmSubMenuID46']//tbody//tr[1]//td[2]","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_POLICYNUMBER","//input[@type='text'][@name='outerForm:policyNumber']",var_PolicyNumber,"FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_CONTINUEPOLICYDETAIL","//input[@type='submit'][@name='outerForm:Continue']","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_SURRENDERAMOUNT","//input[@type='text'][@name='outerForm:grid:0:surrenderAmount']",var_SurrenderAmount,"FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_DISTRIBUTIONINFORMATION","//span[text()='Distribution Information']","TGTYPESCREENREG");

		writeToCSV("SurrenderAmount " , var_SurrenderAmount);
                 LOGGER.info("Executed Step = WRITETOCSV,var_SurrenderAmount,SurrenderAmount,TGTYPESCREENREG");
        TAP.$("ELE_BUTTON_CONTINUE","//input[@type='submit'][@name='outerForm:Continue']","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SUBMIT","//input[@type='submit'][@name='outerForm:Submit']","TGTYPESCREENREG");

        ASSERT.$("ELE_GETMSG_SUCCESS","//li[@class='MessageInfo']","=","Request completed successfully.","TGTYPESCREENREG");

        HOVER.$("ELE_MENU_INQUIRY","//td[text()='Inquiry']","TGTYPESCREENREG");

        TAP.$("ELE_SUBMEN_POLICYDETAIL","//td[text()='Policy Detail']","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_POLICYNUMBER","//input[@type='text'][@name='outerForm:policyNumber']",var_PolicyNumber,"FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_CONTINUE","//input[@type='submit'][@name='outerForm:Continue']","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_DIVIDENDS","//span[text()='Dividends']","TGTYPESCREENREG");

		writeToCSV ("Post Amount " , ActionWrapper.getElementValue("ELE_GETVAL_DEPACCUM", "//span[@id='outerForm:divCpnOnDepositAmt']"));
                 LOGGER.info("Executed Step = WRITETOCSV,ELE_GETVAL_DEPACCUM,Post Amount,TGTYPESCREENREG");
        TAP.$("ELE_BUTTON_CANCEL","//input[@type='submit'][@name='outerForm:Cancel']","TGTYPESCREENREG");

        TAP.$("ELE_LINK_TRANSACTIONHISTORY","//span[@id='outerForm:TransactionHistoryText']","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_TRANSACTIONDATE","//span[@id='outerForm:grid:0:transactionDateOut1']","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_CANCEL","//input[@type='submit'][@name='outerForm:Cancel']","TGTYPESCREENREG");

        TAP.$("ELE_LINK_DIVIDENDDISBURSEMENT","//span[@id='outerForm:grid:1:transactionDateOut1']","TGTYPESCREENREG");

        HOVER.$("ELE_MENU_INQUIRY","//td[text()='Inquiry']","TGTYPESCREENREG");

        TAP.$("ELE_SUBMEN_QUICKINQUIRY","//td[contains(text(),'Quick Inquiry')]","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_POLICYNUMBER","//input[@type='text'][@name='outerForm:policyNumber']",var_PolicyNumber,"FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_CONTINUE","//input[@type='submit'][@name='outerForm:Continue']","TGTYPESCREENREG");

        TAP.$("ELE_LINK_POLICYINQUIRY","//span[@id='outerForm:PolicyInquiryText']","TGTYPESCREENREG");

        TAP.$("ELE_LINK_TRANSACTIONHISTORY","//span[@id='outerForm:TransactionHistoryText']","TGTYPESCREENREG");

        TAP.$("ELE_LINK_TRANSACTIONHISDBO","//span[@id='outerForm:grid:0:transactionDateOut1']","TGTYPESCREENREG");

        ASSERT.$("ELE_TXTBOX_DIVIDENDSURRENDER","//span[text()='Dividend surrender']","VISIBLE","TGTYPESCREENREG");

		var_Count = var_Count + ( 1 );
                 LOGGER.info("Executed Step = INCREASEVAR,var_Count,1,TGTYPESCREENREG");
    	}
          LOGGER.info("Executed Step = WEND,TGTYPESCREENREG");
    	resetAttemps();

    }


    @Test
    public void tc07dbochange() {

        // No need to declare / print method name here, handled by TestNg listener
        // declare("tc07dbochange");

        CALL.$("LoginToGIAS36","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_USERNAME","outerForm:usernameTGWEBCOMMAIdTGWEBCOMMA0","GIAS36ILC","FALSE","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_PASSWORD","outerForm:passwordTGWEBCOMMAIdTGWEBCOMMA0","Automation","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_LOGIN","outerForm:loginButtonTGWEBCOMMAIdTGWEBCOMMA0","TGTYPESCREENREG");

		int var_Count;
                 LOGGER.info("Executed Step = VAR,Integer,var_Count,TGTYPESCREENREG");
		var_Count = 0;
                 LOGGER.info("Executed Step = STORE,var_Count,0,TGTYPESCREENREG");
        LOGGER.info("Executed Step = REPEAT IF");
    	while (check(var_Count,"<",1,"TGTYPESCREENREG")) {
          LOGGER.info("Executed Step = WHILE,var_Count,<,1,TGTYPESCREENREG");
        CALL.$("DBOChangeRR","TGTYPESCREENREG");

		com.jayway.jsonpath.ReadContext var_CSVData = getJsonData("C:/GIAS_Automation/3.6 BaseRegression/InputData/36Reg_DBOChange.csv");
                 LOGGER.info("Executed Step = VAR,Json,var_CSVData,C:/GIAS_Automation/3.6 BaseRegression/InputData/36Reg_DBOChange.csv,TGTYPESCREENREG");
		String var_PolicyNumber;
                 LOGGER.info("Executed Step = VAR,String,var_PolicyNumber,TGTYPESCREENREG");
		var_PolicyNumber = getJsonData(var_CSVData , "$.records["+var_Count+"].PolicyNumber");
                 LOGGER.info("Executed Step = STORE,var_PolicyNumber,var_CSVData,$.records[{var_Count}].PolicyNumber,TGTYPESCREENREG");
        HOVER.$("ELE_MENU_POLICYOWNERSERVICES","//td[@class='ThemePanelMainFolderText'][contains(text(),'Policyowner Services')]","TGTYPESCREENREG");

        HOVER.$("ELE_SUBMEN_NONTRADITIONAL","//div[@id='cmSubMenuID38']//tbody//tr[7]//td[2]","TGTYPESCREENREG");

        TAP.$("ELE_SUBMEN_DBOCHANGE","//td[text()='Death Benefit Option Change']","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_POLICYNUMBER","//input[@type='text'][@name='outerForm:policyNumber']",var_PolicyNumber,"FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_CONTINUEDBO","//input[@type='submit'][@name='outerForm:Continue']","TGTYPESCREENREG");

		writeToCSV("Policy Number " , var_PolicyNumber);
                 LOGGER.info("Executed Step = WRITETOCSV,var_PolicyNumber,Policy Number,TGTYPESCREENREG");
		writeToCSV ("Prior DB Option " , ActionWrapper.getElementValue("ELE_GETVAL_PRIORDBOPTION", "//label[@for='outerForm:deathBenefitOption:1']"));
                 LOGGER.info("Executed Step = WRITETOCSV,ELE_GETVAL_PRIORDBOPTION,Prior DB Option,TGTYPESCREENREG");
		writeToCSV ("Prior Face Amount " , ActionWrapper.getElementValue("ELE_GETVAL_PRIORFACEAMTDB", "//span[@id='outerForm:currentFaceAmount']"));
                 LOGGER.info("Executed Step = WRITETOCSV,ELE_GETVAL_PRIORFACEAMTDB,Prior Face Amount,TGTYPESCREENREG");
		writeToCSV ("Post DB Option " , ActionWrapper.getElementValue("ELE_GETVAL_POSTDBOP", "//label[@for='outerForm:deathBenefitOption:0']"));
                 LOGGER.info("Executed Step = WRITETOCSV,ELE_GETVAL_POSTDBOP,Post DB Option,TGTYPESCREENREG");
        LOGGER.info("Executed Step = START IF");
        if (check("ELE_RDOBTN_FACEAMTPLUS","//input[@id='outerForm:deathBenefitOption:1']","SELECTED","TGTYPESCREENREG")) {
         LOGGER.info("Executed Step = IF,ELE_RDOBTN_FACEAMTPLUS,SELECTED,TGTYPESCREENREG");
        TAP.$("ELE_RDOBTN_FACEAMOUNT","//input[@id='outerForm:deathBenefitOption:0']","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SUBMIT","//input[@type='submit'][@name='outerForm:Submit']","TGTYPESCREENREG");

        }
         else {
          LOGGER.info("Executed Step = ELSE,TGTYPESCREENREG");
        TAP.$("ELE_RDOBTN_FACEAMTPLUS","//input[@id='outerForm:deathBenefitOption:1']","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SUBMIT","//input[@type='submit'][@name='outerForm:Submit']","TGTYPESCREENREG");

        }
          LOGGER.info("Executed Step = ENDIF,TGTYPESCREENREG");
        ASSERT.$("ELE_GETMSG_SUCCESS","//li[@class='MessageInfo']","=","Request completed successfully.","TGTYPESCREENREG");

        HOVER.$("ELE_MENU_INQUIRY","//td[text()='Inquiry']","TGTYPESCREENREG");

        TAP.$("ELE_SUBMEN_QUICKINQUIRY","//td[contains(text(),'Quick Inquiry')]","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_POLICYNUMBER","//input[@type='text'][@name='outerForm:policyNumber']",var_PolicyNumber,"FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_CONTINUE","//input[@type='submit'][@name='outerForm:Continue']","TGTYPESCREENREG");

		writeToCSV ("Post Face Amount " , ActionWrapper.getElementValue("ELE_GETVAL_POSTFACEAMOUNTDB", "//span[@id='outerForm:grid:0:currentFaceAmountOut2']"));
                 LOGGER.info("Executed Step = WRITETOCSV,ELE_GETVAL_POSTFACEAMOUNTDB,Post Face Amount,TGTYPESCREENREG");
		var_Count = var_Count + ( 1 );
                 LOGGER.info("Executed Step = INCREASEVAR,var_Count,1,TGTYPESCREENREG");
    	}
          LOGGER.info("Executed Step = WEND,TGTYPESCREENREG");
    	resetAttemps();

    }


    @Test
    public void tc12fullsurrenderpolicy() {

        // No need to declare / print method name here, handled by TestNg listener
        // declare("tc12fullsurrenderpolicy");

        CALL.$("LoginToGIAS36","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_USERNAME","outerForm:usernameTGWEBCOMMAIdTGWEBCOMMA0","GIAS36ILC","FALSE","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_PASSWORD","outerForm:passwordTGWEBCOMMAIdTGWEBCOMMA0","Automation","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_LOGIN","outerForm:loginButtonTGWEBCOMMAIdTGWEBCOMMA0","TGTYPESCREENREG");

		int var_Count;
                 LOGGER.info("Executed Step = VAR,Integer,var_Count,TGTYPESCREENREG");
		var_Count = 0;
                 LOGGER.info("Executed Step = STORE,var_Count,0,TGTYPESCREENREG");
        LOGGER.info("Executed Step = REPEAT IF");
    	while (check(var_Count,"<",1,"TGTYPESCREENREG")) {
          LOGGER.info("Executed Step = WHILE,var_Count,<,1,TGTYPESCREENREG");
        CALL.$("NavigateToSurrenderPolicy","TGTYPESCREENREG");

        HOVER.$("ELE_MENU_POLICYOWNERSERVICES","//td[@class='ThemePanelMainFolderText'][contains(text(),'Policyowner Services')]","TGTYPESCREENREG");

        HOVER.$("ELE_SUBMEN_SURRENDER","//td[@class='ThemePanelMenuFolderText'][contains(text(),'Surrender')]","TGTYPESCREENREG");

        TAP.$("ELE_SUBMEN_FULLPARTIALSURRENDER","//td[contains(text(),'Full/Partial Surrender')]","TGTYPESCREENREG");

		com.jayway.jsonpath.ReadContext var_CSVData = getJsonData("C:/GIAS_Automation/3.6 BaseRegression/InputData/36Reg_FullSurrender.csv");
                 LOGGER.info("Executed Step = VAR,Json,var_CSVData,C:/GIAS_Automation/3.6 BaseRegression/InputData/36Reg_FullSurrender.csv,TGTYPESCREENREG");
		String var_policyNumber;
                 LOGGER.info("Executed Step = VAR,String,var_policyNumber,TGTYPESCREENREG");
		var_policyNumber = getJsonData(var_CSVData , "$.records.["+var_Count+"].SurrenderPolicyNumber");
                 LOGGER.info("Executed Step = STORE,var_policyNumber,var_CSVData,$.records.[{var_Count}].SurrenderPolicyNumber,TGTYPESCREENREG");
        TYPE.$("ELE_TXTBOX_POLICYNUMBER","//input[@type='text'][@name='outerForm:policyNumber']",var_policyNumber,"FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_FULLSURRENDER","//input[@id='outerForm:FullSurrender']","TGTYPESCREENREG");

        TAP.$("ELE_LINK_BASE","//span[contains(text(),'Base')]",1,0,"TGTYPESCREENREG");

        TAP.$("ELE_LINK_DISTRIBUTIONINFORMATION","//span[@id='outerForm:DistributionInformationText']",1,0,"TGTYPESCREENREG");

        SCROLL.$("ELE_BUTTON_CONTINUE","//input[@type='submit'][@name='outerForm:Continue']","DOWN","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_CONTINUE","//input[@type='submit'][@name='outerForm:Continue']",1,0,"TGTYPESCREENREG");

        SELECT.$("ELE_DRPDWN_DISTRSNCODE","//select[@id='outerForm:distributionReasonCode_full']","7 - Normal Distribution","TGTYPESCREENREG");

        SCROLL.$("ELE_BUTTON_SUBMIT","//input[@type='submit'][@name='outerForm:Submit']","DOWN","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SUBMIT","//input[@type='submit'][@name='outerForm:Submit']",1,0,"TGTYPESCREENREG");

        CALL.$("InquireSurrenderPolicy","TGTYPESCREENREG");

        HOVER.$("ELE_MENU_INQUIRY","//td[text()='Inquiry']","TGTYPESCREENREG");

        TAP.$("ELE_SUBMEN_POLICYDETAIL","//td[text()='Policy Detail']","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_POLICYNUMBER","//input[@type='text'][@name='outerForm:policyNumber']",var_policyNumber,"FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_CONTINUE","//input[@type='submit'][@name='outerForm:Continue']","TGTYPESCREENREG");

		writeToCSV ("Policy Number " , ActionWrapper.getElementValue("ELE_GETVAL_POLICYNUMBER", "//span[@id='outerForm:policyNumber']"));
                 LOGGER.info("Executed Step = WRITETOCSV,ELE_GETVAL_POLICYNUMBER,Policy Number,TGTYPESCREENREG");
		writeToCSV ("Status " , ActionWrapper.getElementValue("ELE_GETVAL_STATUS", "//span[@id='outerForm:statusText']"));
                 LOGGER.info("Executed Step = WRITETOCSV,ELE_GETVAL_STATUS,Status,TGTYPESCREENREG");
		writeToCSV ("Effective Date " , ActionWrapper.getElementValue("ELE_GETVAL_EFFECTIVEDATE", "//span[@id='outerForm:effectiveDate_input']"));
                 LOGGER.info("Executed Step = WRITETOCSV,ELE_GETVAL_EFFECTIVEDATE,Effective Date,TGTYPESCREENREG");
		writeToCSV ("Annual Premium " , ActionWrapper.getElementValue("ELE_GETVAL_ANNUALPREMIUM", "//span[@id='outerForm:annualPremium']"));
                 LOGGER.info("Executed Step = WRITETOCSV,ELE_GETVAL_ANNUALPREMIUM,Annual Premium,TGTYPESCREENREG");
        TAP.$("ELE_LINK_POLICYVALUES","//span[contains(text(),'Policy Values')]","TGTYPESCREENREG");

		writeToCSV ("Net Policy " , ActionWrapper.getElementValue("ELE_GETVAL_POLICYPRINCIPALVALUE", "//span[@id='outerForm:policyPrinCashValue']"));
                 LOGGER.info("Executed Step = WRITETOCSV,ELE_GETVAL_POLICYPRINCIPALVALUE,Net Policy,TGTYPESCREENREG");
		var_Count = var_Count + ( 1 );
                 LOGGER.info("Executed Step = INCREASEVAR,var_Count,1,TGTYPESCREENREG");
    	}
          LOGGER.info("Executed Step = WEND,TGTYPESCREENREG");
    	resetAttemps();

    }


    @Test
    public void tc09suspenseaccount() {

        // No need to declare / print method name here, handled by TestNg listener
        // declare("tc09suspenseaccount");

        CALL.$("LoginToGIAS36","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_USERNAME","outerForm:usernameTGWEBCOMMAIdTGWEBCOMMA0","GIAS36ILC","FALSE","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_PASSWORD","outerForm:passwordTGWEBCOMMAIdTGWEBCOMMA0","Automation","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_LOGIN","outerForm:loginButtonTGWEBCOMMAIdTGWEBCOMMA0","TGTYPESCREENREG");

		int var_Count;
                 LOGGER.info("Executed Step = VAR,Integer,var_Count,TGTYPESCREENREG");
		var_Count = 0;
                 LOGGER.info("Executed Step = STORE,var_Count,0,TGTYPESCREENREG");
		com.jayway.jsonpath.ReadContext var_CSVData = getJsonData("C:/GIAS_Automation/3.6 BaseRegression/InputData/36Reg_SuspenseAccount.csv");
                 LOGGER.info("Executed Step = VAR,Json,var_CSVData,C:/GIAS_Automation/3.6 BaseRegression/InputData/36Reg_SuspenseAccount.csv,TGTYPESCREENREG");
        LOGGER.info("Executed Step = REPEAT IF");
    	while (check(var_Count,"<",1,"TGTYPESCREENREG")) {
          LOGGER.info("Executed Step = WHILE,var_Count,<,1,TGTYPESCREENREG");
		String var_PolicyNumber;
                 LOGGER.info("Executed Step = VAR,String,var_PolicyNumber,TGTYPESCREENREG");
		var_PolicyNumber = getJsonData(var_CSVData , "$.records["+var_Count+"].SuspensePolicyNumber");
                 LOGGER.info("Executed Step = STORE,var_PolicyNumber,var_CSVData,$.records[{var_Count}].SuspensePolicyNumber,TGTYPESCREENREG");
        CALL.$("CheckMinimumIsLessOrEqualToSuspenseBalance","TGTYPESCREENREG");

		int var_SuspenseBalance;
                 LOGGER.info("Executed Step = VAR,Integer,var_SuspenseBalance,TGTYPESCREENREG");
		int var_getMinimumBalance;
                 LOGGER.info("Executed Step = VAR,Integer,var_getMinimumBalance,TGTYPESCREENREG");
		String var_MinimumBalance;
                 LOGGER.info("Executed Step = VAR,String,var_MinimumBalance,TGTYPESCREENREG");
        HOVER.$("ELE_MENU_INQUIRY","//td[text()='Inquiry']","TGTYPESCREENREG");

        TAP.$("ELE_SUBMEN_POLICYDETAIL","//td[text()='Policy Detail']","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_POLICYNUMBER","//input[@type='text'][@name='outerForm:policyNumber']",var_PolicyNumber,"FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_CONTINUE","//input[@type='submit'][@name='outerForm:Continue']","TGTYPESCREENREG");

		var_getMinimumBalance = ActionWrapper.getElementValueForVariableWithInt("ELE_GETVAL_MINIMUMBALANCE", "//span[@id='outerForm:minimumRequiredPrem']");
                 LOGGER.info("Executed Step = STORE,var_getMinimumBalance,ELE_GETVAL_MINIMUMBALANCE,TGTYPESCREENREG");
		var_SuspenseBalance = ActionWrapper.getElementValueForVariableWithInt("ELE_GETVAL_SUSPENSEBALANCE", "//span[@id='outerForm:suspenseBalance']");
                 LOGGER.info("Executed Step = STORE,var_SuspenseBalance,ELE_GETVAL_SUSPENSEBALANCE,TGTYPESCREENREG");
		var_MinimumBalance = ActionWrapper.getElementValue("ELE_GETVAL_MINIMUMBALANCE", "//span[@id='outerForm:minimumRequiredPrem']");
                 LOGGER.info("Executed Step = STORE,var_MinimumBalance,ELE_GETVAL_MINIMUMBALANCE,TGTYPESCREENREG");
        LOGGER.info("Executed Step = START IF");
        if (check(var_getMinimumBalance,"<=",var_SuspenseBalance,"TGTYPESCREENREG")) {
         LOGGER.info("Executed Step = IF,var_getMinimumBalance,<=,var_SuspenseBalance,TGTYPESCREENREG");
        CALL.$("SearchSuspenseAccount","TGTYPESCREENREG");

        HOVER.$("ELE_MENU_PAYMENTS","//td[@class='ThemePanelMainFolderText'][contains(text(),'Payments')]","TGTYPESCREENREG");

        TAP.$("ELE_SUBMEN_SUSPENSE","//div[@id='cmSubMenuID24']//td[@class='ThemePanelMenuItemText'][contains(text(),'Suspense')]","TGTYPESCREENREG");

        TAP.$("ELE_RNDBTN_SEARCH","//img[@id='outerForm:gnSearchImage']","TGTYPESCREENREG");

        SELECT.$("ELE_DRPDWN_SEARCHCONTROLNUMBER","//select[@name='outerForm:grid:3:FullOperator']","Equals","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_SEARCHCONTROLNUMBER","//input[@id='outerForm:grid:3:fieldUpper']",var_PolicyNumber,"FALSE","TGTYPESCREENREG");

        TAP.$("ELE_RECBTN_SEARCH","//input[@id='outerForm:Search']","TGTYPESCREENREG");

        WAIT.$("ELE_RECBTN_SEARCH","//input[@id='outerForm:Search']","TGTYPESCREENREG");

        CALL.$("MakePayment","TGTYPESCREENREG");

        HOVER.$("ELE_MENU_PAYMENTS","//td[@class='ThemePanelMainFolderText'][contains(text(),'Payments')]","TGTYPESCREENREG");

        TAP.$("ELE_SUBMEN_PAYMENTSPOLICY","//div[@id='cmSubMenuID24']//td[@class='ThemePanelMenuItemText'][contains(text(),'Policy')]","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_POLICYNUMBER","//input[@type='text'][@name='outerForm:policyNumber']",var_PolicyNumber,"FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_CONTINUE","//input[@type='submit'][@name='outerForm:Continue']","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SUBMIT","//input[@type='submit'][@name='outerForm:Submit']","TGTYPESCREENREG");

        ASSERT.$("ELE_GETMSG_SUCCESS","//li[@class='MessageInfo']","CONTAINS","successfully","TGTYPESCREENREG");

        }
         else {
          LOGGER.info("Executed Step = ELSE,TGTYPESCREENREG");
        CALL.$("fillAddSuspenseDetailsForm","TGTYPESCREENREG");

        HOVER.$("ELE_MENU_PAYMENTS","//td[@class='ThemePanelMainFolderText'][contains(text(),'Payments')]","TGTYPESCREENREG");

        TAP.$("ELE_SUBMEN_SUSPENSE","//div[@id='cmSubMenuID24']//td[@class='ThemePanelMenuItemText'][contains(text(),'Suspense')]","TGTYPESCREENREG");

        TAP.$("ELE_RNDBTN_ADD","//img[@id='outerForm:gnAddImage']","TGTYPESCREENREG");

        SELECT.$("ELE_DRPDWN_CURRENCYCODE","//select[@id='outerForm:currencyCode']","Dollars [US]","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_SUSPENSECONTROLNUMBER","//input[@id='outerForm:suspenseControlNumb']",var_PolicyNumber,"FALSE","TGTYPESCREENREG");

        SELECT.$("ELE_DRPDWN_SUSPENSETYPE","//select[@id='outerForm:suspenseType']","Premium suspense","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_SUSPENSEAMOUNTOFCHANGE","//input[@id='outerForm:amountOfChange']",var_MinimumBalance,"TRUE","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_SUSPENSECOMMENT","//input[@id='outerForm:suspenseComment']","Premium suspense","FALSE","TGTYPESCREENREG");

        SELECT.$("ELE_DRPDWN_SUSPENSEACCOUNTNUMBER","//select[@id='outerForm:suspenseAccountNumb']","153000 - CASH","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SUBMIT","//input[@type='submit'][@name='outerForm:Submit']","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_CONFIRMSUBMIT2","//input[@id='outerForm:ConfirmSubmit']","TGTYPESCREENREG");

        CALL.$("MakePayment","TGTYPESCREENREG");

        HOVER.$("ELE_MENU_PAYMENTS","//td[@class='ThemePanelMainFolderText'][contains(text(),'Payments')]","TGTYPESCREENREG");

        TAP.$("ELE_SUBMEN_PAYMENTSPOLICY","//div[@id='cmSubMenuID24']//td[@class='ThemePanelMenuItemText'][contains(text(),'Policy')]","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_POLICYNUMBER","//input[@type='text'][@name='outerForm:policyNumber']",var_PolicyNumber,"FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_CONTINUE","//input[@type='submit'][@name='outerForm:Continue']","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SUBMIT","//input[@type='submit'][@name='outerForm:Submit']","TGTYPESCREENREG");

        ASSERT.$("ELE_GETMSG_SUCCESS","//li[@class='MessageInfo']","CONTAINS","successfully","TGTYPESCREENREG");

        }
          LOGGER.info("Executed Step = ENDIF,TGTYPESCREENREG");
        CALL.$("ToCheckSuspenseAccountBalance","TGTYPESCREENREG");

        HOVER.$("ELE_MENU_INQUIRY","//td[text()='Inquiry']","TGTYPESCREENREG");

        TAP.$("ELE_SUBMEN_QUICKINQUIRY","//td[contains(text(),'Quick Inquiry')]","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_POLICYNUMBER","//input[@type='text'][@name='outerForm:policyNumber']",var_PolicyNumber,"FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_CONTINUE","//input[@type='submit'][@name='outerForm:Continue']","TGTYPESCREENREG");

		writeToCSV ("Policy Number " , ActionWrapper.getElementValue("ELE_GETVAL_POLICYNUMBER", "//span[@id='outerForm:policyNumber']"));
                 LOGGER.info("Executed Step = WRITETOCSV,ELE_GETVAL_POLICYNUMBER,Policy Number,TGTYPESCREENREG");
		writeToCSV ("Effective Date " , ActionWrapper.getElementValue("ELE_GETVAL_EFFECTIVEDATETEXTFROMGRID", "//span[@id='outerForm:grid:0:effectiveDateOut2']"));
                 LOGGER.info("Executed Step = WRITETOCSV,ELE_GETVAL_EFFECTIVEDATETEXTFROMGRID,Effective Date,TGTYPESCREENREG");
		writeToCSV ("Status " , ActionWrapper.getElementValue("ELE_GETVAL_STATUS", "//span[@id='outerForm:statusText']"));
                 LOGGER.info("Executed Step = WRITETOCSV,ELE_GETVAL_STATUS,Status,TGTYPESCREENREG");
		writeToCSV ("Suspense Balance " , ActionWrapper.getElementValue("ELE_GETVAL_SUSPENSEBALANCE", "//span[@id='outerForm:suspenseBalance']"));
                 LOGGER.info("Executed Step = WRITETOCSV,ELE_GETVAL_SUSPENSEBALANCE,Suspense Balance,TGTYPESCREENREG");
		var_Count = var_Count + ( 1 );
                 LOGGER.info("Executed Step = INCREASEVAR,var_Count,1,TGTYPESCREENREG");
    	}
          LOGGER.info("Executed Step = WEND,TGTYPESCREENREG");
    	resetAttemps();

    }


    @Test
    public void tc02addabeneficiary() {

        // No need to declare / print method name here, handled by TestNg listener
        // declare("tc02addabeneficiary");

		int var_Count;
                 LOGGER.info("Executed Step = VAR,Integer,var_Count,TGTYPESCREENREG");
		var_Count = 0;
                 LOGGER.info("Executed Step = STORE,var_Count,0,TGTYPESCREENREG");
        LOGGER.info("Executed Step = REPEAT IF");
    	while (check(var_Count,"<",1,"TGTYPESCREENREG")) {
          LOGGER.info("Executed Step = WHILE,var_Count,<,1,TGTYPESCREENREG");
        CALL.$("LoginToGIAS36","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_USERNAME","outerForm:usernameTGWEBCOMMAIdTGWEBCOMMA0","GIAS36ILC","FALSE","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_PASSWORD","outerForm:passwordTGWEBCOMMAIdTGWEBCOMMA0","Automation","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_LOGIN","outerForm:loginButtonTGWEBCOMMAIdTGWEBCOMMA0","TGTYPESCREENREG");

        CALL.$("AddaBeneficiary2","TGTYPESCREENREG");

        HOVER.$("ELE_MENU_CLIENT","//td[text()='Client']","TGTYPESCREENREG");

        TAP.$("ELE_SUBMEN_POLICYRELATIONSHIPS","//td[text()='Policy Relationships']","TGTYPESCREENREG");

		com.jayway.jsonpath.ReadContext var_CSVData = getJsonData("C:/GIAS_Automation/3.6 BaseRegression/InputData/36Reg_AddBeneficiary.csv");
                 LOGGER.info("Executed Step = VAR,Json,var_CSVData,C:/GIAS_Automation/3.6 BaseRegression/InputData/36Reg_AddBeneficiary.csv,TGTYPESCREENREG");
		String var_PolicyNumber = "null";
                 LOGGER.info("Executed Step = VAR,String,var_PolicyNumber,null,TGTYPESCREENREG");
		var_PolicyNumber = getJsonData(var_CSVData , "$.records["+var_Count+"].PolicyNumber");
                 LOGGER.info("Executed Step = STORE,var_PolicyNumber,var_CSVData,$.records[{var_Count}].PolicyNumber,TGTYPESCREENREG");
        TYPE.$("ELE_TXTBOX_POLICYNUMBER","//input[@type='text'][@name='outerForm:policyNumber']",var_PolicyNumber,"FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_CONTINUE","//input[@type='submit'][@name='outerForm:Continue']","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_ADDCLIENTRELATIONSHIP","//input[@type='submit'][@name='outerForm:AddRelationship']","TGTYPESCREENREG");

        SELECT.$("ELE_DRPDWN_RELATIONSHIPTYPE","*[name='outerForm:userDefnRelaType']TGWEBCOMMACssSelectorTGWEBCOMMA0","Beneficiary (Contract)","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_SHAREPERCENTAGE","//input[@type='text'][@name='outerForm:sharePercentage']","100.00","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_LINK_SELECTCLIENT","//span[text()='Select Client']","TGTYPESCREENREG");

        TAP.$("ELE_RECBTN_ADD","//input[@type='submit'][@name='outerForm:Add']","TGTYPESCREENREG");

		String var_BeneficiaryFirstName = "null";
                 LOGGER.info("Executed Step = VAR,String,var_BeneficiaryFirstName,null,TGTYPESCREENREG");
		var_BeneficiaryFirstName = getJsonData(var_CSVData , "$.records["+var_Count+"].BeneficiaryFirstName");
                 LOGGER.info("Executed Step = STORE,var_BeneficiaryFirstName,var_CSVData,$.records[{var_Count}].BeneficiaryFirstName,TGTYPESCREENREG");
		String var_BeneficiaryLastName;
                 LOGGER.info("Executed Step = VAR,String,var_BeneficiaryLastName,TGTYPESCREENREG");
		var_BeneficiaryLastName = getJsonData(var_CSVData , "$.records["+var_Count+"].BeneficiaryLastName");
                 LOGGER.info("Executed Step = STORE,var_BeneficiaryLastName,var_CSVData,$.records[{var_Count}].BeneficiaryLastName,TGTYPESCREENREG");
		String var_BeneficiaryBirthState;
                 LOGGER.info("Executed Step = VAR,String,var_BeneficiaryBirthState,TGTYPESCREENREG");
		var_BeneficiaryBirthState = getJsonData(var_CSVData , "$.records["+var_Count+"].BeneficiaryBirthState");
                 LOGGER.info("Executed Step = STORE,var_BeneficiaryBirthState,var_CSVData,$.records[{var_Count}].BeneficiaryBirthState,TGTYPESCREENREG");
		String var_BeneficiaryDOB;
                 LOGGER.info("Executed Step = VAR,String,var_BeneficiaryDOB,TGTYPESCREENREG");
		var_BeneficiaryDOB = getJsonData(var_CSVData , "$.records["+var_Count+"].BeneficiaryDOB");
                 LOGGER.info("Executed Step = STORE,var_BeneficiaryDOB,var_CSVData,$.records[{var_Count}].BeneficiaryDOB,TGTYPESCREENREG");
		String var_BeneficiaryGender;
                 LOGGER.info("Executed Step = VAR,String,var_BeneficiaryGender,TGTYPESCREENREG");
		var_BeneficiaryGender = getJsonData(var_CSVData , "$.records["+var_Count+"].BeneficiaryGender");
                 LOGGER.info("Executed Step = STORE,var_BeneficiaryGender,var_CSVData,$.records[{var_Count}].BeneficiaryGender,TGTYPESCREENREG");
		String var_BeneficiaryIDType;
                 LOGGER.info("Executed Step = VAR,String,var_BeneficiaryIDType,TGTYPESCREENREG");
		var_BeneficiaryIDType = getJsonData(var_CSVData , "$.records["+var_Count+"].BeneficiaryIDType");
                 LOGGER.info("Executed Step = STORE,var_BeneficiaryIDType,var_CSVData,$.records[{var_Count}].BeneficiaryIDType,TGTYPESCREENREG");
		String var_BeneficiaryIDNumber;
                 LOGGER.info("Executed Step = VAR,String,var_BeneficiaryIDNumber,TGTYPESCREENREG");
		var_BeneficiaryIDNumber = getJsonData(var_CSVData , "$.records["+var_Count+"].BeneficiaryIDNumber");
                 LOGGER.info("Executed Step = STORE,var_BeneficiaryIDNumber,var_CSVData,$.records[{var_Count}].BeneficiaryIDNumber,TGTYPESCREENREG");
        TYPE.$("ELE_TXTBOX_BENEFICIARYFIRSTNAME","input[type=text][name='outerForm:nameFirst']TGWEBCOMMACssSelectorTGWEBCOMMA0",var_BeneficiaryFirstName,"FALSE","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_BENEFICIARYLASTNAME","input[type=text][name='outerForm:nameLast']TGWEBCOMMACssSelectorTGWEBCOMMA0",var_BeneficiaryLastName,"FALSE","TGTYPESCREENREG");

        SELECT.$("ELE_DRPDWN_BENEFICIARYSTATE","select[name='outerForm:birthCountryAndState2']TGWEBCOMMACssSelectorTGWEBCOMMA0",var_BeneficiaryBirthState,"TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_BENEFICIARYDOB","input[type=text][name='outerForm:dateOfBirth2']TGWEBCOMMACssSelectorTGWEBCOMMA0",var_BeneficiaryDOB,"FALSE","TGTYPESCREENREG");

        SELECT.$("ELE_DRPBWN_BENEFICIARYGENDER","select[name='outerForm:sexCode2']TGWEBCOMMACssSelectorTGWEBCOMMA0",var_BeneficiaryGender,"TGTYPESCREENREG");

        SELECT.$("ELE_DRPDWN_BENEFICIARYID","select[name='outerForm:taxIdenUsag2']TGWEBCOMMACssSelectorTGWEBCOMMA0",var_BeneficiaryIDType,"TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_BENEFICIARYIDNUMBER","input[type=text][name='outerForm:identificationNumber2']TGWEBCOMMACssSelectorTGWEBCOMMA0",var_BeneficiaryIDNumber,"FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SUBMIT","//input[@type='submit'][@name='outerForm:Submit']","TGTYPESCREENREG");

        ASSERT.$("ELE_GETMSG_SUCCESS","//li[@class='MessageInfo']","CONTAINS","successfully","TGTYPESCREENREG");

		String var_BeneficiaryStreetAddress = "null";
                 LOGGER.info("Executed Step = VAR,String,var_BeneficiaryStreetAddress,null,TGTYPESCREENREG");
		var_BeneficiaryStreetAddress = getJsonData(var_CSVData , "$.records["+var_Count+"].BeneficiaryStreetAddress");
                 LOGGER.info("Executed Step = STORE,var_BeneficiaryStreetAddress,var_CSVData,$.records[{var_Count}].BeneficiaryStreetAddress,TGTYPESCREENREG");
        TYPE.$("ELE_TXTBOX_BENEFICIARYSTREETADDRESS","input[type=text][name='outerForm:addressLineOne']TGWEBCOMMACssSelectorTGWEBCOMMA0",var_BeneficiaryStreetAddress,"FALSE","TGTYPESCREENREG");

		String var_BeneficiaryCity;
                 LOGGER.info("Executed Step = VAR,String,var_BeneficiaryCity,TGTYPESCREENREG");
		var_BeneficiaryCity = getJsonData(var_CSVData , "$.records["+var_Count+"].BeneficiaryCity");
                 LOGGER.info("Executed Step = STORE,var_BeneficiaryCity,var_CSVData,$.records[{var_Count}].BeneficiaryCity,TGTYPESCREENREG");
        TYPE.$("ELE_TXTBOX_BENEFICIARYCITY","input[type=text][name='outerForm:addressCity']TGWEBCOMMACssSelectorTGWEBCOMMA0",var_BeneficiaryCity,"FALSE","TGTYPESCREENREG");

        SELECT.$("ELE_DRPDWN_BENEFICIARYSTATEANDCOUNTRY","select[name='outerForm:countryAndStateCode']TGWEBCOMMACssSelectorTGWEBCOMMA0",var_BeneficiaryBirthState,"TGTYPESCREENREG");

		String var_BeneficairyZip;
                 LOGGER.info("Executed Step = VAR,String,var_BeneficairyZip,TGTYPESCREENREG");
		var_BeneficairyZip = getJsonData(var_CSVData , "$.records["+var_Count+"].BeneficairyZip");
                 LOGGER.info("Executed Step = STORE,var_BeneficairyZip,var_CSVData,$.records[{var_Count}].BeneficairyZip,TGTYPESCREENREG");
        TYPE.$("ELE_TXTBOX_BENEFICIARYZIP","input[type=text][name='outerForm:zipCode']TGWEBCOMMACssSelectorTGWEBCOMMA0",var_BeneficairyZip,"FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SUBMIT","//input[@type='submit'][@name='outerForm:Submit']","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_CANCEL","//input[@type='submit'][@name='outerForm:Cancel']","TGTYPESCREENREG");

        SELECT.$("ELE_DRPDWN_BENEFICIARYRELATIONSHIP","select[name='outerForm:grid:0:relationship']TGWEBCOMMACssSelectorTGWEBCOMMA0","Beneficiary (Contract)","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SUBMIT","//input[@type='submit'][@name='outerForm:Submit']","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SUBMIT","//input[@type='submit'][@name='outerForm:Submit']","TGTYPESCREENREG");

        TAP.$("ELE_LINK_BENEFICIARYCONTRACT","//span[text()='Beneficiary (Contract)']","TGTYPESCREENREG");

		writeToCSV("Beneficiary Name " , ActionWrapper.getWebElementValueForVariable("span[id='outerForm:clientName']", "CssSelector", 0, "span[id='outerForm:clientName']TGWEBCOMMACssSelectorTGWEBCOMMA0"));
                 LOGGER.info("Executed Step = WRITETOCSV,ELE_GETVAL_CLIENTBENEFICIARYNAME,Beneficiary Name,TGTYPESCREENREG");
		writeToCSV("Beneficiary RelationType " , ActionWrapper.getWebElementValueForVariable("span[id='outerForm:userDefnRelaType']", "CssSelector", 0, "span[id='outerForm:userDefnRelaType']TGWEBCOMMACssSelectorTGWEBCOMMA0"));
                 LOGGER.info("Executed Step = WRITETOCSV,ELE_GETVAL_BENEFIICIARYRELATIONTYPE,Beneficiary RelationType,TGTYPESCREENREG");
		writeToCSV("Beneficiary Percentage " , ActionWrapper.getWebElementValueForVariable("span[id='outerForm:sharePercentageText']", "CssSelector", 0, "span[id='outerForm:sharePercentageText']TGWEBCOMMACssSelectorTGWEBCOMMA0"));
                 LOGGER.info("Executed Step = WRITETOCSV,ELE_GETVAL_BENEFICIARYPERCENTAGE,Beneficiary Percentage,TGTYPESCREENREG");
		var_Count = var_Count + ( 1 );
                 LOGGER.info("Executed Step = INCREASEVAR,var_Count,1,TGTYPESCREENREG");
    	}
          LOGGER.info("Executed Step = WEND,TGTYPESCREENREG");
    	resetAttemps();

    }


    @Test
    public void tc04beneficiarychanges() {

        // No need to declare / print method name here, handled by TestNg listener
        // declare("tc04beneficiarychanges");

		int var_Count;
                 LOGGER.info("Executed Step = VAR,Integer,var_Count,TGTYPESCREENREG");
		var_Count = 0;
                 LOGGER.info("Executed Step = STORE,var_Count,0,TGTYPESCREENREG");
        LOGGER.info("Executed Step = REPEAT IF");
    	while (check(var_Count,"<",1,"TGTYPESCREENREG")) {
          LOGGER.info("Executed Step = WHILE,var_Count,<,1,TGTYPESCREENREG");
        CALL.$("LoginToGIAS36","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_USERNAME","outerForm:usernameTGWEBCOMMAIdTGWEBCOMMA0","GIAS36ILC","FALSE","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_PASSWORD","outerForm:passwordTGWEBCOMMAIdTGWEBCOMMA0","Automation","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_LOGIN","outerForm:loginButtonTGWEBCOMMAIdTGWEBCOMMA0","TGTYPESCREENREG");

        CALL.$("BeneficiaryChanges","TGTYPESCREENREG");

        HOVER.$("ELE_MENU_CLIENT","//td[text()='Client']","TGTYPESCREENREG");

        TAP.$("ELE_SUBMEN_POLICYRELATIONSHIPS","//td[text()='Policy Relationships']","TGTYPESCREENREG");

		com.jayway.jsonpath.ReadContext var_CSVData = getJsonData("C:/GIAS_Automation/3.6 BaseRegression/InputData/36Reg_BenificiaryChanges.csv");
                 LOGGER.info("Executed Step = VAR,Json,var_CSVData,C:/GIAS_Automation/3.6 BaseRegression/InputData/36Reg_BenificiaryChanges.csv,TGTYPESCREENREG");
		String var_PolicyNumber;
                 LOGGER.info("Executed Step = VAR,String,var_PolicyNumber,TGTYPESCREENREG");
		var_PolicyNumber = getJsonData(var_CSVData , "$.records["+var_Count+"].PolicyNumber");
                 LOGGER.info("Executed Step = STORE,var_PolicyNumber,var_CSVData,$.records[{var_Count}].PolicyNumber,TGTYPESCREENREG");
        TYPE.$("ELE_TXTBOX_POLICYNUMBER","//input[@type='text'][@name='outerForm:policyNumber']",var_PolicyNumber,"FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_CONTINUE","//input[@type='submit'][@name='outerForm:Continue']","TGTYPESCREENREG");

        TAP.$("ELE_LINK_BENEFICIARYCONTRACT","//span[text()='Beneficiary (Contract)']","TGTYPESCREENREG");

		writeToCSV("Policy Number " , ActionWrapper.getWebElementValueForVariable("span[id='outerForm:controlNumberOne']", "CssSelector", 0, "span[id='outerForm:controlNumberOne']TGWEBCOMMACssSelectorTGWEBCOMMA0"));
                 LOGGER.info("Executed Step = WRITETOCSV,ELE_GETVAL_CLIENTPOLICYNUMBER,Policy Number,TGTYPESCREENREG");
		writeToCSV("Old Beneficiary Name " , ActionWrapper.getWebElementValueForVariable("span[id='outerForm:clientName']", "CssSelector", 0, "span[id='outerForm:clientName']TGWEBCOMMACssSelectorTGWEBCOMMA0"));
                 LOGGER.info("Executed Step = WRITETOCSV,ELE_GETVAL_CLIENTBENEFICIARYNAME,Old Beneficiary Name,TGTYPESCREENREG");
		writeToCSV("Old Beneficiary Relation Type " , ActionWrapper.getWebElementValueForVariable("span[id='outerForm:userDefnRelaType']", "CssSelector", 0, "span[id='outerForm:userDefnRelaType']TGWEBCOMMACssSelectorTGWEBCOMMA0"));
                 LOGGER.info("Executed Step = WRITETOCSV,ELE_GETVAL_BENEFIICIARYRELATIONTYPE,Old Beneficiary Relation Type,TGTYPESCREENREG");
		writeToCSV("Old Beneficiary Percentage " , ActionWrapper.getWebElementValueForVariable("span[id='outerForm:sharePercentageText']", "CssSelector", 0, "span[id='outerForm:sharePercentageText']TGWEBCOMMACssSelectorTGWEBCOMMA0"));
                 LOGGER.info("Executed Step = WRITETOCSV,ELE_GETVAL_BENEFICIARYPERCENTAGE,Old Beneficiary Percentage,TGTYPESCREENREG");
        TAP.$("ELE_BUTTON_UPDATE","//input[@type='submit'][@name='outerForm:Update']","TGTYPESCREENREG");

        TAP.$("ELE_LINK_SELECTCLIENT","//span[text()='Select Client']","TGTYPESCREENREG");

        TAP.$("ELE_RECBTN_ADD","//input[@type='submit'][@name='outerForm:Add']","TGTYPESCREENREG");

		String var_BeneficiaryFirstName;
                 LOGGER.info("Executed Step = VAR,String,var_BeneficiaryFirstName,TGTYPESCREENREG");
		var_BeneficiaryFirstName = getJsonData(var_CSVData , "$.records["+var_Count+"].BeneficiaryFirstName");
                 LOGGER.info("Executed Step = STORE,var_BeneficiaryFirstName,var_CSVData,$.records[{var_Count}].BeneficiaryFirstName,TGTYPESCREENREG");
		String var_BeneficiaryLastName;
                 LOGGER.info("Executed Step = VAR,String,var_BeneficiaryLastName,TGTYPESCREENREG");
		var_BeneficiaryLastName = getJsonData(var_CSVData , "$.records["+var_Count+"].BeneficiaryLastName");
                 LOGGER.info("Executed Step = STORE,var_BeneficiaryLastName,var_CSVData,$.records[{var_Count}].BeneficiaryLastName,TGTYPESCREENREG");
		String var_BeneficiaryBirthState;
                 LOGGER.info("Executed Step = VAR,String,var_BeneficiaryBirthState,TGTYPESCREENREG");
		var_BeneficiaryBirthState = getJsonData(var_CSVData , "$.records["+var_Count+"].BeneficiaryBirthState");
                 LOGGER.info("Executed Step = STORE,var_BeneficiaryBirthState,var_CSVData,$.records[{var_Count}].BeneficiaryBirthState,TGTYPESCREENREG");
		String var_BeneficiaryDOB;
                 LOGGER.info("Executed Step = VAR,String,var_BeneficiaryDOB,TGTYPESCREENREG");
		var_BeneficiaryDOB = getJsonData(var_CSVData , "$.records["+var_Count+"].BeneficiaryDOB");
                 LOGGER.info("Executed Step = STORE,var_BeneficiaryDOB,var_CSVData,$.records[{var_Count}].BeneficiaryDOB,TGTYPESCREENREG");
		String var_BeneficiaryGender;
                 LOGGER.info("Executed Step = VAR,String,var_BeneficiaryGender,TGTYPESCREENREG");
		var_BeneficiaryGender = getJsonData(var_CSVData , "$.records["+var_Count+"].BeneficiaryGender");
                 LOGGER.info("Executed Step = STORE,var_BeneficiaryGender,var_CSVData,$.records[{var_Count}].BeneficiaryGender,TGTYPESCREENREG");
		String var_BeneficiaryIDType;
                 LOGGER.info("Executed Step = VAR,String,var_BeneficiaryIDType,TGTYPESCREENREG");
		var_BeneficiaryIDType = getJsonData(var_CSVData , "$.records["+var_Count+"].BeneficiaryIDType");
                 LOGGER.info("Executed Step = STORE,var_BeneficiaryIDType,var_CSVData,$.records[{var_Count}].BeneficiaryIDType,TGTYPESCREENREG");
		String var_BeneficiaryIDNumber;
                 LOGGER.info("Executed Step = VAR,String,var_BeneficiaryIDNumber,TGTYPESCREENREG");
		var_BeneficiaryIDNumber = getJsonData(var_CSVData , "$.records["+var_Count+"].BeneficiaryIDNumber");
                 LOGGER.info("Executed Step = STORE,var_BeneficiaryIDNumber,var_CSVData,$.records[{var_Count}].BeneficiaryIDNumber,TGTYPESCREENREG");
        TYPE.$("ELE_TXTBOX_BENEFICIARYFIRSTNAME","input[type=text][name='outerForm:nameFirst']TGWEBCOMMACssSelectorTGWEBCOMMA0",var_BeneficiaryFirstName,"FALSE","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_BENEFICIARYLASTNAME","input[type=text][name='outerForm:nameLast']TGWEBCOMMACssSelectorTGWEBCOMMA0",var_BeneficiaryLastName,"FALSE","TGTYPESCREENREG");

        SELECT.$("ELE_DRPDWN_BENEFICIARYSTATE","select[name='outerForm:birthCountryAndState2']TGWEBCOMMACssSelectorTGWEBCOMMA0",var_BeneficiaryBirthState,"TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_BENEFICIARYDOB","input[type=text][name='outerForm:dateOfBirth2']TGWEBCOMMACssSelectorTGWEBCOMMA0",var_BeneficiaryDOB,"FALSE","TGTYPESCREENREG");

        SELECT.$("ELE_DRPBWN_BENEFICIARYGENDER","select[name='outerForm:sexCode2']TGWEBCOMMACssSelectorTGWEBCOMMA0",var_BeneficiaryGender,"TGTYPESCREENREG");

        SELECT.$("ELE_DRPDWN_BENEFICIARYID","select[name='outerForm:taxIdenUsag2']TGWEBCOMMACssSelectorTGWEBCOMMA0",var_BeneficiaryIDType,"TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_BENEFICIARYIDNUMBER","input[type=text][name='outerForm:identificationNumber2']TGWEBCOMMACssSelectorTGWEBCOMMA0",var_BeneficiaryIDNumber,"FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SUBMIT","//input[@type='submit'][@name='outerForm:Submit']","TGTYPESCREENREG");

		String var_BeneficiaryStreetAddress;
                 LOGGER.info("Executed Step = VAR,String,var_BeneficiaryStreetAddress,TGTYPESCREENREG");
		var_BeneficiaryStreetAddress = getJsonData(var_CSVData , "$.records["+var_Count+"].BeneficiaryStreetAddress");
                 LOGGER.info("Executed Step = STORE,var_BeneficiaryStreetAddress,var_CSVData,$.records[{var_Count}].BeneficiaryStreetAddress,TGTYPESCREENREG");
        TYPE.$("ELE_TXTBOX_BENEFICIARYSTREETADDRESS","input[type=text][name='outerForm:addressLineOne']TGWEBCOMMACssSelectorTGWEBCOMMA0",var_BeneficiaryStreetAddress,"FALSE","TGTYPESCREENREG");

		String var_BeneficiaryCity;
                 LOGGER.info("Executed Step = VAR,String,var_BeneficiaryCity,TGTYPESCREENREG");
		var_BeneficiaryCity = getJsonData(var_CSVData , "$.records["+var_Count+"].BeneficiaryCity");
                 LOGGER.info("Executed Step = STORE,var_BeneficiaryCity,var_CSVData,$.records[{var_Count}].BeneficiaryCity,TGTYPESCREENREG");
        TYPE.$("ELE_TXTBOX_BENEFICIARYCITY","input[type=text][name='outerForm:addressCity']TGWEBCOMMACssSelectorTGWEBCOMMA0",var_BeneficiaryCity,"FALSE","TGTYPESCREENREG");

        SELECT.$("ELE_DRPDWN_BENEFICIARYSTATEANDCOUNTRY","select[name='outerForm:countryAndStateCode']TGWEBCOMMACssSelectorTGWEBCOMMA0",var_BeneficiaryBirthState,"TGTYPESCREENREG");

		String var_BeneficairyZip;
                 LOGGER.info("Executed Step = VAR,String,var_BeneficairyZip,TGTYPESCREENREG");
		var_BeneficairyZip = getJsonData(var_CSVData , "$.records["+var_Count+"].BeneficairyZip");
                 LOGGER.info("Executed Step = STORE,var_BeneficairyZip,var_CSVData,$.records[{var_Count}].BeneficairyZip,TGTYPESCREENREG");
        TYPE.$("ELE_TXTBOX_BENEFICIARYZIP","input[type=text][name='outerForm:zipCode']TGWEBCOMMACssSelectorTGWEBCOMMA0",var_BeneficairyZip,"FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SUBMIT","//input[@type='submit'][@name='outerForm:Submit']","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_CANCEL","//input[@type='submit'][@name='outerForm:Cancel']","TGTYPESCREENREG");

        SELECT.$("ELE_DRPDWN_BENEFICIARYRELATIONSHIP","select[name='outerForm:grid:0:relationship']TGWEBCOMMACssSelectorTGWEBCOMMA0","Beneficiary (Contract)","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SUBMIT","//input[@type='submit'][@name='outerForm:Submit']","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SUBMIT","//input[@type='submit'][@name='outerForm:Submit']","TGTYPESCREENREG");

        TAP.$("ELE_LINK_BENEFICIARYCONTRACT","//span[text()='Beneficiary (Contract)']","TGTYPESCREENREG");

		writeToCSV("Policy Number " , ActionWrapper.getWebElementValueForVariable("span[id='outerForm:controlNumberOne']", "CssSelector", 0, "span[id='outerForm:controlNumberOne']TGWEBCOMMACssSelectorTGWEBCOMMA0"));
                 LOGGER.info("Executed Step = WRITETOCSV,ELE_GETVAL_CLIENTPOLICYNUMBER,Policy Number,TGTYPESCREENREG");
		writeToCSV("New Beneficiary Relation Type " , ActionWrapper.getWebElementValueForVariable("span[id='outerForm:userDefnRelaType']", "CssSelector", 0, "span[id='outerForm:userDefnRelaType']TGWEBCOMMACssSelectorTGWEBCOMMA0"));
                 LOGGER.info("Executed Step = WRITETOCSV,ELE_GETVAL_BENEFIICIARYRELATIONTYPE,New Beneficiary Relation Type,TGTYPESCREENREG");
		writeToCSV("New Beneficiary Percentage " , ActionWrapper.getWebElementValueForVariable("span[id='outerForm:sharePercentageText']", "CssSelector", 0, "span[id='outerForm:sharePercentageText']TGWEBCOMMACssSelectorTGWEBCOMMA0"));
                 LOGGER.info("Executed Step = WRITETOCSV,ELE_GETVAL_BENEFICIARYPERCENTAGE,New Beneficiary Percentage,TGTYPESCREENREG");
		var_Count = var_Count + ( 1 );
                 LOGGER.info("Executed Step = INCREASEVAR,var_Count,1,TGTYPESCREENREG");
    	}
          LOGGER.info("Executed Step = WEND,TGTYPESCREENREG");
    	resetAttemps();

    }


    @Test
    public void tc03addresschanges() {

        // No need to declare / print method name here, handled by TestNg listener
        // declare("tc03addresschanges");

        CALL.$("LoginToGIAS36","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_USERNAME","outerForm:usernameTGWEBCOMMAIdTGWEBCOMMA0","GIAS36ILC","FALSE","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_PASSWORD","outerForm:passwordTGWEBCOMMAIdTGWEBCOMMA0","Automation","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_LOGIN","outerForm:loginButtonTGWEBCOMMAIdTGWEBCOMMA0","TGTYPESCREENREG");

		com.jayway.jsonpath.ReadContext var_CSVData = getJsonData("C:/GIAS_Automation/3.6 BaseRegression/InputData/36Reg_AddressChanges.csv");
                 LOGGER.info("Executed Step = VAR,Json,var_CSVData,C:/GIAS_Automation/3.6 BaseRegression/InputData/36Reg_AddressChanges.csv,TGTYPESCREENREG");
		int var_Count;
                 LOGGER.info("Executed Step = VAR,Integer,var_Count,TGTYPESCREENREG");
		var_Count = 0;
                 LOGGER.info("Executed Step = STORE,var_Count,0,TGTYPESCREENREG");
        LOGGER.info("Executed Step = REPEAT IF");
    	while (check(var_Count,"<",1,"TGTYPESCREENREG")) {
          LOGGER.info("Executed Step = WHILE,var_Count,<,1,TGTYPESCREENREG");
        CALL.$("AddressChanges","TGTYPESCREENREG");

        HOVER.$("ELE_MENU_CLIENT","//td[text()='Client']","TGTYPESCREENREG");

        TAP.$("ELE_SUBMEN_NAMESADDRESSES","//td[text()='Names/Addresses']","TGTYPESCREENREG");

		String var_ClientName;
                 LOGGER.info("Executed Step = VAR,String,var_ClientName,TGTYPESCREENREG");
		var_ClientName = getJsonData(var_CSVData , "$.records["+var_Count+"].ClientName");
                 LOGGER.info("Executed Step = STORE,var_ClientName,var_CSVData,$.records[{var_Count}].ClientName,TGTYPESCREENREG");
		String var_NewStreetName;
                 LOGGER.info("Executed Step = VAR,String,var_NewStreetName,TGTYPESCREENREG");
		var_NewStreetName = getJsonData(var_CSVData , "$.records["+var_Count+"].NewStreetName");
                 LOGGER.info("Executed Step = STORE,var_NewStreetName,var_CSVData,$.records[{var_Count}].NewStreetName,TGTYPESCREENREG");
		String var_NewCity;
                 LOGGER.info("Executed Step = VAR,String,var_NewCity,TGTYPESCREENREG");
		var_NewCity = getJsonData(var_CSVData , "$.records["+var_Count+"].NewCity");
                 LOGGER.info("Executed Step = STORE,var_NewCity,var_CSVData,$.records[{var_Count}].NewCity,TGTYPESCREENREG");
		String var_NewStateandCountry;
                 LOGGER.info("Executed Step = VAR,String,var_NewStateandCountry,TGTYPESCREENREG");
		var_NewStateandCountry = getJsonData(var_CSVData , "$.records["+var_Count+"].NewStateandCountry");
                 LOGGER.info("Executed Step = STORE,var_NewStateandCountry,var_CSVData,$.records[{var_Count}].NewStateandCountry,TGTYPESCREENREG");
		String var_NewZip;
                 LOGGER.info("Executed Step = VAR,String,var_NewZip,TGTYPESCREENREG");
		var_NewZip = getJsonData(var_CSVData , "$.records["+var_Count+"].NewZip");
                 LOGGER.info("Executed Step = STORE,var_NewZip,var_CSVData,$.records[{var_Count}].NewZip,TGTYPESCREENREG");
        TYPE.$("ELE_TXTBOX_CLIENTNAMESEARCH","//input[@id='outerForm:grid:individualName']",var_ClientName,"FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_FINDPOSITIONTO2","//img[@id='outerForm:gnFindImage']","TGTYPESCREENREG");

        TAP.$("ELE_LINK_CLIENTNAMEINGRID","//span[@id='outerForm:grid:0:individualNameOut1']","TGTYPESCREENREG");

        TAP.$("ELE_LINK_ADDRESSINFORMATION","//span[text()='Address Information']","TGTYPESCREENREG");

        TAP.$("ELE_LINK_ADDRESSLINE1","//span[@id='outerForm:grid:0:addressOut1']","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_UPDATE","//input[@type='submit'][@name='outerForm:Update']","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_ADDRESSLINE1","//input[@type='text'][@name='outerForm:addressLineOne']",var_NewStreetName,"FALSE","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_CITY","//input[@type='text'][@name='outerForm:addressCity']",var_NewCity,"FALSE","TGTYPESCREENREG");

        SELECT.$("ELE_DRPDWN_STATECOUNTRY","//select[@name='outerForm:countryAndStateCode']	",var_NewStateandCountry,"TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_ZIPCODE","//input[@type='text'][@name='outerForm:zipCode']",var_NewZip,"FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SUBMIT","//input[@type='submit'][@name='outerForm:Submit']","TGTYPESCREENREG");

        ASSERT.$("ELE_GETMSG_ITEMSUCCESSFULLYUPDATED","//li[@class='MessageInfo']","CONTAINS","successfully","TGTYPESCREENREG");

		var_Count = var_Count + ( 1 );
                 LOGGER.info("Executed Step = INCREASEVAR,var_Count,1,TGTYPESCREENREG");
    	}
          LOGGER.info("Executed Step = WEND,TGTYPESCREENREG");
    	resetAttemps();

    }


    @Test
    public void tc11createnewbusinesspolicy() {

        // No need to declare / print method name here, handled by TestNg listener
        // declare("tc11createnewbusinesspolicy");

        CALL.$("LoginToGIAS36","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_USERNAME","outerForm:usernameTGWEBCOMMAIdTGWEBCOMMA0","GIAS36ILC","FALSE","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_PASSWORD","outerForm:passwordTGWEBCOMMAIdTGWEBCOMMA0","Automation","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_LOGIN","outerForm:loginButtonTGWEBCOMMAIdTGWEBCOMMA0","TGTYPESCREENREG");

		int var_Count = 0;
                 LOGGER.info("Executed Step = VAR,Integer,var_Count,0,TGTYPESCREENREG");
        LOGGER.info("Executed Step = REPEAT IF");
    	while (check(var_Count,"<",1,"TGTYPESCREENREG")) {
          LOGGER.info("Executed Step = WHILE,var_Count,<,1,TGTYPESCREENREG");
		com.jayway.jsonpath.ReadContext var_CSVData = getJsonData("C:/GIAS_Automation/3.6 BaseRegression/InputData/36Reg_CreateAPolicy.csv");
                 LOGGER.info("Executed Step = VAR,Json,var_CSVData,C:/GIAS_Automation/3.6 BaseRegression/InputData/36Reg_CreateAPolicy.csv,TGTYPESCREENREG");
		String var_effectiveDate;
                 LOGGER.info("Executed Step = VAR,String,var_effectiveDate,TGTYPESCREENREG");
		var_effectiveDate = getJsonData(var_CSVData , "$.records["+var_Count+"].EffectiveDate");
                 LOGGER.info("Executed Step = STORE,var_effectiveDate,var_CSVData,$.records[{var_Count}].EffectiveDate,TGTYPESCREENREG");
        CALL.$("AddDistributionChannel","TGTYPESCREENREG");

		String var_DistributionChannelName;
                 LOGGER.info("Executed Step = VAR,String,var_DistributionChannelName,TGTYPESCREENREG");
		var_DistributionChannelName = getJsonData(var_CSVData , "$.records["+var_Count+"].DistributionChannelName");
                 LOGGER.info("Executed Step = STORE,var_DistributionChannelName,var_CSVData,$.records[{var_Count}].DistributionChannelName,TGTYPESCREENREG");
		String var_DistributionChannelDescription;
                 LOGGER.info("Executed Step = VAR,String,var_DistributionChannelDescription,TGTYPESCREENREG");
		var_DistributionChannelDescription = getJsonData(var_CSVData , "$.records["+var_Count+"].DistributionChannelDescription");
                 LOGGER.info("Executed Step = STORE,var_DistributionChannelDescription,var_CSVData,$.records[{var_Count}].DistributionChannelDescription,TGTYPESCREENREG");
        HOVER.$("ELE_MENU_AGENCY","//td[text()='Agency']","TGTYPESCREENREG");

        TAP.$("ELE_SUBMEN_DISTRIBUTIONCHANNEL","//td[text()='Distribution Channel']","TGTYPESCREENREG");

        TAP.$("ELE_RNDBTN_ADD","//img[@id='outerForm:gnAddImage']","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_DISTRIBUTIONCHANNEL","//input[@type='text'][@name='outerForm:distributionChanName']",var_DistributionChannelName,"FALSE","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_DISTRICHANNALDESCRIPTION","input[type=text][name='outerForm:channelDescription']TGWEBCOMMACssSelectorTGWEBCOMMA0",var_DistributionChannelDescription,"FALSE","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_DISTRIBUTIONCHANNELCODE","input[type=text][name='outerForm:duf_dist_code']TGWEBCOMMACssSelectorTGWEBCOMMA0",var_DistributionChannelName,"FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SUBMIT","//input[@type='submit'][@name='outerForm:Submit']","TGTYPESCREENREG");

        ASSERT.$("ELE_GETMSG_SUCCESS","//li[@class='MessageInfo']","CONTAINS","successfully","TGTYPESCREENREG");

        CALL.$("AddAgentRankCode","TGTYPESCREENREG");

        HOVER.$("ELE_MENU_AGENCY","//td[text()='Agency']","TGTYPESCREENREG");

		String var_DistributionChannelDescription2;
                 LOGGER.info("Executed Step = VAR,String,var_DistributionChannelDescription2,TGTYPESCREENREG");
		var_DistributionChannelDescription2 = getJsonData(var_CSVData , "$.records["+var_Count+"].DistributionChannelDescription");
                 LOGGER.info("Executed Step = STORE,var_DistributionChannelDescription2,var_CSVData,$.records[{var_Count}].DistributionChannelDescription,TGTYPESCREENREG");
		String var_AgencyRankDescription;
                 LOGGER.info("Executed Step = VAR,String,var_AgencyRankDescription,TGTYPESCREENREG");
		var_AgencyRankDescription = getJsonData(var_CSVData , "$.records["+var_Count+"].AgencyRankDescription");
                 LOGGER.info("Executed Step = STORE,var_AgencyRankDescription,var_CSVData,$.records[{var_Count}].AgencyRankDescription,TGTYPESCREENREG");
		String var_AgencyRankCode;
                 LOGGER.info("Executed Step = VAR,String,var_AgencyRankCode,TGTYPESCREENREG");
		var_AgencyRankCode = getJsonData(var_CSVData , "$.records["+var_Count+"].AgencyRankCode");
                 LOGGER.info("Executed Step = STORE,var_AgencyRankCode,var_CSVData,$.records[{var_Count}].AgencyRankCode,TGTYPESCREENREG");
        TAP.$("ELE_SUBMEN_AGENTRANKCODES","//td[text()='Agent Rank Codes']","TGTYPESCREENREG");

        TAP.$("ELE_RNDBTN_ADD","//img[@id='outerForm:gnAddImage']","TGTYPESCREENREG");

        SELECT.$("ELE_DRPDWN_DISTRIBUTIONCHANNEL","//select[@name='outerForm:distributionChannel']",var_DistributionChannelDescription2,"TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_AGENCYRANKLEVEL","//input[@type='text'][@name='outerForm:agencyRankLevel']","1","FALSE","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_AGENCYRANKCODE","//input[@type='text'][@name='outerForm:agencyRankCode']",var_AgencyRankCode,"FALSE","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_AGENCYRANKDESCRIPTION","//input[@type='text'][@name='outerForm:description']",var_AgencyRankDescription,"FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SUBMIT","//input[@type='submit'][@name='outerForm:Submit']","TGTYPESCREENREG");

        ASSERT.$("ELE_GETMSG_SUCCESS","//li[@class='MessageInfo']","CONTAINS","successfully","TGTYPESCREENREG");

        CALL.$("AddCommissionScheduleRates","TGTYPESCREENREG");

        HOVER.$("ELE_MENU_AGENCY","//td[text()='Agency']","TGTYPESCREENREG");

		String var_TableName;
                 LOGGER.info("Executed Step = VAR,String,var_TableName,TGTYPESCREENREG");
		var_TableName = getJsonData(var_CSVData , "$.records["+var_Count+"].TableName");
                 LOGGER.info("Executed Step = STORE,var_TableName,var_CSVData,$.records[{var_Count}].TableName,TGTYPESCREENREG");
		String var_TableDescription;
                 LOGGER.info("Executed Step = VAR,String,var_TableDescription,TGTYPESCREENREG");
		var_TableDescription = getJsonData(var_CSVData , "$.records["+var_Count+"].TableDescription");
                 LOGGER.info("Executed Step = STORE,var_TableDescription,var_CSVData,$.records[{var_Count}].TableDescription,TGTYPESCREENREG");
		String var_DistributionChannelDescription3;
                 LOGGER.info("Executed Step = VAR,String,var_DistributionChannelDescription3,TGTYPESCREENREG");
		var_DistributionChannelDescription3 = getJsonData(var_CSVData , "$.records["+var_Count+"].DistributionChannelDescription");
                 LOGGER.info("Executed Step = STORE,var_DistributionChannelDescription3,var_CSVData,$.records[{var_Count}].DistributionChannelDescription,TGTYPESCREENREG");
		String var_AgencyRankDescription3;
                 LOGGER.info("Executed Step = VAR,String,var_AgencyRankDescription3,TGTYPESCREENREG");
		var_AgencyRankDescription3 = getJsonData(var_CSVData , "$.records["+var_Count+"].AgencyRankDescription");
                 LOGGER.info("Executed Step = STORE,var_AgencyRankDescription3,var_CSVData,$.records[{var_Count}].AgencyRankDescription,TGTYPESCREENREG");
        TAP.$("ELE_SUBMEN_COMMISSIONSCHEDULERATES","//td[text()='Commission Schedule Rates']","TGTYPESCREENREG");

        TAP.$("ELE_RECBTN_ADD","//input[@type='submit'][@name='outerForm:Add']","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_TABLENAME","//input[@type='text'][@name='outerForm:tableName']",var_TableName,"FALSE","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_TABLEDESCRIPTION","//input[@type='text'][@name='outerForm:description']",var_TableDescription,"FALSE","TGTYPESCREENREG");

        SELECT.$("ELE_DRPDWN_DISTRIBUTIONCHANNEL","//select[@name='outerForm:distributionChannel']",var_DistributionChannelDescription3,"TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_CONTINUE","//input[@type='submit'][@name='outerForm:Continue']","TGTYPESCREENREG");

        SELECT.$("ELE_DRPPDWN_AGENCYRANKCODE","//select[@name='outerForm:grid:0:agencyRankCode']",var_AgencyRankDescription,"TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_ENDDURATION0","//input[@type='text'][@name='outerForm:grid:endingDuration1']","1","FALSE","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_ENDDURATION1","//input[@type='text'][@name='outerForm:grid:endingDuration2']","5","FALSE","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_ENDDURATION2","//input[@type='text'][@name='outerForm:grid:endingDuration3']","120","FALSE","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_COMMISSIONRATE0","//input[@type='text'][@name='outerForm:grid:0:commissionRate1']","20","FALSE","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_COMMISSIONRATE1","//input[@type='text'][@name='outerForm:grid:0:commissionRate2']","15","FALSE","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_COMMISSIONRATE2","//input[@type='text'][@name='outerForm:grid:0:commissionRate3']","10","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SUBMIT","//input[@type='submit'][@name='outerForm:Submit']","TGTYPESCREENREG");

        ASSERT.$("ELE_GETMSG_SUCCESS","//li[@class='MessageInfo']","CONTAINS","successfully","TGTYPESCREENREG");

        CALL.$("AddCommissionContract","TGTYPESCREENREG");

        HOVER.$("ELE_MENU_AGENCY","//td[text()='Agency']","TGTYPESCREENREG");

		String var_CommissionContractNumber;
                 LOGGER.info("Executed Step = VAR,String,var_CommissionContractNumber,TGTYPESCREENREG");
		var_CommissionContractNumber = getJsonData(var_CSVData , "$.records["+var_Count+"].CommissionContractNumber");
                 LOGGER.info("Executed Step = STORE,var_CommissionContractNumber,var_CSVData,$.records[{var_Count}].CommissionContractNumber,TGTYPESCREENREG");
		String var_CommissionContractDescription;
                 LOGGER.info("Executed Step = VAR,String,var_CommissionContractDescription,TGTYPESCREENREG");
		var_CommissionContractDescription = getJsonData(var_CSVData , "$.records["+var_Count+"].CommissionContractDescription");
                 LOGGER.info("Executed Step = STORE,var_CommissionContractDescription,var_CSVData,$.records[{var_Count}].CommissionContractDescription,TGTYPESCREENREG");
		String var_DistributionChannelDescription4;
                 LOGGER.info("Executed Step = VAR,String,var_DistributionChannelDescription4,TGTYPESCREENREG");
		var_DistributionChannelDescription4 = getJsonData(var_CSVData , "$.records["+var_Count+"].DistributionChannelDescription");
                 LOGGER.info("Executed Step = STORE,var_DistributionChannelDescription4,var_CSVData,$.records[{var_Count}].DistributionChannelDescription,TGTYPESCREENREG");
		String var_AgencyRankDescription4;
                 LOGGER.info("Executed Step = VAR,String,var_AgencyRankDescription4,TGTYPESCREENREG");
		var_AgencyRankDescription4 = getJsonData(var_CSVData , "$.records["+var_Count+"].AgencyRankDescription");
                 LOGGER.info("Executed Step = STORE,var_AgencyRankDescription4,var_CSVData,$.records[{var_Count}].AgencyRankDescription,TGTYPESCREENREG");
        TAP.$("ELE_SUBMEN_COMMISSIONCONTRACTS","//td[text()='Commission Contracts']","TGTYPESCREENREG");

        TAP.$("ELE_RNDBTN_ADD","//img[@id='outerForm:gnAddImage']","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_COMMSSIONCONTRACTNUMBER","//input[@type='text'][@name='outerForm:commissionContNumb']",var_CommissionContractNumber,"FALSE","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_COMMISSIONCONTRACTDESCRIPTION","//input[@type='text'][@name='outerForm:longDescription']",var_CommissionContractDescription,"FALSE","TGTYPESCREENREG");

        SELECT.$("ELE_DRPDWN_DISTRIBUTIONCHANNEL","//select[@name='outerForm:distributionChannel']",var_DistributionChannelDescription4,"TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_EFFECTIVEDATE","//input[@type='text'][@name='outerForm:effectiveDate']",var_effectiveDate,"TRUE","TGTYPESCREENREG");

        SELECT.$("ELE_DRPDWN_PAYBACKMETHOD","//select[@name='outerForm:advancePaybackMethod']","100% of Commission Earned","TGTYPESCREENREG");

        SELECT.$("ELE_DRPDWN_ADVANCETHROUGHAGENTRANK","//select[@name='outerForm:topLevelForAdvance']",var_AgencyRankDescription4,"TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_DAYSPASTDUE","//input[@type='text'][@name='outerForm:daysLateBeforeChar']","3","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SUBMIT","//input[@type='submit'][@name='outerForm:Submit']","TGTYPESCREENREG");

        ASSERT.$("ELE_GETMSG_SUCCESS","//li[@class='MessageInfo']","CONTAINS","successfully","TGTYPESCREENREG");

        CALL.$("AddCommissionSchedule","TGTYPESCREENREG");

        HOVER.$("ELE_MENU_AGENCY","//td[text()='Agency']","TGTYPESCREENREG");

		String var_CommissionContractNumber5;
                 LOGGER.info("Executed Step = VAR,String,var_CommissionContractNumber5,TGTYPESCREENREG");
		var_CommissionContractNumber5 = getJsonData(var_CSVData , "$.records["+var_Count+"].CommissionContractNumber");
                 LOGGER.info("Executed Step = STORE,var_CommissionContractNumber5,var_CSVData,$.records[{var_Count}].CommissionContractNumber,TGTYPESCREENREG");
		String var_TableDescription5;
                 LOGGER.info("Executed Step = VAR,String,var_TableDescription5,TGTYPESCREENREG");
		var_TableDescription5 = getJsonData(var_CSVData , "$.records["+var_Count+"].TableDescription");
                 LOGGER.info("Executed Step = STORE,var_TableDescription5,var_CSVData,$.records[{var_Count}].TableDescription,TGTYPESCREENREG");
		String var_LineDescription;
                 LOGGER.info("Executed Step = VAR,String,var_LineDescription,TGTYPESCREENREG");
		var_LineDescription = getJsonData(var_CSVData , "$.records["+var_Count+"].LineDescription");
                 LOGGER.info("Executed Step = STORE,var_LineDescription,var_CSVData,$.records[{var_Count}].LineDescription,TGTYPESCREENREG");
        TAP.$("ELE_SUBMEN_COMMISSIONCONTRACTS","//td[text()='Commission Contracts']","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_SEACHCOMMISSIONCONTRACTNO","//input[@type='text'][@name='outerForm:grid:commissionContNumb']",var_CommissionContractNumber5,"FALSE","TGTYPESCREENREG");

        TAP.$("ELE_RNDBTN_FINDPOSITIONTO","//img[@id='outerForm:gnFindImage']","TGTYPESCREENREG");

        TAP.$("ELE_LINK_SEARCHEDFIRSTCOMMISSIONCONTRACT","span[id='outerForm:grid:0:commissionContNumbOut1']TGWEBCOMMACssSelectorTGWEBCOMMA0","TGTYPESCREENREG");

        TAP.$("ELE_LINK_COMMISSIONSCHEDULE","//span[text()='Commission Schedule']","TGTYPESCREENREG");

        TAP.$("ELE_RNDBTN_ADD","//img[@id='outerForm:gnAddImage']","TGTYPESCREENREG");

        SELECT.$("ELE_DRPDWN_CONTRACTTYPE","//select[@name='outerForm:commissionContType']","Health","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_EFFECTIVEDATE","//input[@type='text'][@name='outerForm:effectiveDate']",var_effectiveDate,"TRUE","TGTYPESCREENREG");

        SELECT.$("ELE_DRPDWN_COMMISSIONRATETABLENAME","//select[@name='outerForm:commissionRateTableName']",var_TableDescription5,"TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_LINEDESCRIPTION","//input[@type='text'][@name='outerForm:shortDescription']",var_LineDescription,"FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SUBMIT","//input[@type='submit'][@name='outerForm:Submit']","TGTYPESCREENREG");

        ASSERT.$("ELE_GETMSG_SUCCESS","//li[@class='MessageInfo']","CONTAINS","successfully","TGTYPESCREENREG");

        CALL.$("AddNewBusinessPolicy","TGTYPESCREENREG");

        HOVER.$("ELE_MENU_NEWBUSINESS","//td[text()='New Business']","TGTYPESCREENREG");

        TAP.$("ELE_SUBMEN_APPLICATIONENTRYUPDATE","//td[text()='Application Entry/Update']","TGTYPESCREENREG");

        TAP.$("ELE_RNDBTN_ADD","//img[@id='outerForm:gnAddImage']","TGTYPESCREENREG");

		String var_PolicyNumber;
                 LOGGER.info("Executed Step = VAR,String,var_PolicyNumber,TGTYPESCREENREG");
		var_PolicyNumber = getJsonData(var_CSVData , "$.records["+var_Count+"].PolicyNumber");
                 LOGGER.info("Executed Step = STORE,var_PolicyNumber,var_CSVData,$.records[{var_Count}].PolicyNumber,TGTYPESCREENREG");
		String var_FirstName;
                 LOGGER.info("Executed Step = VAR,String,var_FirstName,TGTYPESCREENREG");
		var_FirstName = getJsonData(var_CSVData , "$.records["+var_Count+"].FirstName");
                 LOGGER.info("Executed Step = STORE,var_FirstName,var_CSVData,$.records[{var_Count}].FirstName,TGTYPESCREENREG");
		String var_LastName;
                 LOGGER.info("Executed Step = VAR,String,var_LastName,TGTYPESCREENREG");
		var_LastName = getJsonData(var_CSVData , "$.records["+var_Count+"].LastName");
                 LOGGER.info("Executed Step = STORE,var_LastName,var_CSVData,$.records[{var_Count}].LastName,TGTYPESCREENREG");
		String var_IDNumber;
                 LOGGER.info("Executed Step = VAR,String,var_IDNumber,TGTYPESCREENREG");
		var_IDNumber = getJsonData(var_CSVData , "$.records["+var_Count+"].IDNumber");
                 LOGGER.info("Executed Step = STORE,var_IDNumber,var_CSVData,$.records[{var_Count}].IDNumber,TGTYPESCREENREG");
		String var_ClientSearchName;
                 LOGGER.info("Executed Step = VAR,String,var_ClientSearchName,TGTYPESCREENREG");
		var_ClientSearchName = getJsonData(var_CSVData , "$.records["+var_Count+"].ClientSearchName");
                 LOGGER.info("Executed Step = STORE,var_ClientSearchName,var_CSVData,$.records[{var_Count}].ClientSearchName,TGTYPESCREENREG");
		String var_AgentNumber;
                 LOGGER.info("Executed Step = VAR,String,var_AgentNumber,TGTYPESCREENREG");
		var_AgentNumber = getJsonData(var_CSVData , "$.records["+var_Count+"].AgentNumber");
                 LOGGER.info("Executed Step = STORE,var_AgentNumber,var_CSVData,$.records[{var_Count}].AgentNumber,TGTYPESCREENREG");
        TYPE.$("ELE_TXTBOX_POLICYNUMBER","//input[@type='text'][@name='outerForm:policyNumber']",var_PolicyNumber,"FALSE","TGTYPESCREENREG");

        SELECT.$("ELE_DRPDWN_CURRENCYCODE","//select[@id='outerForm:currencyCode']","Dollars [US]","TGTYPESCREENREG");

        SELECT.$("ELE_DRPDWN_STATECOUNTRY","//select[@name='outerForm:countryAndStateCode']	","Missouri US","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_EFFECTIVEDATE","//input[@type='text'][@name='outerForm:effectiveDate']",var_effectiveDate,"TRUE","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_CASHWITHAPPLICATION","//input[@type='text'][@name='outerForm:cashWithApplication']","25000","FALSE","TGTYPESCREENREG");

        SELECT.$("ELE_DRPDWN_PAYMENTFREQUENCY","//select[@name='outerForm:paymentMode']","Annual","TGTYPESCREENREG");

        SELECT.$("ELE_DRPDWN_PAYMENTMETHOD","//select[@name='outerForm:paymentCode']","Coupon Book","TGTYPESCREENREG");

        SELECT.$("ELE_DRPDWN_TAXQUALIFIEDDESCRIPTION","//select[@name='outerForm:taxQualifiedCode']","NON QUALIFIED","TGTYPESCREENREG");

        SELECT.$("ELE_DRPDWN_TAXWITHHOLDING","//select[@name='outerForm:taxWithholdingCode']","No Withholding","TGTYPESCREENREG");

        TAP.$("ELE_LINK_SELECTCLIENT","//span[text()='Select Client']","TGTYPESCREENREG");

        TAP.$("ELE_RNDBTN_ADD","//img[@id='outerForm:gnAddImage']","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_FIRSTNAME","//input[@type='text'][@name='outerForm:nameFirst']",var_FirstName,"FALSE","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_LASTNAME","//input[@type='text'][@name='outerForm:nameLast']",var_LastName,"FALSE","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_CLIENTDOB","//input[@type='text'][@name='outerForm:dateOfBirth2']","01/01/1980","FALSE","TGTYPESCREENREG");

        SELECT.$("ELE_DRPDWN_CLIENTGENDER","//select[@name='outerForm:sexCode2']","Male","TGTYPESCREENREG");

        SELECT.$("ELE_DRPDWN_IDTYPE","//select[@name='outerForm:taxIdenUsag2']","Social Security Number","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_IDNUMBER","//input[@type='text'][@name='outerForm:identificationNumber2']",var_IDNumber,"FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SUBMIT","//input[@type='submit'][@name='outerForm:Submit']","TGTYPESCREENREG");

        ASSERT.$("ELE_GETMSG_SUCCESS","//li[@class='MessageInfo']","CONTAINS","successfully","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_ADDRESSLINE1","//input[@type='text'][@name='outerForm:addressLineOne']","123 Main St","FALSE","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_CITY","//input[@type='text'][@name='outerForm:addressCity']","Kansas City","FALSE","TGTYPESCREENREG");

        SELECT.$("ELE_DRPDWN_STATECOUNTRY","//select[@name='outerForm:countryAndStateCode']	","Missouri US","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_ZIPCODE","//input[@type='text'][@name='outerForm:zipCode']","64153","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SUBMIT","//input[@type='submit'][@name='outerForm:Submit']","TGTYPESCREENREG");

        ASSERT.$("ELE_GETMSG_SUCCESS","//li[@class='MessageInfo']","CONTAINS","successfully","TGTYPESCREENREG");

        TAP.$("ELE_LINK_SELECTCLIENTRELATIONSHIPS","//a[text()='Select Client Relationships']","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_SEARCHCLIENTNAME","//input[@type='text'][@name='outerForm:grid:individualName']",var_ClientSearchName,"FALSE","TGTYPESCREENREG");

        TAP.$("ELE_RNDBTN_FINDPOSITIONTO","//img[@id='outerForm:gnFindImage']","TGTYPESCREENREG");

        SELECT.$("ELE_DRPDWN_RELATIONSHIPGRID","//select[@name='outerForm:grid:0:relationship']","Owner 1","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SUBMIT","//input[@type='submit'][@name='outerForm:Submit']","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_AGENTNUMBER","//input[@type='text'][@name='outerForm:grid:0:agentNumberOut2']",var_AgentNumber,"FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SUBMIT","//input[@type='submit'][@name='outerForm:Submit']","TGTYPESCREENREG");

        ASSERT.$("ELE_GETMSG_SUCCESS","//li[@class='MessageInfo']","CONTAINS","successfully","TGTYPESCREENREG");

        CALL.$("SettlePolicy","TGTYPESCREENREG");

        TAP.$("ELE_LINK_BENEFITS","//span[text()='Benefits']","TGTYPESCREENREG");

		String var_BenefitPlan = "null";
                 LOGGER.info("Executed Step = VAR,String,var_BenefitPlan,null,TGTYPESCREENREG");
		var_BenefitPlan = getJsonData(var_CSVData , "$.records["+var_Count+"].BenefitPlan");
                 LOGGER.info("Executed Step = STORE,var_BenefitPlan,var_CSVData,$.records[{var_Count}].BenefitPlan,TGTYPESCREENREG");
		String var_universalBenefitPlan = "ULAT1";
                 LOGGER.info("Executed Step = VAR,String,var_universalBenefitPlan,ULAT1,TGTYPESCREENREG");
        SELECT.$("ELE_DRPDWN_ADDABENEFIT","//select[@name='outerForm:initialPlanCode']",var_BenefitPlan,"TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_CONTINUE","//input[@type='submit'][@name='outerForm:Continue']","TGTYPESCREENREG");

        LOGGER.info("Executed Step = START IF");
        if (check(var_BenefitPlan,"CONTAINS",var_universalBenefitPlan,"TGTYPESCREENREG")) {
         LOGGER.info("Executed Step = IF,var_BenefitPlan,CONTAINS,var_universalBenefitPlan,TGTYPESCREENREG");
        TYPE.$("ELE_TXTBOX_UNITSAFTERADDINGBENEFIT","//input[@type='text'][@name='outerForm:unitsOfInsurance']	","250.000","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_RDOBTN_FACEAMOUNT","//input[@id='outerForm:deathBenefitOption:0']","TGTYPESCREENREG");

        SWIPE.$("UP","TGTYPESCREENREG");

        }
         else {
          LOGGER.info("Executed Step = ELSE,TGTYPESCREENREG");
        TYPE.$("ELE_TXTBOX_UNITSAFTERADDINGBENEFIT","//input[@type='text'][@name='outerForm:unitsOfInsurance']	","25.00","TRUE","TGTYPESCREENREG");

		try { 
		  List<WebElement> checkBoxes = driver.findElements(By.xpath("//input[@type='checkbox']"));
				int numberOfBoxes =  checkBoxes.size();
				if( numberOfBoxes > 0) {
						for(int i=0;i<numberOfBoxes;i++)
						{
					 driver.findElement(By.xpath("//input[@id='outerForm:gridSupp:" + i + ":selectedCheck']")).click();

		 if(driver.findElements(By.xpath("//input[@id='outerForm:gridSupp:"+ i + ":unitsOut2']")).size()!= 0)
			 {	 
				driver.findElement(By.xpath("//input[@id='outerForm:gridSupp:" + i + ":unitsOut2']")).clear();
				 driver.findElement(By.xpath("//input[@id='outerForm:gridSupp:" + i + ":unitsOut2']")).sendKeys("25.00");
			}
			else { System.out.println("Not found"); }	
			}
		}
		 
		 ;} 
		 catch(Exception e) { PrintVal("Renderer Script Exception : "+e.toString()); } 
		 PrintVal("Executed Step = CALLRENDERERSCRIPT,COUNTSUPPLEMENTCHECKBOX"); 
        SWIPE.$("UP","TGTYPESCREENREG");

        }
          LOGGER.info("Executed Step = ENDIF,TGTYPESCREENREG");
        TAP.$("ELE_BUTTON_SUBMIT","//input[@type='submit'][@name='outerForm:Submit']","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_CANCEL","//input[@type='submit'][@name='outerForm:Cancel']","TGTYPESCREENREG");

        TAP.$("ELE_LINK_ISSUEDECLINEWITHDRAW","//span[text()='Issue/Decline/Withdraw']","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_EFFECTIVEDATE","//input[@type='text'][@name='outerForm:effectiveDate']",var_effectiveDate,"TRUE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_ISSUE","//input[@type='submit'][@name='outerForm:Issue']	","TGTYPESCREENREG");

        TAP.$("ELE_LINK_SETTLE","//span[text()='Settle']	","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SUBMIT","//input[@type='submit'][@name='outerForm:Submit']","TGTYPESCREENREG");

        ASSERT.$("ELE_GETMSG_SUCCESS","//li[@class='MessageInfo']","CONTAINS","successfully","TGTYPESCREENREG");

        CALL.$("WritePolicyQuickInquiryToCSV","TGTYPESCREENREG");

        HOVER.$("ELE_MENU_INQUIRY","//td[text()='Inquiry']","TGTYPESCREENREG");

        TAP.$("ELE_SUBMEN_QUICKINQUIRY","//td[contains(text(),'Quick Inquiry')]","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_POLICYNUMBER","//input[@type='text'][@name='outerForm:policyNumber']",var_PolicyNumber,"FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_CONTINUE","//input[@type='submit'][@name='outerForm:Continue']","TGTYPESCREENREG");

		writeToCSV ("Policy Number " , ActionWrapper.getElementValue("ELE_GETVAL_POLICYNUMBER", "//span[@id='outerForm:policyNumber']"));
                 LOGGER.info("Executed Step = WRITETOCSV,ELE_GETVAL_POLICYNUMBER,Policy Number,TGTYPESCREENREG");
		writeToCSV ("Status " , ActionWrapper.getElementValue("ELE_GETVAL_STATUS", "//span[@id='outerForm:statusText']"));
                 LOGGER.info("Executed Step = WRITETOCSV,ELE_GETVAL_STATUS,Status,TGTYPESCREENREG");
		writeToCSV ("Effective Date " , ActionWrapper.getElementValue("ELE_GETVAL_EFFECTIVEDATETEXTFROMGRID", "//span[@id='outerForm:grid:0:effectiveDateOut2']"));
                 LOGGER.info("Executed Step = WRITETOCSV,ELE_GETVAL_EFFECTIVEDATETEXTFROMGRID,Effective Date,TGTYPESCREENREG");
		writeToCSV ("State country " , ActionWrapper.getElementValue("ELE_GETVAL_STATECOUNTRY", "//span[@id='outerForm:ownerCityStateZip']"));
                 LOGGER.info("Executed Step = WRITETOCSV,ELE_GETVAL_STATECOUNTRY,State country,TGTYPESCREENREG");
		writeToCSV ("Agent Number " , ActionWrapper.getElementValue("ELE_GETVAL_AGENTNUMBER", "//span[@id='outerForm:agentNumber']"));
                 LOGGER.info("Executed Step = WRITETOCSV,ELE_GETVAL_AGENTNUMBER,Agent Number,TGTYPESCREENREG");
		var_Count = var_Count + ( 1 );
                 LOGGER.info("Executed Step = INCREASEVAR,var_Count,1,TGTYPESCREENREG");
    	}
          LOGGER.info("Executed Step = WEND,TGTYPESCREENREG");
    	resetAttemps();

    }


    @Test
    public void tc01partialsurrender() {

        // No need to declare / print method name here, handled by TestNg listener
        // declare("tc01partialsurrender");

        CALL.$("LoginToGIAS36","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_USERNAME","outerForm:usernameTGWEBCOMMAIdTGWEBCOMMA0","GIAS36ILC","FALSE","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_PASSWORD","outerForm:passwordTGWEBCOMMAIdTGWEBCOMMA0","Automation","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_LOGIN","outerForm:loginButtonTGWEBCOMMAIdTGWEBCOMMA0","TGTYPESCREENREG");

		int var_Count;
                 LOGGER.info("Executed Step = VAR,Integer,var_Count,TGTYPESCREENREG");
		var_Count = 0;
                 LOGGER.info("Executed Step = STORE,var_Count,0,TGTYPESCREENREG");
        LOGGER.info("Executed Step = REPEAT IF");
    	while (check(var_Count,"<",1,"TGTYPESCREENREG")) {
          LOGGER.info("Executed Step = WHILE,var_Count,<,1,TGTYPESCREENREG");
        CALL.$("PartialSurrender5","TGTYPESCREENREG");

		com.jayway.jsonpath.ReadContext var_CSVData = getJsonData("C:/GIAS_Automation/3.6 BaseRegression/InputData/36Reg_PartialSurrender.csv");
                 LOGGER.info("Executed Step = VAR,Json,var_CSVData,C:/GIAS_Automation/3.6 BaseRegression/InputData/36Reg_PartialSurrender.csv,TGTYPESCREENREG");
		String var_PolicyNumber = "null";
                 LOGGER.info("Executed Step = VAR,String,var_PolicyNumber,null,TGTYPESCREENREG");
		var_PolicyNumber = getJsonData(var_CSVData , "$.records["+var_Count+"].PolicyNumber");
                 LOGGER.info("Executed Step = STORE,var_PolicyNumber,var_CSVData,$.records[{var_Count}].PolicyNumber,TGTYPESCREENREG");
		String var_RequestedPartialSurrenderAmount = "null";
                 LOGGER.info("Executed Step = VAR,String,var_RequestedPartialSurrenderAmount,null,TGTYPESCREENREG");
		var_RequestedPartialSurrenderAmount = getJsonData(var_CSVData , "$.records["+var_Count+"].RequestedPartialSurrenderAmount");
                 LOGGER.info("Executed Step = STORE,var_RequestedPartialSurrenderAmount,var_CSVData,$.records[{var_Count}].RequestedPartialSurrenderAmount,TGTYPESCREENREG");
		String var_PartialSurrenderAmtReqType = "null";
                 LOGGER.info("Executed Step = VAR,String,var_PartialSurrenderAmtReqType,null,TGTYPESCREENREG");
		var_PartialSurrenderAmtReqType = getJsonData(var_CSVData , "$.records["+var_Count+"].PartialSurrenderAmtReqType");
                 LOGGER.info("Executed Step = STORE,var_PartialSurrenderAmtReqType,var_CSVData,$.records[{var_Count}].PartialSurrenderAmtReqType,TGTYPESCREENREG");
		writeToCSV("Requested Partial Surrender Amnt " , var_RequestedPartialSurrenderAmount);
                 LOGGER.info("Executed Step = WRITETOCSV,var_RequestedPartialSurrenderAmount,Requested Partial Surrender Amnt,TGTYPESCREENREG");
		int var_minimumSurrenderAmount = 100;
                 LOGGER.info("Executed Step = VAR,Integer,var_minimumSurrenderAmount,100,TGTYPESCREENREG");
        LOGGER.info("Executed Step = START IF");
        if (check(var_RequestedPartialSurrenderAmount,">=",var_minimumSurrenderAmount,"TGTYPESCREENREG")) {
         LOGGER.info("Executed Step = IF,var_RequestedPartialSurrenderAmount,>=,var_minimumSurrenderAmount,TGTYPESCREENREG");
        HOVER.$("ELE_MENU_INQUIRY","//td[text()='Inquiry']","TGTYPESCREENREG");

        TAP.$("ELE_SUBMEN_POLICYDETAIL","//td[text()='Policy Detail']","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_POLICYNUMBER","//input[@type='text'][@name='outerForm:policyNumber']",var_PolicyNumber,"FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_CONTINUE","//input[@type='submit'][@name='outerForm:Continue']","TGTYPESCREENREG");

        TAP.$("ELE_LINK_POLICYVALUES","//span[contains(text(),'Policy Values')]","TGTYPESCREENREG");

		int var_MaxAllowedSurrenderAmnt;
                 LOGGER.info("Executed Step = VAR,Integer,var_MaxAllowedSurrenderAmnt,TGTYPESCREENREG");
		var_MaxAllowedSurrenderAmnt = ActionWrapper.getElementValueForVariableWithInt("ELE_GETVAL_POLICYPRINCIPALVALUE", "//span[@id='outerForm:policyPrinCashValue']");
                 LOGGER.info("Executed Step = STORE,var_MaxAllowedSurrenderAmnt,ELE_GETVAL_POLICYPRINCIPALVALUE,TGTYPESCREENREG");
		String var_ActualSurrenderAmnttoOwner = "null";
                 LOGGER.info("Executed Step = VAR,String,var_ActualSurrenderAmnttoOwner,null,TGTYPESCREENREG");
		writeToCSV ("Max Allowed Surrender Amnt " , ActionWrapper.getElementValue("ELE_GETVAL_POLICYPRINCIPALVALUE", "//span[@id='outerForm:policyPrinCashValue']"));
                 LOGGER.info("Executed Step = WRITETOCSV,ELE_GETVAL_POLICYPRINCIPALVALUE,Max Allowed Surrender Amnt,TGTYPESCREENREG");
        LOGGER.info("Executed Step = START IF");
        if (check(var_MaxAllowedSurrenderAmnt,">",var_RequestedPartialSurrenderAmount,"TGTYPESCREENREG")) {
         LOGGER.info("Executed Step = IF,var_MaxAllowedSurrenderAmnt,>,var_RequestedPartialSurrenderAmount,TGTYPESCREENREG");
        HOVER.$("ELE_MENU_POLICYOWNERSERVICES","//td[@class='ThemePanelMainFolderText'][contains(text(),'Policyowner Services')]","TGTYPESCREENREG");

        HOVER.$("ELE_SUBMEN_SURRENDER","//td[@class='ThemePanelMenuFolderText'][contains(text(),'Surrender')]","TGTYPESCREENREG");

        TAP.$("ELE_SUBMEN_FULLPARTIALSURRENDER","//td[contains(text(),'Full/Partial Surrender')]","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_POLICYNUMBER","//input[@type='text'][@name='outerForm:policyNumber']",var_PolicyNumber,"FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_PARTIALSURRENDER","//input[@id='outerForm:PartialSurrender']","TGTYPESCREENREG");

        TAP.$("ELE_LINK_BASE","//span[contains(text(),'Base')]","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_REQUESTEDSURRENDERAMOUNTPARTIAL","//input[@id='outerForm:requestedSurrenderAmount_partial']",var_RequestedPartialSurrenderAmount,"FALSE","TGTYPESCREENREG");

		String var_RequestType = "Net";
                 LOGGER.info("Executed Step = VAR,String,var_RequestType,Net,TGTYPESCREENREG");
        LOGGER.info("Executed Step = START IF");
        if (check(var_PartialSurrenderAmtReqType,"CONTAINS",var_RequestType,"TGTYPESCREENREG")) {
         LOGGER.info("Executed Step = IF,var_PartialSurrenderAmtReqType,CONTAINS,var_RequestType,TGTYPESCREENREG");
        TAP.$("ELE_RDOBTN_NETPARTIALSURRENDERREQTYPE","//input[@id='outerForm:requestType:1']","TGTYPESCREENREG");

        TAP.$("ELE_LINK_DISTRIBUTIONINFORMATION","//span[@id='outerForm:DistributionInformationText']","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_CONTINUE","//input[@type='submit'][@name='outerForm:Continue']","TGTYPESCREENREG");

		var_ActualSurrenderAmnttoOwner = ActionWrapper.getElementValue("ELE_GETVAL_SURRENDERAMOUNTTOOWNER", "//span[@id='outerForm:amountToOwner_partial']");
                 LOGGER.info("Executed Step = STORE,var_ActualSurrenderAmnttoOwner,ELE_GETVAL_SURRENDERAMOUNTTOOWNER,TGTYPESCREENREG");
		writeToCSV ("Policy Number " , ActionWrapper.getElementValue("ELE_GETVAL_POLICYNUMBER", "//span[@id='outerForm:policyNumber']"));
                 LOGGER.info("Executed Step = WRITETOCSV,ELE_GETVAL_POLICYNUMBER,Policy Number,TGTYPESCREENREG");
		writeToCSV ("Plan " , ActionWrapper.getElementValue("ELE_GETVAL_SURRENDERPLANPARTIAL", "//span[@id='outerForm:planCode']"));
                 LOGGER.info("Executed Step = WRITETOCSV,ELE_GETVAL_SURRENDERPLANPARTIAL,Plan,TGTYPESCREENREG");
		writeToCSV ("Surrender Type " , ActionWrapper.getElementValue("ELE_GETVAL_SURRENDERTYPEPARTIAL", "//span[@id='outerForm:surrenderTypeText']"));
                 LOGGER.info("Executed Step = WRITETOCSV,ELE_GETVAL_SURRENDERTYPEPARTIAL,Surrender Type,TGTYPESCREENREG");
		writeToCSV("Expected Surrender Amnt to Owner " , var_RequestedPartialSurrenderAmount);
                 LOGGER.info("Executed Step = WRITETOCSV,var_RequestedPartialSurrenderAmount,Expected Surrender Amnt to Owner,TGTYPESCREENREG");
		writeToCSV ("Actual Surrender Amnt to Owner " , ActionWrapper.getElementValue("ELE_GETVAL_SURRENDERAMOUNTTOOWNER", "//span[@id='outerForm:amountToOwner_partial']"));
                 LOGGER.info("Executed Step = WRITETOCSV,ELE_GETVAL_SURRENDERAMOUNTTOOWNER,Actual Surrender Amnt to Owner,TGTYPESCREENREG");
		writeToCSV("Surrender Amount Request Type " , var_PartialSurrenderAmtReqType);
                 LOGGER.info("Executed Step = WRITETOCSV,var_PartialSurrenderAmtReqType,Surrender Amount Request Type,TGTYPESCREENREG");
        ASSERT.$(var_RequestedPartialSurrenderAmount,"=",var_ActualSurrenderAmnttoOwner,"TGTYPESCREENREG");

        }
         else {
          LOGGER.info("Executed Step = ELSE,TGTYPESCREENREG");
		String var_SurrenderCharge = "null";
                 LOGGER.info("Executed Step = VAR,String,var_SurrenderCharge,null,TGTYPESCREENREG");
		String var_AdminCharge = "null";
                 LOGGER.info("Executed Step = VAR,String,var_AdminCharge,null,TGTYPESCREENREG");
		String var_SurrenderAmountToOwnerActual = "null";
                 LOGGER.info("Executed Step = VAR,String,var_SurrenderAmountToOwnerActual,null,TGTYPESCREENREG");
        TAP.$("ELE_RDOBTN_GROSSPARTIALSURRENDERREQTYPE","//input[@id='outerForm:requestType:0']","TGTYPESCREENREG");

        TAP.$("ELE_LINK_DISTRIBUTIONINFORMATION","//span[@id='outerForm:DistributionInformationText']","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_CONTINUE","//input[@type='submit'][@name='outerForm:Continue']","TGTYPESCREENREG");

		try { 
		 var_SurrenderCharge = driver.findElement(By.id("outerForm:unprotectedSurrenderChargeDisplay_partial")).getAttribute("value");

		var_AdminCharge = 
				driver.findElement(By.id("outerForm:administrativeChargeDisplay_partial")).getAttribute("value");
		// Add Screenshot will only work for Android platform

		takeScreenshot();


		writeToCSV("Total Charges", var_SurrenderCharge + " + " +  var_AdminCharge);			
		writeToCSV("Expected Surrender Amnt to Owner", var_SurrenderCharge + " + "  +  var_AdminCharge +  " + "  + var_RequestedPartialSurrenderAmount);
		// Add Screenshot will only work for Android platform

		takeScreenshot();

		double var_SurrenderAmnttoOwner1 ;
				var_SurrenderAmnttoOwner1 =  (Double.valueOf(var_SurrenderCharge))  +  (Double.valueOf(var_AdminCharge))+  ( Double.valueOf(var_RequestedPartialSurrenderAmount));
				
				 var_ExpectedSurrenderAmnttoOwner =  Double.toString(var_SurrenderAmnttoOwner1);
		 
		 ;} 
		 catch(Exception e) { PrintVal("Renderer Script Exception : "+e.toString()); } 
		 PrintVal("Executed Step = CALLRENDERERSCRIPT,PARTIALSURRENDERAMOUNTTOOWNERCALCULATION"); 
		writeToCSV ("Policy Number " , ActionWrapper.getElementValue("ELE_GETVAL_POLICYNUMBER", "//span[@id='outerForm:policyNumber']"));
                 LOGGER.info("Executed Step = WRITETOCSV,ELE_GETVAL_POLICYNUMBER,Policy Number,TGTYPESCREENREG");
		writeToCSV ("Plan " , ActionWrapper.getElementValue("ELE_GETVAL_SURRENDERPLANPARTIAL", "//span[@id='outerForm:planCode']"));
                 LOGGER.info("Executed Step = WRITETOCSV,ELE_GETVAL_SURRENDERPLANPARTIAL,Plan,TGTYPESCREENREG");
		writeToCSV ("Surrender Type " , ActionWrapper.getElementValue("ELE_GETVAL_SURRENDERTYPEPARTIAL", "//span[@id='outerForm:surrenderTypeText']"));
                 LOGGER.info("Executed Step = WRITETOCSV,ELE_GETVAL_SURRENDERTYPEPARTIAL,Surrender Type,TGTYPESCREENREG");
		writeToCSV("Surrender Charges " , var_SurrenderCharge);
                 LOGGER.info("Executed Step = WRITETOCSV,var_SurrenderCharge,Surrender Charges,TGTYPESCREENREG");
		writeToCSV("Admin Charges " , var_AdminCharge);
                 LOGGER.info("Executed Step = WRITETOCSV,var_AdminCharge,Admin Charges,TGTYPESCREENREG");
		writeToCSV ("Expected Surrender Amnt to Owner " , ActionWrapper.getElementValue("ELE_GETVAL_SURRENDERAMOUNTTOOWNER", "//span[@id='outerForm:amountToOwner_partial']"));
                 LOGGER.info("Executed Step = WRITETOCSV,ELE_GETVAL_SURRENDERAMOUNTTOOWNER,Expected Surrender Amnt to Owner,TGTYPESCREENREG");
		writeToCSV("Surrender Amount Request Type " , var_PartialSurrenderAmtReqType);
                 LOGGER.info("Executed Step = WRITETOCSV,var_PartialSurrenderAmtReqType,Surrender Amount Request Type,TGTYPESCREENREG");
        }
          LOGGER.info("Executed Step = ENDIF,TGTYPESCREENREG");
        SELECT.$("ELE_DRPDWN_DISTRIBUTIONREASONCODEPARTIAL","//select[@id='outerForm:distributionReasonCode_partial']","7 - Normal Distribution","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SUBMIT","//input[@type='submit'][@name='outerForm:Submit']","TGTYPESCREENREG");

        ASSERT.$("ELE_GETMSG_SUCCESS","//li[@class='MessageInfo']","=","Request completed successfully.","TGTYPESCREENREG");

        TAP.$("ELE_LINK_HOME","outerForm:headerHomeTextTGWEBCOMMAIdTGWEBCOMMA0","TGTYPESCREENREG");

        HOVER.$("ELE_MENU_INQUIRY","//td[text()='Inquiry']","TGTYPESCREENREG");

        TAP.$("ELE_SUBMEN_POLICYDETAIL","//td[text()='Policy Detail']","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_POLICYNUMBER","//input[@type='text'][@name='outerForm:policyNumber']",var_PolicyNumber,"FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_CONTINUEPOLICYDETAIL","//input[@type='submit'][@name='outerForm:Continue']","TGTYPESCREENREG");

        TAP.$("ELE_LINK_TRANSACTIONHISTORY","//span[@id='outerForm:TransactionHistoryText']","TGTYPESCREENREG");

        }
          LOGGER.info("Executed Step = ENDIF,TGTYPESCREENREG");
        }
          LOGGER.info("Executed Step = ENDIF,TGTYPESCREENREG");
		var_Count = var_Count + ( 1 );
                 LOGGER.info("Executed Step = INCREASEVAR,var_Count,1,TGTYPESCREENREG");
    	}
          LOGGER.info("Executed Step = WEND,TGTYPESCREENREG");
    	resetAttemps();

    }


    @Test
    public void tc15loanpayoffquote() {

        // No need to declare / print method name here, handled by TestNg listener
        // declare("tc15loanpayoffquote");

        CALL.$("LoginToGIAS36","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_USERNAME","outerForm:usernameTGWEBCOMMAIdTGWEBCOMMA0","GIAS36ILC","FALSE","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_PASSWORD","outerForm:passwordTGWEBCOMMAIdTGWEBCOMMA0","Automation","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_LOGIN","outerForm:loginButtonTGWEBCOMMAIdTGWEBCOMMA0","TGTYPESCREENREG");

		com.jayway.jsonpath.ReadContext var_CsvData = getJsonData("C:/GIAS_Automation/3.6 BaseRegression/InputData/36Reg_LoanPayOffQuote.csv");
                 LOGGER.info("Executed Step = VAR,Json,var_CsvData,C:/GIAS_Automation/3.6 BaseRegression/InputData/36Reg_LoanPayOffQuote.csv,TGTYPESCREENREG");
		int var_count = 0;
                 LOGGER.info("Executed Step = VAR,Integer,var_count,0,TGTYPESCREENREG");
		int var_CountIncrement = 1;
                 LOGGER.info("Executed Step = VAR,Integer,var_CountIncrement,1,TGTYPESCREENREG");
        LOGGER.info("Executed Step = START IF");
        if (check(var_count,"<=",var_CountIncrement,"TGTYPESCREENREG")) {
         LOGGER.info("Executed Step = IF,var_count,<=,var_CountIncrement,TGTYPESCREENREG");
		String var_policynumber = "null";
                 LOGGER.info("Executed Step = VAR,String,var_policynumber,null,TGTYPESCREENREG");
		String var_effectiveDateValue = "null";
                 LOGGER.info("Executed Step = VAR,String,var_effectiveDateValue,null,TGTYPESCREENREG");
		String var_AdjustedPayoffAmount = "null";
                 LOGGER.info("Executed Step = VAR,String,var_AdjustedPayoffAmount,null,TGTYPESCREENREG");
		var_policynumber = getJsonData(var_CsvData , "$.records["+var_count+"].PolicyNumber");
                 LOGGER.info("Executed Step = STORE,var_policynumber,var_CsvData,$.records[{var_count}].PolicyNumber,TGTYPESCREENREG");
        CALL.$("LoanPayOffQuote","TGTYPESCREENREG");

        HOVER.$("ELE_MENU_POLICYOWNERSERVICES","//td[@class='ThemePanelMainFolderText'][contains(text(),'Policyowner Services')]","TGTYPESCREENREG");

        HOVER.$("ELE_SUBMEN_LOANLIEN","//div[@id='cmSubMenuID38']//tbody//tr[4]//td[2]","TGTYPESCREENREG");

        TAP.$("ELE_SUBMEN_LOANPAYOFFQUOTE","//td[text()='Loan Payoff Quote']","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_POLICYNUMBER","//input[@type='text'][@name='outerForm:policyNumber']",var_policynumber,"TRUE","TGTYPESCREENREG");

        SELECT.$("ELE_DRPDWN_LOANPAYOFFQUOTEBENEFIT","//select[@id='outerForm:baseRiderCode']","Base","TGTYPESCREENREG");

		String var_next2Weeks = "null";
                 LOGGER.info("Executed Step = VAR,String,var_next2Weeks,null,TGTYPESCREENREG");
		try { 
		 java.time.LocalDate today = java.time.LocalDate.now();
		        //add 2 week to the current date
		java.time.LocalDate next2Week = today.plus(2, java.time.temporal.ChronoUnit.WEEKS);
		      //  System.out.println("Next week: " + next2Week);
		java.time.format.DateTimeFormatter formatter = java.time.format.DateTimeFormatter.ofPattern("MM/dd/yyyy");
		String formattedDate = next2Week.format(formatter);
		var_next2Weeks =  formattedDate;
		 
		 ;} 
		 catch(Exception e) { PrintVal("Renderer Script Exception : "+e.toString()); } 
		 PrintVal("Executed Step = CALLRENDERERSCRIPT,ADDINGTWOWEEKSTOCURRENTDATE"); 
        TYPE.$("ELE_TXTBOX_LOANPAYOFFQUOTEDATE","//input[@id='outerForm:lastPaymentDate_input']",var_next2Weeks,"TRUE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_CONTINUELOANPAYOFFQUOTE","//input[@id='outerForm:Display']","TGTYPESCREENREG");

		writeToCSV("Policy Number " , var_policynumber);
                 LOGGER.info("Executed Step = WRITETOCSV,var_policynumber,Policy Number,TGTYPESCREENREG");
		writeToCSV("Apply Date " , var_next2Weeks);
                 LOGGER.info("Executed Step = WRITETOCSV,var_next2Weeks,Apply Date,TGTYPESCREENREG");
		writeToCSV ("Loan Pay Off Amount " , ActionWrapper.getElementValue("ELE_GETVAL_LOANPAYOFFAMOUNT", "//span[@id='outerForm:adjustedLoanPayoffAmount2']"));
                 LOGGER.info("Executed Step = WRITETOCSV,ELE_GETVAL_LOANPAYOFFAMOUNT,Loan Pay Off Amount,TGTYPESCREENREG");
        CALL.$("LoanPrincipalPayment","TGTYPESCREENREG");

        HOVER.$("ELE_MENU_PAYMENTS","//td[@class='ThemePanelMainFolderText'][contains(text(),'Payments')]","TGTYPESCREENREG");

        HOVER.$("ELE_SUBMEN_PAYMENTSLOAN","//td[@class='ThemePanelMenuFolderText'][text()='Loan']","TGTYPESCREENREG");

        TAP.$("ELE_SUBMEN_LOANPRINCIPAL","//td[text()='Loan Principal']","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_POLICYNUMBER","//input[@type='text'][@name='outerForm:policyNumber']",var_policynumber,"TRUE","TGTYPESCREENREG");

		try { 
		 java.time.LocalDate date = java.time.LocalDate.now();
		java.time.format.DateTimeFormatter formatter = java.time.format.DateTimeFormatter.ofPattern("MM/dd/yyyy");
		String formattedDate = date.format(formatter);
		var_effectiveDateValue =  formattedDate;
		 
		 ;} 
		 catch(Exception e) { PrintVal("Renderer Script Exception : "+e.toString()); } 
		 PrintVal("Executed Step = CALLRENDERERSCRIPT,CURRENTDATEASEFFECTIVEDATEFORLOANPAYOFFQUOTE"); 
        TYPE.$("ELE_TXTBOX_APPLYDATE","//input[@type='text'][@name='outerForm:promptDate']",var_effectiveDateValue,"TRUE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_CONTINUE","//input[@type='submit'][@name='outerForm:Continue']","TGTYPESCREENREG");

		var_AdjustedPayoffAmount = ActionWrapper.getElementValue("ELE_GETVAL_TOTALPAYOFFAMOUNT", "//span[@id='outerForm:totalPayoffAmount']");
                 LOGGER.info("Executed Step = STORE,var_AdjustedPayoffAmount,ELE_GETVAL_TOTALPAYOFFAMOUNT,TGTYPESCREENREG");
        TYPE.$("ELE_TXTBOX_PAYMENTAMOUNT","//input[@id='outerForm:paymentamount']",var_AdjustedPayoffAmount,"TRUE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SUBMIT","//input[@type='submit'][@name='outerForm:Submit']","TGTYPESCREENREG");

        ASSERT.$("ELE_GETMSG_SUCCESS","//li[@class='MessageInfo']","CONTAINS","successfully","TGTYPESCREENREG");

        CALL.$("LoanPaymentInFullInquiry","TGTYPESCREENREG");

        HOVER.$("ELE_MENU_INQUIRY","//td[text()='Inquiry']","TGTYPESCREENREG");

        TAP.$("ELE_SUBMEN_POLICYDETAIL","//td[text()='Policy Detail']","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_POLICYNUMBER","//input[@type='text'][@name='outerForm:policyNumber']",var_policynumber,"TRUE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_CONTINUE","//input[@type='submit'][@name='outerForm:Continue']","TGTYPESCREENREG");

        TAP.$("ELE_LINK_LOANS","//span[text()='Loans']","TGTYPESCREENREG");

		int var_TotalLoanBalance = 0;
                 LOGGER.info("Executed Step = VAR,Integer,var_TotalLoanBalance,0,TGTYPESCREENREG");
		var_TotalLoanBalance = ActionWrapper.getElementValueForVariableWithInt("ELE_GETVAL_TOTALLOANBALANCE", "//span[@id='outerForm:grid:0:currentLoanAmountOut2']");
                 LOGGER.info("Executed Step = STORE,var_TotalLoanBalance,ELE_GETVAL_TOTALLOANBALANCE,TGTYPESCREENREG");
        LOGGER.info("Executed Step = START IF");
        if (check(var_TotalLoanBalance,">",0,"TGTYPESCREENREG")) {
         LOGGER.info("Executed Step = IF,var_TotalLoanBalance,>,0,TGTYPESCREENREG");
        CALL.$("LoanPrincipalPayment","TGTYPESCREENREG");

        HOVER.$("ELE_MENU_PAYMENTS","//td[@class='ThemePanelMainFolderText'][contains(text(),'Payments')]","TGTYPESCREENREG");

        HOVER.$("ELE_SUBMEN_PAYMENTSLOAN","//td[@class='ThemePanelMenuFolderText'][text()='Loan']","TGTYPESCREENREG");

        TAP.$("ELE_SUBMEN_LOANPRINCIPAL","//td[text()='Loan Principal']","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_POLICYNUMBER","//input[@type='text'][@name='outerForm:policyNumber']",var_policynumber,"TRUE","TGTYPESCREENREG");

		try { 
		 java.time.LocalDate date = java.time.LocalDate.now();
		java.time.format.DateTimeFormatter formatter = java.time.format.DateTimeFormatter.ofPattern("MM/dd/yyyy");
		String formattedDate = date.format(formatter);
		var_effectiveDateValue =  formattedDate;
		 
		 ;} 
		 catch(Exception e) { PrintVal("Renderer Script Exception : "+e.toString()); } 
		 PrintVal("Executed Step = CALLRENDERERSCRIPT,CURRENTDATEASEFFECTIVEDATEFORLOANPAYOFFQUOTE"); 
        TYPE.$("ELE_TXTBOX_APPLYDATE","//input[@type='text'][@name='outerForm:promptDate']",var_effectiveDateValue,"TRUE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_CONTINUE","//input[@type='submit'][@name='outerForm:Continue']","TGTYPESCREENREG");

		var_AdjustedPayoffAmount = ActionWrapper.getElementValue("ELE_GETVAL_TOTALPAYOFFAMOUNT", "//span[@id='outerForm:totalPayoffAmount']");
                 LOGGER.info("Executed Step = STORE,var_AdjustedPayoffAmount,ELE_GETVAL_TOTALPAYOFFAMOUNT,TGTYPESCREENREG");
        TYPE.$("ELE_TXTBOX_PAYMENTAMOUNT","//input[@id='outerForm:paymentamount']",var_AdjustedPayoffAmount,"TRUE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SUBMIT","//input[@type='submit'][@name='outerForm:Submit']","TGTYPESCREENREG");

        ASSERT.$("ELE_GETMSG_SUCCESS","//li[@class='MessageInfo']","CONTAINS","successfully","TGTYPESCREENREG");

        }
         else {
          LOGGER.info("Executed Step = ELSE,TGTYPESCREENREG");
        ASSERT.$(var_TotalLoanBalance,"=",0,"TGTYPESCREENREG");

        }
          LOGGER.info("Executed Step = ENDIF,TGTYPESCREENREG");
		writeToCSV("Policy Number " , var_policynumber);
                 LOGGER.info("Executed Step = WRITETOCSV,var_policynumber,Policy Number,TGTYPESCREENREG");
		writeToCSV ("Total Balance " , ActionWrapper.getElementValue("ELE_GETVAL_TOTALLOANBALANCE", "//span[@id='outerForm:grid:0:currentLoanAmountOut2']"));
                 LOGGER.info("Executed Step = WRITETOCSV,ELE_GETVAL_TOTALLOANBALANCE,Total Balance,TGTYPESCREENREG");
		writeToCSV ("Effective Date " , ActionWrapper.getElementValue("ELE_GETVAL_EFFECTIVEDATE", "//span[@id='outerForm:effectiveDate_input']"));
                 LOGGER.info("Executed Step = WRITETOCSV,ELE_GETVAL_EFFECTIVEDATE,Effective Date,TGTYPESCREENREG");
		var_count = var_count + ( 1 );
                 LOGGER.info("Executed Step = INCREASEVAR,var_count,1,TGTYPESCREENREG");
        }
          LOGGER.info("Executed Step = ENDIF,TGTYPESCREENREG");

    }


    @Test
    public void tc18uldeathclaim() {

        // No need to declare / print method name here, handled by TestNg listener
        // declare("tc18uldeathclaim");

        CALL.$("LoginToGIAS36","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_USERNAME","outerForm:usernameTGWEBCOMMAIdTGWEBCOMMA0","GIAS36ILC","FALSE","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_PASSWORD","outerForm:passwordTGWEBCOMMAIdTGWEBCOMMA0","Automation","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_LOGIN","outerForm:loginButtonTGWEBCOMMAIdTGWEBCOMMA0","TGTYPESCREENREG");

		int var_Count;
                 LOGGER.info("Executed Step = VAR,Integer,var_Count,TGTYPESCREENREG");
		var_Count = 0;
                 LOGGER.info("Executed Step = STORE,var_Count,0,TGTYPESCREENREG");
        LOGGER.info("Executed Step = REPEAT IF");
    	while (check(var_Count,"<",1,"TGTYPESCREENREG")) {
          LOGGER.info("Executed Step = WHILE,var_Count,<,1,TGTYPESCREENREG");
		com.jayway.jsonpath.ReadContext var_CSVData = getJsonData("C:/GIAS_Automation/3.6 BaseRegression/InputData/36Reg_ULDeathClaimCombined.csv");
                 LOGGER.info("Executed Step = VAR,Json,var_CSVData,C:/GIAS_Automation/3.6 BaseRegression/InputData/36Reg_ULDeathClaimCombined.csv,TGTYPESCREENREG");
		String var_PolicyNumber;
                 LOGGER.info("Executed Step = VAR,String,var_PolicyNumber,TGTYPESCREENREG");
		var_PolicyNumber = getJsonData(var_CSVData , "$.records["+var_Count+"].PolicyNumber");
                 LOGGER.info("Executed Step = STORE,var_PolicyNumber,var_CSVData,$.records[{var_Count}].PolicyNumber,TGTYPESCREENREG");
        CALL.$("ULDeathClaimPendingCombineCopy","TGTYPESCREENREG");

		String var_NotificationDate;
                 LOGGER.info("Executed Step = VAR,String,var_NotificationDate,TGTYPESCREENREG");
		var_NotificationDate = getJsonData(var_CSVData , "$.records["+var_Count+"].NotificationDate");
                 LOGGER.info("Executed Step = STORE,var_NotificationDate,var_CSVData,$.records[{var_Count}].NotificationDate,TGTYPESCREENREG");
		String var_DateofDeath;
                 LOGGER.info("Executed Step = VAR,String,var_DateofDeath,TGTYPESCREENREG");
		var_DateofDeath = getJsonData(var_CSVData , "$.records["+var_Count+"].DateofDeath");
                 LOGGER.info("Executed Step = STORE,var_DateofDeath,var_CSVData,$.records[{var_Count}].DateofDeath,TGTYPESCREENREG");
		String var_DateDeathCertificateReceived;
                 LOGGER.info("Executed Step = VAR,String,var_DateDeathCertificateReceived,TGTYPESCREENREG");
		var_DateDeathCertificateReceived = getJsonData(var_CSVData , "$.records["+var_Count+"].DateDeathCertificateReceived");
                 LOGGER.info("Executed Step = STORE,var_DateDeathCertificateReceived,var_CSVData,$.records[{var_Count}].DateDeathCertificateReceived,TGTYPESCREENREG");
		String var_IndividualName;
                 LOGGER.info("Executed Step = VAR,String,var_IndividualName,TGTYPESCREENREG");
		var_IndividualName = getJsonData(var_CSVData , "$.records["+var_Count+"].IndividualName");
                 LOGGER.info("Executed Step = STORE,var_IndividualName,var_CSVData,$.records[{var_Count}].IndividualName,TGTYPESCREENREG");
        HOVER.$("ELE_MENU_POLICYOWNERSERVICES","//td[@class='ThemePanelMainFolderText'][contains(text(),'Policyowner Services')]","TGTYPESCREENREG");

        TAP.$("ELE_SUBMEN_CLAIMS","//div[@id='cmSubMenuID38']//td[@class='ThemePanelMenuItemText'][contains(text(),'Claims')]","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_ADDDEATHCLAIM","//*[@name='outerForm:AddDeathClaim']","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_NOTIFICATIONDATE","//input[@type='text'][@name='outerForm:notiOfDthDate']",var_NotificationDate,"FALSE","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_DATEOFDEATH","//input[@type='text'][@name='outerForm:dateOfDeathDate']",var_DateofDeath,"FALSE","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_DATEDEATHCERTRECEIVED","//input[@type='text'][@name='outerForm:deathCertRcvdDate']",var_DateDeathCertificateReceived,"FALSE","TGTYPESCREENREG");

        TAP.$("ELE_LINK_SELECTCLIENT","//span[text()='Select Client']","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_ULCLIENTNAMESEARCH","//input[@id='outerForm:grid:clientName']",var_IndividualName,"FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_FINDPOSITIONTO2","//img[@id='outerForm:gnFindImage']","TGTYPESCREENREG");

        TAP.$("ELE_LINK_SELECTCLIENTNAME","//a[@id='outerForm:grid:0:clientNameLink']","TGTYPESCREENREG");

        SELECT.$("ELE_DRPDOWN_REASONFORCLAIM","//select[@name='outerForm:transactionRsnCode']","Leukemia","TGTYPESCREENREG");

        TAP.$("ELE_RDOBTN_YESISSUEAGEVERIFIED","//input[@id='outerForm:wasIssueAgeVerified:1']","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SUBMIT","//input[@type='submit'][@name='outerForm:Submit']","TGTYPESCREENREG");

        ASSERT.$("ELE_GETMSG_SUCCESS","//li[@class='MessageInfo']","=","Item successfully added.","TGTYPESCREENREG");

        TAP.$("ELE_LINK_CLAIMDETAIL","//span[text()='Claim Detail']","TGTYPESCREENREG");

        TAP.$("ELE_LINK_ALLLCLAIMPROCEEDS","//span[text()='All Claim Proceeds']","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_CANCEL","//input[@type='submit'][@name='outerForm:Cancel']","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_CANCEL","//input[@type='submit'][@name='outerForm:Cancel']","TGTYPESCREENREG");

        TAP.$("ELE_LINK_CLIENT","//span[text()='Client']","TGTYPESCREENREG");

        TAP.$("ELE_LINK_HOME","outerForm:headerHomeTextTGWEBCOMMAIdTGWEBCOMMA0","TGTYPESCREENREG");

        HOVER.$("ELE_MENU_INQUIRY","//td[text()='Inquiry']","TGTYPESCREENREG");

        TAP.$("ELE_SUBMEN_POLICYDETAIL","//td[text()='Policy Detail']","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_POLICYNUMBER","//input[@type='text'][@name='outerForm:policyNumber']",var_PolicyNumber,"FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_CONTINUE","//input[@type='submit'][@name='outerForm:Continue']","TGTYPESCREENREG");

        TAP.$("ELE_LINK_BENEFITS","//span[text()='Benefits']","TGTYPESCREENREG");

        TAP.$("ELE_LINK_BASE","//span[contains(text(),'Base')]","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_CANCEL","//input[@type='submit'][@name='outerForm:Cancel']","TGTYPESCREENREG");

        TAP.$("ELE_LINK_RIDER1","//span[text()='Rider 1']","TGTYPESCREENREG");

        ASSERT.$("ELE_GETVAL_BENEFITSTATUS","//span[@id='outerForm:benefitStatusText']","=","Premium Paying","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_CANCEL","//input[@type='submit'][@name='outerForm:Cancel']","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_CANCEL","//input[@type='submit'][@name='outerForm:Cancel']","TGTYPESCREENREG");

		writeToCSV ("Policy Status " , ActionWrapper.getElementValue("ELE_GETVAL_POLICYSTATUS", "//span[@id='outerForm:statusText']"));
                 LOGGER.info("Executed Step = WRITETOCSV,ELE_GETVAL_POLICYSTATUS,Policy Status,TGTYPESCREENREG");
		writeToCSV ("Suspend Reason  " , ActionWrapper.getElementValue("ELE_GETVAL_SUSPENDREASON", "//span[@id='outerForm:suspendReasonCodeText']"));
                 LOGGER.info("Executed Step = WRITETOCSV,ELE_GETVAL_SUSPENDREASON,Suspend Reason ,TGTYPESCREENREG");
		writeToCSV ("Suspense Balance  " , ActionWrapper.getElementValue("ELE_GETVAL_SUSPENSEBALANCE", "//span[@id='outerForm:suspenseBalance']"));
                 LOGGER.info("Executed Step = WRITETOCSV,ELE_GETVAL_SUSPENSEBALANCE,Suspense Balance ,TGTYPESCREENREG");
        TAP.$("ELE_LINK_TRANSACTIONHISTORY","//span[@id='outerForm:TransactionHistoryText']","TGTYPESCREENREG");

		writeToCSV ("Rider 1 Death Claim Number " , ActionWrapper.getElementValue("ELE_GETVAL_RIDER1DEATHCLAIMNUMBER", "//span[@id='outerForm:grid:0:typeTextOut2']"));
                 LOGGER.info("Executed Step = WRITETOCSV,ELE_GETVAL_RIDER1DEATHCLAIMNUMBER,Rider 1 Death Claim Number,TGTYPESCREENREG");
		writeToCSV ("Base Benefit Death Claim Number  " , ActionWrapper.getElementValue("ELE_GETVAL_BASEBENEFITDEATHCLAIMNUMBER", "//span[@id='outerForm:grid:1:typeTextOut2']"));
                 LOGGER.info("Executed Step = WRITETOCSV,ELE_GETVAL_BASEBENEFITDEATHCLAIMNUMBER,Base Benefit Death Claim Number ,TGTYPESCREENREG");
        TAP.$("ELE_LINK_HOME","outerForm:headerHomeTextTGWEBCOMMAIdTGWEBCOMMA0","TGTYPESCREENREG");

        CALL.$("ULDeathClaimApproveCombineCopy","TGTYPESCREENREG");

		String var_NotificationDate2;
                 LOGGER.info("Executed Step = VAR,String,var_NotificationDate2,TGTYPESCREENREG");
		var_NotificationDate2 = getJsonData(var_CSVData , "$.records["+var_Count+"].NotificationDate");
                 LOGGER.info("Executed Step = STORE,var_NotificationDate2,var_CSVData,$.records[{var_Count}].NotificationDate,TGTYPESCREENREG");
		String var_DateofDeath2;
                 LOGGER.info("Executed Step = VAR,String,var_DateofDeath2,TGTYPESCREENREG");
		var_DateofDeath2 = getJsonData(var_CSVData , "$.records["+var_Count+"].DateofDeath");
                 LOGGER.info("Executed Step = STORE,var_DateofDeath2,var_CSVData,$.records[{var_Count}].DateofDeath,TGTYPESCREENREG");
		String var_DateDeathCertificateReceived2;
                 LOGGER.info("Executed Step = VAR,String,var_DateDeathCertificateReceived2,TGTYPESCREENREG");
		var_DateDeathCertificateReceived2 = getJsonData(var_CSVData , "$.records["+var_Count+"].DateDeathCertificateReceived");
                 LOGGER.info("Executed Step = STORE,var_DateDeathCertificateReceived2,var_CSVData,$.records[{var_Count}].DateDeathCertificateReceived,TGTYPESCREENREG");
		String var_DateClaimApproved;
                 LOGGER.info("Executed Step = VAR,String,var_DateClaimApproved,TGTYPESCREENREG");
		var_DateClaimApproved = getJsonData(var_CSVData , "$.records["+var_Count+"].DateClaimApproved");
                 LOGGER.info("Executed Step = STORE,var_DateClaimApproved,var_CSVData,$.records[{var_Count}].DateClaimApproved,TGTYPESCREENREG");
		String var_IndividualName2;
                 LOGGER.info("Executed Step = VAR,String,var_IndividualName2,TGTYPESCREENREG");
		var_IndividualName2 = getJsonData(var_CSVData , "$.records["+var_Count+"].IndividualName");
                 LOGGER.info("Executed Step = STORE,var_IndividualName2,var_CSVData,$.records[{var_Count}].IndividualName,TGTYPESCREENREG");
        HOVER.$("ELE_MENU_POLICYOWNERSERVICES","//td[@class='ThemePanelMainFolderText'][contains(text(),'Policyowner Services')]","TGTYPESCREENREG");

        TAP.$("ELE_SUBMEN_CLAIMS","//div[@id='cmSubMenuID38']//td[@class='ThemePanelMenuItemText'][contains(text(),'Claims')]","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SORTBYNAME","//input[@type='submit'][@name='outerForm:SortByName']","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_INSUREDNAME","//input[@type='text'][@name='outerForm:grid:individualName']",var_IndividualName2,"FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_FINDPOSITIONTOSORTBYCLIENT","//img[@id='outerForm:gnFindImage5']","TGTYPESCREENREG");

        TAP.$("ELE_LINK_SEARCHEDFIRSTINDIVIDUALNAME","span[id='outerForm:grid:0:individualNameOut1']TGWEBCOMMACssSelectorTGWEBCOMMA0","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_UPDATE","//input[@type='submit'][@name='outerForm:Update']","TGTYPESCREENREG");

        TAP.$("ELE_RDOBTN_CLAIMAPPROVE","//input[@id='outerForm:claimStatusCode:0']","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_UPDATECLAIMDATECLAIMAPPROVED","//input[@id='outerForm:dateClaimApprDate_input']",var_DateClaimApproved,"FALSE","TGTYPESCREENREG");

		writeToCSV ("Claim Type " , ActionWrapper.getElementValue("ELE_GETVAL_UPDATECLAIMCLAIMTYPE", "//span[@id='outerForm:claimType']"));
                 LOGGER.info("Executed Step = WRITETOCSV,ELE_GETVAL_UPDATECLAIMCLAIMTYPE,Claim Type,TGTYPESCREENREG");
		writeToCSV ("Claim Number " , ActionWrapper.getElementValue("ELE_GETVAL_UPDATECLAIMCLAIMNUMBER", "//span[@id='outerForm:claimNumber']"));
                 LOGGER.info("Executed Step = WRITETOCSV,ELE_GETVAL_UPDATECLAIMCLAIMNUMBER,Claim Number,TGTYPESCREENREG");
		writeToCSV ("Claim Status " , ActionWrapper.getElementValue("ELE_RDOBTN_CLAIMAPPROVE", "//input[@id='outerForm:claimStatusCode:0']"));
                 LOGGER.info("Executed Step = WRITETOCSV,ELE_RDOBTN_CLAIMAPPROVE,Claim Status,TGTYPESCREENREG");
		writeToCSV ("Insured Name " , ActionWrapper.getElementValue("ELE_GETVAL_UPDATECLAIMINSUREDNAME", "//span[@id='outerForm:individualName']"));
                 LOGGER.info("Executed Step = WRITETOCSV,ELE_GETVAL_UPDATECLAIMINSUREDNAME,Insured Name,TGTYPESCREENREG");
		writeToCSV ("Date of Death " , ActionWrapper.getElementValue("ELE_GETVAL_UPDATECLAIMDODEATH", "//span[@id='outerForm:dateOfDeathDate_input']"));
                 LOGGER.info("Executed Step = WRITETOCSV,ELE_GETVAL_UPDATECLAIMDODEATH,Date of Death,TGTYPESCREENREG");
		writeToCSV ("Claim Original Amount " , ActionWrapper.getElementValue("ELE_GETVAL_UPDATECLAIMORIGINALAMT", "//span[@id='outerForm:totalClaimOrigAmt']"));
                 LOGGER.info("Executed Step = WRITETOCSV,ELE_GETVAL_UPDATECLAIMORIGINALAMT,Claim Original Amount,TGTYPESCREENREG");
		writeToCSV ("Claim Amount Approved " , ActionWrapper.getElementValue("ELE_GETVAL_UPDATECLAIMAMTAPPROVED", "//span[@id='outerForm:totalClaimAmountAppr']"));
                 LOGGER.info("Executed Step = WRITETOCSV,ELE_GETVAL_UPDATECLAIMAMTAPPROVED,Claim Amount Approved,TGTYPESCREENREG");
		writeToCSV ("Claim Amount Paid " , ActionWrapper.getElementValue("ELE_GETVAL_UPDATECLAIMAMTPAID", "//span[@id='outerForm:totalClaimAmountPaid']"));
                 LOGGER.info("Executed Step = WRITETOCSV,ELE_GETVAL_UPDATECLAIMAMTPAID,Claim Amount Paid,TGTYPESCREENREG");
		writeToCSV("Date Claim Approved " , var_DateClaimApproved);
                 LOGGER.info("Executed Step = WRITETOCSV,var_DateClaimApproved,Date Claim Approved,TGTYPESCREENREG");
        TAP.$("ELE_BUTTON_SUBMIT","//input[@type='submit'][@name='outerForm:Submit']","TGTYPESCREENREG");

        ASSERT.$("ELE_GETMSG_SUCCESS","//li[@class='MessageInfo']","=","Item successfully updated.","TGTYPESCREENREG");

        CALL.$("ULDeathClaimPaymentCombineCopy2","TGTYPESCREENREG");

		String var_DateSession1;
                 LOGGER.info("Executed Step = VAR,String,var_DateSession1,TGTYPESCREENREG");
		var_DateSession1 = getJsonData(var_CSVData , "$.records["+var_Count+"].DateSession1");
                 LOGGER.info("Executed Step = STORE,var_DateSession1,var_CSVData,$.records[{var_Count}].DateSession1,TGTYPESCREENREG");
		String var_DateClaimApproved2;
                 LOGGER.info("Executed Step = VAR,String,var_DateClaimApproved2,TGTYPESCREENREG");
		var_DateClaimApproved2 = getJsonData(var_CSVData , "$.records["+var_Count+"].DateClaimApproved2");
                 LOGGER.info("Executed Step = STORE,var_DateClaimApproved2,var_CSVData,$.records[{var_Count}].DateClaimApproved2,TGTYPESCREENREG");
        TAP.$("ELE_LINK_PAYMENT","//span[@id='outerForm:WorkWithPaymentsText']","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_POLICYNUMBERULPAYMENT","//span[@id='outerForm:grid:0:policyNumberOut1']","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_UPDATE","//input[@type='submit'][@name='outerForm:Update']","TGTYPESCREENREG");

        TAP.$("ELE_RDOBTN_PAYMENTSTATUSAPPROVED","//input[@id='outerForm:beneficiaryPmtStat:0']","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_PERCENTAGECLAIMAMT","//input[@id='outerForm:sharePercentage']","100","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_EDIT","//input[@id='outerForm:Edit']","TGTYPESCREENREG");

        ASSERT.$("ELE_GETMSG_SUCCESS","//li[@class='MessageInfo']","=","Edit completed successfully with no errors.","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SUBMIT","//input[@type='submit'][@name='outerForm:Submit']","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_CANCEL","//input[@type='submit'][@name='outerForm:Cancel']","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_POLICYNUMBERULPAYMENT","//span[@id='outerForm:grid:0:policyNumberOut1']","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_PAYCLAIM","//input[@id='outerForm:PayClaim']","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SUBMIT","//input[@type='submit'][@name='outerForm:Submit']","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_POLICYNUMBERULPAYMENT","//span[@id='outerForm:grid:0:policyNumberOut1']","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_CANCEL","//input[@type='submit'][@name='outerForm:Cancel']","TGTYPESCREENREG");

        TAP.$("ELE_LINK_HOME","outerForm:headerHomeTextTGWEBCOMMAIdTGWEBCOMMA0","TGTYPESCREENREG");

        HOVER.$("ELE_MENU_INQUIRY","//td[text()='Inquiry']","TGTYPESCREENREG");

        TAP.$("ELE_SUBMEN_POLICYDETAIL","//td[text()='Policy Detail']","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_POLICYNUMBER","//input[@type='text'][@name='outerForm:policyNumber']",var_PolicyNumber,"FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_CONTINUE","//input[@type='submit'][@name='outerForm:Continue']","TGTYPESCREENREG");

        TAP.$("ELE_LINK_TRANSACTIONHISTORY","//span[@id='outerForm:TransactionHistoryText']","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_TRANSACTIONDATE","//span[@id='outerForm:grid:0:transactionDateOut1']","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_CANCEL","//input[@type='submit'][@name='outerForm:Cancel']","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_TRANSACTIONDATEUL","//span[@id='outerForm:grid:1:transactionDateOut1']","TGTYPESCREENREG");

        TAP.$("ELE_LINK_HOME","outerForm:headerHomeTextTGWEBCOMMAIdTGWEBCOMMA0","TGTYPESCREENREG");

        HOVER.$("ELE_MENU_ACCOUNTING","//td[text()='Accounting']","TGTYPESCREENREG");

        TAP.$("ELE_SUBMEN_LEDGEREXTRACT","//td[text()='Ledger Extract']","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_LASTPAGE","//img[@id='outerForm:gnLastImage']","TGTYPESCREENREG");

        TAP.$("ELE_LINK_ACCOUNT240500","//span[text()='240500 - DISBURSEMENTS']","TGTYPESCREENREG");

        TAP.$("ELE_LINK_HOME","outerForm:headerHomeTextTGWEBCOMMAIdTGWEBCOMMA0","TGTYPESCREENREG");

		var_Count = var_Count + ( 1 );
                 LOGGER.info("Executed Step = INCREASEVAR,var_Count,1,TGTYPESCREENREG");
    	}
          LOGGER.info("Executed Step = WEND,TGTYPESCREENREG");
    	resetAttemps();

    }


}
