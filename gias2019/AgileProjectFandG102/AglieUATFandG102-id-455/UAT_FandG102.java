package appium.tgo;

import com.tg.octopus.base.TGOWrapper;
import com.tg.octopus.libs.testng.listener.TestListener;
import static com.tg.octopus.base.TGOWrapper.Action.*;
import static com.tg.octopus.base.TGOWrapper.LOGGER;
import com.tg.octopus.wrapper.ActionWrapper;

import com.jayway.jsonpath.JsonPath;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.SessionNotCreatedException;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.Augmenter;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.remote.UnreachableBrowserException;

import org.testng.Assert;
import org.testng.ITestContext;
import org.testng.ITestResult;
import org.testng.Reporter;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;
import java.net.HttpURLConnection;
import java.net.URL;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import org.json.JSONArray;
import org.json.JSONObject;
import helper.WebTestUtility;
import java.sql.*;
import com.testautomationguru.utility.PDFUtil;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.multipdf.Splitter;

import java.util.*;
import org.openqa.selenium.*;
import java.io.*;
import java.net.*;

import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;


@Listeners(TestListener.class)
public class UAT_FandG102 extends TGOWrapper {



    static {
       needsresetoncebeforetest ="false";
       needsresetbeforeeverymethod ="true";
       captureverystepsnap ="true";
       appendlastsnap ="false";
       ishybridapp ="true";

    }


    @Test
    public void comparegiasviewstopdf() {

        // No need to declare / print method name here, handled by TestNg listener
        // declare("comparegiasviewstopdf");

        WAIT.$(3,"TGTYPESCREENREG");

		int var_Count = 0;
                 LOGGER.info("Executed Step = VAR,Integer,var_Count,0,TGTYPESCREENREG");
        LOGGER.info("Executed Step = REPEAT IF");
    	while (check(var_Count,"<",1,"TGTYPESCREENREG")) {
          LOGGER.info("Executed Step = WHILE,var_Count,<,1,TGTYPESCREENREG");
		com.jayway.jsonpath.ReadContext var_CSVData = getJsonData("C:/GIAS_Automation/Agile FandG102/InputData/AgileFGSBI_CompPolicy.csv");
                 LOGGER.info("Executed Step = VAR,Json,var_CSVData,C:/GIAS_Automation/Agile FandG102/InputData/AgileFGSBI_CompPolicy.csv,TGTYPESCREENREG");
		String var_PolicyNumber = "null";
                 LOGGER.info("Executed Step = VAR,String,var_PolicyNumber,null,TGTYPESCREENREG");
		var_PolicyNumber = getJsonData(var_CSVData , "$.records["+var_Count+"].PolicyNumber");
                 LOGGER.info("Executed Step = STORE,var_PolicyNumber,var_CSVData,$.records[{var_Count}].PolicyNumber,TGTYPESCREENREG");
		try { 
		  boolean isJointInsureAvailable = false;
						                    boolean flag = false;
						                    ArrayList<String> filteredLines = new ArrayList<>();
						                    String filteredText = "";
						                    String policyLine = "";
						                    String factorLine = "";
						                    String surrenderLine = "";
						                    String yearLine = "";
						                    int yearCount = 0;
						                    int policyCount = 0;
						                    int factorCount = 0;
						                    String issueState = "";
											 String policynumberFromDB = "";

						                    java.sql.Connection con = null;

						                    // Load SQL Server JDBC driver and establish connection.
						                    String connectionUrl = "jdbc:sqlserver://CIS-GIASDBD1:243" + "3;" +
						                            "databaseName=FGLD2DT" + "A;"
						                            + "IntegratedSecurity = true";
						                    System.out.print("Connecting to SQL Server ... ");
						                    con = java.sql.DriverManager.getConnection(connectionUrl);
						                    System.out.println("Connected to database.");

						                    com.jayway.jsonpath.ReadContext CSVData = getJsonData("C:/GIAS_Automation/Agile FandG102/InputData/AgileFGSBI_CompPolicy.csv");
						                    // String var_PolicyNumber = getJsonData(CSVData, "$.records[0].PolicyNumber");

						                    String sql = "select jurisdiction,policy_number from iios.PolicyPageContractView where policy_number=" + "'" + var_PolicyNumber + "'";
						                    System.out.println(sql);

						                    Statement statement = con.createStatement();

						                    ResultSet rs = null;

						                    rs = statement.executeQuery(sql);

						                    System.out.println(rs);

						                    while (rs.next()) {

						                        issueState = rs.getString("jurisdiction");
												policynumberFromDB = rs.getString("policy_number");
						                    }
						                    statement.close();

						                    con.close();
						                    String basePath = "C:\\GIAS_Automation\\Agile FandG102\\InputData";
						                    // String policyNumber = "TGACCUM08";

						                    String fileName = WebTestUtility.getPDFFileForPolicy(basePath, var_PolicyNumber);
											ArrayList<String> pdfFileInText = WebTestUtility.readLinesFromPDFFile(fileName);

											 if (fileName.isEmpty()) {
													writeToCSV("POLICY NUMBER", var_PolicyNumber);
														writeToCSV("PAGE #: DATA TYPE", "PRINT PDF PRESENCE IN INPUTDATA FOLDER");
								                        //	writeToCSV("ISSUE STATE", "issue State: " + issueState);
								                        writeToCSV("EXPECTED DATA", "PRINT PDF NOT FOUND IN INPUTDATA FOLER");
								                        writeToCSV("ACTUAL DATA ON PDF", var_PolicyNumber + " PRINT PDF NOT FOUND");
								                        writeToCSV("RESULT", "SKIP");	
																//break;
										} else {

						                    int pdfLineNumber = 0;
						                    for (String line : pdfFileInText) {
						                        //		                    System.out.println(line);

						                        if (line.equals("STATEMENT OF BENEFIT INFORMATION – CONTRACT SUMMARY")) {
						                            flag = true;
						                        }

						                        if (flag) {
						                            filteredText = filteredText + "\n" + line;
						                            filteredLines.add(line);
						                            LOGGER.info(line);

						                            if (line.equals("POLICY")) {
						                                policyLine = line;
						                                System.out.println(policyLine);
						                                policyCount++;
						                            }
						                            if (line.contains("ANNUITANT(S) NAME(S)") && line.contains("ISSUE AGE(S)")) {
						                                String nextLine = pdfFileInText.get(pdfLineNumber + 1);
						                                String[] insuredParts = nextLine.trim().split(" ");
						                                if (insuredParts.length == 4) {
						                                    isJointInsureAvailable = false;
						                                } else if (insuredParts.length == 2) {
						                                    // For double entry of ANNUITANT
						                                    isJointInsureAvailable = true;
						                                }
						                            }
						                            if (line.equals("FACTOR")) {
						                                factorLine = line;
						                                System.out.println(factorLine);
						                                factorCount++;
						                            }
						                            if (line.equals("SURRENDER")) {
						                                surrenderLine = line;
						                                System.out.println(surrenderLine);
						                            }
						                            if (line.equals("YEAR")) {
						                                yearLine = line;
						                                System.out.println(factorLine);
						                                yearCount++;
						                            }

						                        }
						                        //if (line.equals("THE GUARANTEED VALUES SHOW THE MINIMUM GUARANTEED POLICY VALUES DESCRIBED IN THE POLICY, ASSUMING NO WITHDRAWALS OR SURRENDER.")) {
						                        if (line.contains("READ YOUR POLICY CAREFULLY")) {

						                            flag = false;
						                            break;
						                        }

						                        pdfLineNumber++;

						                    }
						                    System.out.println(filteredText);

						                    if (issueState.equals("MD") || issueState.equals("NV") || issueState.equals("GA") || issueState.equals("WI") || issueState.equals("WA")) {


						                        //Page 1 Static Texts
						                        // Assert.assertTrue(filteredText.contains("STATEMENT OF BENEFIT INFORMATION – CONTRACT SUMMARY"));
												writeToCSV("POLICY NUMBER", var_PolicyNumber);
						                        writeToCSV("PAGE #: DATA TYPE", "PAGE 1: STATIC TEXT");
						                        writeToCSV("EXPECTED DATA", "STATEMENT OF BENEFIT INFORMATION – CONTRACT SUMMARY");
						                        writeToCSV("ACTUAL DATA ON PDF", "STATEMENT OF BENEFIT INFORMATION – CONTRACT SUMMARY");
						                        writeToCSV("RESULT", (filteredText.contains("STATEMENT OF BENEFIT INFORMATION – CONTRACT SUMMARY")) ? "PASS" : "FAIL");

						                        //Page 1 Static Texts
						                        // Assert.assertTrue(filteredText.contains("POLICY NUMBER:"));
												writeToCSV("POLICY NUMBER", var_PolicyNumber);
						                        writeToCSV("PAGE #: DATA TYPE", "PAGE 1: STATIC TEXT");
						                        writeToCSV("EXPECTED DATA", "POLICY NUMBER:");
						                        writeToCSV("ACTUAL DATA ON PDF", "POLICY NUMBER:");
						                        writeToCSV("RESULT", (filteredText.contains("POLICY NUMBER:")) ? "PASS" : "FAIL");

						                        //Page 1: STATIC TEXTs
						                        // Assert.assertTrue(filteredText.contains("CONTRACT SUMMARY DATE:"));
												writeToCSV("POLICY NUMBER", var_PolicyNumber);
						                        writeToCSV("PAGE #: DATA TYPE", "PAGE 1: STATIC TEXT");
						                        writeToCSV("EXPECTED DATA", "CONTRACT SUMMARY DATE:");
						                        writeToCSV("ACTUAL DATA ON PDF", "CONTRACT SUMMARY DATE:");
						                        writeToCSV("RESULT", (filteredText.contains("CONTRACT SUMMARY DATE:")) ? "PASS" : "FAIL");

						                        //Page 1: STATIC TEXTs
						                        // Assert.assertTrue(filteredText.contains("ANNUITANT(S) NAME(S):"));
												writeToCSV("POLICY NUMBER", var_PolicyNumber);
						                        writeToCSV("PAGE #: DATA TYPE", "PAGE 1: STATIC TEXT");
						                        writeToCSV("EXPECTED DATA", "ANNUITANT(S) NAME(S):");
						                        writeToCSV("ACTUAL DATA ON PDF", "ANNUITANT(S) NAME(S):");
						                        writeToCSV("RESULT", (filteredText.contains("ANNUITANT(S) NAME(S):")) ? "PASS" : "FAIL");


						                        //Page 1: STATIC TEXTs
						                        // Assert.assertTrue(filteredText.contains("ISSUE AGE(S):"));
												writeToCSV("POLICY NUMBER", var_PolicyNumber);
						                        writeToCSV("PAGE #: DATA TYPE", "PAGE 1: STATIC TEXT");
						                        writeToCSV("EXPECTED DATA", "ISSUE AGE(S):");
						                        writeToCSV("ACTUAL DATA ON PDF", "ISSUE AGE(S):");
						                        writeToCSV("RESULT", (filteredText.contains("ISSUE AGE(S):")) ? "PASS" : "FAIL");

						                        //Page 1: STATIC TEXTs
						                        // Assert.assertTrue(filteredText.contains("SEX:"));
												writeToCSV("POLICY NUMBER", var_PolicyNumber);
						                        writeToCSV("PAGE #: DATA TYPE", "PAGE 1: STATIC TEXT");
						                        writeToCSV("EXPECTED DATA", "SEX:");
						                        writeToCSV("ACTUAL DATA ON PDF", "SEX:");
						                        writeToCSV("RESULT", (filteredText.contains("SEX:")) ? "PASS" : "FAIL");

						                        //Page 1: STATIC TEXTs
						                        // Assert.assertTrue(filteredText.contains("PREMIUM:"));
												writeToCSV("POLICY NUMBER", var_PolicyNumber);
						                        writeToCSV("PAGE #: DATA TYPE", "PAGE 1: STATIC TEXT");
						                        writeToCSV("EXPECTED DATA", "PREMIUM:");
						                        writeToCSV("ACTUAL DATA ON PDF", "PREMIUM:");
						                        writeToCSV("RESULT", (filteredText.contains("PREMIUM:")) ? "PASS" : "FAIL");

						                        //Page 1: STATIC TEXTs
						                        // Assert.assertTrue(filteredText.contains("THE MINIMUM INITIAL PREMIUM ALLOWED FOR THIS PRODUCT IS"));
												writeToCSV("POLICY NUMBER", var_PolicyNumber);
						                        writeToCSV("PAGE #: DATA TYPE", "PAGE 1: STATIC TEXT");
						                        writeToCSV("EXPECTED DATA", "THE MINIMUM INITIAL PREMIUM ALLOWED FOR THIS PRODUCT IS");
						                        writeToCSV("ACTUAL DATA ON PDF", "THE MINIMUM INITIAL PREMIUM ALLOWED FOR THIS PRODUCT IS");
						                        writeToCSV("RESULT", (filteredText.contains("THE MINIMUM INITIAL PREMIUM ALLOWED FOR THIS PRODUCT IS")) ? "PASS" : "FAIL");

						                        //Page 1: STATIC TEXTs
						                        // Assert.assertTrue(filteredText.contains("THE MAXIMUM PREMIUM ALLOWED FOR THIS PRODUCT WITHOUT HOME OFFICE APPROVAL IS"));
												writeToCSV("POLICY NUMBER", var_PolicyNumber);
						                        writeToCSV("PAGE #: DATA TYPE", "PAGE 1: STATIC TEXT");
						                        writeToCSV("EXPECTED DATA", "THE MAXIMUM PREMIUM ALLOWED FOR THIS PRODUCT WITHOUT HOME OFFICE APPROVAL IS");
						                        writeToCSV("ACTUAL DATA ON PDF", "THE MAXIMUM PREMIUM ALLOWED FOR THIS PRODUCT WITHOUT HOME OFFICE APPROVAL IS");
						                        writeToCSV("RESULT", (filteredText.contains("THE MAXIMUM PREMIUM ALLOWED FOR THIS PRODUCT WITHOUT HOME OFFICE APPROVAL IS")) ? "PASS" : "FAIL");

						                        //Page 1: STATIC TEXTs
						                        // Assert.assertTrue(filteredText.contains("END OF\nPOLICY\nYEAR"));
												writeToCSV("POLICY NUMBER", var_PolicyNumber);
						                        writeToCSV("PAGE #: DATA TYPE", "PAGE 1: STATIC TEXT");
						                        writeToCSV("EXPECTED DATA", "END OF POLICY YEAR");
						                        writeToCSV("ACTUAL DATA ON PDF", "END OF POLICY YEAR");
						                        writeToCSV("RESULT", (filteredText.contains("END OF\nPOLICY\nYEAR")) ? "PASS" : "FAIL");

						                        //Page 1: STATIC TEXTs
						                        // Assert.assertTrue(filteredText.contains("AGE"));
												writeToCSV("POLICY NUMBER", var_PolicyNumber);
						                        writeToCSV("PAGE #: DATA TYPE", "PAGE 1: STATIC TEXT");
						                        writeToCSV("EXPECTED DATA", "AGE");
						                        writeToCSV("ACTUAL DATA ON PDF", "AGE");
						                        writeToCSV("RESULT", (filteredText.contains("AGE")) ? "PASS" : "FAIL");

						                        //Page 1: STATIC TEXTs
						                        // Assert.assertTrue(filteredText.contains("PROJECTED\nANNUAL\nPREMIUMS"));
												writeToCSV("POLICY NUMBER", var_PolicyNumber);
						                        writeToCSV("PAGE #: DATA TYPE", "PAGE 1: STATIC TEXT");
						                        writeToCSV("EXPECTED DATA", "PROJECTED ANNUAL PREMIUMS");
						                        writeToCSV("ACTUAL DATA ON PDF", "PROJECTED ANNUAL PREMIUMS");
						                        writeToCSV("RESULT", (filteredText.contains("PROJECTED\nANNUAL\nPREMIUMS")) ? "PASS" : "FAIL");

						                        //Page 1: STATIC TEXTs
						                        // Assert.assertTrue(filteredText.contains("GUARANTEED\nSURRENDER\nVALUE"));
												writeToCSV("POLICY NUMBER", var_PolicyNumber);
						                        writeToCSV("PAGE #: DATA TYPE", "PAGE 1: STATIC TEXT");
						                        writeToCSV("EXPECTED DATA", "GUARANTEED SURRENDER VALUE");
						                        writeToCSV("ACTUAL DATA ON PDF", "GUARANTEED SURRENDER VALUE");
						                        writeToCSV("RESULT", (filteredText.contains("GUARANTEED\nSURRENDER\nVALUE")) ? "PASS" : "FAIL");

						                        //Page 1: STATIC TEXTs
						                        // Assert.assertTrue(filteredText.contains("GUARANTEED\nDEATH BENEFIT\nVALUE"));
												writeToCSV("POLICY NUMBER", var_PolicyNumber);
						                        writeToCSV("PAGE #: DATA TYPE", "PAGE 1: STATIC TEXT");
						                        writeToCSV("EXPECTED DATA", "GUARANTEED DEATH BENEFIT VALUE");
						                        writeToCSV("ACTUAL DATA ON PDF", "GUARANTEED DEATH BENEFIT VALUE");
						                        writeToCSV("RESULT", (filteredText.contains("GUARANTEED\nDEATH BENEFIT\nVALUE")) ? "PASS" : "FAIL");

						                        //Page 1: STATIC TEXTs
						                        // Assert.assertTrue(filteredText.contains("THIS POLICY MAY RESULT IN A LOSS IF SURRENDERED DURING THE PERIOD WHEN SURRENDER CHARGES APPLY."));
												writeToCSV("POLICY NUMBER", var_PolicyNumber);
						                        writeToCSV("PAGE #: DATA TYPE", "PAGE 1: STATIC TEXT");
						                        writeToCSV("EXPECTED DATA", "THIS POLICY MAY RESULT IN A LOSS IF SURRENDERED DURING THE PERIOD WHEN SURRENDER CHARGES APPLY.");
						                        writeToCSV("ACTUAL DATA ON PDF", "THIS POLICY MAY RESULT IN A LOSS IF SURRENDERED DURING THE PERIOD WHEN SURRENDER CHARGES APPLY.");
						                        writeToCSV("RESULT", (filteredText.contains("THIS POLICY MAY RESULT IN A LOSS IF SURRENDERED DURING THE PERIOD WHEN SURRENDER CHARGES APPLY.")) ? "PASS" : "FAIL");

						                        //Page 1: STATIC TEXTs
						                        // Assert.assertTrue(filteredText.contains("SCENARIO"));
												writeToCSV("POLICY NUMBER", var_PolicyNumber);
						                        writeToCSV("PAGE #: DATA TYPE", "PAGE 1: STATIC TEXT");
						                        writeToCSV("EXPECTED DATA", "SCENARIO");
						                        writeToCSV("ACTUAL DATA ON PDF", "SCENARIO");
						                        writeToCSV("RESULT", (filteredText.contains("SCENARIO")) ? "PASS" : "FAIL");

						                        //Page 1: STATIC TEXTs
						                        // Assert.assertTrue(filteredText.contains("THE TABLE ABOVE SHOWS THE ACCOUNT VALUES, DEATH BENEFIT VALUES, AND SURRENDER VALUES UNDER THE GUARANTEED SCENARIO."));
						                        writeToCSV("POLICY NUMBER", var_PolicyNumber);
												writeToCSV("PAGE #: DATA TYPE", "PAGE 1: STATIC TEXT");
						                        writeToCSV("EXPECTED DATA", "THE TABLE ABOVE SHOWS THE ACCOUNT VALUES, DEATH BENEFIT VALUES, AND SURRENDER VALUES UNDER THE GUARANTEED SCENARIO.");
						                        writeToCSV("ACTUAL DATA ON PDF", "THE TABLE ABOVE SHOWS THE ACCOUNT VALUES, DEATH BENEFIT VALUES, AND SURRENDER VALUES UNDER THE GUARANTEED SCENARIO.");
						                        writeToCSV("RESULT", (filteredText.contains("THE TABLE ABOVE SHOWS THE ACCOUNT VALUES, DEATH BENEFIT VALUES, AND SURRENDER VALUES UNDER THE GUARANTEED SCENARIO.")) ? "PASS" : "FAIL");

						                        //Page 1: STATIC TEXTs
						                        // Assert.assertTrue(filteredText.contains("THE GUARANTEED VALUES SHOWN ASSUME A 0.00%"));
						                        writeToCSV("POLICY NUMBER", var_PolicyNumber);
												writeToCSV("PAGE #: DATA TYPE", "PAGE 1: STATIC TEXT");
						                        writeToCSV("EXPECTED DATA", "THE GUARANTEED VALUES SHOWN ASSUME A 0.00%");
						                        writeToCSV("ACTUAL DATA ON PDF", "THE GUARANTEED VALUES SHOWN ASSUME A 0.00%");
						                        writeToCSV("RESULT", (filteredText.contains("THE GUARANTEED VALUES SHOWN ASSUME A 0.00%")) ? "PASS" : "FAIL");

						                        //Page 1: STATIC TEXTs
						                        // Assert.assertTrue(filteredText.contains("EFFECTIVE ANNUAL INTEREST RATE FOR THE DURATION OF THE PROJECTION."));
												writeToCSV("POLICY NUMBER", var_PolicyNumber);		                       
											   writeToCSV("PAGE #: DATA TYPE", "PAGE 1: STATIC TEXT");
						                        writeToCSV("EXPECTED DATA", "EFFECTIVE ANNUAL INTEREST RATE FOR THE DURATION OF THE PROJECTION.");
						                        writeToCSV("ACTUAL DATA ON PDF", "EFFECTIVE ANNUAL INTEREST RATE FOR THE DURATION OF THE PROJECTION.");
						                        writeToCSV("RESULT", (filteredText.contains("EFFECTIVE ANNUAL INTEREST RATE FOR THE DURATION OF THE PROJECTION.")) ? "PASS" : "FAIL");

						                        //Page 1: STATIC TEXTs
						                        // Assert.assertTrue(filteredText.contains("THE GUARANTEED VALUES SHOW THE MINIMUM GUARANTEED POLICY VALUES DESCRIBED IN THE POLICY, ASSUMING NO WITHDRAWALS OR SURRENDER."));
						                        writeToCSV("POLICY NUMBER", var_PolicyNumber);
												writeToCSV("PAGE #: DATA TYPE", "PAGE 1: STATIC TEXT");
						                        writeToCSV("EXPECTED DATA", "THE GUARANTEED VALUES SHOW THE MINIMUM GUARANTEED POLICY VALUES DESCRIBED IN THE POLICY, ASSUMING NO WITHDRAWALS OR SURRENDER.");
						                        writeToCSV("ACTUAL DATA ON PDF", "THE GUARANTEED VALUES SHOW THE MINIMUM GUARANTEED POLICY VALUES DESCRIBED IN THE POLICY, ASSUMING NO WITHDRAWALS OR SURRENDER.");
						                        writeToCSV("RESULT", (filteredText.contains("THE GUARANTEED VALUES SHOW THE MINIMUM GUARANTEED POLICY VALUES DESCRIBED IN THE POLICY, ASSUMING NO WITHDRAWALS OR SURRENDER.")) ? "PASS" : "FAIL");


						                        //Page 2: STATIC TEXTs
												writeToCSV("POLICY NUMBER", var_PolicyNumber);
						                        writeToCSV("PAGE #: DATA TYPE", "PAGE 2: STATIC TEXT");
						                        writeToCSV("EXPECTED DATA", "Page 2");
						                        writeToCSV("ACTUAL DATA ON PDF", "Page 2");
						                        writeToCSV("RESULT", (filteredText.contains("Page 2")) ? "PASS" : "FAIL");
											    
												writeToCSV("POLICY NUMBER", var_PolicyNumber);
						                        writeToCSV("PAGE #: DATA TYPE", "PAGE 2: STATIC TEXT");
						                        writeToCSV("EXPECTED DATA", "SURRENDERS");
						                        writeToCSV("ACTUAL DATA ON PDF", "SURRENDERS");
						                        writeToCSV("RESULT", (filteredText.contains("SURRENDERS")) ? "PASS" : "FAIL");

						                        //Page 2: STATIC TEXTs
												writeToCSV("POLICY NUMBER", var_PolicyNumber);
						                        writeToCSV("PAGE #: DATA TYPE", "PAGE 2: STATIC TEXT");
						                        writeToCSV("EXPECTED DATA", "POLICY");
						                        writeToCSV("ACTUAL DATA ON PDF", policyLine);
						                        writeToCSV("RESULT", (filteredText.contains(policyLine)) ? "PASS" : "FAIL");

						                        //Page 2: STATIC TEXTs
												writeToCSV("POLICY NUMBER", var_PolicyNumber);
						                        writeToCSV("PAGE #: DATA TYPE", "PAGE 2: STATIC TEXT");
						                        writeToCSV("EXPECTED DATA", "FACTOR");
						                        writeToCSV("ACTUAL DATA ON PDF", factorLine);
						                        writeToCSV("RESULT", (filteredText.contains(factorLine)) ? "PASS" : "FAIL");

						                        //Page 2: STATIC TEXTs
												writeToCSV("POLICY NUMBER", var_PolicyNumber);
						                        writeToCSV("PAGE #: DATA TYPE", "PAGE 2: STATIC TEXT");
						                        writeToCSV("EXPECTED DATA", "SURRENDER");
						                        writeToCSV("ACTUAL DATA ON PDF", surrenderLine);
						                        writeToCSV("RESULT", (filteredText.contains(surrenderLine)) ? "PASS" : "FAIL");

						                        //Page 2: STATIC TEXTs
												writeToCSV("POLICY NUMBER", var_PolicyNumber);
						                        writeToCSV("PAGE #: DATA TYPE", "PAGE 2: STATIC TEXT");
						                        writeToCSV("EXPECTED DATA", "YEAR");
						                        writeToCSV("ACTUAL DATA ON PDF", yearLine);
						                        writeToCSV("RESULT", (filteredText.contains(yearLine)) ? "PASS" : "FAIL");

						                        //Page 2: STATIC TEXTs
												writeToCSV("POLICY NUMBER", var_PolicyNumber);
						                        writeToCSV("PAGE #: DATA TYPE", "PAGE 2: STATIC TEXT");
						                        writeToCSV("EXPECTED DATA", "YEAR COUNT 2");
						                        writeToCSV("ACTUAL DATA ON PDF", "YEAR COUNT " + yearCount);
						                        writeToCSV("RESULT", (yearCount == 2) ? "PASS" : "FAIL");

						                        //Page 2: STATIC TEXTs
												writeToCSV("POLICY NUMBER", var_PolicyNumber);
						                        writeToCSV("PAGE #: DATA TYPE", "PAGE 2: STATIC TEXT");
						                        writeToCSV("EXPECTED DATA", "POLICY COUNT 3");
						                        writeToCSV("ACTUAL DATA ON PDF", "POLICY COUNT " + policyCount);
						                        writeToCSV("RESULT", (policyCount == 3) ? "PASS" : "FAIL");

						                        //Page 2: STATIC TEXTs
												writeToCSV("POLICY NUMBER", var_PolicyNumber);
						                        writeToCSV("PAGE #: DATA TYPE", "PAGE 2: STATIC TEXT");
						                        writeToCSV("EXPECTED DATA", "FACTOR COUNT 2");
						                        writeToCSV("ACTUAL DATA ON PDF", "FACTOR COUNT " + factorCount);
						                        writeToCSV("RESULT", (factorCount == 2) ? "PASS" : "FAIL");

						                        //Page 2: STATIC TEXTs
												writeToCSV("POLICY NUMBER", var_PolicyNumber);
						                        writeToCSV("PAGE #: DATA TYPE", "PAGE 2: STATIC TEXT");
						                        writeToCSV("EXPECTED DATA", "BEFORE THE ANNUITY DATE, THE POLICY MAY BE SURRENDERED FOR ALL OR A PORTION OF I");
						                        writeToCSV("ACTUAL DATA ON PDF", "BEFORE THE ANNUITY DATE, THE POLICY MAY BE SURRENDERED FOR ALL OR A PORTION OF I");
						                        writeToCSV("RESULT", (filteredText.contains("BEFORE THE ANNUITY DATE, THE POLICY MAY BE SURRENDERED FOR ALL OR A PORTION OF I")) ? "PASS" : "FAIL");

						                        //Page 2: STATIC TEXTs
												writeToCSV("POLICY NUMBER", var_PolicyNumber);
						                        writeToCSV("PAGE #: DATA TYPE", "PAGE 2: STATIC TEXT");
						                        writeToCSV("EXPECTED DATA", "TS SURRENDER VALUE. THE TOTAL SURRENDER VALUE EQUALS THE");
						                        writeToCSV("ACTUAL DATA ON PDF", "TS SURRENDER VALUE. THE TOTAL SURRENDER VALUE EQUALS THE");
						                        writeToCSV("RESULT", (filteredText.contains("TS SURRENDER VALUE. THE TOTAL SURRENDER VALUE EQUALS THE")) ? "PASS" : "FAIL");

						                        //Page 2: STATIC TEXTs
												writeToCSV("POLICY NUMBER", var_PolicyNumber);
						                        writeToCSV("PAGE #: DATA TYPE", "PAGE 2: STATIC TEXT");
						                        writeToCSV("EXPECTED DATA", "SUM OF THE SURRENDER VALUES FOR THE INTEREST CREDITING OPTIONS. THE SURRENDER VA");
						                        writeToCSV("ACTUAL DATA ON PDF", "SUM OF THE SURRENDER VALUES FOR THE INTEREST CREDITING OPTIONS. THE SURRENDER VA");
						                        writeToCSV("RESULT", (filteredText.contains("SUM OF THE SURRENDER VALUES FOR THE INTEREST CREDITING OPTIONS. THE SURRENDER VA")) ? "PASS" : "FAIL");


						                        //Page 2: STATIC TEXTs
												writeToCSV("POLICY NUMBER", var_PolicyNumber);
						                        writeToCSV("PAGE #: DATA TYPE", "PAGE 2: STATIC TEXT");
						                        writeToCSV("EXPECTED DATA", "LUE FOR EACH OPTION IS THE GREATER OF THE OPTION'S");
						                        writeToCSV("ACTUAL DATA ON PDF", "LUE FOR EACH OPTION IS THE GREATER OF THE OPTION'S");
						                        writeToCSV("RESULT", (filteredText.contains("LUE FOR EACH OPTION IS THE GREATER OF THE OPTION")) ? "PASS" : "FAIL");

						                        //Page 2: STATIC TEXTs
												writeToCSV("POLICY NUMBER", var_PolicyNumber);
						                        writeToCSV("PAGE #: DATA TYPE", "PAGE 2: STATIC TEXT");
						                        writeToCSV("EXPECTED DATA", "ACCOUNT VALUE LESS SURRENDER CHARGES, OR THE OPTION'S MINIMUM GUARANTEED SURREND");
						                        writeToCSV("ACTUAL DATA ON PDF", "ACCOUNT VALUE LESS SURRENDER CHARGES, OR THE OPTION'S MINIMUM GUARANTEED SURREND");
						                        writeToCSV("RESULT", (filteredText.contains("ACCOUNT VALUE LESS SURRENDER CHARGES, OR THE OPTION") && filteredText.contains("S MINIMUM GUARANTEED SURREND")) ? "PASS" : "FAIL");

						                        //Page 2: STATIC TEXTs
												writeToCSV("POLICY NUMBER", var_PolicyNumber);
						                        writeToCSV("PAGE #: DATA TYPE", "PAGE 2: STATIC TEXT");
						                        writeToCSV("EXPECTED DATA", "ER VALUE DEFINED IN THE POLICY.");
						                        writeToCSV("ACTUAL DATA ON PDF", "ER VALUE DEFINED IN THE POLICY.");
						                        writeToCSV("RESULT", (filteredText.contains("ER VALUE DEFINED IN THE POLICY.")) ? "PASS" : "FAIL");

						                        //Page 2: STATIC TEXTs
												writeToCSV("POLICY NUMBER", var_PolicyNumber);
						                        writeToCSV("PAGE #: DATA TYPE", "PAGE 2: STATIC TEXT");
						                        writeToCSV("EXPECTED DATA", "A SURRENDER CHARGE MAY BE IMPOSED ON WITHDRAWALS AND IN CALCULATING ANNUITY PAYM");
						                        writeToCSV("ACTUAL DATA ON PDF", "A SURRENDER CHARGE MAY BE IMPOSED ON WITHDRAWALS AND IN CALCULATING ANNUITY PAYM");
						                        writeToCSV("RESULT", (filteredText.contains("A SURRENDER CHARGE MAY BE IMPOSED ON WITHDRAWALS AND IN CALCULATING ANNUITY PAYM")) ? "PASS" : "FAIL");

						                        //Page 2: STATIC TEXTs
												writeToCSV("POLICY NUMBER", var_PolicyNumber);
						                        writeToCSV("PAGE #: DATA TYPE", "PAGE 2: STATIC TEXT");
						                        writeToCSV("EXPECTED DATA", "ENTS. THE SURRENDER CHARGE IS EQUAL TO THE SURRENDER");
						                        writeToCSV("ACTUAL DATA ON PDF", "ENTS. THE SURRENDER CHARGE IS EQUAL TO THE SURRENDER");
						                        writeToCSV("RESULT", (filteredText.contains("ENTS. THE SURRENDER CHARGE IS EQUAL TO THE SURRENDER")) ? "PASS" : "FAIL");

						                        //Page 2: STATIC TEXTs
												writeToCSV("POLICY NUMBER", var_PolicyNumber);
						                        writeToCSV("PAGE #: DATA TYPE", "PAGE 2: STATIC TEXT");
						                        writeToCSV("EXPECTED DATA", "FACTOR FOR THE POLICY YEAR (AS SHOWN BELOW) MULTIPLIED BY THE AMOUNT OF THE");
						                        writeToCSV("ACTUAL DATA ON PDF", "FACTOR FOR THE POLICY YEAR (AS SHOWN BELOW) MULTIPLIED BY THE AMOUNT OF THE");
						                        writeToCSV("RESULT", (filteredText.contains("FACTOR FOR THE POLICY YEAR (AS SHOWN BELOW) MULTIPLIED BY THE AMOUNT OF THE")) ? "PASS" : "FAIL");

						                        //Page 2: STATIC TEXTs
												writeToCSV("POLICY NUMBER", var_PolicyNumber);
						                        writeToCSV("PAGE #: DATA TYPE", "PAGE 2: STATIC TEXT");
						                        writeToCSV("EXPECTED DATA", "UNT VALUE IN EXCESS OF ANY APPLICABLE FREE AMOUNT.");
						                        writeToCSV("ACTUAL DATA ON PDF", "UNT VALUE IN EXCESS OF ANY APPLICABLE FREE AMOUNT.");
						                        writeToCSV("RESULT", (filteredText.contains("UNT VALUE IN EXCESS OF ANY APPLICABLE FREE AMOUNT.")) ? "PASS" : "FAIL");

						                        //Page 2: STATIC TEXTs
												writeToCSV("POLICY NUMBER", var_PolicyNumber);
						                        writeToCSV("PAGE #: DATA TYPE", "PAGE 2: STATIC TEXT");
						                        writeToCSV("EXPECTED DATA", "THE SURRENDER CHARGE DOES NOT APPLY:");
						                        writeToCSV("ACTUAL DATA ON PDF", "THE SURRENDER CHARGE DOES NOT APPLY:");
						                        writeToCSV("RESULT", (filteredText.contains("THE SURRENDER CHARGE DOES NOT APPLY:")) ? "PASS" : "FAIL");

						                        //Page 2: STATIC TEXTs
												writeToCSV("POLICY NUMBER", var_PolicyNumber);
						                        writeToCSV("PAGE #: DATA TYPE", "PAGE 2: STATIC TEXT");
						                        writeToCSV("EXPECTED DATA", "- ON A FREE PARTIAL WITHDRAWAL (UP TO 10% OF THE");
						                        writeToCSV("ACTUAL DATA ON PDF", "- ON A FREE PARTIAL WITHDRAWAL (UP TO 10% OF THE");
						                        writeToCSV("RESULT", (filteredText.contains("- ON A FREE PARTIAL WITHDRAWAL (UP TO 10% OF THE")) ? "PASS" : "FAIL");

						                        //Page 2: STATIC TEXTs
												writeToCSV("POLICY NUMBER", var_PolicyNumber);
						                        writeToCSV("PAGE #: DATA TYPE", "PAGE 2: STATIC TEXT");
						                        writeToCSV("EXPECTED DATA", "ACCOUNT VALUE AS OF THE PRIOR P");
						                        writeToCSV("ACTUAL DATA ON PDF", "ACCOUNT VALUE AS OF THE PRIOR P");
						                        writeToCSV("RESULT", (filteredText.contains("ACCOUNT VALUE AS OF THE PRIOR P")) ? "PASS" : "FAIL");

						                        //Page 2: STATIC TEXTs
												writeToCSV("POLICY NUMBER", var_PolicyNumber);
						                        writeToCSV("PAGE #: DATA TYPE", "PAGE 2: STATIC TEXT");
						                        writeToCSV("EXPECTED DATA", "OLICY ANNIVERSARY, AVAILABLE AFTER THE FIRST POLICY");
						                        writeToCSV("ACTUAL DATA ON PDF", "OLICY ANNIVERSARY, AVAILABLE AFTER THE FIRST POLICY");
						                        writeToCSV("RESULT", (filteredText.contains("OLICY ANNIVERSARY, AVAILABLE AFTER THE FIRST POLICY")) ? "PASS" : "FAIL");

						                        //Page 2: STATIC TEXTs
												writeToCSV("POLICY NUMBER", var_PolicyNumber);
						                        writeToCSV("PAGE #: DATA TYPE", "PAGE 2: STATIC TEXT");
						                        writeToCSV("EXPECTED DATA", "ANNIVERSARY);");
						                        writeToCSV("ACTUAL DATA ON PDF", "ANNIVERSARY);");
						                        writeToCSV("RESULT", (filteredText.contains("ANNIVERSARY);")) ? "PASS" : "FAIL");

						                        //Page 2: STATIC TEXTs
												writeToCSV("POLICY NUMBER", var_PolicyNumber);
						                        writeToCSV("PAGE #: DATA TYPE", "PAGE 2: STATIC TEXT");
						                        writeToCSV("EXPECTED DATA", "- AFTER THE OWNER'S DEATH (UNLESS THE SPOUSE OF THE FIRST OWNER TO DIE CONTINUES");
						                        writeToCSV("ACTUAL DATA ON PDF", "- AFTER THE OWNER'S DEATH (UNLESS THE SPOUSE OF THE FIRST OWNER TO DIE CONTINUES");
						                        writeToCSV("RESULT", (filteredText.contains("- AFTER THE OWNER") && filteredText.contains("S DEATH (UNLESS THE SPOUSE OF THE FIRST OWNER TO DIE CONTINUES")) ? "PASS" : "FAIL");

						                        //Page 2: STATIC TEXTs
												writeToCSV("POLICY NUMBER", var_PolicyNumber);
						                        writeToCSV("PAGE #: DATA TYPE", "PAGE 2: STATIC TEXT");
						                        writeToCSV("EXPECTED DATA", "OR SUCCEEDS TO OWNERSHIP OF THE POLICY);");
						                        writeToCSV("ACTUAL DATA ON PDF", "OR SUCCEEDS TO OWNERSHIP OF THE POLICY);");
						                        writeToCSV("RESULT", (filteredText.contains("OR SUCCEEDS TO OWNERSHIP OF THE POLICY);")) ? "PASS" : "FAIL");

						                        //Page 2: STATIC TEXTs
												writeToCSV("POLICY NUMBER", var_PolicyNumber);
						                        writeToCSV("PAGE #: DATA TYPE", "PAGE 2: STATIC TEXT");
						                        writeToCSV("EXPECTED DATA", "- WHERE CHARGES ARE WAIVED VIA AN ATTACHED RIDER; OR");
						                        writeToCSV("ACTUAL DATA ON PDF", "- WHERE CHARGES ARE WAIVED VIA AN ATTACHED RIDER; OR");
						                        writeToCSV("RESULT", (filteredText.contains("- WHERE CHARGES ARE WAIVED VIA AN ATTACHED RIDER; OR")) ? "PASS" : "FAIL");

						                        //Page 2: STATIC TEXTs
												writeToCSV("POLICY NUMBER", var_PolicyNumber);
						                        writeToCSV("PAGE #: DATA TYPE", "PAGE 2: STATIC TEXT");
						                        writeToCSV("EXPECTED DATA", "YEARS AFTER THE DATE OF ISSUE.");
						                        writeToCSV("ACTUAL DATA ON PDF", "YEARS AFTER THE DATE OF ISSUE.");
						                        writeToCSV("RESULT", (filteredText.contains("YEARS AFTER THE DATE OF ISSUE.")) ? "PASS" : "FAIL");

						                        //Page 3: STATIC TEXTs
												writeToCSV("POLICY NUMBER", var_PolicyNumber);
						                        writeToCSV("PAGE #: DATA TYPE", "PAGE 3:: STATIC TEXT");
						                        writeToCSV("EXPECTED DATA", "Page 3");
						                        writeToCSV("ACTUAL DATA ON PDF", "Page 3");
						                        writeToCSV("RESULT", (filteredText.contains("Page 3")) ? "PASS" : "FAIL");


						                        //Page 3 STATIC TEXTs
												writeToCSV("POLICY NUMBER", var_PolicyNumber);
						                        writeToCSV("PAGE #: DATA TYPE", "PAGE 3: STATIC TEXT");
						                        writeToCSV("EXPECTED DATA", "ANNUITY OPTIONS");
						                        writeToCSV("ACTUAL DATA ON PDF", "ANNUITY OPTIONS");
						                        writeToCSV("RESULT", (filteredText.contains("ANNUITY OPTIONS")) ? "PASS" : "FAIL");

						                        //Page 3 STATIC TEXTs
												writeToCSV("POLICY NUMBER", var_PolicyNumber);
						                        writeToCSV("PAGE #: DATA TYPE", "PAGE 3: STATIC TEXT");
						                        writeToCSV("EXPECTED DATA", "THE FOLLOWING ANNUITY OPTIONS ARE AVAILABLE UNDER THIS POLICY:");
						                        writeToCSV("ACTUAL DATA ON PDF", "THE FOLLOWING ANNUITY OPTIONS ARE AVAILABLE UNDER THIS POLICY:");
						                        writeToCSV("RESULT", (filteredText.contains("THE FOLLOWING ANNUITY OPTIONS ARE AVAILABLE UNDER THIS POLICY:")) ? "PASS" : "FAIL");

						                        //Page 3 STATIC TEXTs
												writeToCSV("POLICY NUMBER", var_PolicyNumber);
						                        writeToCSV("PAGE #: DATA TYPE", "PAGE 3: STATIC TEXT");
						                        writeToCSV("EXPECTED DATA", "'- INCOME FOR A FIXED PERIOD");
						                        writeToCSV("ACTUAL DATA ON PDF", "'- INCOME FOR A FIXED PERIOD");
						                        writeToCSV("RESULT", (filteredText.contains("- INCOME FOR A FIXED PERIOD")) ? "PASS" : "FAIL");

						                        //Page 3 STATIC TEXTs
												writeToCSV("POLICY NUMBER", var_PolicyNumber);
						                        writeToCSV("PAGE #: DATA TYPE", "PAGE 3: STATIC TEXT");
						                        writeToCSV("EXPECTED DATA", "'- INCOME FOR A FIXED PERIOD");
						                        writeToCSV("ACTUAL DATA ON PDF", "'- INCOME FOR A FIXED PERIOD");
						                        writeToCSV("RESULT", (filteredText.contains("- INCOME FOR A FIXED PERIOD")) ? "PASS" : "FAIL");

						                        //Page 3 STATIC TEXTs
												writeToCSV("POLICY NUMBER", var_PolicyNumber);
						                        writeToCSV("PAGE #: DATA TYPE", "PAGE 3: STATIC TEXT");
						                        writeToCSV("EXPECTED DATA", "'- INCOME FOR A FIXED PERIOD");
						                        writeToCSV("ACTUAL DATA ON PDF", "'- INCOME FOR A FIXED PERIOD");
						                        writeToCSV("RESULT", (filteredText.contains("- INCOME FOR A FIXED PERIOD")) ? "PASS" : "FAIL");

						                        //Page 3 STATIC TEXTs
												writeToCSV("POLICY NUMBER", var_PolicyNumber);
						                        writeToCSV("PAGE #: DATA TYPE", "PAGE 3: STATIC TEXT");
						                        writeToCSV("EXPECTED DATA", "'- LIFE INCOME WITH A GUARANTEED PERIOD");
						                        writeToCSV("ACTUAL DATA ON PDF", "'- LIFE INCOME WITH A GUARANTEED PERIOD");
						                        writeToCSV("RESULT", (filteredText.contains("- LIFE INCOME WITH A GUARANTEED PERIOD")) ? "PASS" : "FAIL");

						                        //Page 3 STATIC TEXTs
												writeToCSV("POLICY NUMBER", var_PolicyNumber);
						                        writeToCSV("PAGE #: DATA TYPE", "PAGE 3: STATIC TEXT");
						                        writeToCSV("EXPECTED DATA", "'- LIFE INCOME");
						                        writeToCSV("ACTUAL DATA ON PDF", "'- LIFE INCOME");
						                        writeToCSV("RESULT", (filteredText.contains("- LIFE INCOME")) ? "PASS" : "FAIL");

						                        //Page 3 STATIC TEXTs
												writeToCSV("POLICY NUMBER", var_PolicyNumber);
						                        writeToCSV("PAGE #: DATA TYPE", "PAGE 3: STATIC TEXT");
						                        writeToCSV("EXPECTED DATA", "'- JOINT AND SURVIVOR INCOME WITH GUARANTEED PERIOD");
						                        writeToCSV("ACTUAL DATA ON PDF", "'- JOINT AND SURVIVOR INCOME WITH GUARANTEED PERIOD");
						                        writeToCSV("RESULT", (filteredText.contains("- JOINT AND SURVIVOR INCOME WITH GUARANTEED PERIOD")) ? "PASS" : "FAIL");

						                        //Page 3 STATIC TEXTs
												writeToCSV("POLICY NUMBER", var_PolicyNumber);
						                        writeToCSV("PAGE #: DATA TYPE", "PAGE 3: STATIC TEXT");
						                        writeToCSV("EXPECTED DATA", "'- JOINT AND SURVIVOR LIFE INCOME");
						                        writeToCSV("ACTUAL DATA ON PDF", "'- JOINT AND SURVIVOR LIFE INCOME");
						                        writeToCSV("RESULT", (filteredText.contains("- JOINT AND SURVIVOR LIFE INCOME")) ? "PASS" : "FAIL");

						                        //Page 3 STATIC TEXTs
												writeToCSV("POLICY NUMBER", var_PolicyNumber);
						                        writeToCSV("PAGE #: DATA TYPE", "PAGE 3: STATIC TEXT");
						                        writeToCSV("EXPECTED DATA", "'- LIFE INCOME WITH LUMP SUM REFUND AT DEATH");
						                        writeToCSV("ACTUAL DATA ON PDF", "'- LIFE INCOME WITH LUMP SUM REFUND AT DEATH");
						                        writeToCSV("RESULT", (filteredText.contains("- LIFE INCOME WITH LUMP SUM REFUND AT DEATH")) ? "PASS" : "FAIL");

						                        //Page 3 STATIC TEXTs
												writeToCSV("POLICY NUMBER", var_PolicyNumber);
						                        writeToCSV("PAGE #: DATA TYPE", "PAGE 3: STATIC TEXT");
						                        writeToCSV("EXPECTED DATA", "'- LIFE INCOME WITH LUMP SUM REFUND AT DEATH");
						                        writeToCSV("ACTUAL DATA ON PDF", "'- LIFE INCOME WITH LUMP SUM REFUND AT DEATH");
						                        writeToCSV("RESULT", (filteredText.contains("- LIFE INCOME WITH LUMP SUM REFUND AT DEATH")) ? "PASS" : "FAIL");

						                        //Page 3 STATIC TEXTs
												writeToCSV("POLICY NUMBER", var_PolicyNumber);
						                        writeToCSV("PAGE #: DATA TYPE", "PAGE 3: STATIC TEXT");
						                        writeToCSV("EXPECTED DATA", "'- LIFE INCOME WITH LUMP SUM REFUND AT DEATH");
						                        writeToCSV("ACTUAL DATA ON PDF", "'- LIFE INCOME WITH LUMP SUM REFUND AT DEATH");
						                        writeToCSV("RESULT", (filteredText.contains("- LIFE INCOME WITH LUMP SUM REFUND AT DEATH")) ? "PASS" : "FAIL");

						                        //Page 3 STATIC TEXTs
												writeToCSV("POLICY NUMBER", var_PolicyNumber);
						                        writeToCSV("PAGE #: DATA TYPE", "PAGE 3: STATIC TEXT");
						                        writeToCSV("EXPECTED DATA", "GUARANTEED*");
						                        writeToCSV("ACTUAL DATA ON PDF", "GUARANTEED*");
						                        writeToCSV("RESULT", (filteredText.contains("GUARANTEED*")) ? "PASS" : "FAIL");

						                        //Page 3 STATIC TEXTs
												writeToCSV("POLICY NUMBER", var_PolicyNumber);
						                        writeToCSV("PAGE #: DATA TYPE", "PAGE 3: STATIC TEXT");
						                        writeToCSV("EXPECTED DATA", "AMOUNT AVAILABLE TO");
						                        writeToCSV("ACTUAL DATA ON PDF", "AMOUNT AVAILABLE TO");
						                        writeToCSV("RESULT", (filteredText.contains("AMOUNT AVAILABLE TO")) ? "PASS" : "FAIL");

						                        //Page 3 STATIC TEXTs
												writeToCSV("POLICY NUMBER", var_PolicyNumber);
						                        writeToCSV("PAGE #: DATA TYPE", "PAGE 3: STATIC TEXT");
						                        writeToCSV("EXPECTED DATA", "PROVIDE MONTHLY INCOME");
						                        writeToCSV("ACTUAL DATA ON PDF", "PROVIDE MONTHLY INCOME");
						                        writeToCSV("RESULT", (filteredText.contains("PROVIDE MONTHLY INCOME")) ? "PASS" : "FAIL");

						                        if (isJointInsureAvailable) {
						                            //Page 3 STATIC TEXTs
													writeToCSV("POLICY NUMBER", var_PolicyNumber);
						                            writeToCSV("PAGE #: DATA TYPE", "PAGE 3: STATIC TEXT");
						                            writeToCSV("EXPECTED DATA", "JOINT AND 50% SURVIVOR");
						                            writeToCSV("ACTUAL DATA ON PDF", "JOINT AND 50% SURVIVOR");
						                            writeToCSV("RESULT", (filteredText.contains("JOINT AND 50% SURVIVOR")) ? "PASS" : "FAIL");
						                        }

						                        //Page 3 STATIC TEXTs
												writeToCSV("POLICY NUMBER", var_PolicyNumber);
						                        writeToCSV("PAGE #: DATA TYPE", "PAGE 3: STATIC TEXT");
						                        writeToCSV("EXPECTED DATA", "MONTHLY LIFE INCOME WITH");
						                        writeToCSV("ACTUAL DATA ON PDF", "MONTHLY LIFE INCOME WITH");
						                        writeToCSV("RESULT", (filteredText.contains("MONTHLY LIFE INCOME WITH")) ? "PASS" : "FAIL");

						                        //Page 3 STATIC TEXTs
												writeToCSV("POLICY NUMBER", var_PolicyNumber);
						                        writeToCSV("PAGE #: DATA TYPE", "PAGE 3: STATIC TEXT : HEADERS: Annuity date & Age");
						                        writeToCSV("EXPECTED DATA", "ANNUITY DATE AGE");
						                        writeToCSV("ACTUAL DATA ON PDF", "ANNUITY DATE AGE");
						                        writeToCSV("RESULT", (filteredText.contains("ANNUITY DATE AGE")) ? "PASS" : "FAIL");

						                        //Page 3 STATIC TEXTs
												writeToCSV("POLICY NUMBER", var_PolicyNumber);
						                        writeToCSV("PAGE #: DATA TYPE", "PAGE 3: STATIC TEXT");
						                        writeToCSV("EXPECTED DATA", "TENTH POLICY YEAR");
						                        writeToCSV("ACTUAL DATA ON PDF", "TENTH POLICY YEAR");
						                        writeToCSV("RESULT", (filteredText.contains("TENTH POLICY YEAR")) ? "PASS" : "FAIL");

						                        //Page 3 STATIC TEXTs
												writeToCSV("POLICY NUMBER", var_PolicyNumber);
						                        writeToCSV("PAGE #: DATA TYPE", "PAGE 3: STATIC TEXT");
						                        writeToCSV("EXPECTED DATA", "YIELDS ON GROSS PREMIUM");
						                        writeToCSV("ACTUAL DATA ON PDF", "YIELDS ON GROSS PREMIUM");
						                        writeToCSV("RESULT", (filteredText.contains("YIELDS ON GROSS PREMIUM")) ? "PASS" : "FAIL");

						                        //Page 3 STATIC TEXTs
												writeToCSV("POLICY NUMBER", var_PolicyNumber);
						                        writeToCSV("PAGE #: DATA TYPE", "PAGE 3: STATIC TEXT");
						                        writeToCSV("EXPECTED DATA", "*THESE AMOUNTS ASSUME THAT:");
						                        writeToCSV("ACTUAL DATA ON PDF", "*THESE AMOUNTS ASSUME THAT:");
						                        writeToCSV("RESULT", (filteredText.contains("*THESE AMOUNTS ASSUME THAT:")) ? "PASS" : "FAIL");

						                        //Page 3 STATIC TEXTs
												writeToCSV("POLICY NUMBER", var_PolicyNumber);
						                        writeToCSV("PAGE #: DATA TYPE", "PAGE 3: STATIC TEXT");
						                        writeToCSV("EXPECTED DATA", "'- THE AMOUNT AVAILABLE TO PROVIDE MONTHLY INCOME IS BASED ON GUARANTEED VALUES A");
						                        writeToCSV("ACTUAL DATA ON PDF", "'- THE AMOUNT AVAILABLE TO PROVIDE MONTHLY INCOME IS BASED ON GUARANTEED VALUES A");
						                        writeToCSV("RESULT", (filteredText.contains("- THE AMOUNT AVAILABLE TO PROVIDE MONTHLY INCOME IS BASED ON GUARANTEED VALUES A")) ? "PASS" : "FAIL");

						                        //Page 3 STATIC TEXTs
												writeToCSV("POLICY NUMBER", var_PolicyNumber);
						                        writeToCSV("PAGE #: DATA TYPE", "PAGE 3: STATIC TEXT");
						                        writeToCSV("EXPECTED DATA", "S DESCRIBED ON PAGE 1.");
						                        writeToCSV("ACTUAL DATA ON PDF", "S DESCRIBED ON PAGE 1.");
						                        writeToCSV("RESULT", (filteredText.contains("S DESCRIBED ON PAGE 1.")) ? "PASS" : "FAIL");

						                        //Page 3 STATIC TEXTs
												writeToCSV("POLICY NUMBER", var_PolicyNumber);
						                        writeToCSV("PAGE #: DATA TYPE", "PAGE 3: STATIC TEXT");
						                        writeToCSV("EXPECTED DATA", "'- NO PARTIAL SURRENDERS ARE MADE.");
						                        writeToCSV("ACTUAL DATA ON PDF", "'- NO PARTIAL SURRENDERS ARE MADE.");
						                        writeToCSV("RESULT", (filteredText.contains("- NO PARTIAL SURRENDERS ARE MADE.")) ? "PASS" : "FAIL");

						                        //Page 3 STATIC TEXTs
												writeToCSV("POLICY NUMBER", var_PolicyNumber);
						                        writeToCSV("PAGE #: DATA TYPE", "PAGE 3: STATIC TEXT");
						                        writeToCSV("EXPECTED DATA", "'- THE GUARANTEED MONTHLY LIFE INCOME AMOUNTS ARE BASED ON GUARANTEED ANNUITY PUR");
						                        writeToCSV("ACTUAL DATA ON PDF", "'- THE GUARANTEED MONTHLY LIFE INCOME AMOUNTS ARE BASED ON GUARANTEED ANNUITY PUR");
						                        writeToCSV("RESULT", (filteredText.contains("- THE GUARANTEED MONTHLY LIFE INCOME AMOUNTS ARE BASED ON GUARANTEED ANNUITY PUR")) ? "PASS" : "FAIL");

						                        //Page 3 STATIC TEXTs
												writeToCSV("POLICY NUMBER", var_PolicyNumber);
						                        writeToCSV("PAGE #: DATA TYPE", "PAGE 3: STATIC TEXT");
						                        writeToCSV("EXPECTED DATA", "CHASE RATES SHOWN IN THE POLICY.");
						                        writeToCSV("ACTUAL DATA ON PDF", "CHASE RATES SHOWN IN THE POLICY.");
						                        writeToCSV("RESULT", (filteredText.contains("CHASE RATES SHOWN IN THE POLICY.")) ? "PASS" : "FAIL");

						                        //Page 3 STATIC TEXTs
												writeToCSV("POLICY NUMBER", var_PolicyNumber);
						                        writeToCSV("PAGE #: DATA TYPE", "PAGE 3: STATIC TEXT");
						                        writeToCSV("EXPECTED DATA", "AGENT/BROKER:");
						                        writeToCSV("ACTUAL DATA ON PDF", "AGENT/BROKER:");
						                        writeToCSV("RESULT", (filteredText.contains("AGENT/BROKER:")) ? "PASS" : "FAIL");

						                        //Page 3 STATIC TEXTs
												writeToCSV("POLICY NUMBER", var_PolicyNumber);
						                        writeToCSV("PAGE #: DATA TYPE", "PAGE 3: STATIC TEXT");
						                        writeToCSV("EXPECTED DATA", "ADDRESS:");
						                        writeToCSV("ACTUAL DATA ON PDF", "ADDRESS:");
						                        writeToCSV("RESULT", (filteredText.contains("ADDRESS:")) ? "PASS" : "FAIL");

						                        //Page 3 STATIC TEXTs
												writeToCSV("POLICY NUMBER", var_PolicyNumber);
						                        writeToCSV("PAGE #: DATA TYPE", "PAGE 3: STATIC TEXT");
						                        writeToCSV("EXPECTED DATA", "INSURER:");
						                        writeToCSV("ACTUAL DATA ON PDF", "INSURER:");
						                        writeToCSV("RESULT", (filteredText.contains("INSURER:")) ? "PASS" : "FAIL");
						                    } 
											else if (!var_PolicyNumber.equals(policynumberFromDB)) {
													writeToCSV("POLICY NUMBER", var_PolicyNumber);
														writeToCSV("PAGE #: DATA TYPE", "POLICY PRESENCE IN DATABASE");
								                        //	writeToCSV("ISSUE STATE", "issue State: " + issueState);
								                        writeToCSV("EXPECTED DATA", "POLICY NOT FOUND");
								                        writeToCSV("ACTUAL DATA ON PDF", var_PolicyNumber + " NOT FOUND");
								                        writeToCSV("RESULT", "SKIP");				 
										}
										 
						 else {
											      	writeToCSV("POLICY NUMBER", var_PolicyNumber);
												  writeToCSV("PAGE #: DATA TYPE", "STATIC TEXT - STATEMENT OF BENEFIT for NON-SBI State");
								                        //	writeToCSV("ISSUE STATE", "issue State: " + issueState);
								                        writeToCSV("EXPECTED DATA", "No STATEMENT OF BENEFIT text");
								                        writeToCSV("ACTUAL DATA ON PDF", "No STATEMENT OF BENEFIT  FOR ISSUE STATE " +  issueState);
								                        writeToCSV("RESULT", (filteredText.contains("STATEMENT OF BENEFIT")) ? "FAIL" : "PASS");
						 }
						 
						  con.close();
											}
				 
		 
		 ;} 
		 catch(Exception e) { PrintVal("Renderer Script Exception : "+e.toString()); } 
		 PrintVal("Executed Step = CALLRENDERERSCRIPT,COMPARINGSTATICTEXTONPDF"); 
        WAIT.$(2,"TGTYPESCREENREG");

		try { 
		 

						java.text.NumberFormat decimalFormat = java.text.NumberFormat.getInstance();
						decimalFormat.setMinimumFractionDigits(2);
						decimalFormat.setMaximumFractionDigits(2);
						decimalFormat.setGroupingUsed(true);

						java.sql.Connection con = null;

						// com.jayway.jsonpath.ReadContext CSVData = getJsonData("C:/GIAS_Automation/Agile FandG102/InputData/AgileFGSBI_CompPolicy.csv");
						//String policyNumberStr = var_PolicyNumber;  //getJsonData(CSVData, "$.records[0].PolicyNumber");

						// Load SQL Server JDBC driver and establish connection.
						String connectionUrl = "jdbc:sqlserver://CIS-GIASDBD1:243" + "3;" +
								"databaseName=FGLD2DT" + "A;"
								+
								"IntegratedSecurity = true";
						System.out.print("Connecting to SQL Server ... ");
						try {
							con = DriverManager.getConnection(connectionUrl);
						} catch (SQLException e) {
							e.printStackTrace();
						}
						System.out.println("Connected to database.");

						// DB Variables
						String primaryIssueAgeFromDB = "";
						String primaryInsuredNameFromDB = "";
						String primaryInsuredSexFromDB = "";
						String policy_number = "";
						String effectiveDate = "";
						String jointInsuredNameFromDB = "";
						String jointInsuredAgeFromDB = "";
						String jointInsuredSexFromDB = "";
						String cashWithApplication = "";
						String maturityDateFromDB = "";
						ArrayList<String> listDurationFromDB = new ArrayList<>();
						ArrayList<String> listAttainedAgeFromDB = new ArrayList<>();
						ArrayList<String> listProjectedAccountValueFromDB = new ArrayList<>();
						ArrayList<String> listProjectedAnnualPremiumsFromDB = new ArrayList<>();
						ArrayList<String> listSurrenderValueFromDB = new ArrayList<>();
						ArrayList<String> listDeathValueFromDB = new ArrayList<>();
						ArrayList<String> listGeneralFundSurrChargeFromDB = new ArrayList<>();
						ArrayList<String> insurerAddressLinesFromPDF = new ArrayList<>();
						ArrayList<String> agentAddressLinesFromPDF = new ArrayList<>();

						ArrayList<String> YIELD_PERCENTAGFromDB = new ArrayList<>();
						//PDF Variables
						boolean flag = false;
						ArrayList<String> filteredLines = new ArrayList<>();
						String filteredText = "";
						String basePath = "C:\\GIAS_Automation\\Agile FandG102\\InputData";
						// String var_PolicyNumber = policyNumberStr;
						String fileName = helper.WebTestUtility.getPDFFileForPolicy(basePath, var_PolicyNumber);
						ArrayList<String> pdfFileInText = helper.WebTestUtility.readLinesFromPDFFile(fileName);
						if (fileName.isEmpty()) {

							System.out.println("Print PDF File not found");

						} else {

							String companyLongDescFromDB = "";
							String companyAddressLine1FromDB = "";
							String companyAddressLine2FromDB = "";
							String companyCityCodeFromDB = "";
							String companyStateCodeFromDB = "";
							String companyZipCodeFromDB = "";

							String agentFirstNameFromDB = "";
							String agentLastNameFromDB = "";
							String agentAddressCityFromDB = "";
							String agentAddressLine1FromDB = "";
							String agentAddressLine2FromDB = "";
							String agentStateCodeFromDB = "";
							String agentZipCodeFromDB = "";
							String jurisdictionFromDB = "";

							String companyLongDescFromPDF = "";
							String companyAddressLine1FromPDF = "";
							String companyAddressLine2FromPDF = "";
							String companyCityCodeFromPDF = "";
							String companyStateCodeFromPDF = "";
							String companyZipCodeFromPDF = "";

							String agentFirstNameFromPDF = "";
							String agentLastNameFromPDF = "";
							String agentAddressCityFromPDF = "";
							String agentAddressLine1FromPDF = "";
							String agentAddressLine2FromPDF = "";
							String agentStateCodeFromPDF = "";
							String agentZipCodeFromPDF = "";

							String policyDateFromPDF = "";
							String policyNumberFromPDF = "";
							String primaryIssueAgeFromPDF = "";
							String primaryInsuredSexFromPDF = "";
							String nameOfAnnuitantFromPDF = "";

							String jointInsuredNameFromPDF = "";
							String jointIssueAgeFromPDF = "";
							String jointInsuredSexFromPDF = "";
							String MVA_INDICATOR = "";
							String VESTING_TABLE = "";
							String premiumFromPDF = "";
							String minimumPremiumValueFromPDF = "";
							String maximumPremiumValueFromPDF = "";
							int lineNUmber = 0;
							int firstYearRecordLineNumber = 0;
							int tenthYearRecordLineNumber = 0;
							int lastRecordLineNumber = 0;
							String maxAllowedPremiumFromDB = "";
							String minAllowedPremiumFromDB = "";
							ArrayList<String> listIssueAgeFromPDF = new ArrayList<>();
							ArrayList<String> listDurationFromPDF = new ArrayList<>();
							ArrayList<String> listGuaranteedAccountValueFromPDF = new ArrayList<>();
							ArrayList<String> listProjectedAnnualPremiumsFromPDF = new ArrayList<>();
							ArrayList<String> listSurrenderValueFromPDF = new ArrayList<>();
							ArrayList<String> listDeathValueFromPDF = new ArrayList<>();

							int oldestIssueAge;

							ArrayList<String> listDurationFromDB10Plus = new ArrayList<>();
							ArrayList<String> listAttainedAgeFromDB10Plus = new ArrayList<>();
							ArrayList<Double> listProjectedAccountValueFromDB10Plus = new ArrayList<>();
							ArrayList<Double> listProjectedAnnualPremiumsFromDB10Plus = new ArrayList<>();
							ArrayList<Double> listSurrenderValueFromDB10Plus = new ArrayList<>();
							ArrayList<Double> listDeathValueFromDB10Plus = new ArrayList<>();

							ArrayList<String> listIssueAgeFromPDF10Plus = new ArrayList<>();
							ArrayList<String> listDurationFromPDF10Plus = new ArrayList<>();
							ArrayList<Double> listGuaranteedAccountValueFromPDF10Plus = new ArrayList<>();
							ArrayList<Double> listProjectedAnnualPremiumsFromPDF10Plus = new ArrayList<>();
							ArrayList<Double> listSurrenderValueFromPDF10Plus = new ArrayList<>();
							ArrayList<Double> listDeathValueFromPDF10Plus = new ArrayList<>();

							String sql = "SELECT A.ISSUE_AGE,A.POLICY_NUMBER,A.SEX_CODE,A.PRIMARY_INSURED_NAME," +
									"A.VESTING_TABLE,A.MVA_INDICATOR,A.SUPPLEMENTAL_BNFT_TYPE,A.JOINT_INSURED_NAME,A.JNT_INSURED_SEX," +
									"A.JNT_INSURED_AGE,A.JOINT_ISSUE_AGE,A.MAT_DATE,A.MIN_ALLOW_INIT_PREMIUM,A.MAX_ALLOW_INIT_PREMIUM, B.JURISDICTION," +
									"B.CASH_WITH_APPLICATION,B.SBI_FORMATTED_EFFE_DATE " +
									"FROM IIOS.POLICYPAGEBNFTVIEW A,IIOS.POLICYPAGECONTRACTVIEW B " +
									"WHERE A.POLICY_NUMBER = B.POLICY_NUMBER AND " +
									"A.POLICY_NUMBER='" + var_PolicyNumber + "';";

							Statement statement = con.createStatement();
							ResultSet rs = null;
							try {
								rs = statement.executeQuery(sql);
							} catch (SQLException e) {
								e.printStackTrace();
							}
							System.out.println(rs);
							while (rs.next()) {

								primaryIssueAgeFromDB = rs.getString("issue_Age").trim();
								System.out.println("ISSUE AGE FROM DATABASE IS = " + primaryIssueAgeFromDB);

								policy_number = rs.getString("policy_number").trim();

								primaryInsuredSexFromDB = rs.getString("sex_code").trim();

								primaryInsuredNameFromDB = rs.getString("primary_insured_name").trim();

								effectiveDate = rs.getString("SBI_FORMATTED_EFFE_DATE").trim();

								jointInsuredNameFromDB = rs.getString("JOINT_INSURED_NAME").trim();

								jointInsuredAgeFromDB = rs.getString("JNT_INSURED_AGE").trim();

								jointInsuredSexFromDB = rs.getString("JNT_INSURED_SEX").trim();

								maturityDateFromDB = rs.getString("MAT_DATE");
								System.out.println("Maturity Date is " + maturityDateFromDB);

								MVA_INDICATOR = rs.getString("MVA_INDICATOR");
								System.out.println("MVA_INDICATOR = " + MVA_INDICATOR);

								VESTING_TABLE = rs.getString("VESTING_TABLE");
								System.out.println("VESTING_TABLE is " + VESTING_TABLE);

								cashWithApplication = rs.getString("CASH_WITH_APPLICATION");
								jurisdictionFromDB = rs.getString("JURISDICTION");

								String maxAllowedPremiumStr = rs.getString("MAX_ALLOW_INIT_PREMIUM");
								if (maxAllowedPremiumStr != null) {
									maxAllowedPremiumStr = decimalFormat.format(Double.parseDouble(maxAllowedPremiumStr));
									if (!maxAllowedPremiumStr.contains(".")) {
										maxAllowedPremiumStr = maxAllowedPremiumStr + ".00";
									}
								}
								maxAllowedPremiumFromDB = maxAllowedPremiumStr;

								String minAllowedPremiumStr = rs.getString("min_allow_init_premium");
								if (minAllowedPremiumStr != null) {
									minAllowedPremiumStr = decimalFormat.format(Double.parseDouble(minAllowedPremiumStr));
									if (!minAllowedPremiumStr.contains(".")) {
										minAllowedPremiumStr = minAllowedPremiumStr + ".00";
									}
								}
								minAllowedPremiumFromDB = minAllowedPremiumStr;

							}
							statement.close();
							if (jurisdictionFromDB.equals("MD") || jurisdictionFromDB.equals("NV") || jurisdictionFromDB.equals("GA") || jurisdictionFromDB.equals("WI") || jurisdictionFromDB.equals("WA")) {
								String sql2 = "SELECT C.DURATION, C.ATTAINED_AGE,C.PROJECTED_ACCT_VALU_G,C.PROJECTED_DEATH_BNFT_G," +
										"  C.PROJECTED_ANN_PREM,C.SURRENDER_AMOUNT, C.GENERAL_FUND_SURR_CHRG FROM " +
										"  IIOS.POLICYPAGEVALUESVIEW C " +
										"  WHERE C.POLICY_NUMBER='" + var_PolicyNumber + "' and c.DURATION between 1 and 10;";
								Statement statement2 = con.createStatement();
								ResultSet rs2 = null;
								try {
									rs2 = statement2.executeQuery(sql2);
								} catch (SQLException e) {
									e.printStackTrace();
								}
								System.out.println(rs2);
								while (rs2.next()) {

									String duration = rs2.getString("DURATION");
									listDurationFromDB.add(duration);

									String attainedAge = rs2.getString("ATTAINED_AGE");
									listAttainedAgeFromDB.add(attainedAge);

									String projectedAnnualPremium = rs2.getString("PROJECTED_ANN_PREM");
									if (projectedAnnualPremium != null) {
										projectedAnnualPremium = decimalFormat.format(Double.parseDouble(projectedAnnualPremium));
										if (!projectedAnnualPremium.contains(".")) {
											projectedAnnualPremium = projectedAnnualPremium + ".00";
										}
									}
									listProjectedAnnualPremiumsFromDB.add(projectedAnnualPremium);

									String projectedAccountValue = rs2.getString("PROJECTED_ACCT_VALU_G");
									if (projectedAccountValue != null) {
										projectedAccountValue = decimalFormat.format(Double.parseDouble(projectedAccountValue));
										if (!projectedAccountValue.contains(".")) {
											projectedAccountValue = projectedAccountValue + ".00";
										}
									}
									listProjectedAccountValueFromDB.add(projectedAccountValue);

									String surrenderValue = rs2.getString("SURRENDER_AMOUNT");
									if (surrenderValue != null) {
										surrenderValue = decimalFormat.format(Double.parseDouble(surrenderValue));
										if (!surrenderValue.contains(".")) {
											surrenderValue = surrenderValue + ".00";
										}
									}
									listSurrenderValueFromDB.add(surrenderValue);

									String deathValue = rs2.getString("PROJECTED_DEATH_BNFT_G");
									if (deathValue != null) {
										deathValue = decimalFormat.format(Double.parseDouble(deathValue));
										if (!deathValue.contains(".")) {
											deathValue = deathValue + ".00";
										}
									}
									listDeathValueFromDB.add(deathValue);

									String generalFundSurrCharge = rs2.getString("GENERAL_FUND_SURR_CHRG");
									if (generalFundSurrCharge != null) {
										generalFundSurrCharge = decimalFormat.format(Double.parseDouble(generalFundSurrCharge));
										if (!generalFundSurrCharge.contains(".")) {
											generalFundSurrCharge = generalFundSurrCharge + ".00";
										}
									}
									listGeneralFundSurrChargeFromDB.add(generalFundSurrCharge);

								}
								statement2.close();

								String sql4 = "Select DURATION,ATTAINED_AGE,PROJECTED_ANN_PREM,PROJECTED_DEATH_BNFT_G,SURRENDER_AMOUNT, " +
										"PROJECTED_ACCT_VALU_G From " +
										"(Select Row_Number() Over (Order By PROJECTED_ACCT_VALU_G) As RowNum, * From " +
										"iios.PolicyPageValuesView where policy_number='" + var_PolicyNumber + "') t2 " +
										"Where ATTAINED_AGE in (60,65,100) or DURATION in (20);";

								Statement statement4 = con.createStatement();
								ResultSet rs4 = null;
								try {
									rs4 = statement4.executeQuery(sql4);
								} catch (SQLException e) {
									e.printStackTrace();
								}
								System.out.println(rs4);
								while (rs4.next()) {

									String duration = rs4.getString("DURATION");
									listDurationFromDB10Plus.add(duration);

									String attainedAge = rs4.getString("ATTAINED_AGE");
									listAttainedAgeFromDB10Plus.add(attainedAge);

									String projectedAnnualPremium = rs4.getString("PROJECTED_ANN_PREM");
									listProjectedAnnualPremiumsFromDB10Plus.add(Double.parseDouble(projectedAnnualPremium));

									String projectedAccountValue = rs4.getString("PROJECTED_ACCT_VALU_G");
									listProjectedAccountValueFromDB10Plus.add(Double.parseDouble(projectedAccountValue));

									String surrenderValue = rs4.getString("SURRENDER_AMOUNT");
									listSurrenderValueFromDB10Plus.add(Double.parseDouble(surrenderValue));

									String deathValue = rs4.getString("PROJECTED_DEATH_BNFT_G");
									listDeathValueFromDB10Plus.add(Double.parseDouble(deathValue));

								}

								statement4.close();

								String sql8 = "Select YIELD_PERCENTAGE From \n" +
										"(Select Row_Number() Over (Order By projected_Acct_Valu_G) As RowNum, * From iios.PolicyPageValuesView where policy_number='"+var_PolicyNumber+"') t2\n" +
										"Where ATTAINED_AGE in (100) or DURATION in (10);\n";

								Statement statement8 = con.createStatement();
								ResultSet rs8 = null;
								try {
									rs8 = statement8.executeQuery(sql8);
								} catch (SQLException e) {
									e.printStackTrace();
								}
								System.out.println(rs8);

								while (rs8.next()) {


									String userDefinedField15S22Value = rs8.getString("YIELD_PERCENTAGE");
									if (userDefinedField15S22Value != null) {
										userDefinedField15S22Value = decimalFormat.format(Double.parseDouble(userDefinedField15S22Value));
										if (!userDefinedField15S22Value.contains(".")) {
											userDefinedField15S22Value = userDefinedField15S22Value + ".00";
										}
									}
									YIELD_PERCENTAGFromDB.add(userDefinedField15S22Value);
		//				userDefinedField15S22FromDB
								}
								statement8.close();

								boolean isPage2Exists = false;

								for (String line : pdfFileInText) {

									if (line.equals("STATEMENT OF BENEFIT INFORMATION – CONTRACT SUMMARY")) {
										flag = true;
									}

									if (flag) {

										filteredText = filteredText + "\n" + line;

										filteredLines.add(line);

										LOGGER.info(line);

										if (line.contains("ANNUITANT(S) NAME(S)") && line.contains("ISSUE AGE(S)")) {

											String nextLine = pdfFileInText.get(lineNUmber + 1);

											String[] insuredParts = nextLine.trim().split(" ");

											if (insuredParts.length == 4) {

												primaryIssueAgeFromPDF = insuredParts[2];
												System.out.println("PRIMARY ISSUE AGE IS:-" + primaryIssueAgeFromPDF);

												primaryInsuredSexFromPDF = insuredParts[3];
												System.out.println("SEX IS:-" + primaryInsuredSexFromPDF);

												nameOfAnnuitantFromPDF = insuredParts[0] + " " + insuredParts[1];

												oldestIssueAge = Integer.parseInt(primaryIssueAgeFromPDF);

												System.out.println("nameOfAnnuitantFromPDF IS:-" + nameOfAnnuitantFromPDF);

												writeToCSV("POLICY NUMBER", var_PolicyNumber);
												writeToCSV("PAGE #: DATA TYPE", "PAGE 1: " + "PRIMARY ISSUE AGE");
												writeToCSV("EXPECTED DATA", primaryIssueAgeFromDB);
												writeToCSV("ACTUAL DATA ON PDF", primaryIssueAgeFromPDF);
												writeToCSV("RESULT", (primaryIssueAgeFromDB.equals(primaryIssueAgeFromPDF)) ? "PASS" : "FAIL");

												writeToCSV("POLICY NUMBER", var_PolicyNumber);
												writeToCSV("PAGE #: DATA TYPE", "PAGE 1: " + "PRIMARY INSURED NAME");
												writeToCSV("EXPECTED DATA", primaryInsuredNameFromDB.trim());
												writeToCSV("ACTUAL DATA ON PDF", nameOfAnnuitantFromPDF);
												writeToCSV("RESULT", (primaryInsuredNameFromDB.trim().equals(nameOfAnnuitantFromPDF)) ? "PASS" : "FAIL");

												writeToCSV("POLICY NUMBER", var_PolicyNumber);
												writeToCSV("PAGE #: DATA TYPE", "PAGE 1: " + "PRIMARY INSURED SEX");
												writeToCSV("EXPECTED DATA", primaryInsuredSexFromDB);
												writeToCSV("ACTUAL DATA ON PDF", primaryInsuredSexFromPDF);
												writeToCSV("RESULT", (primaryInsuredSexFromDB.equals(primaryInsuredSexFromPDF)) ? "PASS" : "FAIL");

											} else if (insuredParts.length == 2) {
												// For double entry of ANNUITANT

												nameOfAnnuitantFromPDF = nextLine.trim();
												jointInsuredNameFromPDF = pdfFileInText.get(lineNUmber + 2).trim();

												primaryIssueAgeFromPDF = pdfFileInText.get(lineNUmber + 3).trim();
												jointIssueAgeFromPDF = pdfFileInText.get(lineNUmber + 4).trim();

												primaryInsuredSexFromPDF = pdfFileInText.get(lineNUmber + 5).trim();
												jointInsuredSexFromPDF = pdfFileInText.get(lineNUmber + 6).trim();

												if (Integer.parseInt(primaryIssueAgeFromPDF) > Integer.parseInt(jointIssueAgeFromPDF)) {
													oldestIssueAge = Integer.parseInt(primaryIssueAgeFromPDF);
												} else {
													oldestIssueAge = Integer.parseInt(jointIssueAgeFromPDF);
												}
												writeToCSV("POLICY NUMBER", var_PolicyNumber);
												writeToCSV("PAGE #: DATA TYPE", "PAGE 1: " + "PRIMARY ISSUE AGE");
												writeToCSV("EXPECTED DATA", primaryIssueAgeFromDB);
												writeToCSV("ACTUAL DATA ON PDF", primaryIssueAgeFromPDF);
												writeToCSV("RESULT", (primaryIssueAgeFromDB.equals(primaryIssueAgeFromPDF)) ? "PASS" : "FAIL");

												//                        Assert.assertEquals(jointIssueAgeFromPDF,jointInsuredAgeFromDB);
												writeToCSV("POLICY NUMBER", var_PolicyNumber);
												writeToCSV("PAGE #: DATA TYPE", "PAGE 1: " + "JOINT INSURED AGE");
												writeToCSV("EXPECTED DATA", jointInsuredAgeFromDB);
												writeToCSV("ACTUAL DATA ON PDF", jointIssueAgeFromPDF);
												writeToCSV("RESULT", (jointInsuredAgeFromDB.equals(jointIssueAgeFromPDF)) ? "PASS" : "FAIL");

												writeToCSV("POLICY NUMBER", var_PolicyNumber);
												writeToCSV("PAGE #: DATA TYPE", "PAGE 1: " + "PRIMARY INSURED NAME");
												writeToCSV("EXPECTED DATA", primaryInsuredNameFromDB.trim());
												writeToCSV("ACTUAL DATA ON PDF", nameOfAnnuitantFromPDF);
												writeToCSV("RESULT", (primaryInsuredNameFromDB.trim().equals(nameOfAnnuitantFromPDF)) ? "PASS" : "FAIL");

												writeToCSV("POLICY NUMBER", var_PolicyNumber);
												writeToCSV("PAGE #: DATA TYPE", "PAGE 1: " + "JOINT INSURED NAME");
												writeToCSV("EXPECTED DATA", jointInsuredNameFromDB.trim());
												writeToCSV("ACTUAL DATA ON PDF", jointInsuredNameFromPDF);
												writeToCSV("RESULT", (jointInsuredNameFromDB.trim().equals(jointInsuredNameFromPDF)) ? "PASS" : "FAIL");

												writeToCSV("POLICY NUMBER", var_PolicyNumber);
												writeToCSV("PAGE #: DATA TYPE", "PAGE 1: " + "PRIMARY INSURED SEX");
												writeToCSV("EXPECTED DATA", primaryInsuredSexFromDB);
												writeToCSV("ACTUAL DATA ON PDF", primaryInsuredSexFromPDF);
												writeToCSV("RESULT", (primaryInsuredSexFromDB.equals(primaryInsuredSexFromPDF)) ? "PASS" : "FAIL");

												//     Assert.assertEquals(jointInsuredSexFromPDF, jointInsuredSexFromDB);
												writeToCSV("POLICY NUMBER", var_PolicyNumber);
												writeToCSV("PAGE #: DATA TYPE", "PAGE 1: " + "JOINT INSURED SEX");
												writeToCSV("EXPECTED DATA", jointInsuredSexFromDB);
												writeToCSV("ACTUAL DATA ON PDF", jointInsuredSexFromPDF);
												writeToCSV("RESULT", (jointInsuredSexFromDB.equals(jointInsuredSexFromPDF)) ? "PASS" : "FAIL");

											}

										}

										if (line.contains("ISSUE") && line.contains("$")) {

											String previouseLine = pdfFileInText.get(lineNUmber - 1);

											if (previouseLine.trim().equals("VALUE")) {
												firstYearRecordLineNumber = lineNUmber + 1;
												tenthYearRecordLineNumber = firstYearRecordLineNumber + 9;
												System.out.println("Selected lines are...");

												for (int i = firstYearRecordLineNumber; i <= tenthYearRecordLineNumber; i++) {

													String selectedLine = pdfFileInText.get(i);
													String[] parts = selectedLine.split(" ");
													if (parts.length > 2) {

														String durationFromPDF = parts[0].trim();
														System.out.println("DURATION IS:-" + durationFromPDF);
														listDurationFromPDF.add(durationFromPDF);

														String yearFromPdf = parts[1].trim();
														System.out.println("Issue Year IS:-" + yearFromPdf);
														listIssueAgeFromPDF.add(yearFromPdf);

														String projectedAnnualPremium = parts[3];
														System.out.println("Projected Annual Premium Value From PDF IS:-" + projectedAnnualPremium);
														listProjectedAnnualPremiumsFromPDF.add(projectedAnnualPremium);

														String guaranteedAccountValue = parts[5];
														System.out.println("Guaranteed Account Value From PDF IS:-" + guaranteedAccountValue);
														listGuaranteedAccountValueFromPDF.add(guaranteedAccountValue);

														String guaranteedSurrenderValue = parts[7];
														System.out.println("Guaranteed Surrender Value From PDF IS:-" + guaranteedSurrenderValue);
														listSurrenderValueFromPDF.add(guaranteedSurrenderValue);

														String deathValue = parts[9];
														System.out.println("Guaranteed Death Value From PDF IS:-" + deathValue);
														listDeathValueFromPDF.add(deathValue);

													}
													System.out.println();
												}

												//     Assert.assertEquals(primaryIssueAgeFromPDF, primaryIssueAgeFromDB);

												for (int i = 0; i < listDurationFromPDF.size(); i++) {

													//     Assert.assertEquals(listDurationFromPDF.get(i), listDurationFromDB.get(i));
													writeToCSV("POLICY NUMBER", var_PolicyNumber);
													writeToCSV("PAGE #: DATA TYPE", "PAGE 1: " + "DURATION " + (i + 1));
													writeToCSV("EXPECTED DATA", listDurationFromDB.get(i));
													writeToCSV("ACTUAL DATA ON PDF", listDurationFromPDF.get(i));
													writeToCSV("RESULT", listDurationFromPDF.get(i).equals(listDurationFromDB.get(i)) ? "PASS" : "FAIL");

												}

												for (int i = 0; i < listIssueAgeFromPDF.size(); i++) {

													//     Assert.assertEquals(listIssueAgeFromPDF.get(i), listAttainedAgeFromDB.get(i));
													writeToCSV("POLICY NUMBER", var_PolicyNumber);
													writeToCSV("PAGE #: DATA TYPE", "PAGE 1: " + "ISSUE AGE " + (i + 1) + " YEAR");
													writeToCSV("EXPECTED DATA", listAttainedAgeFromDB.get(i));
													writeToCSV("ACTUAL DATA ON PDF", listIssueAgeFromPDF.get(i));
													writeToCSV("RESULT", listIssueAgeFromPDF.get(i).equals(listAttainedAgeFromDB.get(i)) ? "PASS" : "FAIL");

												}

												for (int i = 0; i < listProjectedAnnualPremiumsFromPDF.size(); i++) {

													//     Assert.assertEquals(listProjectedAnnualPremiumsFromPDF.get(i), listProjectedAnnualPremiumsFromDB.get(i));
													writeToCSV("POLICY NUMBER", var_PolicyNumber);
													writeToCSV("PAGE #: DATA TYPE", "PAGE 1: " + "PROJECTED ANNUAL PREMIUM " + (i + 1) + " YEAR");
													writeToCSV("EXPECTED DATA", "$" + listProjectedAnnualPremiumsFromDB.get(i));
													writeToCSV("ACTUAL DATA ON PDF", "$" + listProjectedAnnualPremiumsFromPDF.get(i));
													writeToCSV("RESULT", listProjectedAnnualPremiumsFromPDF.get(i).equals(listProjectedAnnualPremiumsFromDB.get(i)) ? "PASS" : "FAIL");

												}

												for (int i = 0; i < listGuaranteedAccountValueFromPDF.size(); i++) {

													//     Assert.assertEquals(listGuaranteedAccountValueFromPDF.get(i), listProjectedAccountValueFromDB.get(i));
													writeToCSV("POLICY NUMBER", var_PolicyNumber);
													writeToCSV("PAGE #: DATA TYPE", "PAGE 1: " + "PROJECTED ACCOUNT VALUE " + (i + 1) + " YEAR");
													writeToCSV("EXPECTED DATA", "$" + listProjectedAccountValueFromDB.get(i));
													writeToCSV("ACTUAL DATA ON PDF", "$" + listGuaranteedAccountValueFromPDF.get(i));
													writeToCSV("RESULT", listGuaranteedAccountValueFromPDF.get(i).equals(listProjectedAccountValueFromDB.get(i)) ? "PASS" : "FAIL");

												}

												for (int i = 0; i < listDeathValueFromPDF.size(); i++) {

													//            Assert.assertEquals(listDeathValueFromPDF.get(i), listDeathValueFromDB.get(i));
													writeToCSV("POLICY NUMBER", var_PolicyNumber);
													writeToCSV("PAGE #: DATA TYPE", "PAGE 1: " + "GUARANTEED DEATH VALUE " + (i + 1) + " YEAR");
													writeToCSV("EXPECTED DATA", "$" + listDeathValueFromDB.get(i));
													writeToCSV("ACTUAL DATA ON PDF", "$" + listDeathValueFromPDF.get(i));
													writeToCSV("RESULT", listDeathValueFromPDF.get(i).equals(listDeathValueFromDB.get(i)) ? "PASS" : "FAIL");

												}

												for (int i = 0; i < listSurrenderValueFromPDF.size(); i++) {

													//            Assert.assertEquals(listSurrenderValueFromPDF.get(i), listSurrenderValueFromDB.get(i));
													writeToCSV("POLICY NUMBER", var_PolicyNumber);
													writeToCSV("PAGE #: DATA TYPE", "PAGE 1: " + "GUARANTEED SURRENDER VALUE " + (i + 1) + " YEAR");
													writeToCSV("EXPECTED DATA", "$" + listSurrenderValueFromDB.get(i));
													writeToCSV("ACTUAL DATA ON PDF", "$" + listSurrenderValueFromPDF.get(i));
													writeToCSV("RESULT", listSurrenderValueFromPDF.get(i).equals(listSurrenderValueFromDB.get(i)) ? "PASS" : "FAIL");

												}

											}

										}

										if (line.contains("THIS POLICY MAY RESULT IN A LOSS IF SURRENDERED DURING THE PERIOD WHEN SURRENDER CHARGES APPLY")) {
											lastRecordLineNumber = lineNUmber - 1;
											if (primaryIssueAgeFromPDF != null && primaryIssueAgeFromPDF.length() > 0 && Integer.parseInt(primaryIssueAgeFromPDF) < 65) {

												for (int i = tenthYearRecordLineNumber + 1; i <= lastRecordLineNumber; i++) {

													String selectedLine = pdfFileInText.get(i);

													String[] parts = selectedLine.split(" ");
													if (parts.length > 2) {

														String durationFromPDF = parts[0].trim();
														System.out.println("DURATION IS:-" + durationFromPDF);
														listDurationFromPDF10Plus.add(durationFromPDF);

														String issueAgeFromPdf = parts[1].trim();
														System.out.println("Issue Year IS:-" + issueAgeFromPdf);
														listIssueAgeFromPDF10Plus.add(issueAgeFromPdf);

														String projectedAnnualPremium = parts[3];
														System.out.println("Projected Annual Premium Value From PDF IS:-" + projectedAnnualPremium);
														double projectAnnualPremiumFromPDF = Double.parseDouble(projectedAnnualPremium.replace(",", ""));
														listProjectedAnnualPremiumsFromPDF10Plus.add(projectAnnualPremiumFromPDF);

														String guaranteedAccountValue = parts[5];
														System.out.println("Guaranteed Account Value From PDF IS:-" + guaranteedAccountValue);
														double guaranteedAccountValueFromPDF = Double.parseDouble(guaranteedAccountValue.replace(",", ""));
														listGuaranteedAccountValueFromPDF10Plus.add(guaranteedAccountValueFromPDF);

														String guaranteedSurrenderValue = parts[7];
														System.out.println("Guaranteed Surrender Value From PDF IS:-" + guaranteedSurrenderValue);
														double guaranteedSurrenderValueFromPDF = Double.parseDouble(guaranteedSurrenderValue.replace(",", ""));
														listSurrenderValueFromPDF10Plus.add(guaranteedSurrenderValueFromPDF);

														String deathValue = parts[9];
														System.out.println("Guaranteed Death Value From PDF IS:-" + deathValue);
														double deathValueFromPDF = Double.parseDouble(deathValue.replace(",", ""));
														listDeathValueFromPDF10Plus.add(deathValueFromPDF);

														int index = listAttainedAgeFromDB10Plus.indexOf(issueAgeFromPdf);

														if (index >= 0) {

															//     Assert.assertEquals(issueAgeFromPdf, listAttainedAgeFromDB10Plus.get(index));
						                                         /*   writeToCSV("POLICY NUMBER", var_PolicyNumber);
																	writeToCSV("PAGE #: DATA TYPE", "PAGE 1: " + "ISSUE AGE " + durationFromPDF);
						                                            writeToCSV("EXPECTED DATA", listAttainedAgeFromDB10Plus.get(index));
						                                            writeToCSV("ACTUAL DATA ON PDF", issueAgeFromPdf);
						                                            writeToCSV("RESULT", issueAgeFromPdf.equals(listAttainedAgeFromDB10Plus.get(index)) ? "PASS" : "FAIL");

						                                            //     Assert.assertEquals(projectAnnualPremiumFromPDF, listProjectedAnnualPremiumsFromDB10Plus.get(index));
																	writeToCSV("POLICY NUMBER", var_PolicyNumber);
						                                            writeToCSV("PAGE #: DATA TYPE", "PAGE 1: " + "PROJECTED ANNUAL PREMIUM " + durationFromPDF);
						                                            writeToCSV("EXPECTED DATA", "" + listProjectedAnnualPremiumsFromDB10Plus.get(index));
						                                            writeToCSV("ACTUAL DATA ON PDF", "" + projectAnnualPremiumFromPDF);
						                                            writeToCSV("RESULT", projectAnnualPremiumFromPDF == listProjectedAnnualPremiumsFromDB10Plus.get(index) ? "PASS" : "FAIL");

						                                            //	Assert.assertEquals(guaranteedAccountValueFromPDF, listProjectedAccountValueFromDB10Plus.get(index));
						                                            writeToCSV("POLICY NUMBER", var_PolicyNumber);
																	writeToCSV("PAGE #: DATA TYPE", "PAGE 1: " + "PROJECTED ACCOUNT VALUE " + durationFromPDF);
						                                            writeToCSV("EXPECTED DATA", "" + listProjectedAccountValueFromDB10Plus.get(index));
						                                            writeToCSV("ACTUAL DATA ON PDF", "" + guaranteedAccountValueFromPDF);
						                                            writeToCSV("RESULT", guaranteedAccountValueFromPDF == listProjectedAccountValueFromDB10Plus.get(index) ? "PASS" : "FAIL");

						                                            //     Assert.assertEquals(guaranteedSurrenderValueFromPDF, listSurrenderValueFromDB10Plus.get(index));
						                                            writeToCSV("POLICY NUMBER", var_PolicyNumber);
																	writeToCSV("PAGE #: DATA TYPE", "PAGE 1: " + "GUARANTEED SURRENDER VALUE " + durationFromPDF);
						                                            writeToCSV("EXPECTED DATA", "" + listSurrenderValueFromDB10Plus.get(index));
						                                            writeToCSV("ACTUAL DATA ON PDF", "" + guaranteedSurrenderValueFromPDF);
						                                            writeToCSV("RESULT", guaranteedSurrenderValueFromPDF == listSurrenderValueFromDB10Plus.get(index) ? "PASS" : "FAIL");

						                                            // Assert.assertEquals(deathValueFromPDF, listDeathValueFromDB10Plus.get(index));
																	writeToCSV("POLICY NUMBER", var_PolicyNumber);
																   writeToCSV("PAGE #: DATA TYPE", "PAGE 1: " + "GUARANTEED DEATH VALUE " + durationFromPDF);
						                                            writeToCSV("EXPECTED DATA", "$" + listDeathValueFromDB10Plus.get(index));
						                                            writeToCSV("ACTUAL DATA ON PDF", "$" + deathValueFromPDF);
						                                            writeToCSV("RESULT", deathValueFromPDF == listDeathValueFromDB10Plus.get(index) ? "PASS" : "FAIL"); */
															//     Assert.assertEquals(issueAgeFromPdf, listAttainedAgeFromDB10Plus.get(index));
															writeToCSV("POLICY NUMBER", var_PolicyNumber);
															writeToCSV("PAGE #: DATA TYPE", "PAGE 1: " + "ISSUE AGE " + durationFromPDF);
															writeToCSV("EXPECTED DATA", listAttainedAgeFromDB10Plus.get(index));
															writeToCSV("ACTUAL DATA ON PDF", issueAgeFromPdf);
															writeToCSV("RESULT", issueAgeFromPdf.equals(listAttainedAgeFromDB10Plus.get(index)) ? "PASS" : "FAIL");

															//     Assert.assertEquals(projectAnnualPremiumFromPDF, listProjectedAnnualPremiumsFromDB10Plus.get(index));
															writeToCSV("POLICY NUMBER", var_PolicyNumber);
															writeToCSV("PAGE #: DATA TYPE", "PAGE 1: " + "PROJECTED ANNUAL PREMIUM " + durationFromPDF);
															writeToCSV("EXPECTED DATA", "$" + listProjectedAnnualPremiumsFromDB10Plus.get(index));
															writeToCSV("ACTUAL DATA ON PDF", "$" + projectAnnualPremiumFromPDF);
															writeToCSV("RESULT", projectAnnualPremiumFromPDF == listProjectedAnnualPremiumsFromDB10Plus.get(index) ? "PASS" : "FAIL");

															//	Assert.assertEquals(guaranteedAccountValueFromPDF, listProjectedAccountValueFromDB10Plus.get(index));
															writeToCSV("POLICY NUMBER", var_PolicyNumber);
															writeToCSV("PAGE #: DATA TYPE", "PAGE 1: " + "PROJECTED ACCOUNT VALUE " + durationFromPDF);
															writeToCSV("EXPECTED DATA", "$" + listProjectedAccountValueFromDB10Plus.get(index));
															writeToCSV("ACTUAL DATA ON PDF", "$" + guaranteedAccountValueFromPDF);
															writeToCSV("RESULT", guaranteedAccountValueFromPDF == listProjectedAccountValueFromDB10Plus.get(index) ? "PASS" : "FAIL");

															//     Assert.assertEquals(guaranteedSurrenderValueFromPDF, listSurrenderValueFromDB10Plus.get(index));
															writeToCSV("POLICY NUMBER", var_PolicyNumber);
															writeToCSV("PAGE #: DATA TYPE", "PAGE 1: " + "GUARANTEED SURRENDER VALUE " + durationFromPDF);
															writeToCSV("EXPECTED DATA", "$" + listSurrenderValueFromDB10Plus.get(index));
															writeToCSV("ACTUAL DATA ON PDF", "$" + guaranteedSurrenderValueFromPDF);
															writeToCSV("RESULT", guaranteedSurrenderValueFromPDF == listSurrenderValueFromDB10Plus.get(index) ? "PASS" : "FAIL");

															// Assert.assertEquals(deathValueFromPDF, listDeathValueFromDB10Plus.get(index));
															writeToCSV("POLICY NUMBER", var_PolicyNumber);
															writeToCSV("PAGE #: DATA TYPE", "PAGE 1: " + "GUARANTEED DEATH VALUE " + durationFromPDF);
															writeToCSV("EXPECTED DATA", "$" + listDeathValueFromDB10Plus.get(index));
															writeToCSV("ACTUAL DATA ON PDF", "$" + deathValueFromPDF);
															writeToCSV("RESULT", deathValueFromPDF == listDeathValueFromDB10Plus.get(index) ? "PASS" : "FAIL");
														}
													}
												}

											}

											String maturityDateFromPDF = listDurationFromPDF10Plus.get(listDurationFromPDF10Plus.size() - 1);
											writeToCSV("POLICY NUMBER", var_PolicyNumber);
											writeToCSV("PAGE #: DATA TYPE", "PAGE 1: " + "MATURITY DATE");
											writeToCSV("EXPECTED DATA", maturityDateFromDB);
											writeToCSV("ACTUAL DATA ON PDF", maturityDateFromPDF);
											writeToCSV("RESULT", (maturityDateFromDB.equals(maturityDateFromPDF)) ? "PASS" : "FAIL");

										}

										if (line.contains("THE MINIMUM INITIAL PREMIUM ALLOWED FOR THIS PRODUCT IS")) {
											System.out.println("MINIMUM LINE IS   ____________ " + line);
											String minimumValue = line.split("\\$")[1];
											minimumPremiumValueFromPDF = minimumValue;
											System.out.println("MIN VALUE IS:-" + minimumValue);

											//     Assert.assertEquals(minAllowedPremiumFromDB, minimumPremiumValueFromPDF);
											writeToCSV("POLICY NUMBER", var_PolicyNumber);
											writeToCSV("PAGE #: DATA TYPE", "PAGE 1: " + "MINIMUM PREMIUM ALLOWED");
											writeToCSV("EXPECTED DATA", "$" + minAllowedPremiumFromDB);
											writeToCSV("ACTUAL DATA ON PDF", "$" + minimumPremiumValueFromPDF);
											writeToCSV("RESULT", minAllowedPremiumFromDB.equals(minimumPremiumValueFromPDF) ? "PASS" : "FAIL");

										}

										if (line.contains("THE MAXIMUM PREMIUM ALLOWED FOR THIS PRODUCT WITHOUT HOME OFFICE APPROVAL IS")) {
											String maxValue = line.split("\\$")[1];

											String lastChar = maxValue.substring(maxValue.length() - 1);
											if (lastChar.equals(".")) {
												maxValue = maxValue.substring(0, maxValue.length() - 1);
											}
											maximumPremiumValueFromPDF = maxValue;
											System.out.println("MAX VALUE IS:-" + maximumPremiumValueFromPDF);

											//     Assert.assertEquals(maxAllowedPremiumFromDB, maximumPremiumValueFromPDF);
											writeToCSV("POLICY NUMBER", var_PolicyNumber);
											writeToCSV("PAGE #: DATA TYPE", "PAGE 1: " + "MAXIMUM PREMIUM ALLOWED");
											writeToCSV("EXPECTED DATA", "$" + maxAllowedPremiumFromDB);
											writeToCSV("ACTUAL DATA ON PDF", "$" + maximumPremiumValueFromPDF);
											writeToCSV("RESULT", maxAllowedPremiumFromDB.equals(maximumPremiumValueFromPDF) ? "PASS" : "FAIL");
										}

										if (line.contains("POLICY NUMBER:") && line.contains("CONTRACT SUMMARY DATE:")) {
											String[] parts = line.split(":");
											if (parts.length == 3) {
												policyDateFromPDF = parts[2].trim();
												System.out.println("POLICY DATE IS:-" + policyDateFromPDF);
												policyNumberFromPDF = parts[1].trim().split(" ")[0].trim();
												System.out.println("POLICY NUMBER IS:-" + policyNumberFromPDF);

												writeToCSV("POLICY NUMBER", var_PolicyNumber);
												writeToCSV("PAGE #: DATA TYPE", "PAGE 1: " + "POLICY NUMBER");
												writeToCSV("EXPECTED DATA", policy_number.trim());
												writeToCSV("ACTUAL DATA ON PDF", policyNumberFromPDF);
												writeToCSV("RESULT", (policy_number.trim().equals(policyNumberFromPDF)) ? "PASS" : "FAIL");

												writeToCSV("POLICY NUMBER", var_PolicyNumber);
												writeToCSV("PAGE #: DATA TYPE", "PAGE 1: " + "EFFECTIVE DATE");
												writeToCSV("EXPECTED DATA", effectiveDate);
												writeToCSV("ACTUAL DATA ON PDF", policyDateFromPDF);
												writeToCSV("RESULT", (effectiveDate.equals(policyDateFromPDF)) ? "PASS" : "FAIL");

											}

										}

										if (line.contains("PREMIUM:") && line.contains("$")) {
											String[] parts = line.trim().split(" ");
											if (parts.length == 2) {
												premiumFromPDF = parts[1];
												premiumFromPDF = premiumFromPDF.replace("$", "").replace(",", "");
												System.out.println("premiumFromPDF IS:-" + premiumFromPDF);

												writeToCSV("POLICY NUMBER", var_PolicyNumber);
												writeToCSV("PAGE #: DATA TYPE", "PAGE 1: " + "PREMIUM");
												writeToCSV("EXPECTED DATA", "$" + cashWithApplication);
												writeToCSV("ACTUAL DATA ON PDF", "$" + premiumFromPDF);
												writeToCSV("RESULT", (cashWithApplication.equals(premiumFromPDF)) ? "PASS" : "FAIL");

											}
										}

										if (line.contains("BEFORE THE ANNUITY DATE, THE POLICY MAY BE SURRENDERED FOR ALL OR A PORTION OF ITS SURRENDER VALUE.")) {

											String expectedAnnuityText = "BEFORE THE ANNUITY DATE, THE POLICY MAY BE SURRENDERED FOR ALL OR A PORTION OF ITS SURRENDER VALUE. THE TOTAL SURRENDER VALUE EQUALS THE " +
													"SUM OF THE SURRENDER VALUES FOR THE INTEREST CREDITING OPTIONS. THE SURRENDER VALUE FOR EACH OPTION IS THE GREATER OF THE OPTIONÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬ÃƒÂ¢Ã¢â‚¬Å¾Ã‚Â¢S " +
													"VESTED ACCOUNT VALUE LESS SURRENDER CHARGES, OR THE OPTIONÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬ÃƒÂ¢Ã¢â‚¬Å¾Ã‚Â¢S MINIMUM GUARANTEED SURRENDER VALUE DEFINED IN THE POLICY.";
											String beforeAnnuityText = line + " " + pdfFileInText.get(lineNUmber + 1) + " " + pdfFileInText.get(lineNUmber + 2);

											if (isPage2Exists && VESTING_TABLE.equals("Y")) {
												writeToCSV("POLICY NUMBER", var_PolicyNumber);
												writeToCSV("PAGE #: DATA TYPE", "PAGE 2: " + "STATIC TEXT");
												writeToCSV("EXPECTED DATA", expectedAnnuityText);
												writeToCSV("ACTUAL DATA ON PDF", beforeAnnuityText);
												writeToCSV("RESULT", (beforeAnnuityText.contains("VESTED")) ? "PASS" : "FAIL");

											}

										}

										if (line.contains("A SURRENDER CHARGE MAY BE IMPOSED ON WITHDRAWALS AND IN CALCULATING ANNUITY PAYMENTS.")) {

											String expectedAnnuityText = "A SURRENDER CHARGE MAY BE IMPOSED ON WITHDRAWALS AND IN CALCULATING ANNUITY PAYMENTS. THE SURRENDER CHARGE IS EQUAL TO THE SURRENDER " +
													"FACTOR FOR THE POLICY YEAR (AS SHOWN BELOW) MULTIPLIED BY THE AMOUNT OF THE VESTED ACCOUNT VALUE IN EXCESS OF ANY APPLICABLE FREE AMOUNT.";
											String beforeAnnuityText = line + " " + pdfFileInText.get(lineNUmber + 1);

											if (isPage2Exists && VESTING_TABLE.equals("Y")) {
												writeToCSV("POLICY NUMBER", var_PolicyNumber);
												writeToCSV("PAGE #: DATA TYPE", "PAGE 2: " + "STATIC TEXT");
												writeToCSV("EXPECTED DATA", expectedAnnuityText);
												writeToCSV("ACTUAL DATA ON PDF", beforeAnnuityText);
												writeToCSV("RESULT", (beforeAnnuityText.contains("VESTED")) ? "PASS" : "FAIL");

											}


										}

										if (line.equals("SURRENDER")) {

											int startsLine = 0;
											int endLine = 0;
											String nextLine = pdfFileInText.get(lineNUmber + 1);
											if (nextLine.equals("FACTOR")) {

												String nextLine2 = pdfFileInText.get(lineNUmber + 2);

												if (nextLine2.startsWith("1 ")) {

													startsLine = lineNUmber + 2;

													if (startsLine > 0) {

														HashMap<String, String> listSurrFactors = new HashMap<>();

														SURRENDER_FACTORS:
														for (int i = startsLine; i >= startsLine; i++) {

															if (pdfFileInText.get(i).length() > 0 && Character.isDigit(pdfFileInText.get(i).charAt(0))) {

																String sLine = pdfFileInText.get(i);
																String parts[] = sLine.split(" ");
																if (parts.length > 2) {
																	listSurrFactors.put(parts[0], parts[1]);
																	listSurrFactors.put(parts[2], parts[3]);
																} else {
																	listSurrFactors.put(parts[0], parts[1]);
																}

															} else {
																endLine = i - 1;
																break SURRENDER_FACTORS;
															}

														}

														for (int i = 0; i < listGeneralFundSurrChargeFromDB.size(); i++) {

															System.out.println("SURR CHARGE FACTOR:-" + listGeneralFundSurrChargeFromDB.get(i));
															String dbValue = listGeneralFundSurrChargeFromDB.get(i);
															dbValue = decimalFormat.format(Double.parseDouble(dbValue) * 100);
															dbValue = dbValue + "%";

															;//                                    Assert.assertEquals(listGeneralFundSurrChargeFromDB.get(0), listSurrFactors.get(""+(i+1)));
															writeToCSV("POLICY NUMBER", var_PolicyNumber);
															writeToCSV("PAGE #: DATA TYPE", "PAGE 2: " + "SURRENDER FACTOR");
															writeToCSV("EXPECTED DATA", "$" + dbValue);
															writeToCSV("ACTUAL DATA ON PDF", "$" + listSurrFactors.get("" + (i + 1)));
															writeToCSV("RESULT", dbValue.equals(listSurrFactors.get("" + (i + 1))) ? "PASS" : "FAIL");

														}


													}


												}
											}

										}
										if (line.equals("MARKET VALUE ADJUSTMENT") && isPage2Exists && MVA_INDICATOR.equals("Y")) {

											writeToCSV("POLICY NUMBER", var_PolicyNumber);
											writeToCSV("PAGE #: DATA TYPE", "PAGE 2: " + "STATIC TEXT");
											writeToCSV("EXPECTED DATA", "MARKET VALUE ADJUSTMENT");
											writeToCSV("ACTUAL DATA ON PDF", "MARKET VALUE ADJUSTMENT");
											writeToCSV("RESULT", (filteredText.contains("MARKET VALUE ADJUSTMENT")) ? "PASS" : "FAIL");

											writeToCSV("POLICY NUMBER", var_PolicyNumber);
											writeToCSV("PAGE #: DATA TYPE", "PAGE 2: " + "STATIC TEXT");
											writeToCSV("EXPECTED DATA", "THE POLICY'S SURRENDER VALUE MAY BE AFFECTED BY A MARKET VALUE ADJUSTMENT.");
											writeToCSV("ACTUAL DATA ON PDF", "THE POLICY'S SURRENDER VALUE MAY BE AFFECTED BY A MARKET VALUE ADJUSTMENT.");
											writeToCSV("RESULT", (filteredText.contains("SURRENDER VALUE MAY BE AFFECTED BY A MARKET VALUE ADJUSTMENT.")) ? "PASS" : "FAIL");

											writeToCSV("POLICY NUMBER", var_PolicyNumber);
											writeToCSV("PAGE #: DATA TYPE", "PAGE 2: " + "STATIC TEXT");
											writeToCSV("EXPECTED DATA", "THE NET TOTAL OF ALL MARKET VALUE ADJUSTMENTS AND SURRENDER" );
											writeToCSV("ACTUAL DATA ON PDF", "THE NET TOTAL OF ALL MARKET VALUE ADJUSTMENTS AND SURRENDER" );
											writeToCSV("RESULT", (filteredText.contains("THE NET TOTAL OF ALL MARKET VALUE ADJUSTMENTS AND SURRENDER")) ? "PASS" : "FAIL");

											writeToCSV("POLICY NUMBER", var_PolicyNumber);
											writeToCSV("PAGE #: DATA TYPE", "PAGE 2: " + "STATIC TEXT");
											writeToCSV("EXPECTED DATA", "CHARGES WILL NOT REDUCE THE SURRENDER VALUE TO AN AMOUNT LESS THAN THE MINIMUM GUARANTEED SURRENDER VALUE DEFINED IN THE POLICY.  IF THE");
											writeToCSV("ACTUAL DATA ON PDF", "CHARGES WILL NOT REDUCE THE SURRENDER VALUE TO AN AMOUNT LESS THAN THE MINIMUM GUARANTEED SURRENDER VALUE DEFINED IN THE POLICY. IF THE");
											writeToCSV("RESULT", (filteredText.contains("CHARGES WILL NOT REDUCE THE SURRENDER VALUE TO AN AMOUNT LESS THAN THE MINIMUM GUARANTEED SURRENDER VALUE DEFINED IN THE POLICY. IF THE")) ? "PASS" : "FAIL");

											writeToCSV("POLICY NUMBER", var_PolicyNumber);
											writeToCSV("PAGE #: DATA TYPE", "PAGE 2: " + "STATIC TEXT");
											writeToCSV("EXPECTED DATA", "MARKET VALUE ADJUSTMENT RESULTS IN AN INCREASE TO THE SURRENDER VALUE,");
											writeToCSV("ACTUAL DATA ON PDF", "MARKET VALUE ADJUSTMENT RESULTS IN AN INCREASE TO THE SURRENDER VALUE,");
											writeToCSV("RESULT", (filteredText.contains("MARKET VALUE ADJUSTMENT RESULTS IN AN INCREASE TO THE SURRENDER VALUE,")) ? "PASS" : "FAIL");

											writeToCSV("POLICY NUMBER", var_PolicyNumber);
											writeToCSV("PAGE #: DATA TYPE", "PAGE 2: " + "STATIC TEXT");
											writeToCSV("EXPECTED DATA", "THE AMOUNT OF THE INCREASE WILL NOT BE GREATER THAN THE AMOUNT OF ");
											writeToCSV("ACTUAL DATA ON PDF", "THE AMOUNT OF THE INCREASE WILL NOT BE GREATER THAN THE AMOUNT OF ");
											writeToCSV("RESULT", (filteredText.contains("THE AMOUNT OF THE INCREASE WILL NOT BE GREATER THAN THE AMOUNT OF")) ? "PASS" : "FAIL");

											writeToCSV("POLICY NUMBER", var_PolicyNumber);
											writeToCSV("PAGE #: DATA TYPE", "PAGE 2: " + "STATIC TEXT");
											writeToCSV("EXPECTED DATA", "THE REMAINING SURRENDER CHARGE.");
											writeToCSV("ACTUAL DATA ON PDF", "THE REMAINING SURRENDER CHARGE.");
											writeToCSV("RESULT", (filteredText.contains("THE REMAINING SURRENDER CHARGE.")) ? "PASS" : "FAIL");

										}
										if (line.equals("ANNUITY DATE AGE")) {
											String nextLine = pdfFileInText.get(lineNUmber + 1);

											String parts[] = nextLine.split(" ");
											if (parts.length > 0) {

												String annuityDateFromPDFPage3 = parts[0];
												System.out.println("ANNUITY DATE AGE LINE IS :______" + annuityDateFromPDFPage3);

												//                        Assert.assertEquals(maturityDateFromDB, annuityDateFromPDFPage3);
												writeToCSV("POLICY NUMBER", var_PolicyNumber);
												writeToCSV("PAGE #: DATA TYPE", "PAGE 3: " + "ANNUITY DATE");
												writeToCSV("EXPECTED DATA", "" + maturityDateFromDB);
												writeToCSV("ACTUAL DATA ON PDF", "" + annuityDateFromPDFPage3);
												writeToCSV("RESULT", maturityDateFromDB.equals(annuityDateFromPDFPage3) ? "PASS" : "FAIL");

											}

										}

										if (line.contains("YIELDS ON GROSS PREMIUM")) {

											String nextLine = pdfFileInText.get(lineNUmber + 2);

											String parts[] = nextLine.split(" ");

											if (parts.length > 0) {

												String YIELD_PERCENTAGFromDB1 = YIELD_PERCENTAGFromDB.get(0) + "%";


												String YIELD_PERCENTAGPDFPage3 = parts[3];

												writeToCSV("POLICY NUMBER", var_PolicyNumber);
												writeToCSV("PAGE #: DATA TYPE", "PAGE 3: " + "Yield on gross premium PERCENTAGE  1");
												writeToCSV("EXPECTED DATA", "" + YIELD_PERCENTAGFromDB1);
												writeToCSV("ACTUAL DATA ON PDF", "" + YIELD_PERCENTAGPDFPage3);
												writeToCSV("RESULT", YIELD_PERCENTAGFromDB1.equals(YIELD_PERCENTAGPDFPage3) ? "PASS" : "FAIL");

											}
										}
										if (line.contains("YIELDS ON GROSS PREMIUM")) {

											String nextLine = pdfFileInText.get(lineNUmber + 3);

											String parts[] = nextLine.split(" ");

											if (parts.length > 0) {

												String YIELD_PERCENTAGFromDB1 = YIELD_PERCENTAGFromDB.get(1) + "%";


												String YIELD_PERCENTAGYIELD_PERCENTAGPDFPage3 = parts[1];

												writeToCSV("POLICY NUMBER", var_PolicyNumber);
												writeToCSV("PAGE #: DATA TYPE", "PAGE 3: " + "Yield on gross premium PERCENTAGE  2");
												writeToCSV("EXPECTED DATA", "" + YIELD_PERCENTAGFromDB1);
												writeToCSV("ACTUAL DATA ON PDF", "" + YIELD_PERCENTAGYIELD_PERCENTAGPDFPage3);
												writeToCSV("RESULT", YIELD_PERCENTAGFromDB1.equals(YIELD_PERCENTAGYIELD_PERCENTAGPDFPage3) ? "PASS" : "FAIL");

											}
										}


										if (line.contains("AGENT/BROKER:")) {

											int startCount = lineNUmber + 2;
											String nextLine = pdfFileInText.get(startCount);
											while (!nextLine.contains("INSURER:")) {
												agentAddressLinesFromPDF.add(nextLine);
												startCount += 1;
												nextLine = pdfFileInText.get(startCount);

											}

											//

										}

										if (line.contains("INSURER:")) {
											int startCount = lineNUmber + 2;
											String nextLine = pdfFileInText.get(startCount);
											while (!nextLine.isEmpty()) {
												insurerAddressLinesFromPDF.add(nextLine);
												startCount += 1;
												nextLine = pdfFileInText.get(startCount);

											}


										}


									}

									if (line.equals("THE GUARANTEED VALUES SHOW THE MINIMUM GUARANTEED POLICY VALUES DESCRIBED IN THE POLICY, ASSUMING NO WITHDRAWALS OR SURRENDER.")) {
										String nextLine = pdfFileInText.get(lineNUmber + 1);
										if (nextLine.equalsIgnoreCase("PAGE 2")||nextLine.equalsIgnoreCase("PAGE 2:")) {
											isPage2Exists = true;
										}
									}

									if (line.equals("FIDELITY & GUARANTY LIFE INSURANCE COMPANY") && flag) {
										flag = false;
										break;
									}

									lineNUmber++;

								}

		//						if (isPage2Exists && MVA_INDICATOR.equals("Y")) {
		//
		//							writeToCSV("POLICY NUMBER", var_PolicyNumber);
		//							writeToCSV("PAGE #: DATA TYPE", "PAGE 2: " + "STATIC TEXT");
		//							writeToCSV("EXPECTED DATA", "MARKET VALUE ADJUSTMENT");
		//							writeToCSV("ACTUAL DATA ON PDF", "MARKET VALUE ADJUSTMENT");
		//							writeToCSV("RESULT", (filteredText.contains("MARKET VALUE ADJUSTMENT")) ? "PASS" : "FAIL");
		//
		//							writeToCSV("POLICY NUMBER", var_PolicyNumber);
		//							writeToCSV("PAGE #: DATA TYPE", "PAGE 2: " + "STATIC TEXT");
		//							writeToCSV("EXPECTED DATA", "THE POLICY'S SURRENDER VALUE MAY BE AFFECTED BY A MARKET VALUE ADJUSTMENT.");
		//							writeToCSV("ACTUAL DATA ON PDF", "THE POLICY'S SURRENDER VALUE MAY BE AFFECTED BY A MARKET VALUE ADJUSTMENT.");
		//							writeToCSV("RESULT", (filteredText.contains("SURRENDER VALUE MAY BE AFFECTED BY A MARKET VALUE ADJUSTMENT.")) ? "PASS" : "FAIL");
		//
		//							writeToCSV("POLICY NUMBER", var_PolicyNumber);
		//							writeToCSV("PAGE #: DATA TYPE", "PAGE 2: " + "STATIC TEXT");
		//							writeToCSV("EXPECTED DATA", "THE NET TOTAL OF ALL MARKET VALUE ADJUSTMENTS AND SURRENDER" );
		//							writeToCSV("ACTUAL DATA ON PDF", "THE NET TOTAL OF ALL MARKET VALUE ADJUSTMENTS AND SURRENDER" );
		//							writeToCSV("RESULT", (filteredText.contains("THE NET TOTAL OF ALL MARKET VALUE ADJUSTMENTS AND SURRENDER")) ? "PASS" : "FAIL");
		//
		//							writeToCSV("POLICY NUMBER", var_PolicyNumber);
		//							writeToCSV("PAGE #: DATA TYPE", "PAGE 2: " + "STATIC TEXT");
		//							writeToCSV("EXPECTED DATA", "CHARGES WILL NOT REDUCE THE SURRENDER VALUE TO AN AMOUNT LESS THAN THE MINIMUM GUARANTEED SURRENDER VALUE DEFINED IN THE POLICY.  IF THE");
		//							writeToCSV("ACTUAL DATA ON PDF", "CHARGES WILL NOT REDUCE THE SURRENDER VALUE TO AN AMOUNT LESS THAN THE MINIMUM GUARANTEED SURRENDER VALUE DEFINED IN THE POLICY. IF THE");
		//							writeToCSV("RESULT", (filteredText.contains("CHARGES WILL NOT REDUCE THE SURRENDER VALUE TO AN AMOUNT LESS THAN THE MINIMUM GUARANTEED SURRENDER VALUE DEFINED IN THE POLICY. IF THE")) ? "PASS" : "FAIL");
		//
		//							writeToCSV("POLICY NUMBER", var_PolicyNumber);
		//							writeToCSV("PAGE #: DATA TYPE", "PAGE 2: " + "STATIC TEXT");
		//							writeToCSV("EXPECTED DATA", "MARKET VALUE ADJUSTMENT RESULTS IN AN INCREASE TO THE SURRENDER VALUE,");
		//							writeToCSV("ACTUAL DATA ON PDF", "MARKET VALUE ADJUSTMENT RESULTS IN AN INCREASE TO THE SURRENDER VALUE,");
		//							writeToCSV("RESULT", (filteredText.contains("MARKET VALUE ADJUSTMENT RESULTS IN AN INCREASE TO THE SURRENDER VALUE,")) ? "PASS" : "FAIL");
		//
		//							writeToCSV("POLICY NUMBER", var_PolicyNumber);
		//							writeToCSV("PAGE #: DATA TYPE", "PAGE 2: " + "STATIC TEXT");
		//							writeToCSV("EXPECTED DATA", "THE AMOUNT OF THE INCREASE WILL NOT BE GREATER THAN THE AMOUNT OF ");
		//							writeToCSV("ACTUAL DATA ON PDF", "THE AMOUNT OF THE INCREASE WILL NOT BE GREATER THAN THE AMOUNT OF ");
		//							writeToCSV("RESULT", (filteredText.contains("THE AMOUNT OF THE INCREASE WILL NOT BE GREATER THAN THE AMOUNT OF")) ? "PASS" : "FAIL");
		//
		//							writeToCSV("POLICY NUMBER", var_PolicyNumber);
		//							writeToCSV("PAGE #: DATA TYPE", "PAGE 2: " + "STATIC TEXT");
		//							writeToCSV("EXPECTED DATA", "THE REMAINING SURRENDER CHARGE.");
		//							writeToCSV("ACTUAL DATA ON PDF", "THE REMAINING SURRENDER CHARGE.");
		//							writeToCSV("RESULT", (filteredText.contains("THE REMAINING SURRENDER CHARGE.")) ? "PASS" : "FAIL");
		//
		//						}

								String sql5 = "select COMPANY_LONG_DESC,COMPANY_ADDRESS_LINE_1,COMPANY_ADDRESS_LINE_2,COMPANY_CITY_CODE," +
										"COMPANY_STATE_CODE,COMPANY_ZIP_CODE,AGENT_FIRST_NAME,agent_last_name,AGENT_ADDRESS_CITY,agent_address_line_1," +
										"AGENT_address_line_2,AGENT_STATE_CODE,AGENT_ZIP_CODE " +
										"from iios.policyPageContractView where policy_number='" + var_PolicyNumber + "';";

								Statement statement5 = con.createStatement();
								ResultSet rs5 = null;
								rs5 = statement5.executeQuery(sql5);

								while (rs5.next()) {

									companyLongDescFromDB = rs5.getString("COMPANY_LONG_DESC").toUpperCase().trim();
									companyAddressLine1FromDB = rs5.getString("COMPANY_ADDRESS_LINE_1").toUpperCase().trim();
									companyAddressLine2FromDB = rs5.getString("COMPANY_ADDRESS_LINE_2").toUpperCase().trim();
									companyCityCodeFromDB = rs5.getString("COMPANY_CITY_CODE").toUpperCase().trim();
									companyStateCodeFromDB = rs5.getString("COMPANY_STATE_CODE").toUpperCase().trim();
									companyZipCodeFromDB = rs5.getString("COMPANY_ZIP_CODE").toUpperCase().trim();

									agentFirstNameFromDB = rs5.getString("AGENT_FIRST_NAME").toUpperCase().trim();
									agentLastNameFromDB = rs5.getString("agent_last_name").toUpperCase().trim();
									agentAddressCityFromDB = rs5.getString("AGENT_ADDRESS_CITY").toUpperCase().trim();
									agentAddressLine1FromDB = rs5.getString("agent_address_line_1").toUpperCase().trim();
									agentAddressLine2FromDB = rs5.getString("AGENT_address_line_2").toUpperCase().trim();
									agentStateCodeFromDB = rs5.getString("AGENT_STATE_CODE").toUpperCase().trim();
									agentZipCodeFromDB = rs5.getString("AGENT_ZIP_CODE").toUpperCase().trim();

								}

								System.out.println("INSURER ADRESS LINES COUNT IS =" + insurerAddressLinesFromPDF.size());
								if (insurerAddressLinesFromPDF.size() == 3) {
									companyLongDescFromPDF = insurerAddressLinesFromPDF.get(0).trim();

									companyAddressLine1FromPDF = insurerAddressLinesFromPDF.get(1).trim();

									String nextLine3 = insurerAddressLinesFromPDF.get(2).trim();

									String parts[] = nextLine3.split("\\s*(=>|,|\\s)\\s*");
									companyCityCodeFromPDF = parts[0];
									companyStateCodeFromPDF = parts[1];
									companyZipCodeFromPDF = parts[2];

								}

								if (insurerAddressLinesFromPDF.size() == 4) {
									companyLongDescFromPDF = insurerAddressLinesFromPDF.get(0).trim();

									companyAddressLine1FromPDF = insurerAddressLinesFromPDF.get(1).trim();
									companyAddressLine2FromPDF = insurerAddressLinesFromPDF.get(2).trim();

									String nextLine3 = insurerAddressLinesFromPDF.get(3).trim();

									String parts[] = nextLine3.split("\\s*(=>|,|\\s)\\s*");
									companyCityCodeFromPDF = parts[0];
									companyStateCodeFromPDF = parts[1];
									companyZipCodeFromPDF = parts[2];

								}

								if (agentAddressLinesFromPDF.size() == 3) {
									String nextLine1 = agentAddressLinesFromPDF.get(0).trim();
									agentFirstNameFromPDF = nextLine1.split(" ")[0];
									agentLastNameFromPDF = nextLine1.split(" ")[1];
									agentAddressLine1FromPDF = agentAddressLinesFromPDF.get(1).trim();

									String nextLine3 = agentAddressLinesFromPDF.get(2).trim();

									//   String parts[] = nextLine3.split("\\s*(=>|,|\\s)\\s*");
									//  agentAddressCityFromPDF = parts[0];
									//  agentStateCodeFromPDF = parts[1];
									//  agentZipCodeFromPDF = parts[2];
									String parts[] = nextLine3.split(",");
									agentAddressCityFromPDF = parts[0];
									agentStateCodeFromPDF = parts[1].trim();
									String part[] =agentStateCodeFromPDF.split(" ");
									agentStateCodeFromPDF = part[0].trim();
									agentZipCodeFromPDF = part[1].trim();

								}

								if (agentAddressLinesFromPDF.size() == 4) {
									String nextLine1 = agentAddressLinesFromPDF.get(0).trim();
									agentFirstNameFromPDF = nextLine1.split(" ")[0];
									agentLastNameFromPDF = nextLine1.split(" ")[1];
									agentAddressLine1FromPDF = agentAddressLinesFromPDF.get(1).trim();
									agentAddressLine2FromPDF = agentAddressLinesFromPDF.get(2).trim();

									String nextLine3 = agentAddressLinesFromPDF.get(3).trim();

									// String parts[] = nextLine3.split("\\s*(=>|,|\\s)\\s*");
									String parts[] = nextLine3.split(",");
									agentAddressCityFromPDF = parts[0];
									agentStateCodeFromPDF = parts[1].trim();
									String part[] =agentStateCodeFromPDF.split(" ");
									agentStateCodeFromPDF = part[0].trim();
									agentZipCodeFromPDF = part[1].trim();

								}

								System.out.println("Company ADRESS LINES COUNT IS =" + agentAddressLinesFromPDF.size());

								writeToCSV("POLICY NUMBER", var_PolicyNumber);
								writeToCSV("PAGE #: DATA TYPE", "PAGE 3: " + "COMPANY_LONG_DESC");
								writeToCSV("EXPECTED DATA", companyLongDescFromDB);
								writeToCSV("ACTUAL DATA ON PDF", companyLongDescFromPDF);
								writeToCSV("RESULT", (companyLongDescFromDB.equals(companyLongDescFromPDF)) ? "PASS" : "FAIL");

								writeToCSV("POLICY NUMBER", var_PolicyNumber);
								writeToCSV("PAGE #: DATA TYPE", "PAGE 3: " + "COMPANY_ADDRESS_LINE_1");
								writeToCSV("EXPECTED DATA", companyAddressLine1FromDB);
								writeToCSV("ACTUAL DATA ON PDF", companyAddressLine1FromPDF);
								writeToCSV("RESULT", (companyAddressLine1FromDB.equals(companyAddressLine1FromPDF)) ? "PASS" : "FAIL");

								writeToCSV("POLICY NUMBER", var_PolicyNumber);
								writeToCSV("PAGE #: DATA TYPE", "PAGE 3: " + "COMPANY_ADDRESS_LINE_2");
								writeToCSV("EXPECTED DATA", companyAddressLine2FromDB);
								writeToCSV("ACTUAL DATA ON PDF", companyAddressLine2FromPDF);
								writeToCSV("RESULT", (companyAddressLine2FromDB.equals(companyAddressLine2FromPDF)) ? "PASS" : "FAIL");

								writeToCSV("POLICY NUMBER", var_PolicyNumber);
								writeToCSV("PAGE #: DATA TYPE", "PAGE 3: " + "COMPANY_CITY_CODE");
								writeToCSV("EXPECTED DATA", companyCityCodeFromDB);
								writeToCSV("ACTUAL DATA ON PDF", companyCityCodeFromPDF);
								writeToCSV("RESULT", (companyCityCodeFromDB.equals(companyCityCodeFromPDF)) ? "PASS" : "FAIL");

								writeToCSV("POLICY NUMBER", var_PolicyNumber);
								writeToCSV("PAGE #: DATA TYPE", "PAGE 3: " + "COMPANY_STATE_CODE");
								writeToCSV("EXPECTED DATA", companyStateCodeFromDB);
								writeToCSV("ACTUAL DATA ON PDF", companyStateCodeFromPDF);
								writeToCSV("RESULT", (companyStateCodeFromDB.equals(companyStateCodeFromPDF)) ? "PASS" : "FAIL");

								writeToCSV("POLICY NUMBER", var_PolicyNumber);
								writeToCSV("PAGE #: DATA TYPE", "PAGE 3: " + "COMPANY_ZIP_CODE");
								writeToCSV("EXPECTED DATA", companyZipCodeFromDB);
								writeToCSV("ACTUAL DATA ON PDF", companyZipCodeFromPDF);
								writeToCSV("RESULT", (companyZipCodeFromDB.equals(companyZipCodeFromPDF)) ? "PASS" : "FAIL");

								writeToCSV("POLICY NUMBER", var_PolicyNumber);
								writeToCSV("PAGE #: DATA TYPE", "PAGE 3: " + "AGENT_FIRST_NAME");
								writeToCSV("EXPECTED DATA", agentFirstNameFromDB);
								writeToCSV("ACTUAL DATA ON PDF", agentFirstNameFromPDF);
								writeToCSV("RESULT", (agentFirstNameFromDB.equals(agentFirstNameFromPDF)) ? "PASS" : "FAIL");

								writeToCSV("POLICY NUMBER", var_PolicyNumber);
								writeToCSV("PAGE #: DATA TYPE", "PAGE 3: " + "AGENT_LAST_NAME");
								writeToCSV("EXPECTED DATA", agentLastNameFromDB);
								writeToCSV("ACTUAL DATA ON PDF", agentLastNameFromPDF);
								writeToCSV("RESULT", (agentLastNameFromDB.equals(agentLastNameFromPDF)) ? "PASS" : "FAIL");

								writeToCSV("POLICY NUMBER", var_PolicyNumber);
								writeToCSV("PAGE #: DATA TYPE", "PAGE 3: " + "AGENT_ADDRESS_CITY");
								writeToCSV("EXPECTED DATA", agentAddressCityFromDB);
								writeToCSV("ACTUAL DATA ON PDF", agentAddressCityFromPDF);
								writeToCSV("RESULT", (agentAddressCityFromDB.equals(agentAddressCityFromPDF)) ? "PASS" : "FAIL");

								writeToCSV("POLICY NUMBER", var_PolicyNumber);
								writeToCSV("PAGE #: DATA TYPE", "PAGE 3: " + "agent_address_line_1");
								writeToCSV("EXPECTED DATA", agentAddressLine1FromDB);
								writeToCSV("ACTUAL DATA ON PDF", agentAddressLine1FromPDF);
								writeToCSV("RESULT", (agentAddressLine1FromDB.equals(agentAddressLine1FromPDF)) ? "PASS" : "FAIL");

								writeToCSV("POLICY NUMBER", var_PolicyNumber);
								writeToCSV("PAGE #: DATA TYPE", "PAGE 3: " + "AGENT_address_line_2");
								writeToCSV("EXPECTED DATA", agentAddressLine2FromDB);
								writeToCSV("ACTUAL DATA ON PDF", agentAddressLine2FromPDF);
								writeToCSV("RESULT", (agentAddressLine2FromDB.equals(agentAddressLine2FromPDF)) ? "PASS" : "FAIL");


								writeToCSV("POLICY NUMBER", var_PolicyNumber);
								writeToCSV("PAGE #: DATA TYPE", "PAGE 3: " + "AGENT_STATE_CODE");
								writeToCSV("EXPECTED DATA", agentStateCodeFromDB);
								writeToCSV("ACTUAL DATA ON PDF", agentStateCodeFromPDF);
								writeToCSV("RESULT", (agentStateCodeFromDB.equals(agentStateCodeFromPDF)) ? "PASS" : "FAIL");

								writeToCSV("POLICY NUMBER", var_PolicyNumber);
								writeToCSV("PAGE #: DATA TYPE", "PAGE 3: " + "AGENT_ZIP_CODE");
								writeToCSV("EXPECTED DATA", agentZipCodeFromDB);
								writeToCSV("ACTUAL DATA ON PDF", agentZipCodeFromPDF);
								writeToCSV("RESULT", (agentZipCodeFromDB.equals(agentZipCodeFromPDF)) ? "PASS" : "FAIL");
							}
							con.close();
						}
		 
		 ;} 
		 catch(Exception e) { PrintVal("Renderer Script Exception : "+e.toString()); } 
		 PrintVal("Executed Step = CALLRENDERERSCRIPT,CMPNONCACULTEDVALFRMGIASTOPDF"); 
        WAIT.$(2,"TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

		var_Count = var_Count + ( 1 );
                 LOGGER.info("Executed Step = INCREASEVAR,var_Count,1,TGTYPESCREENREG");
    	}
          LOGGER.info("Executed Step = WEND,TGTYPESCREENREG");
    	resetAttemps();

    }


    @Test
    public void createnewbusinesspolicyforwa() {

        // No need to declare / print method name here, handled by TestNg listener
        // declare("createnewbusinesspolicyforwa");

        CALL.$("LoginToGIAS","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_USERNAME","outerForm:usernameTGWEBCOMMAIdTGWEBCOMMA0","x5824","FALSE","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_PASSWORD","outerForm:passwordTGWEBCOMMAIdTGWEBCOMMA0","Welcome1","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_LOGIN","outerForm:loginButtonTGWEBCOMMAIdTGWEBCOMMA0","TGTYPESCREENREG");

		int var_Count = 0;
                 LOGGER.info("Executed Step = VAR,Integer,var_Count,0,TGTYPESCREENREG");
        LOGGER.info("Executed Step = REPEAT IF");
    	while (check(var_Count,"<",1,"TGTYPESCREENREG")) {
          LOGGER.info("Executed Step = WHILE,var_Count,<,1,TGTYPESCREENREG");
		com.jayway.jsonpath.ReadContext var_CSVData = getJsonData("C:/GIAS_Automation/Agile FandG102/InputData/CreateAPolicy.csv");
                 LOGGER.info("Executed Step = VAR,Json,var_CSVData,C:/GIAS_Automation/Agile FandG102/InputData/CreateAPolicy.csv,TGTYPESCREENREG");
		String var_effectiveDate = "null";
                 LOGGER.info("Executed Step = VAR,String,var_effectiveDate,null,TGTYPESCREENREG");
		var_effectiveDate = getJsonData(var_CSVData , "$.records["+var_Count+"].EffectiveDate");
                 LOGGER.info("Executed Step = STORE,var_effectiveDate,var_CSVData,$.records[{var_Count}].EffectiveDate,TGTYPESCREENREG");
        CALL.$("AddNewBusinessPolicyWA","TGTYPESCREENREG");

        HOVER.$("ELE_MENU_NEWBUSINESS","//td[text()='New Business']","TGTYPESCREENREG");

        TAP.$("ELE_SUBMEN_APPLICATIONENTRYUPDATE","//td[text()='Application Entry/Update']","TGTYPESCREENREG");

        TAP.$("ELE_RNDBTN_ADD","//img[@id='outerForm:gnAddImage']","TGTYPESCREENREG");

		String var_PolicyNumber;
                 LOGGER.info("Executed Step = VAR,String,var_PolicyNumber,TGTYPESCREENREG");
		var_PolicyNumber = getJsonData(var_CSVData , "$.records["+var_Count+"].PolicyNumber");
                 LOGGER.info("Executed Step = STORE,var_PolicyNumber,var_CSVData,$.records[{var_Count}].PolicyNumber,TGTYPESCREENREG");
		String var_FirstName;
                 LOGGER.info("Executed Step = VAR,String,var_FirstName,TGTYPESCREENREG");
		var_FirstName = getJsonData(var_CSVData , "$.records["+var_Count+"].FirstName");
                 LOGGER.info("Executed Step = STORE,var_FirstName,var_CSVData,$.records[{var_Count}].FirstName,TGTYPESCREENREG");
		String var_LastName;
                 LOGGER.info("Executed Step = VAR,String,var_LastName,TGTYPESCREENREG");
		var_LastName = getJsonData(var_CSVData , "$.records["+var_Count+"].LastName");
                 LOGGER.info("Executed Step = STORE,var_LastName,var_CSVData,$.records[{var_Count}].LastName,TGTYPESCREENREG");
		String var_IDNumber;
                 LOGGER.info("Executed Step = VAR,String,var_IDNumber,TGTYPESCREENREG");
		var_IDNumber = getJsonData(var_CSVData , "$.records["+var_Count+"].IDNumber");
                 LOGGER.info("Executed Step = STORE,var_IDNumber,var_CSVData,$.records[{var_Count}].IDNumber,TGTYPESCREENREG");
		String var_ClientSearchName;
                 LOGGER.info("Executed Step = VAR,String,var_ClientSearchName,TGTYPESCREENREG");
		var_ClientSearchName = getJsonData(var_CSVData , "$.records["+var_Count+"].ClientSearchName");
                 LOGGER.info("Executed Step = STORE,var_ClientSearchName,var_CSVData,$.records[{var_Count}].ClientSearchName,TGTYPESCREENREG");
		String var_AgentNumber = "000001019";
                 LOGGER.info("Executed Step = VAR,String,var_AgentNumber,000001019,TGTYPESCREENREG");
        TYPE.$("ELE_TXTBOX_POLICYNUMBER","//input[@type='text'][@name='outerForm:policyNumber']",var_PolicyNumber,"FALSE","TGTYPESCREENREG");

        SELECT.$("ELE_DRPDWN_CURRENCYCODE","//select[@id='outerForm:currencyCode']","DOLLAR [US]","TGTYPESCREENREG");

        SELECT.$("ELE_DRPDWN_STATECOUNTRY","//select[@name='outerForm:countryAndStateCode']	","WASHINGTON","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_EFFECTIVEDATE","//input[@type='text'][@name='outerForm:effectiveDate']",var_effectiveDate,"TRUE","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_CASHWITHAPPLICATION","//input[@type='text'][@name='outerForm:cashWithApplication']","25000","FALSE","TGTYPESCREENREG");

        SELECT.$("ELE_DRPDWN_PAYMENTFREQUENCY","//select[@name='outerForm:paymentMode']","Annual","TGTYPESCREENREG");

        SELECT.$("ELE_DRPDWN_PAYMENTMETHOD","//select[@name='outerForm:paymentCode']","No Notice","TGTYPESCREENREG");

        SELECT.$("ELE_DRPDWN_TAXQUALIFIEDDESCRIPTION","//select[@name='outerForm:taxQualifiedCode']","TRADITIONAL IRA","TGTYPESCREENREG");

        SELECT.$("ELE_DRPDWN_TAXWITHHOLDING","//select[@name='outerForm:taxWithholdingCode']","No Withholding","TGTYPESCREENREG");

        TAP.$("ELE_LINK_SELECTCLIENT","//span[text()='Select Client']","TGTYPESCREENREG");

        TAP.$("ELE_RNDBTN_ADD","//img[@id='outerForm:gnAddImage']","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_FIRSTNAME","//input[@type='text'][@name='outerForm:nameFirst']",var_FirstName,"FALSE","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_LASTNAME","//input[@type='text'][@name='outerForm:nameLast']",var_LastName,"FALSE","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_CLIENTDOB","//input[@type='text'][@name='outerForm:dateOfBirth2']","01/01/1980","FALSE","TGTYPESCREENREG");

        SELECT.$("ELE_DRPDWN_CLIENTGENDER","//select[@name='outerForm:sexCode2']","Male","TGTYPESCREENREG");

        SELECT.$("ELE_DRPDWN_IDTYPE","//select[@name='outerForm:taxIdenUsag2']","Social Security Number","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_IDNUMBER","//input[@type='text'][@name='outerForm:identificationNumber2']",var_IDNumber,"FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SUBMIT","//input[@type='submit'][@name='outerForm:Submit']","TGTYPESCREENREG");

        ASSERT.$("ELE_GETMSG_SUCCESS","//li[@class='MessageInfo']","CONTAINS","successfully","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_ADDRESSLINE1","//input[@type='text'][@name='outerForm:addressLineOne']","17801 International Blvd","FALSE","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_CITY","//input[@type='text'][@name='outerForm:addressCity']","Seattle","FALSE","TGTYPESCREENREG");

        SELECT.$("ELE_DRPDWN_STATECOUNTRY","//select[@name='outerForm:countryAndStateCode']	","WASHINGTON","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_ZIPCODE","//input[@type='text'][@name='outerForm:zipCode']","98158","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SUBMIT","//input[@type='submit'][@name='outerForm:Submit']","TGTYPESCREENREG");

        ASSERT.$("ELE_GETMSG_SUCCESS","//li[@class='MessageInfo']","CONTAINS","successfully","TGTYPESCREENREG");

        TAP.$("ELE_LINK_SELECTCLIENTRELATIONSHIPS","//a[text()='Select Client Relationships']","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_SEARCHCLIENTNAME","//input[@type='text'][@name='outerForm:grid:individualName']",var_ClientSearchName,"FALSE","TGTYPESCREENREG");

        TAP.$("ELE_RNDBTN_FINDPOSITIONTO","//img[@id='outerForm:gnFindImage']","TGTYPESCREENREG");

        SELECT.$("ELE_DRPDWN_RELATIONSHIPGRID","//select[@name='outerForm:grid:0:relationship']","Owner one","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SUBMIT","//input[@type='submit'][@name='outerForm:Submit']","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_AGENTNUMBER","//input[@type='text'][@name='outerForm:grid:0:agentNumberOut2']",var_AgentNumber,"FALSE","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_SITUATIONCODE","//input[@type='text'][@name='outerForm:grid:0:agentSituationCodeOut2']","01","TRUE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SUBMIT","//input[@type='submit'][@name='outerForm:Submit']","TGTYPESCREENREG");

        ASSERT.$("ELE_GETMSG_SUCCESS","//li[@class='MessageInfo']","CONTAINS","successfully","TGTYPESCREENREG");

        TAP.$("ELE_LINK_BENEFITS","//span[text()='Benefits']","TGTYPESCREENREG");

        SELECT.$("ELE_DRPDWN_ADDABENEFIT","//select[@name='outerForm:initialPlanCode']","ACU07Q - ACCUMULATOR PLUS 7 Q","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_CONTINUE","//input[@type='submit'][@name='outerForm:Continue']","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SUBMIT","//input[@type='submit'][@name='outerForm:Submit']","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_CANCEL","//input[@type='submit'][@name='outerForm:Cancel']","TGTYPESCREENREG");

        TAP.$("ELE_LINK_ISSUE","//span[@id='outerForm:IssueText']","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_EFFECTIVEDATE","//input[@type='text'][@name='outerForm:effectiveDate']",var_effectiveDate,"TRUE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_ISSUE","//input[@id='outerForm:Issue']","TGTYPESCREENREG");

        ASSERT.$("ELE_GETMSG_SUCCESS","//li[@class='MessageInfo']","CONTAINS","successfully","TGTYPESCREENREG");

        CALL.$("WritePolicyQuickInquiryToCSV","TGTYPESCREENREG");

        HOVER.$("ELE_MENU_INQUIRY","//td[text()='Inquiry']","TGTYPESCREENREG");

        TAP.$("ELE_SUBMEN_QUICKINQUIRY","//td[contains(text(),'Quick Inquiry')]","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_POLICYNUMBER","//input[@type='text'][@name='outerForm:policyNumber']",var_PolicyNumber,"FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_CONTINUE","//input[@type='submit'][@name='outerForm:Continue']","TGTYPESCREENREG");

		writeToCSV ("Policy Number " , ActionWrapper.getElementValue("ELE_GETVAL_POLICYNUMBER", "//span[@id='outerForm:policyNumber']"));
                 LOGGER.info("Executed Step = WRITETOCSV,ELE_GETVAL_POLICYNUMBER,Policy Number,TGTYPESCREENREG");
		writeToCSV ("Status " , ActionWrapper.getElementValue("ELE_GETVAL_STATUS", "//span[@id='outerForm:statusText']"));
                 LOGGER.info("Executed Step = WRITETOCSV,ELE_GETVAL_STATUS,Status,TGTYPESCREENREG");
		writeToCSV ("Effective Date " , ActionWrapper.getElementValue("ELE_GETVAL_EFFECTIVEDATETEXTFROMGRID", "//span[@id='outerForm:grid:0:effectiveDateOut2']"));
                 LOGGER.info("Executed Step = WRITETOCSV,ELE_GETVAL_EFFECTIVEDATETEXTFROMGRID,Effective Date,TGTYPESCREENREG");
		writeToCSV ("State country " , ActionWrapper.getElementValue("ELE_GETVAL_STATECOUNTRY", "//span[@id='outerForm:ownerCityStateZip']"));
                 LOGGER.info("Executed Step = WRITETOCSV,ELE_GETVAL_STATECOUNTRY,State country,TGTYPESCREENREG");
		writeToCSV ("Agent Number " , ActionWrapper.getElementValue("ELE_GETVAL_AGENTNUMBER", "//span[@id='outerForm:agentNumber']"));
                 LOGGER.info("Executed Step = WRITETOCSV,ELE_GETVAL_AGENTNUMBER,Agent Number,TGTYPESCREENREG");
		try { 
		 java.sql.Connection con = null;
		//String connectionUrl = "jdbc:sqlserver://CIS-GIASDBD1:2433;" + "databaseName=FGLD2DTA;" + "ntegratedSecurity = true";
		        // Load SQL Server JDBC driver and establish connection.
		String connectionUrl = "jdbc:sqlserver://CIS-GIASDBD1:2433" + ";" + 
				"databaseName=FGLD2DTA" + ";" 
				 + "IntegratedSecurity = true";
		        System.out.print("Connecting to SQL Server ... ");
		        con = java.sql.DriverManager.getConnection(connectionUrl) ;
		            System.out.println("Connected to database.");
		                 
				String sql = "select count(*) as PolicyPrint from dbo.nbpolicyprintrequest";

				System.out.println(sql);

				java.sql.Statement statement = con.createStatement();

				java.sql.ResultSet rs = null;
				 rs = statement.executeQuery(sql);
		                  System.out.println(rs);
				 while (rs.next())
				      {
				       
				        String countBefore= rs.getString("PolicyPrint");
				        System.out.println("No. of policies before scheduling the job is " + countBefore);
			       
				      }
				      statement.close();
		                      con.close();
		//Assert.assertTrue(Integer.parseInt(countBefore) > 0);
		 
		 ;} 
		 catch(Exception e) { PrintVal("Renderer Script Exception : "+e.toString()); } 
		 PrintVal("Executed Step = CALLRENDERERSCRIPT,CHKPOLICYPRNTREQTTBLBFRJOBRUN"); 
        CALL.$("ScheduleMidDayJob","TGTYPESCREENREG");

        HOVER.$("ELE_MENU_NEWBUSINESS","//td[text()='New Business']","TGTYPESCREENREG");

        TAP.$("ELE_SUBMEN_POLICYPRINTREQUEST","//td[text()='Policy Print Request (Mid-Day)']","TGTYPESCREENREG");

        TAP.$("ELE_RDOBTN_HOLDJOBNO","//input[@id='outerForm:held:0']","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SUBMIT","//input[@type='submit'][@name='outerForm:Submit']","TGTYPESCREENREG");

		try { 
		 java.sql.Connection con = null;
		//String connectionUrl = "jdbc:sqlserver://CIS-GIASDBD1:2433;" + "databaseName=FGLD2DTA;" + "IntegratedSecurity = true";
		String connectionUrl = "jdbc:sqlserver://CIS-GIASDBD1:2433" + ";" + 
				"databaseName=FGLD2DTA" + ";" 
				 + "IntegratedSecurity = true";
		        // Load SQL Server JDBC driver and establish connection.
		        System.out.print("Connecting to SQL Server ... ");
		        con = java.sql.DriverManager.getConnection(connectionUrl) ;
		            System.out.println("Connected to database.");
		                     
				String sql = "select count(*) PolicyPrint from dbo.nbpolicyprintrequest";

				System.out.println(sql);

				java.sql.Statement statement = con.createStatement();

				java.sql.ResultSet rs = null;
				 rs = statement.executeQuery(sql);
		                  System.out.println(rs);
				 while (rs.next())
				      {
				       
				        String countAfter= rs.getString("PolicyPrint");

				        System.out.println("No. of policies AFTER scheduling the job is " + countAfter);
				       
				      }
				      statement.close();
		                      con.close();
		//if(Integer.parseInt(count) > 0)
		//Assert.assertTrue(Integer.parseInt(countAfter) == 0);
		 
		 ;} 
		 catch(Exception e) { PrintVal("Renderer Script Exception : "+e.toString()); } 
		 PrintVal("Executed Step = CALLRENDERERSCRIPT,CHKPOLICYPRNTREQTBLAFTRJOBRUN"); 
		var_Count = var_Count + ( 1 );
                 LOGGER.info("Executed Step = INCREASEVAR,var_Count,1,TGTYPESCREENREG");
    	}
          LOGGER.info("Executed Step = WEND,TGTYPESCREENREG");
    	resetAttemps();

    }


    @Test
    public void createanewbusinesspolicyfornv() {

        // No need to declare / print method name here, handled by TestNg listener
        // declare("createanewbusinesspolicyfornv");

        CALL.$("LoginToGIAS","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_USERNAME","outerForm:usernameTGWEBCOMMAIdTGWEBCOMMA0","x5824","FALSE","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_PASSWORD","outerForm:passwordTGWEBCOMMAIdTGWEBCOMMA0","Welcome1","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_LOGIN","outerForm:loginButtonTGWEBCOMMAIdTGWEBCOMMA0","TGTYPESCREENREG");

		int var_Count = 0;
                 LOGGER.info("Executed Step = VAR,Integer,var_Count,0,TGTYPESCREENREG");
        LOGGER.info("Executed Step = REPEAT IF");
    	while (check(var_Count,"<",1,"TGTYPESCREENREG")) {
          LOGGER.info("Executed Step = WHILE,var_Count,<,1,TGTYPESCREENREG");
		com.jayway.jsonpath.ReadContext var_CSVData = getJsonData("C:/GIAS_Automation/Agile FandG102/InputData/CreateAPolicy.csv");
                 LOGGER.info("Executed Step = VAR,Json,var_CSVData,C:/GIAS_Automation/Agile FandG102/InputData/CreateAPolicy.csv,TGTYPESCREENREG");
		String var_effectiveDate = "null";
                 LOGGER.info("Executed Step = VAR,String,var_effectiveDate,null,TGTYPESCREENREG");
		var_effectiveDate = getJsonData(var_CSVData , "$.records["+var_Count+"].EffectiveDate");
                 LOGGER.info("Executed Step = STORE,var_effectiveDate,var_CSVData,$.records[{var_Count}].EffectiveDate,TGTYPESCREENREG");
        CALL.$("AddNewBusinessPolicyForNV","TGTYPESCREENREG");

        HOVER.$("ELE_MENU_NEWBUSINESS","//td[text()='New Business']","TGTYPESCREENREG");

        TAP.$("ELE_SUBMEN_APPLICATIONENTRYUPDATE","//td[text()='Application Entry/Update']","TGTYPESCREENREG");

        TAP.$("ELE_RNDBTN_ADD","//img[@id='outerForm:gnAddImage']","TGTYPESCREENREG");

		String var_PolicyNumber;
                 LOGGER.info("Executed Step = VAR,String,var_PolicyNumber,TGTYPESCREENREG");
		var_PolicyNumber = getJsonData(var_CSVData , "$.records["+var_Count+"].PolicyNumber");
                 LOGGER.info("Executed Step = STORE,var_PolicyNumber,var_CSVData,$.records[{var_Count}].PolicyNumber,TGTYPESCREENREG");
		String var_FirstName;
                 LOGGER.info("Executed Step = VAR,String,var_FirstName,TGTYPESCREENREG");
		var_FirstName = getJsonData(var_CSVData , "$.records["+var_Count+"].FirstName");
                 LOGGER.info("Executed Step = STORE,var_FirstName,var_CSVData,$.records[{var_Count}].FirstName,TGTYPESCREENREG");
		String var_LastName;
                 LOGGER.info("Executed Step = VAR,String,var_LastName,TGTYPESCREENREG");
		var_LastName = getJsonData(var_CSVData , "$.records["+var_Count+"].LastName");
                 LOGGER.info("Executed Step = STORE,var_LastName,var_CSVData,$.records[{var_Count}].LastName,TGTYPESCREENREG");
		String var_IDNumber;
                 LOGGER.info("Executed Step = VAR,String,var_IDNumber,TGTYPESCREENREG");
		var_IDNumber = getJsonData(var_CSVData , "$.records["+var_Count+"].IDNumber");
                 LOGGER.info("Executed Step = STORE,var_IDNumber,var_CSVData,$.records[{var_Count}].IDNumber,TGTYPESCREENREG");
		String var_ClientSearchName;
                 LOGGER.info("Executed Step = VAR,String,var_ClientSearchName,TGTYPESCREENREG");
		var_ClientSearchName = getJsonData(var_CSVData , "$.records["+var_Count+"].ClientSearchName");
                 LOGGER.info("Executed Step = STORE,var_ClientSearchName,var_CSVData,$.records[{var_Count}].ClientSearchName,TGTYPESCREENREG");
		String var_AgentNumber = "000001019";
                 LOGGER.info("Executed Step = VAR,String,var_AgentNumber,000001019,TGTYPESCREENREG");
        TYPE.$("ELE_TXTBOX_POLICYNUMBER","//input[@type='text'][@name='outerForm:policyNumber']",var_PolicyNumber,"FALSE","TGTYPESCREENREG");

        SELECT.$("ELE_DRPDWN_CURRENCYCODE","//select[@id='outerForm:currencyCode']","DOLLAR [US]","TGTYPESCREENREG");

        SELECT.$("ELE_DRPDWN_STATECOUNTRY","//select[@name='outerForm:countryAndStateCode']	","NEVADA","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_EFFECTIVEDATE","//input[@type='text'][@name='outerForm:effectiveDate']",var_effectiveDate,"TRUE","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_CASHWITHAPPLICATION","//input[@type='text'][@name='outerForm:cashWithApplication']","25000","FALSE","TGTYPESCREENREG");

        SELECT.$("ELE_DRPDWN_PAYMENTFREQUENCY","//select[@name='outerForm:paymentMode']","Annual","TGTYPESCREENREG");

        SELECT.$("ELE_DRPDWN_PAYMENTMETHOD","//select[@name='outerForm:paymentCode']","No Notice","TGTYPESCREENREG");

        SELECT.$("ELE_DRPDWN_TAXQUALIFIEDDESCRIPTION","//select[@name='outerForm:taxQualifiedCode']","TRADITIONAL IRA","TGTYPESCREENREG");

        SELECT.$("ELE_DRPDWN_TAXWITHHOLDING","//select[@name='outerForm:taxWithholdingCode']","No Withholding","TGTYPESCREENREG");

        TAP.$("ELE_LINK_SELECTCLIENT","//span[text()='Select Client']","TGTYPESCREENREG");

        TAP.$("ELE_RNDBTN_ADD","//img[@id='outerForm:gnAddImage']","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_FIRSTNAME","//input[@type='text'][@name='outerForm:nameFirst']",var_FirstName,"FALSE","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_LASTNAME","//input[@type='text'][@name='outerForm:nameLast']",var_LastName,"FALSE","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_CLIENTDOB","//input[@type='text'][@name='outerForm:dateOfBirth2']","01/01/1980","FALSE","TGTYPESCREENREG");

        SELECT.$("ELE_DRPDWN_CLIENTGENDER","//select[@name='outerForm:sexCode2']","Male","TGTYPESCREENREG");

        SELECT.$("ELE_DRPDWN_IDTYPE","//select[@name='outerForm:taxIdenUsag2']","Social Security Number","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_IDNUMBER","//input[@type='text'][@name='outerForm:identificationNumber2']",var_IDNumber,"FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SUBMIT","//input[@type='submit'][@name='outerForm:Submit']","TGTYPESCREENREG");

        ASSERT.$("ELE_GETMSG_SUCCESS","//li[@class='MessageInfo']","CONTAINS","successfully","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_ADDRESSLINE1","//input[@type='text'][@name='outerForm:addressLineOne']","5757 Wayne Newton Blvd","FALSE","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_CITY","//input[@type='text'][@name='outerForm:addressCity']","Las Vegas","FALSE","TGTYPESCREENREG");

        SELECT.$("ELE_DRPDWN_STATECOUNTRY","//select[@name='outerForm:countryAndStateCode']	","NEVADA","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_ZIPCODE","//input[@type='text'][@name='outerForm:zipCode']","89119","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SUBMIT","//input[@type='submit'][@name='outerForm:Submit']","TGTYPESCREENREG");

        ASSERT.$("ELE_GETMSG_SUCCESS","//li[@class='MessageInfo']","CONTAINS","successfully","TGTYPESCREENREG");

        TAP.$("ELE_LINK_SELECTCLIENTRELATIONSHIPS","//a[text()='Select Client Relationships']","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_SEARCHCLIENTNAME","//input[@type='text'][@name='outerForm:grid:individualName']",var_ClientSearchName,"FALSE","TGTYPESCREENREG");

        TAP.$("ELE_RNDBTN_FINDPOSITIONTO","//img[@id='outerForm:gnFindImage']","TGTYPESCREENREG");

        SELECT.$("ELE_DRPDWN_RELATIONSHIPGRID","//select[@name='outerForm:grid:0:relationship']","Owner one","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SUBMIT","//input[@type='submit'][@name='outerForm:Submit']","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_AGENTNUMBER","//input[@type='text'][@name='outerForm:grid:0:agentNumberOut2']",var_AgentNumber,"FALSE","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_SITUATIONCODE","//input[@type='text'][@name='outerForm:grid:0:agentSituationCodeOut2']","01","TRUE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SUBMIT","//input[@type='submit'][@name='outerForm:Submit']","TGTYPESCREENREG");

        ASSERT.$("ELE_GETMSG_SUCCESS","//li[@class='MessageInfo']","CONTAINS","successfully","TGTYPESCREENREG");

        TAP.$("ELE_LINK_BENEFITS","//span[text()='Benefits']","TGTYPESCREENREG");

        SELECT.$("ELE_DRPDWN_ADDABENEFIT","//select[@name='outerForm:initialPlanCode']","ACU10Q - ACCUMULATOR PLUS 10 Q","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_CONTINUE","//input[@type='submit'][@name='outerForm:Continue']","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SUBMIT","//input[@type='submit'][@name='outerForm:Submit']","TGTYPESCREENREG");

        TAP.$("ELE_LINK_ISSUE","//span[@id='outerForm:IssueText']","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_EFFECTIVEDATE","//input[@type='text'][@name='outerForm:effectiveDate']",var_effectiveDate,"TRUE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_ISSUE","//input[@id='outerForm:Issue']","TGTYPESCREENREG");

        ASSERT.$("ELE_GETMSG_SUCCESS","//li[@class='MessageInfo']","CONTAINS","successfully","TGTYPESCREENREG");

        CALL.$("WritePolicyQuickInquiryToCSV","TGTYPESCREENREG");

        HOVER.$("ELE_MENU_INQUIRY","//td[text()='Inquiry']","TGTYPESCREENREG");

        TAP.$("ELE_SUBMEN_QUICKINQUIRY","//td[contains(text(),'Quick Inquiry')]","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_POLICYNUMBER","//input[@type='text'][@name='outerForm:policyNumber']",var_PolicyNumber,"FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_CONTINUE","//input[@type='submit'][@name='outerForm:Continue']","TGTYPESCREENREG");

		writeToCSV ("Policy Number " , ActionWrapper.getElementValue("ELE_GETVAL_POLICYNUMBER", "//span[@id='outerForm:policyNumber']"));
                 LOGGER.info("Executed Step = WRITETOCSV,ELE_GETVAL_POLICYNUMBER,Policy Number,TGTYPESCREENREG");
		writeToCSV ("Status " , ActionWrapper.getElementValue("ELE_GETVAL_STATUS", "//span[@id='outerForm:statusText']"));
                 LOGGER.info("Executed Step = WRITETOCSV,ELE_GETVAL_STATUS,Status,TGTYPESCREENREG");
		writeToCSV ("Effective Date " , ActionWrapper.getElementValue("ELE_GETVAL_EFFECTIVEDATETEXTFROMGRID", "//span[@id='outerForm:grid:0:effectiveDateOut2']"));
                 LOGGER.info("Executed Step = WRITETOCSV,ELE_GETVAL_EFFECTIVEDATETEXTFROMGRID,Effective Date,TGTYPESCREENREG");
		writeToCSV ("State country " , ActionWrapper.getElementValue("ELE_GETVAL_STATECOUNTRY", "//span[@id='outerForm:ownerCityStateZip']"));
                 LOGGER.info("Executed Step = WRITETOCSV,ELE_GETVAL_STATECOUNTRY,State country,TGTYPESCREENREG");
		writeToCSV ("Agent Number " , ActionWrapper.getElementValue("ELE_GETVAL_AGENTNUMBER", "//span[@id='outerForm:agentNumber']"));
                 LOGGER.info("Executed Step = WRITETOCSV,ELE_GETVAL_AGENTNUMBER,Agent Number,TGTYPESCREENREG");
		try { 
		 java.sql.Connection con = null;
		//String connectionUrl = "jdbc:sqlserver://CIS-GIASDBD1:2433;" + "databaseName=FGLD2DTA;" + "ntegratedSecurity = true";
		        // Load SQL Server JDBC driver and establish connection.
		String connectionUrl = "jdbc:sqlserver://CIS-GIASDBD1:2433" + ";" + 
				"databaseName=FGLD2DTA" + ";" 
				 + "IntegratedSecurity = true";
		        System.out.print("Connecting to SQL Server ... ");
		        con = java.sql.DriverManager.getConnection(connectionUrl) ;
		            System.out.println("Connected to database.");
		                 
				String sql = "select count(*) as PolicyPrint from dbo.nbpolicyprintrequest";

				System.out.println(sql);

				java.sql.Statement statement = con.createStatement();

				java.sql.ResultSet rs = null;
				 rs = statement.executeQuery(sql);
		                  System.out.println(rs);
				 while (rs.next())
				      {
				       
				        String countBefore= rs.getString("PolicyPrint");
				        System.out.println("No. of policies before scheduling the job is " + countBefore);
			       
				      }
				      statement.close();
		                      con.close();
		//Assert.assertTrue(Integer.parseInt(countBefore) > 0);
		 
		 ;} 
		 catch(Exception e) { PrintVal("Renderer Script Exception : "+e.toString()); } 
		 PrintVal("Executed Step = CALLRENDERERSCRIPT,CHKPOLICYPRNTREQTTBLBFRJOBRUN"); 
        CALL.$("ScheduleMidDayJob","TGTYPESCREENREG");

        HOVER.$("ELE_MENU_NEWBUSINESS","//td[text()='New Business']","TGTYPESCREENREG");

        TAP.$("ELE_SUBMEN_POLICYPRINTREQUEST","//td[text()='Policy Print Request (Mid-Day)']","TGTYPESCREENREG");

        TAP.$("ELE_RDOBTN_HOLDJOBNO","//input[@id='outerForm:held:0']","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SUBMIT","//input[@type='submit'][@name='outerForm:Submit']","TGTYPESCREENREG");

		try { 
		 java.sql.Connection con = null;
		//String connectionUrl = "jdbc:sqlserver://CIS-GIASDBD1:2433;" + "databaseName=FGLD2DTA;" + "IntegratedSecurity = true";
		String connectionUrl = "jdbc:sqlserver://CIS-GIASDBD1:2433" + ";" + 
				"databaseName=FGLD2DTA" + ";" 
				 + "IntegratedSecurity = true";
		        // Load SQL Server JDBC driver and establish connection.
		        System.out.print("Connecting to SQL Server ... ");
		        con = java.sql.DriverManager.getConnection(connectionUrl) ;
		            System.out.println("Connected to database.");
		                     
				String sql = "select count(*) PolicyPrint from dbo.nbpolicyprintrequest";

				System.out.println(sql);

				java.sql.Statement statement = con.createStatement();

				java.sql.ResultSet rs = null;
				 rs = statement.executeQuery(sql);
		                  System.out.println(rs);
				 while (rs.next())
				      {
				       
				        String countAfter= rs.getString("PolicyPrint");

				        System.out.println("No. of policies AFTER scheduling the job is " + countAfter);
				       
				      }
				      statement.close();
		                      con.close();
		//if(Integer.parseInt(count) > 0)
		//Assert.assertTrue(Integer.parseInt(countAfter) == 0);
		 
		 ;} 
		 catch(Exception e) { PrintVal("Renderer Script Exception : "+e.toString()); } 
		 PrintVal("Executed Step = CALLRENDERERSCRIPT,CHKPOLICYPRNTREQTBLAFTRJOBRUN"); 
		var_Count = var_Count + ( 1 );
                 LOGGER.info("Executed Step = INCREASEVAR,var_Count,1,TGTYPESCREENREG");
    	}
          LOGGER.info("Executed Step = WEND,TGTYPESCREENREG");
    	resetAttemps();

    }


    @Test
    public void verifypolicydetailsfromdatabasewithpdffile() {

        // No need to declare / print method name here, handled by TestNg listener
        // declare("verifypolicydetailsfromdatabasewithpdffile");

        WAIT.$(2,"TGTYPESCREENREG");

		try { 
		    String file1="C:/GIAS_Automation/Agile FandG102/InputData/FG2ISSU_FGLA_FIA_ACCUM_APPMRG__TEST_2019-7-22_000304723_TGACCUM17_POLPRT_GA_1.pdf";
		        String file2="C:/GIAS_Automation/Agile FandG102/InputData/FG2ISSU_FGLA_FIA_ACCUM_APPMRG__TEST_2019-7-22_000304723_TGACCUM17_POLPRT_GA_1.pdf";

		        pdfUtil.setCompareMode(CompareMode.VISUAL_MODE);

		        pdfUtil.highlightPdfDifference(true);
		        pdfUtil.setImageDestinationPath("C:/GIAS_Automation/Agile FandG102/PDF_OUTPUT");
		        if(pdfUtil.compare(file1, file2)){
		            System.out.print("trueeeeeee ");

		        }else{
		            System.out.print("Falseeeeeee ");

		        }
		 
		 ;} 
		 catch(Exception e) { PrintVal("Renderer Script Exception : "+e.toString()); } 
		 PrintVal("Executed Step = CALLRENDERERSCRIPT,TESTRENDERERISSUEY"); 
        WAIT.$(2,"TGTYPESCREENREG");


    }


    @Test
    public void existingpolicyreuse() {

        // No need to declare / print method name here, handled by TestNg listener
        // declare("existingpolicyreuse");

        CALL.$("LoginToGIAS","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_USERNAME","outerForm:usernameTGWEBCOMMAIdTGWEBCOMMA0","x5824","FALSE","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_PASSWORD","outerForm:passwordTGWEBCOMMAIdTGWEBCOMMA0","Welcome1","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_LOGIN","outerForm:loginButtonTGWEBCOMMAIdTGWEBCOMMA0","TGTYPESCREENREG");

		com.jayway.jsonpath.ReadContext var_CSVData = getJsonData("C:/GIAS_Automation/Agile FandG102/InputData/ExistingPolicy.csv");
                 LOGGER.info("Executed Step = VAR,Json,var_CSVData,C:/GIAS_Automation/Agile FandG102/InputData/ExistingPolicy.csv,TGTYPESCREENREG");
		int var_Count = 0;
                 LOGGER.info("Executed Step = VAR,Integer,var_Count,0,TGTYPESCREENREG");
        LOGGER.info("Executed Step = REPEAT IF");
    	while (check(var_Count,"<",2,"TGTYPESCREENREG")) {
          LOGGER.info("Executed Step = WHILE,var_Count,<,2,TGTYPESCREENREG");
		String var_PolicyNumber;
                 LOGGER.info("Executed Step = VAR,String,var_PolicyNumber,TGTYPESCREENREG");
		var_PolicyNumber = getJsonData(var_CSVData , "$.records["+var_Count+"].PolicyNumber");
                 LOGGER.info("Executed Step = STORE,var_PolicyNumber,var_CSVData,$.records[{var_Count}].PolicyNumber,TGTYPESCREENREG");
        CALL.$("SearchAndChangePolicyStatus","TGTYPESCREENREG");

        HOVER.$("ELE_MENU_NEWBUSINESS","//td[text()='New Business']","TGTYPESCREENREG");

        TAP.$("ELE_SUBMEN_APPLICATIONENTRYUPDATE","//td[text()='Application Entry/Update']","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_POLICYSEARCH","//input[@id='outerForm:grid:policyNumber']",var_PolicyNumber,"TRUE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_FINDPOSTO","//img[@id='outerForm:gnFindImage']","TGTYPESCREENREG");

        LOGGER.info("Executed Step = START IF");
        if (check("ELE_GETVAL_SEARCHEDFIRSTPOLICYSTATUS","//span[@id='outerForm:grid:0:statusOut2']","CONTAINS","Issued Not Paid","TGTYPESCREENREG")) {
         LOGGER.info("Executed Step = IF,ELE_GETVAL_SEARCHEDFIRSTPOLICYSTATUS,CONTAINS,Issued Not Paid,TGTYPESCREENREG");
        TAP.$("ELE_LINK_SEARCHEDPOLICYNUMBER","//span[@id='outerForm:grid:0:policyNumberOut1']","TGTYPESCREENREG");

        TAP.$("ELE_LINK_RETURNTOPENDING","//span[@id='outerForm:ReturnToPendingText']","TGTYPESCREENREG");

        TAP.$("ELE_RECBTN_CHANGETOPENDING","//input[@id='outerForm:ChangeToPending']","TGTYPESCREENREG");

        ASSERT.$("ELE_GETMSG_SUCCESS","//li[@class='MessageInfo']","CONTAINS","successfully","TGTYPESCREENREG");

        TAP.$("ELE_LINK_ISSUE","//span[@id='outerForm:IssueText']","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_ISSUE","//input[@id='outerForm:Issue']","TGTYPESCREENREG");

        ASSERT.$("ELE_GETMSG_SUCCESS","//li[@class='MessageInfo']","CONTAINS","successfully","TGTYPESCREENREG");

        }
          LOGGER.info("Executed Step = ENDIF,TGTYPESCREENREG");
		var_Count = var_Count + ( 1 );
                 LOGGER.info("Executed Step = INCREASEVAR,var_Count,1,TGTYPESCREENREG");
    	}
          LOGGER.info("Executed Step = WEND,TGTYPESCREENREG");
    	resetAttemps();
        CALL.$("RunServicPop200","TGTYPESCREENREG");

        HOVER.$("ELE_MENU_TESTINGTOOLS","//td[contains(text(),'Testing Tools')]","TGTYPESCREENREG");

        HOVER.$("ELE_SUBMEN_DEVELOPMENTTESTS","//td[contains(text(),'Development Tests')]","TGTYPESCREENREG");

        TAP.$("ELE_SUBMEN_ONLINETASKTESTING","//td[contains(text(),'Online Task Testing')]","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_TASKTOEXECUTE","//input[@id='outerForm:taskID']","ServicePop200","TRUE","TGTYPESCREENREG");

        TAP.$("ELE_RDOBTN_OFFLINE","//input[@id='outerForm:submitType:1']","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SUBMIT","//input[@type='submit'][@name='outerForm:Submit']","TGTYPESCREENREG");


    }


    @Test
    public void comparingbasetabletopdf() {

        // No need to declare / print method name here, handled by TestNg listener
        // declare("comparingbasetabletopdf");

		com.jayway.jsonpath.ReadContext var_CSVData = getJsonData("C:/GIAS_Automation/Agile FandG102/InputData/AgileFGSBI_CompPolicy.csv");
                 LOGGER.info("Executed Step = VAR,Json,var_CSVData,C:/GIAS_Automation/Agile FandG102/InputData/AgileFGSBI_CompPolicy.csv,TGTYPESCREENREG");
		int var_Count = 0;
                 LOGGER.info("Executed Step = VAR,Integer,var_Count,0,TGTYPESCREENREG");
		String var_PolicyNumber;
                 LOGGER.info("Executed Step = VAR,String,var_PolicyNumber,TGTYPESCREENREG");
        LOGGER.info("Executed Step = REPEAT IF");
    	while (check(var_Count,"<",1,"TGTYPESCREENREG")) {
          LOGGER.info("Executed Step = WHILE,var_Count,<,1,TGTYPESCREENREG");
		var_PolicyNumber = getJsonData(var_CSVData , "$.records["+var_Count+"].PolicyNumber");
                 LOGGER.info("Executed Step = STORE,var_PolicyNumber,var_CSVData,$.records[{var_Count}].PolicyNumber,TGTYPESCREENREG");
		try { 
		 		 boolean isJointInsureAvailable = false;
								                    boolean flag = false;
								                    ArrayList<String> filteredLines = new ArrayList<>();
								                    String filteredText = "";
								                    String policyLine = "";
								                    String factorLine = "";
								                    String surrenderLine = "";
								                    String yearLine = "";
								                    int yearCount = 0;
								                    int policyCount = 0;
								                    int factorCount = 0;
								                    String stateOfIssueFromDB = "";
													String policynumberFromDB="";

								                    java.sql.Connection con = null;

								                    // Load SQL Server JDBC driver and establish connection.
								                  //  String connectionUrl = "jdbc:sqlserver://CIS-GIASDBD1:243" + "3;" +
								                          //  "databaseName=FGLD2DT" + "A;"
								                        //    + "IntegratedSecurity = true";
								                //    System.out.print("Connecting to SQL Server ... ");
								                 //   con = java.sql.DriverManager.getConnection(connectionUrl);
								                 //   System.out.println("Connected to database.");
												 
												    // Load SQL Server JDBC driver and establish connection.
						                String connectionUrl = "jdbc:sqlserver://CIS-GIASDBD1:243" + "3;" +
						                        "databaseName=FGLD2DT" + "A;"
						                        +
						                        "IntegratedSecurity = true";
						                System.out.print("Connecting to SQL Server ... ");
						                try {
						                    con = DriverManager.getConnection(connectionUrl);
						                } catch (SQLException e) {
						                    e.printStackTrace();
						                }
						                System.out.println("Connected to database.");

								                    //com.jayway.jsonpath.ReadContext CSVData = getJsonData("C:/GIAS_Automation/Agile FandG102/InputData/AgileFGSBI_CompPolicy.csv");
								                    // String var_PolicyNumber = getJsonData(CSVData, "$.records[0].PolicyNumber");

								                    String sql = "select stateOfIssue,policynumber from dbo.PolicyPageContract  where policynumber='" + var_PolicyNumber + "';";
								                    System.out.println(sql);

								                    Statement statement = con.createStatement();

								                    ResultSet rs = null;

								                    rs = statement.executeQuery(sql);

								                    System.out.println(rs);

								                    while (rs.next()) {

								                        stateOfIssueFromDB = rs.getString("stateOfIssue").trim();
														policynumberFromDB = rs.getString("policynumber").trim();
								                    }
								                    statement.close();

								                    con.close();
		String basePath = "C:\\GIAS_Automation\\Agile FandG102\\InputData";
								                    
		String fileName = WebTestUtility.getPDFFileForPolicy(basePath, var_PolicyNumber);
		ArrayList<String> pdfFileInText = WebTestUtility.readLinesFromPDFFile(fileName);
			 if (fileName.isEmpty()) {
				writeToCSV("POLICY NUMBER", var_PolicyNumber);
		         	writeToCSV("PAGE #: DATA TYPE", "PRINT PDF PRESENCE IN INPUTDATA FOLDER");
		                writeToCSV("EXPECTED DATA", "PRINT PDF NOT FOUND IN INPUTDATA FOLER");
		                writeToCSV("ACTUAL DATA ON PDF", var_PolicyNumber + " PRINT PDF NOT FOUND");
				writeToCSV("RESULT", "SKIP");	
															
			} else {
													
				 int pdfLineNumber = 0;
				for (String line : pdfFileInText) {
				//System.out.println(line);

					 if (line.equals("STATEMENT OF BENEFIT INFORMATION – CONTRACT SUMMARY")) {
								                            flag = true;
								                        }

								                        if (flag) {
								                            filteredText = filteredText + "\n" + line;
								                            filteredLines.add(line);
								                            LOGGER.info(line);

								                            if (line.equals("POLICY")) {
								                                policyLine = line;
								                                System.out.println(policyLine);
								                                policyCount++;
								                            }
						 if (line.contains("ANNUITANT(S) NAME(S)") && line.contains("ISSUE AGE(S)")) {
								                                String nextLine = pdfFileInText.get(pdfLineNumber + 1);
								                                String[] insuredParts = nextLine.trim().split(" ");
								                                if (insuredParts.length == 4) {
								                                    isJointInsureAvailable = false;
								                                } else if (insuredParts.length == 2) {
								                                    // For double entry of ANNUITANT
								                                    isJointInsureAvailable = true;
								                                }
								                            }
								                            if (line.equals("FACTOR")) {
								                                factorLine = line;
								                                System.out.println(factorLine);
								                                factorCount++;
								                            }
								                            if (line.equals("SURRENDER")) {
								                                surrenderLine = line;
								                                System.out.println(surrenderLine);
								                            }
								                            if (line.equals("YEAR")) {
								                                yearLine = line;
								                                System.out.println(factorLine);
								                                yearCount++;
								                            }

								                        }
		 //if (line.equals("THE GUARANTEED VALUES SHOW THE MINIMUM GUARANTEED POLICY VALUES DESCRIBED IN THE POLICY, ASSUMING NO WITHDRAWALS OR SURRENDER.")) {
		                        if (line.contains("READ YOUR POLICY CAREFULLY")) {

								                            flag = false;
								                            break;
								                        }

								                        pdfLineNumber++;

								                    }
								                    System.out.println(filteredText);

		 if (stateOfIssueFromDB.equals("MD") || stateOfIssueFromDB.equals("NV") || stateOfIssueFromDB.equals("GA") || stateOfIssueFromDB.equals("WI") || stateOfIssueFromDB.equals("WA")) {


								                        //Page 1 Static Texts
								                        // Assert.assertTrue(filteredText.contains("STATEMENT OF BENEFIT INFORMATION – CONTRACT SUMMARY"));
														writeToCSV("POLICY NUMBER", var_PolicyNumber);
				writeToCSV("PAGE #: DATA TYPE", "PAGE 1: STATIC TEXT");
				writeToCSV("EXPECTED DATA", "STATEMENT OF BENEFIT INFORMATION – CONTRACT SUMMARY");
								                        writeToCSV("ACTUAL DATA ON PDF", "STATEMENT OF BENEFIT INFORMATION – CONTRACT SUMMARY");
								                        writeToCSV("RESULT", (filteredText.contains("STATEMENT OF BENEFIT INFORMATION – CONTRACT SUMMARY")) ? "PASS" : "FAIL");

								                        //Page 1 Static Texts
								                        // Assert.assertTrue(filteredText.contains("POLICY NUMBER:"));
														writeToCSV("POLICY NUMBER", var_PolicyNumber);
								                        writeToCSV("PAGE #: DATA TYPE", "PAGE 1: STATIC TEXT");
								                        writeToCSV("EXPECTED DATA", "POLICY NUMBER:");
								                        writeToCSV("ACTUAL DATA ON PDF", "POLICY NUMBER:");
								                        writeToCSV("RESULT", (filteredText.contains("POLICY NUMBER:")) ? "PASS" : "FAIL");

								                        //Page 1: STATIC TEXTs
								                        // Assert.assertTrue(filteredText.contains("CONTRACT SUMMARY DATE:"));
														writeToCSV("POLICY NUMBER", var_PolicyNumber);
								                        writeToCSV("PAGE #: DATA TYPE", "PAGE 1: STATIC TEXT");
								                        writeToCSV("EXPECTED DATA", "CONTRACT SUMMARY DATE:");
								                        writeToCSV("ACTUAL DATA ON PDF", "CONTRACT SUMMARY DATE:");
								                        writeToCSV("RESULT", (filteredText.contains("CONTRACT SUMMARY DATE:")) ? "PASS" : "FAIL");

								                        //Page 1: STATIC TEXTs
								                        // Assert.assertTrue(filteredText.contains("ANNUITANT(S) NAME(S):"));
														writeToCSV("POLICY NUMBER", var_PolicyNumber);
								                        writeToCSV("PAGE #: DATA TYPE", "PAGE 1: STATIC TEXT");
								                        writeToCSV("EXPECTED DATA", "ANNUITANT(S) NAME(S):");
								                        writeToCSV("ACTUAL DATA ON PDF", "ANNUITANT(S) NAME(S):");
								                        writeToCSV("RESULT", (filteredText.contains("ANNUITANT(S) NAME(S):")) ? "PASS" : "FAIL");


								                        //Page 1: STATIC TEXTs
								                        // Assert.assertTrue(filteredText.contains("ISSUE AGE(S):"));
														writeToCSV("POLICY NUMBER", var_PolicyNumber);
								                        writeToCSV("PAGE #: DATA TYPE", "PAGE 1: STATIC TEXT");
								                        writeToCSV("EXPECTED DATA", "ISSUE AGE(S):");
								                        writeToCSV("ACTUAL DATA ON PDF", "ISSUE AGE(S):");
								                        writeToCSV("RESULT", (filteredText.contains("ISSUE AGE(S):")) ? "PASS" : "FAIL");

								                        //Page 1: STATIC TEXTs
								                        // Assert.assertTrue(filteredText.contains("SEX:"));
														writeToCSV("POLICY NUMBER", var_PolicyNumber);
								                        writeToCSV("PAGE #: DATA TYPE", "PAGE 1: STATIC TEXT");
								                        writeToCSV("EXPECTED DATA", "SEX:");
								                        writeToCSV("ACTUAL DATA ON PDF", "SEX:");
								                        writeToCSV("RESULT", (filteredText.contains("SEX:")) ? "PASS" : "FAIL");

								                        //Page 1: STATIC TEXTs
								                        // Assert.assertTrue(filteredText.contains("PREMIUM:"));
														writeToCSV("POLICY NUMBER", var_PolicyNumber);
								                        writeToCSV("PAGE #: DATA TYPE", "PAGE 1: STATIC TEXT");
								                        writeToCSV("EXPECTED DATA", "PREMIUM:");
								                        writeToCSV("ACTUAL DATA ON PDF", "PREMIUM:");
								                        writeToCSV("RESULT", (filteredText.contains("PREMIUM:")) ? "PASS" : "FAIL");

								                        //Page 1: STATIC TEXTs
								                        // Assert.assertTrue(filteredText.contains("THE MINIMUM INITIAL PREMIUM ALLOWED FOR THIS PRODUCT IS"));
														writeToCSV("POLICY NUMBER", var_PolicyNumber);
								                        writeToCSV("PAGE #: DATA TYPE", "PAGE 1: STATIC TEXT");
								                        writeToCSV("EXPECTED DATA", "THE MINIMUM INITIAL PREMIUM ALLOWED FOR THIS PRODUCT IS");
								                        writeToCSV("ACTUAL DATA ON PDF", "THE MINIMUM INITIAL PREMIUM ALLOWED FOR THIS PRODUCT IS");
								                        writeToCSV("RESULT", (filteredText.contains("THE MINIMUM INITIAL PREMIUM ALLOWED FOR THIS PRODUCT IS")) ? "PASS" : "FAIL");

								                        //Page 1: STATIC TEXTs
								                        // Assert.assertTrue(filteredText.contains("THE MAXIMUM PREMIUM ALLOWED FOR THIS PRODUCT WITHOUT HOME OFFICE APPROVAL IS"));
														writeToCSV("POLICY NUMBER", var_PolicyNumber);
								                        writeToCSV("PAGE #: DATA TYPE", "PAGE 1: STATIC TEXT");
								                        writeToCSV("EXPECTED DATA", "THE MAXIMUM PREMIUM ALLOWED FOR THIS PRODUCT WITHOUT HOME OFFICE APPROVAL IS");
								                        writeToCSV("ACTUAL DATA ON PDF", "THE MAXIMUM PREMIUM ALLOWED FOR THIS PRODUCT WITHOUT HOME OFFICE APPROVAL IS");
								                        writeToCSV("RESULT", (filteredText.contains("THE MAXIMUM PREMIUM ALLOWED FOR THIS PRODUCT WITHOUT HOME OFFICE APPROVAL IS")) ? "PASS" : "FAIL");

								                        //Page 1: STATIC TEXTs
								                        // Assert.assertTrue(filteredText.contains("END OF\nPOLICY\nYEAR"));
														writeToCSV("POLICY NUMBER", var_PolicyNumber);
								                        writeToCSV("PAGE #: DATA TYPE", "PAGE 1: STATIC TEXT");
								                        writeToCSV("EXPECTED DATA", "END OF POLICY YEAR");
								                        writeToCSV("ACTUAL DATA ON PDF", "END OF POLICY YEAR");
								                        writeToCSV("RESULT", (filteredText.contains("END OF\nPOLICY\nYEAR")) ? "PASS" : "FAIL");

								                        //Page 1: STATIC TEXTs
								                        // Assert.assertTrue(filteredText.contains("AGE"));
														writeToCSV("POLICY NUMBER", var_PolicyNumber);
								                        writeToCSV("PAGE #: DATA TYPE", "PAGE 1: STATIC TEXT");
								                        writeToCSV("EXPECTED DATA", "AGE");
								                        writeToCSV("ACTUAL DATA ON PDF", "AGE");
								                        writeToCSV("RESULT", (filteredText.contains("AGE")) ? "PASS" : "FAIL");

								                        //Page 1: STATIC TEXTs
								                        // Assert.assertTrue(filteredText.contains("PROJECTED\nANNUAL\nPREMIUMS"));
														writeToCSV("POLICY NUMBER", var_PolicyNumber);
								                        writeToCSV("PAGE #: DATA TYPE", "PAGE 1: STATIC TEXT");
								                        writeToCSV("EXPECTED DATA", "PROJECTED ANNUAL PREMIUMS");
								                        writeToCSV("ACTUAL DATA ON PDF", "PROJECTED ANNUAL PREMIUMS");
								                        writeToCSV("RESULT", (filteredText.contains("PROJECTED\nANNUAL\nPREMIUMS")) ? "PASS" : "FAIL");

								                        //Page 1: STATIC TEXTs
								                        // Assert.assertTrue(filteredText.contains("GUARANTEED\nSURRENDER\nVALUE"));
														writeToCSV("POLICY NUMBER", var_PolicyNumber);
								                        writeToCSV("PAGE #: DATA TYPE", "PAGE 1: STATIC TEXT");
								                        writeToCSV("EXPECTED DATA", "GUARANTEED SURRENDER VALUE");
								                        writeToCSV("ACTUAL DATA ON PDF", "GUARANTEED SURRENDER VALUE");
								                        writeToCSV("RESULT", (filteredText.contains("GUARANTEED\nSURRENDER\nVALUE")) ? "PASS" : "FAIL");

								                        //Page 1: STATIC TEXTs
								                        // Assert.assertTrue(filteredText.contains("GUARANTEED\nDEATH BENEFIT\nVALUE"));
														writeToCSV("POLICY NUMBER", var_PolicyNumber);
								                        writeToCSV("PAGE #: DATA TYPE", "PAGE 1: STATIC TEXT");
								                        writeToCSV("EXPECTED DATA", "GUARANTEED DEATH BENEFIT VALUE");
								                        writeToCSV("ACTUAL DATA ON PDF", "GUARANTEED DEATH BENEFIT VALUE");
								                        writeToCSV("RESULT", (filteredText.contains("GUARANTEED\nDEATH BENEFIT\nVALUE")) ? "PASS" : "FAIL");

								                        //Page 1: STATIC TEXTs
								                        // Assert.assertTrue(filteredText.contains("THIS POLICY MAY RESULT IN A LOSS IF SURRENDERED DURING THE PERIOD WHEN SURRENDER CHARGES APPLY."));
														writeToCSV("POLICY NUMBER", var_PolicyNumber);
								                        writeToCSV("PAGE #: DATA TYPE", "PAGE 1: STATIC TEXT");
								                        writeToCSV("EXPECTED DATA", "THIS POLICY MAY RESULT IN A LOSS IF SURRENDERED DURING THE PERIOD WHEN SURRENDER CHARGES APPLY.");
								                        writeToCSV("ACTUAL DATA ON PDF", "THIS POLICY MAY RESULT IN A LOSS IF SURRENDERED DURING THE PERIOD WHEN SURRENDER CHARGES APPLY.");
								                        writeToCSV("RESULT", (filteredText.contains("THIS POLICY MAY RESULT IN A LOSS IF SURRENDERED DURING THE PERIOD WHEN SURRENDER CHARGES APPLY.")) ? "PASS" : "FAIL");

								                        //Page 1: STATIC TEXTs
								                        // Assert.assertTrue(filteredText.contains("SCENARIO"));
														writeToCSV("POLICY NUMBER", var_PolicyNumber);
								                        writeToCSV("PAGE #: DATA TYPE", "PAGE 1: STATIC TEXT");
								                        writeToCSV("EXPECTED DATA", "SCENARIO");
								                        writeToCSV("ACTUAL DATA ON PDF", "SCENARIO");
								                        writeToCSV("RESULT", (filteredText.contains("SCENARIO")) ? "PASS" : "FAIL");

								                        //Page 1: STATIC TEXTs
								                        // Assert.assertTrue(filteredText.contains("THE TABLE ABOVE SHOWS THE ACCOUNT VALUES, DEATH BENEFIT VALUES, AND SURRENDER VALUES UNDER THE GUARANTEED SCENARIO."));
								                        writeToCSV("POLICY NUMBER", var_PolicyNumber);
														writeToCSV("PAGE #: DATA TYPE", "PAGE 1: STATIC TEXT");
								                        writeToCSV("EXPECTED DATA", "THE TABLE ABOVE SHOWS THE ACCOUNT VALUES, DEATH BENEFIT VALUES, AND SURRENDER VALUES UNDER THE GUARANTEED SCENARIO.");
								                        writeToCSV("ACTUAL DATA ON PDF", "THE TABLE ABOVE SHOWS THE ACCOUNT VALUES, DEATH BENEFIT VALUES, AND SURRENDER VALUES UNDER THE GUARANTEED SCENARIO.");
								                        writeToCSV("RESULT", (filteredText.contains("THE TABLE ABOVE SHOWS THE ACCOUNT VALUES, DEATH BENEFIT VALUES, AND SURRENDER VALUES UNDER THE GUARANTEED SCENARIO.")) ? "PASS" : "FAIL");

								                        //Page 1: STATIC TEXTs
								                        // Assert.assertTrue(filteredText.contains("THE GUARANTEED VALUES SHOWN ASSUME A 0.00%"));
								                        writeToCSV("POLICY NUMBER", var_PolicyNumber);
														writeToCSV("PAGE #: DATA TYPE", "PAGE 1: STATIC TEXT");
								                        writeToCSV("EXPECTED DATA", "THE GUARANTEED VALUES SHOWN ASSUME A 0.00%");
								                        writeToCSV("ACTUAL DATA ON PDF", "THE GUARANTEED VALUES SHOWN ASSUME A 0.00%");
								                        writeToCSV("RESULT", (filteredText.contains("THE GUARANTEED VALUES SHOWN ASSUME A 0.00%")) ? "PASS" : "FAIL");

								                        //Page 1: STATIC TEXTs
								                        // Assert.assertTrue(filteredText.contains("EFFECTIVE ANNUAL INTEREST RATE FOR THE DURATION OF THE PROJECTION."));
														writeToCSV("POLICY NUMBER", var_PolicyNumber);		                       
													   writeToCSV("PAGE #: DATA TYPE", "PAGE 1: STATIC TEXT");
								                        writeToCSV("EXPECTED DATA", "EFFECTIVE ANNUAL INTEREST RATE FOR THE DURATION OF THE PROJECTION.");
								                        writeToCSV("ACTUAL DATA ON PDF", "EFFECTIVE ANNUAL INTEREST RATE FOR THE DURATION OF THE PROJECTION.");
								                        writeToCSV("RESULT", (filteredText.contains("EFFECTIVE ANNUAL INTEREST RATE FOR THE DURATION OF THE PROJECTION.")) ? "PASS" : "FAIL");

								                        //Page 1: STATIC TEXTs
								                        // Assert.assertTrue(filteredText.contains("THE GUARANTEED VALUES SHOW THE MINIMUM GUARANTEED POLICY VALUES DESCRIBED IN THE POLICY, ASSUMING NO WITHDRAWALS OR SURRENDER."));
								                        writeToCSV("POLICY NUMBER", var_PolicyNumber);
														writeToCSV("PAGE #: DATA TYPE", "PAGE 1: STATIC TEXT");
								                        writeToCSV("EXPECTED DATA", "THE GUARANTEED VALUES SHOW THE MINIMUM GUARANTEED POLICY VALUES DESCRIBED IN THE POLICY, ASSUMING NO WITHDRAWALS OR SURRENDER.");
								                        writeToCSV("ACTUAL DATA ON PDF", "THE GUARANTEED VALUES SHOW THE MINIMUM GUARANTEED POLICY VALUES DESCRIBED IN THE POLICY, ASSUMING NO WITHDRAWALS OR SURRENDER.");
								                        writeToCSV("RESULT", (filteredText.contains("THE GUARANTEED VALUES SHOW THE MINIMUM GUARANTEED POLICY VALUES DESCRIBED IN THE POLICY, ASSUMING NO WITHDRAWALS OR SURRENDER.")) ? "PASS" : "FAIL");


								                        //Page 2: STATIC TEXTs
														writeToCSV("POLICY NUMBER", var_PolicyNumber);
								                        writeToCSV("PAGE #: DATA TYPE", "PAGE 2: STATIC TEXT");
								                        writeToCSV("EXPECTED DATA", "Page 2");
								                        writeToCSV("ACTUAL DATA ON PDF", "Page 2");
								                        writeToCSV("RESULT", (filteredText.contains("Page 2")) ? "PASS" : "FAIL");
													    
														writeToCSV("POLICY NUMBER", var_PolicyNumber);
								                        writeToCSV("PAGE #: DATA TYPE", "PAGE 2: STATIC TEXT");
								                        writeToCSV("EXPECTED DATA", "SURRENDERS");
								                        writeToCSV("ACTUAL DATA ON PDF", "SURRENDERS");
								                        writeToCSV("RESULT", (filteredText.contains("SURRENDERS")) ? "PASS" : "FAIL");

								                        //Page 2: STATIC TEXTs
														writeToCSV("POLICY NUMBER", var_PolicyNumber);
								                        writeToCSV("PAGE #: DATA TYPE", "PAGE 2: STATIC TEXT");
								                        writeToCSV("EXPECTED DATA", "POLICY");
								                        writeToCSV("ACTUAL DATA ON PDF", policyLine);
								                        writeToCSV("RESULT", (filteredText.contains(policyLine)) ? "PASS" : "FAIL");

								                        //Page 2: STATIC TEXTs
														writeToCSV("POLICY NUMBER", var_PolicyNumber);
								                        writeToCSV("PAGE #: DATA TYPE", "PAGE 2: STATIC TEXT");
								                        writeToCSV("EXPECTED DATA", "FACTOR");
								                        writeToCSV("ACTUAL DATA ON PDF", factorLine);
								                        writeToCSV("RESULT", (filteredText.contains(factorLine)) ? "PASS" : "FAIL");

								                        //Page 2: STATIC TEXTs
														writeToCSV("POLICY NUMBER", var_PolicyNumber);
								                        writeToCSV("PAGE #: DATA TYPE", "PAGE 2: STATIC TEXT");
								                        writeToCSV("EXPECTED DATA", "SURRENDER");
								                        writeToCSV("ACTUAL DATA ON PDF", surrenderLine);
								                        writeToCSV("RESULT", (filteredText.contains(surrenderLine)) ? "PASS" : "FAIL");

								                        //Page 2: STATIC TEXTs
														writeToCSV("POLICY NUMBER", var_PolicyNumber);
								                        writeToCSV("PAGE #: DATA TYPE", "PAGE 2: STATIC TEXT");
								                        writeToCSV("EXPECTED DATA", "YEAR");
								                        writeToCSV("ACTUAL DATA ON PDF", yearLine);
								                        writeToCSV("RESULT", (filteredText.contains(yearLine)) ? "PASS" : "FAIL");

								                        //Page 2: STATIC TEXTs
														writeToCSV("POLICY NUMBER", var_PolicyNumber);
								                        writeToCSV("PAGE #: DATA TYPE", "PAGE 2: STATIC TEXT");
								                        writeToCSV("EXPECTED DATA", "YEAR COUNT 2");
								                        writeToCSV("ACTUAL DATA ON PDF", "YEAR COUNT " + yearCount);
								                        writeToCSV("RESULT", (yearCount == 2) ? "PASS" : "FAIL");

								                        //Page 2: STATIC TEXTs
														writeToCSV("POLICY NUMBER", var_PolicyNumber);
								                        writeToCSV("PAGE #: DATA TYPE", "PAGE 2: STATIC TEXT");
								                        writeToCSV("EXPECTED DATA", "POLICY COUNT 3");
								                        writeToCSV("ACTUAL DATA ON PDF", "POLICY COUNT " + policyCount);
								                        writeToCSV("RESULT", (policyCount == 3) ? "PASS" : "FAIL");

								                        //Page 2: STATIC TEXTs
														writeToCSV("POLICY NUMBER", var_PolicyNumber);
								                        writeToCSV("PAGE #: DATA TYPE", "PAGE 2: STATIC TEXT");
								                        writeToCSV("EXPECTED DATA", "FACTOR COUNT 2");
								                        writeToCSV("ACTUAL DATA ON PDF", "FACTOR COUNT " + factorCount);
								                        writeToCSV("RESULT", (factorCount == 2) ? "PASS" : "FAIL");

								                        //Page 2: STATIC TEXTs
														writeToCSV("POLICY NUMBER", var_PolicyNumber);
								                        writeToCSV("PAGE #: DATA TYPE", "PAGE 2: STATIC TEXT");
								                        writeToCSV("EXPECTED DATA", "BEFORE THE ANNUITY DATE, THE POLICY MAY BE SURRENDERED FOR ALL OR A PORTION OF I");
								                        writeToCSV("ACTUAL DATA ON PDF", "BEFORE THE ANNUITY DATE, THE POLICY MAY BE SURRENDERED FOR ALL OR A PORTION OF I");
								                        writeToCSV("RESULT", (filteredText.contains("BEFORE THE ANNUITY DATE, THE POLICY MAY BE SURRENDERED FOR ALL OR A PORTION OF I")) ? "PASS" : "FAIL");

								                        //Page 2: STATIC TEXTs
														writeToCSV("POLICY NUMBER", var_PolicyNumber);
								                        writeToCSV("PAGE #: DATA TYPE", "PAGE 2: STATIC TEXT");
								                        writeToCSV("EXPECTED DATA", "TS SURRENDER VALUE. THE TOTAL SURRENDER VALUE EQUALS THE");
								                        writeToCSV("ACTUAL DATA ON PDF", "TS SURRENDER VALUE. THE TOTAL SURRENDER VALUE EQUALS THE");
								                        writeToCSV("RESULT", (filteredText.contains("TS SURRENDER VALUE. THE TOTAL SURRENDER VALUE EQUALS THE")) ? "PASS" : "FAIL");

								                        //Page 2: STATIC TEXTs
														writeToCSV("POLICY NUMBER", var_PolicyNumber);
								                        writeToCSV("PAGE #: DATA TYPE", "PAGE 2: STATIC TEXT");
								                        writeToCSV("EXPECTED DATA", "SUM OF THE SURRENDER VALUES FOR THE INTEREST CREDITING OPTIONS. THE SURRENDER VA");
								                        writeToCSV("ACTUAL DATA ON PDF", "SUM OF THE SURRENDER VALUES FOR THE INTEREST CREDITING OPTIONS. THE SURRENDER VA");
								                        writeToCSV("RESULT", (filteredText.contains("SUM OF THE SURRENDER VALUES FOR THE INTEREST CREDITING OPTIONS. THE SURRENDER VA")) ? "PASS" : "FAIL");


								                        //Page 2: STATIC TEXTs
														writeToCSV("POLICY NUMBER", var_PolicyNumber);
								                        writeToCSV("PAGE #: DATA TYPE", "PAGE 2: STATIC TEXT");
								                        writeToCSV("EXPECTED DATA", "LUE FOR EACH OPTION IS THE GREATER OF THE OPTION'S");
								                        writeToCSV("ACTUAL DATA ON PDF", "LUE FOR EACH OPTION IS THE GREATER OF THE OPTION'S");
								                        writeToCSV("RESULT", (filteredText.contains("LUE FOR EACH OPTION IS THE GREATER OF THE OPTION")) ? "PASS" : "FAIL");

								                        //Page 2: STATIC TEXTs
														writeToCSV("POLICY NUMBER", var_PolicyNumber);
								                        writeToCSV("PAGE #: DATA TYPE", "PAGE 2: STATIC TEXT");
								                        writeToCSV("EXPECTED DATA", "ACCOUNT VALUE LESS SURRENDER CHARGES, OR THE OPTION'S MINIMUM GUARANTEED SURREND");
								                        writeToCSV("ACTUAL DATA ON PDF", "ACCOUNT VALUE LESS SURRENDER CHARGES, OR THE OPTION'S MINIMUM GUARANTEED SURREND");
								                        writeToCSV("RESULT", (filteredText.contains("ACCOUNT VALUE LESS SURRENDER CHARGES, OR THE OPTION") && filteredText.contains("S MINIMUM GUARANTEED SURREND")) ? "PASS" : "FAIL");

								                        //Page 2: STATIC TEXTs
														writeToCSV("POLICY NUMBER", var_PolicyNumber);
								                        writeToCSV("PAGE #: DATA TYPE", "PAGE 2: STATIC TEXT");
								                        writeToCSV("EXPECTED DATA", "ER VALUE DEFINED IN THE POLICY.");
								                        writeToCSV("ACTUAL DATA ON PDF", "ER VALUE DEFINED IN THE POLICY.");
								                        writeToCSV("RESULT", (filteredText.contains("ER VALUE DEFINED IN THE POLICY.")) ? "PASS" : "FAIL");

								                        //Page 2: STATIC TEXTs
														writeToCSV("POLICY NUMBER", var_PolicyNumber);
								                        writeToCSV("PAGE #: DATA TYPE", "PAGE 2: STATIC TEXT");
								                        writeToCSV("EXPECTED DATA", "A SURRENDER CHARGE MAY BE IMPOSED ON WITHDRAWALS AND IN CALCULATING ANNUITY PAYM");
								                        writeToCSV("ACTUAL DATA ON PDF", "A SURRENDER CHARGE MAY BE IMPOSED ON WITHDRAWALS AND IN CALCULATING ANNUITY PAYM");
								                        writeToCSV("RESULT", (filteredText.contains("A SURRENDER CHARGE MAY BE IMPOSED ON WITHDRAWALS AND IN CALCULATING ANNUITY PAYM")) ? "PASS" : "FAIL");

								                        //Page 2: STATIC TEXTs
														writeToCSV("POLICY NUMBER", var_PolicyNumber);
								                        writeToCSV("PAGE #: DATA TYPE", "PAGE 2: STATIC TEXT");
								                        writeToCSV("EXPECTED DATA", "ENTS. THE SURRENDER CHARGE IS EQUAL TO THE SURRENDER");
								                        writeToCSV("ACTUAL DATA ON PDF", "ENTS. THE SURRENDER CHARGE IS EQUAL TO THE SURRENDER");
								                        writeToCSV("RESULT", (filteredText.contains("ENTS. THE SURRENDER CHARGE IS EQUAL TO THE SURRENDER")) ? "PASS" : "FAIL");

								                        //Page 2: STATIC TEXTs
														writeToCSV("POLICY NUMBER", var_PolicyNumber);
								                        writeToCSV("PAGE #: DATA TYPE", "PAGE 2: STATIC TEXT");
								                        writeToCSV("EXPECTED DATA", "FACTOR FOR THE POLICY YEAR (AS SHOWN BELOW) MULTIPLIED BY THE AMOUNT OF THE");
								                        writeToCSV("ACTUAL DATA ON PDF", "FACTOR FOR THE POLICY YEAR (AS SHOWN BELOW) MULTIPLIED BY THE AMOUNT OF THE");
								                        writeToCSV("RESULT", (filteredText.contains("FACTOR FOR THE POLICY YEAR (AS SHOWN BELOW) MULTIPLIED BY THE AMOUNT OF THE")) ? "PASS" : "FAIL");

								                        //Page 2: STATIC TEXTs
														writeToCSV("POLICY NUMBER", var_PolicyNumber);
								                        writeToCSV("PAGE #: DATA TYPE", "PAGE 2: STATIC TEXT");
								                        writeToCSV("EXPECTED DATA", "UNT VALUE IN EXCESS OF ANY APPLICABLE FREE AMOUNT.");
								                        writeToCSV("ACTUAL DATA ON PDF", "UNT VALUE IN EXCESS OF ANY APPLICABLE FREE AMOUNT.");
								                        writeToCSV("RESULT", (filteredText.contains("UNT VALUE IN EXCESS OF ANY APPLICABLE FREE AMOUNT.")) ? "PASS" : "FAIL");

								                        //Page 2: STATIC TEXTs
														writeToCSV("POLICY NUMBER", var_PolicyNumber);
								                        writeToCSV("PAGE #: DATA TYPE", "PAGE 2: STATIC TEXT");
								                        writeToCSV("EXPECTED DATA", "THE SURRENDER CHARGE DOES NOT APPLY:");
								                        writeToCSV("ACTUAL DATA ON PDF", "THE SURRENDER CHARGE DOES NOT APPLY:");
								                        writeToCSV("RESULT", (filteredText.contains("THE SURRENDER CHARGE DOES NOT APPLY:")) ? "PASS" : "FAIL");

								                        //Page 2: STATIC TEXTs
														writeToCSV("POLICY NUMBER", var_PolicyNumber);
								                        writeToCSV("PAGE #: DATA TYPE", "PAGE 2: STATIC TEXT");
								                        writeToCSV("EXPECTED DATA", "- ON A FREE PARTIAL WITHDRAWAL (UP TO 10% OF THE");
								                        writeToCSV("ACTUAL DATA ON PDF", "- ON A FREE PARTIAL WITHDRAWAL (UP TO 10% OF THE");
								                        writeToCSV("RESULT", (filteredText.contains("- ON A FREE PARTIAL WITHDRAWAL (UP TO 10% OF THE")) ? "PASS" : "FAIL");

								                        //Page 2: STATIC TEXTs
														writeToCSV("POLICY NUMBER", var_PolicyNumber);
								                        writeToCSV("PAGE #: DATA TYPE", "PAGE 2: STATIC TEXT");
								                        writeToCSV("EXPECTED DATA", "ACCOUNT VALUE AS OF THE PRIOR P");
								                        writeToCSV("ACTUAL DATA ON PDF", "ACCOUNT VALUE AS OF THE PRIOR P");
								                        writeToCSV("RESULT", (filteredText.contains("ACCOUNT VALUE AS OF THE PRIOR P")) ? "PASS" : "FAIL");

								                        //Page 2: STATIC TEXTs
														writeToCSV("POLICY NUMBER", var_PolicyNumber);
								                        writeToCSV("PAGE #: DATA TYPE", "PAGE 2: STATIC TEXT");
								                        writeToCSV("EXPECTED DATA", "OLICY ANNIVERSARY, AVAILABLE AFTER THE FIRST POLICY");
								                        writeToCSV("ACTUAL DATA ON PDF", "OLICY ANNIVERSARY, AVAILABLE AFTER THE FIRST POLICY");
								                        writeToCSV("RESULT", (filteredText.contains("OLICY ANNIVERSARY, AVAILABLE AFTER THE FIRST POLICY")) ? "PASS" : "FAIL");

								                        //Page 2: STATIC TEXTs
														writeToCSV("POLICY NUMBER", var_PolicyNumber);
								                        writeToCSV("PAGE #: DATA TYPE", "PAGE 2: STATIC TEXT");
								                        writeToCSV("EXPECTED DATA", "ANNIVERSARY);");
								                        writeToCSV("ACTUAL DATA ON PDF", "ANNIVERSARY);");
								                        writeToCSV("RESULT", (filteredText.contains("ANNIVERSARY);")) ? "PASS" : "FAIL");

								                        //Page 2: STATIC TEXTs
														writeToCSV("POLICY NUMBER", var_PolicyNumber);
								                        writeToCSV("PAGE #: DATA TYPE", "PAGE 2: STATIC TEXT");
								                        writeToCSV("EXPECTED DATA", "- AFTER THE OWNER'S DEATH (UNLESS THE SPOUSE OF THE FIRST OWNER TO DIE CONTINUES");
								                        writeToCSV("ACTUAL DATA ON PDF", "- AFTER THE OWNER'S DEATH (UNLESS THE SPOUSE OF THE FIRST OWNER TO DIE CONTINUES");
								                        writeToCSV("RESULT", (filteredText.contains("- AFTER THE OWNER") && filteredText.contains("S DEATH (UNLESS THE SPOUSE OF THE FIRST OWNER TO DIE CONTINUES")) ? "PASS" : "FAIL");

								                        //Page 2: STATIC TEXTs
														writeToCSV("POLICY NUMBER", var_PolicyNumber);
								                        writeToCSV("PAGE #: DATA TYPE", "PAGE 2: STATIC TEXT");
								                        writeToCSV("EXPECTED DATA", "OR SUCCEEDS TO OWNERSHIP OF THE POLICY);");
								                        writeToCSV("ACTUAL DATA ON PDF", "OR SUCCEEDS TO OWNERSHIP OF THE POLICY);");
								                        writeToCSV("RESULT", (filteredText.contains("OR SUCCEEDS TO OWNERSHIP OF THE POLICY);")) ? "PASS" : "FAIL");

								                        //Page 2: STATIC TEXTs
														writeToCSV("POLICY NUMBER", var_PolicyNumber);
								                        writeToCSV("PAGE #: DATA TYPE", "PAGE 2: STATIC TEXT");
								                        writeToCSV("EXPECTED DATA", "- WHERE CHARGES ARE WAIVED VIA AN ATTACHED RIDER; OR");
								                        writeToCSV("ACTUAL DATA ON PDF", "- WHERE CHARGES ARE WAIVED VIA AN ATTACHED RIDER; OR");
								                        writeToCSV("RESULT", (filteredText.contains("- WHERE CHARGES ARE WAIVED VIA AN ATTACHED RIDER; OR")) ? "PASS" : "FAIL");

								                        //Page 2: STATIC TEXTs
														writeToCSV("POLICY NUMBER", var_PolicyNumber);
								                        writeToCSV("PAGE #: DATA TYPE", "PAGE 2: STATIC TEXT");
								                        writeToCSV("EXPECTED DATA", "YEARS AFTER THE DATE OF ISSUE.");
								                        writeToCSV("ACTUAL DATA ON PDF", "YEARS AFTER THE DATE OF ISSUE.");
								                        writeToCSV("RESULT", (filteredText.contains("YEARS AFTER THE DATE OF ISSUE.")) ? "PASS" : "FAIL");

								                        //Page 3: STATIC TEXTs
														writeToCSV("POLICY NUMBER", var_PolicyNumber);
								                        writeToCSV("PAGE #: DATA TYPE", "PAGE 3:: STATIC TEXT");
								                        writeToCSV("EXPECTED DATA", "Page 3");
								                        writeToCSV("ACTUAL DATA ON PDF", "Page 3");
								                        writeToCSV("RESULT", (filteredText.contains("Page 3")) ? "PASS" : "FAIL");


								                        //Page 3 STATIC TEXTs
														writeToCSV("POLICY NUMBER", var_PolicyNumber);
								                        writeToCSV("PAGE #: DATA TYPE", "PAGE 3: STATIC TEXT");
								                        writeToCSV("EXPECTED DATA", "ANNUITY OPTIONS");
								                        writeToCSV("ACTUAL DATA ON PDF", "ANNUITY OPTIONS");
								                        writeToCSV("RESULT", (filteredText.contains("ANNUITY OPTIONS")) ? "PASS" : "FAIL");

								                        //Page 3 STATIC TEXTs
														writeToCSV("POLICY NUMBER", var_PolicyNumber);
								                        writeToCSV("PAGE #: DATA TYPE", "PAGE 3: STATIC TEXT");
								                        writeToCSV("EXPECTED DATA", "THE FOLLOWING ANNUITY OPTIONS ARE AVAILABLE UNDER THIS POLICY:");
								                        writeToCSV("ACTUAL DATA ON PDF", "THE FOLLOWING ANNUITY OPTIONS ARE AVAILABLE UNDER THIS POLICY:");
								                        writeToCSV("RESULT", (filteredText.contains("THE FOLLOWING ANNUITY OPTIONS ARE AVAILABLE UNDER THIS POLICY:")) ? "PASS" : "FAIL");

								                        //Page 3 STATIC TEXTs
														writeToCSV("POLICY NUMBER", var_PolicyNumber);
								                        writeToCSV("PAGE #: DATA TYPE", "PAGE 3: STATIC TEXT");
								                        writeToCSV("EXPECTED DATA", "'- INCOME FOR A FIXED PERIOD");
								                        writeToCSV("ACTUAL DATA ON PDF", "'- INCOME FOR A FIXED PERIOD");
								                        writeToCSV("RESULT", (filteredText.contains("- INCOME FOR A FIXED PERIOD")) ? "PASS" : "FAIL");

								                        //Page 3 STATIC TEXTs
														writeToCSV("POLICY NUMBER", var_PolicyNumber);
								                        writeToCSV("PAGE #: DATA TYPE", "PAGE 3: STATIC TEXT");
								                        writeToCSV("EXPECTED DATA", "'- INCOME FOR A FIXED PERIOD");
								                        writeToCSV("ACTUAL DATA ON PDF", "'- INCOME FOR A FIXED PERIOD");
								                        writeToCSV("RESULT", (filteredText.contains("- INCOME FOR A FIXED PERIOD")) ? "PASS" : "FAIL");

								                        //Page 3 STATIC TEXTs
														writeToCSV("POLICY NUMBER", var_PolicyNumber);
								                        writeToCSV("PAGE #: DATA TYPE", "PAGE 3: STATIC TEXT");
								                        writeToCSV("EXPECTED DATA", "'- INCOME FOR A FIXED PERIOD");
								                        writeToCSV("ACTUAL DATA ON PDF", "'- INCOME FOR A FIXED PERIOD");
								                        writeToCSV("RESULT", (filteredText.contains("- INCOME FOR A FIXED PERIOD")) ? "PASS" : "FAIL");

								                        //Page 3 STATIC TEXTs
														writeToCSV("POLICY NUMBER", var_PolicyNumber);
								                        writeToCSV("PAGE #: DATA TYPE", "PAGE 3: STATIC TEXT");
								                        writeToCSV("EXPECTED DATA", "'- LIFE INCOME WITH A GUARANTEED PERIOD");
								                        writeToCSV("ACTUAL DATA ON PDF", "'- LIFE INCOME WITH A GUARANTEED PERIOD");
								                        writeToCSV("RESULT", (filteredText.contains("- LIFE INCOME WITH A GUARANTEED PERIOD")) ? "PASS" : "FAIL");

								                        //Page 3 STATIC TEXTs
														writeToCSV("POLICY NUMBER", var_PolicyNumber);
								                        writeToCSV("PAGE #: DATA TYPE", "PAGE 3: STATIC TEXT");
								                        writeToCSV("EXPECTED DATA", "'- LIFE INCOME");
								                        writeToCSV("ACTUAL DATA ON PDF", "'- LIFE INCOME");
								                        writeToCSV("RESULT", (filteredText.contains("- LIFE INCOME")) ? "PASS" : "FAIL");

								                        //Page 3 STATIC TEXTs
														writeToCSV("POLICY NUMBER", var_PolicyNumber);
								                        writeToCSV("PAGE #: DATA TYPE", "PAGE 3: STATIC TEXT");
								                        writeToCSV("EXPECTED DATA", "'- JOINT AND SURVIVOR INCOME WITH GUARANTEED PERIOD");
								                        writeToCSV("ACTUAL DATA ON PDF", "'- JOINT AND SURVIVOR INCOME WITH GUARANTEED PERIOD");
								                        writeToCSV("RESULT", (filteredText.contains("- JOINT AND SURVIVOR INCOME WITH GUARANTEED PERIOD")) ? "PASS" : "FAIL");

								                        //Page 3 STATIC TEXTs
														writeToCSV("POLICY NUMBER", var_PolicyNumber);
								                        writeToCSV("PAGE #: DATA TYPE", "PAGE 3: STATIC TEXT");
								                        writeToCSV("EXPECTED DATA", "'- JOINT AND SURVIVOR LIFE INCOME");
								                        writeToCSV("ACTUAL DATA ON PDF", "'- JOINT AND SURVIVOR LIFE INCOME");
								                        writeToCSV("RESULT", (filteredText.contains("- JOINT AND SURVIVOR LIFE INCOME")) ? "PASS" : "FAIL");

								                        //Page 3 STATIC TEXTs
														writeToCSV("POLICY NUMBER", var_PolicyNumber);
								                        writeToCSV("PAGE #: DATA TYPE", "PAGE 3: STATIC TEXT");
								                        writeToCSV("EXPECTED DATA", "'- LIFE INCOME WITH LUMP SUM REFUND AT DEATH");
								                        writeToCSV("ACTUAL DATA ON PDF", "'- LIFE INCOME WITH LUMP SUM REFUND AT DEATH");
								                        writeToCSV("RESULT", (filteredText.contains("- LIFE INCOME WITH LUMP SUM REFUND AT DEATH")) ? "PASS" : "FAIL");

								                        //Page 3 STATIC TEXTs
														writeToCSV("POLICY NUMBER", var_PolicyNumber);
								                        writeToCSV("PAGE #: DATA TYPE", "PAGE 3: STATIC TEXT");
								                        writeToCSV("EXPECTED DATA", "'- LIFE INCOME WITH LUMP SUM REFUND AT DEATH");
								                        writeToCSV("ACTUAL DATA ON PDF", "'- LIFE INCOME WITH LUMP SUM REFUND AT DEATH");
								                        writeToCSV("RESULT", (filteredText.contains("- LIFE INCOME WITH LUMP SUM REFUND AT DEATH")) ? "PASS" : "FAIL");

								                        //Page 3 STATIC TEXTs
														writeToCSV("POLICY NUMBER", var_PolicyNumber);
								                        writeToCSV("PAGE #: DATA TYPE", "PAGE 3: STATIC TEXT");
								                        writeToCSV("EXPECTED DATA", "'- LIFE INCOME WITH LUMP SUM REFUND AT DEATH");
								                        writeToCSV("ACTUAL DATA ON PDF", "'- LIFE INCOME WITH LUMP SUM REFUND AT DEATH");
								                        writeToCSV("RESULT", (filteredText.contains("- LIFE INCOME WITH LUMP SUM REFUND AT DEATH")) ? "PASS" : "FAIL");

								                        //Page 3 STATIC TEXTs
														writeToCSV("POLICY NUMBER", var_PolicyNumber);
								                        writeToCSV("PAGE #: DATA TYPE", "PAGE 3: STATIC TEXT");
								                        writeToCSV("EXPECTED DATA", "GUARANTEED*");
								                        writeToCSV("ACTUAL DATA ON PDF", "GUARANTEED*");
								                        writeToCSV("RESULT", (filteredText.contains("GUARANTEED*")) ? "PASS" : "FAIL");

								                        //Page 3 STATIC TEXTs
														writeToCSV("POLICY NUMBER", var_PolicyNumber);
								                        writeToCSV("PAGE #: DATA TYPE", "PAGE 3: STATIC TEXT");
								                        writeToCSV("EXPECTED DATA", "AMOUNT AVAILABLE TO");
								                        writeToCSV("ACTUAL DATA ON PDF", "AMOUNT AVAILABLE TO");
								                        writeToCSV("RESULT", (filteredText.contains("AMOUNT AVAILABLE TO")) ? "PASS" : "FAIL");

								                        //Page 3 STATIC TEXTs
														writeToCSV("POLICY NUMBER", var_PolicyNumber);
								                        writeToCSV("PAGE #: DATA TYPE", "PAGE 3: STATIC TEXT");
								                        writeToCSV("EXPECTED DATA", "PROVIDE MONTHLY INCOME");
								                        writeToCSV("ACTUAL DATA ON PDF", "PROVIDE MONTHLY INCOME");
								                        writeToCSV("RESULT", (filteredText.contains("PROVIDE MONTHLY INCOME")) ? "PASS" : "FAIL");

								                        if (isJointInsureAvailable) {
								                            //Page 3 STATIC TEXTs
															writeToCSV("POLICY NUMBER", var_PolicyNumber);
								                            writeToCSV("PAGE #: DATA TYPE", "PAGE 3: STATIC TEXT");
								                            writeToCSV("EXPECTED DATA", "JOINT AND 50% SURVIVOR");
								                            writeToCSV("ACTUAL DATA ON PDF", "JOINT AND 50% SURVIVOR");
								                            writeToCSV("RESULT", (filteredText.contains("JOINT AND 50% SURVIVOR")) ? "PASS" : "FAIL");
								                        }

								                        //Page 3 STATIC TEXTs
														writeToCSV("POLICY NUMBER", var_PolicyNumber);
								                        writeToCSV("PAGE #: DATA TYPE", "PAGE 3: STATIC TEXT");
								                        writeToCSV("EXPECTED DATA", "MONTHLY LIFE INCOME WITH");
								                        writeToCSV("ACTUAL DATA ON PDF", "MONTHLY LIFE INCOME WITH");
								                        writeToCSV("RESULT", (filteredText.contains("MONTHLY LIFE INCOME WITH")) ? "PASS" : "FAIL");

								                        //Page 3 STATIC TEXTs
														writeToCSV("POLICY NUMBER", var_PolicyNumber);
								                        writeToCSV("PAGE #: DATA TYPE", "PAGE 3: STATIC TEXT : HEADERS: Annuity date & Age");
								                        writeToCSV("EXPECTED DATA", "ANNUITY DATE AGE");
								                        writeToCSV("ACTUAL DATA ON PDF", "ANNUITY DATE AGE");
								                        writeToCSV("RESULT", (filteredText.contains("ANNUITY DATE AGE")) ? "PASS" : "FAIL");

								                        //Page 3 STATIC TEXTs
														writeToCSV("POLICY NUMBER", var_PolicyNumber);
								                        writeToCSV("PAGE #: DATA TYPE", "PAGE 3: STATIC TEXT");
								                        writeToCSV("EXPECTED DATA", "TENTH POLICY YEAR");
								                        writeToCSV("ACTUAL DATA ON PDF", "TENTH POLICY YEAR");
								                        writeToCSV("RESULT", (filteredText.contains("TENTH POLICY YEAR")) ? "PASS" : "FAIL");

								                        //Page 3 STATIC TEXTs
														writeToCSV("POLICY NUMBER", var_PolicyNumber);
								                        writeToCSV("PAGE #: DATA TYPE", "PAGE 3: STATIC TEXT");
								                        writeToCSV("EXPECTED DATA", "YIELDS ON GROSS PREMIUM");
								                        writeToCSV("ACTUAL DATA ON PDF", "YIELDS ON GROSS PREMIUM");
								                        writeToCSV("RESULT", (filteredText.contains("YIELDS ON GROSS PREMIUM")) ? "PASS" : "FAIL");

								                        //Page 3 STATIC TEXTs
														writeToCSV("POLICY NUMBER", var_PolicyNumber);
								                        writeToCSV("PAGE #: DATA TYPE", "PAGE 3: STATIC TEXT");
								                        writeToCSV("EXPECTED DATA", "*THESE AMOUNTS ASSUME THAT:");
								                        writeToCSV("ACTUAL DATA ON PDF", "*THESE AMOUNTS ASSUME THAT:");
								                        writeToCSV("RESULT", (filteredText.contains("*THESE AMOUNTS ASSUME THAT:")) ? "PASS" : "FAIL");

								                        //Page 3 STATIC TEXTs
														writeToCSV("POLICY NUMBER", var_PolicyNumber);
								                        writeToCSV("PAGE #: DATA TYPE", "PAGE 3: STATIC TEXT");
								                        writeToCSV("EXPECTED DATA", "'- THE AMOUNT AVAILABLE TO PROVIDE MONTHLY INCOME IS BASED ON GUARANTEED VALUES A");
								                        writeToCSV("ACTUAL DATA ON PDF", "'- THE AMOUNT AVAILABLE TO PROVIDE MONTHLY INCOME IS BASED ON GUARANTEED VALUES A");
								                        writeToCSV("RESULT", (filteredText.contains("- THE AMOUNT AVAILABLE TO PROVIDE MONTHLY INCOME IS BASED ON GUARANTEED VALUES A")) ? "PASS" : "FAIL");

								                        //Page 3 STATIC TEXTs
														writeToCSV("POLICY NUMBER", var_PolicyNumber);
								                        writeToCSV("PAGE #: DATA TYPE", "PAGE 3: STATIC TEXT");
								                        writeToCSV("EXPECTED DATA", "S DESCRIBED ON PAGE 1.");
								                        writeToCSV("ACTUAL DATA ON PDF", "S DESCRIBED ON PAGE 1.");
								                        writeToCSV("RESULT", (filteredText.contains("S DESCRIBED ON PAGE 1.")) ? "PASS" : "FAIL");

								                        //Page 3 STATIC TEXTs
														writeToCSV("POLICY NUMBER", var_PolicyNumber);
								                        writeToCSV("PAGE #: DATA TYPE", "PAGE 3: STATIC TEXT");
								                        writeToCSV("EXPECTED DATA", "'- NO PARTIAL SURRENDERS ARE MADE.");
								                        writeToCSV("ACTUAL DATA ON PDF", "'- NO PARTIAL SURRENDERS ARE MADE.");
								                        writeToCSV("RESULT", (filteredText.contains("- NO PARTIAL SURRENDERS ARE MADE.")) ? "PASS" : "FAIL");

								                        //Page 3 STATIC TEXTs
														writeToCSV("POLICY NUMBER", var_PolicyNumber);
								                        writeToCSV("PAGE #: DATA TYPE", "PAGE 3: STATIC TEXT");
								                        writeToCSV("EXPECTED DATA", "'- THE GUARANTEED MONTHLY LIFE INCOME AMOUNTS ARE BASED ON GUARANTEED ANNUITY PUR");
								                        writeToCSV("ACTUAL DATA ON PDF", "'- THE GUARANTEED MONTHLY LIFE INCOME AMOUNTS ARE BASED ON GUARANTEED ANNUITY PUR");
								                        writeToCSV("RESULT", (filteredText.contains("- THE GUARANTEED MONTHLY LIFE INCOME AMOUNTS ARE BASED ON GUARANTEED ANNUITY PUR")) ? "PASS" : "FAIL");

								                        //Page 3 STATIC TEXTs
														writeToCSV("POLICY NUMBER", var_PolicyNumber);
								                        writeToCSV("PAGE #: DATA TYPE", "PAGE 3: STATIC TEXT");
								                        writeToCSV("EXPECTED DATA", "CHASE RATES SHOWN IN THE POLICY.");
								                        writeToCSV("ACTUAL DATA ON PDF", "CHASE RATES SHOWN IN THE POLICY.");
								                        writeToCSV("RESULT", (filteredText.contains("CHASE RATES SHOWN IN THE POLICY.")) ? "PASS" : "FAIL");

								                        //Page 3 STATIC TEXTs
														writeToCSV("POLICY NUMBER", var_PolicyNumber);
								                        writeToCSV("PAGE #: DATA TYPE", "PAGE 3: STATIC TEXT");
								                        writeToCSV("EXPECTED DATA", "AGENT/BROKER:");
								                        writeToCSV("ACTUAL DATA ON PDF", "AGENT/BROKER:");
								                        writeToCSV("RESULT", (filteredText.contains("AGENT/BROKER:")) ? "PASS" : "FAIL");

								                        //Page 3 STATIC TEXTs
														writeToCSV("POLICY NUMBER", var_PolicyNumber);
								                        writeToCSV("PAGE #: DATA TYPE", "PAGE 3: STATIC TEXT");
								                        writeToCSV("EXPECTED DATA", "ADDRESS:");
								                        writeToCSV("ACTUAL DATA ON PDF", "ADDRESS:");
								                        writeToCSV("RESULT", (filteredText.contains("ADDRESS:")) ? "PASS" : "FAIL");

								                        //Page 3 STATIC TEXTs
														writeToCSV("POLICY NUMBER", var_PolicyNumber);
								                        writeToCSV("PAGE #: DATA TYPE", "PAGE 3: STATIC TEXT");
								                        writeToCSV("EXPECTED DATA", "INSURER:");
								                        writeToCSV("ACTUAL DATA ON PDF", "INSURER:");
								                        writeToCSV("RESULT", (filteredText.contains("INSURER:")) ? "PASS" : "FAIL");
								                    } 
										else if (!var_PolicyNumber.equals(policynumberFromDB)) {
							writeToCSV("POLICY NUMBER", var_PolicyNumber);
											writeToCSV("PAGE #: DATA TYPE", "POLICY PRESENCE IN DATABASE");
								                        //	writeToCSV("ISSUE STATE", "issue State: " + issueState);
								                        writeToCSV("EXPECTED DATA", "POLICY NOT FOUND");
								                        writeToCSV("ACTUAL DATA ON PDF", var_PolicyNumber + " NOT FOUND");
								                        writeToCSV("RESULT", "SKIP");				 
										}
						 
						 else {
											      	writeToCSV("POLICY NUMBER", var_PolicyNumber);
												  writeToCSV("PAGE #: DATA TYPE", "STATIC TEXT - STATEMENT OF BENEFIT for NON-SBI State");
								                        //	writeToCSV("ISSUE STATE", "issue State: " + issueState);
								                        writeToCSV("EXPECTED DATA", "No STATEMENT OF BENEFIT text");
								                        writeToCSV("ACTUAL DATA ON PDF", "No STATEMENT OF BENEFIT  FOR ISSUE STATE " +  stateOfIssueFromDB);
								                        writeToCSV("RESULT", (filteredText.contains("STATEMENT OF BENEFIT")) ? "FAIL" : "PASS");
						 }
						  con.close();
					}
		 
		 ;} 
		 catch(Exception e) { PrintVal("Renderer Script Exception : "+e.toString()); } 
		 PrintVal("Executed Step = CALLRENDERERSCRIPT,BASECOMPARINGSTATICTEXTFROMPDF"); 
        WAIT.$(2,"TGTYPESCREENREG");

		try { 
		 
				 
							java.text.NumberFormat decimalFormat = java.text.NumberFormat.getInstance();
							decimalFormat.setMinimumFractionDigits(2);
							decimalFormat.setMaximumFractionDigits(2);
							decimalFormat.setGroupingUsed(true);

							java.sql.Connection con = null;

							// Load SQL Server JDBC driver and establish connection.
							String connectionUrl = "jdbc:sqlserver://CIS-GIASDBD1:243" + "3;" +
									"databaseName=FGLD2DT" + "A;"
									+
									"IntegratedSecurity = true";
							System.out.print("Connecting to SQL Server ... ");
							try {
								con = DriverManager.getConnection(connectionUrl);
							} catch (SQLException e) {
								e.printStackTrace();
							}
							System.out.println("Connected to database.");
							System.out.println("policyNumberStr. ==" + var_PolicyNumber);

							// DB Variables

							String policynumberFromDB = "";
							String primaryInsuredNameFromDB = "";
							String jointInsuredNameFromDB = "";
							String issueAgeFromDB = "";
							String mvaindicatorFromDB = "";
							String fmtExpMatDateFromDB = "";
							String JNT_INSURED_AGEFromDB = "";
							String sexCodeFromDB = "";
							String JNT_INSURED_SEXFromDB = "";
							String formattedEffeDateFromDB = "";
							String cashWithApplicationFromDB = "";
							String stateOfIssueFromDB = "";
							String maxAllowedPremiumFromDB = "";
							String minAllowedPremiumFromDB = "";

							String ATTAINEDAGEFromDB = "";
							String SURRENDERAMOUNTFromDB = "";
							String GUARANTEEDSURRENDERVALUEFromDB = "";

							String companyLongDescFromDB = "";
							String companyAddressLine1FromDB = "";
							String companyAddressLine2FromDB = "";
							String companyCityCodeFromDB = "";
							String companyStateCodeFromDB = "";
							String companyZipCodeFromDB = "";

							String agentFirstNameFromDB = "";
							String agentLastNameFromDB = "";
							String agentAddressCityFromDB = "";
							String agentAddressLine1FromDB = "";
							String agentAddressLine2FromDB = "";
							String agentStateCodeFromDB = "";
							String agentZipCodeFromDB = "";

							String primaryIssueAgeFromDB = "";
							//String primaryInsuredNameFromDB = "";
							String primaryInsuredSexFromDB = "";
							//String policy_number = "";
							//String effectiveDate = "";
							//String jointInsuredNameFromDB = "";
							String jointInsuredAgeFromDB = "";
							String jointInsuredSexFromDB = "";
							//String cashWithApplication = "";
							//String maturityDateFromDB = "";
							ArrayList<String> listDurationFromDB = new ArrayList<>();
							ArrayList<String> listAttainedAgeFromDB = new ArrayList<>();
							ArrayList<String> listProjectedAccountValueFromDB = new ArrayList<>();
							ArrayList<String> listProjectedAnnualPremiumsFromDB = new ArrayList<>();
							ArrayList<String> listSurrenderValueFromDB = new ArrayList<>();
							ArrayList<String> listDeathValueFromDB = new ArrayList<>();
							ArrayList<String> listGeneralFundSurrChargeFromDB = new ArrayList<>();
							// ArrayList<String> insurerAddressLinesFromPDF = new ArrayList<>();
							// ArrayList<String> agentAddressLinesFromPDF = new ArrayList<>();

							String ageFromDB = "";
							ArrayList<String> userDefinedField15S22FromDB = new ArrayList<>();

							//PDF Variables
							boolean flag = false;
							ArrayList<String> filteredLines = new ArrayList<>();
							String filteredText = "";
							String basePath = "C:\\GIAS_Automation\\Agile FandG102\\InputData";
							// String var_PolicyNumber = policyNumberStr;
							String fileName = helper.WebTestUtility.getPDFFileForPolicy(basePath, var_PolicyNumber);
							ArrayList<String> pdfFileInText = helper.WebTestUtility.readLinesFromPDFFile(fileName);
							if (fileName.isEmpty()) {

								System.out.println("Print PDF File not found");

							} else {

								String companyLongDescFromPDF = "";
								String companyAddressLine1FromPDF = "";
								String companyAddressLine2FromPDF = "";
								String companyCityCodeFromPDF = "";
								String companyStateCodeFromPDF = "";
								String companyZipCodeFromPDF = "";

								String agentFirstNameFromPDF = "";
								String agentLastNameFromPDF = "";
								String agentAddressCityFromPDF = "";
								String agentAddressLine1FromPDF = "";
								String agentAddressLine2FromPDF = "";
								String agentStateCodeFromPDF = "";
								String agentZipCodeFromPDF = "";

								String policyDateFromPDF = "";
								String policyNumberFromPDF = "";
								String primaryIssueAgeFromPDF = "";
								String primaryInsuredSexFromPDF = "";
								String nameOfAnnuitantFromPDF = "";
								String jointInsuredNameFromPDF = "";
								String jointIssueAgeFromPDF = "";
								String jointInsuredSexFromPDF = "";
								String MVA_INDICATOR = "";
								String VESTING_TABLE = "";
								String premiumFromPDF = "";
								String minimumPremiumValueFromPDF = "";
								String maximumPremiumValueFromPDF = "";
								int lineNUmber = 0;
								int firstYearRecordLineNumber = 0;
								int tenthYearRecordLineNumber = 0;
								int lastRecordLineNumber = 0;

								ArrayList<String> listIssueAgeFromPDF = new ArrayList<>();
								ArrayList<String> listDurationFromPDF = new ArrayList<>();
								ArrayList<String> listGuaranteedAccountValueFromPDF = new ArrayList<>();
								ArrayList<String> listProjectedAnnualPremiumsFromPDF = new ArrayList<>();
								ArrayList<String> listSurrenderValueFromPDF = new ArrayList<>();
								ArrayList<String> listDeathValueFromPDF = new ArrayList<>();

								int oldestIssueAge;

								ArrayList<String> listDurationFromDB10Plus = new ArrayList<>();
								ArrayList<String> listAttainedAgeFromDB10Plus = new ArrayList<>();
								ArrayList<Double> listProjectedAccountValueFromDB10Plus = new ArrayList<>();
								ArrayList<Double> listProjectedAnnualPremiumsFromDB10Plus = new ArrayList<>();
								ArrayList<Double> listSurrenderValueFromDB10Plus = new ArrayList<>();
								ArrayList<Double> listDeathValueFromDB10Plus = new ArrayList<>();

								ArrayList<String> listIssueAgeFromPDF10Plus = new ArrayList<>();
								ArrayList<String> listDurationFromPDF10Plus = new ArrayList<>();
								ArrayList<Double> listGuaranteedAccountValueFromPDF10Plus = new ArrayList<>();
								ArrayList<Double> listProjectedAnnualPremiumsFromPDF10Plus = new ArrayList<>();
								ArrayList<Double> listSurrenderValueFromPDF10Plus = new ArrayList<>();
								ArrayList<Double> listDeathValueFromPDF10Plus = new ArrayList<>();

								ArrayList<String> insurerAddressLinesFromPDF = new ArrayList<>();
								ArrayList<String> agentAddressLinesFromPDF = new ArrayList<>();


								String sql = "Select b.policynumber, b.primaryInsuredName , b.jointInsuredName, b.issueAge , b.mvaindicator,b.fmtExpMatDate,\n" +
										"b.userDefinedField3A as JNT_INSURED_AGE , b.sexCode, b.userDefinedField3A2 as JNT_INSURED_SEX, \n" +
										"a.formattedEffeDate , a.cashWithApplication, a.stateOfIssue from dbo. PolicyPageContract  a, dbo.PolicyPageBnft b  \n" +
										"where a.policynumber = b.policynumber and b.policynumber='" +  var_PolicyNumber + "';";

								System.out.println("sql query. ==" + sql);
								Statement statement = con.createStatement();
								ResultSet rs = null;
								try {
									rs = statement.executeQuery(sql);
								} catch (SQLException e) {
									e.printStackTrace();
								}
								System.out.println("==============================================================================");

								while(rs.next()){


									policynumberFromDB = rs.getString("policynumber").trim();
									System.out.println("PolicyNumberFromDB = " + policynumberFromDB);

									primaryInsuredNameFromDB = rs.getString("primaryInsuredName").trim();
									System.out.println("primaryInsuredNameFromDB = " + primaryInsuredNameFromDB);

									jointInsuredNameFromDB = rs.getString("jointInsuredName").trim();
									System.out.println("jointInsuredNameFromDB = " + jointInsuredNameFromDB);

									primaryIssueAgeFromDB = rs.getString("issueAge").trim();
									System.out.println("issueAgeFromDB = " + issueAgeFromDB);

									mvaindicatorFromDB = rs.getString("mvaindicator").trim();
									System.out.println("mvaindicatorFromDB = " + mvaindicatorFromDB);

									fmtExpMatDateFromDB = rs.getString("fmtExpMatDate").trim();
									System.out.println("fmtExpMatDateFromDB = " + fmtExpMatDateFromDB);

									jointInsuredAgeFromDB = rs.getString("JNT_INSURED_AGE").trim();
									System.out.println("JNT_INSURED_AGEFromDB = " + jointInsuredAgeFromDB);

									primaryInsuredSexFromDB = rs.getString("sexCode").trim();
									System.out.println("sexCodeFromDB = " + sexCodeFromDB);

									jointInsuredSexFromDB = rs.getString("JNT_INSURED_SEX").trim();
									System.out.println("JNT_INSURED_SEXFromDB = " + jointInsuredSexFromDB);

									formattedEffeDateFromDB = rs.getString("formattedEffeDate");
									System.out.println("formattedEffeDateFromDB = " + formattedEffeDateFromDB);

									cashWithApplicationFromDB = rs.getString("cashWithApplication");
									System.out.println("cashWithApplicationFromDB = " + cashWithApplicationFromDB);

									stateOfIssueFromDB = rs.getString("stateOfIssue").trim();
									System.out.println("StateOfIssueFromDB = " + stateOfIssueFromDB);


								}
								statement.close();

								if ( stateOfIssueFromDB.equals("MD") || stateOfIssueFromDB.equals("NV") || stateOfIssueFromDB.equals("GA") || stateOfIssueFromDB.equals("WI") || stateOfIssueFromDB.equals("WA")) {
									// DB Variables
									String sql1 = "\n" +
											"select duration, attainedAge, projectedAcctValuG, surrenderAmount , PROJECTEDANNPREM, projectedDeathBnftG , GENERALFUNDSURRCHRG\n" +
											"from  dbo.policyPageValues  where policynumber='" + var_PolicyNumber + "' and duration between 1 and 10;";

									Statement statement1 = con.createStatement();
									ResultSet rs2 = null;
									try {
										rs2 = statement1.executeQuery(sql1);
									} catch (SQLException e) {
										e.printStackTrace();
									}
									System.out.println(rs2);

									while(rs2.next()){

										String duration = rs2.getString("duration");
										listDurationFromDB.add(duration);

										String attainedAge = rs2.getString("attainedAge");
										listAttainedAgeFromDB.add(attainedAge);

										String projectedAnnualPremium = rs2.getString("PROJECTEDANNPREM");
										if (projectedAnnualPremium != null) {
											projectedAnnualPremium = decimalFormat.format(Double.parseDouble(projectedAnnualPremium));
											if (!projectedAnnualPremium.contains(".")) {
												projectedAnnualPremium = projectedAnnualPremium + ".00";
											}
										}
										listProjectedAnnualPremiumsFromDB.add(projectedAnnualPremium);

										String projectedAccountValue = rs2.getString("PROJECTEDACCTVALUG");
										if (projectedAccountValue != null) {
											projectedAccountValue = decimalFormat.format(Double.parseDouble(projectedAccountValue));
											if (!projectedAccountValue.contains(".")) {
												projectedAccountValue = projectedAccountValue + ".00";
											}
										}
										listProjectedAccountValueFromDB.add(projectedAccountValue);

										String surrenderValue = rs2.getString("SURRENDERAMOUNT");
										if (surrenderValue != null) {
											surrenderValue = decimalFormat.format(Double.parseDouble(surrenderValue));
											if (!surrenderValue.contains(".")) {
												surrenderValue = surrenderValue + ".00";
											}
										}
										listSurrenderValueFromDB.add(surrenderValue);

										String deathValue = rs2.getString("PROJECTEDDEATHBNFTG");
										if (deathValue != null) {
											deathValue = decimalFormat.format(Double.parseDouble(deathValue));
											if (!deathValue.contains(".")) {
												deathValue = deathValue + ".00";
											}
										}
										listDeathValueFromDB.add(deathValue);

										String generalFundSurrCharge = rs2.getString("GENERALFUNDSURRCHRG");
										if (generalFundSurrCharge != null) {
											generalFundSurrCharge = decimalFormat.format(Double.parseDouble(generalFundSurrCharge));
											if (!generalFundSurrCharge.contains(".")) {
												generalFundSurrCharge = generalFundSurrCharge + ".00";
											}
										}
										listGeneralFundSurrChargeFromDB.add(generalFundSurrCharge);


									}

									statement1.close();


									String sql4 = "Select DURATION,ATTAINEDAGE,PROJECTEDANNPREM,PROJECTEDDEATHBNFTG,SURRENDERAMOUNT, " +
											"PROJECTEDACCTVALUG From " +
											"(Select Row_Number() Over (Order By PROJECTEDACCTVALUG) As RowNum, * From " +
											"dbo.PolicyPageValues where policynumber='" + var_PolicyNumber + "') t2 " +
											"Where ATTAINEDAGE in (60,65,100) or DURATION in (20);";

									Statement statement4 = con.createStatement();
									ResultSet rs4 = null;
									try {
										rs4 = statement4.executeQuery(sql4);
									} catch (SQLException e) {
										e.printStackTrace();
									}
									System.out.println(rs4);
									while (rs4.next()) {

										String duration = rs4.getString("DURATION");
										listDurationFromDB10Plus.add(duration);

										String attainedAge = rs4.getString("ATTAINEDAGE");
										listAttainedAgeFromDB10Plus.add(attainedAge);

										String projectedAnnualPremium = rs4.getString("PROJECTEDANNPREM");
										listProjectedAnnualPremiumsFromDB10Plus.add(Double.parseDouble(projectedAnnualPremium));

										String projectedAccountValue = rs4.getString("PROJECTEDACCTVALUG");
										listProjectedAccountValueFromDB10Plus.add(Double.parseDouble(projectedAccountValue));

										String surrenderValue = rs4.getString("SURRENDERAMOUNT");
										listSurrenderValueFromDB10Plus.add(Double.parseDouble(surrenderValue));

										String deathValue = rs4.getString("PROJECTEDDEATHBNFTG");
										listDeathValueFromDB10Plus.add(Double.parseDouble(deathValue));

									}

									statement4.close();


									String sql6 = "Select ATTAINEDAGE,SURRENDERAMOUNT From \n" +
											"(Select Row_Number() Over (Order By projectedAcctValuG) As RowNum, * From dbo.PolicyPageValues where policynumber='" + var_PolicyNumber + "') t2\n" +
											"Where ATTAINEDAGE in (100);";

									Statement statement6 = con.createStatement();
									ResultSet rs6 = null;
									try {
										rs6 = statement6.executeQuery(sql6);
									} catch (SQLException e) {
										e.printStackTrace();
									}
									System.out.println(rs6);

									while (rs6.next()) {

										String ATTAINEDAGE = rs6.getString("ATTAINEDAGE");
										ATTAINEDAGEFromDB = ATTAINEDAGE;

										String SURRENDERAMOUNT = rs6.getString("SURRENDERAMOUNT");
										SURRENDERAMOUNTFromDB = SURRENDERAMOUNT.trim();

									}

									statement6.close();






									String sql7 = "select af.annuityFactor, p.planMinimumUnits*1000 as MIN_ALLOW_INIT_PREMIUM,p.planMaximumUnits*1000 as MAX_ALLOW_INIT_PREMIUM from \n" +
											"PlanDescription as p\n" +
											"join PolicyPageBnft as ppb on ppb.planCode = p.planCode\n" +
											"join PolicyPageContract as ppc on ppc.policyNumber = ppb.policyNumber\n" +
											"join (select distinct ppb.policyNumber,\n" +
											"        case when af.stateCode is null\n" +
											"            then '**'\n" +
											"        else af.stateCode\n" +
											"    end as afstate\n" +
											"     from PlanDescription as p\n" +
											"    join PolicyPageBnft as ppb on p.planCode = ppb.planCode\n" +
											"    join PolicyPageContract as ppc on ppb.policyNumber = ppc.policyNumber\n" +
											"    left join AnnuityFactor as af on p.userDefinedField10A2 = af.tableCode \n" +
											"        and ppc.stateOfIssue = af.stateCode) as afstate on ppb.policyNumber = afstate.policyNumber\n" +
											"join AnnuityFactor as af on af.tableCode = p.userDefinedField10A2\n" +
											"    and af.stateCode = afstate.afstate\n" +
											"    and af.sexCode = ppb.sexCode\n" +
											"where 1=1\n" +
											"and ppb.policyNumber = '"+var_PolicyNumber+"'\n" +
											"and af.duration1 = p.benefitPeriodAge\n" +
											"and duration2 = case when ppb.userDefinedField3A = 0\n" +
											"    then 0\n" +
											"        else (select min(x) from (values (p.benefitPeriodAge), (p.benefitPeriodAge - ppb.issueAge + ppb.userDefinedField3A)) as value(x)\n" +
											"        ) end\n" +
											"order by ppb.policyNumber;";

									Statement statement7 = con.createStatement();
									ResultSet rs7 = null;
									try {
										rs7 = statement7.executeQuery(sql7);
									} catch (SQLException e) {
										e.printStackTrace();
									}
									System.out.println(rs7);

									while (rs7.next()) {


										String annuityFactor = rs7.getString("annuityFactor");
										java.text.DecimalFormat df = new java.text.DecimalFormat("#.00");
										Double annuityFactor1 = Double.parseDouble(annuityFactor) * listSurrenderValueFromDB10Plus.get(listSurrenderValueFromDB10Plus.size() - 1) / 1000;
										GUARANTEEDSURRENDERVALUEFromDB = df.format(annuityFactor1);


										String minAllowedPremiumFromDB1 = rs7.getString("MIN_ALLOW_INIT_PREMIUM");

										minAllowedPremiumFromDB =  df.format(Double.parseDouble(minAllowedPremiumFromDB1));

										//LOGGER.info(""+minAllowedPremiumFromDB);

										if (minAllowedPremiumFromDB1.equals("0.000")){

											LOGGER.info("minAllowedPremiumFromDB1 ------- "+minAllowedPremiumFromDB1);
											minAllowedPremiumFromDB = "0.00";
											LOGGER.info("minAllowedPremiumFromDB1 ------ "+minAllowedPremiumFromDB);
										}
										String maxAllowedPremiumFromDB1 = rs7.getString("MAX_ALLOW_INIT_PREMIUM");


										maxAllowedPremiumFromDB = df.format(Double.parseDouble(maxAllowedPremiumFromDB1));

									}

									statement7.close();

									String sql8 = "Select userDefinedField15S22 From \n" +
											"(Select Row_Number() Over (Order By projectedAcctValuG) As RowNum, * From dbo.PolicyPageValues where policynumber='"+var_PolicyNumber+"') t2\n" +
											"Where ATTAINEDAGE in (100) or DURATION in (10);\n";

									Statement statement8 = con.createStatement();
									ResultSet rs8 = null;
									try {
										rs8 = statement8.executeQuery(sql8);
									} catch (SQLException e) {
										e.printStackTrace();
									}
									System.out.println(rs8);

									while (rs8.next()) {


										String userDefinedField15S22Value = rs8.getString("userDefinedField15S22");
										if (userDefinedField15S22Value != null) {
											userDefinedField15S22Value = decimalFormat.format(Double.parseDouble(userDefinedField15S22Value));
											if (!userDefinedField15S22Value.contains(".")) {
												userDefinedField15S22Value = userDefinedField15S22Value + ".00";
											}
										}
										userDefinedField15S22FromDB.add(userDefinedField15S22Value);
				//				userDefinedField15S22FromDB
									}
									statement8.close();

									String sql9 = "select max(ATTAINEDAGE) age from dbo.PolicyPageValues where policyNumber='"+var_PolicyNumber+"';";

									Statement statement9 = con.createStatement();
									ResultSet rs9 = null;
									try {
										rs9 = statement9.executeQuery(sql9);
									} catch (SQLException e) {
										e.printStackTrace();
									}
									System.out.println(rs9);

									while (rs9.next()) {


										String age = rs9.getString("age");

										ageFromDB = age;

									}
									statement9.close();

									boolean isPage2Exists = false;

									for (String line : pdfFileInText) {

										if (line.equals("STATEMENT OF BENEFIT INFORMATION – CONTRACT SUMMARY")) {
											flag = true;
										}

										if (flag) {

											filteredText = filteredText + "\n" + line;

											filteredLines.add(line);

											LOGGER.info(line);

											if (line.contains("ANNUITANT(S) NAME(S)") && line.contains("ISSUE AGE(S)")) {

												String nextLine = pdfFileInText.get(lineNUmber + 1);

												String[] insuredParts = nextLine.trim().split(" ");

												if (insuredParts.length == 4) {

													primaryIssueAgeFromPDF = insuredParts[2];
													System.out.println("PRIMARY ISSUE AGE IS:-" + primaryIssueAgeFromPDF);

													primaryInsuredSexFromPDF = insuredParts[3];
													System.out.println("SEX IS:-" + primaryInsuredSexFromPDF);

													nameOfAnnuitantFromPDF = insuredParts[0] + " " + insuredParts[1];

													oldestIssueAge = Integer.parseInt(primaryIssueAgeFromPDF);

													System.out.println("nameOfAnnuitantFromPDF IS:-" + nameOfAnnuitantFromPDF);

													writeToCSV("POLICY NUMBER", var_PolicyNumber);
													writeToCSV("PAGE #: DATA TYPE", "PAGE 1: " + "PRIMARY ISSUE AGE");
													writeToCSV("EXPECTED DATA", primaryIssueAgeFromDB);
													writeToCSV("ACTUAL DATA ON PDF", primaryIssueAgeFromPDF);
													writeToCSV("RESULT", (primaryIssueAgeFromDB.equals(primaryIssueAgeFromPDF)) ? "PASS" : "FAIL");

													writeToCSV("POLICY NUMBER", var_PolicyNumber);
													writeToCSV("PAGE #: DATA TYPE", "PAGE 1: " + "PRIMARY INSURED NAME");
													writeToCSV("EXPECTED DATA", primaryInsuredNameFromDB.trim());
													writeToCSV("ACTUAL DATA ON PDF", nameOfAnnuitantFromPDF);
													writeToCSV("RESULT", (primaryInsuredNameFromDB.trim().equals(nameOfAnnuitantFromPDF)) ? "PASS" : "FAIL");

													writeToCSV("POLICY NUMBER", var_PolicyNumber);
													writeToCSV("PAGE #: DATA TYPE", "PAGE 1: " + "PRIMARY INSURED SEX");
													writeToCSV("EXPECTED DATA", primaryInsuredSexFromDB);
													writeToCSV("ACTUAL DATA ON PDF", primaryInsuredSexFromPDF);
													writeToCSV("RESULT", (primaryInsuredSexFromDB.equals(primaryInsuredSexFromPDF)) ? "PASS" : "FAIL");

												} else if (insuredParts.length == 2) {
													// For double entry of ANNUITANT

													nameOfAnnuitantFromPDF = nextLine.trim();
													jointInsuredNameFromPDF = pdfFileInText.get(lineNUmber + 2).trim();

													primaryIssueAgeFromPDF = pdfFileInText.get(lineNUmber + 3).trim();
													jointIssueAgeFromPDF = pdfFileInText.get(lineNUmber + 4).trim();

													primaryInsuredSexFromPDF = pdfFileInText.get(lineNUmber + 5).trim();
													jointInsuredSexFromPDF = pdfFileInText.get(lineNUmber + 6).trim();

										                               /* if (Integer.parseInt(primaryIssueAgeFromPDF) > Integer.parseInt(jointIssueAgeFromPDF)) {
										                                    oldestIssueAge = Integer.parseInt(primaryIssueAgeFromPDF);
										                                } else {
										                                    oldestIssueAge = Integer.parseInt(jointIssueAgeFromPDF);
										                                } */
													writeToCSV("POLICY NUMBER", var_PolicyNumber);
													writeToCSV("PAGE #: DATA TYPE", "PAGE 1: " + "PRIMARY ISSUE AGE");
													writeToCSV("EXPECTED DATA", primaryIssueAgeFromDB);
													writeToCSV("ACTUAL DATA ON PDF", primaryIssueAgeFromPDF);
													writeToCSV("RESULT", (primaryIssueAgeFromDB.equals(primaryIssueAgeFromPDF)) ? "PASS" : "FAIL");

													//                        Assert.assertEquals(jointIssueAgeFromPDF,jointInsuredAgeFromDB);
													writeToCSV("POLICY NUMBER", var_PolicyNumber);
													writeToCSV("PAGE #: DATA TYPE", "PAGE 1: " + "JOINT INSURED AGE");
													writeToCSV("EXPECTED DATA", jointInsuredAgeFromDB);
													writeToCSV("ACTUAL DATA ON PDF", jointIssueAgeFromPDF);
													writeToCSV("RESULT", (jointInsuredAgeFromDB.equals(jointIssueAgeFromPDF)) ? "PASS" : "FAIL");

													writeToCSV("POLICY NUMBER", var_PolicyNumber);
													writeToCSV("PAGE #: DATA TYPE", "PAGE 1: " + "PRIMARY INSURED NAME");
													writeToCSV("EXPECTED DATA", primaryInsuredNameFromDB.trim());
													writeToCSV("ACTUAL DATA ON PDF", nameOfAnnuitantFromPDF);
													writeToCSV("RESULT", (primaryInsuredNameFromDB.trim().equals(nameOfAnnuitantFromPDF)) ? "PASS" : "FAIL");

													writeToCSV("POLICY NUMBER", var_PolicyNumber);
													writeToCSV("PAGE #: DATA TYPE", "PAGE 1: " + "JOINT INSURED NAME");
													writeToCSV("EXPECTED DATA", jointInsuredNameFromDB.trim());
													writeToCSV("ACTUAL DATA ON PDF", jointInsuredNameFromPDF);
													writeToCSV("RESULT", (jointInsuredNameFromDB.trim().equals(jointInsuredNameFromPDF)) ? "PASS" : "FAIL");

													writeToCSV("POLICY NUMBER", var_PolicyNumber);
													writeToCSV("PAGE #: DATA TYPE", "PAGE 1: " + "PRIMARY INSURED SEX");
													writeToCSV("EXPECTED DATA", primaryInsuredSexFromDB);
													writeToCSV("ACTUAL DATA ON PDF", primaryInsuredSexFromPDF);
													writeToCSV("RESULT", (primaryInsuredSexFromDB.equals(primaryInsuredSexFromPDF)) ? "PASS" : "FAIL");

													//     Assert.assertEquals(jointInsuredSexFromPDF, jointInsuredSexFromDB);
													writeToCSV("POLICY NUMBER", var_PolicyNumber);
													writeToCSV("PAGE #: DATA TYPE", "PAGE 1: " + "JOINT INSURED SEX");
													writeToCSV("EXPECTED DATA", jointInsuredSexFromDB);
													writeToCSV("ACTUAL DATA ON PDF", jointInsuredSexFromPDF);
													writeToCSV("RESULT", (jointInsuredSexFromDB.equals(jointInsuredSexFromPDF)) ? "PASS" : "FAIL");

												}

											}

											if (line.contains("ISSUE") && line.contains("$")) {

												String previouseLine = pdfFileInText.get(lineNUmber - 1);

												if (previouseLine.trim().equals("VALUE")) {
													firstYearRecordLineNumber = lineNUmber + 1;
													tenthYearRecordLineNumber = firstYearRecordLineNumber + 9;
													System.out.println("Selected lines are...");

													for (int i = firstYearRecordLineNumber; i <= tenthYearRecordLineNumber; i++) {

														String selectedLine = pdfFileInText.get(i);
														String[] parts = selectedLine.split(" ");
														if (parts.length > 2) {

															String durationFromPDF = parts[0].trim();
															System.out.println("DURATION IS:-" + durationFromPDF);
															listDurationFromPDF.add(durationFromPDF);

															String yearFromPdf = parts[1].trim();
															System.out.println("Issue Year IS:-" + yearFromPdf);
															listIssueAgeFromPDF.add(yearFromPdf);

															String projectedAnnualPremium = parts[3];
															System.out.println("Projected Annual Premium Value From PDF IS:-" + projectedAnnualPremium);
															listProjectedAnnualPremiumsFromPDF.add(projectedAnnualPremium);

															String guaranteedAccountValue = parts[5];
															System.out.println("Guaranteed Account Value From PDF IS:-" + guaranteedAccountValue);
															listGuaranteedAccountValueFromPDF.add(guaranteedAccountValue);

															String guaranteedSurrenderValue = parts[7];
															System.out.println("Guaranteed Surrender Value From PDF IS:-" + guaranteedSurrenderValue);
															listSurrenderValueFromPDF.add(guaranteedSurrenderValue);

															String deathValue = parts[9];
															System.out.println("Guaranteed Death Value From PDF IS:-" + deathValue);
															listDeathValueFromPDF.add(deathValue);

														}
														System.out.println();
													}

													//     Assert.assertEquals(primaryIssueAgeFromPDF, primaryIssueAgeFromDB);

													for (int i = 0; i < listDurationFromPDF.size(); i++) {

														//     Assert.assertEquals(listDurationFromPDF.get(i), listDurationFromDB.get(i));
														writeToCSV("POLICY NUMBER", var_PolicyNumber);
														writeToCSV("PAGE #: DATA TYPE", "PAGE 1: " + "DURATION " + (i + 1));
														writeToCSV("EXPECTED DATA", listDurationFromDB.get(i));
														writeToCSV("ACTUAL DATA ON PDF", listDurationFromPDF.get(i));
														writeToCSV("RESULT", listDurationFromPDF.get(i).equals(listDurationFromDB.get(i)) ? "PASS" : "FAIL");

													}

													for (int i = 0; i < listIssueAgeFromPDF.size(); i++) {

														//     Assert.assertEquals(listIssueAgeFromPDF.get(i), listAttainedAgeFromDB.get(i));
														writeToCSV("POLICY NUMBER", var_PolicyNumber);
														writeToCSV("PAGE #: DATA TYPE", "PAGE 1: " + "ISSUE AGE " + (i + 1) + " YEAR");
														writeToCSV("EXPECTED DATA", listAttainedAgeFromDB.get(i));
														writeToCSV("ACTUAL DATA ON PDF", listIssueAgeFromPDF.get(i));
														writeToCSV("RESULT", listIssueAgeFromPDF.get(i).equals(listAttainedAgeFromDB.get(i)) ? "PASS" : "FAIL");

													}

													for (int i = 0; i < listProjectedAnnualPremiumsFromPDF.size(); i++) {

														//     Assert.assertEquals(listProjectedAnnualPremiumsFromPDF.get(i), listProjectedAnnualPremiumsFromDB.get(i));
														writeToCSV("POLICY NUMBER", var_PolicyNumber);
														writeToCSV("PAGE #: DATA TYPE", "PAGE 1: " + "PROJECTED ANNUAL PREMIUM " + (i + 1) + " YEAR");
														writeToCSV("EXPECTED DATA", "$" + listProjectedAnnualPremiumsFromDB.get(i));
														writeToCSV("ACTUAL DATA ON PDF", "$" + listProjectedAnnualPremiumsFromPDF.get(i));
														writeToCSV("RESULT", listProjectedAnnualPremiumsFromPDF.get(i).equals(listProjectedAnnualPremiumsFromDB.get(i)) ? "PASS" : "FAIL");

													}

													for (int i = 0; i < listGuaranteedAccountValueFromPDF.size(); i++) {

														//     Assert.assertEquals(listGuaranteedAccountValueFromPDF.get(i), listProjectedAccountValueFromDB.get(i));
														writeToCSV("POLICY NUMBER", var_PolicyNumber);
														writeToCSV("PAGE #: DATA TYPE", "PAGE 1: " + "PROJECTED ACCOUNT VALUE " + (i + 1) + " YEAR");
														writeToCSV("EXPECTED DATA", "$" + listProjectedAccountValueFromDB.get(i));
														writeToCSV("ACTUAL DATA ON PDF", "$" + listGuaranteedAccountValueFromPDF.get(i));
														writeToCSV("RESULT", listGuaranteedAccountValueFromPDF.get(i).equals(listProjectedAccountValueFromDB.get(i)) ? "PASS" : "FAIL");

													}

													for (int i = 0; i < listDeathValueFromPDF.size(); i++) {

														//            Assert.assertEquals(listDeathValueFromPDF.get(i), listDeathValueFromDB.get(i));
														writeToCSV("POLICY NUMBER", var_PolicyNumber);
														writeToCSV("PAGE #: DATA TYPE", "PAGE 1: " + "GUARANTEED DEATH VALUE " + (i + 1) + " YEAR");
														writeToCSV("EXPECTED DATA", "$" + listDeathValueFromDB.get(i));
														writeToCSV("ACTUAL DATA ON PDF", "$" + listDeathValueFromPDF.get(i));
														writeToCSV("RESULT", listDeathValueFromPDF.get(i).equals(listDeathValueFromDB.get(i)) ? "PASS" : "FAIL");

													}

													for (int i = 0; i < listSurrenderValueFromPDF.size(); i++) {

														//            Assert.assertEquals(listSurrenderValueFromPDF.get(i), listSurrenderValueFromDB.get(i));
														writeToCSV("POLICY NUMBER", var_PolicyNumber);
														writeToCSV("PAGE #: DATA TYPE", "PAGE 1: " + "GUARANTEED SURRENDER VALUE " + (i + 1) + " YEAR");
														writeToCSV("EXPECTED DATA", "$" + listSurrenderValueFromDB.get(i));
														writeToCSV("ACTUAL DATA ON PDF", "$" + listSurrenderValueFromPDF.get(i));
														writeToCSV("RESULT", listSurrenderValueFromPDF.get(i).equals(listSurrenderValueFromDB.get(i)) ? "PASS" : "FAIL");

													}

												}

											}

											if (line.contains("THIS POLICY MAY RESULT IN A LOSS IF SURRENDERED DURING THE PERIOD WHEN SURRENDER CHARGES APPLY")) {
												lastRecordLineNumber = lineNUmber - 1;
												if (primaryIssueAgeFromPDF != null && primaryIssueAgeFromPDF.length() > 0 && Integer.parseInt(primaryIssueAgeFromPDF) < 65) {

													for (int i = tenthYearRecordLineNumber + 1; i <= lastRecordLineNumber; i++) {

														String selectedLine = pdfFileInText.get(i);

														String[] parts = selectedLine.split(" ");
														if (parts.length > 2) {

															String durationFromPDF = parts[0].trim();
															System.out.println("DURATION IS:-" + durationFromPDF);
															listDurationFromPDF10Plus.add(durationFromPDF);

															String issueAgeFromPdf = parts[1].trim();
															System.out.println("Issue Year IS:-" + issueAgeFromPdf);
															listIssueAgeFromPDF10Plus.add(issueAgeFromPdf);

															String projectedAnnualPremium = parts[3];
															System.out.println("Projected Annual Premium Value From PDF IS:-" + projectedAnnualPremium);
															double projectAnnualPremiumFromPDF = Double.parseDouble(projectedAnnualPremium.replace(",", ""));
															listProjectedAnnualPremiumsFromPDF10Plus.add(projectAnnualPremiumFromPDF);

															String guaranteedAccountValue = parts[5];
															System.out.println("Guaranteed Account Value From PDF IS:-" + guaranteedAccountValue);
															double guaranteedAccountValueFromPDF = Double.parseDouble(guaranteedAccountValue.replace(",", ""));
															listGuaranteedAccountValueFromPDF10Plus.add(guaranteedAccountValueFromPDF);

															String guaranteedSurrenderValue = parts[7];
															System.out.println("Guaranteed Surrender Value From PDF IS:-" + guaranteedSurrenderValue);
															double guaranteedSurrenderValueFromPDF = Double.parseDouble(guaranteedSurrenderValue.replace(",", ""));
															listSurrenderValueFromPDF10Plus.add(guaranteedSurrenderValueFromPDF);

															String deathValue = parts[9];
															System.out.println("Guaranteed Death Value From PDF IS:-" + deathValue);
															double deathValueFromPDF = Double.parseDouble(deathValue.replace(",", ""));
															listDeathValueFromPDF10Plus.add(deathValueFromPDF);

															int index = listAttainedAgeFromDB10Plus.indexOf(issueAgeFromPdf);

															if (index >= 0) {


																//     Assert.assertEquals(issueAgeFromPdf, listAttainedAgeFromDB10Plus.get(index));
																writeToCSV("POLICY NUMBER", var_PolicyNumber);
																writeToCSV("PAGE #: DATA TYPE", "PAGE 1: " + "ISSUE AGE " + listAttainedAgeFromDB10Plus.get(index));
																writeToCSV("EXPECTED DATA", listAttainedAgeFromDB10Plus.get(index));
																writeToCSV("ACTUAL DATA ON PDF", issueAgeFromPdf);
																writeToCSV("RESULT", issueAgeFromPdf.equals(listAttainedAgeFromDB10Plus.get(index)) ? "PASS" : "FAIL");

																//     Assert.assertEquals(projectAnnualPremiumFromPDF, listProjectedAnnualPremiumsFromDB10Plus.get(index));
																writeToCSV("POLICY NUMBER", var_PolicyNumber);
																writeToCSV("PAGE #: DATA TYPE", "PAGE 1: " + "PROJECTED ANNUAL PREMIUM " + listAttainedAgeFromDB10Plus.get(index));
																writeToCSV("EXPECTED DATA", "$" + listProjectedAnnualPremiumsFromDB10Plus.get(index));
																writeToCSV("ACTUAL DATA ON PDF", "$" + projectAnnualPremiumFromPDF);
																writeToCSV("RESULT", projectAnnualPremiumFromPDF == listProjectedAnnualPremiumsFromDB10Plus.get(index) ? "PASS" : "FAIL");

																//	Assert.assertEquals(guaranteedAccountValueFromPDF, listProjectedAccountValueFromDB10Plus.get(index));
																writeToCSV("POLICY NUMBER", var_PolicyNumber);
																writeToCSV("PAGE #: DATA TYPE", "PAGE 1: " + "PROJECTED ACCOUNT VALUE " + listAttainedAgeFromDB10Plus.get(index));
																writeToCSV("EXPECTED DATA", "$" + listProjectedAccountValueFromDB10Plus.get(index));
																writeToCSV("ACTUAL DATA ON PDF", "$" + guaranteedAccountValueFromPDF);
																writeToCSV("RESULT", guaranteedAccountValueFromPDF == listProjectedAccountValueFromDB10Plus.get(index) ? "PASS" : "FAIL");

																//     Assert.assertEquals(guaranteedSurrenderValueFromPDF, listSurrenderValueFromDB10Plus.get(index));
																writeToCSV("POLICY NUMBER", var_PolicyNumber);
																writeToCSV("PAGE #: DATA TYPE", "PAGE 1: " + "GUARANTEED SURRENDER VALUE " + listAttainedAgeFromDB10Plus.get(index));
																writeToCSV("EXPECTED DATA", "$" + listSurrenderValueFromDB10Plus.get(index));
																writeToCSV("ACTUAL DATA ON PDF", "$" + guaranteedSurrenderValueFromPDF);
																writeToCSV("RESULT", guaranteedSurrenderValueFromPDF == listSurrenderValueFromDB10Plus.get(index) ? "PASS" : "FAIL");

																// Assert.assertEquals(deathValueFromPDF, listDeathValueFromDB10Plus.get(index));
																writeToCSV("POLICY NUMBER", var_PolicyNumber);
																writeToCSV("PAGE #: DATA TYPE", "PAGE 1: " + "GUARANTEED DEATH VALUE " + listAttainedAgeFromDB10Plus.get(index));
																writeToCSV("EXPECTED DATA", "$" + listDeathValueFromDB10Plus.get(index));
																writeToCSV("ACTUAL DATA ON PDF", "$" + deathValueFromPDF);
																writeToCSV("RESULT", deathValueFromPDF == listDeathValueFromDB10Plus.get(index) ? "PASS" : "FAIL");
															}
														}
													}

												}

												String maturityDateFromPDF = listDurationFromPDF10Plus.get(listDurationFromPDF10Plus.size() - 1);
												writeToCSV("POLICY NUMBER", var_PolicyNumber);
												writeToCSV("PAGE #: DATA TYPE", "PAGE 1: " + "MATURITY DATE");
												writeToCSV("EXPECTED DATA", fmtExpMatDateFromDB);
												writeToCSV("ACTUAL DATA ON PDF", maturityDateFromPDF);
												writeToCSV("RESULT", (fmtExpMatDateFromDB.equals(maturityDateFromPDF)) ? "PASS" : "FAIL");

											}

											if (line.contains("THE MINIMUM INITIAL PREMIUM ALLOWED FOR THIS PRODUCT IS")) {
												System.out.println("MINIMUM LINE IS   ____________ " + line);
												String minimumValue = line.split("\\$")[1];
												minimumPremiumValueFromPDF = minimumValue.replace(",", "");;


												System.out.println("MIN VALUE IS:-" + minAllowedPremiumFromDB);
												System.out.println("MIN VALUE IS:-" + minimumValue);

												//     Assert.assertEquals(minAllowedPremiumFromDB, minimumPremiumValueFromPDF);
										                           writeToCSV("POLICY NUMBER", var_PolicyNumber);
																	writeToCSV("PAGE #: DATA TYPE", "PAGE 1: " + "MINIMUM PREMIUM ALLOWED");
										                            writeToCSV("EXPECTED DATA", "$" + minAllowedPremiumFromDB);
										                            writeToCSV("ACTUAL DATA ON PDF", "$" + minimumPremiumValueFromPDF);
										                            writeToCSV("RESULT", minAllowedPremiumFromDB.equals(minimumPremiumValueFromPDF) ? "PASS" : "FAIL");

											}

											if (line.contains("THE MAXIMUM PREMIUM ALLOWED FOR THIS PRODUCT WITHOUT HOME OFFICE APPROVAL IS")) {
												String maxValue = line.split("\\$")[1];

												String lastChar = maxValue.substring(maxValue.length() - 1);
												if (lastChar.equals(".")) {
													maxValue = maxValue.substring(0, maxValue.length() - 1);
												}
												maximumPremiumValueFromPDF = maxValue.replace(",", "");


												System.out.println("MAX VALUE IS:-" + maxAllowedPremiumFromDB);
												System.out.println("MAX VALUE IS:-" + maximumPremiumValueFromPDF);

												//     Assert.assertEquals(maxAllowedPremiumFromDB, maximumPremiumValueFromPDF);
												 writeToCSV("POLICY NUMBER", var_PolicyNumber);
																	writeToCSV("PAGE #: DATA TYPE", "PAGE 1: " + "MAXIMUM PREMIUM ALLOWED");
										                            writeToCSV("EXPECTED DATA", "$" + maxAllowedPremiumFromDB);
										                            writeToCSV("ACTUAL DATA ON PDF", "$" + maximumPremiumValueFromPDF);
										                            writeToCSV("RESULT", maxAllowedPremiumFromDB.equals(maximumPremiumValueFromPDF) ? "PASS" : "FAIL");
											}

											if (line.contains("POLICY NUMBER:") && line.contains("CONTRACT SUMMARY DATE:")) {
												String[] parts = line.split(":");
												if (parts.length == 3) {
													policyDateFromPDF = parts[2].trim();
													System.out.println("POLICY DATE IS:-" + policyDateFromPDF);
													policyNumberFromPDF = parts[1].trim().split(" ")[0].trim();
													System.out.println("POLICY NUMBER IS:-" + policyNumberFromPDF);

													writeToCSV("POLICY NUMBER", var_PolicyNumber);
													writeToCSV("PAGE #: DATA TYPE", "PAGE 1: " + "POLICY NUMBER");
													writeToCSV("EXPECTED DATA", policynumberFromDB.trim());
													writeToCSV("ACTUAL DATA ON PDF", policyNumberFromPDF);
													writeToCSV("RESULT", (policynumberFromDB.trim().equals(policyNumberFromPDF)) ? "PASS" : "FAIL");

													writeToCSV("POLICY NUMBER", var_PolicyNumber);
													writeToCSV("PAGE #: DATA TYPE", "PAGE 1: " + "EFFECTIVE DATE");
													writeToCSV("EXPECTED DATA", formattedEffeDateFromDB);
													writeToCSV("ACTUAL DATA ON PDF", policyDateFromPDF);
													writeToCSV("RESULT", (formattedEffeDateFromDB.equals(policyDateFromPDF)) ? "PASS" : "FAIL");

												}

											}

											if (line.contains("PREMIUM:") && line.contains("$")) {
												String[] parts = line.trim().split(" ");
												if (parts.length == 2) {
													premiumFromPDF = parts[1];
													premiumFromPDF = premiumFromPDF.replace("$", "").replace(",", "");
													System.out.println("premiumFromPDF IS:-" + premiumFromPDF);

													writeToCSV("POLICY NUMBER", var_PolicyNumber);
													writeToCSV("PAGE #: DATA TYPE", "PAGE 1: " + "PREMIUM");
													writeToCSV("EXPECTED DATA", "$" + cashWithApplicationFromDB);
													writeToCSV("ACTUAL DATA ON PDF", "$" + premiumFromPDF);
													writeToCSV("RESULT", (cashWithApplicationFromDB.equals(premiumFromPDF)) ? "PASS" : "FAIL");

												}
											}

										                      /*  if (line.contains("BEFORE THE ANNUITY DATE, THE POLICY MAY BE SURRENDERED FOR ALL OR A PORTION OF ITS SURRENDER VALUE.")) {

										                            String expectedAnnuityText = "BEFORE THE ANNUITY DATE, THE POLICY MAY BE SURRENDERED FOR ALL OR A PORTION OF ITS SURRENDER VALUE. THE TOTAL SURRENDER VALUE EQUALS THE " +
										                                    "SUM OF THE SURRENDER VALUES FOR THE INTEREST CREDITING OPTIONS. THE SURRENDER VALUE FOR EACH OPTION IS THE GREATER OF THE OPTION'S " +
										                                    "VESTED ACCOUNT VALUE LESS SURRENDER CHARGES, OR THE OPTION'S MINIMUM GUARANTEED SURRENDER VALUE DEFINED IN THE POLICY.";
										                            String beforeAnnuityText = line + " " + pdfFileInText.get(lineNUmber + 1) + " " + pdfFileInText.get(lineNUmber + 2);

										                            if (isPage2Exists && VESTING_TABLE.equals("Y")) {
										                                writeToCSV("POLICY NUMBER", var_PolicyNumber);
																		writeToCSV("PAGE #: DATA TYPE", "PAGE 2: " + "STATIC TEXT");
										                                writeToCSV("EXPECTED DATA", expectedAnnuityText);
										                                writeToCSV("ACTUAL DATA ON PDF", beforeAnnuityText);
										                                writeToCSV("RESULT", (beforeAnnuityText.contains("VESTED")) ? "PASS" : "FAIL");

										                            }

										                        }

										                        if (line.contains("A SURRENDER CHARGE MAY BE IMPOSED ON WITHDRAWALS AND IN CALCULATING ANNUITY PAYMENTS.")) {

										                            String expectedAnnuityText = "A SURRENDER CHARGE MAY BE IMPOSED ON WITHDRAWALS AND IN CALCULATING ANNUITY PAYMENTS. THE SURRENDER CHARGE IS EQUAL TO THE SURRENDER " +
										                                    "FACTOR FOR THE POLICY YEAR (AS SHOWN BELOW) MULTIPLIED BY THE AMOUNT OF THE VESTED ACCOUNT VALUE IN EXCESS OF ANY APPLICABLE FREE AMOUNT.";
										                            String beforeAnnuityText = line + " " + pdfFileInText.get(lineNUmber + 1);

										                            if (isPage2Exists && VESTING_TABLE.equals("Y")) {
										                                writeToCSV("POLICY NUMBER", var_PolicyNumber);
																		writeToCSV("PAGE #: DATA TYPE", "PAGE 2: " + "STATIC TEXT");
										                                writeToCSV("EXPECTED DATA", expectedAnnuityText);
										                                writeToCSV("ACTUAL DATA ON PDF", beforeAnnuityText);
										                                writeToCSV("RESULT", (beforeAnnuityText.contains("VESTED")) ? "PASS" : "FAIL");

										                            }


										                        } */

											if (line.equals("SURRENDER")) {

												int startsLine = 0;
												int endLine = 0;
												String nextLine = pdfFileInText.get(lineNUmber + 1);
												if (nextLine.equals("FACTOR")) {

													String nextLine2 = pdfFileInText.get(lineNUmber + 2);

													if (nextLine2.startsWith("1 ")) {

														startsLine = lineNUmber + 2;

														if (startsLine > 0) {

															HashMap<String, String> listSurrFactors = new HashMap<>();

															SURRENDER_FACTORS:
															for (int i = startsLine; i >= startsLine; i++) {

																if (pdfFileInText.get(i).length() > 0 && Character.isDigit(pdfFileInText.get(i).charAt(0))) {

																	String sLine = pdfFileInText.get(i);
																	String parts[] = sLine.split(" ");
																	if (parts.length > 2) {
																		listSurrFactors.put(parts[0], parts[1]);
																		listSurrFactors.put(parts[2], parts[3]);
																	} else {
																		listSurrFactors.put(parts[0], parts[1]);
																	}

																} else {
																	endLine = i - 1;
																	break SURRENDER_FACTORS;
																}

															}

															for (int i = 0; i < listGeneralFundSurrChargeFromDB.size(); i++) {

																System.out.println("SURR CHARGE FACTOR:-" + listGeneralFundSurrChargeFromDB.get(i));
																String dbValue = listGeneralFundSurrChargeFromDB.get(i);
																dbValue = decimalFormat.format(Double.parseDouble(dbValue) * 100);
																dbValue = dbValue + "%";

																;//                                    Assert.assertEquals(listGeneralFundSurrChargeFromDB.get(0), listSurrFactors.get(""+(i+1)));
																writeToCSV("POLICY NUMBER", var_PolicyNumber);
																writeToCSV("PAGE #: DATA TYPE", "PAGE 2: " + "SURRENDER FACTOR");
																writeToCSV("EXPECTED DATA", "$" + dbValue);
																writeToCSV("ACTUAL DATA ON PDF", "$" + listSurrFactors.get("" + (i + 1)));
																writeToCSV("RESULT", dbValue.equals(listSurrFactors.get("" + (i + 1))) ? "PASS" : "FAIL");

															}


														}


													}
												}

											}
											if (line.equals("MARKET VALUE ADJUSTMENT") && isPage2Exists && mvaindicatorFromDB.equals("Y")) {

												writeToCSV("POLICY NUMBER", var_PolicyNumber);
												writeToCSV("PAGE #: DATA TYPE", "PAGE 2: " + "STATIC TEXT");
												writeToCSV("EXPECTED DATA", "MARKET VALUE ADJUSTMENT");
												writeToCSV("ACTUAL DATA ON PDF", "MARKET VALUE ADJUSTMENT");
												writeToCSV("RESULT", (filteredText.contains("MARKET VALUE ADJUSTMENT")) ? "PASS" : "FAIL");

												writeToCSV("POLICY NUMBER", var_PolicyNumber);
												writeToCSV("PAGE #: DATA TYPE", "PAGE 2: " + "STATIC TEXT");
												writeToCSV("EXPECTED DATA", "THE POLICY'S SURRENDER VALUE MAY BE AFFECTED BY A MARKET VALUE ADJUSTMENT.");
												writeToCSV("ACTUAL DATA ON PDF", "THE POLICY'S SURRENDER VALUE MAY BE AFFECTED BY A MARKET VALUE ADJUSTMENT.");
												writeToCSV("RESULT", (filteredText.contains("SURRENDER VALUE MAY BE AFFECTED BY A MARKET VALUE ADJUSTMENT.")) ? "PASS" : "FAIL");

												writeToCSV("POLICY NUMBER", var_PolicyNumber);
												writeToCSV("PAGE #: DATA TYPE", "PAGE 2: " + "STATIC TEXT");
												writeToCSV("EXPECTED DATA", "THE NET TOTAL OF ALL MARKET VALUE ADJUSTMENTS AND SURRENDER" );
												writeToCSV("ACTUAL DATA ON PDF", "THE NET TOTAL OF ALL MARKET VALUE ADJUSTMENTS AND SURRENDER" );
												writeToCSV("RESULT", (filteredText.contains("THE NET TOTAL OF ALL MARKET VALUE ADJUSTMENTS AND SURRENDER")) ? "PASS" : "FAIL");

												writeToCSV("POLICY NUMBER", var_PolicyNumber);
												writeToCSV("PAGE #: DATA TYPE", "PAGE 2: " + "STATIC TEXT");
												writeToCSV("EXPECTED DATA", "CHARGES WILL NOT REDUCE THE SURRENDER VALUE TO AN AMOUNT LESS THAN THE MINIMUM GUARANTEED SURRENDER VALUE DEFINED IN THE POLICY.  IF THE");
												writeToCSV("ACTUAL DATA ON PDF", "CHARGES WILL NOT REDUCE THE SURRENDER VALUE TO AN AMOUNT LESS THAN THE MINIMUM GUARANTEED SURRENDER VALUE DEFINED IN THE POLICY. IF THE");
												writeToCSV("RESULT", (filteredText.contains("CHARGES WILL NOT REDUCE THE SURRENDER VALUE TO AN AMOUNT LESS THAN THE MINIMUM GUARANTEED SURRENDER VALUE DEFINED IN THE POLICY. IF THE")) ? "PASS" : "FAIL");

												writeToCSV("POLICY NUMBER", var_PolicyNumber);
												writeToCSV("PAGE #: DATA TYPE", "PAGE 2: " + "STATIC TEXT");
												writeToCSV("EXPECTED DATA", "MARKET VALUE ADJUSTMENT RESULTS IN AN INCREASE TO THE SURRENDER VALUE,");
												writeToCSV("ACTUAL DATA ON PDF", "MARKET VALUE ADJUSTMENT RESULTS IN AN INCREASE TO THE SURRENDER VALUE,");
												writeToCSV("RESULT", (filteredText.contains("MARKET VALUE ADJUSTMENT RESULTS IN AN INCREASE TO THE SURRENDER VALUE,")) ? "PASS" : "FAIL");

												writeToCSV("POLICY NUMBER", var_PolicyNumber);
												writeToCSV("PAGE #: DATA TYPE", "PAGE 2: " + "STATIC TEXT");
												writeToCSV("EXPECTED DATA", "THE AMOUNT OF THE INCREASE WILL NOT BE GREATER THAN THE AMOUNT OF ");
												writeToCSV("ACTUAL DATA ON PDF", "THE AMOUNT OF THE INCREASE WILL NOT BE GREATER THAN THE AMOUNT OF ");
												writeToCSV("RESULT", (filteredText.contains("THE AMOUNT OF THE INCREASE WILL NOT BE GREATER THAN THE AMOUNT OF")) ? "PASS" : "FAIL");

												writeToCSV("POLICY NUMBER", var_PolicyNumber);
												writeToCSV("PAGE #: DATA TYPE", "PAGE 2: " + "STATIC TEXT");
												writeToCSV("EXPECTED DATA", "THE REMAINING SURRENDER CHARGE.");
												writeToCSV("ACTUAL DATA ON PDF", "THE REMAINING SURRENDER CHARGE.");
												writeToCSV("RESULT", (filteredText.contains("THE REMAINING SURRENDER CHARGE.")) ? "PASS" : "FAIL");

											}

											if (line.equals("ANNUITY DATE AGE")) {
												String nextLine = pdfFileInText.get(lineNUmber + 1);

												String parts[] = nextLine.split(" ");
												if (parts.length > 0) {

													String annuityDateFromPDFPage3 = parts[0];
													System.out.println("ANNUITY DATE AGE LINE IS :______" + annuityDateFromPDFPage3);

													//                        Assert.assertEquals(maturityDateFromDB, annuityDateFromPDFPage3);
													writeToCSV("POLICY NUMBER", var_PolicyNumber);
													writeToCSV("PAGE #: DATA TYPE", "PAGE 3: " + "ANNUITY DATE");
													writeToCSV("EXPECTED DATA", "" + fmtExpMatDateFromDB);
													writeToCSV("ACTUAL DATA ON PDF", "" + annuityDateFromPDFPage3);
													writeToCSV("RESULT", fmtExpMatDateFromDB.equals(annuityDateFromPDFPage3) ? "PASS" : "FAIL");


													String ageFromPDFPage3 = parts[1];
													System.out.println(" AGE LINE IS :______" + ageFromPDFPage3);

													//                        Assert.assertEquals(maturityDateFromDB, annuityDateFromPDFPage3);
													writeToCSV("POLICY NUMBER", var_PolicyNumber);
													writeToCSV("PAGE #: DATA TYPE", "PAGE 3: " + "AGE");
													writeToCSV("EXPECTED DATA", "" + ageFromDB);
													writeToCSV("ACTUAL DATA ON PDF", "" + ageFromPDFPage3);
													writeToCSV("RESULT", ageFromDB.equals(ageFromPDFPage3) ? "PASS" : "FAIL");



													String SURRENDERAMOUNTFromPDFPage3 = parts[3].replace(",", "");
													System.out.println("Guaranteed monthly income  :______" + parts[3]+"From DB"+SURRENDERAMOUNTFromDB);

													writeToCSV("POLICY NUMBER", var_PolicyNumber);
													writeToCSV("PAGE #: DATA TYPE", "PAGE 3: " + "Guaranteed monthly income");
													writeToCSV("EXPECTED DATA", "$" + SURRENDERAMOUNTFromDB);
													writeToCSV("ACTUAL DATA ON PDF", "$" + SURRENDERAMOUNTFromPDFPage3);
													writeToCSV("RESULT", (SURRENDERAMOUNTFromDB.equals(SURRENDERAMOUNTFromPDFPage3)) ? "PASS" : "FAIL");

													String GUARANTEEDSURRENDERVALUEFromPDFPage3 = parts[5].replace(",", "");
													System.out.println("Guaranteed monthly income for 10 years" + GUARANTEEDSURRENDERVALUEFromPDFPage3 +"From DB"+GUARANTEEDSURRENDERVALUEFromDB);

													writeToCSV("POLICY NUMBER", var_PolicyNumber);
													writeToCSV("PAGE #: DATA TYPE", "PAGE 3: " + "Guaranteed monthly income for 10 years");
													writeToCSV("EXPECTED DATA", "$" + GUARANTEEDSURRENDERVALUEFromDB);
													writeToCSV("ACTUAL DATA ON PDF", "$" + GUARANTEEDSURRENDERVALUEFromPDFPage3);
													writeToCSV("RESULT", (GUARANTEEDSURRENDERVALUEFromDB.equals(GUARANTEEDSURRENDERVALUEFromPDFPage3)) ? "PASS" : "FAIL");

												}



											}
											if (line.contains("YIELDS ON GROSS PREMIUM")) {

												String nextLine = pdfFileInText.get(lineNUmber + 2);

												String parts[] = nextLine.split(" ");

												if (parts.length > 0) {

													String userDefinedField15S22FromDB1 = userDefinedField15S22FromDB.get(0) + "%";


													String userDefinedField15S22PDFPage3 = parts[3];

													writeToCSV("POLICY NUMBER", var_PolicyNumber);
													writeToCSV("PAGE #: DATA TYPE", "PAGE 3: " + "Yield on gross premium PERCENTAGE  1");
													writeToCSV("EXPECTED DATA", "" + userDefinedField15S22FromDB1);
													writeToCSV("ACTUAL DATA ON PDF", "" + userDefinedField15S22PDFPage3);
													writeToCSV("RESULT", userDefinedField15S22FromDB1.equals(userDefinedField15S22PDFPage3) ? "PASS" : "FAIL");

												}
											}
											if (line.contains("YIELDS ON GROSS PREMIUM")) {

												String nextLine = pdfFileInText.get(lineNUmber + 3);

												String parts[] = nextLine.split(" ");

												if (parts.length > 0) {

													String userDefinedField15S22FromDB1 = userDefinedField15S22FromDB.get(1) + "%";


													String userDefinedField15S22PDFPage3 = parts[1];

													writeToCSV("POLICY NUMBER", var_PolicyNumber);
													writeToCSV("PAGE #: DATA TYPE", "PAGE 3: " + "Yield on gross premium PERCENTAGE  2");
													writeToCSV("EXPECTED DATA", "" + userDefinedField15S22FromDB1);
													writeToCSV("ACTUAL DATA ON PDF", "" + userDefinedField15S22PDFPage3);
													writeToCSV("RESULT", userDefinedField15S22FromDB1.equals(userDefinedField15S22PDFPage3) ? "PASS" : "FAIL");

												}
											}
											if (line.contains("AGENT/BROKER:")) {

												int startCount = lineNUmber + 2;
												String nextLine = pdfFileInText.get(startCount);
												while (!nextLine.contains("INSURER:")) {
													agentAddressLinesFromPDF.add(nextLine);
													startCount += 1;
													nextLine = pdfFileInText.get(startCount);

												}

												//

											}

											if (line.contains("INSURER:")) {
												int startCount = lineNUmber + 2;
												String nextLine = pdfFileInText.get(startCount);
												while (!nextLine.isEmpty()) {
													insurerAddressLinesFromPDF.add(nextLine);
													startCount += 1;
													nextLine = pdfFileInText.get(startCount);

												}


											}


										}

										if (line.equals("THE GUARANTEED VALUES SHOW THE MINIMUM GUARANTEED POLICY VALUES DESCRIBED IN THE POLICY, ASSUMING NO WITHDRAWALS OR SURRENDER.")) {
											String nextLine = pdfFileInText.get(lineNUmber + 1);
											if (nextLine.equalsIgnoreCase("PAGE 2")||nextLine.equalsIgnoreCase("PAGE 2:")) {
												isPage2Exists = true;
											}
										}

										if (line.equals("FIDELITY & GUARANTY LIFE INSURANCE COMPANY") && flag) {
											flag = false;
											break;
										}

										lineNUmber++;

									}

				//						if (isPage2Exists && mvaindicatorFromDB.equals("Y")) {
				//
				//							writeToCSV("POLICY NUMBER", var_PolicyNumber);
				//							writeToCSV("PAGE #: DATA TYPE", "PAGE 2: " + "STATIC TEXT");
				//							writeToCSV("EXPECTED DATA", "MARKET VALUE ADJUSTMENT");
				//							writeToCSV("ACTUAL DATA ON PDF", "MARKET VALUE ADJUSTMENT");
				//							writeToCSV("RESULT", (filteredText.contains("MARKET VALUE ADJUSTMENT")) ? "PASS" : "FAIL");
				//
				//							writeToCSV("POLICY NUMBER", var_PolicyNumber);
				//							writeToCSV("PAGE #: DATA TYPE", "PAGE 2: " + "STATIC TEXT");
				//							writeToCSV("EXPECTED DATA", "THE POLICY'S SURRENDER VALUE MAY BE AFFECTED BY A MARKET VALUE ADJUSTMENT.");
				//							writeToCSV("ACTUAL DATA ON PDF", "THE POLICY'S SURRENDER VALUE MAY BE AFFECTED BY A MARKET VALUE ADJUSTMENT.");
				//							writeToCSV("RESULT", (filteredText.contains("SURRENDER VALUE MAY BE AFFECTED BY A MARKET VALUE ADJUSTMENT.")) ? "PASS" : "FAIL");
				//
				//							writeToCSV("POLICY NUMBER", var_PolicyNumber);
				//							writeToCSV("PAGE #: DATA TYPE", "PAGE 2: " + "STATIC TEXT");
				//							writeToCSV("EXPECTED DATA", "THE NET TOTAL OF ALL MARKET VALUE ADJUSTMENTS AND SURRENDER" );
				//							writeToCSV("ACTUAL DATA ON PDF", "THE NET TOTAL OF ALL MARKET VALUE ADJUSTMENTS AND SURRENDER" );
				//							writeToCSV("RESULT", (filteredText.contains("THE NET TOTAL OF ALL MARKET VALUE ADJUSTMENTS AND SURRENDER")) ? "PASS" : "FAIL");
				//
				//							writeToCSV("POLICY NUMBER", var_PolicyNumber);
				//							writeToCSV("PAGE #: DATA TYPE", "PAGE 2: " + "STATIC TEXT");
				//							writeToCSV("EXPECTED DATA", "CHARGES WILL NOT REDUCE THE SURRENDER VALUE TO AN AMOUNT LESS THAN THE MINIMUM GUARANTEED SURRENDER VALUE DEFINED IN THE POLICY.  IF THE");
				//							writeToCSV("ACTUAL DATA ON PDF", "CHARGES WILL NOT REDUCE THE SURRENDER VALUE TO AN AMOUNT LESS THAN THE MINIMUM GUARANTEED SURRENDER VALUE DEFINED IN THE POLICY. IF THE");
				//							writeToCSV("RESULT", (filteredText.contains("CHARGES WILL NOT REDUCE THE SURRENDER VALUE TO AN AMOUNT LESS THAN THE MINIMUM GUARANTEED SURRENDER VALUE DEFINED IN THE POLICY. IF THE")) ? "PASS" : "FAIL");
				//
				//							writeToCSV("POLICY NUMBER", var_PolicyNumber);
				//							writeToCSV("PAGE #: DATA TYPE", "PAGE 2: " + "STATIC TEXT");
				//							writeToCSV("EXPECTED DATA", "MARKET VALUE ADJUSTMENT RESULTS IN AN INCREASE TO THE SURRENDER VALUE,");
				//							writeToCSV("ACTUAL DATA ON PDF", "MARKET VALUE ADJUSTMENT RESULTS IN AN INCREASE TO THE SURRENDER VALUE,");
				//							writeToCSV("RESULT", (filteredText.contains("MARKET VALUE ADJUSTMENT RESULTS IN AN INCREASE TO THE SURRENDER VALUE,")) ? "PASS" : "FAIL");
				//
				//							writeToCSV("POLICY NUMBER", var_PolicyNumber);
				//							writeToCSV("PAGE #: DATA TYPE", "PAGE 2: " + "STATIC TEXT");
				//							writeToCSV("EXPECTED DATA", "THE AMOUNT OF THE INCREASE WILL NOT BE GREATER THAN THE AMOUNT OF ");
				//							writeToCSV("ACTUAL DATA ON PDF", "THE AMOUNT OF THE INCREASE WILL NOT BE GREATER THAN THE AMOUNT OF ");
				//							writeToCSV("RESULT", (filteredText.contains("THE AMOUNT OF THE INCREASE WILL NOT BE GREATER THAN THE AMOUNT OF")) ? "PASS" : "FAIL");
				//
				//							writeToCSV("POLICY NUMBER", var_PolicyNumber);
				//							writeToCSV("PAGE #: DATA TYPE", "PAGE 2: " + "STATIC TEXT");
				//							writeToCSV("EXPECTED DATA", "THE REMAINING SURRENDER CHARGE.");
				//							writeToCSV("ACTUAL DATA ON PDF", "THE REMAINING SURRENDER CHARGE.");
				//							writeToCSV("RESULT", (filteredText.contains("THE REMAINING SURRENDER CHARGE.")) ? "PASS" : "FAIL");
				//
				//						}

									String sql5 = "select AGENTFIRSTNAME, agentlastname, agentAddressLine1,agentAddressLine2,agentAddressCity,agentStateCode,agentZipCode,companyLongDesc,companyAddressLine1," +
											"companyAddressLine2,companyCityCode,companyStateCode,companyZipCode, stateOfIssue from dbo.PolicyPageContract " +
											"where policynumber='" + var_PolicyNumber + "';";

									Statement statement5 = con.createStatement();
									ResultSet rs5 = null;
									rs5 = statement5.executeQuery(sql5);

									while (rs5.next()) {

										companyLongDescFromDB = rs5.getString("COMPANYLONGDESC").toUpperCase().trim();
										companyAddressLine1FromDB = rs5.getString("COMPANYADDRESSLINE1").toUpperCase().trim();
										companyAddressLine2FromDB = rs5.getString("COMPANYADDRESSLINE2").toUpperCase().trim();
										companyCityCodeFromDB = rs5.getString("COMPANYCITYCODE").toUpperCase().trim();
										companyStateCodeFromDB = rs5.getString("COMPANYSTATECODE").toUpperCase().trim();
										companyZipCodeFromDB = rs5.getString("COMPANYZIPCODE").toUpperCase().trim();

										agentFirstNameFromDB = rs5.getString("AGENTFIRSTNAME").toUpperCase().trim();
										agentLastNameFromDB = rs5.getString("agentlastname").toUpperCase().trim();
										agentAddressCityFromDB = rs5.getString("AGENTADDRESSCITY").toUpperCase().trim();
										agentAddressLine1FromDB = rs5.getString("agentaddressline1").toUpperCase().trim();
										agentAddressLine2FromDB = rs5.getString("AGENTaddressline2").toUpperCase().trim();
										agentStateCodeFromDB = rs5.getString("AGENTSTATECODE").toUpperCase().trim();
										agentZipCodeFromDB = rs5.getString("AGENTZIPCODE").toUpperCase().trim();

										//stateOfIssueFromDB = rs5.getString("stateOfIssue").toUpperCase().trim();


									}
									statement5.close();
									System.out.println("INSURER ADRESS LINES COUNT IS =" + insurerAddressLinesFromPDF.size());
									if (insurerAddressLinesFromPDF.size() == 3) {
										companyLongDescFromPDF = insurerAddressLinesFromPDF.get(0).trim();

										companyAddressLine1FromPDF = insurerAddressLinesFromPDF.get(1).trim();

										String nextLine3 = insurerAddressLinesFromPDF.get(2).trim();

										String parts[] = nextLine3.split("\\s*(=>|,|\\s)\\s*");
										companyCityCodeFromPDF = parts[0];
										companyStateCodeFromPDF = parts[1];
										companyZipCodeFromPDF = parts[2];

									}

									if (insurerAddressLinesFromPDF.size() == 4) {
										companyLongDescFromPDF = insurerAddressLinesFromPDF.get(0).trim();

										companyAddressLine1FromPDF = insurerAddressLinesFromPDF.get(1).trim();
										companyAddressLine2FromPDF = insurerAddressLinesFromPDF.get(2).trim();

										String nextLine3 = insurerAddressLinesFromPDF.get(3).trim();

										String parts[] = nextLine3.split("\\s*(=>|,|\\s)\\s*");
										companyCityCodeFromPDF = parts[0];
										companyStateCodeFromPDF = parts[1];
										companyZipCodeFromPDF = parts[2];

									}

									if (agentAddressLinesFromPDF.size() == 3) {
										String nextLine1 = agentAddressLinesFromPDF.get(0).trim();
										agentFirstNameFromPDF = nextLine1.split(" ")[0];
										agentLastNameFromPDF = nextLine1.split(" ")[1];
										agentAddressLine1FromPDF = agentAddressLinesFromPDF.get(1).trim();

										String nextLine3 = agentAddressLinesFromPDF.get(2).trim();

										String parts[] = nextLine3.split(",");
										agentAddressCityFromPDF = parts[0];
										agentStateCodeFromPDF = parts[1].trim();
										String part[] =agentStateCodeFromPDF.split(" ");
										agentStateCodeFromPDF = part[0].trim();
										agentZipCodeFromPDF = part[1].trim();

									}

									if (agentAddressLinesFromPDF.size() == 4) {
										String nextLine1 = agentAddressLinesFromPDF.get(0).trim();
										agentFirstNameFromPDF = nextLine1.split(" ")[0];
										agentLastNameFromPDF = nextLine1.split(" ")[1];
										agentAddressLine1FromPDF = agentAddressLinesFromPDF.get(1).trim();
										agentAddressLine2FromPDF = agentAddressLinesFromPDF.get(2).trim();

										String nextLine3 = agentAddressLinesFromPDF.get(3).trim();

										String parts[] = nextLine3.split(",");
										agentAddressCityFromPDF = parts[0];
										agentStateCodeFromPDF = parts[1].trim();
										String part[] =agentStateCodeFromPDF.split(" ");
										agentStateCodeFromPDF = part[0].trim();
										agentZipCodeFromPDF = part[1].trim();

									}

									System.out.println("Company ADRESS LINES COUNT IS =" + agentAddressLinesFromPDF.size());
									//	 if ( stateOfIssueFromDB.equals("MD") || stateOfIssueFromDB.equals("NV") || stateOfIssueFromDB.equals("GA") || stateOfIssueFromDB.equals("WI") || stateOfIssueFromDB.equals("WA")) {

									writeToCSV("POLICY NUMBER", var_PolicyNumber);
									writeToCSV("PAGE #: DATA TYPE", "PAGE 3: " + "COMPANY_LONG_DESC");
									writeToCSV("EXPECTED DATA", companyLongDescFromDB);
									writeToCSV("ACTUAL DATA ON PDF", companyLongDescFromPDF);
									writeToCSV("RESULT", (companyLongDescFromDB.equals(companyLongDescFromPDF)) ? "PASS" : "FAIL");

									writeToCSV("POLICY NUMBER", var_PolicyNumber);
									writeToCSV("PAGE #: DATA TYPE", "PAGE 3: " + "COMPANY_ADDRESS_LINE_1");
									writeToCSV("EXPECTED DATA", companyAddressLine1FromDB);
									writeToCSV("ACTUAL DATA ON PDF", companyAddressLine1FromPDF);
									writeToCSV("RESULT", (companyAddressLine1FromDB.equals(companyAddressLine1FromPDF)) ? "PASS" : "FAIL");

									writeToCSV("POLICY NUMBER", var_PolicyNumber);
									writeToCSV("PAGE #: DATA TYPE", "PAGE 3: " + "COMPANY_ADDRESS_LINE_2");
									writeToCSV("EXPECTED DATA", companyAddressLine2FromDB);
									writeToCSV("ACTUAL DATA ON PDF", companyAddressLine2FromPDF);
									writeToCSV("RESULT", (companyAddressLine2FromDB.equals(companyAddressLine2FromPDF)) ? "PASS" : "FAIL");

									writeToCSV("POLICY NUMBER", var_PolicyNumber);
									writeToCSV("PAGE #: DATA TYPE", "PAGE 3: " + "COMPANY_CITY_CODE");
									writeToCSV("EXPECTED DATA", companyCityCodeFromDB);
									writeToCSV("ACTUAL DATA ON PDF", companyCityCodeFromPDF);
									writeToCSV("RESULT", (companyCityCodeFromDB.equals(companyCityCodeFromPDF)) ? "PASS" : "FAIL");

									writeToCSV("POLICY NUMBER", var_PolicyNumber);
									writeToCSV("PAGE #: DATA TYPE", "PAGE 3: " + "COMPANY_STATE_CODE");
									writeToCSV("EXPECTED DATA", companyStateCodeFromDB);
									writeToCSV("ACTUAL DATA ON PDF", companyStateCodeFromPDF);
									writeToCSV("RESULT", (companyStateCodeFromDB.equals(companyStateCodeFromPDF)) ? "PASS" : "FAIL");

									writeToCSV("POLICY NUMBER", var_PolicyNumber);
									writeToCSV("PAGE #: DATA TYPE", "PAGE 3: " + "COMPANY_ZIP_CODE");
									writeToCSV("EXPECTED DATA", companyZipCodeFromDB);
									writeToCSV("ACTUAL DATA ON PDF", companyZipCodeFromPDF);
									writeToCSV("RESULT", (companyZipCodeFromDB.equals(companyZipCodeFromPDF)) ? "PASS" : "FAIL");

									writeToCSV("POLICY NUMBER", var_PolicyNumber);
									writeToCSV("PAGE #: DATA TYPE", "PAGE 3: " + "AGENT_FIRST_NAME");
									writeToCSV("EXPECTED DATA", agentFirstNameFromDB);
									writeToCSV("ACTUAL DATA ON PDF", agentFirstNameFromPDF);
									writeToCSV("RESULT", (agentFirstNameFromDB.equals(agentFirstNameFromPDF)) ? "PASS" : "FAIL");

									writeToCSV("POLICY NUMBER", var_PolicyNumber);
									writeToCSV("PAGE #: DATA TYPE", "PAGE 3: " + "AGENT_LAST_NAME");
									writeToCSV("EXPECTED DATA", agentLastNameFromDB);
									writeToCSV("ACTUAL DATA ON PDF", agentLastNameFromPDF);
									writeToCSV("RESULT", (agentLastNameFromDB.equals(agentLastNameFromPDF)) ? "PASS" : "FAIL");

									writeToCSV("POLICY NUMBER", var_PolicyNumber);
									writeToCSV("PAGE #: DATA TYPE", "PAGE 3: " + "AGENT_ADDRESS_CITY");
									writeToCSV("EXPECTED DATA", agentAddressCityFromDB);
									writeToCSV("ACTUAL DATA ON PDF", agentAddressCityFromPDF);
									writeToCSV("RESULT", (agentAddressCityFromDB.equals(agentAddressCityFromPDF)) ? "PASS" : "FAIL");

									writeToCSV("POLICY NUMBER", var_PolicyNumber);
									writeToCSV("PAGE #: DATA TYPE", "PAGE 3: " + "agent_address_line_1");
									writeToCSV("EXPECTED DATA", agentAddressLine1FromDB);
									writeToCSV("ACTUAL DATA ON PDF", agentAddressLine1FromPDF);
									writeToCSV("RESULT", (agentAddressLine1FromDB.equals(agentAddressLine1FromPDF)) ? "PASS" : "FAIL");

									writeToCSV("POLICY NUMBER", var_PolicyNumber);
									writeToCSV("PAGE #: DATA TYPE", "PAGE 3: " + "AGENT_address_line_2");
									writeToCSV("EXPECTED DATA", agentAddressLine2FromDB);
									writeToCSV("ACTUAL DATA ON PDF", agentAddressLine2FromPDF);
									writeToCSV("RESULT", (agentAddressLine2FromDB.equals(agentAddressLine2FromPDF)) ? "PASS" : "FAIL");

									writeToCSV("POLICY NUMBER", var_PolicyNumber);
									writeToCSV("PAGE #: DATA TYPE", "PAGE 3: " + "AGENT_STATE_CODE");
									writeToCSV("EXPECTED DATA", agentStateCodeFromDB);
									writeToCSV("ACTUAL DATA ON PDF", agentStateCodeFromPDF);
									writeToCSV("RESULT", (agentStateCodeFromDB.equals(agentStateCodeFromPDF)) ? "PASS" : "FAIL");

									writeToCSV("POLICY NUMBER", var_PolicyNumber);
									writeToCSV("PAGE #: DATA TYPE", "PAGE 3: " + "AGENT_ZIP_CODE");
									writeToCSV("EXPECTED DATA", agentZipCodeFromDB);
									writeToCSV("ACTUAL DATA ON PDF", agentZipCodeFromPDF);
									writeToCSV("RESULT", (agentZipCodeFromDB.equals(agentZipCodeFromPDF)) ? "PASS" : "FAIL");
								}
								/*else if (!var_PolicyNumber.equals(policynumberFromDB)) {
									writeToCSV("POLICY NUMBER", var_PolicyNumber);
													writeToCSV("PAGE #: DATA TYPE", "POLICY PRESENCE IN DATABASE");
										                        //	writeToCSV("ISSUE STATE", "issue State: " + issueState);
										                        writeToCSV("EXPECTED DATA", "POLICY NOT FOUND");
										                        writeToCSV("ACTUAL DATA ON PDF", var_PolicyNumber  + " NOT FOUND");
										                        writeToCSV("RESULT", "SKIP");
								 }

								 else  {
													      writeToCSV("PAGE #: DATA TYPE", "STATIC TEXT - STATEMENT OF BENEFIT for NON-SBI State");
										                        //	writeToCSV("ISSUE STATE", "issue State: " + issueState);
										                        writeToCSV("EXPECTED DATA", "No STATEMENT OF BENEFIT text");
										                        writeToCSV("ACTUAL DATA ON PDF", "No STATEMENT OF BENEFIT  FOR ISSUE STATE " +  stateOfIssueFromDB);
										                        writeToCSV("RESULT", (filteredText.contains("STATEMENT OF BENEFIT")) ? "FAIL" : "PASS");
								 }*/

								con.close();
							}


						
				 
				 
		 
		 ;} 
		 catch(Exception e) { PrintVal("Renderer Script Exception : "+e.toString()); } 
		 PrintVal("Executed Step = CALLRENDERERSCRIPT,BASECOMPAREBASETBLTOPDF"); 
        WAIT.$(2,"TGTYPESCREENREG");

        WAIT.$(2,"TGTYPESCREENREG");

		var_Count = var_Count + ( 1 );
                 LOGGER.info("Executed Step = INCREASEVAR,var_Count,1,TGTYPESCREENREG");
    	}
          LOGGER.info("Executed Step = WEND,TGTYPESCREENREG");
    	resetAttemps();

    }


    @Test
    public void comparingbaselinepdftabtoactualpdf() {

        // No need to declare / print method name here, handled by TestNg listener
        // declare("comparingbaselinepdftabtoactualpdf");

		int var_Count = 0;
                 LOGGER.info("Executed Step = VAR,Integer,var_Count,0,TGTYPESCREENREG");
		com.jayway.jsonpath.ReadContext var_CSVData = getJsonData("C:/GIAS_Automation/Agile FandG102/InputData/BaselinedPdf/Pdfslocation.csv");
                 LOGGER.info("Executed Step = VAR,Json,var_CSVData,C:/GIAS_Automation/Agile FandG102/InputData/BaselinedPdf/Pdfslocation.csv,TGTYPESCREENREG");
		String var_baselinePdfPath = "null";
                 LOGGER.info("Executed Step = VAR,String,var_baselinePdfPath,null,TGTYPESCREENREG");
		String var_ActualPdfPath = "null";
                 LOGGER.info("Executed Step = VAR,String,var_ActualPdfPath,null,TGTYPESCREENREG");
        LOGGER.info("Executed Step = REPEAT IF");
    	while (check(var_Count,"<",1,"TGTYPESCREENREG")) {
          LOGGER.info("Executed Step = WHILE,var_Count,<,1,TGTYPESCREENREG");
		var_baselinePdfPath = getJsonData(var_CSVData , "$.records["+var_Count+"].BaselinePDFPath");
                 LOGGER.info("Executed Step = STORE,var_baselinePdfPath,var_CSVData,$.records[{var_Count}].BaselinePDFPath,TGTYPESCREENREG");
		var_ActualPdfPath = getJsonData(var_CSVData , "$.records["+var_Count+"].ActualPdfPath");
                 LOGGER.info("Executed Step = STORE,var_ActualPdfPath,var_CSVData,$.records[{var_Count}].ActualPdfPath,TGTYPESCREENREG");
		try { 
		         int baselinepdfstartpagenumber = WebTestUtility.getPDFPageNumber(var_baselinePdfPath);
		        int ActualPdfPathstartpagenumber = WebTestUtility.getPDFPageNumber(var_ActualPdfPath);

		        int startPagebaseline =  baselinepdfstartpagenumber;
		        int endPagebaseline = baselinepdfstartpagenumber + 3;


		        Timestamp timestamp = new Timestamp(System.currentTimeMillis());
		        String BaselinePDFPathExport = "C:/GIAS_Automation/Agile FandG102/InputData/BaselinedPdf"+"/BaselinePDF"+timestamp.getTime()+".pdf";

		        Splitter splitter = new Splitter();
		        splitter.setStartPage(startPagebaseline);
		        splitter.setEndPage(endPagebaseline);
		        splitter.setSplitAtPage(endPagebaseline);
		        try (final PDDocument document = PDDocument.load(new File(var_baselinePdfPath));) {
		            List<PDDocument> pages = splitter.split(document);

		            PDDocument pd = null;
		            try {
		                pd = pages.get(0);
		                pd.save(BaselinePDFPathExport);
		            } finally {
		                if (pd != null) {
		                    pd.close();
		                }
		            }
		        }catch (IOException ex) {

		            System.out.println(ex.getLocalizedMessage());

		        }

		        int startPageActualPdf =  ActualPdfPathstartpagenumber;
		        int endPageActualPdf = ActualPdfPathstartpagenumber + 3;

		        String ActualPdfPathExport = "C:/GIAS_Automation/Agile FandG102/InputData/BaselinedPdf"+"/ActualPdf"+timestamp.getTime()+".pdf";

		        Splitter splitter1 = new Splitter();
		        splitter1.setStartPage(startPageActualPdf);
		        splitter1.setEndPage(endPageActualPdf);
		        splitter1.setSplitAtPage(endPageActualPdf);
		        try (final PDDocument document2 = PDDocument.load(new File(var_ActualPdfPath));) {
		            List<PDDocument> pages = splitter1.split(document2);

		            PDDocument pd = null;
		            try {
		                pd = pages.get(0);
		                pd.save(ActualPdfPathExport);
		            } finally {
		                if (pd != null) {
		                    pd.close();
		                }
		            }
		        }catch (IOException ex) {

		            System.out.println(ex.getLocalizedMessage());

		        }

		        String ActualPdfFile = "";
		        String baselinePdfFile = "";

		        ActualPdfFile = ActualPdfPathExport;
		        baselinePdfFile = BaselinePDFPathExport;


		        String ResultpdfFile = "C:/GIAS_Automation/Agile FandG102/OutputData/outputPdf"+timestamp.getTime();
		        String ignoreFilename = "C:/GIAS_Automation/Agile FandG102/InputData/ignorepdfContent.conf";


		        boolean isEqual =  new de.redsix.pdfcompare.PdfComparator(ActualPdfFile, baselinePdfFile).withIgnore(ignoreFilename).compare().writeTo(ResultpdfFile);
		        System.out.print("pdf output:---"+ isEqual);
		  if (isEqual) {
		            Assert.assertTrue(true,"ActualPdf and baselinePdf  are same.");
		        }else{
		            Assert.assertTrue(false,"ActualPdf and baselinePdf are not same.");

		        }
		 
		 ;} 
		 catch(Exception e) { PrintVal("Renderer Script Exception : "+e.toString()); } 
		 PrintVal("Executed Step = CALLRENDERERSCRIPT,PDFTOPDFCOMPAREWITHLAYOUT"); 
		var_Count = var_Count + ( 1 );
                 LOGGER.info("Executed Step = INCREASEVAR,var_Count,1,TGTYPESCREENREG");
    	}
          LOGGER.info("Executed Step = WEND,TGTYPESCREENREG");
    	resetAttemps();

    }


    @Test
    public void tc01createnewbusinesspolicy() {

        // No need to declare / print method name here, handled by TestNg listener
        // declare("tc01createnewbusinesspolicy");

        CALL.$("LoginToGIASFandG","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_USERNAME","outerForm:usernameTGWEBCOMMAIdTGWEBCOMMA0","x5990","FALSE","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_PASSWORD","outerForm:passwordTGWEBCOMMAIdTGWEBCOMMA0","Big1Blue2!","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_LOGIN","outerForm:loginButtonTGWEBCOMMAIdTGWEBCOMMA0","TGTYPESCREENREG");

		int var_Count = 0;
                 LOGGER.info("Executed Step = VAR,Integer,var_Count,0,TGTYPESCREENREG");
        LOGGER.info("Executed Step = REPEAT IF");
    	while (check(var_Count,"<",1,"TGTYPESCREENREG")) {
          LOGGER.info("Executed Step = WHILE,var_Count,<,1,TGTYPESCREENREG");
		com.jayway.jsonpath.ReadContext var_CSVData = getJsonData("C:/GIAS_Automation/FandG Regression/InputData/FandGReg_CreateAPolicy.csv");
                 LOGGER.info("Executed Step = VAR,Json,var_CSVData,C:/GIAS_Automation/FandG Regression/InputData/FandGReg_CreateAPolicy.csv,TGTYPESCREENREG");
		String var_effectiveDate = "null";
                 LOGGER.info("Executed Step = VAR,String,var_effectiveDate,null,TGTYPESCREENREG");
		var_effectiveDate = getJsonData(var_CSVData , "$.records["+var_Count+"].EffectiveDate");
                 LOGGER.info("Executed Step = STORE,var_effectiveDate,var_CSVData,$.records[{var_Count}].EffectiveDate,TGTYPESCREENREG");
        CALL.$("AddNewBusinessPolicy","TGTYPESCREENREG");

        HOVER.$("ELE_MENU_NEWBUSINESS","//td[text()='New Business']","TGTYPESCREENREG");

        TAP.$("ELE_SUBMEN_APPLICATIONENTRYUPDATE","//td[text()='Application Entry/Update']","TGTYPESCREENREG");

        TAP.$("ELE_RNDBTN_ADD","//img[@id='outerForm:gnAddImage']","TGTYPESCREENREG");

		String var_PolicyNumber;
                 LOGGER.info("Executed Step = VAR,String,var_PolicyNumber,TGTYPESCREENREG");
		var_PolicyNumber = getJsonData(var_CSVData , "$.records["+var_Count+"].PolicyNumber");
                 LOGGER.info("Executed Step = STORE,var_PolicyNumber,var_CSVData,$.records[{var_Count}].PolicyNumber,TGTYPESCREENREG");
		String var_FirstName;
                 LOGGER.info("Executed Step = VAR,String,var_FirstName,TGTYPESCREENREG");
		var_FirstName = getJsonData(var_CSVData , "$.records["+var_Count+"].FirstName");
                 LOGGER.info("Executed Step = STORE,var_FirstName,var_CSVData,$.records[{var_Count}].FirstName,TGTYPESCREENREG");
		String var_LastName;
                 LOGGER.info("Executed Step = VAR,String,var_LastName,TGTYPESCREENREG");
		var_LastName = getJsonData(var_CSVData , "$.records["+var_Count+"].LastName");
                 LOGGER.info("Executed Step = STORE,var_LastName,var_CSVData,$.records[{var_Count}].LastName,TGTYPESCREENREG");
		String var_IDNumber;
                 LOGGER.info("Executed Step = VAR,String,var_IDNumber,TGTYPESCREENREG");
		var_IDNumber = getJsonData(var_CSVData , "$.records["+var_Count+"].IDNumber");
                 LOGGER.info("Executed Step = STORE,var_IDNumber,var_CSVData,$.records[{var_Count}].IDNumber,TGTYPESCREENREG");
		String var_ClientSearchName;
                 LOGGER.info("Executed Step = VAR,String,var_ClientSearchName,TGTYPESCREENREG");
		var_ClientSearchName = getJsonData(var_CSVData , "$.records["+var_Count+"].ClientSearchName");
                 LOGGER.info("Executed Step = STORE,var_ClientSearchName,var_CSVData,$.records[{var_Count}].ClientSearchName,TGTYPESCREENREG");
		String var_CashWithApp = "20000";
                 LOGGER.info("Executed Step = VAR,String,var_CashWithApp,20000,TGTYPESCREENREG");
        TYPE.$("ELE_TXTBOX_POLICYNUMBER","//input[@type='text'][@name='outerForm:policyNumber']",var_PolicyNumber,"FALSE","TGTYPESCREENREG");

        SELECT.$("ELE_DRPDWN_CURRENCYCODE","//select[@id='outerForm:currencyCode']","DOLLAR [US]","TGTYPESCREENREG");

        SELECT.$("ELE_DRPDWN_STATECOUNTRY","//select[@name='outerForm:countryAndStateCode']	","MARYLAND","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_CASHWITHAPPLICATION","//input[@type='text'][@name='outerForm:cashWithApplication']",var_CashWithApp,"FALSE","TGTYPESCREENREG");

        SELECT.$("ELE_DRPDWN_PAYMENTFREQUENCY","//select[@name='outerForm:paymentMode']","Annual","TGTYPESCREENREG");

        SELECT.$("ELE_DRPDWN_PAYMENTMETHOD","//select[@name='outerForm:paymentCode']","No Notice","TGTYPESCREENREG");

        SELECT.$("ELE_DRPDWN_TAXQUALIFIEDDESCRIPTION","//select[@name='outerForm:taxQualifiedCode']","TRADITIONAL IRA","TGTYPESCREENREG");

        SELECT.$("ELE_DRPDWN_TAXWITHHOLDING","//select[@name='outerForm:taxWithholdingCode']","No Withholding","TGTYPESCREENREG");

        TAP.$("ELE_LINK_SELECTCLIENT","//span[text()='Select Client']","TGTYPESCREENREG");

        TAP.$("ELE_RNDBTN_ADD","//img[@id='outerForm:gnAddImage']","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_FIRSTNAME","//input[@type='text'][@name='outerForm:nameFirst']",var_FirstName,"FALSE","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_LASTNAME","//input[@type='text'][@name='outerForm:nameLast']",var_LastName,"FALSE","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_CLIENTDOB","//input[@type='text'][@name='outerForm:dateOfBirth2']","01/01/1980","FALSE","TGTYPESCREENREG");

        SELECT.$("ELE_DRPDWN_CLIENTGENDER","//select[@name='outerForm:sexCode2']","Male","TGTYPESCREENREG");

        SELECT.$("ELE_DRPDWN_IDTYPE","//select[@name='outerForm:taxIdenUsag2']","Social Security Number","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_IDNUMBER","//input[@type='text'][@name='outerForm:identificationNumber2']",var_IDNumber,"FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SUBMIT","//input[@type='submit'][@name='outerForm:Submit']","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_ADDRESSLINE1","//input[@type='text'][@name='outerForm:addressLineOne']","2000 WADE HAMPTON BLVD","FALSE","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_CITY","//input[@type='text'][@name='outerForm:addressCity']","GREENVILLE","FALSE","TGTYPESCREENREG");

        SELECT.$("ELE_DRPDWN_STATECOUNTRY","//select[@name='outerForm:countryAndStateCode']	","SOUTH CAROLINA","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_ZIPCODE","//input[@type='text'][@name='outerForm:zipCode']","29662","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SUBMIT","//input[@type='submit'][@name='outerForm:Submit']","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_CANCEL","//input[@type='submit'][@name='outerForm:Cancel']","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_SEARCHCLIENTNAME","//input[@type='text'][@name='outerForm:grid:individualName']",var_ClientSearchName,"FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_FINDPOSTO","//img[@id='outerForm:gnFindImage']","TGTYPESCREENREG");

        SELECT.$("ELE_DRPDWN_RELATIONSHIPGRID","//select[@name='outerForm:grid:0:relationship']","Owner one","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SUBMIT","//input[@type='submit'][@name='outerForm:Submit']","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_AGENTNUMBER","//input[@type='text'][@name='outerForm:grid:0:agentNumberOut2']","000112680","FALSE","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_SITUATIONCODE","//input[@type='text'][@name='outerForm:grid:0:agentSituationCodeOut2']","01","TRUE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SUBMIT","//input[@type='submit'][@name='outerForm:Submit']","TGTYPESCREENREG");

        ASSERT.$("ELE_GETMSG_SUCCESS","//li[@class='MessageInfo']","CONTAINS","successfully","TGTYPESCREENREG");

        CALL.$("SettlePolicy","TGTYPESCREENREG");

        TAP.$("ELE_LINK_BENEFITS","//span[text()='Benefits']","TGTYPESCREENREG");

        SELECT.$("ELE_DRPDWN_ADDABENEFIT","//select[@name='outerForm:initialPlanCode']","PE07PQ - PROSP ELITE 7 YR PROTECTION PKG Q","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_CONTINUE","//input[@type='submit'][@name='outerForm:Continue']","TGTYPESCREENREG");

        TAP.$("ELE_LINK_FUNDSELECTION","//span[contains(text(),'Fund Selection')]","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_PREMIUMALLOC0","//input[@type='text'][@name='outerForm:grid:0:premiumAllocPercOut2']","0.200000","FALSE","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_PREMIUMALLOC1","//input[@id='outerForm:grid:1:premiumAllocPercOut2']","0.200000","FALSE","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_PREMIUMALLOC2","//input[@id='outerForm:grid:2:premiumAllocPercOut2']","0.200000","FALSE","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_PREMIUMALLOC3","//input[@id='outerForm:grid:3:premiumAllocPercOut2']","0.200000","FALSE","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_PREMIUMALLOC4","//input[@id='outerForm:grid:4:premiumAllocPercOut2']","0.100000","FALSE","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_PREMIUMALLOC5","//input[@id='outerForm:grid:5:premiumAllocPercOut2']","0.050000","FALSE","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_PREMIUMALLOC6","//input[@id='outerForm:grid:6:premiumAllocPercOut2']","0.050000","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SUBMIT","//input[@type='submit'][@name='outerForm:Submit']","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_UNITSAFTERADDINGBENEFIT","//input[@type='text'][@name='outerForm:unitsOfInsurance']	","0.000","FALSE","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_REQUESTPREMIUNAMNT","//input[@id='outerForm:annualPremium']",var_CashWithApp,"TRUE","TGTYPESCREENREG");

        SELECT.$("ELE_DRPDWN_GUARWITHDRAWALMETHOD","//select[@id='outerForm:gridSupp:1:guarWithdrawalMethodOut3']","Single","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SUBMIT","//input[@type='submit'][@name='outerForm:Submit']","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_CANCEL","//input[@type='submit'][@name='outerForm:Cancel']","TGTYPESCREENREG");

        TAP.$("ELE_LINK_ISSUE","//span[@id='outerForm:IssueText']","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_EFFECTIVEDATE","//input[@type='text'][@name='outerForm:effectiveDate']",var_effectiveDate,"FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_ISSUE","//input[@id='outerForm:Issue']","TGTYPESCREENREG");

        TAP.$("ELE_LINK_SETTLE","//span[text()='Settle']	","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_PAYMENTATSETTLE","//input[@id='outerForm:paymentAtSettle']",var_CashWithApp,"TRUE","TGTYPESCREENREG");

        CALL.$("ValidatePolicyExchange","TGTYPESCREENREG");

		String var_ModalPremium = "null";
                 LOGGER.info("Executed Step = VAR,String,var_ModalPremium,null,TGTYPESCREENREG");
		var_ModalPremium = ActionWrapper.getElementValue("ELE_GETVAL_MODALPREMIUMAMNTPOLICYEXCHANGE", "//span[@id='outerForm:modalPremium']");
                 LOGGER.info("Executed Step = STORE,var_ModalPremium,ELE_GETVAL_MODALPREMIUMAMNTPOLICYEXCHANGE,TGTYPESCREENREG");
		String var_ExchangeType = "null";
                 LOGGER.info("Executed Step = VAR,String,var_ExchangeType,null,TGTYPESCREENREG");
		var_ExchangeType = getJsonData(var_CSVData , "$.records["+var_Count+"].ExchangeType");
                 LOGGER.info("Executed Step = STORE,var_ExchangeType,var_CSVData,$.records[{var_Count}].ExchangeType,TGTYPESCREENREG");
		String var_ReplacementType = "null";
                 LOGGER.info("Executed Step = VAR,String,var_ReplacementType,null,TGTYPESCREENREG");
		var_ReplacementType = getJsonData(var_CSVData , "$.records["+var_Count+"].ReplacementType");
                 LOGGER.info("Executed Step = STORE,var_ReplacementType,var_CSVData,$.records[{var_Count}].ReplacementType,TGTYPESCREENREG");
		try { 
		 org.openqa.selenium.support.ui.Select select = new org.openqa.selenium.support.ui.Select(driver.findElement(By.xpath("//select[@id='outerForm:rolloverType']")));
		// Add Screenshot will only work for Android platform

		takeScreenshot();

				WebElement option = select.getFirstSelectedOption();
				String defaultItem = option.getText();
				System.out.println(defaultItem );

				if(defaultItem.contains("Direct Transfer") || defaultItem.contains("External Rollover"))
				{ 
				        driver.findElement(By.xpath("//span[@id='outerForm:PolicyExchangeText']")).click();
		// Add Screenshot will only work for Android platform

		takeScreenshot();

					driver.findElement(By.xpath("//img[@id='outerForm:gnAddImage']")).click();
		// Add Screenshot will only work for Android platform

		takeScreenshot();

					 org.openqa.selenium.support.ui.Select select1 = new org.openqa.selenium.support.ui.Select(driver.findElement(By.xpath("//select[@id='outerForm:status']")));
					 select1.selectByVisibleText("Completed");
		// Add Screenshot will only work for Android platform

		takeScreenshot();

								 Thread.sleep(500);
					org.openqa.selenium.support.ui.Select select2 = new org.openqa.selenium.support.ui.Select(driver.findElement(By.xpath("//select[@id='outerForm:exchangeCode']")));
					select2.selectByVisibleText(var_ExchangeType);
		// Add Screenshot will only work for Android platform

		takeScreenshot();

					 Thread.sleep(500);
		org.openqa.selenium.support.ui.Select select3 = new org.openqa.selenium.support.ui.Select(driver.findElement(By.xpath("//select[@id='outerForm:replacementType']")));
					select3.selectByVisibleText(var_ReplacementType);
		// Add Screenshot will only work for Android platform

		takeScreenshot();

					 Thread.sleep(500);
					driver.findElement(By.xpath("//input[@id='outerForm:estimateExchangeAmt']")).clear();
		driver.findElement(By.xpath("//input[@id='outerForm:estimateExchangeAmt']")).sendKeys(var_ModalPremium);
		// Add Screenshot will only work for Android platform

		takeScreenshot();

					 Thread.sleep(500);

		driver.findElement(By.xpath("//input[@id='outerForm:cashReceivedFromExch']")).clear();			driver.findElement(By.xpath("//input[@id='outerForm:cashReceivedFromExch']")).sendKeys(var_ModalPremium);
				   driver.findElement(By.xpath("//input[@id='outerForm:cashReceivedDate_input']")).sendKeys(var_effectiveDate);
				    Thread.sleep(500);
				   		   driver.findElement(By.xpath("//input[@id='outerForm:priorCompanyName']")).sendKeys("American General");
						    Thread.sleep(500);
						org.openqa.selenium.support.ui.Select select4 = new org.openqa.selenium.support.ui.Select(driver.findElement(By.xpath("//select[@id='outerForm:priorInvestmentType']")));
						select4.selectByVisibleText("Annuity");
		// Add Screenshot will only work for Android platform

		takeScreenshot();

						 Thread.sleep(500);
					org.openqa.selenium.support.ui.Select select5 = new org.openqa.selenium.support.ui.Select(driver.findElement(By.xpath("//select[@id='outerForm:currencyCode']")));
					select5.selectByVisibleText("DOLLAR [US]");
					 Thread.sleep(500);
				        driver.findElement(By.xpath("//input[@type='submit'][@name='outerForm:Submit']")).click();
		// Add Screenshot will only work for Android platform

		takeScreenshot();

									 Thread.sleep(500);
						driver.findElement(By.xpath("//input[@type='submit'][@name='outerForm:Cancel']")).click();
						 Thread.sleep(500);
						driver.findElement(By.xpath("//input[@type='submit'][@name='outerForm:Cancel']")).click();
									 Thread.sleep(500);

						driver.findElement(By.xpath("//input[@type='submit'][@name='outerForm:Submit']")).click();
		// Add Screenshot will only work for Android platform

		takeScreenshot();

									 Thread.sleep(500);
		// Add Screenshot will only work for Android platform

		takeScreenshot();

						driver.findElement(By.xpath("//input[@type='submit'][@name='outerForm:Cancel']")).click();
						 Thread.sleep(500);
		// Add Screenshot will only work for Android platform

		takeScreenshot();

				}
				 
				 else {
						driver.findElement(By.xpath("//input[@type='submit'][@name='outerForm:Submit']")).click();
		// Add Screenshot will only work for Android platform

		takeScreenshot();
						
						Assert.	assertTrue(driver.findElement(By.xpath("//li[@class='MessageInfo']")).getText().equals("Request completed successfully."));
		// Add Screenshot will only work for Android platform

		takeScreenshot();
				
		 }
		 
		 ;} 
		 catch(Exception e) { PrintVal("Renderer Script Exception : "+e.toString()); } 
		 PrintVal("Executed Step = CALLRENDERERSCRIPT,VERIFYTHEROLLOVERCONDITION"); 
        CALL.$("WritePolicyQuickInquiryToCSV","TGTYPESCREENREG");

        HOVER.$("ELE_MENU_INQUIRY","//td[text()='Inquiry']","TGTYPESCREENREG");

        TAP.$("ELE_SUBMEN_QUICKINQUIRY","//td[contains(text(),'Quick Inquiry')]","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_POLICYNUMBER","//input[@type='text'][@name='outerForm:policyNumber']",var_PolicyNumber,"FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_CONTINUE","//input[@type='submit'][@name='outerForm:Continue']","TGTYPESCREENREG");

		writeToCSV ("Policy Number " , ActionWrapper.getElementValue("ELE_GETVAL_POLICYNUMBER", "//span[@id='outerForm:policyNumber']"));
                 LOGGER.info("Executed Step = WRITETOCSV,ELE_GETVAL_POLICYNUMBER,Policy Number,TGTYPESCREENREG");
		writeToCSV ("Status " , ActionWrapper.getElementValue("ELE_GETVAL_STATUS", "//span[@id='outerForm:statusText']"));
                 LOGGER.info("Executed Step = WRITETOCSV,ELE_GETVAL_STATUS,Status,TGTYPESCREENREG");
		writeToCSV ("Effective Date " , ActionWrapper.getElementValue("ELE_GETVAL_EFFECTIVEDATETEXTFROMGRID", "//span[@id='outerForm:grid:0:effectiveDateOut2']"));
                 LOGGER.info("Executed Step = WRITETOCSV,ELE_GETVAL_EFFECTIVEDATETEXTFROMGRID,Effective Date,TGTYPESCREENREG");
		writeToCSV ("State country " , ActionWrapper.getElementValue("ELE_GETVAL_STATECOUNTRY", "//span[@id='outerForm:ownerCityStateZip']"));
                 LOGGER.info("Executed Step = WRITETOCSV,ELE_GETVAL_STATECOUNTRY,State country,TGTYPESCREENREG");
		writeToCSV ("Agent Number " , ActionWrapper.getElementValue("ELE_GETVAL_AGENTNUMBER", "//span[@id='outerForm:agentNumber']"));
                 LOGGER.info("Executed Step = WRITETOCSV,ELE_GETVAL_AGENTNUMBER,Agent Number,TGTYPESCREENREG");
		var_Count = var_Count + ( 1 );
                 LOGGER.info("Executed Step = INCREASEVAR,var_Count,1,TGTYPESCREENREG");
    	}
          LOGGER.info("Executed Step = WEND,TGTYPESCREENREG");
    	resetAttemps();

    }


    @Test
    public void tc01createnewbusinesspolicyuat2() {

        // No need to declare / print method name here, handled by TestNg listener
        // declare("tc01createnewbusinesspolicyuat2");

        CALL.$("LoginToGIASFandG","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_USERNAME","outerForm:usernameTGWEBCOMMAIdTGWEBCOMMA0","x5990","FALSE","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_PASSWORD","outerForm:passwordTGWEBCOMMAIdTGWEBCOMMA0","Big1Blue2!","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_LOGIN","outerForm:loginButtonTGWEBCOMMAIdTGWEBCOMMA0","TGTYPESCREENREG");

		int var_Count = 0;
                 LOGGER.info("Executed Step = VAR,Integer,var_Count,0,TGTYPESCREENREG");
        LOGGER.info("Executed Step = REPEAT IF");
    	while (check(var_Count,"<",1,"TGTYPESCREENREG")) {
          LOGGER.info("Executed Step = WHILE,var_Count,<,1,TGTYPESCREENREG");
		com.jayway.jsonpath.ReadContext var_CSVData = getJsonData("C:/GIAS_Automation/FandG Regression/InputData/FandGReg_CreateAPolicy.csv");
                 LOGGER.info("Executed Step = VAR,Json,var_CSVData,C:/GIAS_Automation/FandG Regression/InputData/FandGReg_CreateAPolicy.csv,TGTYPESCREENREG");
		String var_EffectiveDate = "null";
                 LOGGER.info("Executed Step = VAR,String,var_EffectiveDate,null,TGTYPESCREENREG");
		var_EffectiveDate = getJsonData(var_CSVData , "$.records["+var_Count+"].EffectiveDate");
                 LOGGER.info("Executed Step = STORE,var_EffectiveDate,var_CSVData,$.records[{var_Count}].EffectiveDate,TGTYPESCREENREG");
        CALL.$("AddNewBusinessPolicyUAT2","TGTYPESCREENREG");

        HOVER.$("ELE_MENU_NEWBUSINESS","//td[text()='New Business']","TGTYPESCREENREG");

        TAP.$("ELE_SUBMEN_APPLICATIONENTRYUPDATE","//td[text()='Application Entry/Update']","TGTYPESCREENREG");

        TAP.$("ELE_RNDBTN_ADD","//img[@id='outerForm:gnAddImage']","TGTYPESCREENREG");

		String var_PolicyNumber;
                 LOGGER.info("Executed Step = VAR,String,var_PolicyNumber,TGTYPESCREENREG");
		var_PolicyNumber = getJsonData(var_CSVData , "$.records["+var_Count+"].PolicyNumber");
                 LOGGER.info("Executed Step = STORE,var_PolicyNumber,var_CSVData,$.records[{var_Count}].PolicyNumber,TGTYPESCREENREG");
		String var_Name1;
                 LOGGER.info("Executed Step = VAR,String,var_Name1,TGTYPESCREENREG");
		var_Name1 = getJsonData(var_CSVData , "$.records["+var_Count+"].Name1");
                 LOGGER.info("Executed Step = STORE,var_Name1,var_CSVData,$.records[{var_Count}].Name1,TGTYPESCREENREG");
		String var_FirstName1;
                 LOGGER.info("Executed Step = VAR,String,var_FirstName1,TGTYPESCREENREG");
		var_FirstName1 = getJsonData(var_CSVData , "$.records["+var_Count+"].First1");
                 LOGGER.info("Executed Step = STORE,var_FirstName1,var_CSVData,$.records[{var_Count}].First1,TGTYPESCREENREG");
		String var_LastName1;
                 LOGGER.info("Executed Step = VAR,String,var_LastName1,TGTYPESCREENREG");
		var_LastName1 = getJsonData(var_CSVData , "$.records["+var_Count+"].Last1");
                 LOGGER.info("Executed Step = STORE,var_LastName1,var_CSVData,$.records[{var_Count}].Last1,TGTYPESCREENREG");
		String var_BirthStateCountry1;
                 LOGGER.info("Executed Step = VAR,String,var_BirthStateCountry1,TGTYPESCREENREG");
		var_BirthStateCountry1 = getJsonData(var_CSVData , "$.records["+var_Count+"].BirthStateCountry1");
                 LOGGER.info("Executed Step = STORE,var_BirthStateCountry1,var_CSVData,$.records[{var_Count}].BirthStateCountry1,TGTYPESCREENREG");
		String var_DateOfBirth1;
                 LOGGER.info("Executed Step = VAR,String,var_DateOfBirth1,TGTYPESCREENREG");
		var_DateOfBirth1 = getJsonData(var_CSVData , "$.records["+var_Count+"].DateOfBirth1");
                 LOGGER.info("Executed Step = STORE,var_DateOfBirth1,var_CSVData,$.records[{var_Count}].DateOfBirth1,TGTYPESCREENREG");
		String var_Gender1;
                 LOGGER.info("Executed Step = VAR,String,var_Gender1,TGTYPESCREENREG");
		var_Gender1 = getJsonData(var_CSVData , "$.records["+var_Count+"].Gender1");
                 LOGGER.info("Executed Step = STORE,var_Gender1,var_CSVData,$.records[{var_Count}].Gender1,TGTYPESCREENREG");
		String var_IDType1;
                 LOGGER.info("Executed Step = VAR,String,var_IDType1,TGTYPESCREENREG");
		var_IDType1 = getJsonData(var_CSVData , "$.records["+var_Count+"].IDType1");
                 LOGGER.info("Executed Step = STORE,var_IDType1,var_CSVData,$.records[{var_Count}].IDType1,TGTYPESCREENREG");
		String var_IDNumber1;
                 LOGGER.info("Executed Step = VAR,String,var_IDNumber1,TGTYPESCREENREG");
		var_IDNumber1 = getJsonData(var_CSVData , "$.records["+var_Count+"].IDNumber1");
                 LOGGER.info("Executed Step = STORE,var_IDNumber1,var_CSVData,$.records[{var_Count}].IDNumber1,TGTYPESCREENREG");
		String var_FirstName2;
                 LOGGER.info("Executed Step = VAR,String,var_FirstName2,TGTYPESCREENREG");
		var_FirstName2 = getJsonData(var_CSVData , "$.records["+var_Count+"].First2");
                 LOGGER.info("Executed Step = STORE,var_FirstName2,var_CSVData,$.records[{var_Count}].First2,TGTYPESCREENREG");
		String var_LastName2;
                 LOGGER.info("Executed Step = VAR,String,var_LastName2,TGTYPESCREENREG");
		var_LastName2 = getJsonData(var_CSVData , "$.records["+var_Count+"].Last2");
                 LOGGER.info("Executed Step = STORE,var_LastName2,var_CSVData,$.records[{var_Count}].Last2,TGTYPESCREENREG");
		String var_BirthStateCountry2;
                 LOGGER.info("Executed Step = VAR,String,var_BirthStateCountry2,TGTYPESCREENREG");
		var_BirthStateCountry2 = getJsonData(var_CSVData , "$.records["+var_Count+"].BirthStateCountry2");
                 LOGGER.info("Executed Step = STORE,var_BirthStateCountry2,var_CSVData,$.records[{var_Count}].BirthStateCountry2,TGTYPESCREENREG");
		String var_DateOfBirth2;
                 LOGGER.info("Executed Step = VAR,String,var_DateOfBirth2,TGTYPESCREENREG");
		var_DateOfBirth2 = getJsonData(var_CSVData , "$.records["+var_Count+"].DateOfBirth2");
                 LOGGER.info("Executed Step = STORE,var_DateOfBirth2,var_CSVData,$.records[{var_Count}].DateOfBirth2,TGTYPESCREENREG");
		String var_Gender2;
                 LOGGER.info("Executed Step = VAR,String,var_Gender2,TGTYPESCREENREG");
		var_Gender2 = getJsonData(var_CSVData , "$.records["+var_Count+"].Gender2");
                 LOGGER.info("Executed Step = STORE,var_Gender2,var_CSVData,$.records[{var_Count}].Gender2,TGTYPESCREENREG");
		String var_IDType2;
                 LOGGER.info("Executed Step = VAR,String,var_IDType2,TGTYPESCREENREG");
		var_IDType2 = getJsonData(var_CSVData , "$.records["+var_Count+"].IDType2");
                 LOGGER.info("Executed Step = STORE,var_IDType2,var_CSVData,$.records[{var_Count}].IDType2,TGTYPESCREENREG");
		String var_IDNumber2;
                 LOGGER.info("Executed Step = VAR,String,var_IDNumber2,TGTYPESCREENREG");
		var_IDNumber2 = getJsonData(var_CSVData , "$.records["+var_Count+"].IDNumber2");
                 LOGGER.info("Executed Step = STORE,var_IDNumber2,var_CSVData,$.records[{var_Count}].IDNumber2,TGTYPESCREENREG");
		String var_AddressLine11;
                 LOGGER.info("Executed Step = VAR,String,var_AddressLine11,TGTYPESCREENREG");
		var_AddressLine11 = getJsonData(var_CSVData , "$.records["+var_Count+"].AddressLine1_1");
                 LOGGER.info("Executed Step = STORE,var_AddressLine11,var_CSVData,$.records[{var_Count}].AddressLine1_1,TGTYPESCREENREG");
		String var_AddressLine21;
                 LOGGER.info("Executed Step = VAR,String,var_AddressLine21,TGTYPESCREENREG");
		var_AddressLine21 = getJsonData(var_CSVData , "$.records["+var_Count+"].AddressLine2_1");
                 LOGGER.info("Executed Step = STORE,var_AddressLine21,var_CSVData,$.records[{var_Count}].AddressLine2_1,TGTYPESCREENREG");
		String var_AddressLine31;
                 LOGGER.info("Executed Step = VAR,String,var_AddressLine31,TGTYPESCREENREG");
		var_AddressLine31 = getJsonData(var_CSVData , "$.records["+var_Count+"].AddressLine3_1");
                 LOGGER.info("Executed Step = STORE,var_AddressLine31,var_CSVData,$.records[{var_Count}].AddressLine3_1,TGTYPESCREENREG");
		String var_City1;
                 LOGGER.info("Executed Step = VAR,String,var_City1,TGTYPESCREENREG");
		var_City1 = getJsonData(var_CSVData , "$.records["+var_Count+"].City1");
                 LOGGER.info("Executed Step = STORE,var_City1,var_CSVData,$.records[{var_Count}].City1,TGTYPESCREENREG");
		String var_StateandCountry1;
                 LOGGER.info("Executed Step = VAR,String,var_StateandCountry1,TGTYPESCREENREG");
		var_StateandCountry1 = getJsonData(var_CSVData , "$.records["+var_Count+"].StateandCountry1");
                 LOGGER.info("Executed Step = STORE,var_StateandCountry1,var_CSVData,$.records[{var_Count}].StateandCountry1,TGTYPESCREENREG");
		String var_ZipCode1;
                 LOGGER.info("Executed Step = VAR,String,var_ZipCode1,TGTYPESCREENREG");
		var_ZipCode1 = getJsonData(var_CSVData , "$.records["+var_Count+"].ZipCode1");
                 LOGGER.info("Executed Step = STORE,var_ZipCode1,var_CSVData,$.records[{var_Count}].ZipCode1,TGTYPESCREENREG");
		String var_AddressLine12;
                 LOGGER.info("Executed Step = VAR,String,var_AddressLine12,TGTYPESCREENREG");
		var_AddressLine12 = getJsonData(var_CSVData , "$.records["+var_Count+"].AddressLine1_2");
                 LOGGER.info("Executed Step = STORE,var_AddressLine12,var_CSVData,$.records[{var_Count}].AddressLine1_2,TGTYPESCREENREG");
		String var_AddressLine22;
                 LOGGER.info("Executed Step = VAR,String,var_AddressLine22,TGTYPESCREENREG");
		var_AddressLine22 = getJsonData(var_CSVData , "$.records["+var_Count+"].AddressLine2_2");
                 LOGGER.info("Executed Step = STORE,var_AddressLine22,var_CSVData,$.records[{var_Count}].AddressLine2_2,TGTYPESCREENREG");
		String var_AddressLine32;
                 LOGGER.info("Executed Step = VAR,String,var_AddressLine32,TGTYPESCREENREG");
		var_AddressLine32 = getJsonData(var_CSVData , "$.records["+var_Count+"].AddressLine3_2");
                 LOGGER.info("Executed Step = STORE,var_AddressLine32,var_CSVData,$.records[{var_Count}].AddressLine3_2,TGTYPESCREENREG");
		String var_City2;
                 LOGGER.info("Executed Step = VAR,String,var_City2,TGTYPESCREENREG");
		var_City2 = getJsonData(var_CSVData , "$.records["+var_Count+"].City2");
                 LOGGER.info("Executed Step = STORE,var_City2,var_CSVData,$.records[{var_Count}].City2,TGTYPESCREENREG");
		String var_StateandCountry2;
                 LOGGER.info("Executed Step = VAR,String,var_StateandCountry2,TGTYPESCREENREG");
		var_StateandCountry2 = getJsonData(var_CSVData , "$.records["+var_Count+"].StateandCountry2");
                 LOGGER.info("Executed Step = STORE,var_StateandCountry2,var_CSVData,$.records[{var_Count}].StateandCountry2,TGTYPESCREENREG");
		String var_ZipCode2;
                 LOGGER.info("Executed Step = VAR,String,var_ZipCode2,TGTYPESCREENREG");
		var_ZipCode2 = getJsonData(var_CSVData , "$.records["+var_Count+"].ZipCode2");
                 LOGGER.info("Executed Step = STORE,var_ZipCode2,var_CSVData,$.records[{var_Count}].ZipCode2,TGTYPESCREENREG");
		String var_SelectTypeOne;
                 LOGGER.info("Executed Step = VAR,String,var_SelectTypeOne,TGTYPESCREENREG");
		var_SelectTypeOne = getJsonData(var_CSVData , "$.records["+var_Count+"].SelectTypeOne");
                 LOGGER.info("Executed Step = STORE,var_SelectTypeOne,var_CSVData,$.records[{var_Count}].SelectTypeOne,TGTYPESCREENREG");
		String var_SelectTypeTwo;
                 LOGGER.info("Executed Step = VAR,String,var_SelectTypeTwo,TGTYPESCREENREG");
		var_SelectTypeTwo = getJsonData(var_CSVData , "$.records["+var_Count+"].SelectTypeTwo");
                 LOGGER.info("Executed Step = STORE,var_SelectTypeTwo,var_CSVData,$.records[{var_Count}].SelectTypeTwo,TGTYPESCREENREG");
		String var_Name2;
                 LOGGER.info("Executed Step = VAR,String,var_Name2,TGTYPESCREENREG");
		var_Name2 = getJsonData(var_CSVData , "$.records["+var_Count+"].Name2");
                 LOGGER.info("Executed Step = STORE,var_Name2,var_CSVData,$.records[{var_Count}].Name2,TGTYPESCREENREG");
		String var_SelectTypeThree;
                 LOGGER.info("Executed Step = VAR,String,var_SelectTypeThree,TGTYPESCREENREG");
		var_SelectTypeThree = getJsonData(var_CSVData , "$.records["+var_Count+"].SelectTypeThree");
                 LOGGER.info("Executed Step = STORE,var_SelectTypeThree,var_CSVData,$.records[{var_Count}].SelectTypeThree,TGTYPESCREENREG");
		String var_ApplicationDate;
                 LOGGER.info("Executed Step = VAR,String,var_ApplicationDate,TGTYPESCREENREG");
		var_ApplicationDate = getJsonData(var_CSVData , "$.records["+var_Count+"].ApplicationDate");
                 LOGGER.info("Executed Step = STORE,var_ApplicationDate,var_CSVData,$.records[{var_Count}].ApplicationDate,TGTYPESCREENREG");
		String var_IssueStateCountry;
                 LOGGER.info("Executed Step = VAR,String,var_IssueStateCountry,TGTYPESCREENREG");
		var_IssueStateCountry = getJsonData(var_CSVData , "$.records["+var_Count+"].IssueStateCountry");
                 LOGGER.info("Executed Step = STORE,var_IssueStateCountry,var_CSVData,$.records[{var_Count}].IssueStateCountry,TGTYPESCREENREG");
		String var_EffectiveDatenew;
                 LOGGER.info("Executed Step = VAR,String,var_EffectiveDatenew,TGTYPESCREENREG");
		var_EffectiveDatenew = getJsonData(var_CSVData , "$.records["+var_Count+"].EffectiveDate");
                 LOGGER.info("Executed Step = STORE,var_EffectiveDatenew,var_CSVData,$.records[{var_Count}].EffectiveDate,TGTYPESCREENREG");
		String var_CashWithApplication;
                 LOGGER.info("Executed Step = VAR,String,var_CashWithApplication,TGTYPESCREENREG");
		var_CashWithApplication = getJsonData(var_CSVData , "$.records["+var_Count+"].CashWithApplication");
                 LOGGER.info("Executed Step = STORE,var_CashWithApplication,var_CSVData,$.records[{var_Count}].CashWithApplication,TGTYPESCREENREG");
		String var_TaxQualifiedDescription;
                 LOGGER.info("Executed Step = VAR,String,var_TaxQualifiedDescription,TGTYPESCREENREG");
		var_TaxQualifiedDescription = getJsonData(var_CSVData , "$.records["+var_Count+"].TaxQualifiedDescription");
                 LOGGER.info("Executed Step = STORE,var_TaxQualifiedDescription,var_CSVData,$.records[{var_Count}].TaxQualifiedDescription,TGTYPESCREENREG");
		String var_AgentNumberOne;
                 LOGGER.info("Executed Step = VAR,String,var_AgentNumberOne,TGTYPESCREENREG");
		var_AgentNumberOne = getJsonData(var_CSVData , "$.records["+var_Count+"].AgentNumberOne");
                 LOGGER.info("Executed Step = STORE,var_AgentNumberOne,var_CSVData,$.records[{var_Count}].AgentNumberOne,TGTYPESCREENREG");
		String var_SituationCode1;
                 LOGGER.info("Executed Step = VAR,String,var_SituationCode1,TGTYPESCREENREG");
		var_SituationCode1 = getJsonData(var_CSVData , "$.records["+var_Count+"].SituationCode1");
                 LOGGER.info("Executed Step = STORE,var_SituationCode1,var_CSVData,$.records[{var_Count}].SituationCode1,TGTYPESCREENREG");
		String var_SplitPercentage1;
                 LOGGER.info("Executed Step = VAR,String,var_SplitPercentage1,TGTYPESCREENREG");
		var_SplitPercentage1 = getJsonData(var_CSVData , "$.records["+var_Count+"].SplitPercentage1");
                 LOGGER.info("Executed Step = STORE,var_SplitPercentage1,var_CSVData,$.records[{var_Count}].SplitPercentage1,TGTYPESCREENREG");
		String var_AgentNumberTwo;
                 LOGGER.info("Executed Step = VAR,String,var_AgentNumberTwo,TGTYPESCREENREG");
		var_AgentNumberTwo = getJsonData(var_CSVData , "$.records["+var_Count+"].AgentNumberTwo");
                 LOGGER.info("Executed Step = STORE,var_AgentNumberTwo,var_CSVData,$.records[{var_Count}].AgentNumberTwo,TGTYPESCREENREG");
		String var_SituationCode2;
                 LOGGER.info("Executed Step = VAR,String,var_SituationCode2,TGTYPESCREENREG");
		var_SituationCode2 = getJsonData(var_CSVData , "$.records["+var_Count+"].SituationCode2");
                 LOGGER.info("Executed Step = STORE,var_SituationCode2,var_CSVData,$.records[{var_Count}].SituationCode2,TGTYPESCREENREG");
		String var_SplitPercentage2;
                 LOGGER.info("Executed Step = VAR,String,var_SplitPercentage2,TGTYPESCREENREG");
		var_SplitPercentage2 = getJsonData(var_CSVData , "$.records["+var_Count+"].SplitPercentage2");
                 LOGGER.info("Executed Step = STORE,var_SplitPercentage2,var_CSVData,$.records[{var_Count}].SplitPercentage2,TGTYPESCREENREG");
        TYPE.$("ELE_TXTBOX_POLICYNUMBER","//input[@type='text'][@name='outerForm:policyNumber']",var_PolicyNumber,"FALSE","TGTYPESCREENREG");

        SELECT.$("ELE_DRPDWN_CURRENCY","//select[@id='outerForm:grid:currencyCode']","DOLLAR [US]","TGTYPESCREENREG");

        SELECT.$("ELE_DRPDWN_STATECOUNTRY","//select[@name='outerForm:countryAndStateCode']	",var_StateandCountry1,"TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_CASHWITHAPPLICATION","//input[@type='text'][@name='outerForm:cashWithApplication']",var_CashWithApplication,"FALSE","TGTYPESCREENREG");

        SELECT.$("ELE_DRPDWN_PAYMENTFREQUENCY","//select[@name='outerForm:paymentMode']","Annual","TGTYPESCREENREG");

        SELECT.$("ELE_DRPDWN_PAYMENTMETHOD","//select[@name='outerForm:paymentCode']","No Notice","TGTYPESCREENREG");

        SELECT.$("ELE_DRPDWN_TAXQUALIFIEDDESCRIPTION","//select[@name='outerForm:taxQualifiedCode']",var_TaxQualifiedDescription,"TGTYPESCREENREG");

        SELECT.$("ELE_DRPDWN_TAXWITHHOLDING","//select[@name='outerForm:taxWithholdingCode']","No Withholding","TGTYPESCREENREG");

        TAP.$("ELE_LINK_SELECTCLIENT","//span[text()='Select Client']","TGTYPESCREENREG");

        TAP.$("ELE_RNDBTN_ADD","//img[@id='outerForm:gnAddImage']","TGTYPESCREENREG");

        LOGGER.info("Executed Step = START IF");
        if (check(var_FirstName2,"=","null","TGTYPESCREENREG")) {
         LOGGER.info("Executed Step = IF,var_FirstName2,=,null,TGTYPESCREENREG");
        TYPE.$("ELE_TXTBOX_FIRSTNAME","//input[@type='text'][@name='outerForm:nameFirst']",var_FirstName1,"FALSE","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_LASTNAME","//input[@type='text'][@name='outerForm:nameLast']",var_LastName1,"FALSE","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_CLIENTDOB","//input[@type='text'][@name='outerForm:dateOfBirth2']",var_DateOfBirth1,"FALSE","TGTYPESCREENREG");

        SELECT.$("ELE_DRPDWN_CLIENTGENDER","//select[@name='outerForm:sexCode2']",var_Gender1,"TGTYPESCREENREG");

        SELECT.$("ELE_DRPDWN_IDTYPE","//select[@name='outerForm:taxIdenUsag2']",var_IDType1,"TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_IDNUMBER","//input[@type='text'][@name='outerForm:identificationNumber2']",var_IDNumber1,"FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SUBMIT","//input[@type='submit'][@name='outerForm:Submit']","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_ADDRESSLINE1","//input[@type='text'][@name='outerForm:addressLineOne']",var_AddressLine11,"FALSE","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_ADDRESSLINE2","//input[@type='text'][@name='outerForm:addressLineTwo']",var_AddressLine21,"FALSE","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_ADDRESSLINE3","//input[@type='text'][@name='outerForm:addressLineThree']",var_AddressLine31,"FALSE","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_CITY","//input[@type='text'][@name='outerForm:addressCity']",var_City1,"FALSE","TGTYPESCREENREG");

        SELECT.$("ELE_DRPDWN_STATECOUNTRY","//select[@name='outerForm:countryAndStateCode']	",var_StateandCountry1,"TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_ZIPCODE","//input[@type='text'][@name='outerForm:zipCode']",var_ZipCode1,"FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SUBMIT","//input[@type='submit'][@name='outerForm:Submit']","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_CANCEL","//input[@type='submit'][@name='outerForm:Cancel']","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_SEARCHCLIENTNAME","//input[@type='text'][@name='outerForm:grid:individualName']",var_Name1,"FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_FINDPOSTO","//img[@id='outerForm:gnFindImage']","TGTYPESCREENREG");

        SELECT.$("ELE_DRPDWN_RELATIONSHIPGRID","//select[@name='outerForm:grid:0:relationship']",var_SelectTypeOne,"TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SUBMIT","//input[@type='submit'][@name='outerForm:Submit']","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_AGENTNUMBER","//input[@type='text'][@name='outerForm:grid:0:agentNumberOut2']",var_AgentNumberOne,"FALSE","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_SITUATIONCODE","//input[@type='text'][@name='outerForm:grid:0:agentSituationCodeOut2']",var_SituationCode1,"FALSE","TGTYPESCREENREG");

        }
         else {
          LOGGER.info("Executed Step = ELSE,TGTYPESCREENREG");
        TYPE.$("ELE_TXTBOX_FIRSTNAME","//input[@type='text'][@name='outerForm:nameFirst']",var_FirstName1,"FALSE","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_LASTNAME","//input[@type='text'][@name='outerForm:nameLast']",var_LastName1,"FALSE","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_CLIENTDOB","//input[@type='text'][@name='outerForm:dateOfBirth2']",var_DateOfBirth1,"FALSE","TGTYPESCREENREG");

        SELECT.$("ELE_DRPDWN_CLIENTGENDER","//select[@name='outerForm:sexCode2']",var_Gender1,"TGTYPESCREENREG");

        SELECT.$("ELE_DRPDWN_IDTYPE","//select[@name='outerForm:taxIdenUsag2']",var_IDType1,"TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_IDNUMBER","//input[@type='text'][@name='outerForm:identificationNumber2']",var_IDNumber1,"FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SUBMIT","//input[@type='submit'][@name='outerForm:Submit']","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_ADDRESSLINE1","//input[@type='text'][@name='outerForm:addressLineOne']",var_AddressLine11,"FALSE","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_ADDRESSLINE2","//input[@type='text'][@name='outerForm:addressLineTwo']",var_AddressLine21,"FALSE","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_ADDRESSLINE3","//input[@type='text'][@name='outerForm:addressLineThree']",var_AddressLine31,"FALSE","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_CITY","//input[@type='text'][@name='outerForm:addressCity']",var_City1,"FALSE","TGTYPESCREENREG");

        SELECT.$("ELE_DRPDWN_STATECOUNTRY","//select[@name='outerForm:countryAndStateCode']	",var_StateandCountry1,"TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_ZIPCODE","//input[@type='text'][@name='outerForm:zipCode']",var_ZipCode1,"FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SUBMIT","//input[@type='submit'][@name='outerForm:Submit']","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_FIRSTNAME","//input[@type='text'][@name='outerForm:nameFirst']",var_FirstName2,"FALSE","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_LASTNAME","//input[@type='text'][@name='outerForm:nameLast']",var_LastName2,"FALSE","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_CLIENTDOB","//input[@type='text'][@name='outerForm:dateOfBirth2']",var_DateOfBirth2,"FALSE","TGTYPESCREENREG");

        SELECT.$("ELE_DRPDWN_CLIENTGENDER","//select[@name='outerForm:sexCode2']",var_Gender2,"TGTYPESCREENREG");

        SELECT.$("ELE_DRPDWN_IDTYPE","//select[@name='outerForm:taxIdenUsag2']",var_IDType2,"TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_IDNUMBER","//input[@type='text'][@name='outerForm:identificationNumber2']",var_IDNumber2,"FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SUBMIT","//input[@type='submit'][@name='outerForm:Submit']","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_ADDRESSLINE1","//input[@type='text'][@name='outerForm:addressLineOne']",var_AddressLine12,"FALSE","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_ADDRESSLINE2","//input[@type='text'][@name='outerForm:addressLineTwo']",var_AddressLine22,"FALSE","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_ADDRESSLINE3","//input[@type='text'][@name='outerForm:addressLineThree']",var_AddressLine32,"FALSE","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_CITY","//input[@type='text'][@name='outerForm:addressCity']",var_City2,"FALSE","TGTYPESCREENREG");

        SELECT.$("ELE_DRPDWN_STATECOUNTRY","//select[@name='outerForm:countryAndStateCode']	",var_StateandCountry2,"TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_ZIPCODE","//input[@type='text'][@name='outerForm:zipCode']",var_ZipCode2,"FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SUBMIT","//input[@type='submit'][@name='outerForm:Submit']","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_CANCEL","//input[@type='submit'][@name='outerForm:Cancel']","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_SEARCHCLIENTNAME","//input[@type='text'][@name='outerForm:grid:individualName']",var_Name1,"FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_FINDPOSTO","//img[@id='outerForm:gnFindImage']","TGTYPESCREENREG");

        SELECT.$("ELE_DRPDWN_RELATIONSHIPGRID","//select[@name='outerForm:grid:0:relationship']",var_SelectTypeOne,"TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_SEARCHCLIENTNAME","//input[@type='text'][@name='outerForm:grid:individualName']",var_Name2,"FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_FINDPOSTO","//img[@id='outerForm:gnFindImage']","TGTYPESCREENREG");

        SELECT.$("ELE_DRPDWN_RELATIONSHIPGRID","//select[@name='outerForm:grid:0:relationship']",var_SelectTypeTwo,"TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SUBMIT","//input[@type='submit'][@name='outerForm:Submit']","TGTYPESCREENREG");

        LOGGER.info("Executed Step = START IF");
        if (check(var_AgentNumberTwo,"=","null","TGTYPESCREENREG")) {
         LOGGER.info("Executed Step = IF,var_AgentNumberTwo,=,null,TGTYPESCREENREG");
        TYPE.$("ELE_TXTBOX_AGENTNUMBER","//input[@type='text'][@name='outerForm:grid:0:agentNumberOut2']",var_AgentNumberOne,"FALSE","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_SITUATIONCODE","//input[@type='text'][@name='outerForm:grid:0:agentSituationCodeOut2']",var_SituationCode1,"FALSE","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_SPLITPERCENTAGE","//input[@type='text'][@name='outerForm:grid:0:agentSplitPercentageOut2']",var_SplitPercentage1,"FALSE","TGTYPESCREENREG");

        }
         else {
          LOGGER.info("Executed Step = ELSE,TGTYPESCREENREG");
        TYPE.$("ELE_TXTBOX_AGENTNUMBER","//input[@type='text'][@name='outerForm:grid:0:agentNumberOut2']",var_AgentNumberOne,"FALSE","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_SITUATIONCODE","//input[@type='text'][@name='outerForm:grid:0:agentSituationCodeOut2']",var_SituationCode1,"FALSE","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_SPLITPERCENTAGE","//input[@type='text'][@name='outerForm:grid:0:agentSplitPercentageOut2']",var_SplitPercentage1,"FALSE","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_AGENTNUMBER2","//input[@type='text'][@name='outerForm:grid:1:agentNumberOut2']",var_AgentNumberTwo,"FALSE","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_SITUATIONCODE2","//input[@type='text'][@name='outerForm:grid:1:agentSituationCodeOut2']",var_SituationCode2,"FALSE","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_SPLITPERCENTAGE2","//input[@type='text'][@name='outerForm:grid:1:agentSplitPercentageOut2']",var_SplitPercentage2,"FALSE","TGTYPESCREENREG");

        }
          LOGGER.info("Executed Step = ENDIF,TGTYPESCREENREG");
        }
          LOGGER.info("Executed Step = ENDIF,TGTYPESCREENREG");
        TAP.$("ELE_BUTTON_SUBMIT","//input[@type='submit'][@name='outerForm:Submit']","TGTYPESCREENREG");

        ASSERT.$("ELE_GETMSG_SUCCESS","//li[@class='MessageInfo']","CONTAINS","successfully","TGTYPESCREENREG");

        CALL.$("SettlePolicyUAT2","TGTYPESCREENREG");

        TAP.$("ELE_LINK_BENEFITS","//span[text()='Benefits']","TGTYPESCREENREG");

		String var_Plan;
                 LOGGER.info("Executed Step = VAR,String,var_Plan,TGTYPESCREENREG");
		var_Plan = getJsonData(var_CSVData , "$.records["+var_Count+"].Plan");
                 LOGGER.info("Executed Step = STORE,var_Plan,var_CSVData,$.records[{var_Count}].Plan,TGTYPESCREENREG");
        SELECT.$("ELE_DRPDWN_ADDABENEFIT","//select[@name='outerForm:initialPlanCode']",var_Plan,"TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_CONTINUE","//input[@type='submit'][@name='outerForm:Continue']","TGTYPESCREENREG");

        TAP.$("ELE_LINK_FUNDSELECTION","//span[contains(text(),'Fund Selection')]","TGTYPESCREENREG");

		try { 
		  try {
		            List<WebElement> baseRider= driver.findElements(By.xpath("//span[contains(@id,'premiumAllocPercOut')]"));
		            int baseRiderCount=  baseRider.size();
		            System.out.println("BaseRiderCount is  " + baseRiderCount);
		            if( baseRiderCount> 0) {
		                for(int i=0; i< baseRiderCount;i++)
		                {
		                 String Funds = "Fund"+ i+1;
		                         
		                    System.out.println("i is  " + i);
		                   String premiumAllocPercOut2 = getJsonData(var_CSVData , "$.records["+var_Count+"]."+Funds+"");

		                    //input[@type='text'][@name='outerForm:grid:7:premiumAllocPercOut2']
		                    driver.findElement(By.xpath("//span[@id='outerForm:grid:" + i + ":premiumAllocPercOut2']")).sendKeys(premiumAllocPercOut2);

		                    // Add Screenshot will only work for Android platform

		                    takeScreenshot();

		                    System.out.println("next value of i is  " + i);

		                }
		            }

		            }
		        catch(Exception e) { PrintVal("Renderer Script Exception : "+e.toString()); }
		        PrintVal("Executed Step = CALLRENDERERSCRIPT,COUNTREINSTATEMENTBENEFITSANDSUBMIT");
		        WAIT.$(2);
		 
		 ;} 
		 catch(Exception e) { PrintVal("Renderer Script Exception : "+e.toString()); } 
		 PrintVal("Executed Step = CALLRENDERERSCRIPT,FUNDSELECTIONUAT2"); 
        TAP.$("ELE_BUTTON_SUBMIT","//input[@type='submit'][@name='outerForm:Submit']","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_UNITSAFTERADDINGBENEFIT","//input[@type='text'][@name='outerForm:unitsOfInsurance']	","0.000","FALSE","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_REQUESTPREMIUNAMNT","//input[@id='outerForm:annualPremium']",var_CashWithApplication,"TRUE","TGTYPESCREENREG");

        SELECT.$("ELE_DRPDWN_GUARWITHDRAWALMETHOD","//select[@id='outerForm:gridSupp:1:guarWithdrawalMethodOut3']","Single","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SUBMIT","//input[@type='submit'][@name='outerForm:Submit']","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_CANCEL","//input[@type='submit'][@name='outerForm:Cancel']","TGTYPESCREENREG");

        TAP.$("ELE_LINK_ISSUE","//span[@id='outerForm:IssueText']","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_EFFECTIVEDATE","//input[@type='text'][@name='outerForm:effectiveDate']",var_EffectiveDatenew,"FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_ISSUE","//input[@id='outerForm:Issue']","TGTYPESCREENREG");

        TAP.$("ELE_LINK_SETTLE","//span[text()='Settle']	","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_PAYMENTATSETTLE","//input[@id='outerForm:paymentAtSettle']",var_CashWithApplication,"TRUE","TGTYPESCREENREG");

		String var_PolicyExchangeCheck;
                 LOGGER.info("Executed Step = VAR,String,var_PolicyExchangeCheck,TGTYPESCREENREG");
		var_PolicyExchangeCheck = getJsonData(var_CSVData , "$.records["+var_Count+"].Clients/Endorsements/Exclusions/FundSelection?PolicyExchange");
                 LOGGER.info("Executed Step = STORE,var_PolicyExchangeCheck,var_CSVData,$.records[{var_Count}].Clients/Endorsements/Exclusions/FundSelection?PolicyExchange,TGTYPESCREENREG");
        LOGGER.info("Executed Step = START IF");
        if (check(var_PolicyExchangeCheck,"=",var_PolicyExchangeCheck,"TGTYPESCREENREG")) {
         LOGGER.info("Executed Step = IF,var_PolicyExchangeCheck,=,var_PolicyExchangeCheck,TGTYPESCREENREG");
        CALL.$("ValidatePolicyExchangeUAT2","TGTYPESCREENREG");

		String var_ModalPremium = "null";
                 LOGGER.info("Executed Step = VAR,String,var_ModalPremium,null,TGTYPESCREENREG");
		var_ModalPremium = ActionWrapper.getElementValue("ELE_GETVAL_MODALPREMIUMAMNTPOLICYEXCHANGE", "//span[@id='outerForm:modalPremium']");
                 LOGGER.info("Executed Step = STORE,var_ModalPremium,ELE_GETVAL_MODALPREMIUMAMNTPOLICYEXCHANGE,$.records[{var_Count}].EstimatedAmount,TGTYPESCREENREG");
		String var_ExchangeType = "null";
                 LOGGER.info("Executed Step = VAR,String,var_ExchangeType,null,TGTYPESCREENREG");
		var_ExchangeType = getJsonData(var_CSVData , "$.records["+var_Count+"].ExchangeType");
                 LOGGER.info("Executed Step = STORE,var_ExchangeType,var_CSVData,$.records[{var_Count}].ExchangeType,TGTYPESCREENREG");
		String var_ReplacementType = "null";
                 LOGGER.info("Executed Step = VAR,String,var_ReplacementType,null,TGTYPESCREENREG");
		var_ReplacementType = getJsonData(var_CSVData , "$.records["+var_Count+"].ReplacementType");
                 LOGGER.info("Executed Step = STORE,var_ReplacementType,var_CSVData,$.records[{var_Count}].ReplacementType,TGTYPESCREENREG");
		String var_PriorCompanyName = "null";
                 LOGGER.info("Executed Step = VAR,String,var_PriorCompanyName,null,TGTYPESCREENREG");
		var_PriorCompanyName = getJsonData(var_CSVData , "$.records["+var_Count+"].PriorCompanName");
                 LOGGER.info("Executed Step = STORE,var_PriorCompanyName,var_CSVData,$.records[{var_Count}].PriorCompanName,TGTYPESCREENREG");
		String var_PriorInvestmentType = "null";
                 LOGGER.info("Executed Step = VAR,String,var_PriorInvestmentType,null,TGTYPESCREENREG");
		var_PriorInvestmentType = getJsonData(var_CSVData , "$.records["+var_Count+"].PriorInvestmentType");
                 LOGGER.info("Executed Step = STORE,var_PriorInvestmentType,var_CSVData,$.records[{var_Count}].PriorInvestmentType,TGTYPESCREENREG");
		String var_PolicyStatus = "null";
                 LOGGER.info("Executed Step = VAR,String,var_PolicyStatus,null,TGTYPESCREENREG");
		var_PolicyStatus = getJsonData(var_CSVData , "$.records["+var_Count+"].Status");
                 LOGGER.info("Executed Step = STORE,var_PolicyStatus,var_CSVData,$.records[{var_Count}].Status,TGTYPESCREENREG");
		String var_RollOverType = "null";
                 LOGGER.info("Executed Step = VAR,String,var_RollOverType,null,TGTYPESCREENREG");
		var_RollOverType = getJsonData(var_CSVData , "$.records["+var_Count+"].RollOverType");
                 LOGGER.info("Executed Step = STORE,var_RollOverType,var_CSVData,$.records[{var_Count}].RollOverType,TGTYPESCREENREG");
        SELECT.$("ELE_DRPDWN_ROLLOVERTYPE","//select[@id='outerForm:rolloverType']",var_RollOverType,"TGTYPESCREENREG");

		String var_RolloverPremium = "null";
                 LOGGER.info("Executed Step = VAR,String,var_RolloverPremium,null,TGTYPESCREENREG");
		var_RolloverPremium = getJsonData(var_CSVData , "$.records["+var_Count+"].RolloverPremium");
                 LOGGER.info("Executed Step = STORE,var_RolloverPremium,var_CSVData,$.records[{var_Count}].RolloverPremium,TGTYPESCREENREG");
		try { 
		 org.openqa.selenium.support.ui.Select select = new org.openqa.selenium.support.ui.Select(driver.findElement(By.xpath("//select[@id='outerForm:rolloverType']")));
		                    // Add Screenshot will only work for Android platform

		                    takeScreenshot();

		                    WebElement option = select.getFirstSelectedOption();
		                    String defaultItem = option.getText();
		                    System.out.println(defaultItem);

		                    if (!defaultItem.contains("No Rollover")) {
		                        WebElement eleRolloverPremium = driver.findElement(By.xpath("//input[@id='outerForm:rolloverPremium']"));
		                        eleRolloverPremium.clear();
		                        eleRolloverPremium.sendKeys(var_RolloverPremium);

		                        Thread.sleep(500);

		                        WebElement eleModalPremium = driver.findElement(By.cssSelector("span[id='outerForm:modalPremium']"));
		                        String strModalPremium = eleModalPremium.getText();
		                        strModalPremium = strModalPremium.replace(",","");

		                        var_RolloverPremium = var_RolloverPremium.replace(",","");

		                        Double doubleModalPremium = Double.parseDouble(strModalPremium);
		                        Double doubleRollverPremium = Double.parseDouble(var_RolloverPremium);

		                        Double remainingValue = doubleModalPremium - doubleRollverPremium;

		                        WebElement elePaymentAtSettle = driver.findElement(By.xpath("//input[@id='outerForm:paymentAtSettle']"));
		                        elePaymentAtSettle.clear();
		                        elePaymentAtSettle.sendKeys(remainingValue.toString());

		                        takeScreenshot();

		                    } else {
		                        WebElement elePaymentAtSettle = driver.findElement(By.xpath("//input[@id='outerForm:paymentAtSettle']"));
		                        elePaymentAtSettle.clear();
		                        elePaymentAtSettle.sendKeys(var_CashWithApplication);
		                    }

		                    if (defaultItem.contains("Direct Transfer") || defaultItem.contains("External Rollover")) {
		                        driver.findElement(By.xpath("//span[@id='outerForm:PolicyExchangeText']")).click();
		                        // Add Screenshot will only work for Android platform

		                        takeScreenshot();

		                        driver.findElement(By.xpath("//img[@id='outerForm:gnAddImage']")).click();
		                        // Add Screenshot will only work for Android platform

		                        takeScreenshot();

		                        org.openqa.selenium.support.ui.Select select1 = new org.openqa.selenium.support.ui.Select(driver.findElement(By.xpath("//select[@id='outerForm:status']")));
		                        select1.selectByVisibleText(var_PolicyStatus);
		                        // Add Screenshot will only work for Android platform

		                        takeScreenshot();

		                        Thread.sleep(500);
		                        org.openqa.selenium.support.ui.Select select2 = new org.openqa.selenium.support.ui.Select(driver.findElement(By.xpath("//select[@id='outerForm:exchangeCode']")));
		                        select2.selectByVisibleText(var_ExchangeType);
		                        // Add Screenshot will only work for Android platform

		                        takeScreenshot();

		                        Thread.sleep(500);
		                        org.openqa.selenium.support.ui.Select select3 = new org.openqa.selenium.support.ui.Select(driver.findElement(By.xpath("//select[@id='outerForm:replacementType']")));
		                        select3.selectByVisibleText(var_ReplacementType);
		                        // Add Screenshot will only work for Android platform

		                        takeScreenshot();

		                        Thread.sleep(500);
		                        driver.findElement(By.xpath("//input[@id='outerForm:estimateExchangeAmt']")).clear();
		                        driver.findElement(By.xpath("//input[@id='outerForm:estimateExchangeAmt']")).sendKeys(var_ModalPremium);
		                        // Add Screenshot will only work for Android platform

		                        takeScreenshot();

		                        Thread.sleep(500);

		                        driver.findElement(By.xpath("//input[@id='outerForm:cashReceivedFromExch']")).clear();
		                        driver.findElement(By.xpath("//input[@id='outerForm:cashReceivedFromExch']")).sendKeys(var_ModalPremium);
		                        driver.findElement(By.xpath("//input[@id='outerForm:cashReceivedDate_input']")).sendKeys(var_EffectiveDate);
		                        Thread.sleep(500);
		                        driver.findElement(By.xpath("//input[@id='outerForm:priorCompanyName']")).sendKeys(var_PriorCompanyName);
		                        Thread.sleep(500);
		                        org.openqa.selenium.support.ui.Select select4 = new org.openqa.selenium.support.ui.Select(driver.findElement(By.xpath("//select[@id='outerForm:priorInvestmentType']")));
		                        select4.selectByVisibleText(var_PriorInvestmentType);
		                        // Add Screenshot will only work for Android platform

		                        takeScreenshot();

		                        Thread.sleep(500);
		                        org.openqa.selenium.support.ui.Select select5 = new org.openqa.selenium.support.ui.Select(driver.findElement(By.xpath("//select[@id='outerForm:currencyCode']")));
		                        select5.selectByVisibleText("DOLLAR [US]");
		                        Thread.sleep(500);
		                        driver.findElement(By.xpath("//input[@type='submit'][@name='outerForm:Submit']")).click();
		                        // Add Screenshot will only work for Android platform

		                        takeScreenshot();

		                        Thread.sleep(500);
		                        driver.findElement(By.xpath("//input[@type='submit'][@name='outerForm:Cancel']")).click();
		                        Thread.sleep(500);
		                        driver.findElement(By.xpath("//input[@type='submit'][@name='outerForm:Cancel']")).click();
		                        Thread.sleep(500);

		                        driver.findElement(By.xpath("//input[@type='submit'][@name='outerForm:Submit']")).click();
		                        // Add Screenshot will only work for Android platform

		                        takeScreenshot();

		                        Thread.sleep(500);

		                        WebElement eleConfirmSubmit = driver.findElement(By.xpath("//input[@type='submit'][@name='outerForm:ConfirmSubmit']"));
		                        if (eleConfirmSubmit.isDisplayed()){
		                            eleConfirmSubmit.click();
		                            takeScreenshot();
		                            Thread.sleep(500);
		                        }

		                        Assert.assertTrue(driver.findElement(By.xpath("//li[@class='MessageInfo']")).getText().equals("Request completed successfully."));
		                        // Add Screenshot will only work for Android platform

		                        takeScreenshot();

		                        Thread.sleep(500);

		                        driver.findElement(By.xpath("//input[@type='submit'][@name='outerForm:Cancel']")).click();
		                        Thread.sleep(500);
		                        // Add Screenshot will only work for Android platform

		                        takeScreenshot();

		                    } else if (defaultItem.contains("No Rollover")){
		                        driver.findElement(By.xpath("//input[@type='submit'][@name='outerForm:Submit']")).click();
		                        // Add Screenshot will only work for Android platform

		                        takeScreenshot();

		                        Assert.assertTrue(driver.findElement(By.xpath("//li[@class='MessageInfo']")).getText().equals("Request completed successfully."));
		                        // Add Screenshot will only work for Android platform

		                        takeScreenshot();

		                    } else {

		                        driver.findElement(By.xpath("//input[@type='submit'][@name='outerForm:Submit']")).click();
		                        // Add Screenshot will only work for Android platform

		                        takeScreenshot();

		                        WebElement eleConfirmSubmit = driver.findElement(By.xpath("//input[@type='submit'][@name='outerForm:ConfirmSubmit']"));
		                        if (eleConfirmSubmit.isDisplayed()){
		                            eleConfirmSubmit.click();
		                            takeScreenshot();
		                            Thread.sleep(500);
		                        }

		                        Assert.assertTrue(driver.findElement(By.xpath("//li[@class='MessageInfo']")).getText().equals("Request completed successfully."));
		                        // Add Screenshot will only work for Android platform

		                        takeScreenshot();

		                    }

		                    ;
		 
		 ;} 
		 catch(Exception e) { PrintVal("Renderer Script Exception : "+e.toString()); } 
		 PrintVal("Executed Step = CALLRENDERERSCRIPT,VALIDATEPOLICYEXCHANGEUAT3"); 
        }
          LOGGER.info("Executed Step = ENDIF,TGTYPESCREENREG");
        CALL.$("WritePolicyQuickInquiryToCSV","TGTYPESCREENREG");

        HOVER.$("ELE_MENU_INQUIRY","//td[text()='Inquiry']","TGTYPESCREENREG");

        TAP.$("ELE_SUBMEN_QUICKINQUIRY","//td[contains(text(),'Quick Inquiry')]","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_POLICYNUMBER","//input[@type='text'][@name='outerForm:policyNumber']",var_PolicyNumber,"FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_CONTINUE","//input[@type='submit'][@name='outerForm:Continue']","TGTYPESCREENREG");

		writeToCSV ("Policy Number " , ActionWrapper.getElementValue("ELE_GETVAL_POLICYNUMBER", "//span[@id='outerForm:policyNumber']"));
                 LOGGER.info("Executed Step = WRITETOCSV,ELE_GETVAL_POLICYNUMBER,Policy Number,TGTYPESCREENREG");
		writeToCSV ("Status " , ActionWrapper.getElementValue("ELE_GETVAL_STATUS", "//span[@id='outerForm:statusText']"));
                 LOGGER.info("Executed Step = WRITETOCSV,ELE_GETVAL_STATUS,Status,TGTYPESCREENREG");
		writeToCSV ("Effective Date " , ActionWrapper.getElementValue("ELE_GETVAL_EFFECTIVEDATETEXTFROMGRID", "//span[@id='outerForm:grid:0:effectiveDateOut2']"));
                 LOGGER.info("Executed Step = WRITETOCSV,ELE_GETVAL_EFFECTIVEDATETEXTFROMGRID,Effective Date,TGTYPESCREENREG");
		writeToCSV ("State country " , ActionWrapper.getElementValue("ELE_GETVAL_STATECOUNTRY", "//span[@id='outerForm:ownerCityStateZip']"));
                 LOGGER.info("Executed Step = WRITETOCSV,ELE_GETVAL_STATECOUNTRY,State country,TGTYPESCREENREG");
		writeToCSV ("Agent Number " , ActionWrapper.getElementValue("ELE_GETVAL_AGENTNUMBER", "//span[@id='outerForm:agentNumber']"));
                 LOGGER.info("Executed Step = WRITETOCSV,ELE_GETVAL_AGENTNUMBER,Agent Number,TGTYPESCREENREG");
		var_Count = var_Count + ( 1 );
                 LOGGER.info("Executed Step = INCREASEVAR,var_Count,1,TGTYPESCREENREG");
    	}
          LOGGER.info("Executed Step = WEND,TGTYPESCREENREG");
    	resetAttemps();

    }


    @Test
    public void tc01createnewbusinesspolicyuat2regressionedits() {

        // No need to declare / print method name here, handled by TestNg listener
        // declare("tc01createnewbusinesspolicyuat2regressionedits");

        CALL.$("LoginToGIASFandGRegression","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_USERNAME","outerForm:usernameTGWEBCOMMAIdTGWEBCOMMA0","x5990","FALSE","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_PASSWORD","outerForm:passwordTGWEBCOMMAIdTGWEBCOMMA0","cnx12345","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_LOGIN","outerForm:loginButtonTGWEBCOMMAIdTGWEBCOMMA0","TGTYPESCREENREG");

		int var_Count = 0;
                 LOGGER.info("Executed Step = VAR,Integer,var_Count,0,TGTYPESCREENREG");
        LOGGER.info("Executed Step = REPEAT IF");
    	while (check(var_Count,"<",1,"TGTYPESCREENREG")) {
          LOGGER.info("Executed Step = WHILE,var_Count,<,1,TGTYPESCREENREG");
		com.jayway.jsonpath.ReadContext var_CSVData = getJsonData("C:/GIAS_Automation/FandG Regression/InputData/FandGReg_CreateAPolicy.csv");
                 LOGGER.info("Executed Step = VAR,Json,var_CSVData,C:/GIAS_Automation/FandG Regression/InputData/FandGReg_CreateAPolicy.csv,TGTYPESCREENREG");
		String var_EffectiveDate = "null";
                 LOGGER.info("Executed Step = VAR,String,var_EffectiveDate,null,TGTYPESCREENREG");
		var_EffectiveDate = getJsonData(var_CSVData , "$.records["+var_Count+"].EffectiveDate");
                 LOGGER.info("Executed Step = STORE,var_EffectiveDate,var_CSVData,$.records[{var_Count}].EffectiveDate,TGTYPESCREENREG");
        CALL.$("AddNewBusinessPolicyUAT2Regression2","TGTYPESCREENREG");

        HOVER.$("ELE_MENU_NEWBUSINESS","//td[text()='New Business']","TGTYPESCREENREG");

        TAP.$("ELE_SUBMEN_APPLICATIONENTRYUPDATE","//td[text()='Application Entry/Update']","TGTYPESCREENREG");

        TAP.$("ELE_RNDBTN_ADD","//img[@id='outerForm:gnAddImage']","TGTYPESCREENREG");

		String var_PolicyNumber;
                 LOGGER.info("Executed Step = VAR,String,var_PolicyNumber,TGTYPESCREENREG");
		var_PolicyNumber = getJsonData(var_CSVData , "$.records["+var_Count+"].PolicyNumber");
                 LOGGER.info("Executed Step = STORE,var_PolicyNumber,var_CSVData,$.records[{var_Count}].PolicyNumber,TGTYPESCREENREG");
		String var_Name1;
                 LOGGER.info("Executed Step = VAR,String,var_Name1,TGTYPESCREENREG");
		var_Name1 = getJsonData(var_CSVData , "$.records["+var_Count+"].Name1");
                 LOGGER.info("Executed Step = STORE,var_Name1,var_CSVData,$.records[{var_Count}].Name1,TGTYPESCREENREG");
		String var_FirstName1;
                 LOGGER.info("Executed Step = VAR,String,var_FirstName1,TGTYPESCREENREG");
		var_FirstName1 = getJsonData(var_CSVData , "$.records["+var_Count+"].First1");
                 LOGGER.info("Executed Step = STORE,var_FirstName1,var_CSVData,$.records[{var_Count}].First1,TGTYPESCREENREG");
		String var_LastName1;
                 LOGGER.info("Executed Step = VAR,String,var_LastName1,TGTYPESCREENREG");
		var_LastName1 = getJsonData(var_CSVData , "$.records["+var_Count+"].Last1");
                 LOGGER.info("Executed Step = STORE,var_LastName1,var_CSVData,$.records[{var_Count}].Last1,TGTYPESCREENREG");
		String var_BirthStateCountry1;
                 LOGGER.info("Executed Step = VAR,String,var_BirthStateCountry1,TGTYPESCREENREG");
		var_BirthStateCountry1 = getJsonData(var_CSVData , "$.records["+var_Count+"].BirthStateCountry1");
                 LOGGER.info("Executed Step = STORE,var_BirthStateCountry1,var_CSVData,$.records[{var_Count}].BirthStateCountry1,TGTYPESCREENREG");
		String var_DateOfBirth1;
                 LOGGER.info("Executed Step = VAR,String,var_DateOfBirth1,TGTYPESCREENREG");
		var_DateOfBirth1 = getJsonData(var_CSVData , "$.records["+var_Count+"].DateOfBirth1");
                 LOGGER.info("Executed Step = STORE,var_DateOfBirth1,var_CSVData,$.records[{var_Count}].DateOfBirth1,TGTYPESCREENREG");
		String var_Gender1;
                 LOGGER.info("Executed Step = VAR,String,var_Gender1,TGTYPESCREENREG");
		var_Gender1 = getJsonData(var_CSVData , "$.records["+var_Count+"].Gender1");
                 LOGGER.info("Executed Step = STORE,var_Gender1,var_CSVData,$.records[{var_Count}].Gender1,TGTYPESCREENREG");
		String var_IDType1;
                 LOGGER.info("Executed Step = VAR,String,var_IDType1,TGTYPESCREENREG");
		var_IDType1 = getJsonData(var_CSVData , "$.records["+var_Count+"].IDType1");
                 LOGGER.info("Executed Step = STORE,var_IDType1,var_CSVData,$.records[{var_Count}].IDType1,TGTYPESCREENREG");
		String var_IDNumber1;
                 LOGGER.info("Executed Step = VAR,String,var_IDNumber1,TGTYPESCREENREG");
		var_IDNumber1 = getJsonData(var_CSVData , "$.records["+var_Count+"].IDNumber1");
                 LOGGER.info("Executed Step = STORE,var_IDNumber1,var_CSVData,$.records[{var_Count}].IDNumber1,TGTYPESCREENREG");
		String var_FirstName2;
                 LOGGER.info("Executed Step = VAR,String,var_FirstName2,TGTYPESCREENREG");
		var_FirstName2 = getJsonData(var_CSVData , "$.records["+var_Count+"].First2");
                 LOGGER.info("Executed Step = STORE,var_FirstName2,var_CSVData,$.records[{var_Count}].First2,TGTYPESCREENREG");
		String var_LastName2;
                 LOGGER.info("Executed Step = VAR,String,var_LastName2,TGTYPESCREENREG");
		var_LastName2 = getJsonData(var_CSVData , "$.records["+var_Count+"].Last2");
                 LOGGER.info("Executed Step = STORE,var_LastName2,var_CSVData,$.records[{var_Count}].Last2,TGTYPESCREENREG");
		String var_BirthStateCountry2;
                 LOGGER.info("Executed Step = VAR,String,var_BirthStateCountry2,TGTYPESCREENREG");
		var_BirthStateCountry2 = getJsonData(var_CSVData , "$.records["+var_Count+"].BirthStateCountry2");
                 LOGGER.info("Executed Step = STORE,var_BirthStateCountry2,var_CSVData,$.records[{var_Count}].BirthStateCountry2,TGTYPESCREENREG");
		String var_DateOfBirth2;
                 LOGGER.info("Executed Step = VAR,String,var_DateOfBirth2,TGTYPESCREENREG");
		var_DateOfBirth2 = getJsonData(var_CSVData , "$.records["+var_Count+"].DateOfBirth2");
                 LOGGER.info("Executed Step = STORE,var_DateOfBirth2,var_CSVData,$.records[{var_Count}].DateOfBirth2,TGTYPESCREENREG");
		String var_Gender2;
                 LOGGER.info("Executed Step = VAR,String,var_Gender2,TGTYPESCREENREG");
		var_Gender2 = getJsonData(var_CSVData , "$.records["+var_Count+"].Gender2");
                 LOGGER.info("Executed Step = STORE,var_Gender2,var_CSVData,$.records[{var_Count}].Gender2,TGTYPESCREENREG");
		String var_IDType2;
                 LOGGER.info("Executed Step = VAR,String,var_IDType2,TGTYPESCREENREG");
		var_IDType2 = getJsonData(var_CSVData , "$.records["+var_Count+"].IDType2");
                 LOGGER.info("Executed Step = STORE,var_IDType2,var_CSVData,$.records[{var_Count}].IDType2,TGTYPESCREENREG");
		String var_IDNumber2;
                 LOGGER.info("Executed Step = VAR,String,var_IDNumber2,TGTYPESCREENREG");
		var_IDNumber2 = getJsonData(var_CSVData , "$.records["+var_Count+"].IDNumber2");
                 LOGGER.info("Executed Step = STORE,var_IDNumber2,var_CSVData,$.records[{var_Count}].IDNumber2,TGTYPESCREENREG");
		String var_AddressLine11;
                 LOGGER.info("Executed Step = VAR,String,var_AddressLine11,TGTYPESCREENREG");
		var_AddressLine11 = getJsonData(var_CSVData , "$.records["+var_Count+"].AddressLine1_1");
                 LOGGER.info("Executed Step = STORE,var_AddressLine11,var_CSVData,$.records[{var_Count}].AddressLine1_1,TGTYPESCREENREG");
		String var_City1;
                 LOGGER.info("Executed Step = VAR,String,var_City1,TGTYPESCREENREG");
		var_City1 = getJsonData(var_CSVData , "$.records["+var_Count+"].City1");
                 LOGGER.info("Executed Step = STORE,var_City1,var_CSVData,$.records[{var_Count}].City1,TGTYPESCREENREG");
		String var_StateandCountry1;
                 LOGGER.info("Executed Step = VAR,String,var_StateandCountry1,TGTYPESCREENREG");
		var_StateandCountry1 = getJsonData(var_CSVData , "$.records["+var_Count+"].StateandCountry1");
                 LOGGER.info("Executed Step = STORE,var_StateandCountry1,var_CSVData,$.records[{var_Count}].StateandCountry1,TGTYPESCREENREG");
		String var_ZipCode1;
                 LOGGER.info("Executed Step = VAR,String,var_ZipCode1,TGTYPESCREENREG");
		var_ZipCode1 = getJsonData(var_CSVData , "$.records["+var_Count+"].ZipCode1");
                 LOGGER.info("Executed Step = STORE,var_ZipCode1,var_CSVData,$.records[{var_Count}].ZipCode1,TGTYPESCREENREG");
		String var_AddressLine12;
                 LOGGER.info("Executed Step = VAR,String,var_AddressLine12,TGTYPESCREENREG");
		var_AddressLine12 = getJsonData(var_CSVData , "$.records["+var_Count+"].AddressLine1_2");
                 LOGGER.info("Executed Step = STORE,var_AddressLine12,var_CSVData,$.records[{var_Count}].AddressLine1_2,TGTYPESCREENREG");
		String var_City2;
                 LOGGER.info("Executed Step = VAR,String,var_City2,TGTYPESCREENREG");
		var_City2 = getJsonData(var_CSVData , "$.records["+var_Count+"].City2");
                 LOGGER.info("Executed Step = STORE,var_City2,var_CSVData,$.records[{var_Count}].City2,TGTYPESCREENREG");
		String var_StateandCountry2;
                 LOGGER.info("Executed Step = VAR,String,var_StateandCountry2,TGTYPESCREENREG");
		var_StateandCountry2 = getJsonData(var_CSVData , "$.records["+var_Count+"].StateandCountry2");
                 LOGGER.info("Executed Step = STORE,var_StateandCountry2,var_CSVData,$.records[{var_Count}].StateandCountry2,TGTYPESCREENREG");
		String var_ZipCode2;
                 LOGGER.info("Executed Step = VAR,String,var_ZipCode2,TGTYPESCREENREG");
		var_ZipCode2 = getJsonData(var_CSVData , "$.records["+var_Count+"].ZipCode2");
                 LOGGER.info("Executed Step = STORE,var_ZipCode2,var_CSVData,$.records[{var_Count}].ZipCode2,TGTYPESCREENREG");
		String var_SelectTypeTwo;
                 LOGGER.info("Executed Step = VAR,String,var_SelectTypeTwo,TGTYPESCREENREG");
		var_SelectTypeTwo = getJsonData(var_CSVData , "$.records["+var_Count+"].SelectTypeTwo");
                 LOGGER.info("Executed Step = STORE,var_SelectTypeTwo,var_CSVData,$.records[{var_Count}].SelectTypeTwo,TGTYPESCREENREG");
		String var_Name2;
                 LOGGER.info("Executed Step = VAR,String,var_Name2,TGTYPESCREENREG");
		var_Name2 = getJsonData(var_CSVData , "$.records["+var_Count+"].Name2");
                 LOGGER.info("Executed Step = STORE,var_Name2,var_CSVData,$.records[{var_Count}].Name2,TGTYPESCREENREG");
		String var_SelectTypeThree;
                 LOGGER.info("Executed Step = VAR,String,var_SelectTypeThree,TGTYPESCREENREG");
		var_SelectTypeThree = getJsonData(var_CSVData , "$.records["+var_Count+"].SelectTypeThree");
                 LOGGER.info("Executed Step = STORE,var_SelectTypeThree,var_CSVData,$.records[{var_Count}].SelectTypeThree,TGTYPESCREENREG");
		String var_ApplicationDate;
                 LOGGER.info("Executed Step = VAR,String,var_ApplicationDate,TGTYPESCREENREG");
		var_ApplicationDate = getJsonData(var_CSVData , "$.records["+var_Count+"].ApplicationDate");
                 LOGGER.info("Executed Step = STORE,var_ApplicationDate,var_CSVData,$.records[{var_Count}].ApplicationDate,TGTYPESCREENREG");
		String var_IssueStateCountry;
                 LOGGER.info("Executed Step = VAR,String,var_IssueStateCountry,TGTYPESCREENREG");
		var_IssueStateCountry = getJsonData(var_CSVData , "$.records["+var_Count+"].IssueStateCountry");
                 LOGGER.info("Executed Step = STORE,var_IssueStateCountry,var_CSVData,$.records[{var_Count}].IssueStateCountry,TGTYPESCREENREG");
		String var_EffectiveDatenew;
                 LOGGER.info("Executed Step = VAR,String,var_EffectiveDatenew,TGTYPESCREENREG");
		var_EffectiveDatenew = getJsonData(var_CSVData , "$.records["+var_Count+"].EffectiveDate");
                 LOGGER.info("Executed Step = STORE,var_EffectiveDatenew,var_CSVData,$.records[{var_Count}].EffectiveDate,TGTYPESCREENREG");
		String var_CashWithApplication;
                 LOGGER.info("Executed Step = VAR,String,var_CashWithApplication,TGTYPESCREENREG");
		var_CashWithApplication = getJsonData(var_CSVData , "$.records["+var_Count+"].CashWithApplication");
                 LOGGER.info("Executed Step = STORE,var_CashWithApplication,var_CSVData,$.records[{var_Count}].CashWithApplication,TGTYPESCREENREG");
		String var_TaxQualifiedDescription;
                 LOGGER.info("Executed Step = VAR,String,var_TaxQualifiedDescription,TGTYPESCREENREG");
		var_TaxQualifiedDescription = getJsonData(var_CSVData , "$.records["+var_Count+"].TaxQualifiedDescription");
                 LOGGER.info("Executed Step = STORE,var_TaxQualifiedDescription,var_CSVData,$.records[{var_Count}].TaxQualifiedDescription,TGTYPESCREENREG");
		String var_AgentNumberOne;
                 LOGGER.info("Executed Step = VAR,String,var_AgentNumberOne,TGTYPESCREENREG");
		var_AgentNumberOne = getJsonData(var_CSVData , "$.records["+var_Count+"].AgentNumberOne");
                 LOGGER.info("Executed Step = STORE,var_AgentNumberOne,var_CSVData,$.records[{var_Count}].AgentNumberOne,TGTYPESCREENREG");
		String var_SituationCode1;
                 LOGGER.info("Executed Step = VAR,String,var_SituationCode1,TGTYPESCREENREG");
		var_SituationCode1 = getJsonData(var_CSVData , "$.records["+var_Count+"].SituationCode1");
                 LOGGER.info("Executed Step = STORE,var_SituationCode1,var_CSVData,$.records[{var_Count}].SituationCode1,TGTYPESCREENREG");
		String var_SplitPercentage1;
                 LOGGER.info("Executed Step = VAR,String,var_SplitPercentage1,TGTYPESCREENREG");
		var_SplitPercentage1 = getJsonData(var_CSVData , "$.records["+var_Count+"].SplitPercentage1");
                 LOGGER.info("Executed Step = STORE,var_SplitPercentage1,var_CSVData,$.records[{var_Count}].SplitPercentage1,TGTYPESCREENREG");
		String var_AgentNumberTwo;
                 LOGGER.info("Executed Step = VAR,String,var_AgentNumberTwo,TGTYPESCREENREG");
		var_AgentNumberTwo = getJsonData(var_CSVData , "$.records["+var_Count+"].AgentNumberTwo");
                 LOGGER.info("Executed Step = STORE,var_AgentNumberTwo,var_CSVData,$.records[{var_Count}].AgentNumberTwo,TGTYPESCREENREG");
		String var_SituationCode2;
                 LOGGER.info("Executed Step = VAR,String,var_SituationCode2,TGTYPESCREENREG");
		var_SituationCode2 = getJsonData(var_CSVData , "$.records["+var_Count+"].SituationCode2");
                 LOGGER.info("Executed Step = STORE,var_SituationCode2,var_CSVData,$.records[{var_Count}].SituationCode2,TGTYPESCREENREG");
		String var_SplitPercentage2;
                 LOGGER.info("Executed Step = VAR,String,var_SplitPercentage2,TGTYPESCREENREG");
		var_SplitPercentage2 = getJsonData(var_CSVData , "$.records["+var_Count+"].SplitPercentage2");
                 LOGGER.info("Executed Step = STORE,var_SplitPercentage2,var_CSVData,$.records[{var_Count}].SplitPercentage2,TGTYPESCREENREG");
		try { 
		 var_AgentNumberOne = "000112680";
				var_AgentNumberTwo = "000112680";
				var_SituationCode1 = "01";
				var_SituationCode2 = "01";
		 
		 ;} 
		 catch(Exception e) { PrintVal("Renderer Script Exception : "+e.toString()); } 
		 PrintVal("Executed Step = CALLRENDERERSCRIPT,ASSIGNSTATICVALUEFORAGENTNUMBERANDSITUATIONCODE"); 
        TYPE.$("ELE_TXTBOX_POLICYNUMBER","//input[@type='text'][@name='outerForm:policyNumber']",var_PolicyNumber,"FALSE","TGTYPESCREENREG");

        SELECT.$("ELE_DRPDWN_CURRENCYREGRESSION","//select[@id='outerForm:currencyCode']","DOLLAR [US]","TGTYPESCREENREG");

        SELECT.$("ELE_DRPDWN_STATECOUNTRY","//select[@name='outerForm:countryAndStateCode']	",var_StateandCountry1,"TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_CASHWITHAPPLICATION","//input[@type='text'][@name='outerForm:cashWithApplication']",var_CashWithApplication,"FALSE","TGTYPESCREENREG");

        SELECT.$("ELE_DRPDWN_PAYMENTFREQUENCY","//select[@name='outerForm:paymentMode']","Annual","TGTYPESCREENREG");

        SELECT.$("ELE_DRPDWN_PAYMENTMETHOD","//select[@name='outerForm:paymentCode']","No Notice","TGTYPESCREENREG");

        SELECT.$("ELE_DRPDWN_TAXQUALIFIEDDESCRIPTION","//select[@name='outerForm:taxQualifiedCode']",var_TaxQualifiedDescription,"TGTYPESCREENREG");

        SELECT.$("ELE_DRPDWN_TAXWITHHOLDING","//select[@name='outerForm:taxWithholdingCode']","No Withholding","TGTYPESCREENREG");

        TAP.$("ELE_LINK_SELECTCLIENT","//span[text()='Select Client']","TGTYPESCREENREG");

        TAP.$("ELE_RNDBTN_ADD","//img[@id='outerForm:gnAddImage']","TGTYPESCREENREG");

        LOGGER.info("Executed Step = START IF");
        if (check(var_FirstName2,"=","null","TGTYPESCREENREG")) {
         LOGGER.info("Executed Step = IF,var_FirstName2,=,null,TGTYPESCREENREG");
        TYPE.$("ELE_TXTBOX_FIRSTNAME","//input[@type='text'][@name='outerForm:nameFirst']",var_FirstName1,"FALSE","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_LASTNAME","//input[@type='text'][@name='outerForm:nameLast']",var_LastName1,"FALSE","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_CLIENTDOB","//input[@type='text'][@name='outerForm:dateOfBirth2']",var_DateOfBirth1,"FALSE","TGTYPESCREENREG");

        SELECT.$("ELE_DRPDWN_CLIENTGENDER","//select[@name='outerForm:sexCode2']",var_Gender1,"TGTYPESCREENREG");

        SELECT.$("ELE_DRPDWN_IDTYPE","//select[@name='outerForm:taxIdenUsag2']",var_IDType1,"TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_IDNUMBER","//input[@type='text'][@name='outerForm:identificationNumber2']",var_IDNumber1,"FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SUBMIT","//input[@type='submit'][@name='outerForm:Submit']","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_ADDRESSLINE1","//input[@type='text'][@name='outerForm:addressLineOne']",var_AddressLine11,"FALSE","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_CITY","//input[@type='text'][@name='outerForm:addressCity']",var_City1,"FALSE","TGTYPESCREENREG");

        SELECT.$("ELE_DRPDWN_STATECOUNTRY","//select[@name='outerForm:countryAndStateCode']	",var_StateandCountry1,"TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_ZIPCODE","//input[@type='text'][@name='outerForm:zipCode']",var_ZipCode1,"FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SUBMIT","//input[@type='submit'][@name='outerForm:Submit']","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_CANCEL","//input[@type='submit'][@name='outerForm:Cancel']","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_SEARCHCLIENTNAME","//input[@type='text'][@name='outerForm:grid:individualName']",var_Name1,"FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_FINDPOSTO","//img[@id='outerForm:gnFindImage']","TGTYPESCREENREG");

        SELECT.$("ELE_DRPDWN_RELATIONSHIPGRID","//select[@name='outerForm:grid:0:relationship']","Owner one","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SUBMIT","//input[@type='submit'][@name='outerForm:Submit']","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_AGENTNUMBER","//input[@type='text'][@name='outerForm:grid:0:agentNumberOut2']",var_AgentNumberOne,"FALSE","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_SITUATIONCODE","//input[@type='text'][@name='outerForm:grid:0:agentSituationCodeOut2']",var_SituationCode1,"FALSE","TGTYPESCREENREG");

        }
         else {
          LOGGER.info("Executed Step = ELSE,TGTYPESCREENREG");
        TYPE.$("ELE_TXTBOX_FIRSTNAME","//input[@type='text'][@name='outerForm:nameFirst']",var_FirstName1,"FALSE","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_LASTNAME","//input[@type='text'][@name='outerForm:nameLast']",var_LastName1,"FALSE","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_CLIENTDOB","//input[@type='text'][@name='outerForm:dateOfBirth2']",var_DateOfBirth1,"FALSE","TGTYPESCREENREG");

        SELECT.$("ELE_DRPDWN_CLIENTGENDER","//select[@name='outerForm:sexCode2']",var_Gender1,"TGTYPESCREENREG");

        SELECT.$("ELE_DRPDWN_IDTYPE","//select[@name='outerForm:taxIdenUsag2']",var_IDType1,"TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_IDNUMBER","//input[@type='text'][@name='outerForm:identificationNumber2']",var_IDNumber1,"FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SUBMIT","//input[@type='submit'][@name='outerForm:Submit']","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_ADDRESSLINE1","//input[@type='text'][@name='outerForm:addressLineOne']",var_AddressLine11,"FALSE","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_CITY","//input[@type='text'][@name='outerForm:addressCity']",var_City1,"FALSE","TGTYPESCREENREG");

        SELECT.$("ELE_DRPDWN_STATECOUNTRY","//select[@name='outerForm:countryAndStateCode']	",var_StateandCountry1,"TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_ZIPCODE","//input[@type='text'][@name='outerForm:zipCode']",var_ZipCode1,"FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SUBMIT","//input[@type='submit'][@name='outerForm:Submit']","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_FIRSTNAME","//input[@type='text'][@name='outerForm:nameFirst']",var_FirstName2,"FALSE","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_LASTNAME","//input[@type='text'][@name='outerForm:nameLast']",var_LastName2,"FALSE","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_CLIENTDOB","//input[@type='text'][@name='outerForm:dateOfBirth2']",var_DateOfBirth2,"FALSE","TGTYPESCREENREG");

        SELECT.$("ELE_DRPDWN_CLIENTGENDER","//select[@name='outerForm:sexCode2']",var_Gender2,"TGTYPESCREENREG");

        SELECT.$("ELE_DRPDWN_IDTYPE","//select[@name='outerForm:taxIdenUsag2']",var_IDType2,"TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_IDNUMBER","//input[@type='text'][@name='outerForm:identificationNumber2']",var_IDNumber2,"FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SUBMIT","//input[@type='submit'][@name='outerForm:Submit']","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_ADDRESSLINE1","//input[@type='text'][@name='outerForm:addressLineOne']",var_AddressLine12,"FALSE","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_CITY","//input[@type='text'][@name='outerForm:addressCity']",var_City2,"FALSE","TGTYPESCREENREG");

        SELECT.$("ELE_DRPDWN_STATECOUNTRY","//select[@name='outerForm:countryAndStateCode']	",var_StateandCountry2,"TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_ZIPCODE","//input[@type='text'][@name='outerForm:zipCode']",var_ZipCode2,"FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SUBMIT","//input[@type='submit'][@name='outerForm:Submit']","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_CANCEL","//input[@type='submit'][@name='outerForm:Cancel']","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_SEARCHCLIENTNAME","//input[@type='text'][@name='outerForm:grid:individualName']",var_Name1,"FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_FINDPOSTO","//img[@id='outerForm:gnFindImage']","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_SEARCHCLIENTNAME","//input[@type='text'][@name='outerForm:grid:individualName']",var_Name2,"FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_FINDPOSTO","//img[@id='outerForm:gnFindImage']","TGTYPESCREENREG");

        SELECT.$("ELE_DRPDWN_RELATIONSHIPGRID","//select[@name='outerForm:grid:0:relationship']",var_SelectTypeTwo,"TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SUBMIT","//input[@type='submit'][@name='outerForm:Submit']","TGTYPESCREENREG");

        LOGGER.info("Executed Step = START IF");
        if (check(var_AgentNumberTwo,"=","null","TGTYPESCREENREG")) {
         LOGGER.info("Executed Step = IF,var_AgentNumberTwo,=,null,TGTYPESCREENREG");
        TYPE.$("ELE_TXTBOX_AGENTNUMBER","//input[@type='text'][@name='outerForm:grid:0:agentNumberOut2']",var_AgentNumberOne,"FALSE","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_SITUATIONCODE","//input[@type='text'][@name='outerForm:grid:0:agentSituationCodeOut2']",var_SituationCode1,"FALSE","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_SPLITPERCENTAGE","//input[@type='text'][@name='outerForm:grid:0:agentSplitPercentageOut2']",var_SplitPercentage1,"FALSE","TGTYPESCREENREG");

        }
         else {
          LOGGER.info("Executed Step = ELSE,TGTYPESCREENREG");
        TYPE.$("ELE_TXTBOX_AGENTNUMBER","//input[@type='text'][@name='outerForm:grid:0:agentNumberOut2']",var_AgentNumberOne,"FALSE","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_SITUATIONCODE","//input[@type='text'][@name='outerForm:grid:0:agentSituationCodeOut2']",var_SituationCode1,"FALSE","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_SPLITPERCENTAGE","//input[@type='text'][@name='outerForm:grid:0:agentSplitPercentageOut2']",var_SplitPercentage1,"FALSE","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_AGENTNUMBER2","//input[@type='text'][@name='outerForm:grid:1:agentNumberOut2']",var_AgentNumberTwo,"FALSE","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_SITUATIONCODE2","//input[@type='text'][@name='outerForm:grid:1:agentSituationCodeOut2']",var_SituationCode2,"FALSE","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_SPLITPERCENTAGE2","//input[@type='text'][@name='outerForm:grid:1:agentSplitPercentageOut2']",var_SplitPercentage2,"FALSE","TGTYPESCREENREG");

        }
          LOGGER.info("Executed Step = ENDIF,TGTYPESCREENREG");
        }
          LOGGER.info("Executed Step = ENDIF,TGTYPESCREENREG");
        TAP.$("ELE_BUTTON_SUBMIT","//input[@type='submit'][@name='outerForm:Submit']","TGTYPESCREENREG");

        ASSERT.$("ELE_GETMSG_SUCCESS","//li[@class='MessageInfo']","CONTAINS","successfully","TGTYPESCREENREG");

        CALL.$("SettlePolicyUAT2RegressionAddingFunds","TGTYPESCREENREG");

        TAP.$("ELE_LINK_BENEFITS","//span[text()='Benefits']","TGTYPESCREENREG");

		String var_Plan;
                 LOGGER.info("Executed Step = VAR,String,var_Plan,TGTYPESCREENREG");
		var_Plan = getJsonData(var_CSVData , "$.records["+var_Count+"].Plan");
                 LOGGER.info("Executed Step = STORE,var_Plan,var_CSVData,$.records[{var_Count}].Plan,TGTYPESCREENREG");
        SELECT.$("ELE_DRPDWN_ADDABENEFIT","//select[@name='outerForm:initialPlanCode']",var_Plan,"TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_CONTINUE","//input[@type='submit'][@name='outerForm:Continue']","TGTYPESCREENREG");

        TAP.$("ELE_LINK_FUNDSELECTION","//span[contains(text(),'Fund Selection')]","TGTYPESCREENREG");

		try { 
		      
		        List<WebElement> col = driver.findElements(By.xpath("//tbody[@id='outerForm:gri:tbody_element']//tr"));
		        System.out.println("No of cols are : " +col.size()); 
		if(col.size()>1) {
		        float count = (col.size()-1);
		        int counttotal =(col.size()-1);
		        int totalcount = col.size();
		        String ax = Integer.toString(counttotal);
		        
		        
		        String invertvalue = "0.050000";
		       float ab =Float.valueOf(invertvalue);
		 	   float total = ab*counttotal;
		 	   System.out.println("Total::"+total);
		 	   float abcd = 1 - total;
		 	   String remaining = String.format("%.2f", abcd);
		 	   System.out.println("1111:::::::::"+remaining);
		        
		        
		  
		        for(int i= 0; i<counttotal;i++) {
		        
		        	String a = Integer.toString(i);
		        	
		        	
		        	
		        	String XpathForAllocation ="//input[@id='outerForm:grid:0:premiumAllocPercOut2']";
		        	String XpathForAllocationR = XpathForAllocation.replace("0", a);
		          	WebElement premiumAllocation = driver.findElement(By.xpath(XpathForAllocationR));
		          	System.out.println("inside :::::::::::::::"+XpathForAllocationR);
		          	premiumAllocation.clear();
		          	premiumAllocation.sendKeys(invertvalue);
		          
		          
		        	}
		        
		        String XpathForAllocationn ="//input[@id='outerForm:grid:0:premiumAllocPercOut2']";
		    	String XpathForAllocationRR = XpathForAllocationn.replace("0", ax);
		      	WebElement premiumAllocationn = driver.findElement(By.xpath(XpathForAllocationRR));
		      	System.out.println("inside :::::::::::::::"+premiumAllocationn);
		      	premiumAllocationn.clear();
		      	premiumAllocationn.sendKeys(remaining);

		}

		 else {
				        	String XpathForAllocatione ="//input[@id='outerForm:grid:0:premiumAllocPercOut2']";
				        
				          	WebElement premiumAllocation = driver.findElement(By.xpath(XpathForAllocatione));
				          	System.out.println("inside :::::::::::::::"+XpathForAllocatione);
				          	premiumAllocation.clear();
				          	premiumAllocation.sendKeys("1.000000");
				        	}
		 
		 ;} 
		 catch(Exception e) { PrintVal("Renderer Script Exception : "+e.toString()); } 
		 PrintVal("Executed Step = CALLRENDERERSCRIPT,ADDINGFUNDSCREATEPOLICYREGRESSION"); 
		takeFullScreenShot();
                 LOGGER.info("Executed Step = TAKEFULLSCREENSHOT,TGTYPESCREENREG");
        TAP.$("ELE_BUTTON_SUBMIT","//input[@type='submit'][@name='outerForm:Submit']","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_UNITSAFTERADDINGBENEFIT","//input[@type='text'][@name='outerForm:unitsOfInsurance']	","0.000","FALSE","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_REQUESTPREMIUNAMNT","//input[@id='outerForm:annualPremium']",var_CashWithApplication,"TRUE","TGTYPESCREENREG");

        LOGGER.info("Executed Step = START IF");
        if (check("ELE_DRPDWN_GUARWITHDRAWALMETHODREGRESSION","//select[@id='outerForm:gridSupp:0:guarWithdrawalMethodOut3']","VISIBLE","TGTYPESCREENREG")) {
         LOGGER.info("Executed Step = IF,ELE_DRPDWN_GUARWITHDRAWALMETHODREGRESSION,VISIBLE,TGTYPESCREENREG");
        SELECT.$("ELE_DRPDWN_GUARWITHDRAWALMETHODREGRESSION","//select[@id='outerForm:gridSupp:0:guarWithdrawalMethodOut3']","Single","TGTYPESCREENREG");

        }
          LOGGER.info("Executed Step = ENDIF,TGTYPESCREENREG");
        TAP.$("ELE_BUTTON_SUBMIT","//input[@type='submit'][@name='outerForm:Submit']","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_CANCEL","//input[@type='submit'][@name='outerForm:Cancel']","TGTYPESCREENREG");

        TAP.$("ELE_LINK_ISSUE","//span[@id='outerForm:IssueText']","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_EFFECTIVEDATE","//input[@type='text'][@name='outerForm:effectiveDate']",var_EffectiveDatenew,"FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_ISSUE","//input[@id='outerForm:Issue']","TGTYPESCREENREG");

        TAP.$("ELE_LINK_SETTLE","//span[text()='Settle']	","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_PAYMENTATSETTLE","//input[@id='outerForm:paymentAtSettle']",var_CashWithApplication,"TRUE","TGTYPESCREENREG");

		String var_PolicyExchangeCheck;
                 LOGGER.info("Executed Step = VAR,String,var_PolicyExchangeCheck,TGTYPESCREENREG");
		var_PolicyExchangeCheck = getJsonData(var_CSVData , "$.records["+var_Count+"].Clients/Endorsements/Exclusions/FundSelection?PolicyExchange");
                 LOGGER.info("Executed Step = STORE,var_PolicyExchangeCheck,var_CSVData,$.records[{var_Count}].Clients/Endorsements/Exclusions/FundSelection?PolicyExchange,TGTYPESCREENREG");
        LOGGER.info("Executed Step = START IF");
        if (check(var_PolicyExchangeCheck,"=",var_PolicyExchangeCheck,"TGTYPESCREENREG")) {
         LOGGER.info("Executed Step = IF,var_PolicyExchangeCheck,=,var_PolicyExchangeCheck,TGTYPESCREENREG");
        CALL.$("ValidatePolicyExchangeUAT3","TGTYPESCREENREG");

		String var_ModalPremium = "null";
                 LOGGER.info("Executed Step = VAR,String,var_ModalPremium,null,TGTYPESCREENREG");
		var_ModalPremium = ActionWrapper.getElementValue("ELE_GETVAL_MODALPREMIUMAMNTPOLICYEXCHANGE", "//span[@id='outerForm:modalPremium']");
                 LOGGER.info("Executed Step = STORE,var_ModalPremium,ELE_GETVAL_MODALPREMIUMAMNTPOLICYEXCHANGE,$.records[{var_Count}].EstimatedAmount,TGTYPESCREENREG");
		try { 
		 
		 
		 ;} 
		 catch(Exception e) { PrintVal("Renderer Script Exception : "+e.toString()); } 
		 PrintVal("Executed Step = CALLRENDERERSCRIPT,VALIDATEPOLICYEXCHANGEUTA2"); 
        }
          LOGGER.info("Executed Step = ENDIF,TGTYPESCREENREG");
        CALL.$("WritePolicyQuickInquiryToCSV","TGTYPESCREENREG");

        HOVER.$("ELE_MENU_INQUIRY","//td[text()='Inquiry']","TGTYPESCREENREG");

        TAP.$("ELE_SUBMEN_QUICKINQUIRY","//td[contains(text(),'Quick Inquiry')]","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_POLICYNUMBER","//input[@type='text'][@name='outerForm:policyNumber']",var_PolicyNumber,"FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_CONTINUE","//input[@type='submit'][@name='outerForm:Continue']","TGTYPESCREENREG");

		writeToCSV ("Policy Number " , ActionWrapper.getElementValue("ELE_GETVAL_POLICYNUMBER", "//span[@id='outerForm:policyNumber']"));
                 LOGGER.info("Executed Step = WRITETOCSV,ELE_GETVAL_POLICYNUMBER,Policy Number,TGTYPESCREENREG");
		writeToCSV ("Status " , ActionWrapper.getElementValue("ELE_GETVAL_STATUS", "//span[@id='outerForm:statusText']"));
                 LOGGER.info("Executed Step = WRITETOCSV,ELE_GETVAL_STATUS,Status,TGTYPESCREENREG");
		writeToCSV ("Effective Date " , ActionWrapper.getElementValue("ELE_GETVAL_EFFECTIVEDATETEXTFROMGRID", "//span[@id='outerForm:grid:0:effectiveDateOut2']"));
                 LOGGER.info("Executed Step = WRITETOCSV,ELE_GETVAL_EFFECTIVEDATETEXTFROMGRID,Effective Date,TGTYPESCREENREG");
		writeToCSV ("State country " , ActionWrapper.getElementValue("ELE_GETVAL_STATECOUNTRY", "//span[@id='outerForm:ownerCityStateZip']"));
                 LOGGER.info("Executed Step = WRITETOCSV,ELE_GETVAL_STATECOUNTRY,State country,TGTYPESCREENREG");
		writeToCSV ("Agent Number " , ActionWrapper.getElementValue("ELE_GETVAL_AGENTNUMBER", "//span[@id='outerForm:agentNumber']"));
                 LOGGER.info("Executed Step = WRITETOCSV,ELE_GETVAL_AGENTNUMBER,Agent Number,TGTYPESCREENREG");
		var_Count = var_Count + ( 1 );
                 LOGGER.info("Executed Step = INCREASEVAR,var_Count,1,TGTYPESCREENREG");
    	}
          LOGGER.info("Executed Step = WEND,TGTYPESCREENREG");
    	resetAttemps();

    }


    @Test
    public void tc01createnewbusinesspolicyuat2regression2() {

        // No need to declare / print method name here, handled by TestNg listener
        // declare("tc01createnewbusinesspolicyuat2regression2");

        CALL.$("LoginToGIASFandGRegression","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_USERNAME","outerForm:usernameTGWEBCOMMAIdTGWEBCOMMA0","x5990","FALSE","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_PASSWORD","outerForm:passwordTGWEBCOMMAIdTGWEBCOMMA0","cnx12345","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_LOGIN","outerForm:loginButtonTGWEBCOMMAIdTGWEBCOMMA0","TGTYPESCREENREG");

		int var_Count = 0;
                 LOGGER.info("Executed Step = VAR,Integer,var_Count,0,TGTYPESCREENREG");
        LOGGER.info("Executed Step = REPEAT IF");
    	while (check(var_Count,"<",4,"TGTYPESCREENREG")) {
          LOGGER.info("Executed Step = WHILE,var_Count,<,4,TGTYPESCREENREG");
		com.jayway.jsonpath.ReadContext var_CSVData = getJsonData("C:/GIAS_Automation/FandG Regression/InputData/FandGReg_CreateAPolicy.csv");
                 LOGGER.info("Executed Step = VAR,Json,var_CSVData,C:/GIAS_Automation/FandG Regression/InputData/FandGReg_CreateAPolicy.csv,TGTYPESCREENREG");
		String var_EffectiveDate = "null";
                 LOGGER.info("Executed Step = VAR,String,var_EffectiveDate,null,TGTYPESCREENREG");
		var_EffectiveDate = getJsonData(var_CSVData , "$.records["+var_Count+"].EffectiveDate");
                 LOGGER.info("Executed Step = STORE,var_EffectiveDate,var_CSVData,$.records[{var_Count}].EffectiveDate,TGTYPESCREENREG");
        CALL.$("AddNewBusinessPolicyUAT2Regression2","TGTYPESCREENREG");

        HOVER.$("ELE_MENU_NEWBUSINESS","//td[text()='New Business']","TGTYPESCREENREG");

        TAP.$("ELE_SUBMEN_APPLICATIONENTRYUPDATE","//td[text()='Application Entry/Update']","TGTYPESCREENREG");

        TAP.$("ELE_RNDBTN_ADD","//img[@id='outerForm:gnAddImage']","TGTYPESCREENREG");

		String var_PolicyNumber;
                 LOGGER.info("Executed Step = VAR,String,var_PolicyNumber,TGTYPESCREENREG");
		var_PolicyNumber = getJsonData(var_CSVData , "$.records["+var_Count+"].PolicyNumber");
                 LOGGER.info("Executed Step = STORE,var_PolicyNumber,var_CSVData,$.records[{var_Count}].PolicyNumber,TGTYPESCREENREG");
		String var_Name1;
                 LOGGER.info("Executed Step = VAR,String,var_Name1,TGTYPESCREENREG");
		var_Name1 = getJsonData(var_CSVData , "$.records["+var_Count+"].Name1");
                 LOGGER.info("Executed Step = STORE,var_Name1,var_CSVData,$.records[{var_Count}].Name1,TGTYPESCREENREG");
		String var_FirstName1;
                 LOGGER.info("Executed Step = VAR,String,var_FirstName1,TGTYPESCREENREG");
		var_FirstName1 = getJsonData(var_CSVData , "$.records["+var_Count+"].First1");
                 LOGGER.info("Executed Step = STORE,var_FirstName1,var_CSVData,$.records[{var_Count}].First1,TGTYPESCREENREG");
		String var_LastName1;
                 LOGGER.info("Executed Step = VAR,String,var_LastName1,TGTYPESCREENREG");
		var_LastName1 = getJsonData(var_CSVData , "$.records["+var_Count+"].Last1");
                 LOGGER.info("Executed Step = STORE,var_LastName1,var_CSVData,$.records[{var_Count}].Last1,TGTYPESCREENREG");
		String var_BirthStateCountry1;
                 LOGGER.info("Executed Step = VAR,String,var_BirthStateCountry1,TGTYPESCREENREG");
		var_BirthStateCountry1 = getJsonData(var_CSVData , "$.records["+var_Count+"].BirthStateCountry1");
                 LOGGER.info("Executed Step = STORE,var_BirthStateCountry1,var_CSVData,$.records[{var_Count}].BirthStateCountry1,TGTYPESCREENREG");
		String var_DateOfBirth1;
                 LOGGER.info("Executed Step = VAR,String,var_DateOfBirth1,TGTYPESCREENREG");
		var_DateOfBirth1 = getJsonData(var_CSVData , "$.records["+var_Count+"].DateOfBirth1");
                 LOGGER.info("Executed Step = STORE,var_DateOfBirth1,var_CSVData,$.records[{var_Count}].DateOfBirth1,TGTYPESCREENREG");
		String var_Gender1;
                 LOGGER.info("Executed Step = VAR,String,var_Gender1,TGTYPESCREENREG");
		var_Gender1 = getJsonData(var_CSVData , "$.records["+var_Count+"].Gender1");
                 LOGGER.info("Executed Step = STORE,var_Gender1,var_CSVData,$.records[{var_Count}].Gender1,TGTYPESCREENREG");
		String var_IDType1;
                 LOGGER.info("Executed Step = VAR,String,var_IDType1,TGTYPESCREENREG");
		var_IDType1 = getJsonData(var_CSVData , "$.records["+var_Count+"].IDType1");
                 LOGGER.info("Executed Step = STORE,var_IDType1,var_CSVData,$.records[{var_Count}].IDType1,TGTYPESCREENREG");
		String var_IDNumber1;
                 LOGGER.info("Executed Step = VAR,String,var_IDNumber1,TGTYPESCREENREG");
		var_IDNumber1 = getJsonData(var_CSVData , "$.records["+var_Count+"].IDNumber1");
                 LOGGER.info("Executed Step = STORE,var_IDNumber1,var_CSVData,$.records[{var_Count}].IDNumber1,TGTYPESCREENREG");
		String var_FirstName2;
                 LOGGER.info("Executed Step = VAR,String,var_FirstName2,TGTYPESCREENREG");
		var_FirstName2 = getJsonData(var_CSVData , "$.records["+var_Count+"].First2");
                 LOGGER.info("Executed Step = STORE,var_FirstName2,var_CSVData,$.records[{var_Count}].First2,TGTYPESCREENREG");
		String var_LastName2;
                 LOGGER.info("Executed Step = VAR,String,var_LastName2,TGTYPESCREENREG");
		var_LastName2 = getJsonData(var_CSVData , "$.records["+var_Count+"].Last2");
                 LOGGER.info("Executed Step = STORE,var_LastName2,var_CSVData,$.records[{var_Count}].Last2,TGTYPESCREENREG");
		String var_BirthStateCountry2;
                 LOGGER.info("Executed Step = VAR,String,var_BirthStateCountry2,TGTYPESCREENREG");
		var_BirthStateCountry2 = getJsonData(var_CSVData , "$.records["+var_Count+"].BirthStateCountry2");
                 LOGGER.info("Executed Step = STORE,var_BirthStateCountry2,var_CSVData,$.records[{var_Count}].BirthStateCountry2,TGTYPESCREENREG");
		String var_DateOfBirth2;
                 LOGGER.info("Executed Step = VAR,String,var_DateOfBirth2,TGTYPESCREENREG");
		var_DateOfBirth2 = getJsonData(var_CSVData , "$.records["+var_Count+"].DateOfBirth2");
                 LOGGER.info("Executed Step = STORE,var_DateOfBirth2,var_CSVData,$.records[{var_Count}].DateOfBirth2,TGTYPESCREENREG");
		String var_Gender2;
                 LOGGER.info("Executed Step = VAR,String,var_Gender2,TGTYPESCREENREG");
		var_Gender2 = getJsonData(var_CSVData , "$.records["+var_Count+"].Gender2");
                 LOGGER.info("Executed Step = STORE,var_Gender2,var_CSVData,$.records[{var_Count}].Gender2,TGTYPESCREENREG");
		String var_IDType2;
                 LOGGER.info("Executed Step = VAR,String,var_IDType2,TGTYPESCREENREG");
		var_IDType2 = getJsonData(var_CSVData , "$.records["+var_Count+"].IDType2");
                 LOGGER.info("Executed Step = STORE,var_IDType2,var_CSVData,$.records[{var_Count}].IDType2,TGTYPESCREENREG");
		String var_IDNumber2;
                 LOGGER.info("Executed Step = VAR,String,var_IDNumber2,TGTYPESCREENREG");
		var_IDNumber2 = getJsonData(var_CSVData , "$.records["+var_Count+"].IDNumber2");
                 LOGGER.info("Executed Step = STORE,var_IDNumber2,var_CSVData,$.records[{var_Count}].IDNumber2,TGTYPESCREENREG");
		String var_AddressLine11;
                 LOGGER.info("Executed Step = VAR,String,var_AddressLine11,TGTYPESCREENREG");
		var_AddressLine11 = getJsonData(var_CSVData , "$.records["+var_Count+"].AddressLine1_1");
                 LOGGER.info("Executed Step = STORE,var_AddressLine11,var_CSVData,$.records[{var_Count}].AddressLine1_1,TGTYPESCREENREG");
		String var_City1;
                 LOGGER.info("Executed Step = VAR,String,var_City1,TGTYPESCREENREG");
		var_City1 = getJsonData(var_CSVData , "$.records["+var_Count+"].City1");
                 LOGGER.info("Executed Step = STORE,var_City1,var_CSVData,$.records[{var_Count}].City1,TGTYPESCREENREG");
		String var_StateandCountry1;
                 LOGGER.info("Executed Step = VAR,String,var_StateandCountry1,TGTYPESCREENREG");
		var_StateandCountry1 = getJsonData(var_CSVData , "$.records["+var_Count+"].StateandCountry1");
                 LOGGER.info("Executed Step = STORE,var_StateandCountry1,var_CSVData,$.records[{var_Count}].StateandCountry1,TGTYPESCREENREG");
		String var_ZipCode1;
                 LOGGER.info("Executed Step = VAR,String,var_ZipCode1,TGTYPESCREENREG");
		var_ZipCode1 = getJsonData(var_CSVData , "$.records["+var_Count+"].ZipCode1");
                 LOGGER.info("Executed Step = STORE,var_ZipCode1,var_CSVData,$.records[{var_Count}].ZipCode1,TGTYPESCREENREG");
		String var_AddressLine12;
                 LOGGER.info("Executed Step = VAR,String,var_AddressLine12,TGTYPESCREENREG");
		var_AddressLine12 = getJsonData(var_CSVData , "$.records["+var_Count+"].AddressLine1_2");
                 LOGGER.info("Executed Step = STORE,var_AddressLine12,var_CSVData,$.records[{var_Count}].AddressLine1_2,TGTYPESCREENREG");
		String var_City2;
                 LOGGER.info("Executed Step = VAR,String,var_City2,TGTYPESCREENREG");
		var_City2 = getJsonData(var_CSVData , "$.records["+var_Count+"].City2");
                 LOGGER.info("Executed Step = STORE,var_City2,var_CSVData,$.records[{var_Count}].City2,TGTYPESCREENREG");
		String var_StateandCountry2;
                 LOGGER.info("Executed Step = VAR,String,var_StateandCountry2,TGTYPESCREENREG");
		var_StateandCountry2 = getJsonData(var_CSVData , "$.records["+var_Count+"].StateandCountry2");
                 LOGGER.info("Executed Step = STORE,var_StateandCountry2,var_CSVData,$.records[{var_Count}].StateandCountry2,TGTYPESCREENREG");
		String var_ZipCode2;
                 LOGGER.info("Executed Step = VAR,String,var_ZipCode2,TGTYPESCREENREG");
		var_ZipCode2 = getJsonData(var_CSVData , "$.records["+var_Count+"].ZipCode2");
                 LOGGER.info("Executed Step = STORE,var_ZipCode2,var_CSVData,$.records[{var_Count}].ZipCode2,TGTYPESCREENREG");
		String var_SelectTypeTwo;
                 LOGGER.info("Executed Step = VAR,String,var_SelectTypeTwo,TGTYPESCREENREG");
		var_SelectTypeTwo = getJsonData(var_CSVData , "$.records["+var_Count+"].SelectTypeTwo");
                 LOGGER.info("Executed Step = STORE,var_SelectTypeTwo,var_CSVData,$.records[{var_Count}].SelectTypeTwo,TGTYPESCREENREG");
		String var_Name2;
                 LOGGER.info("Executed Step = VAR,String,var_Name2,TGTYPESCREENREG");
		var_Name2 = getJsonData(var_CSVData , "$.records["+var_Count+"].Name2");
                 LOGGER.info("Executed Step = STORE,var_Name2,var_CSVData,$.records[{var_Count}].Name2,TGTYPESCREENREG");
		String var_SelectTypeThree;
                 LOGGER.info("Executed Step = VAR,String,var_SelectTypeThree,TGTYPESCREENREG");
		var_SelectTypeThree = getJsonData(var_CSVData , "$.records["+var_Count+"].SelectTypeThree");
                 LOGGER.info("Executed Step = STORE,var_SelectTypeThree,var_CSVData,$.records[{var_Count}].SelectTypeThree,TGTYPESCREENREG");
		String var_ApplicationDate;
                 LOGGER.info("Executed Step = VAR,String,var_ApplicationDate,TGTYPESCREENREG");
		var_ApplicationDate = getJsonData(var_CSVData , "$.records["+var_Count+"].ApplicationDate");
                 LOGGER.info("Executed Step = STORE,var_ApplicationDate,var_CSVData,$.records[{var_Count}].ApplicationDate,TGTYPESCREENREG");
		String var_IssueStateCountry;
                 LOGGER.info("Executed Step = VAR,String,var_IssueStateCountry,TGTYPESCREENREG");
		var_IssueStateCountry = getJsonData(var_CSVData , "$.records["+var_Count+"].IssueStateCountry");
                 LOGGER.info("Executed Step = STORE,var_IssueStateCountry,var_CSVData,$.records[{var_Count}].IssueStateCountry,TGTYPESCREENREG");
		String var_EffectiveDatenew;
                 LOGGER.info("Executed Step = VAR,String,var_EffectiveDatenew,TGTYPESCREENREG");
		var_EffectiveDatenew = getJsonData(var_CSVData , "$.records["+var_Count+"].EffectiveDate");
                 LOGGER.info("Executed Step = STORE,var_EffectiveDatenew,var_CSVData,$.records[{var_Count}].EffectiveDate,TGTYPESCREENREG");
		String var_CashWithApplication;
                 LOGGER.info("Executed Step = VAR,String,var_CashWithApplication,TGTYPESCREENREG");
		var_CashWithApplication = getJsonData(var_CSVData , "$.records["+var_Count+"].CashWithApplication");
                 LOGGER.info("Executed Step = STORE,var_CashWithApplication,var_CSVData,$.records[{var_Count}].CashWithApplication,TGTYPESCREENREG");
		String var_TaxQualifiedDescription;
                 LOGGER.info("Executed Step = VAR,String,var_TaxQualifiedDescription,TGTYPESCREENREG");
		var_TaxQualifiedDescription = getJsonData(var_CSVData , "$.records["+var_Count+"].TaxQualifiedDescription");
                 LOGGER.info("Executed Step = STORE,var_TaxQualifiedDescription,var_CSVData,$.records[{var_Count}].TaxQualifiedDescription,TGTYPESCREENREG");
		String var_AgentNumberOne;
                 LOGGER.info("Executed Step = VAR,String,var_AgentNumberOne,TGTYPESCREENREG");
		var_AgentNumberOne = getJsonData(var_CSVData , "$.records["+var_Count+"].AgentNumberOne");
                 LOGGER.info("Executed Step = STORE,var_AgentNumberOne,var_CSVData,$.records[{var_Count}].AgentNumberOne,TGTYPESCREENREG");
		String var_SituationCode1;
                 LOGGER.info("Executed Step = VAR,String,var_SituationCode1,TGTYPESCREENREG");
		var_SituationCode1 = getJsonData(var_CSVData , "$.records["+var_Count+"].SituationCode1");
                 LOGGER.info("Executed Step = STORE,var_SituationCode1,var_CSVData,$.records[{var_Count}].SituationCode1,TGTYPESCREENREG");
		String var_SplitPercentage1;
                 LOGGER.info("Executed Step = VAR,String,var_SplitPercentage1,TGTYPESCREENREG");
		var_SplitPercentage1 = getJsonData(var_CSVData , "$.records["+var_Count+"].SplitPercentage1");
                 LOGGER.info("Executed Step = STORE,var_SplitPercentage1,var_CSVData,$.records[{var_Count}].SplitPercentage1,TGTYPESCREENREG");
		String var_AgentNumberTwo;
                 LOGGER.info("Executed Step = VAR,String,var_AgentNumberTwo,TGTYPESCREENREG");
		var_AgentNumberTwo = getJsonData(var_CSVData , "$.records["+var_Count+"].AgentNumberTwo");
                 LOGGER.info("Executed Step = STORE,var_AgentNumberTwo,var_CSVData,$.records[{var_Count}].AgentNumberTwo,TGTYPESCREENREG");
		String var_SituationCode2;
                 LOGGER.info("Executed Step = VAR,String,var_SituationCode2,TGTYPESCREENREG");
		var_SituationCode2 = getJsonData(var_CSVData , "$.records["+var_Count+"].SituationCode2");
                 LOGGER.info("Executed Step = STORE,var_SituationCode2,var_CSVData,$.records[{var_Count}].SituationCode2,TGTYPESCREENREG");
		String var_SplitPercentage2;
                 LOGGER.info("Executed Step = VAR,String,var_SplitPercentage2,TGTYPESCREENREG");
		var_SplitPercentage2 = getJsonData(var_CSVData , "$.records["+var_Count+"].SplitPercentage2");
                 LOGGER.info("Executed Step = STORE,var_SplitPercentage2,var_CSVData,$.records[{var_Count}].SplitPercentage2,TGTYPESCREENREG");
		try { 
		 var_AgentNumberOne = "000112680";
				var_AgentNumberTwo = "000112680";
				var_SituationCode1 = "01";
				var_SituationCode2 = "01";
		 
		 ;} 
		 catch(Exception e) { PrintVal("Renderer Script Exception : "+e.toString()); } 
		 PrintVal("Executed Step = CALLRENDERERSCRIPT,ASSIGNSTATICVALUEFORAGENTNUMBERANDSITUATIONCODE"); 
        TYPE.$("ELE_TXTBOX_POLICYNUMBER","//input[@type='text'][@name='outerForm:policyNumber']",var_PolicyNumber,"FALSE","TGTYPESCREENREG");

        SELECT.$("ELE_DRPDWN_CURRENCYREGRESSION","//select[@id='outerForm:currencyCode']","DOLLAR [US]","TGTYPESCREENREG");

        SELECT.$("ELE_DRPDWN_STATECOUNTRY","//select[@name='outerForm:countryAndStateCode']	",var_StateandCountry1,"TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_CASHWITHAPPLICATION","//input[@type='text'][@name='outerForm:cashWithApplication']",var_CashWithApplication,"FALSE","TGTYPESCREENREG");

        SELECT.$("ELE_DRPDWN_PAYMENTFREQUENCY","//select[@name='outerForm:paymentMode']","Annual","TGTYPESCREENREG");

        SELECT.$("ELE_DRPDWN_PAYMENTMETHOD","//select[@name='outerForm:paymentCode']","No Notice","TGTYPESCREENREG");

        SELECT.$("ELE_DRPDWN_TAXQUALIFIEDDESCRIPTION","//select[@name='outerForm:taxQualifiedCode']",var_TaxQualifiedDescription,"TGTYPESCREENREG");

        SELECT.$("ELE_DRPDWN_TAXWITHHOLDING","//select[@name='outerForm:taxWithholdingCode']","No Withholding","TGTYPESCREENREG");

        TAP.$("ELE_LINK_SELECTCLIENT","//span[text()='Select Client']","TGTYPESCREENREG");

        TAP.$("ELE_RNDBTN_ADD","//img[@id='outerForm:gnAddImage']","TGTYPESCREENREG");

        LOGGER.info("Executed Step = START IF");
        if (check(var_FirstName2,"=","null","TGTYPESCREENREG")) {
         LOGGER.info("Executed Step = IF,var_FirstName2,=,null,TGTYPESCREENREG");
        TYPE.$("ELE_TXTBOX_FIRSTNAME","//input[@type='text'][@name='outerForm:nameFirst']",var_FirstName1,"FALSE","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_LASTNAME","//input[@type='text'][@name='outerForm:nameLast']",var_LastName1,"FALSE","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_CLIENTDOB","//input[@type='text'][@name='outerForm:dateOfBirth2']",var_DateOfBirth1,"FALSE","TGTYPESCREENREG");

        SELECT.$("ELE_DRPDWN_CLIENTGENDER","//select[@name='outerForm:sexCode2']",var_Gender1,"TGTYPESCREENREG");

        SELECT.$("ELE_DRPDWN_IDTYPE","//select[@name='outerForm:taxIdenUsag2']",var_IDType1,"TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_IDNUMBER","//input[@type='text'][@name='outerForm:identificationNumber2']",var_IDNumber1,"FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SUBMIT","//input[@type='submit'][@name='outerForm:Submit']","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_ADDRESSLINE1","//input[@type='text'][@name='outerForm:addressLineOne']",var_AddressLine11,"FALSE","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_CITY","//input[@type='text'][@name='outerForm:addressCity']",var_City1,"FALSE","TGTYPESCREENREG");

        SELECT.$("ELE_DRPDWN_STATECOUNTRY","//select[@name='outerForm:countryAndStateCode']	",var_StateandCountry1,"TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_ZIPCODE","//input[@type='text'][@name='outerForm:zipCode']",var_ZipCode1,"FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SUBMIT","//input[@type='submit'][@name='outerForm:Submit']","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_CANCEL","//input[@type='submit'][@name='outerForm:Cancel']","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_SEARCHCLIENTNAME","//input[@type='text'][@name='outerForm:grid:individualName']",var_Name1,"FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_FINDPOSTO","//img[@id='outerForm:gnFindImage']","TGTYPESCREENREG");

        SELECT.$("ELE_DRPDWN_RELATIONSHIPGRID","//select[@name='outerForm:grid:0:relationship']","Owner one","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SUBMIT","//input[@type='submit'][@name='outerForm:Submit']","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_AGENTNUMBER","//input[@type='text'][@name='outerForm:grid:0:agentNumberOut2']",var_AgentNumberOne,"FALSE","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_SITUATIONCODE","//input[@type='text'][@name='outerForm:grid:0:agentSituationCodeOut2']",var_SituationCode1,"FALSE","TGTYPESCREENREG");

        }
         else {
          LOGGER.info("Executed Step = ELSE,TGTYPESCREENREG");
        TYPE.$("ELE_TXTBOX_FIRSTNAME","//input[@type='text'][@name='outerForm:nameFirst']",var_FirstName1,"FALSE","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_LASTNAME","//input[@type='text'][@name='outerForm:nameLast']",var_LastName1,"FALSE","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_CLIENTDOB","//input[@type='text'][@name='outerForm:dateOfBirth2']",var_DateOfBirth1,"FALSE","TGTYPESCREENREG");

        SELECT.$("ELE_DRPDWN_CLIENTGENDER","//select[@name='outerForm:sexCode2']",var_Gender1,"TGTYPESCREENREG");

        SELECT.$("ELE_DRPDWN_IDTYPE","//select[@name='outerForm:taxIdenUsag2']",var_IDType1,"TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_IDNUMBER","//input[@type='text'][@name='outerForm:identificationNumber2']",var_IDNumber1,"FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SUBMIT","//input[@type='submit'][@name='outerForm:Submit']","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_ADDRESSLINE1","//input[@type='text'][@name='outerForm:addressLineOne']",var_AddressLine11,"FALSE","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_CITY","//input[@type='text'][@name='outerForm:addressCity']",var_City1,"FALSE","TGTYPESCREENREG");

        SELECT.$("ELE_DRPDWN_STATECOUNTRY","//select[@name='outerForm:countryAndStateCode']	",var_StateandCountry1,"TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_ZIPCODE","//input[@type='text'][@name='outerForm:zipCode']",var_ZipCode1,"FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SUBMIT","//input[@type='submit'][@name='outerForm:Submit']","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_FIRSTNAME","//input[@type='text'][@name='outerForm:nameFirst']",var_FirstName2,"FALSE","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_LASTNAME","//input[@type='text'][@name='outerForm:nameLast']",var_LastName2,"FALSE","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_CLIENTDOB","//input[@type='text'][@name='outerForm:dateOfBirth2']",var_DateOfBirth2,"FALSE","TGTYPESCREENREG");

        SELECT.$("ELE_DRPDWN_CLIENTGENDER","//select[@name='outerForm:sexCode2']",var_Gender2,"TGTYPESCREENREG");

        SELECT.$("ELE_DRPDWN_IDTYPE","//select[@name='outerForm:taxIdenUsag2']",var_IDType2,"TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_IDNUMBER","//input[@type='text'][@name='outerForm:identificationNumber2']",var_IDNumber2,"FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SUBMIT","//input[@type='submit'][@name='outerForm:Submit']","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_ADDRESSLINE1","//input[@type='text'][@name='outerForm:addressLineOne']",var_AddressLine12,"FALSE","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_CITY","//input[@type='text'][@name='outerForm:addressCity']",var_City2,"FALSE","TGTYPESCREENREG");

        SELECT.$("ELE_DRPDWN_STATECOUNTRY","//select[@name='outerForm:countryAndStateCode']	",var_StateandCountry2,"TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_ZIPCODE","//input[@type='text'][@name='outerForm:zipCode']",var_ZipCode2,"FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SUBMIT","//input[@type='submit'][@name='outerForm:Submit']","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_CANCEL","//input[@type='submit'][@name='outerForm:Cancel']","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_SEARCHCLIENTNAME","//input[@type='text'][@name='outerForm:grid:individualName']",var_Name1,"FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_FINDPOSTO","//img[@id='outerForm:gnFindImage']","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_SEARCHCLIENTNAME","//input[@type='text'][@name='outerForm:grid:individualName']",var_Name2,"FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_FINDPOSTO","//img[@id='outerForm:gnFindImage']","TGTYPESCREENREG");

        SELECT.$("ELE_DRPDWN_RELATIONSHIPGRID","//select[@name='outerForm:grid:0:relationship']",var_SelectTypeTwo,"TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SUBMIT","//input[@type='submit'][@name='outerForm:Submit']","TGTYPESCREENREG");

        LOGGER.info("Executed Step = START IF");
        if (check(var_AgentNumberTwo,"=","null","TGTYPESCREENREG")) {
         LOGGER.info("Executed Step = IF,var_AgentNumberTwo,=,null,TGTYPESCREENREG");
        TYPE.$("ELE_TXTBOX_AGENTNUMBER","//input[@type='text'][@name='outerForm:grid:0:agentNumberOut2']",var_AgentNumberOne,"FALSE","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_SITUATIONCODE","//input[@type='text'][@name='outerForm:grid:0:agentSituationCodeOut2']",var_SituationCode1,"FALSE","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_SPLITPERCENTAGE","//input[@type='text'][@name='outerForm:grid:0:agentSplitPercentageOut2']",var_SplitPercentage1,"FALSE","TGTYPESCREENREG");

        }
         else {
          LOGGER.info("Executed Step = ELSE,TGTYPESCREENREG");
        TYPE.$("ELE_TXTBOX_AGENTNUMBER","//input[@type='text'][@name='outerForm:grid:0:agentNumberOut2']",var_AgentNumberOne,"FALSE","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_SITUATIONCODE","//input[@type='text'][@name='outerForm:grid:0:agentSituationCodeOut2']",var_SituationCode1,"FALSE","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_SPLITPERCENTAGE","//input[@type='text'][@name='outerForm:grid:0:agentSplitPercentageOut2']",var_SplitPercentage1,"FALSE","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_AGENTNUMBER2","//input[@type='text'][@name='outerForm:grid:1:agentNumberOut2']",var_AgentNumberTwo,"FALSE","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_SITUATIONCODE2","//input[@type='text'][@name='outerForm:grid:1:agentSituationCodeOut2']",var_SituationCode2,"FALSE","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_SPLITPERCENTAGE2","//input[@type='text'][@name='outerForm:grid:1:agentSplitPercentageOut2']",var_SplitPercentage2,"FALSE","TGTYPESCREENREG");

        }
          LOGGER.info("Executed Step = ENDIF,TGTYPESCREENREG");
        }
          LOGGER.info("Executed Step = ENDIF,TGTYPESCREENREG");
        TAP.$("ELE_BUTTON_SUBMIT","//input[@type='submit'][@name='outerForm:Submit']","TGTYPESCREENREG");

        ASSERT.$("ELE_GETMSG_SUCCESS","//li[@class='MessageInfo']","CONTAINS","successfully","TGTYPESCREENREG");

        CALL.$("SettlePolicyUAT2Regression","TGTYPESCREENREG");

        TAP.$("ELE_LINK_BENEFITS","//span[text()='Benefits']","TGTYPESCREENREG");

		String var_Plan;
                 LOGGER.info("Executed Step = VAR,String,var_Plan,TGTYPESCREENREG");
		var_Plan = getJsonData(var_CSVData , "$.records["+var_Count+"].Plan");
                 LOGGER.info("Executed Step = STORE,var_Plan,var_CSVData,$.records[{var_Count}].Plan,TGTYPESCREENREG");
        SELECT.$("ELE_DRPDWN_ADDABENEFIT","//select[@name='outerForm:initialPlanCode']",var_Plan,"TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_CONTINUE","//input[@type='submit'][@name='outerForm:Continue']","TGTYPESCREENREG");

        TAP.$("ELE_LINK_FUNDSELECTION","//span[contains(text(),'Fund Selection')]","TGTYPESCREENREG");

		try { 
		 List<WebElement> baseRider= driver.findElements(By.cssSelector("*[class='ListNumeric']"));
		              int baseRiderCount=  baseRider.size();
		              String nullString = "null";
		              if( baseRiderCount> 0) {

		Double totalAmount = 0.0;
		            for(int i=0; i< 12;i++){
		                int incrementali = i+1;
		                String PremiumAllocation = "PremiumAllocation"+incrementali;
		                String premiumAllocPercOut2 = getJsonData(var_CSVData , "$.records["+var_Count+"]."+PremiumAllocation.trim()+"");
		                if ((premiumAllocPercOut2 != null) && !(premiumAllocPercOut2.trim().equalsIgnoreCase(nullString))) {
		                    totalAmount = totalAmount + Double.parseDouble(premiumAllocPercOut2.trim());
		                }
		            }
		            totalAmount = Math.round(totalAmount * 10.0) / 10.0;
		            Double maximumPercentage = 1.0;
		            if (Double.compare(totalAmount, maximumPercentage) != 0){
		                Assert.assertTrue(false,"Total fund allocation must be 1.0");
		            }

		                  for(int j=0; j< baseRiderCount;j++){
		                      WebElement eleTextBoxPercentage = driver.findElement(By.xpath("//input[@type='text'][@name='outerForm:grid:" + j + ":premiumAllocPercOut2']"));
		                      eleTextBoxPercentage.clear();
		                      eleTextBoxPercentage.sendKeys("0.0");
		                  }

		                  for(int i=0; i< 12;i++)
		                  {
		                      int incrementali = i+1;
		                      String Funds = "Fund"+ incrementali;
		                      String PremiumAllocation = "PremiumAllocation"+ incrementali;

		                      String fundName = getJsonData(var_CSVData , "$.records["+var_Count+"]."+Funds+"");
		                      LOGGER.info("fundname111"+fundName);
		                      String premiumAllocPercOut2 = getJsonData(var_CSVData , "$.records["+var_Count+"]."+PremiumAllocation+"");
		                      LOGGER.info("fundname222"+premiumAllocPercOut2);

		                      if ((fundName != null) && !(fundName.trim().equalsIgnoreCase(nullString))) {
		                          LOGGER.info("fundname333");
		                          for(int j=0; j< baseRiderCount;j++)
		                          {
		                              WebElement eleFundName = driver.findElement(By.cssSelector("span[id='outerForm:grid:" + j + ":fundCodeTextOut2']"));
		                              String fullFundName = eleFundName.getText();
		                              LOGGER.info("fundname444"+fullFundName);
		                              if (fullFundName.toLowerCase().trim().contains(fundName.toLowerCase().trim())){
		                                  WebElement eleTextBoxPercentage = driver.findElement(By.xpath("//input[@type='text'][@name='outerForm:grid:" + j + ":premiumAllocPercOut2']"));
		                                  eleTextBoxPercentage.clear();
		                                  if ((premiumAllocPercOut2 != null) && !(premiumAllocPercOut2.trim().equalsIgnoreCase(nullString))){
		                                      eleTextBoxPercentage.sendKeys(premiumAllocPercOut2);
		                                  } else {
		                                      eleTextBoxPercentage.sendKeys("0.0");
		                                  }

		                                  takeScreenshot();
		                                  break;
		                              }

		                          }
		                      }

		                      System.out.println("next value of i is  " + i);

		                  }
		              }
		 
		 ;} 
		 catch(Exception e) { PrintVal("Renderer Script Exception : "+e.toString()); } 
		 PrintVal("Executed Step = CALLRENDERERSCRIPT,FUNDSELECTIONUAT3"); 
		takeFullScreenShot();
                 LOGGER.info("Executed Step = TAKEFULLSCREENSHOT,TGTYPESCREENREG");
        TAP.$("ELE_BUTTON_SUBMIT","//input[@type='submit'][@name='outerForm:Submit']","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_UNITSAFTERADDINGBENEFIT","//input[@type='text'][@name='outerForm:unitsOfInsurance']	","0.000","FALSE","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_REQUESTPREMIUNAMNT","//input[@id='outerForm:annualPremium']",var_CashWithApplication,"TRUE","TGTYPESCREENREG");

        LOGGER.info("Executed Step = START IF");
        if (check("ELE_DRPDWN_GUARWITHDRAWALMETHODREGRESSION","//select[@id='outerForm:gridSupp:0:guarWithdrawalMethodOut3']","VISIBLE","TGTYPESCREENREG")) {
         LOGGER.info("Executed Step = IF,ELE_DRPDWN_GUARWITHDRAWALMETHODREGRESSION,VISIBLE,TGTYPESCREENREG");
        SELECT.$("ELE_DRPDWN_GUARWITHDRAWALMETHODREGRESSION","//select[@id='outerForm:gridSupp:0:guarWithdrawalMethodOut3']","Single","TGTYPESCREENREG");

        }
          LOGGER.info("Executed Step = ENDIF,TGTYPESCREENREG");
        LOGGER.info("Executed Step = START IF");
        if (check("ELE_DRPDWN_GUARWITHDRAWLMETHODREG2","//select[@id='outerForm:gridSupp:1:guarWithdrawalMethodOut3']","VISIBLE","TGTYPESCREENREG")) {
         LOGGER.info("Executed Step = IF,ELE_DRPDWN_GUARWITHDRAWLMETHODREG2,VISIBLE,TGTYPESCREENREG");
        SELECT.$("ELE_DRPDWN_GUARWITHDRAWLMETHODREG2","//select[@id='outerForm:gridSupp:1:guarWithdrawalMethodOut3']","Single","TGTYPESCREENREG");

        }
          LOGGER.info("Executed Step = ENDIF,TGTYPESCREENREG");
        TAP.$("ELE_BUTTON_SUBMIT","//input[@type='submit'][@name='outerForm:Submit']","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_CANCEL","//input[@type='submit'][@name='outerForm:Cancel']","TGTYPESCREENREG");

        TAP.$("ELE_LINK_ISSUE","//span[@id='outerForm:IssueText']","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_EFFECTIVEDATE","//input[@type='text'][@name='outerForm:effectiveDate']",var_EffectiveDatenew,"FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_ISSUE","//input[@id='outerForm:Issue']","TGTYPESCREENREG");

        TAP.$("ELE_LINK_SETTLE","//span[text()='Settle']	","TGTYPESCREENREG");

		String var_PolicyExchangeCheck;
                 LOGGER.info("Executed Step = VAR,String,var_PolicyExchangeCheck,TGTYPESCREENREG");
		var_PolicyExchangeCheck = getJsonData(var_CSVData , "$.records["+var_Count+"].Clients/Endorsements/Exclusions/FundSelection?PolicyExchange");
                 LOGGER.info("Executed Step = STORE,var_PolicyExchangeCheck,var_CSVData,$.records[{var_Count}].Clients/Endorsements/Exclusions/FundSelection?PolicyExchange,TGTYPESCREENREG");
        LOGGER.info("Executed Step = START IF");
        if (check(var_PolicyExchangeCheck,"=",var_PolicyExchangeCheck,"TGTYPESCREENREG")) {
         LOGGER.info("Executed Step = IF,var_PolicyExchangeCheck,=,var_PolicyExchangeCheck,TGTYPESCREENREG");
        CALL.$("ValidatePolicyExchangeUAT2","TGTYPESCREENREG");

		String var_ModalPremium = "null";
                 LOGGER.info("Executed Step = VAR,String,var_ModalPremium,null,TGTYPESCREENREG");
		var_ModalPremium = ActionWrapper.getElementValue("ELE_GETVAL_MODALPREMIUMAMNTPOLICYEXCHANGE", "//span[@id='outerForm:modalPremium']");
                 LOGGER.info("Executed Step = STORE,var_ModalPremium,ELE_GETVAL_MODALPREMIUMAMNTPOLICYEXCHANGE,$.records[{var_Count}].EstimatedAmount,TGTYPESCREENREG");
		String var_ExchangeType = "null";
                 LOGGER.info("Executed Step = VAR,String,var_ExchangeType,null,TGTYPESCREENREG");
		var_ExchangeType = getJsonData(var_CSVData , "$.records["+var_Count+"].ExchangeType");
                 LOGGER.info("Executed Step = STORE,var_ExchangeType,var_CSVData,$.records[{var_Count}].ExchangeType,TGTYPESCREENREG");
		String var_ReplacementType = "null";
                 LOGGER.info("Executed Step = VAR,String,var_ReplacementType,null,TGTYPESCREENREG");
		var_ReplacementType = getJsonData(var_CSVData , "$.records["+var_Count+"].ReplacementType");
                 LOGGER.info("Executed Step = STORE,var_ReplacementType,var_CSVData,$.records[{var_Count}].ReplacementType,TGTYPESCREENREG");
		String var_PriorCompanyName = "null";
                 LOGGER.info("Executed Step = VAR,String,var_PriorCompanyName,null,TGTYPESCREENREG");
		var_PriorCompanyName = getJsonData(var_CSVData , "$.records["+var_Count+"].PriorCompanName");
                 LOGGER.info("Executed Step = STORE,var_PriorCompanyName,var_CSVData,$.records[{var_Count}].PriorCompanName,TGTYPESCREENREG");
		String var_PriorInvestmentType = "null";
                 LOGGER.info("Executed Step = VAR,String,var_PriorInvestmentType,null,TGTYPESCREENREG");
		var_PriorInvestmentType = getJsonData(var_CSVData , "$.records["+var_Count+"].PriorInvestmentType");
                 LOGGER.info("Executed Step = STORE,var_PriorInvestmentType,var_CSVData,$.records[{var_Count}].PriorInvestmentType,TGTYPESCREENREG");
		String var_PolicyStatus = "null";
                 LOGGER.info("Executed Step = VAR,String,var_PolicyStatus,null,TGTYPESCREENREG");
		var_PolicyStatus = getJsonData(var_CSVData , "$.records["+var_Count+"].Status");
                 LOGGER.info("Executed Step = STORE,var_PolicyStatus,var_CSVData,$.records[{var_Count}].Status,TGTYPESCREENREG");
		String var_RollOverType = "null";
                 LOGGER.info("Executed Step = VAR,String,var_RollOverType,null,TGTYPESCREENREG");
		var_RollOverType = getJsonData(var_CSVData , "$.records["+var_Count+"].RollOverType");
                 LOGGER.info("Executed Step = STORE,var_RollOverType,var_CSVData,$.records[{var_Count}].RollOverType,TGTYPESCREENREG");
        SELECT.$("ELE_DRPDWN_ROLLOVERTYPE","//select[@id='outerForm:rolloverType']",var_RollOverType,"TGTYPESCREENREG");

		String var_RolloverPremium = "null";
                 LOGGER.info("Executed Step = VAR,String,var_RolloverPremium,null,TGTYPESCREENREG");
		var_RolloverPremium = getJsonData(var_CSVData , "$.records["+var_Count+"].RolloverPremium");
                 LOGGER.info("Executed Step = STORE,var_RolloverPremium,var_CSVData,$.records[{var_Count}].RolloverPremium,TGTYPESCREENREG");
		try { 
		 org.openqa.selenium.support.ui.Select select = new org.openqa.selenium.support.ui.Select(driver.findElement(By.xpath("//select[@id='outerForm:rolloverType']")));
		                    // Add Screenshot will only work for Android platform

		                    takeScreenshot();

		                    WebElement option = select.getFirstSelectedOption();
		                    String defaultItem = option.getText();
		                    System.out.println(defaultItem);

		                    if (!defaultItem.contains("No Rollover")) {
		                        WebElement eleRolloverPremium = driver.findElement(By.xpath("//input[@id='outerForm:rolloverPremium']"));
		                        eleRolloverPremium.clear();
		                        eleRolloverPremium.sendKeys(var_RolloverPremium);

		                        Thread.sleep(500);

		                        WebElement eleModalPremium = driver.findElement(By.cssSelector("span[id='outerForm:modalPremium']"));
		                        String strModalPremium = eleModalPremium.getText();
		                        strModalPremium = strModalPremium.replace(",","");

		                        var_RolloverPremium = var_RolloverPremium.replace(",","");

		                        Double doubleModalPremium = Double.parseDouble(strModalPremium);
		                        Double doubleRollverPremium = Double.parseDouble(var_RolloverPremium);

		                        Double remainingValue = doubleModalPremium - doubleRollverPremium;

		                        WebElement elePaymentAtSettle = driver.findElement(By.xpath("//input[@id='outerForm:paymentAtSettle']"));
		                        elePaymentAtSettle.clear();
		                        elePaymentAtSettle.sendKeys(remainingValue.toString());

		                        takeScreenshot();

		                    } else {
		                        WebElement elePaymentAtSettle = driver.findElement(By.xpath("//input[@id='outerForm:paymentAtSettle']"));
		                        elePaymentAtSettle.clear();
		                        elePaymentAtSettle.sendKeys(var_CashWithApplication);
		                    }

		                    if (defaultItem.contains("Direct Transfer") || defaultItem.contains("External Rollover")) {
		                        driver.findElement(By.xpath("//span[@id='outerForm:PolicyExchangeText']")).click();
		                        // Add Screenshot will only work for Android platform

		                        takeScreenshot();

		                        driver.findElement(By.xpath("//img[@id='outerForm:gnAddImage']")).click();
		                        // Add Screenshot will only work for Android platform

		                        takeScreenshot();

		                        org.openqa.selenium.support.ui.Select select1 = new org.openqa.selenium.support.ui.Select(driver.findElement(By.xpath("//select[@id='outerForm:status']")));
		                        select1.selectByVisibleText(var_PolicyStatus);
		                        // Add Screenshot will only work for Android platform

		                        takeScreenshot();

		                        Thread.sleep(500);
		                        org.openqa.selenium.support.ui.Select select2 = new org.openqa.selenium.support.ui.Select(driver.findElement(By.xpath("//select[@id='outerForm:exchangeCode']")));
		                        select2.selectByVisibleText(var_ExchangeType);
		                        // Add Screenshot will only work for Android platform

		                        takeScreenshot();

		                        Thread.sleep(500);
		                        org.openqa.selenium.support.ui.Select select3 = new org.openqa.selenium.support.ui.Select(driver.findElement(By.xpath("//select[@id='outerForm:replacementType']")));
		                        select3.selectByVisibleText(var_ReplacementType);
		                        // Add Screenshot will only work for Android platform

		                        takeScreenshot();

		                        Thread.sleep(500);
		                        driver.findElement(By.xpath("//input[@id='outerForm:estimateExchangeAmt']")).clear();
		                        driver.findElement(By.xpath("//input[@id='outerForm:estimateExchangeAmt']")).sendKeys(var_ModalPremium);
		                        // Add Screenshot will only work for Android platform

		                        takeScreenshot();

		                        Thread.sleep(500);

		                        driver.findElement(By.xpath("//input[@id='outerForm:cashReceivedFromExch']")).clear();
		                        driver.findElement(By.xpath("//input[@id='outerForm:cashReceivedFromExch']")).sendKeys(var_ModalPremium);
		                        driver.findElement(By.xpath("//input[@id='outerForm:cashReceivedDate_input']")).sendKeys(var_EffectiveDate);
		                        Thread.sleep(500);
		                        driver.findElement(By.xpath("//input[@id='outerForm:priorCompanyName']")).sendKeys(var_PriorCompanyName);
		                        Thread.sleep(500);
		                        org.openqa.selenium.support.ui.Select select4 = new org.openqa.selenium.support.ui.Select(driver.findElement(By.xpath("//select[@id='outerForm:priorInvestmentType']")));
		                        select4.selectByVisibleText(var_PriorInvestmentType);
		                        // Add Screenshot will only work for Android platform

		                        takeScreenshot();

		                        Thread.sleep(500);
		                        org.openqa.selenium.support.ui.Select select5 = new org.openqa.selenium.support.ui.Select(driver.findElement(By.xpath("//select[@id='outerForm:currencyCode']")));
		                        select5.selectByVisibleText("DOLLAR [US]");
		                        Thread.sleep(500);
		                        driver.findElement(By.xpath("//input[@type='submit'][@name='outerForm:Submit']")).click();
		                        // Add Screenshot will only work for Android platform

		                        takeScreenshot();

		                        Thread.sleep(500);
		                        driver.findElement(By.xpath("//input[@type='submit'][@name='outerForm:Cancel']")).click();
		                        Thread.sleep(500);
		                        driver.findElement(By.xpath("//input[@type='submit'][@name='outerForm:Cancel']")).click();
		                        Thread.sleep(500);

		                        driver.findElement(By.xpath("//input[@type='submit'][@name='outerForm:Submit']")).click();
		                        // Add Screenshot will only work for Android platform

		                        takeScreenshot();

		                        Thread.sleep(500);

		                        WebElement eleConfirmSubmit = driver.findElement(By.xpath("//input[@type='submit'][@name='outerForm:ConfirmSubmit']"));
		                        if (eleConfirmSubmit.isDisplayed()){
		                            eleConfirmSubmit.click();
		                            takeScreenshot();
		                            Thread.sleep(500);
		                        }

		                        Assert.assertTrue(driver.findElement(By.xpath("//li[@class='MessageInfo']")).getText().equals("Request completed successfully."));
		                        // Add Screenshot will only work for Android platform

		                        takeScreenshot();

		                        Thread.sleep(500);

		                        driver.findElement(By.xpath("//input[@type='submit'][@name='outerForm:Cancel']")).click();
		                        Thread.sleep(500);
		                        // Add Screenshot will only work for Android platform

		                        takeScreenshot();

		                    } else if (defaultItem.contains("No Rollover")){
		                        driver.findElement(By.xpath("//input[@type='submit'][@name='outerForm:Submit']")).click();
		                        // Add Screenshot will only work for Android platform

		                        takeScreenshot();

		                        Assert.assertTrue(driver.findElement(By.xpath("//li[@class='MessageInfo']")).getText().equals("Request completed successfully."));
		                        // Add Screenshot will only work for Android platform

		                        takeScreenshot();

		                    } else {

		                        driver.findElement(By.xpath("//input[@type='submit'][@name='outerForm:Submit']")).click();
		                        // Add Screenshot will only work for Android platform

		                        takeScreenshot();

		                        WebElement eleConfirmSubmit = driver.findElement(By.xpath("//input[@type='submit'][@name='outerForm:ConfirmSubmit']"));
		                        if (eleConfirmSubmit.isDisplayed()){
		                            eleConfirmSubmit.click();
		                            takeScreenshot();
		                            Thread.sleep(500);
		                        }

		                        Assert.assertTrue(driver.findElement(By.xpath("//li[@class='MessageInfo']")).getText().equals("Request completed successfully."));
		                        // Add Screenshot will only work for Android platform

		                        takeScreenshot();

		                    }

		                    ;
		 
		 ;} 
		 catch(Exception e) { PrintVal("Renderer Script Exception : "+e.toString()); } 
		 PrintVal("Executed Step = CALLRENDERERSCRIPT,VALIDATEPOLICYEXCHANGEUAT3"); 
		takeFullScreenShot();
                 LOGGER.info("Executed Step = TAKEFULLSCREENSHOT,TGTYPESCREENREG");
        }
          LOGGER.info("Executed Step = ENDIF,TGTYPESCREENREG");
        CALL.$("WritePolicyQuickInquiryToCSV","TGTYPESCREENREG");

        HOVER.$("ELE_MENU_INQUIRY","//td[text()='Inquiry']","TGTYPESCREENREG");

        TAP.$("ELE_SUBMEN_QUICKINQUIRY","//td[contains(text(),'Quick Inquiry')]","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_POLICYNUMBER","//input[@type='text'][@name='outerForm:policyNumber']",var_PolicyNumber,"FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_CONTINUE","//input[@type='submit'][@name='outerForm:Continue']","TGTYPESCREENREG");

		writeToCSV ("Policy Number " , ActionWrapper.getElementValue("ELE_GETVAL_POLICYNUMBER", "//span[@id='outerForm:policyNumber']"));
                 LOGGER.info("Executed Step = WRITETOCSV,ELE_GETVAL_POLICYNUMBER,Policy Number,TGTYPESCREENREG");
		writeToCSV ("Status " , ActionWrapper.getElementValue("ELE_GETVAL_STATUS", "//span[@id='outerForm:statusText']"));
                 LOGGER.info("Executed Step = WRITETOCSV,ELE_GETVAL_STATUS,Status,TGTYPESCREENREG");
		writeToCSV ("Effective Date " , ActionWrapper.getElementValue("ELE_GETVAL_EFFECTIVEDATETEXTFROMGRID", "//span[@id='outerForm:grid:0:effectiveDateOut2']"));
                 LOGGER.info("Executed Step = WRITETOCSV,ELE_GETVAL_EFFECTIVEDATETEXTFROMGRID,Effective Date,TGTYPESCREENREG");
		writeToCSV ("State country " , ActionWrapper.getElementValue("ELE_GETVAL_STATECOUNTRY", "//span[@id='outerForm:ownerCityStateZip']"));
                 LOGGER.info("Executed Step = WRITETOCSV,ELE_GETVAL_STATECOUNTRY,State country,TGTYPESCREENREG");
		writeToCSV ("Agent Number " , ActionWrapper.getElementValue("ELE_GETVAL_AGENTNUMBER", "//span[@id='outerForm:agentNumber']"));
                 LOGGER.info("Executed Step = WRITETOCSV,ELE_GETVAL_AGENTNUMBER,Agent Number,TGTYPESCREENREG");
		var_Count = var_Count + ( 1 );
                 LOGGER.info("Executed Step = INCREASEVAR,var_Count,1,TGTYPESCREENREG");
    	}
          LOGGER.info("Executed Step = WEND,TGTYPESCREENREG");
    	resetAttemps();

    }


    @Test
    public void tc01createnewbusinesspolicyuat2regression3() {

        // No need to declare / print method name here, handled by TestNg listener
        // declare("tc01createnewbusinesspolicyuat2regression3");

        CALL.$("LoginToGIASFandGRegression","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_USERNAME","outerForm:usernameTGWEBCOMMAIdTGWEBCOMMA0","x5990","FALSE","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_PASSWORD","outerForm:passwordTGWEBCOMMAIdTGWEBCOMMA0","cnx12345","FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_LOGIN","outerForm:loginButtonTGWEBCOMMAIdTGWEBCOMMA0","TGTYPESCREENREG");

		int var_Count = 0;
                 LOGGER.info("Executed Step = VAR,Integer,var_Count,0,TGTYPESCREENREG");
        LOGGER.info("Executed Step = REPEAT IF");
    	while (check(var_Count,"<",1,"TGTYPESCREENREG")) {
          LOGGER.info("Executed Step = WHILE,var_Count,<,1,TGTYPESCREENREG");
		com.jayway.jsonpath.ReadContext var_CSVData = getJsonData("C:/GIAS_Automation/FandG Regression/InputData/FandGReg_CreateAPolicy.csv");
                 LOGGER.info("Executed Step = VAR,Json,var_CSVData,C:/GIAS_Automation/FandG Regression/InputData/FandGReg_CreateAPolicy.csv,TGTYPESCREENREG");
		String var_EffectiveDate = "null";
                 LOGGER.info("Executed Step = VAR,String,var_EffectiveDate,null,TGTYPESCREENREG");
		var_EffectiveDate = getJsonData(var_CSVData , "$.records["+var_Count+"].EffectiveDate");
                 LOGGER.info("Executed Step = STORE,var_EffectiveDate,var_CSVData,$.records[{var_Count}].EffectiveDate,TGTYPESCREENREG");
        CALL.$("AddNewBusinessPolicyUAT2Regression2","TGTYPESCREENREG");

        HOVER.$("ELE_MENU_NEWBUSINESS","//td[text()='New Business']","TGTYPESCREENREG");

        TAP.$("ELE_SUBMEN_APPLICATIONENTRYUPDATE","//td[text()='Application Entry/Update']","TGTYPESCREENREG");

        TAP.$("ELE_RNDBTN_ADD","//img[@id='outerForm:gnAddImage']","TGTYPESCREENREG");

		String var_PolicyNumber;
                 LOGGER.info("Executed Step = VAR,String,var_PolicyNumber,TGTYPESCREENREG");
		var_PolicyNumber = getJsonData(var_CSVData , "$.records["+var_Count+"].PolicyNumber");
                 LOGGER.info("Executed Step = STORE,var_PolicyNumber,var_CSVData,$.records[{var_Count}].PolicyNumber,TGTYPESCREENREG");
		String var_Name1;
                 LOGGER.info("Executed Step = VAR,String,var_Name1,TGTYPESCREENREG");
		var_Name1 = getJsonData(var_CSVData , "$.records["+var_Count+"].Name1");
                 LOGGER.info("Executed Step = STORE,var_Name1,var_CSVData,$.records[{var_Count}].Name1,TGTYPESCREENREG");
		String var_FirstName1;
                 LOGGER.info("Executed Step = VAR,String,var_FirstName1,TGTYPESCREENREG");
		var_FirstName1 = getJsonData(var_CSVData , "$.records["+var_Count+"].First1");
                 LOGGER.info("Executed Step = STORE,var_FirstName1,var_CSVData,$.records[{var_Count}].First1,TGTYPESCREENREG");
		String var_LastName1;
                 LOGGER.info("Executed Step = VAR,String,var_LastName1,TGTYPESCREENREG");
		var_LastName1 = getJsonData(var_CSVData , "$.records["+var_Count+"].Last1");
                 LOGGER.info("Executed Step = STORE,var_LastName1,var_CSVData,$.records[{var_Count}].Last1,TGTYPESCREENREG");
		String var_BirthStateCountry1;
                 LOGGER.info("Executed Step = VAR,String,var_BirthStateCountry1,TGTYPESCREENREG");
		var_BirthStateCountry1 = getJsonData(var_CSVData , "$.records["+var_Count+"].BirthStateCountry1");
                 LOGGER.info("Executed Step = STORE,var_BirthStateCountry1,var_CSVData,$.records[{var_Count}].BirthStateCountry1,TGTYPESCREENREG");
		String var_DateOfBirth1;
                 LOGGER.info("Executed Step = VAR,String,var_DateOfBirth1,TGTYPESCREENREG");
		var_DateOfBirth1 = getJsonData(var_CSVData , "$.records["+var_Count+"].DateOfBirth1");
                 LOGGER.info("Executed Step = STORE,var_DateOfBirth1,var_CSVData,$.records[{var_Count}].DateOfBirth1,TGTYPESCREENREG");
		String var_Gender1;
                 LOGGER.info("Executed Step = VAR,String,var_Gender1,TGTYPESCREENREG");
		var_Gender1 = getJsonData(var_CSVData , "$.records["+var_Count+"].Gender1");
                 LOGGER.info("Executed Step = STORE,var_Gender1,var_CSVData,$.records[{var_Count}].Gender1,TGTYPESCREENREG");
		String var_IDType1;
                 LOGGER.info("Executed Step = VAR,String,var_IDType1,TGTYPESCREENREG");
		var_IDType1 = getJsonData(var_CSVData , "$.records["+var_Count+"].IDType1");
                 LOGGER.info("Executed Step = STORE,var_IDType1,var_CSVData,$.records[{var_Count}].IDType1,TGTYPESCREENREG");
		String var_IDNumber1;
                 LOGGER.info("Executed Step = VAR,String,var_IDNumber1,TGTYPESCREENREG");
		var_IDNumber1 = getJsonData(var_CSVData , "$.records["+var_Count+"].IDNumber1");
                 LOGGER.info("Executed Step = STORE,var_IDNumber1,var_CSVData,$.records[{var_Count}].IDNumber1,TGTYPESCREENREG");
		String var_FirstName2;
                 LOGGER.info("Executed Step = VAR,String,var_FirstName2,TGTYPESCREENREG");
		var_FirstName2 = getJsonData(var_CSVData , "$.records["+var_Count+"].First2");
                 LOGGER.info("Executed Step = STORE,var_FirstName2,var_CSVData,$.records[{var_Count}].First2,TGTYPESCREENREG");
		String var_LastName2;
                 LOGGER.info("Executed Step = VAR,String,var_LastName2,TGTYPESCREENREG");
		var_LastName2 = getJsonData(var_CSVData , "$.records["+var_Count+"].Last2");
                 LOGGER.info("Executed Step = STORE,var_LastName2,var_CSVData,$.records[{var_Count}].Last2,TGTYPESCREENREG");
		String var_BirthStateCountry2;
                 LOGGER.info("Executed Step = VAR,String,var_BirthStateCountry2,TGTYPESCREENREG");
		var_BirthStateCountry2 = getJsonData(var_CSVData , "$.records["+var_Count+"].BirthStateCountry2");
                 LOGGER.info("Executed Step = STORE,var_BirthStateCountry2,var_CSVData,$.records[{var_Count}].BirthStateCountry2,TGTYPESCREENREG");
		String var_DateOfBirth2;
                 LOGGER.info("Executed Step = VAR,String,var_DateOfBirth2,TGTYPESCREENREG");
		var_DateOfBirth2 = getJsonData(var_CSVData , "$.records["+var_Count+"].DateOfBirth2");
                 LOGGER.info("Executed Step = STORE,var_DateOfBirth2,var_CSVData,$.records[{var_Count}].DateOfBirth2,TGTYPESCREENREG");
		String var_Gender2;
                 LOGGER.info("Executed Step = VAR,String,var_Gender2,TGTYPESCREENREG");
		var_Gender2 = getJsonData(var_CSVData , "$.records["+var_Count+"].Gender2");
                 LOGGER.info("Executed Step = STORE,var_Gender2,var_CSVData,$.records[{var_Count}].Gender2,TGTYPESCREENREG");
		String var_IDType2;
                 LOGGER.info("Executed Step = VAR,String,var_IDType2,TGTYPESCREENREG");
		var_IDType2 = getJsonData(var_CSVData , "$.records["+var_Count+"].IDType2");
                 LOGGER.info("Executed Step = STORE,var_IDType2,var_CSVData,$.records[{var_Count}].IDType2,TGTYPESCREENREG");
		String var_IDNumber2;
                 LOGGER.info("Executed Step = VAR,String,var_IDNumber2,TGTYPESCREENREG");
		var_IDNumber2 = getJsonData(var_CSVData , "$.records["+var_Count+"].IDNumber2");
                 LOGGER.info("Executed Step = STORE,var_IDNumber2,var_CSVData,$.records[{var_Count}].IDNumber2,TGTYPESCREENREG");
		String var_AddressLine11;
                 LOGGER.info("Executed Step = VAR,String,var_AddressLine11,TGTYPESCREENREG");
		var_AddressLine11 = getJsonData(var_CSVData , "$.records["+var_Count+"].AddressLine1_1");
                 LOGGER.info("Executed Step = STORE,var_AddressLine11,var_CSVData,$.records[{var_Count}].AddressLine1_1,TGTYPESCREENREG");
		String var_City1;
                 LOGGER.info("Executed Step = VAR,String,var_City1,TGTYPESCREENREG");
		var_City1 = getJsonData(var_CSVData , "$.records["+var_Count+"].City1");
                 LOGGER.info("Executed Step = STORE,var_City1,var_CSVData,$.records[{var_Count}].City1,TGTYPESCREENREG");
		String var_StateandCountry1;
                 LOGGER.info("Executed Step = VAR,String,var_StateandCountry1,TGTYPESCREENREG");
		var_StateandCountry1 = getJsonData(var_CSVData , "$.records["+var_Count+"].StateandCountry1");
                 LOGGER.info("Executed Step = STORE,var_StateandCountry1,var_CSVData,$.records[{var_Count}].StateandCountry1,TGTYPESCREENREG");
		String var_ZipCode1;
                 LOGGER.info("Executed Step = VAR,String,var_ZipCode1,TGTYPESCREENREG");
		var_ZipCode1 = getJsonData(var_CSVData , "$.records["+var_Count+"].ZipCode1");
                 LOGGER.info("Executed Step = STORE,var_ZipCode1,var_CSVData,$.records[{var_Count}].ZipCode1,TGTYPESCREENREG");
		String var_AddressLine12;
                 LOGGER.info("Executed Step = VAR,String,var_AddressLine12,TGTYPESCREENREG");
		var_AddressLine12 = getJsonData(var_CSVData , "$.records["+var_Count+"].AddressLine1_2");
                 LOGGER.info("Executed Step = STORE,var_AddressLine12,var_CSVData,$.records[{var_Count}].AddressLine1_2,TGTYPESCREENREG");
		String var_City2;
                 LOGGER.info("Executed Step = VAR,String,var_City2,TGTYPESCREENREG");
		var_City2 = getJsonData(var_CSVData , "$.records["+var_Count+"].City2");
                 LOGGER.info("Executed Step = STORE,var_City2,var_CSVData,$.records[{var_Count}].City2,TGTYPESCREENREG");
		String var_StateandCountry2;
                 LOGGER.info("Executed Step = VAR,String,var_StateandCountry2,TGTYPESCREENREG");
		var_StateandCountry2 = getJsonData(var_CSVData , "$.records["+var_Count+"].StateandCountry2");
                 LOGGER.info("Executed Step = STORE,var_StateandCountry2,var_CSVData,$.records[{var_Count}].StateandCountry2,TGTYPESCREENREG");
		String var_ZipCode2;
                 LOGGER.info("Executed Step = VAR,String,var_ZipCode2,TGTYPESCREENREG");
		var_ZipCode2 = getJsonData(var_CSVData , "$.records["+var_Count+"].ZipCode2");
                 LOGGER.info("Executed Step = STORE,var_ZipCode2,var_CSVData,$.records[{var_Count}].ZipCode2,TGTYPESCREENREG");
		String var_SelectTypeTwo;
                 LOGGER.info("Executed Step = VAR,String,var_SelectTypeTwo,TGTYPESCREENREG");
		var_SelectTypeTwo = getJsonData(var_CSVData , "$.records["+var_Count+"].SelectTypeTwo");
                 LOGGER.info("Executed Step = STORE,var_SelectTypeTwo,var_CSVData,$.records[{var_Count}].SelectTypeTwo,TGTYPESCREENREG");
		String var_Name2;
                 LOGGER.info("Executed Step = VAR,String,var_Name2,TGTYPESCREENREG");
		var_Name2 = getJsonData(var_CSVData , "$.records["+var_Count+"].Name2");
                 LOGGER.info("Executed Step = STORE,var_Name2,var_CSVData,$.records[{var_Count}].Name2,TGTYPESCREENREG");
		String var_SelectTypeThree;
                 LOGGER.info("Executed Step = VAR,String,var_SelectTypeThree,TGTYPESCREENREG");
		var_SelectTypeThree = getJsonData(var_CSVData , "$.records["+var_Count+"].SelectTypeThree");
                 LOGGER.info("Executed Step = STORE,var_SelectTypeThree,var_CSVData,$.records[{var_Count}].SelectTypeThree,TGTYPESCREENREG");
		String var_ApplicationDate;
                 LOGGER.info("Executed Step = VAR,String,var_ApplicationDate,TGTYPESCREENREG");
		var_ApplicationDate = getJsonData(var_CSVData , "$.records["+var_Count+"].ApplicationDate");
                 LOGGER.info("Executed Step = STORE,var_ApplicationDate,var_CSVData,$.records[{var_Count}].ApplicationDate,TGTYPESCREENREG");
		String var_IssueStateCountry;
                 LOGGER.info("Executed Step = VAR,String,var_IssueStateCountry,TGTYPESCREENREG");
		var_IssueStateCountry = getJsonData(var_CSVData , "$.records["+var_Count+"].IssueStateCountry");
                 LOGGER.info("Executed Step = STORE,var_IssueStateCountry,var_CSVData,$.records[{var_Count}].IssueStateCountry,TGTYPESCREENREG");
		String var_EffectiveDatenew;
                 LOGGER.info("Executed Step = VAR,String,var_EffectiveDatenew,TGTYPESCREENREG");
		var_EffectiveDatenew = getJsonData(var_CSVData , "$.records["+var_Count+"].EffectiveDate");
                 LOGGER.info("Executed Step = STORE,var_EffectiveDatenew,var_CSVData,$.records[{var_Count}].EffectiveDate,TGTYPESCREENREG");
		String var_CashWithApplication;
                 LOGGER.info("Executed Step = VAR,String,var_CashWithApplication,TGTYPESCREENREG");
		var_CashWithApplication = getJsonData(var_CSVData , "$.records["+var_Count+"].CashWithApplication");
                 LOGGER.info("Executed Step = STORE,var_CashWithApplication,var_CSVData,$.records[{var_Count}].CashWithApplication,TGTYPESCREENREG");
		String var_TaxQualifiedDescription;
                 LOGGER.info("Executed Step = VAR,String,var_TaxQualifiedDescription,TGTYPESCREENREG");
		var_TaxQualifiedDescription = getJsonData(var_CSVData , "$.records["+var_Count+"].TaxQualifiedDescription");
                 LOGGER.info("Executed Step = STORE,var_TaxQualifiedDescription,var_CSVData,$.records[{var_Count}].TaxQualifiedDescription,TGTYPESCREENREG");
		String var_AgentNumberOne;
                 LOGGER.info("Executed Step = VAR,String,var_AgentNumberOne,TGTYPESCREENREG");
		var_AgentNumberOne = getJsonData(var_CSVData , "$.records["+var_Count+"].AgentNumberOne");
                 LOGGER.info("Executed Step = STORE,var_AgentNumberOne,var_CSVData,$.records[{var_Count}].AgentNumberOne,TGTYPESCREENREG");
		String var_SituationCode1;
                 LOGGER.info("Executed Step = VAR,String,var_SituationCode1,TGTYPESCREENREG");
		var_SituationCode1 = getJsonData(var_CSVData , "$.records["+var_Count+"].SituationCode1");
                 LOGGER.info("Executed Step = STORE,var_SituationCode1,var_CSVData,$.records[{var_Count}].SituationCode1,TGTYPESCREENREG");
		String var_SplitPercentage1;
                 LOGGER.info("Executed Step = VAR,String,var_SplitPercentage1,TGTYPESCREENREG");
		var_SplitPercentage1 = getJsonData(var_CSVData , "$.records["+var_Count+"].SplitPercentage1");
                 LOGGER.info("Executed Step = STORE,var_SplitPercentage1,var_CSVData,$.records[{var_Count}].SplitPercentage1,TGTYPESCREENREG");
		String var_AgentNumberTwo;
                 LOGGER.info("Executed Step = VAR,String,var_AgentNumberTwo,TGTYPESCREENREG");
		var_AgentNumberTwo = getJsonData(var_CSVData , "$.records["+var_Count+"].AgentNumberTwo");
                 LOGGER.info("Executed Step = STORE,var_AgentNumberTwo,var_CSVData,$.records[{var_Count}].AgentNumberTwo,TGTYPESCREENREG");
		String var_SituationCode2;
                 LOGGER.info("Executed Step = VAR,String,var_SituationCode2,TGTYPESCREENREG");
		var_SituationCode2 = getJsonData(var_CSVData , "$.records["+var_Count+"].SituationCode2");
                 LOGGER.info("Executed Step = STORE,var_SituationCode2,var_CSVData,$.records[{var_Count}].SituationCode2,TGTYPESCREENREG");
		String var_SplitPercentage2;
                 LOGGER.info("Executed Step = VAR,String,var_SplitPercentage2,TGTYPESCREENREG");
		var_SplitPercentage2 = getJsonData(var_CSVData , "$.records["+var_Count+"].SplitPercentage2");
                 LOGGER.info("Executed Step = STORE,var_SplitPercentage2,var_CSVData,$.records[{var_Count}].SplitPercentage2,TGTYPESCREENREG");
		try { 
		 var_AgentNumberOne = "000112680";
				var_AgentNumberTwo = "000112680";
				var_SituationCode1 = "01";
				var_SituationCode2 = "01";
		 
		 ;} 
		 catch(Exception e) { PrintVal("Renderer Script Exception : "+e.toString()); } 
		 PrintVal("Executed Step = CALLRENDERERSCRIPT,ASSIGNSTATICVALUEFORAGENTNUMBERANDSITUATIONCODE"); 
        TYPE.$("ELE_TXTBOX_POLICYNUMBER","//input[@type='text'][@name='outerForm:policyNumber']",var_PolicyNumber,"FALSE","TGTYPESCREENREG");

        SELECT.$("ELE_DRPDWN_CURRENCYREGRESSION","//select[@id='outerForm:currencyCode']","DOLLAR [US]","TGTYPESCREENREG");

        SELECT.$("ELE_DRPDWN_STATECOUNTRY","//select[@name='outerForm:countryAndStateCode']	",var_StateandCountry1,"TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_CASHWITHAPPLICATION","//input[@type='text'][@name='outerForm:cashWithApplication']",var_CashWithApplication,"FALSE","TGTYPESCREENREG");

        SELECT.$("ELE_DRPDWN_PAYMENTFREQUENCY","//select[@name='outerForm:paymentMode']","Annual","TGTYPESCREENREG");

        SELECT.$("ELE_DRPDWN_PAYMENTMETHOD","//select[@name='outerForm:paymentCode']","No Notice","TGTYPESCREENREG");

        SELECT.$("ELE_DRPDWN_TAXQUALIFIEDDESCRIPTION","//select[@name='outerForm:taxQualifiedCode']",var_TaxQualifiedDescription,"TGTYPESCREENREG");

        SELECT.$("ELE_DRPDWN_TAXWITHHOLDING","//select[@name='outerForm:taxWithholdingCode']","No Withholding","TGTYPESCREENREG");

        TAP.$("ELE_LINK_SELECTCLIENT","//span[text()='Select Client']","TGTYPESCREENREG");

        TAP.$("ELE_RNDBTN_ADD","//img[@id='outerForm:gnAddImage']","TGTYPESCREENREG");

        LOGGER.info("Executed Step = START IF");
        if (check(var_FirstName2,"=","null","TGTYPESCREENREG")) {
         LOGGER.info("Executed Step = IF,var_FirstName2,=,null,TGTYPESCREENREG");
        TYPE.$("ELE_TXTBOX_FIRSTNAME","//input[@type='text'][@name='outerForm:nameFirst']",var_FirstName1,"FALSE","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_LASTNAME","//input[@type='text'][@name='outerForm:nameLast']",var_LastName1,"FALSE","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_CLIENTDOB","//input[@type='text'][@name='outerForm:dateOfBirth2']",var_DateOfBirth1,"FALSE","TGTYPESCREENREG");

        SELECT.$("ELE_DRPDWN_CLIENTGENDER","//select[@name='outerForm:sexCode2']",var_Gender1,"TGTYPESCREENREG");

        SELECT.$("ELE_DRPDWN_IDTYPE","//select[@name='outerForm:taxIdenUsag2']",var_IDType1,"TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_IDNUMBER","//input[@type='text'][@name='outerForm:identificationNumber2']",var_IDNumber1,"FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SUBMIT","//input[@type='submit'][@name='outerForm:Submit']","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_ADDRESSLINE1","//input[@type='text'][@name='outerForm:addressLineOne']",var_AddressLine11,"FALSE","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_CITY","//input[@type='text'][@name='outerForm:addressCity']",var_City1,"FALSE","TGTYPESCREENREG");

        SELECT.$("ELE_DRPDWN_STATECOUNTRY","//select[@name='outerForm:countryAndStateCode']	",var_StateandCountry1,"TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_ZIPCODE","//input[@type='text'][@name='outerForm:zipCode']",var_ZipCode1,"FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SUBMIT","//input[@type='submit'][@name='outerForm:Submit']","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_CANCEL","//input[@type='submit'][@name='outerForm:Cancel']","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_SEARCHCLIENTNAME","//input[@type='text'][@name='outerForm:grid:individualName']",var_Name1,"FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_FINDPOSTO","//img[@id='outerForm:gnFindImage']","TGTYPESCREENREG");

        SELECT.$("ELE_DRPDWN_RELATIONSHIPGRID","//select[@name='outerForm:grid:0:relationship']","Owner one","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SUBMIT","//input[@type='submit'][@name='outerForm:Submit']","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_AGENTNUMBER","//input[@type='text'][@name='outerForm:grid:0:agentNumberOut2']",var_AgentNumberOne,"FALSE","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_SITUATIONCODE","//input[@type='text'][@name='outerForm:grid:0:agentSituationCodeOut2']",var_SituationCode1,"FALSE","TGTYPESCREENREG");

        }
         else {
          LOGGER.info("Executed Step = ELSE,TGTYPESCREENREG");
        TYPE.$("ELE_TXTBOX_FIRSTNAME","//input[@type='text'][@name='outerForm:nameFirst']",var_FirstName1,"FALSE","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_LASTNAME","//input[@type='text'][@name='outerForm:nameLast']",var_LastName1,"FALSE","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_CLIENTDOB","//input[@type='text'][@name='outerForm:dateOfBirth2']",var_DateOfBirth1,"FALSE","TGTYPESCREENREG");

        SELECT.$("ELE_DRPDWN_CLIENTGENDER","//select[@name='outerForm:sexCode2']",var_Gender1,"TGTYPESCREENREG");

        SELECT.$("ELE_DRPDWN_IDTYPE","//select[@name='outerForm:taxIdenUsag2']",var_IDType1,"TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_IDNUMBER","//input[@type='text'][@name='outerForm:identificationNumber2']",var_IDNumber1,"FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SUBMIT","//input[@type='submit'][@name='outerForm:Submit']","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_ADDRESSLINE1","//input[@type='text'][@name='outerForm:addressLineOne']",var_AddressLine11,"FALSE","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_CITY","//input[@type='text'][@name='outerForm:addressCity']",var_City1,"FALSE","TGTYPESCREENREG");

        SELECT.$("ELE_DRPDWN_STATECOUNTRY","//select[@name='outerForm:countryAndStateCode']	",var_StateandCountry1,"TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_ZIPCODE","//input[@type='text'][@name='outerForm:zipCode']",var_ZipCode1,"FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SUBMIT","//input[@type='submit'][@name='outerForm:Submit']","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_FIRSTNAME","//input[@type='text'][@name='outerForm:nameFirst']",var_FirstName2,"FALSE","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_LASTNAME","//input[@type='text'][@name='outerForm:nameLast']",var_LastName2,"FALSE","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_CLIENTDOB","//input[@type='text'][@name='outerForm:dateOfBirth2']",var_DateOfBirth2,"FALSE","TGTYPESCREENREG");

        SELECT.$("ELE_DRPDWN_CLIENTGENDER","//select[@name='outerForm:sexCode2']",var_Gender2,"TGTYPESCREENREG");

        SELECT.$("ELE_DRPDWN_IDTYPE","//select[@name='outerForm:taxIdenUsag2']",var_IDType2,"TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_IDNUMBER","//input[@type='text'][@name='outerForm:identificationNumber2']",var_IDNumber2,"FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SUBMIT","//input[@type='submit'][@name='outerForm:Submit']","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_ADDRESSLINE1","//input[@type='text'][@name='outerForm:addressLineOne']",var_AddressLine12,"FALSE","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_CITY","//input[@type='text'][@name='outerForm:addressCity']",var_City2,"FALSE","TGTYPESCREENREG");

        SELECT.$("ELE_DRPDWN_STATECOUNTRY","//select[@name='outerForm:countryAndStateCode']	",var_StateandCountry2,"TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_ZIPCODE","//input[@type='text'][@name='outerForm:zipCode']",var_ZipCode2,"FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SUBMIT","//input[@type='submit'][@name='outerForm:Submit']","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_CANCEL","//input[@type='submit'][@name='outerForm:Cancel']","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_SEARCHCLIENTNAME","//input[@type='text'][@name='outerForm:grid:individualName']",var_Name1,"FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_FINDPOSTO","//img[@id='outerForm:gnFindImage']","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_SEARCHCLIENTNAME","//input[@type='text'][@name='outerForm:grid:individualName']",var_Name2,"FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_FINDPOSTO","//img[@id='outerForm:gnFindImage']","TGTYPESCREENREG");

        SELECT.$("ELE_DRPDWN_RELATIONSHIPGRID","//select[@name='outerForm:grid:0:relationship']",var_SelectTypeTwo,"TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_SUBMIT","//input[@type='submit'][@name='outerForm:Submit']","TGTYPESCREENREG");

        LOGGER.info("Executed Step = START IF");
        if (check(var_AgentNumberTwo,"=","null","TGTYPESCREENREG")) {
         LOGGER.info("Executed Step = IF,var_AgentNumberTwo,=,null,TGTYPESCREENREG");
        TYPE.$("ELE_TXTBOX_AGENTNUMBER","//input[@type='text'][@name='outerForm:grid:0:agentNumberOut2']",var_AgentNumberOne,"FALSE","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_SITUATIONCODE","//input[@type='text'][@name='outerForm:grid:0:agentSituationCodeOut2']",var_SituationCode1,"FALSE","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_SPLITPERCENTAGE","//input[@type='text'][@name='outerForm:grid:0:agentSplitPercentageOut2']",var_SplitPercentage1,"FALSE","TGTYPESCREENREG");

        }
         else {
          LOGGER.info("Executed Step = ELSE,TGTYPESCREENREG");
        TYPE.$("ELE_TXTBOX_AGENTNUMBER","//input[@type='text'][@name='outerForm:grid:0:agentNumberOut2']",var_AgentNumberOne,"FALSE","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_SITUATIONCODE","//input[@type='text'][@name='outerForm:grid:0:agentSituationCodeOut2']",var_SituationCode1,"FALSE","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_SPLITPERCENTAGE","//input[@type='text'][@name='outerForm:grid:0:agentSplitPercentageOut2']",var_SplitPercentage1,"FALSE","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_AGENTNUMBER2","//input[@type='text'][@name='outerForm:grid:1:agentNumberOut2']",var_AgentNumberTwo,"FALSE","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_SITUATIONCODE2","//input[@type='text'][@name='outerForm:grid:1:agentSituationCodeOut2']",var_SituationCode2,"FALSE","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_SPLITPERCENTAGE2","//input[@type='text'][@name='outerForm:grid:1:agentSplitPercentageOut2']",var_SplitPercentage2,"FALSE","TGTYPESCREENREG");

        }
          LOGGER.info("Executed Step = ENDIF,TGTYPESCREENREG");
        }
          LOGGER.info("Executed Step = ENDIF,TGTYPESCREENREG");
        TAP.$("ELE_BUTTON_SUBMIT","//input[@type='submit'][@name='outerForm:Submit']","TGTYPESCREENREG");

        ASSERT.$("ELE_GETMSG_SUCCESS","//li[@class='MessageInfo']","CONTAINS","successfully","TGTYPESCREENREG");

        CALL.$("SettlePolicyUAT2Regression2","TGTYPESCREENREG");

        TAP.$("ELE_LINK_BENEFITS","//span[text()='Benefits']","TGTYPESCREENREG");

		String var_Plan;
                 LOGGER.info("Executed Step = VAR,String,var_Plan,TGTYPESCREENREG");
		var_Plan = getJsonData(var_CSVData , "$.records["+var_Count+"].Plan");
                 LOGGER.info("Executed Step = STORE,var_Plan,var_CSVData,$.records[{var_Count}].Plan,TGTYPESCREENREG");
        SELECT.$("ELE_DRPDWN_ADDABENEFIT","//select[@name='outerForm:initialPlanCode']",var_Plan,"TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_CONTINUE","//input[@type='submit'][@name='outerForm:Continue']","TGTYPESCREENREG");

        TAP.$("ELE_LINK_FUNDSELECTION","//span[contains(text(),'Fund Selection')]","TGTYPESCREENREG");

		try { 
		 List<WebElement> baseRider= driver.findElements(By.cssSelector("*[class='ListNumeric']"));
		              int baseRiderCount=  baseRider.size();
		              String nullString = "null";
		              if( baseRiderCount> 0) {

		Double totalAmount = 0.0;
		            for(int i=0; i< 12;i++){
		                int incrementali = i+1;
		                String PremiumAllocation = "PremiumAllocation"+incrementali;
		                String premiumAllocPercOut2 = getJsonData(var_CSVData , "$.records["+var_Count+"]."+PremiumAllocation.trim()+"");
		                if ((premiumAllocPercOut2 != null) && !(premiumAllocPercOut2.trim().equalsIgnoreCase(nullString))) {
		                    totalAmount = totalAmount + Double.parseDouble(premiumAllocPercOut2.trim());
		                }
		            }
		            totalAmount = Math.round(totalAmount * 10.0) / 10.0;
		            Double maximumPercentage = 1.0;
		            if (Double.compare(totalAmount, maximumPercentage) != 0){
		                Assert.assertTrue(false,"Total fund allocation must be 1.0");
		            }

		                  for(int j=0; j< baseRiderCount;j++){
		                      WebElement eleTextBoxPercentage = driver.findElement(By.xpath("//input[@type='text'][@name='outerForm:grid:" + j + ":premiumAllocPercOut2']"));
		                      eleTextBoxPercentage.clear();
		                      eleTextBoxPercentage.sendKeys("0.0");
		                  }

		                  for(int i=0; i< 12;i++)
		                  {
		                      int incrementali = i+1;
		                      String Funds = "Fund"+ incrementali;
		                      String PremiumAllocation = "PremiumAllocation"+ incrementali;

		                      String fundName = getJsonData(var_CSVData , "$.records["+var_Count+"]."+Funds+"");
		                      LOGGER.info("fundname111"+fundName);
		                      String premiumAllocPercOut2 = getJsonData(var_CSVData , "$.records["+var_Count+"]."+PremiumAllocation+"");
		                      LOGGER.info("fundname222"+premiumAllocPercOut2);

		                      if ((fundName != null) && !(fundName.trim().equalsIgnoreCase(nullString))) {
		                          LOGGER.info("fundname333");
		                          for(int j=0; j< baseRiderCount;j++)
		                          {
		                              WebElement eleFundName = driver.findElement(By.cssSelector("span[id='outerForm:grid:" + j + ":fundCodeTextOut2']"));
		                              String fullFundName = eleFundName.getText();
		                              LOGGER.info("fundname444"+fullFundName);
		                              if (fullFundName.toLowerCase().trim().contains(fundName.toLowerCase().trim())){
		                                  WebElement eleTextBoxPercentage = driver.findElement(By.xpath("//input[@type='text'][@name='outerForm:grid:" + j + ":premiumAllocPercOut2']"));
		                                  eleTextBoxPercentage.clear();
		                                  if ((premiumAllocPercOut2 != null) && !(premiumAllocPercOut2.trim().equalsIgnoreCase(nullString))){
		                                      eleTextBoxPercentage.sendKeys(premiumAllocPercOut2);
		                                  } else {
		                                      eleTextBoxPercentage.sendKeys("0.0");
		                                  }

		                                  takeScreenshot();
		                                  break;
		                              }

		                          }
		                      }

		                      System.out.println("next value of i is  " + i);

		                  }
		              }
		 
		 ;} 
		 catch(Exception e) { PrintVal("Renderer Script Exception : "+e.toString()); } 
		 PrintVal("Executed Step = CALLRENDERERSCRIPT,FUNDSELECTIONUAT3"); 
		takeFullScreenShot();
                 LOGGER.info("Executed Step = TAKEFULLSCREENSHOT,TGTYPESCREENREG");
        TAP.$("ELE_BUTTON_SUBMIT","//input[@type='submit'][@name='outerForm:Submit']","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_UNITSAFTERADDINGBENEFIT","//input[@type='text'][@name='outerForm:unitsOfInsurance']	","0.000","FALSE","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_REQUESTPREMIUNAMNT","//input[@id='outerForm:annualPremium']",var_CashWithApplication,"TRUE","TGTYPESCREENREG");

        LOGGER.info("Executed Step = START IF");
        if (check("ELE_DRPDWN_GUARWITHDRAWALMETHODREGRESSION","//select[@id='outerForm:gridSupp:0:guarWithdrawalMethodOut3']","VISIBLE","TGTYPESCREENREG")) {
         LOGGER.info("Executed Step = IF,ELE_DRPDWN_GUARWITHDRAWALMETHODREGRESSION,VISIBLE,TGTYPESCREENREG");
        SELECT.$("ELE_DRPDWN_GUARWITHDRAWALMETHODREGRESSION","//select[@id='outerForm:gridSupp:0:guarWithdrawalMethodOut3']","Single","TGTYPESCREENREG");

        }
          LOGGER.info("Executed Step = ENDIF,TGTYPESCREENREG");
        TAP.$("ELE_BUTTON_SUBMIT","//input[@type='submit'][@name='outerForm:Submit']","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_CANCEL","//input[@type='submit'][@name='outerForm:Cancel']","TGTYPESCREENREG");

        TAP.$("ELE_LINK_ISSUE","//span[@id='outerForm:IssueText']","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_EFFECTIVEDATE","//input[@type='text'][@name='outerForm:effectiveDate']",var_EffectiveDatenew,"FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_ISSUE","//input[@id='outerForm:Issue']","TGTYPESCREENREG");

        TAP.$("ELE_LINK_SETTLE","//span[text()='Settle']	","TGTYPESCREENREG");

		String var_PolicyExchangeCheck;
                 LOGGER.info("Executed Step = VAR,String,var_PolicyExchangeCheck,TGTYPESCREENREG");
		var_PolicyExchangeCheck = getJsonData(var_CSVData , "$.records["+var_Count+"].Clients/Endorsements/Exclusions/FundSelection?PolicyExchange");
                 LOGGER.info("Executed Step = STORE,var_PolicyExchangeCheck,var_CSVData,$.records[{var_Count}].Clients/Endorsements/Exclusions/FundSelection?PolicyExchange,TGTYPESCREENREG");
        LOGGER.info("Executed Step = START IF");
        if (check(var_PolicyExchangeCheck,"=",var_PolicyExchangeCheck,"TGTYPESCREENREG")) {
         LOGGER.info("Executed Step = IF,var_PolicyExchangeCheck,=,var_PolicyExchangeCheck,TGTYPESCREENREG");
        CALL.$("ValidatePolicyExchangeUAT4","TGTYPESCREENREG");

		String var_ModalPremium = "null";
                 LOGGER.info("Executed Step = VAR,String,var_ModalPremium,null,TGTYPESCREENREG");
		var_ModalPremium = ActionWrapper.getElementValue("ELE_GETVAL_MODALPREMIUMAMNTPOLICYEXCHANGE", "//span[@id='outerForm:modalPremium']");
                 LOGGER.info("Executed Step = STORE,var_ModalPremium,ELE_GETVAL_MODALPREMIUMAMNTPOLICYEXCHANGE,$.records[{var_Count}].EstimatedAmount,TGTYPESCREENREG");
		String var_ExchangeType = "null";
                 LOGGER.info("Executed Step = VAR,String,var_ExchangeType,null,TGTYPESCREENREG");
		var_ExchangeType = getJsonData(var_CSVData , "$.records["+var_Count+"].ExchangeType");
                 LOGGER.info("Executed Step = STORE,var_ExchangeType,var_CSVData,$.records[{var_Count}].ExchangeType,TGTYPESCREENREG");
		String var_ReplacementType = "null";
                 LOGGER.info("Executed Step = VAR,String,var_ReplacementType,null,TGTYPESCREENREG");
		var_ReplacementType = getJsonData(var_CSVData , "$.records["+var_Count+"].ReplacementType");
                 LOGGER.info("Executed Step = STORE,var_ReplacementType,var_CSVData,$.records[{var_Count}].ReplacementType,TGTYPESCREENREG");
		String var_PriorCompanyName = "null";
                 LOGGER.info("Executed Step = VAR,String,var_PriorCompanyName,null,TGTYPESCREENREG");
		var_PriorCompanyName = getJsonData(var_CSVData , "$.records["+var_Count+"].PriorCompanName");
                 LOGGER.info("Executed Step = STORE,var_PriorCompanyName,var_CSVData,$.records[{var_Count}].PriorCompanName,TGTYPESCREENREG");
		String var_PriorInvestmentType = "null";
                 LOGGER.info("Executed Step = VAR,String,var_PriorInvestmentType,null,TGTYPESCREENREG");
		var_PriorInvestmentType = getJsonData(var_CSVData , "$.records["+var_Count+"].PriorInvestmentType");
                 LOGGER.info("Executed Step = STORE,var_PriorInvestmentType,var_CSVData,$.records[{var_Count}].PriorInvestmentType,TGTYPESCREENREG");
		String var_PolicyStatus = "null";
                 LOGGER.info("Executed Step = VAR,String,var_PolicyStatus,null,TGTYPESCREENREG");
		var_PolicyStatus = getJsonData(var_CSVData , "$.records["+var_Count+"].Status");
                 LOGGER.info("Executed Step = STORE,var_PolicyStatus,var_CSVData,$.records[{var_Count}].Status,TGTYPESCREENREG");
		String var_RollOverType = "null";
                 LOGGER.info("Executed Step = VAR,String,var_RollOverType,null,TGTYPESCREENREG");
		var_RollOverType = getJsonData(var_CSVData , "$.records["+var_Count+"].RollOverType");
                 LOGGER.info("Executed Step = STORE,var_RollOverType,var_CSVData,$.records[{var_Count}].RollOverType,TGTYPESCREENREG");
        SELECT.$("ELE_DRPDWN_ROLLOVERTYPE","//select[@id='outerForm:rolloverType']",var_RollOverType,"TGTYPESCREENREG");

		String var_RolloverPremium = "null";
                 LOGGER.info("Executed Step = VAR,String,var_RolloverPremium,null,TGTYPESCREENREG");
		var_RolloverPremium = getJsonData(var_CSVData , "$.records["+var_Count+"].RolloverPremium");
                 LOGGER.info("Executed Step = STORE,var_RolloverPremium,var_CSVData,$.records[{var_Count}].RolloverPremium,TGTYPESCREENREG");
		String var_preBasisContribution = "null";
                 LOGGER.info("Executed Step = VAR,String,var_preBasisContribution,null,TGTYPESCREENREG");
		var_preBasisContribution = getJsonData(var_CSVData , "$.records["+var_Count+"].PreTEFRABasisContribution");
                 LOGGER.info("Executed Step = STORE,var_preBasisContribution,var_CSVData,$.records[{var_Count}].PreTEFRABasisContribution,TGTYPESCREENREG");
		String var_preGainContribution = "null";
                 LOGGER.info("Executed Step = VAR,String,var_preGainContribution,null,TGTYPESCREENREG");
		var_preGainContribution = getJsonData(var_CSVData , "$.records["+var_Count+"].PreTEFRAGainContribution");
                 LOGGER.info("Executed Step = STORE,var_preGainContribution,var_CSVData,$.records[{var_Count}].PreTEFRAGainContribution,TGTYPESCREENREG");
		String var_postBasisContribution = "null";
                 LOGGER.info("Executed Step = VAR,String,var_postBasisContribution,null,TGTYPESCREENREG");
		var_postBasisContribution = getJsonData(var_CSVData , "$.records["+var_Count+"].PostTEFRABasisContribution");
                 LOGGER.info("Executed Step = STORE,var_postBasisContribution,var_CSVData,$.records[{var_Count}].PostTEFRABasisContribution,TGTYPESCREENREG");
		String var_postGainContribution = "null";
                 LOGGER.info("Executed Step = VAR,String,var_postGainContribution,null,TGTYPESCREENREG");
		var_postGainContribution = getJsonData(var_CSVData , "$.records["+var_Count+"].PostTEFRAGainContribution");
                 LOGGER.info("Executed Step = STORE,var_postGainContribution,var_CSVData,$.records[{var_Count}].PostTEFRAGainContribution,TGTYPESCREENREG");
		try { 
		 org.openqa.selenium.support.ui.Select select = new org.openqa.selenium.support.ui.Select(driver.findElement(By.xpath("//select[@id='outerForm:rolloverType']")));
				                    // Add Screenshot will only work for Android platform

				                    takeScreenshot();

				                    WebElement option = select.getFirstSelectedOption();
				                    String defaultItem = option.getText();
				                    System.out.println(defaultItem);

		            String selectedPlanName = var_Plan.trim();

				                    if ((!defaultItem.contains("No Rollover")) && (!selectedPlanName.endsWith("NQ"))) {
				                        WebElement eleRolloverPremium = driver.findElement(By.xpath("//input[@id='outerForm:rolloverPremium']"));
				                        eleRolloverPremium.clear();
				                        eleRolloverPremium.sendKeys(var_RolloverPremium);

				                        Thread.sleep(500);

				                        WebElement eleModalPremium = driver.findElement(By.cssSelector("span[id='outerForm:modalPremium']"));
				                        String strModalPremium = eleModalPremium.getText();
				                        strModalPremium = strModalPremium.replace(",","");

				                        var_RolloverPremium = var_RolloverPremium.replace(",","");

				                        Double doubleModalPremium = Double.parseDouble(strModalPremium);
				                        Double doubleRollverPremium = Double.parseDouble(var_RolloverPremium);

				                        Double remainingValue = doubleModalPremium - doubleRollverPremium;

				                        WebElement elePaymentAtSettle = driver.findElement(By.xpath("//input[@id='outerForm:paymentAtSettle']"));
				                        elePaymentAtSettle.clear();
				                        elePaymentAtSettle.sendKeys(remainingValue.toString());

				                        takeScreenshot();

				                    } else if ((defaultItem.contains("No Rollover")) && (!selectedPlanName.endsWith("NQ"))) {
				                        WebElement elePaymentAtSettle = driver.findElement(By.xpath("//input[@id='outerForm:paymentAtSettle']"));
				                        elePaymentAtSettle.clear();
				                        elePaymentAtSettle.sendKeys(var_CashWithApplication);
				                    }



				                   if ((selectedPlanName.endsWith("NQ")) && (!defaultItem.contains("No Rollover"))){

		                               var_preBasisContribution = var_preBasisContribution.replace(",","");
				                       Double doubleprebasis = Double.parseDouble(var_preBasisContribution);

		                               var_preGainContribution = var_preGainContribution.replace(",","");
		                               Double doublepregain = Double.parseDouble(var_preGainContribution);

		                               var_postBasisContribution = var_postBasisContribution.replace(",","");
		                               Double doublepostbasis = Double.parseDouble(var_postBasisContribution);

		                               var_postGainContribution = var_postGainContribution.replace(",","");
		                               Double doublepostgain = Double.parseDouble(var_postGainContribution);

		                               Double totalValue = doubleprebasis + doublepregain + doublepostbasis + doublepostgain;

		                               var_RolloverPremium = var_RolloverPremium.replace(",","");
		                               Double doubleRollOverPremium = Double.parseDouble(var_RolloverPremium);

		                               Integer compareResult = totalValue.compareTo(doubleRollOverPremium);
		                               if (compareResult != 0){
		                                   Assert.assertTrue(false,"Rollover premium should be sum of Basis and Gain Contributions");
		                               }

		                               WebElement elePaymentAtSettle = driver.findElement(By.xpath("//input[@id='outerForm:paymentAtSettle']"));
		                               elePaymentAtSettle.clear();
		                               elePaymentAtSettle.sendKeys("0");

		                               WebElement eleRolloverPremium = driver.findElement(By.xpath("//input[@id='outerForm:rolloverPremium']"));
		                               eleRolloverPremium.clear();
		                               eleRolloverPremium.sendKeys(var_RolloverPremium);

				                        WebElement elepreBasisAmount = driver.findElement(By.xpath("//input[@type='text'][@name='outerForm:rolloverPreTefraBasisContribution']"));
						                        elepreBasisAmount.clear();
						                        elepreBasisAmount.sendKeys(var_preBasisContribution);

				                               WebElement elepreGainAmount = driver.findElement(By.xpath("//input[@type='text'][@name='outerForm:rolloverPreTefraGainContribution']"));
				                               elepreGainAmount.clear();
				                               elepreGainAmount.sendKeys(var_preGainContribution);

				                               WebElement elepostBasisAmount = driver.findElement(By.xpath("//input[@type='text'][@name='outerForm:rolloverPostTefraBasisContribution']"));
				                               elepostBasisAmount.clear();
				                               elepostBasisAmount.sendKeys(var_postBasisContribution);

				                               WebElement elepostGainAmount = driver.findElement(By.xpath("//input[@type='text'][@name='outerForm:rolloverPostTefraGainContribution']"));
				                               elepostGainAmount.clear();
				                               elepostGainAmount.sendKeys(var_postGainContribution);


		                            } else if ((selectedPlanName.endsWith("NQ")) && (defaultItem.contains("No Rollover"))){
		                               WebElement elePaymentAtSettle = driver.findElement(By.xpath("//input[@id='outerForm:paymentAtSettle']"));
		                               elePaymentAtSettle.clear();
		                               elePaymentAtSettle.sendKeys(var_CashWithApplication);
		                           }

				                    if (defaultItem.contains("Direct Transfer") || defaultItem.contains("External Rollover")) {
				                        driver.findElement(By.xpath("//span[@id='outerForm:PolicyExchangeText']")).click();
				                        // Add Screenshot will only work for Android platform

				                        takeScreenshot();

				                        driver.findElement(By.xpath("//img[@id='outerForm:gnAddImage']")).click();
				                        // Add Screenshot will only work for Android platform

				                        takeScreenshot();

				                        org.openqa.selenium.support.ui.Select select1 = new org.openqa.selenium.support.ui.Select(driver.findElement(By.xpath("//select[@id='outerForm:status']")));
				                        select1.selectByVisibleText(var_PolicyStatus);
				                        // Add Screenshot will only work for Android platform

				                        takeScreenshot();

				                        Thread.sleep(500);
				                        org.openqa.selenium.support.ui.Select select2 = new org.openqa.selenium.support.ui.Select(driver.findElement(By.xpath("//select[@id='outerForm:exchangeCode']")));
				                        select2.selectByVisibleText(var_ExchangeType);
				                        // Add Screenshot will only work for Android platform

				                        takeScreenshot();

				                        Thread.sleep(500);
				                        org.openqa.selenium.support.ui.Select select3 = new org.openqa.selenium.support.ui.Select(driver.findElement(By.xpath("//select[@id='outerForm:replacementType']")));
				                        select3.selectByVisibleText(var_ReplacementType);
				                        // Add Screenshot will only work for Android platform

				                        takeScreenshot();

				                        Thread.sleep(500);
				                        driver.findElement(By.xpath("//input[@id='outerForm:estimateExchangeAmt']")).clear();
				                        driver.findElement(By.xpath("//input[@id='outerForm:estimateExchangeAmt']")).sendKeys(var_ModalPremium);
				                        // Add Screenshot will only work for Android platform

				                        takeScreenshot();

				                        Thread.sleep(500);

				                        driver.findElement(By.xpath("//input[@id='outerForm:cashReceivedFromExch']")).clear();
				                        driver.findElement(By.xpath("//input[@id='outerForm:cashReceivedFromExch']")).sendKeys(var_ModalPremium);
				                        driver.findElement(By.xpath("//input[@id='outerForm:cashReceivedDate_input']")).sendKeys(var_EffectiveDate);
				                        Thread.sleep(500);
				                        driver.findElement(By.xpath("//input[@id='outerForm:priorCompanyName']")).sendKeys(var_PriorCompanyName);
				                        Thread.sleep(500);
				                        org.openqa.selenium.support.ui.Select select4 = new org.openqa.selenium.support.ui.Select(driver.findElement(By.xpath("//select[@id='outerForm:priorInvestmentType']")));
				                        select4.selectByVisibleText(var_PriorInvestmentType);
				                        // Add Screenshot will only work for Android platform

				                        takeScreenshot();

				                        Thread.sleep(500);
				                        org.openqa.selenium.support.ui.Select select5 = new org.openqa.selenium.support.ui.Select(driver.findElement(By.xpath("//select[@id='outerForm:currencyCode']")));
				                        select5.selectByVisibleText("DOLLAR [US]");
				                        Thread.sleep(500);
				                        driver.findElement(By.xpath("//input[@type='submit'][@name='outerForm:Submit']")).click();
				                        // Add Screenshot will only work for Android platform

				                        takeScreenshot();

				                        Thread.sleep(500);
				                        driver.findElement(By.xpath("//input[@type='submit'][@name='outerForm:Cancel']")).click();
				                        Thread.sleep(500);
				                        driver.findElement(By.xpath("//input[@type='submit'][@name='outerForm:Cancel']")).click();
				                        Thread.sleep(500);

				                        driver.findElement(By.xpath("//input[@type='submit'][@name='outerForm:Submit']")).click();
				                        // Add Screenshot will only work for Android platform

				                        takeScreenshot();

				                        Thread.sleep(500);

				                        WebElement eleConfirmSubmit = driver.findElement(By.xpath("//input[@type='submit'][@name='outerForm:ConfirmSubmit']"));
				                        if (eleConfirmSubmit.isDisplayed()){
				                            eleConfirmSubmit.click();
				                            takeScreenshot();
				                            Thread.sleep(500);
				                        }

				                        Assert.assertTrue(driver.findElement(By.xpath("//li[@class='MessageInfo']")).getText().equals("Request completed successfully."));
				                        // Add Screenshot will only work for Android platform

				                        takeScreenshot();

				                        Thread.sleep(500);

				                        driver.findElement(By.xpath("//input[@type='submit'][@name='outerForm:Cancel']")).click();
				                        Thread.sleep(500);
				                        // Add Screenshot will only work for Android platform

				                        takeScreenshot();

				                    } else if (defaultItem.contains("No Rollover")){
				                        driver.findElement(By.xpath("//input[@type='submit'][@name='outerForm:Submit']")).click();
				                        // Add Screenshot will only work for Android platform

				                        takeScreenshot();

				                        Assert.assertTrue(driver.findElement(By.xpath("//li[@class='MessageInfo']")).getText().equals("Request completed successfully."));
				                        // Add Screenshot will only work for Android platform

				                        takeScreenshot();

				                    } else {

				                        driver.findElement(By.xpath("//input[@type='submit'][@name='outerForm:Submit']")).click();
				                        // Add Screenshot will only work for Android platform

				                        takeScreenshot();

				                        WebElement eleConfirmSubmit = driver.findElement(By.xpath("//input[@type='submit'][@name='outerForm:ConfirmSubmit']"));
				                        if (eleConfirmSubmit.isDisplayed()){
				                            eleConfirmSubmit.click();
				                            takeScreenshot();
				                            Thread.sleep(500);
				                        }

				                        Assert.assertTrue(driver.findElement(By.xpath("//li[@class='MessageInfo']")).getText().equals("Request completed successfully."));
				                        // Add Screenshot will only work for Android platform

				                        takeScreenshot();

				                    }

				                    ;
		 
		 ;} 
		 catch(Exception e) { PrintVal("Renderer Script Exception : "+e.toString()); } 
		 PrintVal("Executed Step = CALLRENDERERSCRIPT,VALIDATEPOLICYEXCHANGEUAT4"); 
		takeFullScreenShot();
                 LOGGER.info("Executed Step = TAKEFULLSCREENSHOT,TGTYPESCREENREG");
        }
          LOGGER.info("Executed Step = ENDIF,TGTYPESCREENREG");
        CALL.$("WritePolicyQuickInquiryToCSV","TGTYPESCREENREG");

        HOVER.$("ELE_MENU_INQUIRY","//td[text()='Inquiry']","TGTYPESCREENREG");

        TAP.$("ELE_SUBMEN_QUICKINQUIRY","//td[contains(text(),'Quick Inquiry')]","TGTYPESCREENREG");

        TYPE.$("ELE_TXTBOX_POLICYNUMBER","//input[@type='text'][@name='outerForm:policyNumber']",var_PolicyNumber,"FALSE","TGTYPESCREENREG");

        TAP.$("ELE_BUTTON_CONTINUE","//input[@type='submit'][@name='outerForm:Continue']","TGTYPESCREENREG");

		writeToCSV ("Policy Number " , ActionWrapper.getElementValue("ELE_GETVAL_POLICYNUMBER", "//span[@id='outerForm:policyNumber']"));
                 LOGGER.info("Executed Step = WRITETOCSV,ELE_GETVAL_POLICYNUMBER,Policy Number,TGTYPESCREENREG");
		writeToCSV ("Status " , ActionWrapper.getElementValue("ELE_GETVAL_STATUS", "//span[@id='outerForm:statusText']"));
                 LOGGER.info("Executed Step = WRITETOCSV,ELE_GETVAL_STATUS,Status,TGTYPESCREENREG");
		writeToCSV ("Effective Date " , ActionWrapper.getElementValue("ELE_GETVAL_EFFECTIVEDATETEXTFROMGRID", "//span[@id='outerForm:grid:0:effectiveDateOut2']"));
                 LOGGER.info("Executed Step = WRITETOCSV,ELE_GETVAL_EFFECTIVEDATETEXTFROMGRID,Effective Date,TGTYPESCREENREG");
		writeToCSV ("State country " , ActionWrapper.getElementValue("ELE_GETVAL_STATECOUNTRY", "//span[@id='outerForm:ownerCityStateZip']"));
                 LOGGER.info("Executed Step = WRITETOCSV,ELE_GETVAL_STATECOUNTRY,State country,TGTYPESCREENREG");
		writeToCSV ("Agent Number " , ActionWrapper.getElementValue("ELE_GETVAL_AGENTNUMBER", "//span[@id='outerForm:agentNumber']"));
                 LOGGER.info("Executed Step = WRITETOCSV,ELE_GETVAL_AGENTNUMBER,Agent Number,TGTYPESCREENREG");
		var_Count = var_Count + ( 1 );
                 LOGGER.info("Executed Step = INCREASEVAR,var_Count,1,TGTYPESCREENREG");
    	}
          LOGGER.info("Executed Step = WEND,TGTYPESCREENREG");
    	resetAttemps();

    }


}
