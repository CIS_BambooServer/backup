package appium.tgo;

import com.tg.octopus.base.TGOWrapper;
import com.tg.octopus.libs.testng.listener.TestListener;
import static com.tg.octopus.base.TGOWrapper.Action.*;
import static com.tg.octopus.base.TGOWrapper.LOGGER;
import com.tg.octopus.wrapper.ActionWrapper;

import com.jayway.jsonpath.JsonPath;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.SessionNotCreatedException;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.Augmenter;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.remote.UnreachableBrowserException;

import org.testng.Assert;
import org.testng.ITestContext;
import org.testng.ITestResult;
import org.testng.Reporter;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;
import java.net.HttpURLConnection;
import java.net.URL;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import org.json.JSONArray;
import org.json.JSONObject;
import helper.WebTestUtility;
import java.sql.*;
import com.testautomationguru.utility.PDFUtil;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.multipdf.Splitter;

import java.util.*;
import org.openqa.selenium.*;
import java.io.*;
import java.net.*;

import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;


@Listeners(TestListener.class)
public class CLIC_Master_Regression_Prod_V.1.1.1 extends TGOWrapper {



    static {
       needsresetoncebeforetest ="false";
       needsresetbeforeeverymethod ="true";
       captureverystepsnap ="true";
       appendlastsnap ="false";
       ishybridapp ="true";

    }


    @Test
    public void clicregcompareclclbilltiffimages() {

        // No need to declare / print method name here, handled by TestNg listener
        // declare("clicregcompareclclbilltiffimages");

		int var_Count = 0;
                 LOGGER.info("Executed Step = VAR,Integer,var_Count,0,TGTYPESCREENREG");
		com.jayway.jsonpath.ReadContext var_CSVData = getJsonData("C:/GIAS_Automation/CLIC Regression/QUA1/InputData/CLIC36_IP_CompareImages.csv");
                 LOGGER.info("Executed Step = VAR,Json,var_CSVData,C:/GIAS_Automation/CLIC Regression/QUA1/InputData/CLIC36_IP_CompareImages.csv,TGTYPESCREENREG");
		String var_Date;
                 LOGGER.info("Executed Step = VAR,String,var_Date,TGTYPESCREENREG");
		var_Date = getJsonData(var_CSVData , "$.records["+var_Count+"].Date");
                 LOGGER.info("Executed Step = STORE,var_Date,var_CSVData,$.records[{var_Count}].Date,TGTYPESCREENREG");
		try { 
		 				    	
				    	//  COMPANY OF Texas (CL CL) PREMIUM NOTICE 
				    	
					 Boolean policyFound =false; 
			  //       String var_Date ="10/06/2020";
			         String PolicyNumber ="";
			         String PolicyNumberfileName ="";
			         String day="";
			        
			    	 String array[] = var_Date.split("/");
					 System.out.println("1st"+array[0].length());
					 if(array[0].length()==1) {
						 array[0]="0"+array[0];
						 System.out.println("after adding"+array[0]);
					 }
					 if(array[1].length()==1) {
						 array[1]="0"+array[1];
						 System.out.println("after adding"+array[1]);
					 }
					 int dayint =Integer.parseInt(array[1]);
					 day = array[1];
					 if(dayint<31) {
						 dayint =dayint +1;
						 day=String.valueOf(dayint);
						 if(day.length()==1) {
							 day ="0"+day;
						 }
					 }
							 
					 String var_DateforDB = array[0]+"/"+array[1]+"/"+array[2];
					  
					 var_Date = array[2]+"-"+array[0]+"-"+day;
					 System.out.println("a: "+var_Date);
						
							
							// BaseLined Image for CompanyTaxes (PremNotice)
							
							String BaseImageforPremNotice ="C:\\/GIAS_Automation\\/Images\\/CLIC_CCLINDBILL_PREMIUM (Base Lined).TIF";
							String GeneratedPath ="\\\\gvlfp04\\printreview\\CLIC_CCLINDBILLP_TIFs_PROD";
							
							// Create  folder Name by Appending Date  
							
							String FolderPrefix ="CLIC_CCLINDBILLP_"+var_Date;
							
							GeneratedPath = GeneratedPath+"\\"+FolderPrefix;
							
							
							System.out.println("GenereatedPath ::"+GeneratedPath);
							
							
							java.sql.Connection con = null;   

					        // Load SQL Server JDBC driver and establish connection UAT.
					        String connectionUrl = "jdbc:sqlserver://CIS-GIASDBP2:443" + "3;" +
					                "databaseName=CLIP1DT" + "A;"
					                +
					                "IntegratedSecurity = true";
					        System.out.print("Connecting to SQL Server ... ");
					        try {
								con = DriverManager.getConnection(connectionUrl);
							} catch (SQLException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
				            System.out.println("Connected to database.");
				            
				            String sql1primCL ="Use clis1dta "+
				            		  "Select c.field1, c.company,c.insuranceType, p.noticeType, p.company, p.controlNumber, p.noticeNumber, p.serialNumber, p.insuredName, p.policynumber,"+
				            		 "p.policyNumber, p.amount, p.dueDate, a.accountName, a.accountNameAddress2, a.accountNameAddress4 "+
				            		"from iios.BillingCombinedPol p join iios.BillingCombinedAddr a on p.company = a.company and p.controlNumber = a.controlNumber "+
				            		"and p.noticeNumber = a.noticeNumber "+
				            		"join iios.BillingCombined c on p.company = c.company and p.controlNumber = c.controlNumber and p.noticeNumber = c.noticeNumber "+
				            		"where "+
				            		"field1 = '"+var_DateforDB+"' "+
				            		"and c.company= 'CLCL' "+
				            		"and p.noticeType ='1' "+
				            		"and insuranceType ='H'"+
				            		"order by controlNumber, field1,p.noticeNumber, p.serialNumber";
				            			
				            System.out.println(sql1primCL);
				            Statement statement;
							try {
								statement = con.createStatement();
							
							ResultSet rs = null;
							statement.setMaxRows(1);
							System.out.println("EXECUTED");
							rs = statement.executeQuery(sql1primCL);
							 while (rs.next()) {
								 
								  PolicyNumber =rs.getString("policyNumber");
								 
								  System.out.println("PolicyNumber::"+PolicyNumber);
								  
								  if(PolicyNumber.equalsIgnoreCase("Null")) {
									    policyFound = false ;
								  }
								  else {
									  policyFound = true;
								  }
					            }
							
							 statement.close();
							}
							 catch (SQLException e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
								}
							
							if(policyFound =true) {
							 
							// List of file names in the folder 
								
							// List of file names in the folder 
								
								File folder = new File(GeneratedPath);
								File[] listOfFiles = folder.listFiles();
								
								System.out.println("Lenght ::"+listOfFiles.length);

								for (int i = 0; i < listOfFiles.length; i++) {
								 // if (listOfFiles[i].isFile()) {
								    System.out.println("File " + listOfFiles[i].getName());
								    System.out.println("POLICY ::"+PolicyNumber);
								    String imagname = listOfFiles[i].getName().trim();
								    System.out.println("Condition:"+imagname.toLowerCase().contains(PolicyNumber.toLowerCase()));
								    System.out.println("imagname"+imagname);
								    if(imagname.toLowerCase().contains(PolicyNumber.trim().toLowerCase())) { 	
								    	PolicyNumberfileName =listOfFiles[i].getName();
								    	GeneratedPath = GeneratedPath+"\\"+PolicyNumberfileName;
								    	i=listOfFiles.length+1;
								    }
								  } 
								 
							//	}
								
								
							System.out.println(" Full Path is ::"+GeneratedPath);
							
							GeneratedPath= GeneratedPath.replace("\\","\\\\");
							
							
							// ALL The API FOR  COMPARING IMAGE
							
							String url = "http://10.1.104.229/compare/index.php";
					    	CloseableHttpClient httpClient = HttpClients.createDefault();
							HttpPost httppost = new HttpPost(url); //http post request
							
							// passing json as a string 
							String entityString ="{\"image1\":\""+BaseImageforPremNotice+"\" ,\"image2\": \""+GeneratedPath+"\",\"coordinates\": \"15,218,779,703|188,35,282,76|652,740,775,790|126,812,253,844|96,907,336,980|68,998,768,1025\"}\r\n"
									+ "";
							System.out.println("EntityString::"+entityString);
							try {
								httppost.setEntity(new StringEntity(entityString));
								CloseableHttpResponse closebaleHttpResponse;
								closebaleHttpResponse = httpClient.execute(httppost);
								int statusCode = closebaleHttpResponse.getStatusLine().getStatusCode();
								System.out.println("Status from API:"+statusCode);
								// convert json to string 	
								String responseString = EntityUtils.toString(closebaleHttpResponse.getEntity(), "UTF-8");
								System.out.println("Response from API :"+responseString);
								
								JSONObject response = new JSONObject(responseString);
								System.out.println("Image : "+response.getString("image_difference_link"));
								System.out.println("Both the image are same "+response.getBoolean("both_are_same"));
								writeToCSV("Company","CLCL");
								writeToCSV("Image compared ", PolicyNumberfileName);
								writeToCSV("Image Result After comparision", response.getString("image_difference_link"));
								if(response.getBoolean("both_are_same")== true) {
									writeToCSV("Resut", "Both are same");
								}
								else {
									writeToCSV("Resut", "Both are not same");
								}
								
								
							} catch (ClientProtocolException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							} catch (IOException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
						
							
							}
							else {
								
								writeToCSV("Company","CLCL");
								writeToCSV("Image compared ", "NO CLCL POLICY FOR PREMIUM ");
								writeToCSV("Image Result After comparision", "NULL");
								
								
							}
							
							
							
		 
		 ;} 
		 catch(Exception e) { PrintVal("Renderer Script Exception : "+e.toString()); } 
		 PrintVal("Executed Step = CALLRENDERERSCRIPT,COMPAREIMAGECLCLPREMIUMNOTICE"); 
		try { 
		   	
				    	
				    	//  COMPANY OF Texas (CL CL) REMINDER NOTICE 
				    	
				    	 Boolean policyFound =false; 
				 //        String var_Date ="10/06/2020";
				         String PolicyNumber ="";
				         String PolicyNumberfileName ="";
				         String day="";
				        
				    	 String array[] = var_Date.split("/");
						 System.out.println("1st"+array[0].length());
						 if(array[0].length()==1) {
							 array[0]="0"+array[0];
							 System.out.println("after adding"+array[0]);
						 }
						 if(array[1].length()==1) {
							 array[1]="0"+array[1];
							 System.out.println("after adding"+array[1]);
						 }
						 int dayint =Integer.parseInt(array[1]);
						 day = array[1];
						 if(dayint<31) {
							 dayint =dayint +1;
							 day=String.valueOf(dayint); 
							 if(day.length()==1) {
								 day ="0"+day;
							 }
						 }
								 
						 String var_DateforDB = array[0]+"/"+array[1]+"/"+array[2];
						  
						 var_Date = array[2]+"-"+array[0]+"-"+day;
						 System.out.println("a: "+var_Date);
				    	
						
							
							// BaseLined Image for CompanyTaxes (REMINDER NOTICE)
							
							String BaseImageforPremNotice ="C:\\/GIAS_Automation\\/Images\\/CLIC_CCLINDBILL_REMINDER.TIF";
							String GeneratedPath ="\\\\gvlfp04\\printreview\\CLIC_CCLINDBILLP_TIFs_PROD";
							
							// Create  folder Name by Appending Date  
							
							String FolderPrefix ="CCLIC_CCLINDBILLP_"+var_Date;
							
							GeneratedPath = GeneratedPath+"\\"+FolderPrefix;
							
							
							System.out.println("GenereatedPath ::"+GeneratedPath);
							
							
							java.sql.Connection con = null;   

					        // Load SQL Server JDBC driver and establish connection UAT.
					        String connectionUrl = "jdbc:sqlserver://CIS-GIASDBP2:443" + "3;" +
					                "databaseName=CLIP1DT" + "A;"
					                +
					                "IntegratedSecurity = true";
					        System.out.print("Connecting to SQL Server ... ");
					        try {
								con = DriverManager.getConnection(connectionUrl);
							} catch (SQLException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
				            System.out.println("Connected to database.");
				            
				            String sql1primCL ="Use clis1dta "+
				            		  "Select c.field1, c.company,c.insuranceType, p.noticeType, p.company, p.controlNumber, p.noticeNumber, p.serialNumber, p.insuredName, p.policynumber,"+
				            		 "p.policyNumber, p.amount, p.dueDate, a.accountName, a.accountNameAddress2, a.accountNameAddress4 "+
				            		"from iios.BillingCombinedPol p join iios.BillingCombinedAddr a on p.company = a.company and p.controlNumber = a.controlNumber "+
				            		"and p.noticeNumber = a.noticeNumber "+
				            		"join iios.BillingCombined c on p.company = c.company and p.controlNumber = c.controlNumber and p.noticeNumber = c.noticeNumber "+
				            		"where "+
				            		"field1 = '"+var_DateforDB+"' "+
				            		"and c.company= 'CLCL' "+
				            		"and p.noticeType ='2' "+
				            		"and insuranceType ='H' "+
				            		"order by controlNumber, field1,p.noticeNumber, p.serialNumber";
				            			
				            System.out.println(sql1primCL);
				            Statement statement;
							try {
								statement = con.createStatement();
							
							ResultSet rs = null;
							statement.setMaxRows(1);
							System.out.println("EXECUTED");
							rs = statement.executeQuery(sql1primCL);
							 while (rs.next()) {
								 
								  PolicyNumber =rs.getString("policyNumber");
								 
								  System.out.println("PolicyNumber::"+PolicyNumber);
								  
								  if(PolicyNumber.equalsIgnoreCase("Null")) {
									    policyFound = false ;
								  }
								  else {
									  policyFound = true;
								  }
					            }
							
							 statement.close();
							}
							 catch (SQLException e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
								}
							
							
							System.out.println("POLICY FOUND"+policyFound);
							if(policyFound==true) {
							 
							// List of file names in the folder 
								
								File folder = new File(GeneratedPath);
								File[] listOfFiles = folder.listFiles();
								
								System.out.println("Lenght ::"+listOfFiles.length);

								for (int i = 0; i < listOfFiles.length; i++) {
								 // if (listOfFiles[i].isFile()) {
								    System.out.println("File " + listOfFiles[i].getName());
								    System.out.println("POLICY ::"+PolicyNumber);
								    String imagname = listOfFiles[i].getName().trim();
								    System.out.println("Condition:"+imagname.toLowerCase().contains(PolicyNumber.toLowerCase()));
								    System.out.println("imagname"+imagname);
								    if(imagname.toLowerCase().contains(PolicyNumber.trim().toLowerCase())) { 	
								    	PolicyNumberfileName =listOfFiles[i].getName();
								    	GeneratedPath = GeneratedPath+"\\"+PolicyNumberfileName;
								    	i=listOfFiles.length+1;
								    }
								  } 
								 
							//	}
								
							System.out.println(" Full Path is ::"+GeneratedPath);
							
							GeneratedPath= GeneratedPath.replace("\\","\\\\");
							
							
							// ALL The API FOR  COMPARING IMAGE
							
							String url = "http://10.1.104.229/compare/index.php";
					    	CloseableHttpClient httpClient = HttpClients.createDefault();
							HttpPost httppost = new HttpPost(url); //http post request
							
							// passing json as a string 
							String entityString ="{\"image1\":\""+BaseImageforPremNotice+"\" ,\"image2\": \""+GeneratedPath+"\",\"coordinates\": \"163,38,304,83|19,218,784,697|648,742,783,809|126,817,233,842|67,904,346,986|68,995,774,1031\"}\r\n"
									+ "";
							System.out.println("EntityString::"+entityString);
							try {
								httppost.setEntity(new StringEntity(entityString));
								CloseableHttpResponse closebaleHttpResponse;
								closebaleHttpResponse = httpClient.execute(httppost);
								int statusCode = closebaleHttpResponse.getStatusLine().getStatusCode();
								System.out.println("Status from API:"+statusCode);
								// convert json to string 	
								String responseString = EntityUtils.toString(closebaleHttpResponse.getEntity(), "UTF-8");
								System.out.println("Response from API :"+responseString);
								
								JSONObject response = new JSONObject(responseString);
								System.out.println("Image : "+response.getString("image_difference_link"));
								System.out.println("Both the image are same "+response.getBoolean("both_are_same"));
								writeToCSV("Company","CLCL");
								writeToCSV("Image compared ", PolicyNumberfileName);
								writeToCSV("Image Result After comparision", response.getString("image_difference_link"));
								if(response.getBoolean("both_are_same")== true) {
									writeToCSV("Resut", "Both are same");
								}
								else {
									writeToCSV("Resut", "Both are not same");
								}
								
								
							} catch (ClientProtocolException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							} catch (IOException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
						
							
							}
							else {
								
								writeToCSV("Company","CLCL");
								writeToCSV("Image compared ", "NO CLCL POLICY FOR REMINDER NOTICE ");
								writeToCSV("Image Result After comparision", "NULL");
								
								
							}
							
							
							
						
		 
		 ;} 
		 catch(Exception e) { PrintVal("Renderer Script Exception : "+e.toString()); } 
		 PrintVal("Executed Step = CALLRENDERERSCRIPT,COMPAREIMAGECLCLREMINDERNOTICE"); 
		try { 
		 		    	
				    	//  COMPANY OF Texas (CL CL) LAPSE NOTICE  FOR LIFE
				    	
					 Boolean policyFound =false; 
			   //      String var_Date ="10/06/2020";
			         String PolicyNumber ="";
			         String PolicyNumberfileName ="";
			         String day="";
			        
			    	 String array[] = var_Date.split("/");
					 System.out.println("1st"+array[0].length());
					 if(array[0].length()==1) {
						 array[0]="0"+array[0];
						 System.out.println("after adding"+array[0]);
					 }
					 if(array[1].length()==1) {
						 array[1]="0"+array[1];
						 System.out.println("after adding"+array[1]);
					 }
					 int dayint =Integer.parseInt(array[1]);
					 day = array[1];
					 if(dayint<31) {
						 dayint =dayint +1;
						 day=String.valueOf(dayint);
						 if(day.length()==1) {
							 day ="0"+day;
						 }
					 }
							 
					 String var_DateforDB = array[0]+"/"+array[1]+"/"+array[2];
					  
					 var_Date = array[2]+"-"+array[0]+"-"+day;
					 System.out.println("a: "+var_Date);
						
							
							// BaseLined Image for CompanyTaxes (LapseNOTICE LIFE)
							
							String BaseImageforPremNotice ="C:\\/GIAS_Automation\\/Images\\/CLCL\\/LIFE_LAPSENOTICE.TIF";
							String GeneratedPath ="\\\\gvlfp04\\printreview\\CLIC_CCLINDBILLP_TIFs_PROD";
							
							// Create  folder Name by Appending Date  
							
							String FolderPrefix ="CLIC_CCLINDBILLP_"+var_Date;
							
							GeneratedPath = GeneratedPath+"\\"+FolderPrefix;
							
							
							System.out.println("GenereatedPath ::"+GeneratedPath);
							
							
							java.sql.Connection con = null;   

					        // Load SQL Server JDBC driver and establish connection UAT.
					        String connectionUrl = "jdbc:sqlserver://CIS-GIASDBP2:443" + "3;" +
					                "databaseName=CLIP1DT" + "A;"
					                +
					                "IntegratedSecurity = true";
					        System.out.print("Connecting to SQL Server ... ");
					        try {
								con = DriverManager.getConnection(connectionUrl);
							} catch (SQLException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
				            System.out.println("Connected to database.");
				            
				            String sql1primCL ="Use clis1dta "+
				            		  "Select c.field1, c.company,c.insuranceType, p.noticeType, p.company, p.controlNumber, p.noticeNumber, p.serialNumber, p.insuredName, p.policynumber,"+
				            		 "p.policyNumber, p.amount, p.dueDate, a.accountName, a.accountNameAddress2, a.accountNameAddress4 "+
				            		"from iios.BillingCombinedPol p join iios.BillingCombinedAddr a on p.company = a.company and p.controlNumber = a.controlNumber "+
				            		"and p.noticeNumber = a.noticeNumber "+
				            		"join iios.BillingCombined c on p.company = c.company and p.controlNumber = c.controlNumber and p.noticeNumber = c.noticeNumber "+
				            		"where "+
				            		"field1 = '"+var_DateforDB+"' "+
				            		"and c.company= 'CLCL' "+
				            		"and p.noticeType ='3' "+
				            		"and insuranceType ='L'"+
				            		"order by controlNumber, field1,p.noticeNumber, p.serialNumber";
				            			
				            System.out.println(sql1primCL);
				            Statement statement;
							try {
								statement = con.createStatement();
							
							ResultSet rs = null;
							statement.setMaxRows(1);
							System.out.println("EXECUTED");
							rs = statement.executeQuery(sql1primCL);
							 while (rs.next()) {
								 
								  PolicyNumber =rs.getString("policyNumber");
								 
								  System.out.println("PolicyNumber::"+PolicyNumber);
								  
								  if(PolicyNumber.equalsIgnoreCase("Null")) {
									    policyFound = false ;
								  }
								  else {
									  policyFound = true;
								  }
					            }
							
							 statement.close();
							}
							 catch (SQLException e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
								}
							
							if(policyFound =true) {
							 
							// List of file names in the folder 
								
							// List of file names in the folder 
								
								File folder = new File(GeneratedPath);
								File[] listOfFiles = folder.listFiles();
								
								System.out.println("Lenght ::"+listOfFiles.length);

								for (int i = 0; i < listOfFiles.length; i++) {
								 // if (listOfFiles[i].isFile()) {
								    System.out.println("File " + listOfFiles[i].getName());
								    System.out.println("POLICY ::"+PolicyNumber);
								    String imagname = listOfFiles[i].getName().trim();
								    System.out.println("Condition:"+imagname.toLowerCase().contains(PolicyNumber.toLowerCase()));
								    System.out.println("imagname"+imagname);
								    if(imagname.toLowerCase().contains(PolicyNumber.trim().toLowerCase())) { 	
								    	PolicyNumberfileName =listOfFiles[i].getName();
								    	GeneratedPath = GeneratedPath+"\\"+PolicyNumberfileName;
								    	i=listOfFiles.length+1;
								    }
								  } 
								 
							//	}
								
								
							System.out.println(" Full Path is ::"+GeneratedPath);
							
							GeneratedPath= GeneratedPath.replace("\\","\\\\");
							
							
							// ALL The API FOR  COMPARING IMAGE
							
							String url = "http://10.1.104.229/compare/index.php";
					    	CloseableHttpClient httpClient = HttpClients.createDefault();
							HttpPost httppost = new HttpPost(url); //http post request
							
							// passing json as a string 
							String entityString ="{\"image1\":\""+BaseImageforPremNotice+"\" ,\"image2\": \""+GeneratedPath+"\",\"coordinates\": \"187,38,286,109|618,115,755,155|13,221,802,640|648,742,783,809|126,817,233,842|67,904,346,986|68,995,774,1031\"}\r\n"
									+ "";
							System.out.println("EntityString::"+entityString);
							try {
								httppost.setEntity(new StringEntity(entityString));
								CloseableHttpResponse closebaleHttpResponse;
								closebaleHttpResponse = httpClient.execute(httppost);
								int statusCode = closebaleHttpResponse.getStatusLine().getStatusCode();
								System.out.println("Status from API:"+statusCode);
								// convert json to string 	
								String responseString = EntityUtils.toString(closebaleHttpResponse.getEntity(), "UTF-8");
								System.out.println("Response from API :"+responseString);
								
								JSONObject response = new JSONObject(responseString);
								System.out.println("Image : "+response.getString("image_difference_link"));
								System.out.println("Both the image are same "+response.getBoolean("both_are_same"));
								writeToCSV("Company","CLCL LIFE LAPSE NOTICE");
								writeToCSV("Image compared ", PolicyNumberfileName);
								writeToCSV("Image Result After comparision", response.getString("image_difference_link"));
								if(response.getBoolean("both_are_same")== true) {
									writeToCSV("Resut", "Both are same");
								}
								else {
									writeToCSV("Resut", "Both are not same");
								}
								
								
							} catch (ClientProtocolException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							} catch (IOException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
						
							
							}
							else {
								
								writeToCSV("Company","CLCL LIFE LAPSENOTICE");
								writeToCSV("Image compared ", "NO CLCL POLICY FOR LAPSENOTICE ");
								writeToCSV("Image Result After comparision", "NULL");
								
								
							}
							
		 
		 ;} 
		 catch(Exception e) { PrintVal("Renderer Script Exception : "+e.toString()); } 
		 PrintVal("Executed Step = CALLRENDERERSCRIPT,COMPAREIMAGECLCLLIFELAPSENOTICE"); 
		try { 
		    	
				    	//  COMPANY OF Texas (CL CL) LOAN INTREST NOTICE  FOR LIFE
				    	
					 Boolean policyFound =false; 
			    //     String var_Date ="10/06/2020";
			         String PolicyNumber ="";
			         String PolicyNumberfileName ="";
			         String day="";
			        
			    	 String array[] = var_Date.split("/");
					 System.out.println("1st"+array[0].length());
					 if(array[0].length()==1) {
						 array[0]="0"+array[0];
						 System.out.println("after adding"+array[0]);
					 }
					 if(array[1].length()==1) {
						 array[1]="0"+array[1];
						 System.out.println("after adding"+array[1]);
					 }
					 int dayint =Integer.parseInt(array[1]);
					 day = array[1];
					 if(dayint<31) {
						 dayint =dayint +1;
						 day=String.valueOf(dayint);
						 if(day.length()==1) {
							 day ="0"+day;
						 }
					 }
							 
					 String var_DateforDB = array[0]+"/"+array[1]+"/"+array[2];
					  
					 var_Date = array[2]+"-"+array[0]+"-"+day;
					 System.out.println("a: "+var_Date);
						
							
							// BaseLined Image for CompanyTaxes (LapseNOTICE LIFE)
							
							String BaseImageforPremNotice ="C:\\/GIAS_Automation\\/Images\\/CLCL\\/LIFE_LOANINTEREST.TIF";
							String GeneratedPath ="\\\\gvlfp04\\printreview\\CLIC_CCLINDBILLM_TIFs_UAT1";
							
							// Create  folder Name by Appending Date  
							
							String FolderPrefix ="CLIC_CCLINDBILL_UAT1_"+var_Date;
							
							GeneratedPath = GeneratedPath+"\\"+FolderPrefix;
							
							
							System.out.println("GenereatedPath ::"+GeneratedPath);
							
							
							java.sql.Connection con = null;   

					        // Load SQL Server JDBC driver and establish connection UAT.
					        String connectionUrl = "jdbc:sqlserver://CIS-GIASDBT2:443" + "3;" +
					                "databaseName=CLIS1DT" + "A;"
					                +
					                "IntegratedSecurity = true";
					        System.out.print("Connecting to SQL Server ... ");
					        try {
								con = DriverManager.getConnection(connectionUrl);
							} catch (SQLException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
				            System.out.println("Connected to database.");
				            
				            String sql1primCL ="Use clis1dta "+
				            		  "Select c.field1, c.company,c.insuranceType, p.noticeType, p.company, p.controlNumber, p.noticeNumber, p.serialNumber, p.insuredName, p.policynumber,"+
				            		 "p.policyNumber, p.amount, p.dueDate, a.accountName, a.accountNameAddress2, a.accountNameAddress4 "+
				            		"from iios.BillingCombinedPol p join iios.BillingCombinedAddr a on p.company = a.company and p.controlNumber = a.controlNumber "+
				            		"and p.noticeNumber = a.noticeNumber "+
				            		"join iios.BillingCombined c on p.company = c.company and p.controlNumber = c.controlNumber and p.noticeNumber = c.noticeNumber "+
				            		"where "+
				            		"field1 = '"+var_DateforDB+"' "+
				            		"and c.company= 'CLCL' "+
				            		"and p.noticeType ='L' "+
				            		"and insuranceType ='L'"+
				            		"order by controlNumber, field1,p.noticeNumber, p.serialNumber";
				            			
				            System.out.println(sql1primCL);
				            Statement statement;
							try {
								statement = con.createStatement();
							
							ResultSet rs = null;
							statement.setMaxRows(1);
							System.out.println("EXECUTED");
							rs = statement.executeQuery(sql1primCL);
							 while (rs.next()) {
								 
								  PolicyNumber =rs.getString("policyNumber");
								 
								  System.out.println("PolicyNumber::"+PolicyNumber);
								  
								  if(PolicyNumber.equalsIgnoreCase("Null")) {
									    policyFound = false ;
								  }
								  else {
									  policyFound = true;
								  }
					            }
							
							 statement.close();
							}
							 catch (SQLException e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
								}
							
							if(policyFound =true) {
							 
							// List of file names in the folder 
								
							// List of file names in the folder 
								
								File folder = new File(GeneratedPath);
								File[] listOfFiles = folder.listFiles();
								
								System.out.println("Lenght ::"+listOfFiles.length);

								for (int i = 0; i < listOfFiles.length; i++) {
								 // if (listOfFiles[i].isFile()) {
								    System.out.println("File " + listOfFiles[i].getName());
								    System.out.println("POLICY ::"+PolicyNumber);
								    String imagname = listOfFiles[i].getName().trim();
								    System.out.println("Condition:"+imagname.toLowerCase().contains(PolicyNumber.toLowerCase()));
								    System.out.println("imagname"+imagname);
								    if(imagname.toLowerCase().contains(PolicyNumber.trim().toLowerCase())) { 	
								    	PolicyNumberfileName =listOfFiles[i].getName();
								    	GeneratedPath = GeneratedPath+"\\"+PolicyNumberfileName;
								    	i=listOfFiles.length+1;
								    }
								  } 
								 
							//	}
								
								
							System.out.println(" Full Path is ::"+GeneratedPath);
							
							GeneratedPath= GeneratedPath.replace("\\","\\\\");
							
							
							// ALL The API FOR  COMPARING IMAGE
							
							String url = "http://10.1.104.229/compare/index.php";
					    	CloseableHttpClient httpClient = HttpClients.createDefault();
							HttpPost httppost = new HttpPost(url); //http post request
							
							// passing json as a string 
							String entityString ="{\"image1\":\""+BaseImageforPremNotice+"\" ,\"image2\": \""+GeneratedPath+"\",\"coordinates\": \"187,38,286,109|618,115,755,155|13,221,802,640|648,742,783,809|126,817,233,842|67,904,346,986|68,995,774,1031\"}\r\n"
									+ "";
							System.out.println("EntityString::"+entityString);
							try {
								httppost.setEntity(new StringEntity(entityString));
								CloseableHttpResponse closebaleHttpResponse;
								closebaleHttpResponse = httpClient.execute(httppost);
								int statusCode = closebaleHttpResponse.getStatusLine().getStatusCode();
								System.out.println("Status from API:"+statusCode);
								// convert json to string 	
								String responseString = EntityUtils.toString(closebaleHttpResponse.getEntity(), "UTF-8");
								System.out.println("Response from API :"+responseString);
								
								JSONObject response = new JSONObject(responseString);
								System.out.println("Image : "+response.getString("image_difference_link"));
								System.out.println("Both the image are same "+response.getBoolean("both_are_same"));
								writeToCSV("Company","CLCL LIFE LAPSE NOTICE");
								writeToCSV("Image compared ", PolicyNumberfileName);
								writeToCSV("Image Result After comparision", response.getString("image_difference_link"));
								if(response.getBoolean("both_are_same")== true) {
									writeToCSV("Resut", "Both are same");
								}
								else {
									writeToCSV("Resut", "Both are not same");
								}
								
								
							} catch (ClientProtocolException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							} catch (IOException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
						
							
							}
							else {
								
								writeToCSV("Company","CLCL LIFE LAPSENOTICE");
								writeToCSV("Image compared ", "NO CLCL POLICY FOR LAPSENOTICE ");
								writeToCSV("Image Result After comparision", "NULL");
								
								
							}
							
							
							
		 
		 ;} 
		 catch(Exception e) { PrintVal("Renderer Script Exception : "+e.toString()); } 
		 PrintVal("Executed Step = CALLRENDERERSCRIPT,COMPAREIMAGECLCLLIFELOANINTRESTNOTICE"); 

    }


    @Test
    public void clicregcompareimageclnw() {

        // No need to declare / print method name here, handled by TestNg listener
        // declare("clicregcompareimageclnw");

		int var_Count = 0;
                 LOGGER.info("Executed Step = VAR,Integer,var_Count,0,TGTYPESCREENREG");
		com.jayway.jsonpath.ReadContext var_CSVData = getJsonData("C:/GIAS_Automation/CLIC Regression/QUA1/InputData/CLIC36_IP_CompareImages.csv");
                 LOGGER.info("Executed Step = VAR,Json,var_CSVData,C:/GIAS_Automation/CLIC Regression/QUA1/InputData/CLIC36_IP_CompareImages.csv,TGTYPESCREENREG");
		String var_Date;
                 LOGGER.info("Executed Step = VAR,String,var_Date,TGTYPESCREENREG");
		var_Date = getJsonData(var_CSVData , "$.records["+var_Count+"].Date");
                 LOGGER.info("Executed Step = STORE,var_Date,var_CSVData,$.records[{var_Count}].Date,TGTYPESCREENREG");
		try { 
		  
				    	
						    	//  COMPANY OF Texas (CL NW) PREMIUM NOTICE FOR HEATH
						    	
							 Boolean policyFound =false; 
					        // var_Date= getJsonData(var_CSVData , "$.records["+var_Count+"].Date");
					         String PolicyNumber ="";
					         String PolicyNumberfileName ="";
					         String day="";
					        
					    	 String array[] = var_Date.split("/");
							 System.out.println("1st"+array[0].length());
							 if(array[0].length()==1) {
								 array[0]="0"+array[0];
								 System.out.println("after adding"+array[0]);
							 }
							 if(array[1].length()==1) {
								 array[1]="0"+array[1];
								 System.out.println("after adding"+array[1]);
							 }
							 int dayint =Integer.parseInt(array[1]);
							 day = array[1];
								/*
								 * if(dayint<31) { dayint =dayint +1; day=String.valueOf(dayint);
								 * if(day.length()==1) { day ="0"+day; } }
								 */
									 
							 String var_DateforDB = array[0]+"/"+array[1]+"/"+array[2];
							  
							 var_Date = array[2]+"-"+array[0]+"-"+day;
							 System.out.println("a: "+var_Date);
								
									
									// BaseLined Image for CLNW (PremNotice) for health
									
									String BaseImageforPremNotice ="C:\\/GIAS_Automation\\/Images\\/CLNW\\/CLIC_CNWINDBILL_PREMIUM.TIF";
									String GeneratedPath ="\\\\gvlfp04\\printreview\\CLIC_CNWINDBILLM_TIFs_UAT1";
									
									// Create  folder Name by Appending Date  
									
									String FolderPrefix ="CLIC_CNWINDBILL_UAT1_"+var_Date;
									
									GeneratedPath = GeneratedPath+"\\"+FolderPrefix;
									
									
									System.out.println("GenereatedPath ::"+GeneratedPath);
									
									
									java.sql.Connection con = null;   

							        // Load SQL Server JDBC driver and establish connection UAT.
							        String connectionUrl = "jdbc:sqlserver://CIS-GIASDBT2:443" + "3;" +
							                "databaseName=CLIS1DT" + "A;"
							                +
							                "IntegratedSecurity = true";
							        System.out.print("Connecting to SQL Server ... ");
							        try {
										con = DriverManager.getConnection(connectionUrl);
									} catch (SQLException e) {
										// TODO Auto-generated catch block
										e.printStackTrace();
									}
						            System.out.println("Connected to database.");
						            
						            String sql1primCL ="Use clis1dta "+
						            		  "Select c.field1, c.company,c.insuranceType, p.noticeType, p.company, p.controlNumber, p.noticeNumber, p.serialNumber, p.insuredName, p.policynumber,"+
						            		 "p.policyNumber, p.amount, p.dueDate, a.accountName, a.accountNameAddress2, a.accountNameAddress4 "+
						            		"from iios.BillingCombinedPol p join iios.BillingCombinedAddr a on p.company = a.company and p.controlNumber = a.controlNumber "+
						            		"and p.noticeNumber = a.noticeNumber "+
						            		"join iios.BillingCombined c on p.company = c.company and p.controlNumber = c.controlNumber and p.noticeNumber = c.noticeNumber "+
						            		"where "+
						            		"field1 = '"+var_DateforDB+"' "+
						            		"and c.company= 'CLNW' "+
						            		"and p.noticeType ='1' "+
						            		"and insuranceType ='H'"+
						            		"order by controlNumber, field1,p.noticeNumber, p.serialNumber";
						            			
						            System.out.println(sql1primCL);
						            Statement statement;
									try {
										statement = con.createStatement();
									
									ResultSet rs = null;
									statement.setMaxRows(1);
									System.out.println("EXECUTED");
									rs = statement.executeQuery(sql1primCL);
									 while (rs.next()) {
										 
										  PolicyNumber =rs.getString("policyNumber");
										 
										  System.out.println("PolicyNumber::"+PolicyNumber);
										  
										  if(PolicyNumber.equalsIgnoreCase("Null")) {
											    policyFound = false ;
										  }
										  else {
											  policyFound = true;
										  }
							            }
									
									 statement.close();
									}
									 catch (SQLException e) {
											// TODO Auto-generated catch block
											e.printStackTrace();
										}
									
									if(policyFound ==true) {
									 
									// List of file names in the folder 
										
									// List of file names in the folder 
										
										File folder = new File(GeneratedPath);
										File[] listOfFiles = folder.listFiles();
										
										System.out.println("Lenght ::"+listOfFiles.length);

										for (int i = 0; i < listOfFiles.length; i++) {
										 // if (listOfFiles[i].isFile()) {
										    System.out.println("File " + listOfFiles[i].getName());
										    System.out.println("POLICY ::"+PolicyNumber);
										    String imagname = listOfFiles[i].getName().trim();
										    System.out.println("Condition:"+imagname.toLowerCase().contains(PolicyNumber.toLowerCase()));
										    System.out.println("imagname"+imagname);
										    if(imagname.toLowerCase().contains(PolicyNumber.trim().toLowerCase())) { 	
										    	PolicyNumberfileName =listOfFiles[i].getName();
										    	GeneratedPath = GeneratedPath+"\\"+PolicyNumberfileName;
										    	i=listOfFiles.length+1;
										    }
										  } 
										 
									//	}
										
										
									System.out.println(" Full Path is ::"+GeneratedPath);
									
									GeneratedPath= GeneratedPath.replace("\\","\\\\");
									
									
									// ALL The API FOR  COMPARING IMAGE
									
									String url = "http://10.1.104.229/compare/index.php";
							    	CloseableHttpClient httpClient = HttpClients.createDefault();
									HttpPost httppost = new HttpPost(url); //http post request
									
									// passing json as a string 
									String entityString ="{\"image1\":\""+BaseImageforPremNotice+"\" ,\"image2\": \""+GeneratedPath+"\",\"coordinates\": \"15,218,779,703|188,35,282,76|652,740,775,790|126,812,253,844|96,907,336,980|68,998,768,1025\"}\r\n"
											+ "";
									System.out.println("EntityString::"+entityString);
									try {
										httppost.setEntity(new StringEntity(entityString));
										CloseableHttpResponse closebaleHttpResponse;
										closebaleHttpResponse = httpClient.execute(httppost);
										int statusCode = closebaleHttpResponse.getStatusLine().getStatusCode();
										System.out.println("Status from API:"+statusCode);
										// convert json to string 	
										String responseString = EntityUtils.toString(closebaleHttpResponse.getEntity(), "UTF-8");
										System.out.println("Response from API :"+responseString);
										
										JSONObject response = new JSONObject(responseString);
										System.out.println("Status:"+response.get("msg"));
										if(response.getString("msg").equalsIgnoreCase("Something went wrong in image2")) {
											writeToCSV("Company","CLNW HEATH PREMIUM");
											writeToCSV("Image compared ", "NO CLNW POLICY FOR PREMIUM ");
											writeToCSV("Image Result After comparision", "NULL");
										}
										System.out.println("Image : "+response.getString("image_difference_link"));
										System.out.println("Both the image are same "+response.getBoolean("both_are_same"));
										writeToCSV("Company","CLNW  HEALTH PRIMIUM");
										writeToCSV("Image compared ", PolicyNumberfileName);
										writeToCSV("Image Result After comparision", response.getString("image_difference_link"));
										if(response.getBoolean("both_are_same")== true) {
											writeToCSV("Resut", "Both are same");
										}
										else {
											writeToCSV("Resut", "Both are not same");
											Assert.assertTrue(false,"Comparsion failed");
										}
										
										
									} catch (ClientProtocolException e) {
										// TODO Auto-generated catch block
										
									} catch (IOException e) {
										// TODO Auto-generated catch block
										
										e.printStackTrace();
										e.printStackTrace();
									}
								
									
									}
									else {
										
										writeToCSV("Company","CLNW HEATH PREMIUM");
										writeToCSV("Image compared ", "NO CLNW POLICY FOR PREMIUM ");
										writeToCSV("Image Result After comparision", "NULL");
										
										
									}
									
									
				 
		 
		 ;} 
		 catch(Exception e) { PrintVal("Renderer Script Exception : "+e.toString()); } 
		 PrintVal("Executed Step = CALLRENDERERSCRIPT,COMPAREIMAGECLNWHEALTHPREMIUMNOTICE"); 
		try { 
		  
				     	
						    	//  COMPANY OF Texas (CL NW) REMINDER NOTICE  FOR HEALTH
						    	
						    	 Boolean policyFound =false; 
						         var_Date= getJsonData(var_CSVData , "$.records["+var_Count+"].Date");
						         String PolicyNumber ="";
						         String PolicyNumberfileName ="";
						         String day="";
						        
						    	 String array[] = var_Date.split("/");
								 System.out.println("1st"+array[0].length());
								 if(array[0].length()==1) {
									 array[0]="0"+array[0];
									 System.out.println("after adding"+array[0]);
								 }
								 if(array[1].length()==1) {
									 array[1]="0"+array[1];
									 System.out.println("after adding"+array[1]);
								 }
								 int dayint =Integer.parseInt(array[1]);
								 day = array[1];
									/*
									 * if(dayint<31) { dayint =dayint +1; day=String.valueOf(dayint);
									 * if(day.length()==1) { day ="0"+day; } }
									 */
										 
								 String var_DateforDB = array[0]+"/"+array[1]+"/"+array[2];
								  
								 var_Date = array[2]+"-"+array[0]+"-"+day;
								 System.out.println("a: "+var_Date);
						    	
								
									
									// BaseLined Image for CompanyTaxes (REMINDER NOTICE)
									
									String BaseImageforPremNotice ="C:\\/GIAS_Automation\\/Images\\/CLNW\\/CLIC_CNWINDBILL_REMINDER.TIF";
									String GeneratedPath ="\\\\gvlfp04\\printreview\\CLIC_CNWINDBILLM_TIFs_UAT1";
									
									// Create  folder Name by Appending Date  
									
									String FolderPrefix ="CLIC_CNWINDBILL_UAT1_"+var_Date;
									
									GeneratedPath = GeneratedPath+"\\"+FolderPrefix;
									
									
									System.out.println("GenereatedPath ::"+GeneratedPath);
									
									
									java.sql.Connection con = null;   

							        // Load SQL Server JDBC driver and establish connection UAT.
							        String connectionUrl = "jdbc:sqlserver://CIS-GIASDBT2:443" + "3;" +
							                "databaseName=CLIS1DT" + "A;"
							                +
							                "IntegratedSecurity = true";
							        System.out.print("Connecting to SQL Server ... ");
							        try {
										con = DriverManager.getConnection(connectionUrl);
									} catch (SQLException e) {
										// TODO Auto-generated catch block
										e.printStackTrace();
									}
						            System.out.println("Connected to database.");
						            
						            String sql1primCL ="Use clis1dta "+
						            		  "Select c.field1, c.company,c.insuranceType, p.noticeType, p.company, p.controlNumber, p.noticeNumber, p.serialNumber, p.insuredName, p.policynumber,"+
						            		 "p.policyNumber, p.amount, p.dueDate, a.accountName, a.accountNameAddress2, a.accountNameAddress4 "+
						            		"from iios.BillingCombinedPol p join iios.BillingCombinedAddr a on p.company = a.company and p.controlNumber = a.controlNumber "+
						            		"and p.noticeNumber = a.noticeNumber "+
						            		"join iios.BillingCombined c on p.company = c.company and p.controlNumber = c.controlNumber and p.noticeNumber = c.noticeNumber "+
						            		"where "+
						            		"field1 = '"+var_DateforDB+"' "+
						            		"and c.company= 'CLNW' "+
						            		"and p.noticeType ='2' "+
						            		"and insuranceType ='H' "+
						            		"order by controlNumber, field1,p.noticeNumber, p.serialNumber";
						            			
						            System.out.println(sql1primCL);
						            Statement statement;
									try {
										statement = con.createStatement();
									
									ResultSet rs = null;
									statement.setMaxRows(1);
									System.out.println("EXECUTED");
									rs = statement.executeQuery(sql1primCL);
									 while (rs.next()) {
										 
										  PolicyNumber =rs.getString("policyNumber");
										 
										  System.out.println("PolicyNumber::"+PolicyNumber);
										  
										  if(PolicyNumber.equalsIgnoreCase("Null")) {
											    policyFound = false ;
										  }
										  else {
											  policyFound = true;
										  }
							            }
									
									 statement.close();
									}
									 catch (SQLException e) {
											// TODO Auto-generated catch block
											e.printStackTrace();
										}
									
									
									System.out.println("POLICY FOUND"+policyFound);
									if(policyFound==true) {
									 
									// List of file names in the folder 
										
										File folder = new File(GeneratedPath);
										File[] listOfFiles = folder.listFiles();
										
										System.out.println("Lenght ::"+listOfFiles.length);

										for (int i = 0; i < listOfFiles.length; i++) {
										 // if (listOfFiles[i].isFile()) {
										    System.out.println("File " + listOfFiles[i].getName());
										    System.out.println("POLICY ::"+PolicyNumber);
										    String imagname = listOfFiles[i].getName().trim();
										    System.out.println("Condition:"+imagname.toLowerCase().contains(PolicyNumber.toLowerCase()));
										    System.out.println("imagname"+imagname);
										    if(imagname.toLowerCase().contains(PolicyNumber.trim().toLowerCase())) { 	
										    	PolicyNumberfileName =listOfFiles[i].getName();
										    	GeneratedPath = GeneratedPath+"\\"+PolicyNumberfileName;
										    	i=listOfFiles.length+1;
										    }
										  } 
										 
									//	}
										
									System.out.println(" Full Path is ::"+GeneratedPath);
									
									GeneratedPath= GeneratedPath.replace("\\","\\\\");
									
									
									// ALL The API FOR  COMPARING IMAGE
									
									String url = "http://10.1.104.229/compare/index.php";
							    	CloseableHttpClient httpClient = HttpClients.createDefault();
									HttpPost httppost = new HttpPost(url); //http post request
									
									// passing json as a string 
									String entityString ="{\"image1\":\""+BaseImageforPremNotice+"\" ,\"image2\": \""+GeneratedPath+"\",\"coordinates\": \"163,38,304,83|19,218,784,697|648,742,783,809|126,817,233,842|67,904,346,986|68,995,774,1031\"}\r\n"
											+ "";
									System.out.println("EntityString::"+entityString);
									try {
										httppost.setEntity(new StringEntity(entityString));
										CloseableHttpResponse closebaleHttpResponse;
										closebaleHttpResponse = httpClient.execute(httppost);
										int statusCode = closebaleHttpResponse.getStatusLine().getStatusCode();
										System.out.println("Status from API:"+statusCode);
										// convert json to string 	
										String responseString = EntityUtils.toString(closebaleHttpResponse.getEntity(), "UTF-8");
										System.out.println("Response from API :"+responseString);
										
										JSONObject response = new JSONObject(responseString);
										System.out.println("Status:"+response.get("msg"));
										if(response.getString("msg").equalsIgnoreCase("Something went wrong in image2")) {
											writeToCSV("Company","CLNW HEALTH REMIDER NOTICE");
											writeToCSV("Image compared ", "NO CLNW HEALTH REMIDER NOTICE ");
											writeToCSV("Image Result After comparision", "NULL");
										}
										System.out.println("Image : "+response.getString("image_difference_link"));
										System.out.println("Both the image are same "+response.getBoolean("both_are_same"));
										writeToCSV("Company","CLNW HEALTH REMIDER NOTICE");
										writeToCSV("Image compared ", PolicyNumberfileName);
										writeToCSV("Image Result After comparision", response.getString("image_difference_link"));
										if(response.getBoolean("both_are_same")== true) {
											writeToCSV("Resut", "Both are same");
										}
										else {
											writeToCSV("Resut", "Both are not same");
										}
										
										
									} catch (ClientProtocolException e) {
										// TODO Auto-generated catch block
										e.printStackTrace();
									} catch (IOException e) {
										// TODO Auto-generated catch block
										e.printStackTrace();
									}
								
									
									}
									else {
										
										writeToCSV("Company","CLNW  HEALTH REMINDER  NOTICE");
										writeToCSV("Image compared ", "NO CLNW POLICY FOR HEATH REMINDER NOTICE ");
										writeToCSV("Image Result After comparision", "NULL");
										
										
									}
									
									
				 
		 
		 ;} 
		 catch(Exception e) { PrintVal("Renderer Script Exception : "+e.toString()); } 
		 PrintVal("Executed Step = CALLRENDERERSCRIPT,COMPAREIMAGESCLNWHEALTHREMAINDERNOTICE"); 
		try { 
		  
				 		    	
						    	//  COMPANY OF Texas (CL NW) LAPSE NOTICE  FOR HEALTH
						    	
							 Boolean policyFound =false; 
					       var_Date= getJsonData(var_CSVData , "$.records["+var_Count+"].Date");
					         String PolicyNumber ="";
					         String PolicyNumberfileName ="";
					         String day="";
					        
					    	 String array[] = var_Date.split("/");
							 System.out.println("1st"+array[0].length());
							 if(array[0].length()==1) {
								 array[0]="0"+array[0];
								 System.out.println("after adding"+array[0]);
							 }
							 if(array[1].length()==1) {
								 array[1]="0"+array[1];
								 System.out.println("after adding"+array[1]);
							 }
							 int dayint =Integer.parseInt(array[1]);
							 day = array[1];
								/*
								 * if(dayint<31) { dayint =dayint +1; day=String.valueOf(dayint);
								 * if(day.length()==1) { day ="0"+day; } }
								 */
							 String var_DateforDB = array[0]+"/"+array[1]+"/"+array[2];
							  
							 var_Date = array[2]+"-"+array[0]+"-"+day;
							 System.out.println("a: "+var_Date);
								
									
									// BaseLined Image for CompanyTaxes (LapseNOTICE LIFE)
									
									String BaseImageforPremNotice ="C:\\/GIAS_Automation\\/Images\\/CLNW\\/CLIC_CNWINDBILL_LAPSE.TIF";
									String GeneratedPath ="\\\\gvlfp04\\printreview\\CLIC_CNWINDBILLM_TIFs_UAT1";
									
									// Create  folder Name by Appending Date  
									
									String FolderPrefix ="CLIC_CNWINDBILL_UAT1_"+var_Date;
									
									GeneratedPath = GeneratedPath+"\\"+FolderPrefix;
									
									
									System.out.println("GenereatedPath ::"+GeneratedPath);
									
									
									java.sql.Connection con = null;   

							        // Load SQL Server JDBC driver and establish connection UAT.
							        String connectionUrl = "jdbc:sqlserver://CIS-GIASDBT2:443" + "3;" +
							                "databaseName=CLIS1DT" + "A;"
							                +
							                "IntegratedSecurity = true";
							        System.out.print("Connecting to SQL Server ... ");
							        try {
										con = DriverManager.getConnection(connectionUrl);
									} catch (SQLException e) {
										// TODO Auto-generated catch block
										e.printStackTrace();
									}
						            System.out.println("Connected to database.");
						            
						           System.out.println("POLICY NUMBER ::::::"+PolicyNumber);
						            
						            String sql1primCL ="Use clis1dta "+
						            		  "Select c.field1, c.company,c.insuranceType, p.noticeType, p.company, p.controlNumber, p.noticeNumber, p.serialNumber, p.insuredName, p.policynumber,"+
						            		 "p.policyNumber, p.amount, p.dueDate, a.accountName, a.accountNameAddress2, a.accountNameAddress4 "+
						            		"from iios.BillingCombinedPol p join iios.BillingCombinedAddr a on p.company = a.company and p.controlNumber = a.controlNumber "+
						            		"and p.noticeNumber = a.noticeNumber "+
						            		"join iios.BillingCombined c on p.company = c.company and p.controlNumber = c.controlNumber and p.noticeNumber = c.noticeNumber "+
						            		"where "+
						            		"field1 = '"+var_DateforDB+"' "+
						            		"and c.company= 'CLNW' "+
						            		"and p.noticeType ='3' "+
						            		"and insuranceType ='H'"+
						            		"order by controlNumber, field1,p.noticeNumber, p.serialNumber";
						            			
						            System.out.println(sql1primCL);
						            Statement statement;
									try {
										statement = con.createStatement();
									
									ResultSet rs = null;
									statement.setMaxRows(1);
									System.out.println("EXECUTED");
									rs = statement.executeQuery(sql1primCL);
									 while (rs.next()) {
										 
										  PolicyNumber =rs.getString("policyNumber");
										 
										  System.out.println("PolicyNumber::"+PolicyNumber);
										  
										  if(PolicyNumber.equalsIgnoreCase("Null")) {
											    policyFound = false ;
										  }
										  else {
											  policyFound = true;
										  }
							            }
									
									 statement.close();
									}
									 catch (SQLException e) {
											// TODO Auto-generated catch block
											e.printStackTrace();
										}
									
									if(policyFound ==true) {
									 
									// List of file names in the folder 
										
									// List of file names in the folder 
										
										File folder = new File(GeneratedPath);
										File[] listOfFiles = folder.listFiles();
										
										System.out.println("Lenght ::"+listOfFiles.length);

										for (int i = 0; i < listOfFiles.length; i++) {
										 // if (listOfFiles[i].isFile()) {
										    System.out.println("File " + listOfFiles[i].getName());
										    System.out.println("POLICY ::"+PolicyNumber);
										    String imagname = listOfFiles[i].getName().trim();
										    System.out.println("Condition:"+imagname.toLowerCase().contains(PolicyNumber.toLowerCase()));
										    System.out.println("imagname"+imagname);
										    if(imagname.toLowerCase().contains(PolicyNumber.trim().toLowerCase())) { 	
										    	PolicyNumberfileName =listOfFiles[i].getName();
										    	GeneratedPath = GeneratedPath+"\\"+PolicyNumberfileName;
										    	i=listOfFiles.length+1;
										    }
										  } 
										 
									//	}
										
										
									System.out.println(" Full Path is ::"+GeneratedPath);
									
									GeneratedPath= GeneratedPath.replace("\\","\\\\");
									
									
									// ALL The API FOR  COMPARING IMAGE
									
									String url = "http://10.1.104.229/compare/index.php";
							    	CloseableHttpClient httpClient = HttpClients.createDefault();
									HttpPost httppost = new HttpPost(url); //http post request
									
									// passing json as a string 
									String entityString ="{\"image1\":\""+BaseImageforPremNotice+"\" ,\"image2\": \""+GeneratedPath+"\",\"coordinates\": \"187,38,286,109|618,115,755,155|13,221,802,640|648,742,783,809|126,817,233,842|67,904,346,986|68,995,774,1031\"}\r\n"
											+ "";
									System.out.println("EntityString::"+entityString);
									try {
										httppost.setEntity(new StringEntity(entityString));
										CloseableHttpResponse closebaleHttpResponse;
										closebaleHttpResponse = httpClient.execute(httppost);
										int statusCode = closebaleHttpResponse.getStatusLine().getStatusCode();
										System.out.println("Status from API:"+statusCode);
										// convert json to string 	
										String responseString = EntityUtils.toString(closebaleHttpResponse.getEntity(), "UTF-8");
										System.out.println("Response from API :"+responseString);
										
										JSONObject response = new JSONObject(responseString);
										if(response.getString("msg").equalsIgnoreCase("Something went wrong in image2")) {
											writeToCSV("Company","CLNW HEALTH LAPSE NOTICE");
											writeToCSV("Image compared ", "NO CLNW HEALTH LAPSE NOTICE ");
											writeToCSV("Image Result After comparision", "NULL");
										}
										
										System.out.println("Image : "+response.getString("image_difference_link"));
										System.out.println("Both the image are same "+response.getBoolean("both_are_same"));
										writeToCSV("Company","CLNW HEALTH LAPSE NOTICE");
										writeToCSV("Image compared ", PolicyNumberfileName);
										writeToCSV("Image Result After comparision", response.getString("image_difference_link"));
										if(response.getBoolean("both_are_same")== true) {
											writeToCSV("Resut", "Both are same");
										}
										else {
											writeToCSV("Resut", "Both are not same");
										}
										
										
									} catch (ClientProtocolException e) {
										// TODO Auto-generated catch block
										e.printStackTrace();
									} catch (IOException e) {
										// TODO Auto-generated catch block
										e.printStackTrace();
									}
								
									
									}
									else {
										
										writeToCSV("Company","CLNW HEALTH LAPSENOTICE");
										writeToCSV("Image compared ", "NO CLNW POLICY FOR LAPSENOTICE ");
										writeToCSV("Image Result After comparision", "NULL");
										
										
									}
									
									
									
				 
				 
		 
		 ;} 
		 catch(Exception e) { PrintVal("Renderer Script Exception : "+e.toString()); } 
		 PrintVal("Executed Step = CALLRENDERERSCRIPT,COMPAREIMAGECLNWHEALTHLAPSENOTICE"); 
		try { 
		     	
				    	//  COMPANY OF Texas (CL NW) LAPSE NOTICE  FOR Health
				    	
					 Boolean policyFound =false; 
			    //     String var_Date ="09/21/2020";
			         String PolicyNumber ="";
			         String PolicyNumberfileName ="";
			         String day="";
			        
			    	 String array[] = var_Date.split("/");
					 System.out.println("1st"+array[0].length());
					 if(array[0].length()==1) {
						 array[0]="0"+array[0];
						 System.out.println("after adding"+array[0]);
					 }
					 if(array[1].length()==1) {
						 array[1]="0"+array[1];
						 System.out.println("after adding"+array[1]);
					 }
					 int dayint =Integer.parseInt(array[1]);
					 day = array[1];
					 if(dayint<31) {
						 dayint =dayint +1;
						 day=String.valueOf(dayint);
						 if(day.length()==1) {
							 day ="0"+day;
						 }
					 }
							 
					 String var_DateforDB = array[0]+"/"+array[1]+"/"+array[2];
					  
					 var_Date = array[2]+"-"+array[0]+"-"+day;
					 System.out.println("a: "+var_Date);
						
							
							// BaseLined Image for CompanyTaxes (LapseNOTICE LIFE)
							
							String BaseImageforPremNotice ="C:\\/GIAS_Automation\\/Images\\/CLNW\\/CLIC_CNWINDBILL_LAPSE.TIF";
							String GeneratedPath ="\\\\gvlfp04\\printreview\\CLIC_CNWINDBILLM_TIFs_UAT1";
							
							// Create  folder Name by Appending Date  
							
							String FolderPrefix ="CLIC_CNWINDBILL_UAT1_"+var_Date;
							
							GeneratedPath = GeneratedPath+"\\"+FolderPrefix;
							
							
							System.out.println("GenereatedPath ::"+GeneratedPath);
							
							
							java.sql.Connection con = null;   

					        // Load SQL Server JDBC driver and establish connection UAT.
					        String connectionUrl = "jdbc:sqlserver://CIS-GIASDBT2:443" + "3;" +
					                "databaseName=CLIS1DT" + "A;"
					                +
					                "IntegratedSecurity = true";
					        System.out.print("Connecting to SQL Server ... ");
					        try {
								con = DriverManager.getConnection(connectionUrl);
							} catch (SQLException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
				            System.out.println("Connected to database.");
				            
				            String sql1primCL ="Use clis1dta "+
				            		  "Select c.field1, c.company,c.insuranceType, p.noticeType, p.company, p.controlNumber, p.noticeNumber, p.serialNumber, p.insuredName, p.policynumber,"+
				            		 "p.policyNumber, p.amount, p.dueDate, a.accountName, a.accountNameAddress2, a.accountNameAddress4 "+
				            		"from iios.BillingCombinedPol p join iios.BillingCombinedAddr a on p.company = a.company and p.controlNumber = a.controlNumber "+
				            		"and p.noticeNumber = a.noticeNumber "+
				            		"join iios.BillingCombined c on p.company = c.company and p.controlNumber = c.controlNumber and p.noticeNumber = c.noticeNumber "+
				            		"where "+
				            		"field1 = '"+var_DateforDB+"' "+
				            		"and c.company= 'CLNW' "+
				            		"and p.noticeType ='L' "+
				            		"and insuranceType ='L'"+
				            		"order by controlNumber, field1,p.noticeNumber, p.serialNumber";
				            			
				            System.out.println(sql1primCL);
				            Statement statement;
							try {
								statement = con.createStatement();
							
							ResultSet rs = null;
							statement.setMaxRows(1);
							System.out.println("EXECUTED");
							rs = statement.executeQuery(sql1primCL);
							 while (rs.next()) {
								 
								  PolicyNumber =rs.getString("policyNumber");
								 
								  System.out.println("PolicyNumber::"+PolicyNumber);
								  
								  if(PolicyNumber.equalsIgnoreCase("Null")) {
									    policyFound = false ;
								  }
								  else {
									  policyFound = true;
								  }
					            }
							
							 statement.close();
							}
							 catch (SQLException e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
								}
							
							if(policyFound ==true) {
							 
							// List of file names in the folder 
								
							// List of file names in the folder 
								
								File folder = new File(GeneratedPath);
								File[] listOfFiles = folder.listFiles();
								
								System.out.println("Lenght ::"+listOfFiles.length);

								for (int i = 0; i < listOfFiles.length; i++) {
								 // if (listOfFiles[i].isFile()) {
								    System.out.println("File " + listOfFiles[i].getName());
								    System.out.println("POLICY ::"+PolicyNumber);
								    String imagname = listOfFiles[i].getName().trim();
								    System.out.println("Condition:"+imagname.toLowerCase().contains(PolicyNumber.toLowerCase()));
								    System.out.println("imagname"+imagname);
								    if(imagname.toLowerCase().contains(PolicyNumber.trim().toLowerCase())) { 	
								    	PolicyNumberfileName =listOfFiles[i].getName();
								    	GeneratedPath = GeneratedPath+"\\"+PolicyNumberfileName;
								    	i=listOfFiles.length+1;
								    }
								  } 
								 
							//	}
								
								
							System.out.println(" Full Path is ::"+GeneratedPath);
							
							GeneratedPath= GeneratedPath.replace("\\","\\\\");
							
							
							// ALL The API FOR  COMPARING IMAGE
							
							String url = "http://10.1.104.229/compare/index.php";
					    	CloseableHttpClient httpClient = HttpClients.createDefault();
							HttpPost httppost = new HttpPost(url); //http post request
							
							// passing json as a string 
							String entityString ="{\"image1\":\""+BaseImageforPremNotice+"\" ,\"image2\": \""+GeneratedPath+"\",\"coordinates\": \"187,38,286,109|618,115,755,155|13,221,802,640|648,742,783,809|126,817,233,842|67,904,346,986|68,995,774,1031\"}\r\n"
									+ "";
							System.out.println("EntityString::"+entityString);
							try {
								httppost.setEntity(new StringEntity(entityString));
								CloseableHttpResponse closebaleHttpResponse;
								closebaleHttpResponse = httpClient.execute(httppost);
								int statusCode = closebaleHttpResponse.getStatusLine().getStatusCode();
								System.out.println("Status from API:"+statusCode);
								// convert json to string 	
								String responseString = EntityUtils.toString(closebaleHttpResponse.getEntity(), "UTF-8");
								System.out.println("Response from API :"+responseString);
								
								JSONObject response = new JSONObject(responseString);
								System.out.println("Image : "+response.getString("image_difference_link"));
								System.out.println("Both the image are same "+response.getBoolean("both_are_same"));
								writeToCSV("Company","CLNW LIFE LOANINTREST NOTICE");
								writeToCSV("Image compared ", PolicyNumberfileName);
								writeToCSV("Image Result After comparision", response.getString("image_difference_link"));
								if(response.getBoolean("both_are_same")== true) {
									writeToCSV("Resut", "Both are same");
								}
								else {
									writeToCSV("Resut", "Both are not same");
								}
								
								
							} catch (ClientProtocolException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							} catch (IOException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
						
							
							}
							else {
								
								writeToCSV("Company","CLNW LIFE LAPSENOTICE");
								writeToCSV("Image compared ", "NO CLNW POLICY FOR LOAN INTRESTNOTICE ");
								writeToCSV("Image Result After comparision", "NULL");
								
								
							}
							
		 
		 ;} 
		 catch(Exception e) { PrintVal("Renderer Script Exception : "+e.toString()); } 
		 PrintVal("Executed Step = CALLRENDERERSCRIPT,COMPAREIMAGESCLNWHEALTHLOANINTRESTNOTICE"); 

    }


    @Test
    public void clicregcompareimageclgr() {

        // No need to declare / print method name here, handled by TestNg listener
        // declare("clicregcompareimageclgr");

		int var_Count = 0;
                 LOGGER.info("Executed Step = VAR,Integer,var_Count,0,TGTYPESCREENREG");
		com.jayway.jsonpath.ReadContext var_CSVData = getJsonData("C:/GIAS_Automation/CLIC Regression/QUA1/InputData/CLIC36_IP_CompareImages.csv");
                 LOGGER.info("Executed Step = VAR,Json,var_CSVData,C:/GIAS_Automation/CLIC Regression/QUA1/InputData/CLIC36_IP_CompareImages.csv,TGTYPESCREENREG");
		String var_Date;
                 LOGGER.info("Executed Step = VAR,String,var_Date,TGTYPESCREENREG");
		var_Date = getJsonData(var_CSVData , "$.records["+var_Count+"].Date");
                 LOGGER.info("Executed Step = STORE,var_Date,var_CSVData,$.records[{var_Count}].Date,TGTYPESCREENREG");
		try { 
		  
				  
						    	
								    	//  COMPANY OF Texas (CL GR) PREMIUM NOTICE FOR HEATH
								    	
									 Boolean policyFound =false; 
							        // var_Date= getJsonData(var_CSVData , "$.records["+var_Count+"].Date");
							         String PolicyNumber ="";
							         String PolicyNumberfileName ="";
							         String day="";
							        
							    	 String array[] = var_Date.split("/");
									 System.out.println("1st"+array[0].length());
									 if(array[0].length()==1) {
										 array[0]="0"+array[0];
										 System.out.println("after adding"+array[0]);
									 }
									 if(array[1].length()==1) {
										 array[1]="0"+array[1];
										 System.out.println("after adding"+array[1]);
									 }
									 int dayint =Integer.parseInt(array[1]);
									 day = array[1];
										/*
										 * if(dayint<31) { dayint =dayint +1; day=String.valueOf(dayint);
										 * if(day.length()==1) { day ="0"+day; } }
										 */
											 
									 String var_DateforDB = array[0]+"/"+array[1]+"/"+array[2];
									  
									 var_Date = array[2]+"-"+array[0]+"-"+day;
									 System.out.println("a: "+var_Date);
										
											
											// BaseLined Image for CLNW (PremNotice) for health
											
											String BaseImageforPremNotice ="C:\\/GIAS_Automation\\/Images\\/CLGR\\/CLIC_CGRINDBILL_PREMIUM.TIF";
											String GeneratedPath ="\\\\gvlfp04\\printreview\\CLIC_CGRINDBILLM_TIFs_UAT1";
											
											// Create  folder Name by Appending Date  
											
											String FolderPrefix ="CLIC_CGRINDBILL_UAT1_"+var_Date;
											
											GeneratedPath = GeneratedPath+"\\"+FolderPrefix;
											
											
											System.out.println("GenereatedPath ::"+GeneratedPath);
											
											
											java.sql.Connection con = null;   

									        // Load SQL Server JDBC driver and establish connection UAT.
									        String connectionUrl = "jdbc:sqlserver://CIS-GIASDBT2:443" + "3;" +
									                "databaseName=CLIS1DT" + "A;"
									                +
									                "IntegratedSecurity = true";
									        System.out.print("Connecting to SQL Server ... ");
									        try {
												con = DriverManager.getConnection(connectionUrl);
											} catch (SQLException e) {
												// TODO Auto-generated catch block
												e.printStackTrace();
											}
								            System.out.println("Connected to database.");
								            
								            String sql1primCL ="Use clis1dta "+
								            		  "Select c.field1, c.company,c.insuranceType, p.noticeType, p.company, p.controlNumber, p.noticeNumber, p.serialNumber, p.insuredName, p.policynumber,"+
								            		 "p.policyNumber, p.amount, p.dueDate, a.accountName, a.accountNameAddress2, a.accountNameAddress4 "+
								            		"from iios.BillingCombinedPol p join iios.BillingCombinedAddr a on p.company = a.company and p.controlNumber = a.controlNumber "+
								            		"and p.noticeNumber = a.noticeNumber "+
								            		"join iios.BillingCombined c on p.company = c.company and p.controlNumber = c.controlNumber and p.noticeNumber = c.noticeNumber "+
								            		"where "+
								            		"field1 = '"+var_DateforDB+"' "+
								            		"and c.company= 'CLGR' "+
								            		"and p.noticeType ='1' "+
								            		"and insuranceType ='H'"+
								            		"order by controlNumber, field1,p.noticeNumber, p.serialNumber";
								            			
								            System.out.println(sql1primCL);
								            Statement statement;
											try {
												statement = con.createStatement();
											
											ResultSet rs = null;
											statement.setMaxRows(1);
											System.out.println("EXECUTED");
											rs = statement.executeQuery(sql1primCL);
											 while (rs.next()) {
												 
												  PolicyNumber =rs.getString("policyNumber");
												 
												  System.out.println("PolicyNumber::"+PolicyNumber);
												  
												  if(PolicyNumber.equalsIgnoreCase("Null")) {
													    policyFound = false ;
												  }
												  else {
													  policyFound = true;
												  }
									            }
											
											 statement.close();
											}
											 catch (SQLException e) {
													// TODO Auto-generated catch block
													e.printStackTrace();
												}
											
											if(policyFound ==true) {
											 
											// List of file names in the folder 
												
											// List of file names in the folder 
												
												File folder = new File(GeneratedPath);
												File[] listOfFiles = folder.listFiles();
												
												System.out.println("Lenght ::"+listOfFiles.length);

												for (int i = 0; i < listOfFiles.length; i++) {
												 // if (listOfFiles[i].isFile()) {
												    System.out.println("File " + listOfFiles[i].getName());
												    System.out.println("POLICY ::"+PolicyNumber);
												    String imagname = listOfFiles[i].getName().trim();
												    System.out.println("Condition:"+imagname.toLowerCase().contains(PolicyNumber.toLowerCase()));
												    System.out.println("imagname"+imagname);
												    if(imagname.toLowerCase().contains(PolicyNumber.trim().toLowerCase())) { 	
												    	PolicyNumberfileName =listOfFiles[i].getName();
												    	GeneratedPath = GeneratedPath+"\\"+PolicyNumberfileName;
												    	i=listOfFiles.length+1;
												    }
												  } 
												 
											//	}
												
												
											System.out.println(" Full Path is ::"+GeneratedPath);
											
											GeneratedPath= GeneratedPath.replace("\\","\\\\");
											
											
											// ALL The API FOR  COMPARING IMAGE
											
											String url = "http://10.1.104.229/compare/index.php";
									    	CloseableHttpClient httpClient = HttpClients.createDefault();
											HttpPost httppost = new HttpPost(url); //http post request
											
											// passing json as a string 
											String entityString ="{\"image1\":\""+BaseImageforPremNotice+"\" ,\"image2\": \""+GeneratedPath+"\",\"coordinates\": \"15,218,779,703|188,35,282,76|652,740,775,790|126,812,253,844|96,907,336,980|68,998,768,1025\"}\r\n"
													+ "";
											System.out.println("EntityString::"+entityString);
											try {
												httppost.setEntity(new StringEntity(entityString));
												CloseableHttpResponse closebaleHttpResponse;
												closebaleHttpResponse = httpClient.execute(httppost);
												int statusCode = closebaleHttpResponse.getStatusLine().getStatusCode();
												System.out.println("Status from API:"+statusCode);
												// convert json to string 	
												String responseString = EntityUtils.toString(closebaleHttpResponse.getEntity(), "UTF-8");
												System.out.println("Response from API :"+responseString);
												
												JSONObject response = new JSONObject(responseString);
												System.out.println("Status:"+response.get("msg"));
												if(response.getString("msg").equalsIgnoreCase("Something went wrong in image2")) {
													writeToCSV("Company","CLGR HEATH PREMIUM");
													writeToCSV("Image compared ", "NO CLGR POLICY FOR PREMIUM ");
													writeToCSV("Image Result After comparision", "NULL");
												}
												System.out.println("Image : "+response.getString("image_difference_link"));
												System.out.println("Both the image are same "+response.getBoolean("both_are_same"));
												writeToCSV("Company","CLGR  HEALTH PRIMIUM");
												writeToCSV("Image compared ", PolicyNumberfileName);
												writeToCSV("Image Result After comparision", response.getString("image_difference_link"));
												if(response.getBoolean("both_are_same")== true) {
													writeToCSV("Resut", "Both are same");
												}
												else {
													writeToCSV("Resut", "Both are not same");
													Assert.assertTrue(false,"Comparsion failed");
												}
												
												
											} catch (ClientProtocolException e) {
												// TODO Auto-generated catch block
												
											} catch (IOException e) {
												// TODO Auto-generated catch block
												
												e.printStackTrace();
												e.printStackTrace();
											}
										
											
											}
											else {
												
												writeToCSV("Company","CLGR HEATH PREMIUM");
												writeToCSV("Image compared ", "NO CLGR POLICY FOR PREMIUM ");
												writeToCSV("Image Result After comparision", "NULL");
												
												
											}
											
											
						 
		 
		 ;} 
		 catch(Exception e) { PrintVal("Renderer Script Exception : "+e.toString()); } 
		 PrintVal("Executed Step = CALLRENDERERSCRIPT,COMPAREIMAGECLGRHEALTHPRIMIUM"); 
		try { 
		  
				  
						     	
								    	//  COMPANY OF Texas (CL GR) REMINDER NOTICE  FOR HEALTH
								    	
								    	 Boolean policyFound =false; 
								         var_Date= getJsonData(var_CSVData , "$.records["+var_Count+"].Date");
								         String PolicyNumber ="";
								         String PolicyNumberfileName ="";
								         String day="";
								        
								    	 String array[] = var_Date.split("/");
										 System.out.println("1st"+array[0].length());
										 if(array[0].length()==1) {
											 array[0]="0"+array[0];
											 System.out.println("after adding"+array[0]);
										 }
										 if(array[1].length()==1) {
											 array[1]="0"+array[1];
											 System.out.println("after adding"+array[1]);
										 }
										 int dayint =Integer.parseInt(array[1]);
										 day = array[1];
											/*
											 * if(dayint<31) { dayint =dayint +1; day=String.valueOf(dayint);
											 * if(day.length()==1) { day ="0"+day; } }
											 */
												 
										 String var_DateforDB = array[0]+"/"+array[1]+"/"+array[2];
										  
										 var_Date = array[2]+"-"+array[0]+"-"+day;
										 System.out.println("a: "+var_Date);
								    	
										
											
											// BaseLined Image for CompanyTaxes (REMINDER NOTICE)
											
											String BaseImageforPremNotice ="C:\\/GIAS_Automation\\/Images\\/CLGR\\/CLIC_CGRINDBILL_REMINDER.TIF";
											String GeneratedPath ="\\\\gvlfp04\\printreview\\CLIC_CGRINDBILLM_TIFs_UAT1";
											
											// Create  folder Name by Appending Date  
											
											String FolderPrefix ="CLIC_CGRINDBILL_UAT1_"+var_Date;
											
											GeneratedPath = GeneratedPath+"\\"+FolderPrefix;
											
											
											System.out.println("GenereatedPath ::"+GeneratedPath);
											
											
											java.sql.Connection con = null;   

									        // Load SQL Server JDBC driver and establish connection UAT.
									        String connectionUrl = "jdbc:sqlserver://CIS-GIASDBT2:443" + "3;" +
									                "databaseName=CLIS1DT" + "A;"
									                +
									                "IntegratedSecurity = true";
									        System.out.print("Connecting to SQL Server ... ");
									        try {
												con = DriverManager.getConnection(connectionUrl);
											} catch (SQLException e) {
												// TODO Auto-generated catch block
												e.printStackTrace();
											}
								            System.out.println("Connected to database.");
								            
								            String sql1primCL ="Use clis1dta "+
								            		  "Select c.field1, c.company,c.insuranceType, p.noticeType, p.company, p.controlNumber, p.noticeNumber, p.serialNumber, p.insuredName, p.policynumber,"+
								            		 "p.policyNumber, p.amount, p.dueDate, a.accountName, a.accountNameAddress2, a.accountNameAddress4 "+
								            		"from iios.BillingCombinedPol p join iios.BillingCombinedAddr a on p.company = a.company and p.controlNumber = a.controlNumber "+
								            		"and p.noticeNumber = a.noticeNumber "+
								            		"join iios.BillingCombined c on p.company = c.company and p.controlNumber = c.controlNumber and p.noticeNumber = c.noticeNumber "+
								            		"where "+
								            		"field1 = '"+var_DateforDB+"' "+
								            		"and c.company= 'CLGR' "+
								            		"and p.noticeType ='2' "+
								            		"and insuranceType ='H' "+
								            		"order by controlNumber, field1,p.noticeNumber, p.serialNumber";
								            			
								            System.out.println(sql1primCL);
								            Statement statement;
											try {
												statement = con.createStatement();
											
											ResultSet rs = null;
											statement.setMaxRows(1);
											System.out.println("EXECUTED");
											rs = statement.executeQuery(sql1primCL);
											 while (rs.next()) {
												 
												  PolicyNumber =rs.getString("policyNumber");
												 
												  System.out.println("PolicyNumber::"+PolicyNumber);
												  
												  if(PolicyNumber.equalsIgnoreCase("Null")) {
													    policyFound = false ;
												  }
												  else {
													  policyFound = true;
												  }
									            }
											
											 statement.close();
											}
											 catch (SQLException e) {
													// TODO Auto-generated catch block
													e.printStackTrace();
												}
											
											
											System.out.println("POLICY FOUND"+policyFound);
											if(policyFound==true) {
											 
											// List of file names in the folder 
												
												File folder = new File(GeneratedPath);
												File[] listOfFiles = folder.listFiles();
												
												System.out.println("Lenght ::"+listOfFiles.length);

												for (int i = 0; i < listOfFiles.length; i++) {
												 // if (listOfFiles[i].isFile()) {
												    System.out.println("File " + listOfFiles[i].getName());
												    System.out.println("POLICY ::"+PolicyNumber);
												    String imagname = listOfFiles[i].getName().trim();
												    System.out.println("Condition:"+imagname.toLowerCase().contains(PolicyNumber.toLowerCase()));
												    System.out.println("imagname"+imagname);
												    if(imagname.toLowerCase().contains(PolicyNumber.trim().toLowerCase())) { 	
												    	PolicyNumberfileName =listOfFiles[i].getName();
												    	GeneratedPath = GeneratedPath+"\\"+PolicyNumberfileName;
												    	i=listOfFiles.length+1;
												    }
												  } 
												 
											//	}
												
											System.out.println(" Full Path is ::"+GeneratedPath);
											
											GeneratedPath= GeneratedPath.replace("\\","\\\\");
											
											
											// ALL The API FOR  COMPARING IMAGE
											
											String url = "http://10.1.104.229/compare/index.php";
									    	CloseableHttpClient httpClient = HttpClients.createDefault();
											HttpPost httppost = new HttpPost(url); //http post request
											
											// passing json as a string 
											String entityString ="{\"image1\":\""+BaseImageforPremNotice+"\" ,\"image2\": \""+GeneratedPath+"\",\"coordinates\": \"163,38,304,83|19,218,784,697|648,742,783,809|126,817,233,842|67,904,346,986|68,995,774,1031\"}\r\n"
													+ "";
											System.out.println("EntityString::"+entityString);
											try {
												httppost.setEntity(new StringEntity(entityString));
												CloseableHttpResponse closebaleHttpResponse;
												closebaleHttpResponse = httpClient.execute(httppost);
												int statusCode = closebaleHttpResponse.getStatusLine().getStatusCode();
												System.out.println("Status from API:"+statusCode);
												// convert json to string 	
												String responseString = EntityUtils.toString(closebaleHttpResponse.getEntity(), "UTF-8");
												System.out.println("Response from API :"+responseString);
												
												JSONObject response = new JSONObject(responseString);
												System.out.println("Status:"+response.get("msg"));
												if(response.getString("msg").equalsIgnoreCase("Something went wrong in image2")) {
													writeToCSV("Company","CLGR HEALTH REMIDER NOTICE");
													writeToCSV("Image compared ", "NO CLGR HEALTH REMIDER NOTICE ");
													writeToCSV("Image Result After comparision", "NULL");
												}
												System.out.println("Image : "+response.getString("image_difference_link"));
												System.out.println("Both the image are same "+response.getBoolean("both_are_same"));
												writeToCSV("Company","CLGR HEALTH REMIDER NOTICE");
												writeToCSV("Image compared ", PolicyNumberfileName);
												writeToCSV("Image Result After comparision", response.getString("image_difference_link"));
												if(response.getBoolean("both_are_same")== true) {
													writeToCSV("Resut", "Both are same");
												}
												else {
													writeToCSV("Resut", "Both are not same");
												}
												
												
											} catch (ClientProtocolException e) {
												// TODO Auto-generated catch block
												e.printStackTrace();
											} catch (IOException e) {
												// TODO Auto-generated catch block
												e.printStackTrace();
											}
										
											
											}
											else {
												
												writeToCSV("Company","CLGR  HEALTH REMINDER  NOTICE");
												writeToCSV("Image compared ", "NO CLGR POLICY FOR HEATH REMINDER NOTICE ");
												writeToCSV("Image Result After comparision", "NULL");
												
												
											}
											
											
						 
				 
				 
		 
		 ;} 
		 catch(Exception e) { PrintVal("Renderer Script Exception : "+e.toString()); } 
		 PrintVal("Executed Step = CALLRENDERERSCRIPT,COMPAREIMAGECLGRHEALTHREMINDER"); 
		try { 
		  
				  
						 		    	
								    	//  COMPANY OF Texas (CLGR) LAPSE NOTICE  FOR HEALTH
								    	
									 Boolean policyFound =false; 
							       var_Date= getJsonData(var_CSVData , "$.records["+var_Count+"].Date");
							         String PolicyNumber ="";
							         String PolicyNumberfileName ="";
							         String day="";
							        
							    	 String array[] = var_Date.split("/");
									 System.out.println("1st"+array[0].length());
									 if(array[0].length()==1) {
										 array[0]="0"+array[0];
										 System.out.println("after adding"+array[0]);
									 }
									 if(array[1].length()==1) {
										 array[1]="0"+array[1];
										 System.out.println("after adding"+array[1]);
									 }
									 int dayint =Integer.parseInt(array[1]);
									 day = array[1];
										/*
										 * if(dayint<31) { dayint =dayint +1; day=String.valueOf(dayint);
										 * if(day.length()==1) { day ="0"+day; } }
										 */
									 String var_DateforDB = array[0]+"/"+array[1]+"/"+array[2];
									  
									 var_Date = array[2]+"-"+array[0]+"-"+day;
									 System.out.println("a: "+var_Date);
										
											
											// BaseLined Image for CompanyTaxes (LapseNOTICE LIFE)
											
											String BaseImageforPremNotice ="C:\\/GIAS_Automation\\/Images\\/CLNW\\/CLIC_CGRINDBILL_LAPSE.TIF";
											String GeneratedPath ="\\\\gvlfp04\\printreview\\CLIC_CGRINDBILLM_TIFs_UAT1";
											
											// Create  folder Name by Appending Date  
											
											String FolderPrefix ="CLIC_CGRINDBILL_UAT1_"+var_Date;
											
											GeneratedPath = GeneratedPath+"\\"+FolderPrefix;
											
											
											System.out.println("GenereatedPath ::"+GeneratedPath);
											
											
											java.sql.Connection con = null;   

									        // Load SQL Server JDBC driver and establish connection UAT.
									        String connectionUrl = "jdbc:sqlserver://CIS-GIASDBT2:443" + "3;" +
									                "databaseName=CLIS1DT" + "A;"
									                +
									                "IntegratedSecurity = true";
									        System.out.print("Connecting to SQL Server ... ");
									        try {
												con = DriverManager.getConnection(connectionUrl);
											} catch (SQLException e) {
												// TODO Auto-generated catch block
												e.printStackTrace();
											}
								            System.out.println("Connected to database.");
								            
								           System.out.println("POLICY NUMBER ::::::"+PolicyNumber);
								            
								            String sql1primCL ="Use clis1dta "+
								            		  "Select c.field1, c.company,c.insuranceType, p.noticeType, p.company, p.controlNumber, p.noticeNumber, p.serialNumber, p.insuredName, p.policynumber,"+
								            		 "p.policyNumber, p.amount, p.dueDate, a.accountName, a.accountNameAddress2, a.accountNameAddress4 "+
								            		"from iios.BillingCombinedPol p join iios.BillingCombinedAddr a on p.company = a.company and p.controlNumber = a.controlNumber "+
								            		"and p.noticeNumber = a.noticeNumber "+
								            		"join iios.BillingCombined c on p.company = c.company and p.controlNumber = c.controlNumber and p.noticeNumber = c.noticeNumber "+
								            		"where "+
								            		"field1 = '"+var_DateforDB+"' "+
								            		"and c.company= 'CLGR' "+
								            		"and p.noticeType ='3' "+
								            		"and insuranceType ='H'"+
								            		"order by controlNumber, field1,p.noticeNumber, p.serialNumber";
								            			
								            System.out.println(sql1primCL);
								            Statement statement;
											try {
												statement = con.createStatement();
											
											ResultSet rs = null;
											statement.setMaxRows(1);
											System.out.println("EXECUTED");
											rs = statement.executeQuery(sql1primCL);
											 while (rs.next()) {
												 
												  PolicyNumber =rs.getString("policyNumber");
												 
												  System.out.println("PolicyNumber::"+PolicyNumber);
												  
												  if(PolicyNumber.equalsIgnoreCase("Null")) {
													    policyFound = false ;
												  }
												  else {
													  policyFound = true;
												  }
									            }
											
											 statement.close();
											}
											 catch (SQLException e) {
													// TODO Auto-generated catch block
													e.printStackTrace();
												}
											
											if(policyFound ==true) {
											 
											// List of file names in the folder 
												
											// List of file names in the folder 
												
												File folder = new File(GeneratedPath);
												File[] listOfFiles = folder.listFiles();
												
												System.out.println("Lenght ::"+listOfFiles.length);

												for (int i = 0; i < listOfFiles.length; i++) {
												 // if (listOfFiles[i].isFile()) {
												    System.out.println("File " + listOfFiles[i].getName());
												    System.out.println("POLICY ::"+PolicyNumber);
												    String imagname = listOfFiles[i].getName().trim();
												    System.out.println("Condition:"+imagname.toLowerCase().contains(PolicyNumber.toLowerCase()));
												    System.out.println("imagname"+imagname);
												    if(imagname.toLowerCase().contains(PolicyNumber.trim().toLowerCase())) { 	
												    	PolicyNumberfileName =listOfFiles[i].getName();
												    	GeneratedPath = GeneratedPath+"\\"+PolicyNumberfileName;
												    	i=listOfFiles.length+1;
												    }
												  } 
												 
											//	}
												
												
												
											System.out.println(" Full Path is ::"+GeneratedPath);
											
											GeneratedPath= GeneratedPath.replace("\\","\\\\");
											
											
											// ALL The API FOR  COMPARING IMAGE
											
											String url = "http://10.1.104.229/compare/index.php";
									    	CloseableHttpClient httpClient = HttpClients.createDefault();
											HttpPost httppost = new HttpPost(url); //http post request
											
											// passing json as a string 
											String entityString ="{\"image1\":\""+BaseImageforPremNotice+"\" ,\"image2\": \""+GeneratedPath+"\",\"coordinates\": \"187,38,286,109|618,115,755,155|13,221,802,640|648,742,783,809|126,817,233,842|67,904,346,986|68,995,774,1031\"}\r\n"
													+ "";
											System.out.println("EntityString::"+entityString);
											try {
												httppost.setEntity(new StringEntity(entityString));
												CloseableHttpResponse closebaleHttpResponse;
												closebaleHttpResponse = httpClient.execute(httppost);
												int statusCode = closebaleHttpResponse.getStatusLine().getStatusCode();
												System.out.println("Status from API:"+statusCode);
												// convert json to string 	
												String responseString = EntityUtils.toString(closebaleHttpResponse.getEntity(), "UTF-8");
												System.out.println("Response from API :"+responseString);
												
												JSONObject response = new JSONObject(responseString);
												if(response.getString("msg").equalsIgnoreCase("Something went wrong in image2")) {
													writeToCSV("Company","CLGR HEALTH LAPSE NOTICE");
													writeToCSV("Image compared ", "NO CLGR HEALTH LAPSE NOTICE ");
													writeToCSV("Image Result After comparision", "NULL");
												}
												
												System.out.println("Image : "+response.getString("image_difference_link"));
												System.out.println("Both the image are same "+response.getBoolean("both_are_same"));
												writeToCSV("Company","CLGR HEALTH LAPSE NOTICE");
												writeToCSV("Image compared ", PolicyNumberfileName);
												writeToCSV("Image Result After comparision", response.getString("image_difference_link"));
												if(response.getBoolean("both_are_same")== true) {
													writeToCSV("Resut", "Both are same");
												}
												else {
													writeToCSV("Resut", "Both are not same");
												}
												
												
											} catch (ClientProtocolException e) {
												// TODO Auto-generated catch block
												e.printStackTrace();
											} catch (IOException e) {
												// TODO Auto-generated catch block
												e.printStackTrace();
											}
										
											
											}
											else {
												
												writeToCSV("Company","CLGR HEALTH LAPSENOTICE");
												writeToCSV("Image compared ", "NO CLGR POLICY FOR LAPSENOTICE ");
												writeToCSV("Image Result After comparision", "NULL");
												
												
											}
											
											
											
						 
						 
		 
		 ;} 
		 catch(Exception e) { PrintVal("Renderer Script Exception : "+e.toString()); } 
		 PrintVal("Executed Step = CALLRENDERERSCRIPT,COMPAREIMAGECLGRHEALTHLAPESNOTICE"); 

    }


    @Test
    public void clicregcompareimageclnc() {

        // No need to declare / print method name here, handled by TestNg listener
        // declare("clicregcompareimageclnc");

		int var_Count = 0;
                 LOGGER.info("Executed Step = VAR,Integer,var_Count,0,TGTYPESCREENREG");
		com.jayway.jsonpath.ReadContext var_CSVData = getJsonData("C:/GIAS_Automation/CLIC Regression/QUA1/InputData/CLIC36_IP_CompareImages.csv");
                 LOGGER.info("Executed Step = VAR,Json,var_CSVData,C:/GIAS_Automation/CLIC Regression/QUA1/InputData/CLIC36_IP_CompareImages.csv,TGTYPESCREENREG");
		String var_Date;
                 LOGGER.info("Executed Step = VAR,String,var_Date,TGTYPESCREENREG");
		var_Date = getJsonData(var_CSVData , "$.records["+var_Count+"].Date");
                 LOGGER.info("Executed Step = STORE,var_Date,var_CSVData,$.records[{var_Count}].Date,TGTYPESCREENREG");
		try { 
		  
				  
						    	
								    	//  COMPANY OF Texas (CL NC) PREMIUM NOTICE FOR HEATH
								    	
									 Boolean policyFound =false; 
							        // var_Date= getJsonData(var_CSVData , "$.records["+var_Count+"].Date");
							         String PolicyNumber ="";
							         String PolicyNumberfileName ="";
							         String day="";
							        
							    	 String array[] = var_Date.split("/");
									 System.out.println("1st"+array[0].length());
									 if(array[0].length()==1) {
										 array[0]="0"+array[0];
										 System.out.println("after adding"+array[0]);
									 }
									 if(array[1].length()==1) {
										 array[1]="0"+array[1];
										 System.out.println("after adding"+array[1]);
									 }
									 int dayint =Integer.parseInt(array[1]);
									 day = array[1];
										/*
										 * if(dayint<31) { dayint =dayint +1; day=String.valueOf(dayint);
										 * if(day.length()==1) { day ="0"+day; } }
										 */
											 
									 String var_DateforDB = array[0]+"/"+array[1]+"/"+array[2];
									  
									 var_Date = array[2]+"-"+array[0]+"-"+day;
									 System.out.println("a: "+var_Date);
										
											
											// BaseLined Image for CLNW (PremNotice) for health
											
											String BaseImageforPremNotice ="C:\\/GIAS_Automation\\/Images\\/CLNC\\/CLIC_CNCINDBILL_PREMIUM.TIF";
											String GeneratedPath ="\\\\gvlfp04\\printreview\\CLIC_CNCINDBILLM_TIFs_UAT1";
											
											// Create  folder Name by Appending Date  
											
											String FolderPrefix ="CLIC_CNCINDBILL_UAT1_"+var_Date;
											
											GeneratedPath = GeneratedPath+"\\"+FolderPrefix;
											
											
											System.out.println("GenereatedPath ::"+GeneratedPath);
											
											
											java.sql.Connection con = null;   

									        // Load SQL Server JDBC driver and establish connection UAT.
									        String connectionUrl = "jdbc:sqlserver://CIS-GIASDBT2:443" + "3;" +
									                "databaseName=CLIS1DT" + "A;"
									                +
									                "IntegratedSecurity = true";
									        System.out.print("Connecting to SQL Server ... ");
									        try {
												con = DriverManager.getConnection(connectionUrl);
											} catch (SQLException e) {
												// TODO Auto-generated catch block
												e.printStackTrace();
											}
								            System.out.println("Connected to database.");
								            
								            String sql1primCL ="Use clis1dta "+
								            		  "Select c.field1, c.company,c.insuranceType, p.noticeType, p.company, p.controlNumber, p.noticeNumber, p.serialNumber, p.insuredName, p.policynumber,"+
								            		 "p.policyNumber, p.amount, p.dueDate, a.accountName, a.accountNameAddress2, a.accountNameAddress4 "+
								            		"from iios.BillingCombinedPol p join iios.BillingCombinedAddr a on p.company = a.company and p.controlNumber = a.controlNumber "+
								            		"and p.noticeNumber = a.noticeNumber "+
								            		"join iios.BillingCombined c on p.company = c.company and p.controlNumber = c.controlNumber and p.noticeNumber = c.noticeNumber "+
								            		"where "+
								            		"field1 = '"+var_DateforDB+"' "+
								            		"and c.company= 'CLNC' "+
								            		"and p.noticeType ='1' "+
								            		"and insuranceType ='H'"+
								            		"order by controlNumber, field1,p.noticeNumber, p.serialNumber";
								            			
								            System.out.println(sql1primCL);
								            Statement statement;
											try {
												statement = con.createStatement();
											
											ResultSet rs = null;
											statement.setMaxRows(1);
											System.out.println("EXECUTED");
											rs = statement.executeQuery(sql1primCL);
											 while (rs.next()) {
												 
												  PolicyNumber =rs.getString("policyNumber");
												 
												  System.out.println("PolicyNumber::"+PolicyNumber);
												  
												  if(PolicyNumber.equalsIgnoreCase("Null")) {
													    policyFound = false ;
												  }
												  else {
													  policyFound = true;
												  }
									            }
											
											 statement.close();
											}
											 catch (SQLException e) {
													// TODO Auto-generated catch block
													e.printStackTrace();
												}
											
											if(policyFound ==true) {
											 
											// List of file names in the folder 
												
											// List of file names in the folder 
												
												File folder = new File(GeneratedPath);
												File[] listOfFiles = folder.listFiles();
												
												System.out.println("Lenght ::"+listOfFiles.length);

												for (int i = 0; i < listOfFiles.length; i++) {
												 // if (listOfFiles[i].isFile()) {
												    System.out.println("File " + listOfFiles[i].getName());
												    System.out.println("POLICY ::"+PolicyNumber);
												    String imagname = listOfFiles[i].getName().trim();
												    System.out.println("Condition:"+imagname.toLowerCase().contains(PolicyNumber.toLowerCase()));
												    System.out.println("imagname"+imagname);
												    if(imagname.toLowerCase().contains(PolicyNumber.trim().toLowerCase())) { 	
												    	PolicyNumberfileName =listOfFiles[i].getName();
												    	GeneratedPath = GeneratedPath+"\\"+PolicyNumberfileName;
												    	i=listOfFiles.length+1;
												    }
												  } 
												 
											//	}
												
												
											System.out.println(" Full Path is ::"+GeneratedPath);
											
											GeneratedPath= GeneratedPath.replace("\\","\\\\");
											
											
											// ALL The API FOR  COMPARING IMAGE
											
											String url = "http://10.1.104.229/compare/index.php";
									    	CloseableHttpClient httpClient = HttpClients.createDefault();
											HttpPost httppost = new HttpPost(url); //http post request
											
											// passing json as a string 
											String entityString ="{\"image1\":\""+BaseImageforPremNotice+"\" ,\"image2\": \""+GeneratedPath+"\",\"coordinates\": \"15,218,779,703|188,35,282,76|652,740,775,790|126,812,253,844|96,907,336,980|68,998,768,1025\"}\r\n"
													+ "";
											System.out.println("EntityString::"+entityString);
											try {
												httppost.setEntity(new StringEntity(entityString));
												CloseableHttpResponse closebaleHttpResponse;
												closebaleHttpResponse = httpClient.execute(httppost);
												int statusCode = closebaleHttpResponse.getStatusLine().getStatusCode();
												System.out.println("Status from API:"+statusCode);
												// convert json to string 	
												String responseString = EntityUtils.toString(closebaleHttpResponse.getEntity(), "UTF-8");
												System.out.println("Response from API :"+responseString);
												
												JSONObject response = new JSONObject(responseString);
												System.out.println("Status:"+response.get("msg"));
												if(response.getString("msg").equalsIgnoreCase("Something went wrong in image2")) {
													writeToCSV("Company","CLNC HEATH PREMIUM");
													writeToCSV("Image compared ", "NO CLNC POLICY FOR PREMIUM ");
													writeToCSV("Image Result After comparision", "NULL");
												}
												System.out.println("Image : "+response.getString("image_difference_link"));
												System.out.println("Both the image are same "+response.getBoolean("both_are_same"));
												writeToCSV("Company","CLNC  HEALTH PRIMIUM");
												writeToCSV("Image compared ", PolicyNumberfileName);
												writeToCSV("Image Result After comparision", response.getString("image_difference_link"));
												if(response.getBoolean("both_are_same")== true) {
													writeToCSV("Resut", "Both are same");
												}
												else {
													writeToCSV("Resut", "Both are not same");
													Assert.assertTrue(false,"Comparsion failed");
												}
												
												
											} catch (ClientProtocolException e) {
												// TODO Auto-generated catch block
												
											} catch (IOException e) {
												// TODO Auto-generated catch block
												
												e.printStackTrace();
												e.printStackTrace();
											}
										
											
											}
											else {
												
												writeToCSV("Company","CLNC HEATH PREMIUM");
												writeToCSV("Image compared ", "NO CLNC POLICY FOR PREMIUM ");
												writeToCSV("Image Result After comparision", "NULL");
												
												
											}
											
											
						 
		 
		 ;} 
		 catch(Exception e) { PrintVal("Renderer Script Exception : "+e.toString()); } 
		 PrintVal("Executed Step = CALLRENDERERSCRIPT,COMAPAREIMAGECLNCHEALTHPREMIUM"); 
		try { 
		  
				  
						     	
								    	//  COMPANY OF Texas (CL NW) REMINDER NOTICE  FOR HEALTH
								    	
								    	 Boolean policyFound =false; 
								         var_Date= getJsonData(var_CSVData , "$.records["+var_Count+"].Date");
								         String PolicyNumber ="";
								         String PolicyNumberfileName ="";
								         String day="";
								        
								    	 String array[] = var_Date.split("/");
										 System.out.println("1st"+array[0].length());
										 if(array[0].length()==1) {
											 array[0]="0"+array[0];
											 System.out.println("after adding"+array[0]);
										 }
										 if(array[1].length()==1) {
											 array[1]="0"+array[1];
											 System.out.println("after adding"+array[1]);
										 }
										 int dayint =Integer.parseInt(array[1]);
										 day = array[1];
											/*
											 * if(dayint<31) { dayint =dayint +1; day=String.valueOf(dayint);
											 * if(day.length()==1) { day ="0"+day; } }
											 */
												 
										 String var_DateforDB = array[0]+"/"+array[1]+"/"+array[2];
										  
										 var_Date = array[2]+"-"+array[0]+"-"+day;
										 System.out.println("a: "+var_Date);
								    	
										
											
											// BaseLined Image for CompanyTaxes (REMINDER NOTICE)
											
											String BaseImageforPremNotice ="C:\\/GIAS_Automation\\/Images\\/CLNC\\/CLIC_CNCINDBILL_REMINDER.TIF";
											String GeneratedPath ="\\\\gvlfp04\\printreview\\CLIC_CNCINDBILLM_TIFs_UAT1";
											
											// Create  folder Name by Appending Date  
											
											String FolderPrefix ="CLIC_CNCINDBILL_UAT1_"+var_Date;
											
											GeneratedPath = GeneratedPath+"\\"+FolderPrefix;
											
											
											System.out.println("GenereatedPath ::"+GeneratedPath);
											
											
											java.sql.Connection con = null;   

									        // Load SQL Server JDBC driver and establish connection UAT.
									        String connectionUrl = "jdbc:sqlserver://CIS-GIASDBT2:443" + "3;" +
									                "databaseName=CLIS1DT" + "A;"
									                +
									                "IntegratedSecurity = true";
									        System.out.print("Connecting to SQL Server ... ");
									        try {
												con = DriverManager.getConnection(connectionUrl);
											} catch (SQLException e) {
												// TODO Auto-generated catch block
												e.printStackTrace();
											}
								            System.out.println("Connected to database.");
								            
								            String sql1primCL ="Use clis1dta "+
								            		  "Select c.field1, c.company,c.insuranceType, p.noticeType, p.company, p.controlNumber, p.noticeNumber, p.serialNumber, p.insuredName, p.policynumber,"+
								            		 "p.policyNumber, p.amount, p.dueDate, a.accountName, a.accountNameAddress2, a.accountNameAddress4 "+
								            		"from iios.BillingCombinedPol p join iios.BillingCombinedAddr a on p.company = a.company and p.controlNumber = a.controlNumber "+
								            		"and p.noticeNumber = a.noticeNumber "+
								            		"join iios.BillingCombined c on p.company = c.company and p.controlNumber = c.controlNumber and p.noticeNumber = c.noticeNumber "+
								            		"where "+
								            		"field1 = '"+var_DateforDB+"' "+
								            		"and c.company= 'CLNC' "+
								            		"and p.noticeType ='2' "+
								            		"and insuranceType ='H' "+
								            		"order by controlNumber, field1,p.noticeNumber, p.serialNumber";
								            			
								            System.out.println(sql1primCL);
								            Statement statement;
											try {
												statement = con.createStatement();
											
											ResultSet rs = null;
											statement.setMaxRows(1);
											System.out.println("EXECUTED");
											rs = statement.executeQuery(sql1primCL);
											 while (rs.next()) {
												 
												  PolicyNumber =rs.getString("policyNumber");
												 
												  System.out.println("PolicyNumber::"+PolicyNumber);
												  
												  if(PolicyNumber.equalsIgnoreCase("Null")) {
													    policyFound = false ;
												  }
												  else {
													  policyFound = true;
												  }
									            }
											
											 statement.close();
											}
											 catch (SQLException e) {
													// TODO Auto-generated catch block
													e.printStackTrace();
												}
											
											
											System.out.println("POLICY FOUND"+policyFound);
											if(policyFound==true) {
											 
											// List of file names in the folder 
												
												File folder = new File(GeneratedPath);
												File[] listOfFiles = folder.listFiles();
												
												System.out.println("Lenght ::"+listOfFiles.length);

												for (int i = 0; i < listOfFiles.length; i++) {
												 // if (listOfFiles[i].isFile()) {
												    System.out.println("File " + listOfFiles[i].getName());
												    System.out.println("POLICY ::"+PolicyNumber);
												    String imagname = listOfFiles[i].getName().trim();
												    System.out.println("Condition:"+imagname.toLowerCase().contains(PolicyNumber.toLowerCase()));
												    System.out.println("imagname"+imagname);
												    if(imagname.toLowerCase().contains(PolicyNumber.trim().toLowerCase())) { 	
												    	PolicyNumberfileName =listOfFiles[i].getName();
												    	GeneratedPath = GeneratedPath+"\\"+PolicyNumberfileName;
												    	i=listOfFiles.length+1;
												    }
												  } 
												 
											//	}
												
											System.out.println(" Full Path is ::"+GeneratedPath);
											
											GeneratedPath= GeneratedPath.replace("\\","\\\\");
											
											
											// ALL The API FOR  COMPARING IMAGE
											
											String url = "http://10.1.104.229/compare/index.php";
									    	CloseableHttpClient httpClient = HttpClients.createDefault();
											HttpPost httppost = new HttpPost(url); //http post request
											
											// passing json as a string 
											String entityString ="{\"image1\":\""+BaseImageforPremNotice+"\" ,\"image2\": \""+GeneratedPath+"\",\"coordinates\": \"163,38,304,83|19,218,784,697|648,742,783,809|126,817,233,842|67,904,346,986|68,995,774,1031\"}\r\n"
													+ "";
											System.out.println("EntityString::"+entityString);
											try {
												httppost.setEntity(new StringEntity(entityString));
												CloseableHttpResponse closebaleHttpResponse;
												closebaleHttpResponse = httpClient.execute(httppost);
												int statusCode = closebaleHttpResponse.getStatusLine().getStatusCode();
												System.out.println("Status from API:"+statusCode);
												// convert json to string 	
												String responseString = EntityUtils.toString(closebaleHttpResponse.getEntity(), "UTF-8");
												System.out.println("Response from API :"+responseString);
												
												JSONObject response = new JSONObject(responseString);
												System.out.println("Status:"+response.get("msg"));
												if(response.getString("msg").equalsIgnoreCase("Something went wrong in image2")) {
													writeToCSV("Company","CLNC HEALTH REMIDER NOTICE");
													writeToCSV("Image compared ", "NO CLNC HEALTH REMIDER NOTICE ");
													writeToCSV("Image Result After comparision", "NULL");
												}
												System.out.println("Image : "+response.getString("image_difference_link"));
												System.out.println("Both the image are same "+response.getBoolean("both_are_same"));
												writeToCSV("Company","CLNC HEALTH REMIDER NOTICE");
												writeToCSV("Image compared ", PolicyNumberfileName);
												writeToCSV("Image Result After comparision", response.getString("image_difference_link"));
												if(response.getBoolean("both_are_same")== true) {
													writeToCSV("Resut", "Both are same");
												}
												else {
													writeToCSV("Resut", "Both are not same");
												}
												
												
											} catch (ClientProtocolException e) {
												// TODO Auto-generated catch block
												e.printStackTrace();
											} catch (IOException e) {
												// TODO Auto-generated catch block
												e.printStackTrace();
											}
										
											
											}
											else {
												
												writeToCSV("Company","CLNC  HEALTH REMINDER  NOTICE");
												writeToCSV("Image compared ", "NO CLNC POLICY FOR HEATH REMINDER NOTICE ");
												writeToCSV("Image Result After comparision", "NULL");
												
												
											}
											
											
						 
				
		 
		 ;} 
		 catch(Exception e) { PrintVal("Renderer Script Exception : "+e.toString()); } 
		 PrintVal("Executed Step = CALLRENDERERSCRIPT,COMPAREIMAGECLNCHEALTHREMINDER"); 
		try { 
		  
				  
						 		    	
								    	//  COMPANY OF Texas (CL NC) LAPSE NOTICE  FOR HEALTH
								    	
									 Boolean policyFound =false; 
							       var_Date= getJsonData(var_CSVData , "$.records["+var_Count+"].Date");
							         String PolicyNumber ="";
							         String PolicyNumberfileName ="";
							         String day="";
							        
							    	 String array[] = var_Date.split("/");
									 System.out.println("1st"+array[0].length());
									 if(array[0].length()==1) {
										 array[0]="0"+array[0];
										 System.out.println("after adding"+array[0]);
									 }
									 if(array[1].length()==1) {
										 array[1]="0"+array[1];
										 System.out.println("after adding"+array[1]);
									 }
									 int dayint =Integer.parseInt(array[1]);
									 day = array[1];
										/*
										 * if(dayint<31) { dayint =dayint +1; day=String.valueOf(dayint);
										 * if(day.length()==1) { day ="0"+day; } }
										 */
									 String var_DateforDB = array[0]+"/"+array[1]+"/"+array[2];
									  
									 var_Date = array[2]+"-"+array[0]+"-"+day;
									 System.out.println("a: "+var_Date);
										
											
											// BaseLined Image for CompanyTaxes (LapseNOTICE LIFE)
											
											String BaseImageforPremNotice ="C:\\/GIAS_Automation\\/Images\\/CLNC\\/CLIC_CNCINDBILL_LAPSE.TIF";
											String GeneratedPath ="\\\\gvlfp04\\printreview\\CLIC_CNCINDBILLM_TIFs_UAT1";
											
											// Create  folder Name by Appending Date  
											
											String FolderPrefix ="CLIC_CNCINDBILL_UAT1_"+var_Date;
											
											GeneratedPath = GeneratedPath+"\\"+FolderPrefix;
											
											
											System.out.println("GenereatedPath ::"+GeneratedPath);
											
											
											java.sql.Connection con = null;   

									        // Load SQL Server JDBC driver and establish connection UAT.
									        String connectionUrl = "jdbc:sqlserver://CIS-GIASDBT2:443" + "3;" +
									                "databaseName=CLIS1DT" + "A;"
									                +
									                "IntegratedSecurity = true";
									        System.out.print("Connecting to SQL Server ... ");
									        try {
												con = DriverManager.getConnection(connectionUrl);
											} catch (SQLException e) {
												// TODO Auto-generated catch block
												e.printStackTrace();
											}
								            System.out.println("Connected to database.");
								            
								           System.out.println("POLICY NUMBER ::::::"+PolicyNumber);
								            
								            String sql1primCL ="Use clis1dta "+
								            		  "Select c.field1, c.company,c.insuranceType, p.noticeType, p.company, p.controlNumber, p.noticeNumber, p.serialNumber, p.insuredName, p.policynumber,"+
								            		 "p.policyNumber, p.amount, p.dueDate, a.accountName, a.accountNameAddress2, a.accountNameAddress4 "+
								            		"from iios.BillingCombinedPol p join iios.BillingCombinedAddr a on p.company = a.company and p.controlNumber = a.controlNumber "+
								            		"and p.noticeNumber = a.noticeNumber "+
								            		"join iios.BillingCombined c on p.company = c.company and p.controlNumber = c.controlNumber and p.noticeNumber = c.noticeNumber "+
								            		"where "+
								            		"field1 = '"+var_DateforDB+"' "+
								            		"and c.company= 'CLNC' "+
								            		"and p.noticeType ='3' "+
								            		"and insuranceType ='H'"+
								            		"order by controlNumber, field1,p.noticeNumber, p.serialNumber";
								            			
								            System.out.println(sql1primCL);
								            Statement statement;
											try {
												statement = con.createStatement();
											
											ResultSet rs = null;
											statement.setMaxRows(1);
											System.out.println("EXECUTED");
											rs = statement.executeQuery(sql1primCL);
											 while (rs.next()) {
												 
												  PolicyNumber =rs.getString("policyNumber");
												 
												  System.out.println("PolicyNumber::"+PolicyNumber);
												  
												  if(PolicyNumber.equalsIgnoreCase("Null")) {
													    policyFound = false ;
												  }
												  else {
													  policyFound = true;
												  }
									            }
											
											 statement.close();
											}
											 catch (SQLException e) {
													// TODO Auto-generated catch block
													e.printStackTrace();
												}
											
											if(policyFound ==true) {
											 
											// List of file names in the folder 
												
											// List of file names in the folder 
												
												File folder = new File(GeneratedPath);
												File[] listOfFiles = folder.listFiles();
												
												System.out.println("Lenght ::"+listOfFiles.length);

												for (int i = 0; i < listOfFiles.length; i++) {
												 // if (listOfFiles[i].isFile()) {
												    System.out.println("File " + listOfFiles[i].getName());
												    System.out.println("POLICY ::"+PolicyNumber);
												    String imagname = listOfFiles[i].getName().trim();
												    System.out.println("Condition:"+imagname.toLowerCase().contains(PolicyNumber.toLowerCase()));
												    System.out.println("imagname"+imagname);
												    if(imagname.toLowerCase().contains(PolicyNumber.trim().toLowerCase())) { 	
												    	PolicyNumberfileName =listOfFiles[i].getName();
												    	GeneratedPath = GeneratedPath+"\\"+PolicyNumberfileName;
												    	i=listOfFiles.length+1;
												    }
												  } 
												 
											//	}
												
												
												
											System.out.println(" Full Path is ::"+GeneratedPath);
											
											GeneratedPath= GeneratedPath.replace("\\","\\\\");
											
											
											// ALL The API FOR  COMPARING IMAGE
											
											String url = "http://10.1.104.229/compare/index.php";
									    	CloseableHttpClient httpClient = HttpClients.createDefault();
											HttpPost httppost = new HttpPost(url); //http post request
											
											// passing json as a string 
											String entityString ="{\"image1\":\""+BaseImageforPremNotice+"\" ,\"image2\": \""+GeneratedPath+"\",\"coordinates\": \"187,38,286,109|618,115,755,155|13,221,802,640|648,742,783,809|126,817,233,842|67,904,346,986|68,995,774,1031\"}\r\n"
													+ "";
											System.out.println("EntityString::"+entityString);
											try {
												httppost.setEntity(new StringEntity(entityString));
												CloseableHttpResponse closebaleHttpResponse;
												closebaleHttpResponse = httpClient.execute(httppost);
												int statusCode = closebaleHttpResponse.getStatusLine().getStatusCode();
												System.out.println("Status from API:"+statusCode);
												// convert json to string 	
												String responseString = EntityUtils.toString(closebaleHttpResponse.getEntity(), "UTF-8");
												System.out.println("Response from API :"+responseString);
												
												JSONObject response = new JSONObject(responseString);
												if(response.getString("msg").equalsIgnoreCase("Something went wrong in image2")) {
													writeToCSV("Company","CLNC HEALTH LAPSE NOTICE");
													writeToCSV("Image compared ", "NO CLNC HEALTH LAPSE NOTICE ");
													writeToCSV("Image Result After comparision", "NULL");
												}
												
												System.out.println("Image : "+response.getString("image_difference_link"));
												System.out.println("Both the image are same "+response.getBoolean("both_are_same"));
												writeToCSV("Company","CLNC HEALTH LAPSE NOTICE");
												writeToCSV("Image compared ", PolicyNumberfileName);
												writeToCSV("Image Result After comparision", response.getString("image_difference_link"));
												if(response.getBoolean("both_are_same")== true) {
													writeToCSV("Resut", "Both are same");
												}
												else {
													writeToCSV("Resut", "Both are not same");
												}
												
												
											} catch (ClientProtocolException e) {
												// TODO Auto-generated catch block
												e.printStackTrace();
											} catch (IOException e) {
												// TODO Auto-generated catch block
												e.printStackTrace();
											}
										
											
											}
											else {
												
												writeToCSV("Company","CLNC HEALTH LAPSENOTICE");
												writeToCSV("Image compared ", "NO CLNC POLICY FOR LAPSENOTICE ");
												writeToCSV("Image Result After comparision", "NULL");
												
												
											}
											
											
											
						 
						 
		 
		 ;} 
		 catch(Exception e) { PrintVal("Renderer Script Exception : "+e.toString()); } 
		 PrintVal("Executed Step = CALLRENDERERSCRIPT,COMPAREIMAGECLNCHEALTHLAPSE"); 

    }


    @Test
    public void clicregcompareimageclpr() {

        // No need to declare / print method name here, handled by TestNg listener
        // declare("clicregcompareimageclpr");

		int var_Count = 0;
                 LOGGER.info("Executed Step = VAR,Integer,var_Count,0,TGTYPESCREENREG");
		com.jayway.jsonpath.ReadContext var_CSVData = getJsonData("C:/GIAS_Automation/CLIC Regression/QUA1/InputData/CLIC36_IP_CompareImages.csv");
                 LOGGER.info("Executed Step = VAR,Json,var_CSVData,C:/GIAS_Automation/CLIC Regression/QUA1/InputData/CLIC36_IP_CompareImages.csv,TGTYPESCREENREG");
		String var_Date;
                 LOGGER.info("Executed Step = VAR,String,var_Date,TGTYPESCREENREG");
		var_Date = getJsonData(var_CSVData , "$.records["+var_Count+"].Date");
                 LOGGER.info("Executed Step = STORE,var_Date,var_CSVData,$.records[{var_Count}].Date,TGTYPESCREENREG");
		try { 
		  
				  
						    	
								    	//  COMPANY OF Texas (CL PR) PREMIUM NOTICE FOR LIFE
								    	
									 Boolean policyFound =false; 
							        // var_Date= getJsonData(var_CSVData , "$.records["+var_Count+"].Date");
							         String PolicyNumber ="";
							         String PolicyNumberfileName ="";
							         String day="";
							        
							    	 String array[] = var_Date.split("/");
									 System.out.println("1st"+array[0].length());
									 if(array[0].length()==1) {
										 array[0]="0"+array[0];
										 System.out.println("after adding"+array[0]);
									 }
									 if(array[1].length()==1) {
										 array[1]="0"+array[1];
										 System.out.println("after adding"+array[1]);
									 }
									 int dayint =Integer.parseInt(array[1]);
									 day = array[1];
										
										 if(dayint<31) { 
											 dayint =dayint +1;
											 day=String.valueOf(dayint);
										  if(day.length()==1) {
											  day ="0"+day; 
											  } }
										 
											 
									 String var_DateforDB = array[0]+"/"+array[1]+"/"+array[2];
									  
									 var_Date = array[2]+"-"+array[0]+"-"+day;
									 System.out.println("a: "+var_Date);
										
											
											// BaseLined Image for CLNW (PremNotice) for health
											
											String BaseImageforPremNotice ="C:\\/GIAS_Automation\\/Images\\/CLPR\\/Life\\/CLIC_CPRINDBILL_PREMIUM.TIF";
											String GeneratedPath ="\\\\gvlfp04\\printreview\\CLIC_CPRINDBILLM_TIFs_UAT1";
											
											// Create  folder Name by Appending Date  
											
											String FolderPrefix ="CLIC_CPRINDBILL_UAT1_"+var_Date;
											
											GeneratedPath = GeneratedPath+"\\"+FolderPrefix;
											
											
											System.out.println("GenereatedPath ::"+GeneratedPath);
											
											
											java.sql.Connection con = null;   

									        // Load SQL Server JDBC driver and establish connection UAT.
									        String connectionUrl = "jdbc:sqlserver://CIS-GIASDBT2:443" + "3;" +
									                "databaseName=CLIS1DT" + "A;"
									                +
									                "IntegratedSecurity = true";
									        System.out.print("Connecting to SQL Server ... ");
									        try {
												con = DriverManager.getConnection(connectionUrl);
											} catch (SQLException e) {
												// TODO Auto-generated catch block
												e.printStackTrace();
											}
								            System.out.println("Connected to database.");
								            
								            String sql1primCL ="Use clis1dta "+
								            		  "Select c.field1, c.company,c.insuranceType, p.noticeType, p.company, p.controlNumber, p.noticeNumber, p.serialNumber, p.insuredName, p.policynumber,"+
								            		 "p.policyNumber, p.amount, p.dueDate, a.accountName, a.accountNameAddress2, a.accountNameAddress4 "+
								            		"from iios.BillingCombinedPol p join iios.BillingCombinedAddr a on p.company = a.company and p.controlNumber = a.controlNumber "+
								            		"and p.noticeNumber = a.noticeNumber "+
								            		"join iios.BillingCombined c on p.company = c.company and p.controlNumber = c.controlNumber and p.noticeNumber = c.noticeNumber "+
								            		"where "+
								            		"field1 = '"+var_DateforDB+"' "+
								            		"and c.company= 'CLPR' "+
								            		"and p.noticeType ='1' "+
								            		"and insuranceType ='L'"+
								            		"order by controlNumber, field1,p.noticeNumber, p.serialNumber";
								            			
								            System.out.println(sql1primCL);
								            Statement statement;
											try {
												statement = con.createStatement();
											
											ResultSet rs = null;
											statement.setMaxRows(1);
											System.out.println("EXECUTED");
											rs = statement.executeQuery(sql1primCL);
											 while (rs.next()) {
												 
												  PolicyNumber =rs.getString("policyNumber");
												 
												  System.out.println("PolicyNumber::"+PolicyNumber);
												  
												  if(PolicyNumber.equalsIgnoreCase("Null")) {
													    policyFound = false ;
												  }
												  else {
													  policyFound = true;
												  }
									            }
											
											 statement.close();
											}
											 catch (SQLException e) {
													// TODO Auto-generated catch block
													e.printStackTrace();
												}
											
											if(policyFound ==true) {
											 
											// List of file names in the folder 
												
											// List of file names in the folder 
												
												File folder = new File(GeneratedPath);
												File[] listOfFiles = folder.listFiles();
												
												System.out.println("Lenght ::"+listOfFiles.length);

												for (int i = 0; i < listOfFiles.length; i++) {
												 // if (listOfFiles[i].isFile()) {
												    System.out.println("File " + listOfFiles[i].getName());
												    System.out.println("POLICY ::"+PolicyNumber);
												    String imagname = listOfFiles[i].getName().trim();
												    System.out.println("Condition:"+imagname.toLowerCase().contains(PolicyNumber.toLowerCase()));
												    System.out.println("imagname"+imagname);
												    if(imagname.toLowerCase().contains(PolicyNumber.trim().toLowerCase())) { 	
												    	PolicyNumberfileName =listOfFiles[i].getName();
												    	GeneratedPath = GeneratedPath+"\\"+PolicyNumberfileName;
												    	i=listOfFiles.length+1;
												    }
												  } 
												 
											//	}
												
												
											System.out.println(" Full Path is ::"+GeneratedPath);
											
											GeneratedPath= GeneratedPath.replace("\\","\\\\");
											
											
											// ALL The API FOR  COMPARING IMAGE
											
											String url = "http://10.1.104.229/compare/index.php";
									    	CloseableHttpClient httpClient = HttpClients.createDefault();
											HttpPost httppost = new HttpPost(url); //http post request
											
											// passing json as a string 
											String entityString ="{\"image1\":\""+BaseImageforPremNotice+"\" ,\"image2\": \""+GeneratedPath+"\",\"coordinates\": \"15,218,779,703|180,38,292,121|605,125,750,176|652,740,775,790|126,812,253,844|96,907,336,980|68,998,768,1025\"}\r\n"
													+ "";
											System.out.println("EntityString::"+entityString);
											try {
												httppost.setEntity(new StringEntity(entityString));
												CloseableHttpResponse closebaleHttpResponse;
												closebaleHttpResponse = httpClient.execute(httppost);
												int statusCode = closebaleHttpResponse.getStatusLine().getStatusCode();
												System.out.println("Status from API:"+statusCode);
												// convert json to string 	
												String responseString = EntityUtils.toString(closebaleHttpResponse.getEntity(), "UTF-8");
												System.out.println("Response from API :"+responseString);
												
												JSONObject response = new JSONObject(responseString);
												System.out.println("Status:"+response.get("msg"));
												if(response.getString("msg").equalsIgnoreCase("Something went wrong in image2")) {
													writeToCSV("Company","CLPR LIFE PREMIUM");
													writeToCSV("Image compared ", "NO CLPR POLICY FOR PREMIUM ");
													writeToCSV("Image Result After comparision", "NULL");
												}
												System.out.println("Image : "+response.getString("image_difference_link"));
												System.out.println("Both the image are same "+response.getBoolean("both_are_same"));
												writeToCSV("Company","CLPR  LIFE PRIMIUM");
												writeToCSV("Image compared ", PolicyNumberfileName);
												writeToCSV("Image Result After comparision", response.getString("image_difference_link"));
												if(response.getBoolean("both_are_same")== true) {
													writeToCSV("Resut", "Both are same");
												}
												else {
													writeToCSV("Resut", "Both are not same");
													Assert.assertTrue(false,"Comparsion failed");
												}
												
												
											} catch (ClientProtocolException e) {
												// TODO Auto-generated catch block
												
											} catch (IOException e) {
												// TODO Auto-generated catch block
												
												e.printStackTrace();
												e.printStackTrace();
											}
										
											
											}
											else {
												
												writeToCSV("Company","CLPR LIFE PREMIUM");
												writeToCSV("Image compared ", "NO CLPR POLICY FOR PREMIUM ");
												writeToCSV("Image Result After comparision", "NULL");
												
												
											}
											
											
						 
				
		 
		 ;} 
		 catch(Exception e) { PrintVal("Renderer Script Exception : "+e.toString()); } 
		 PrintVal("Executed Step = CALLRENDERERSCRIPT,COMPAREIMAGECLPRLIFEPRIM"); 
		try { 
		  
				  
						     	
								    	//  COMPANY OF Texas (CL PR) REMINDER NOTICE  FOR LIFE
								    	
								    	 Boolean policyFound =false; 
								         var_Date= getJsonData(var_CSVData , "$.records["+var_Count+"].Date");
								         String PolicyNumber ="";
								         String PolicyNumberfileName ="";
								         String day="";
								        
								    	 String array[] = var_Date.split("/");
										 System.out.println("1st"+array[0].length());
										 if(array[0].length()==1) {
											 array[0]="0"+array[0];
											 System.out.println("after adding"+array[0]);
										 }
										 if(array[1].length()==1) {
											 array[1]="0"+array[1];
											 System.out.println("after adding"+array[1]);
										 }
										 int dayint =Integer.parseInt(array[1]);
										 day = array[1];
											
											 if(dayint<31) { dayint =dayint +1; day=String.valueOf(dayint);
											 if(day.length()==1) { day ="0"+day; } }
											 
												 
										 String var_DateforDB = array[0]+"/"+array[1]+"/"+array[2];
										  
										 var_Date = array[2]+"-"+array[0]+"-"+day;
										 System.out.println("a: "+var_Date);
								    	
										
											
											// BaseLined Image for CompanyTaxes (REMINDER NOTICE)
											
											String BaseImageforPremNotice ="C:\\/GIAS_Automation\\/Images\\/CLPR\\/Life\\/CLIC_CPRINDBILL_REMINDER.TIF";
											String GeneratedPath ="\\\\gvlfp04\\printreview\\CLIC_CPRINDBILLM_TIFs_UAT1";
											
											// Create  folder Name by Appending Date  
											
											String FolderPrefix ="CLIC_CPRINDBILL_UAT1_"+var_Date;
											
											GeneratedPath = GeneratedPath+"\\"+FolderPrefix;
											
											
											System.out.println("GenereatedPath ::"+GeneratedPath);
											
											
											java.sql.Connection con = null;   

									        // Load SQL Server JDBC driver and establish connection UAT.
									        String connectionUrl = "jdbc:sqlserver://CIS-GIASDBT2:443" + "3;" +
									                "databaseName=CLIS1DT" + "A;"
									                +
									                "IntegratedSecurity = true";
									        System.out.print("Connecting to SQL Server ... ");
									        try {
												con = DriverManager.getConnection(connectionUrl);
											} catch (SQLException e) {
												// TODO Auto-generated catch block
												e.printStackTrace();
											}
								            System.out.println("Connected to database.");
								            
								            String sql1primCL ="Use clis1dta "+
								            		  "Select c.field1, c.company,c.insuranceType, p.noticeType, p.company, p.controlNumber, p.noticeNumber, p.serialNumber, p.insuredName, p.policynumber,"+
								            		 "p.policyNumber, p.amount, p.dueDate, a.accountName, a.accountNameAddress2, a.accountNameAddress4 "+
								            		"from iios.BillingCombinedPol p join iios.BillingCombinedAddr a on p.company = a.company and p.controlNumber = a.controlNumber "+
								            		"and p.noticeNumber = a.noticeNumber "+
								            		"join iios.BillingCombined c on p.company = c.company and p.controlNumber = c.controlNumber and p.noticeNumber = c.noticeNumber "+
								            		"where "+
								            		"field1 = '"+var_DateforDB+"' "+
								            		"and c.company= 'CLPR' "+
								            		"and p.noticeType ='2' "+
								            		"and insuranceType ='L' "+
								            		"order by controlNumber, field1,p.noticeNumber, p.serialNumber";
								            			
								            System.out.println(sql1primCL);
								            Statement statement;
											try {
												statement = con.createStatement();
											
											ResultSet rs = null;
											statement.setMaxRows(1);
											System.out.println("EXECUTED");
											rs = statement.executeQuery(sql1primCL);
											 while (rs.next()) {
												 
												  PolicyNumber =rs.getString("policyNumber");
												 
												  System.out.println("PolicyNumber::"+PolicyNumber);
												  
												  if(PolicyNumber.equalsIgnoreCase("Null")) {
													    policyFound = false ;
												  }
												  else {
													  policyFound = true;
												  }
									            }
											
											 statement.close();
											}
											 catch (SQLException e) {
													// TODO Auto-generated catch block
													e.printStackTrace();
												}
											
											
											System.out.println("POLICY FOUND"+policyFound);
											if(policyFound==true) {
											 
											// List of file names in the folder 
												
												File folder = new File(GeneratedPath);
												File[] listOfFiles = folder.listFiles();
												
												System.out.println("Lenght ::"+listOfFiles.length);

												for (int i = 0; i < listOfFiles.length; i++) {
												 // if (listOfFiles[i].isFile()) {
												    System.out.println("File " + listOfFiles[i].getName());
												    System.out.println("POLICY ::"+PolicyNumber);
												    String imagname = listOfFiles[i].getName().trim();
												    System.out.println("Condition:"+imagname.toLowerCase().contains(PolicyNumber.toLowerCase()));
												    System.out.println("imagname"+imagname);
												    if(imagname.toLowerCase().contains(PolicyNumber.trim().toLowerCase())) { 	
												    	PolicyNumberfileName =listOfFiles[i].getName();
												    	GeneratedPath = GeneratedPath+"\\"+PolicyNumberfileName;
												    	i=listOfFiles.length+1;
												    }
												  } 
												 
											//	}
												
											System.out.println(" Full Path is ::"+GeneratedPath);
											
											GeneratedPath= GeneratedPath.replace("\\","\\\\");
											
											
											// ALL The API FOR  COMPARING IMAGE
											
											String url = "http://10.1.104.229/compare/index.php";
									    	CloseableHttpClient httpClient = HttpClients.createDefault();
											HttpPost httppost = new HttpPost(url); //http post request
											
											// passing json as a string 
											String entityString ="{\"image1\":\""+BaseImageforPremNotice+"\" ,\"image2\": \""+GeneratedPath+"\",\"coordinates\": \"163,38,308,101|15,218,784,703|605,125,750,176|648,742,783,809|126,817,233,842|67,904,346,986|68,995,774,1031\"}\r\n"
													+ "";
											System.out.println("EntityString::"+entityString);
											try {
												httppost.setEntity(new StringEntity(entityString));
												CloseableHttpResponse closebaleHttpResponse;
												closebaleHttpResponse = httpClient.execute(httppost);
												int statusCode = closebaleHttpResponse.getStatusLine().getStatusCode();
												System.out.println("Status from API:"+statusCode);
												// convert json to string 	
												String responseString = EntityUtils.toString(closebaleHttpResponse.getEntity(), "UTF-8");
												System.out.println("Response from API :"+responseString);
												
												JSONObject response = new JSONObject(responseString);
												System.out.println("Status:"+response.get("msg"));
												if(response.getString("msg").equalsIgnoreCase("Something went wrong in image2")) {
													writeToCSV("Company","CLPR LIFE REMIDER NOTICE");
													writeToCSV("Image compared ", "NO CLPR LIFE REMIDER NOTICE ");
													writeToCSV("Image Result After comparision", "NULL");
												}
												System.out.println("Image : "+response.getString("image_difference_link"));
												System.out.println("Both the image are same "+response.getBoolean("both_are_same"));
												writeToCSV("Company","CLPR LIFE REMIDER NOTICE");
												writeToCSV("Image compared ", PolicyNumberfileName);
												writeToCSV("Image Result After comparision", response.getString("image_difference_link"));
												if(response.getBoolean("both_are_same")== true) {
													writeToCSV("Resut", "Both are same");
												}
												else {
													writeToCSV("Resut", "Both are not same");
													Assert.assertTrue(false,"Comparsion failed");
													
												}
												
												
											} catch (ClientProtocolException e) {
												// TODO Auto-generated catch block
												e.printStackTrace();
											} catch (IOException e) {
												// TODO Auto-generated catch block
												e.printStackTrace();
											}
										
											
											}
											else {
												
												writeToCSV("Company","CLPR  LIFE REMINDER  NOTICE");
												writeToCSV("Image compared ", "NO CLPR POLICY FOR LIFE REMINDER NOTICE ");
												writeToCSV("Image Result After comparision", "NULL");
												
												
											}
											
											
						 
				 
			
		 
		 ;} 
		 catch(Exception e) { PrintVal("Renderer Script Exception : "+e.toString()); } 
		 PrintVal("Executed Step = CALLRENDERERSCRIPT,COMPAREIMAGECLPRLIFEREMINDER"); 
		try { 
		  
				  
						 		    	
								    	//  COMPANY OF Texas (CL PR) LAPSE NOTICE  FOR HEALTH
								    	
									 Boolean policyFound =false; 
							       var_Date= getJsonData(var_CSVData , "$.records["+var_Count+"].Date");
							         String PolicyNumber ="";
							         String PolicyNumberfileName ="";
							         String day="";
							        
							    	 String array[] = var_Date.split("/");
									 System.out.println("1st"+array[0].length());
									 if(array[0].length()==1) {
										 array[0]="0"+array[0];
										 System.out.println("after adding"+array[0]);
									 }
									 if(array[1].length()==1) {
										 array[1]="0"+array[1];
										 System.out.println("after adding"+array[1]);
									 }
									 int dayint =Integer.parseInt(array[1]);
									 day = array[1];
										
										 // if(dayint<31) { dayint =dayint +1; day=String.valueOf(dayint);
										 //if(day.length()==1) { day ="0"+day; } }
										 
									 String var_DateforDB = array[0]+"/"+array[1]+"/"+array[2];
									  
									 var_Date = array[2]+"-"+array[0]+"-"+day;
									 System.out.println("a: "+var_Date);
										
											
											// BaseLined Image for CompanyTaxes (LapseNOTICE LIFE)
											
											String BaseImageforPremNotice ="C:\\/GIAS_Automation\\/Images\\/CLPR\\/CLIC_CPRINDBILL_LAPSE.TIF";
											String GeneratedPath ="\\\\gvlfp04\\printreview\\CLIC_CPRINDBILLM_TIFs_UAT1";
											
											// Create  folder Name by Appending Date  
											
											String FolderPrefix ="CLIC_CPRINDBILL_UAT1_"+var_Date;
											
											GeneratedPath = GeneratedPath+"\\"+FolderPrefix;
											
											
											System.out.println("GenereatedPath ::"+GeneratedPath);
											
											
											java.sql.Connection con = null;   

									        // Load SQL Server JDBC driver and establish connection UAT.
									        String connectionUrl = "jdbc:sqlserver://CIS-GIASDBT2:443" + "3;" +
									                "databaseName=CLIS1DT" + "A;"
									                +
									                "IntegratedSecurity = true";
									        System.out.print("Connecting to SQL Server ... ");
									        try {
												con = DriverManager.getConnection(connectionUrl);
											} catch (SQLException e) {
												// TODO Auto-generated catch block
												e.printStackTrace();
											}
								            System.out.println("Connected to database.");
								            
								           System.out.println("POLICY NUMBER ::::::"+PolicyNumber);
								            
								            String sql1primCL ="Use clis1dta "+
								            		  "Select c.field1, c.company,c.insuranceType, p.noticeType, p.company, p.controlNumber, p.noticeNumber, p.serialNumber, p.insuredName, p.policynumber,"+
								            		 "p.policyNumber, p.amount, p.dueDate, a.accountName, a.accountNameAddress2, a.accountNameAddress4 "+
								            		"from iios.BillingCombinedPol p join iios.BillingCombinedAddr a on p.company = a.company and p.controlNumber = a.controlNumber "+
								            		"and p.noticeNumber = a.noticeNumber "+
								            		"join iios.BillingCombined c on p.company = c.company and p.controlNumber = c.controlNumber and p.noticeNumber = c.noticeNumber "+
								            		"where "+
								            		"field1 = '"+var_DateforDB+"' "+
								            		"and c.company= 'CLPR' "+
								            		"and p.noticeType ='3' "+
								            		"and insuranceType ='H'"+
								            		"order by controlNumber, field1,p.noticeNumber, p.serialNumber";
								            			
								            System.out.println(sql1primCL);
								            Statement statement;
											try {
												statement = con.createStatement();
											
											ResultSet rs = null;
											statement.setMaxRows(1);
											System.out.println("EXECUTED");
											rs = statement.executeQuery(sql1primCL);
											 while (rs.next()) {
												 
												  PolicyNumber =rs.getString("policyNumber");
												 
												  System.out.println("PolicyNumber::"+PolicyNumber);
												  
												  if(PolicyNumber.equalsIgnoreCase("Null")) {
													    policyFound = false ;
												  }
												  else {
													  policyFound = true;
												  }
									            }
											
											 statement.close();
											}
											 catch (SQLException e) {
													// TODO Auto-generated catch block
													e.printStackTrace();
												}
											
											if(policyFound ==true) {
											 
											// List of file names in the folder 
												
											// List of file names in the folder 
												
												File folder = new File(GeneratedPath);
												File[] listOfFiles = folder.listFiles();
												
												System.out.println("Lenght ::"+listOfFiles.length);

												for (int i = 0; i < listOfFiles.length; i++) {
												 // if (listOfFiles[i].isFile()) {
												    System.out.println("File " + listOfFiles[i].getName());
												    System.out.println("POLICY ::"+PolicyNumber);
												    String imagname = listOfFiles[i].getName().trim();
												    System.out.println("Condition:"+imagname.toLowerCase().contains(PolicyNumber.toLowerCase()));
												    System.out.println("imagname"+imagname);
												    if(imagname.toLowerCase().contains(PolicyNumber.trim().toLowerCase())) { 	
												    	PolicyNumberfileName =listOfFiles[i].getName();
												    	GeneratedPath = GeneratedPath+"\\"+PolicyNumberfileName;
												    	i=listOfFiles.length+1;
												    }
												  } 
												 
											//	}
												
												
												
											System.out.println(" Full Path is ::"+GeneratedPath);
											
											GeneratedPath= GeneratedPath.replace("\\","\\\\");
											
											
											// ALL The API FOR  COMPARING IMAGE
											
											String url = "http://10.1.104.229/compare/index.php";
									    	CloseableHttpClient httpClient = HttpClients.createDefault();
											HttpPost httppost = new HttpPost(url); //http post request
											
											// passing json as a string 
											String entityString ="{\"image1\":\""+BaseImageforPremNotice+"\" ,\"image2\": \""+GeneratedPath+"\",\"coordinates\": \"187,38,286,109|618,115,755,155|13,221,802,640|648,742,783,809|126,817,233,842|67,904,346,986|68,995,774,1031\"}\r\n"
													+ "";
											System.out.println("EntityString::"+entityString);
											try {
												httppost.setEntity(new StringEntity(entityString));
												CloseableHttpResponse closebaleHttpResponse;
												closebaleHttpResponse = httpClient.execute(httppost);
												int statusCode = closebaleHttpResponse.getStatusLine().getStatusCode();
												System.out.println("Status from API:"+statusCode);
												// convert json to string 	
												String responseString = EntityUtils.toString(closebaleHttpResponse.getEntity(), "UTF-8");
												System.out.println("Response from API :"+responseString);
												
												JSONObject response = new JSONObject(responseString);
												if(response.getString("msg").equalsIgnoreCase("Something went wrong in image2")) {
													writeToCSV("Company","CLPR HEALTH LAPSE NOTICE");
													writeToCSV("Image compared ", "NO CLPR HEALTH LAPSE NOTICE ");
													writeToCSV("Image Result After comparision", "NULL");
												}
												
												System.out.println("Image : "+response.getString("image_difference_link"));
												System.out.println("Both the image are same "+response.getBoolean("both_are_same"));
												writeToCSV("Company","CLPR HEALTH LAPSE NOTICE");
												writeToCSV("Image compared ", PolicyNumberfileName);
												writeToCSV("Image Result After comparision", response.getString("image_difference_link"));
												if(response.getBoolean("both_are_same")== true) {
													writeToCSV("Resut", "Both are same");
												}
												else {
													writeToCSV("Resut", "Both are not same");
													Assert.assertTrue(false, "Comparsion failed");
												}
												
												
											} catch (ClientProtocolException e) {
												// TODO Auto-generated catch block
												e.printStackTrace();
											} catch (IOException e) {
												// TODO Auto-generated catch block
												e.printStackTrace();
											}
										
											
											}
											else {
												
												writeToCSV("Company","CLPR HEALTH LAPSENOTICE");
												writeToCSV("Image compared ", "NO CLPR POLICY FOR LAPSENOTICE ");
												writeToCSV("Image Result After comparision", "NULL");
												
												
											}
											
											
											
						 
						 
				
		 
		 ;} 
		 catch(Exception e) { PrintVal("Renderer Script Exception : "+e.toString()); } 
		 PrintVal("Executed Step = CALLRENDERERSCRIPT,COMPAREIMAGECLPRHEALTHLAPSE"); 

    }


    @Test
    public void clicregcompareimageclna2() {

        // No need to declare / print method name here, handled by TestNg listener
        // declare("clicregcompareimageclna2");

		int var_Count = 0;
                 LOGGER.info("Executed Step = VAR,Integer,var_Count,0,TGTYPESCREENREG");
		com.jayway.jsonpath.ReadContext var_CSVData = getJsonData("C:/GIAS_Automation/CLIC Regression/QUA1/InputData/CLIC36_IP_CompareImages.csv");
                 LOGGER.info("Executed Step = VAR,Json,var_CSVData,C:/GIAS_Automation/CLIC Regression/QUA1/InputData/CLIC36_IP_CompareImages.csv,TGTYPESCREENREG");
		String var_Date;
                 LOGGER.info("Executed Step = VAR,String,var_Date,TGTYPESCREENREG");
		var_Date = getJsonData(var_CSVData , "$.records["+var_Count+"].Date");
                 LOGGER.info("Executed Step = STORE,var_Date,var_CSVData,$.records[{var_Count}].Date,TGTYPESCREENREG");
		try { 
		  
					  
			    	
			    	//  COMPANY OF Texas (CL NA) PREMIUM NOTICE FOR HEATH
			    	
				 Boolean policyFound =false; 
		        // var_Date= getJsonData(var_CSVData , "$.records["+var_Count+"].Date");
		         String PolicyNumber ="";
		         String PolicyNumberfileName ="";
		         String day="";
		        
		    	 String array[] = var_Date.split("/");
				 System.out.println("1st"+array[0].length());
				 if(array[0].length()==1) {
					 array[0]="0"+array[0];
					 System.out.println("after adding"+array[0]);
				 }
				 if(array[1].length()==1) {
					 array[1]="0"+array[1];
					 System.out.println("after adding"+array[1]);
				 }
				 int dayint =Integer.parseInt(array[1]);
				 day = array[1];
					/*
					 * if(dayint<31) { dayint =dayint +1; day=String.valueOf(dayint);
					 * if(day.length()==1) { day ="0"+day; } }
					 */
						 
				 String var_DateforDB = array[0]+"/"+array[1]+"/"+array[2];
				  
				 var_Date = array[2]+"-"+array[0]+"-"+day;
				 System.out.println("a: "+var_Date);
					
						
						// BaseLined Image for CLNW (PremNotice) for health
						
						String BaseImageforPremNotice ="C:\\/GIAS_Automation\\/Images\\/CLNA\\/CLIC_CNAINDBILL_PREMIUM.TIF";
						String GeneratedPath ="\\\\gvlfp04\\printreview\\CLIC_CNAINDBILLM_TIFs_UAT1";
						
						// Create  folder Name by Appending Date  
						
						String FolderPrefix ="CLIC_CNAINDBILL_UAT1_"+var_Date;
						
						GeneratedPath = GeneratedPath+"\\"+FolderPrefix;
						
						
						System.out.println("GenereatedPath ::"+GeneratedPath);
						
						
						java.sql.Connection con = null;   

				        // Load SQL Server JDBC driver and establish connection UAT.
				        String connectionUrl = "jdbc:sqlserver://CIS-GIASDBT2:443" + "3;" +
				                "databaseName=CLIS1DT" + "A;"
				                +
				                "IntegratedSecurity = true";
				        System.out.print("Connecting to SQL Server ... ");
				        try {
							con = DriverManager.getConnection(connectionUrl);
						} catch (SQLException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
			            System.out.println("Connected to database.");
			            
			            String sql1primCL ="Use clis1dta "+
			            		  "Select c.field1, c.company,c.insuranceType, p.noticeType, p.company, p.controlNumber, p.noticeNumber, p.serialNumber, p.insuredName, p.policynumber,"+
			            		 "p.policyNumber, p.amount, p.dueDate, a.accountName, a.accountNameAddress2, a.accountNameAddress4 "+
			            		"from iios.BillingCombinedPol p join iios.BillingCombinedAddr a on p.company = a.company and p.controlNumber = a.controlNumber "+
			            		"and p.noticeNumber = a.noticeNumber "+
			            		"join iios.BillingCombined c on p.company = c.company and p.controlNumber = c.controlNumber and p.noticeNumber = c.noticeNumber "+
			            		"where "+
			            		"field1 = '"+var_DateforDB+"' "+
			            		"and c.company= 'CLNA' "+
			            		"and p.noticeType ='1' "+
			            		"and insuranceType ='H'"+
			            		"order by controlNumber, field1,p.noticeNumber, p.serialNumber";
			            			
			            System.out.println(sql1primCL);
			            Statement statement;
						try {
							statement = con.createStatement();
						
						ResultSet rs = null;
						statement.setMaxRows(1);
						System.out.println("EXECUTED");
						rs = statement.executeQuery(sql1primCL);
						 while (rs.next()) {
							 
							  PolicyNumber =rs.getString("policyNumber");
							 
							  System.out.println("PolicyNumber::"+PolicyNumber);
							  
							  if(PolicyNumber.equalsIgnoreCase("Null")) {
								    policyFound = false ;
							  }
							  else {
								  policyFound = true;
							  }
				            }
						
						 statement.close();
						}
						 catch (SQLException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
						
						if(policyFound ==true) {
						 
						// List of file names in the folder 
							
						// List of file names in the folder 
							
							File folder = new File(GeneratedPath);
							File[] listOfFiles = folder.listFiles();
							
							System.out.println("Lenght ::"+listOfFiles.length);

							for (int i = 0; i < listOfFiles.length; i++) {
							 // if (listOfFiles[i].isFile()) {
							    System.out.println("File " + listOfFiles[i].getName());
							    System.out.println("POLICY ::"+PolicyNumber);
							    String imagname = listOfFiles[i].getName().trim();
							    System.out.println("Condition:"+imagname.toLowerCase().contains(PolicyNumber.toLowerCase()));
							    System.out.println("imagname"+imagname);
							    if(imagname.toLowerCase().contains(PolicyNumber.trim().toLowerCase())) { 	
							    	PolicyNumberfileName =listOfFiles[i].getName();
							    	GeneratedPath = GeneratedPath+"\\"+PolicyNumberfileName;
							    	i=listOfFiles.length+1;
							    }
							  } 
							 
						//	}
							
							
						System.out.println(" Full Path is ::"+GeneratedPath);
						
						GeneratedPath= GeneratedPath.replace("\\","\\\\");
						
						
						// ALL The API FOR  COMPARING IMAGE
						
						String url = "http://10.1.104.229/compare/index.php";
				    	CloseableHttpClient httpClient = HttpClients.createDefault();
						HttpPost httppost = new HttpPost(url); //http post request
						
						// passing json as a string 
						String entityString ="{\"image1\":\""+BaseImageforPremNotice+"\" ,\"image2\": \""+GeneratedPath+"\",\"coordinates\": \"15,218,779,703|188,35,282,76|652,740,775,790|126,812,253,844|96,907,336,980|68,998,768,1025\"}\r\n"
								+ "";
						System.out.println("EntityString::"+entityString);
						try {
							httppost.setEntity(new StringEntity(entityString));
							CloseableHttpResponse closebaleHttpResponse;
							closebaleHttpResponse = httpClient.execute(httppost);
							int statusCode = closebaleHttpResponse.getStatusLine().getStatusCode();
							System.out.println("Status from API:"+statusCode);
							// convert json to string 	
							String responseString = EntityUtils.toString(closebaleHttpResponse.getEntity(), "UTF-8");
							System.out.println("Response from API :"+responseString);
							
							JSONObject response = new JSONObject(responseString);
							System.out.println("Status:"+response.get("msg"));
							if(response.getString("msg").equalsIgnoreCase("Something went wrong in image2")) {
								writeToCSV("Company","CLNA HEATH PREMIUM");
								writeToCSV("Image compared ", "NO CLNA POLICY FOR PREMIUM ");
								writeToCSV("Image Result After comparision", "NULL");
							}
							System.out.println("Image : "+response.getString("image_difference_link"));
							System.out.println("Both the image are same "+response.getBoolean("both_are_same"));
							writeToCSV("Company","CLNA  HEALTH PRIMIUM");
							writeToCSV("Image compared ", PolicyNumberfileName);
							writeToCSV("Image Result After comparision", response.getString("image_difference_link"));
							if(response.getBoolean("both_are_same")== true) {
								writeToCSV("Resut", "Both are same");
							}
							else {
								writeToCSV("Resut", "Both are not same");
								Assert.assertTrue(false,"Comparsion failed");
							}
							
							
						} catch (ClientProtocolException e) {
							// TODO Auto-generated catch block
							
						} catch (IOException e) {
							// TODO Auto-generated catch block
							
							e.printStackTrace();
							e.printStackTrace();
						}
					
						
						}
						else {
							
							writeToCSV("Company","CLNA HEATH PREMIUM");
							writeToCSV("Image compared ", "NO CLNA POLICY FOR PREMIUM ");
							writeToCSV("Image Result After comparision", "NULL");
							
							
						}
						
						
		 
		 ;} 
		 catch(Exception e) { PrintVal("Renderer Script Exception : "+e.toString()); } 
		 PrintVal("Executed Step = CALLRENDERERSCRIPT,COMPARECLNAHEALTHPREM2"); 
		try { 
		  
					  
			     	
			    	//  COMPANY OF Texas (CL NA) REMINDER NOTICE  FOR HEALTH
			    	
			    	 Boolean policyFound =false; 
			         var_Date= getJsonData(var_CSVData , "$.records["+var_Count+"].Date");
			         String PolicyNumber ="";
			         String PolicyNumberfileName ="";
			         String day="";
			        
			    	 String array[] = var_Date.split("/");
					 System.out.println("1st"+array[0].length());
					 if(array[0].length()==1) {
						 array[0]="0"+array[0];
						 System.out.println("after adding"+array[0]);
					 }
					 if(array[1].length()==1) {
						 array[1]="0"+array[1];
						 System.out.println("after adding"+array[1]);
					 }
					 int dayint =Integer.parseInt(array[1]);
					 day = array[1];
						/*
						 * if(dayint<31) { dayint =dayint +1; day=String.valueOf(dayint);
						 * if(day.length()==1) { day ="0"+day; } }
						 */
							 
					 String var_DateforDB = array[0]+"/"+array[1]+"/"+array[2];
					  
					 var_Date = array[2]+"-"+array[0]+"-"+day;
					 System.out.println("a: "+var_Date);
			    	
					
						
						// BaseLined Image for CompanyTaxes (REMINDER NOTICE)
						
						String BaseImageforPremNotice ="C:\\/GIAS_Automation\\/Images\\/CLNA\\/CLIC_CNAINDBILL_REMINDER.TIF";
						String GeneratedPath ="\\\\gvlfp04\\printreview\\CLIC_CNAINDBILLM_TIFs_UAT1";
						
						// Create  folder Name by Appending Date  
						
						String FolderPrefix ="CLIC_CNAINDBILL_UAT1_"+var_Date;
						
						GeneratedPath = GeneratedPath+"\\"+FolderPrefix;
						
						
						System.out.println("GenereatedPath ::"+GeneratedPath);
						
						
						java.sql.Connection con = null;   

				        // Load SQL Server JDBC driver and establish connection UAT.
				        String connectionUrl = "jdbc:sqlserver://CIS-GIASDBT2:443" + "3;" +
				                "databaseName=CLIS1DT" + "A;"
				                +
				                "IntegratedSecurity = true";
				        System.out.print("Connecting to SQL Server ... ");
				        try {
							con = DriverManager.getConnection(connectionUrl);
						} catch (SQLException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
			            System.out.println("Connected to database.");
			            
			            String sql1primCL ="Use clis1dta "+
			            		  "Select c.field1, c.company,c.insuranceType, p.noticeType, p.company, p.controlNumber, p.noticeNumber, p.serialNumber, p.insuredName, p.policynumber,"+
			            		 "p.policyNumber, p.amount, p.dueDate, a.accountName, a.accountNameAddress2, a.accountNameAddress4 "+
			            		"from iios.BillingCombinedPol p join iios.BillingCombinedAddr a on p.company = a.company and p.controlNumber = a.controlNumber "+
			            		"and p.noticeNumber = a.noticeNumber "+
			            		"join iios.BillingCombined c on p.company = c.company and p.controlNumber = c.controlNumber and p.noticeNumber = c.noticeNumber "+
			            		"where "+
			            		"field1 = '"+var_DateforDB+"' "+
			            		"and c.company= 'CLNA' "+
			            		"and p.noticeType ='2' "+
			            		"and insuranceType ='H' "+
			            		"order by controlNumber, field1,p.noticeNumber, p.serialNumber";
			            			
			            System.out.println(sql1primCL);
			            Statement statement;
						try {
							statement = con.createStatement();
						
						ResultSet rs = null;
						statement.setMaxRows(1);
						System.out.println("EXECUTED");
						rs = statement.executeQuery(sql1primCL);
						 while (rs.next()) {
							 
							  PolicyNumber =rs.getString("policyNumber");
							 
							  System.out.println("PolicyNumber::"+PolicyNumber);
							  
							  if(PolicyNumber.equalsIgnoreCase("Null")) {
								    policyFound = false ;
							  }
							  else {
								  policyFound = true;
							  }
				            }
						
						 statement.close();
						}
						 catch (SQLException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
						
						
						System.out.println("POLICY FOUND"+policyFound);
						if(policyFound==true) {
						 
						// List of file names in the folder 
							
							File folder = new File(GeneratedPath);
							File[] listOfFiles = folder.listFiles();
							
							System.out.println("Lenght ::"+listOfFiles.length);

							for (int i = 0; i < listOfFiles.length; i++) {
							 // if (listOfFiles[i].isFile()) {
							    System.out.println("File " + listOfFiles[i].getName());
							    System.out.println("POLICY ::"+PolicyNumber);
							    String imagname = listOfFiles[i].getName().trim();
							    System.out.println("Condition:"+imagname.toLowerCase().contains(PolicyNumber.toLowerCase()));
							    System.out.println("imagname"+imagname);
							    if(imagname.toLowerCase().contains(PolicyNumber.trim().toLowerCase())) { 	
							    	PolicyNumberfileName =listOfFiles[i].getName();
							    	GeneratedPath = GeneratedPath+"\\"+PolicyNumberfileName;
							    	i=listOfFiles.length+1;
							    }
							  } 
							 
						//	}
							
						System.out.println(" Full Path is ::"+GeneratedPath);
						
						GeneratedPath= GeneratedPath.replace("\\","\\\\");
						
						
						// ALL The API FOR  COMPARING IMAGE
						
						String url = "http://10.1.104.229/compare/index.php";
				    	CloseableHttpClient httpClient = HttpClients.createDefault();
						HttpPost httppost = new HttpPost(url); //http post request
						
						// passing json as a string 
						String entityString ="{\"image1\":\""+BaseImageforPremNotice+"\" ,\"image2\": \""+GeneratedPath+"\",\"coordinates\": \"163,38,304,83|19,218,784,697|648,742,783,809|126,817,233,842|67,904,346,986|68,995,774,1031\"}\r\n"
								+ "";
						System.out.println("EntityString::"+entityString);
						try {
							httppost.setEntity(new StringEntity(entityString));
							CloseableHttpResponse closebaleHttpResponse;
							closebaleHttpResponse = httpClient.execute(httppost);
							int statusCode = closebaleHttpResponse.getStatusLine().getStatusCode();
							System.out.println("Status from API:"+statusCode);
							// convert json to string 	
							String responseString = EntityUtils.toString(closebaleHttpResponse.getEntity(), "UTF-8");
							System.out.println("Response from API :"+responseString);
							
							JSONObject response = new JSONObject(responseString);
							System.out.println("Status:"+response.get("msg"));
							if(response.getString("msg").equalsIgnoreCase("Something went wrong in image2")) {
								writeToCSV("Company","CLNA HEALTH REMIDER NOTICE");
								writeToCSV("Image compared ", "NO CLNA HEALTH REMIDER NOTICE ");
								writeToCSV("Image Result After comparision", "NULL");
							}
							System.out.println("Image : "+response.getString("image_difference_link"));
							System.out.println("Both the image are same "+response.getBoolean("both_are_same"));
							writeToCSV("Company","CLNA HEALTH REMIDER NOTICE");
							writeToCSV("Image compared ", PolicyNumberfileName);
							writeToCSV("Image Result After comparision", response.getString("image_difference_link"));
							if(response.getBoolean("both_are_same")== true) {
								writeToCSV("Resut", "Both are same");
							}
							else {
								writeToCSV("Resut", "Both are not same");
							}
							
							
						} catch (ClientProtocolException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						} catch (IOException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					
						
						}
						else {
							
							writeToCSV("Company","CLNA  HEALTH REMINDER  NOTICE");
							writeToCSV("Image compared ", "NO CLNA POLICY FOR HEATH REMINDER NOTICE ");
							writeToCSV("Image Result After comparision", "NULL");
							
							
						}
						
						
		 
		 ;} 
		 catch(Exception e) { PrintVal("Renderer Script Exception : "+e.toString()); } 
		 PrintVal("Executed Step = CALLRENDERERSCRIPT,COMPAREIMAGECLNAHEALTHREMINDER2"); 
		try { 
		  
					  
				    	
			    	//  COMPANY OF Texas (CL NA) LAPSE NOTICE  FOR HEALTH
			    	
				 Boolean policyFound =false; 
		       var_Date= getJsonData(var_CSVData , "$.records["+var_Count+"].Date");
		         String PolicyNumber ="";
		         String PolicyNumberfileName ="";
		         String day="";
		        
		    	 String array[] = var_Date.split("/");
				 System.out.println("1st"+array[0].length());
				 if(array[0].length()==1) {
					 array[0]="0"+array[0];
					 System.out.println("after adding"+array[0]);
				 }
				 if(array[1].length()==1) {
					 array[1]="0"+array[1];
					 System.out.println("after adding"+array[1]);
				 }
				 int dayint =Integer.parseInt(array[1]);
				 day = array[1];
					/*
					 * if(dayint<31) { dayint =dayint +1; day=String.valueOf(dayint);
					 * if(day.length()==1) { day ="0"+day; } }
					 */
				 String var_DateforDB = array[0]+"/"+array[1]+"/"+array[2];
				  
				 var_Date = array[2]+"-"+array[0]+"-"+day;
				 System.out.println("a: "+var_Date);
					
						
						// BaseLined Image for CompanyTaxes (LapseNOTICE LIFE)
						
						String BaseImageforPremNotice ="C:\\/GIAS_Automation\\/Images\\/CLNA\\/CLIC_CNAINDBILL_LAPSE.TIF";
						String GeneratedPath ="\\\\gvlfp04\\printreview\\CLIC_CNAINDBILLM_TIFs_UAT1";
						
						// Create  folder Name by Appending Date  
						
						String FolderPrefix ="CLIC_CNAINDBILL_UAT1_"+var_Date;
						
						GeneratedPath = GeneratedPath+"\\"+FolderPrefix;
						
						
						System.out.println("GenereatedPath ::"+GeneratedPath);
						
						
						java.sql.Connection con = null;   

				        // Load SQL Server JDBC driver and establish connection UAT.
				        String connectionUrl = "jdbc:sqlserver://CIS-GIASDBT2:443" + "3;" +
				                "databaseName=CLIS1DT" + "A;"
				                +
				                "IntegratedSecurity = true";
				        System.out.print("Connecting to SQL Server ... ");
				        try {
							con = DriverManager.getConnection(connectionUrl);
						} catch (SQLException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
			            System.out.println("Connected to database.");
			            
			           System.out.println("POLICY NUMBER ::::::"+PolicyNumber);
			            
			            String sql1primCL ="Use clis1dta "+
			            		  "Select c.field1, c.company,c.insuranceType, p.noticeType, p.company, p.controlNumber, p.noticeNumber, p.serialNumber, p.insuredName, p.policynumber,"+
			            		 "p.policyNumber, p.amount, p.dueDate, a.accountName, a.accountNameAddress2, a.accountNameAddress4 "+
			            		"from iios.BillingCombinedPol p join iios.BillingCombinedAddr a on p.company = a.company and p.controlNumber = a.controlNumber "+
			            		"and p.noticeNumber = a.noticeNumber "+
			            		"join iios.BillingCombined c on p.company = c.company and p.controlNumber = c.controlNumber and p.noticeNumber = c.noticeNumber "+
			            		"where "+
			            		"field1 = '"+var_DateforDB+"' "+
			            		"and c.company= 'CLNA' "+
			            		"and p.noticeType ='3' "+
			            		"and insuranceType ='H'"+
			            		"order by controlNumber, field1,p.noticeNumber, p.serialNumber";
			            			
			            System.out.println(sql1primCL);
			            Statement statement;
						try {
							statement = con.createStatement();
						
						ResultSet rs = null;
						statement.setMaxRows(1);
						System.out.println("EXECUTED");
						rs = statement.executeQuery(sql1primCL);
						 while (rs.next()) {
							 
							  PolicyNumber =rs.getString("policyNumber");
							 
							  System.out.println("PolicyNumber::"+PolicyNumber);
							  
							  if(PolicyNumber.equalsIgnoreCase("Null")) {
								    policyFound = false ;
							  }
							  else {
								  policyFound = true;
							  }
				            }
						
						 statement.close();
						}
						 catch (SQLException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
						
						if(policyFound ==true) {
						 
						// List of file names in the folder 
							
						// List of file names in the folder 
							
							File folder = new File(GeneratedPath);
							File[] listOfFiles = folder.listFiles();
							
							System.out.println("Lenght ::"+listOfFiles.length);

							for (int i = 0; i < listOfFiles.length; i++) {
							 // if (listOfFiles[i].isFile()) {
							    System.out.println("File " + listOfFiles[i].getName());
							    System.out.println("POLICY ::"+PolicyNumber);
							    String imagname = listOfFiles[i].getName().trim();
							    System.out.println("Condition:"+imagname.toLowerCase().contains(PolicyNumber.toLowerCase()));
							    System.out.println("imagname"+imagname);
							    if(imagname.toLowerCase().contains(PolicyNumber.trim().toLowerCase())) { 	
							    	PolicyNumberfileName =listOfFiles[i].getName();
							    	GeneratedPath = GeneratedPath+"\\"+PolicyNumberfileName;
							    	i=listOfFiles.length+1;
							    }
							  } 
							 
						//	}
							
							
						System.out.println(" Full Path is ::"+GeneratedPath);
						
						GeneratedPath= GeneratedPath.replace("\\","\\\\");
						
						
						// ALL The API FOR  COMPARING IMAGE
						
						String url = "http://10.1.104.229/compare/index.php";
				    	CloseableHttpClient httpClient = HttpClients.createDefault();
						HttpPost httppost = new HttpPost(url); //http post request
						
						// passing json as a string 
						String entityString ="{\"image1\":\""+BaseImageforPremNotice+"\" ,\"image2\": \""+GeneratedPath+"\",\"coordinates\": \"187,38,286,109|618,115,755,155|13,221,802,640|648,742,783,809|126,817,233,842|67,904,346,986|68,995,774,1031\"}\r\n"
								+ "";
						System.out.println("EntityString::"+entityString);
						try {
							httppost.setEntity(new StringEntity(entityString));
							CloseableHttpResponse closebaleHttpResponse;
							closebaleHttpResponse = httpClient.execute(httppost);
							int statusCode = closebaleHttpResponse.getStatusLine().getStatusCode();
							System.out.println("Status from API:"+statusCode);
							// convert json to string 	
							String responseString = EntityUtils.toString(closebaleHttpResponse.getEntity(), "UTF-8");
							System.out.println("Response from API :"+responseString);
							
							JSONObject response = new JSONObject(responseString);
							if(response.getString("msg").equalsIgnoreCase("Something went wrong in image2")) {
								writeToCSV("Company","CLNA HEALTH LAPSE NOTICE");
								writeToCSV("Image compared ", "NO CLNA HEALTH LAPSE NOTICE ");
								writeToCSV("Image Result After comparision", "NULL");
							}
							
							System.out.println("Image : "+response.getString("image_difference_link"));
							System.out.println("Both the image are same "+response.getBoolean("both_are_same"));
							writeToCSV("Company","CLNA HEALTH LAPSE NOTICE");
							writeToCSV("Image compared ", PolicyNumberfileName);
							writeToCSV("Image Result After comparision", response.getString("image_difference_link"));
							if(response.getBoolean("both_are_same")== true) {
								writeToCSV("Resut", "Both are same");
							}
							else {
								writeToCSV("Resut", "Both are not same");
							}
							
							
						} catch (ClientProtocolException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						} catch (IOException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					
						
						}
						else {
							
							writeToCSV("Company","CLNA HEALTH LAPSENOTICE");
							writeToCSV("Image compared ", "NO CLNA POLICY FOR LAPSENOTICE ");
							writeToCSV("Image Result After comparision", "NULL");
							
							
						}
						
						
						
		 
		 ;} 
		 catch(Exception e) { PrintVal("Renderer Script Exception : "+e.toString()); } 
		 PrintVal("Executed Step = CALLRENDERERSCRIPT,COMPAREIMAGECLNAHEALTHLAPSE2"); 

    }


}
